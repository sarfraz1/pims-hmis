'use strict';

module.exports = {
	app: {
		title: 'talkhmis',
		description: 'TalkHealth HMIS',
		keywords: 'MongoDB, Express, AngularJS, Node.js'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/jscrollpane/style/jquery.jscrollpane.css',
				'public/lib/angular-ui-grid/ui-grid.css',
				'public/lib/ngToast/dist/ngToast.min.css',
				'public/lib/ladda/dist/ladda-themeless.min.css',
				'public/lib/angular-datepicker/dist/angular-datepicker.min.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/jscrollpane/script/jquery.jscrollpane.js',
				'public/lib/angular-ui-grid/ui-grid.js',
				'public/lib/ng-file-upload/ng-file-upload.min.js',
				'public/lib/gsap/src/minified/TimelineMax.min.js',
				'public/lib/gsap/src/minified/TweenMax.min.js',
				'public/lib/ngToast/dist/ngToast.min.js',
				'public/lib/ladda/dist/spin.min.js',
				'public/lib/ladda/dist/ladda.min.js',
				'public/lib/angular-ladda/dist/angular-ladda.min.js',
				'public/lib/angular-datepicker/dist/angular-datepicker.min.js',
				'public/lib/moment/min/moment.min.js',
				'public/lib/lodash/dist/lodash.min.js',
				'public/lib/json-export-excel/dest/json-export-excel.min.js',
				'public/lib/file-saver/FileSaver.min.js',
				'public/lib/angular-io-barcode/build/angular-io-barcode.min.js',
				'public/lib/d3/d3.js',
				'public/lib/nvd3/build/nv.d3.js',
				'public/lib/angular-nvd3/dist/angular-nvd3.js'

			]
		},
		css: [
			'public/style/css/*.css',
			'public/lib/nvd3/build/nv.d3.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};