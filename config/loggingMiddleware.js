var winston = require("winston");
var expressWinston = require("express-winston");

module.exports = expressWinston.logger({
  format: winston.format.json(),
  requestWhitelist: ["query", "body"],
  bodyBlacklist: ["password"],
  transports: [
    new winston.transports.File({
      filename: `logs/talkhmis.log`,
      level: "info"
    }),
    new winston.transports.File({
      filename: `logs/talkhmis-errors.log`,
      level: "error"
    })
  ],
  dynamicMeta: function(req, res) {
    return { user: req.user, timestamp: (new Date()).toISOString() };
  },
  meta: true,
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: function(req, res) {
    return req.method === "GET";
  } // optional: allows to skip some log messages based on request and/or response
});
