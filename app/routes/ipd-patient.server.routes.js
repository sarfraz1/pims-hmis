'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var patient = require('../../app/controllers/ipd-patient.server.controller');

	// Inventories Routes
	app.route('/ipdpatient')
		.get(patient.list)
		.post(patient.create);

		app.route('/admittedipdpatient')
			.get(patient.list_admitted)
		//	.post(patient.create);

	app.route('/ipdpatient/:key')
		.get(patient.read)
		.put(patient.update)
		.delete(patient.delete);

	app.route('/ipdpatient/updatemedicine')
		.post(patient.updatemedicine);

	app.route('/getipdPatient/:keyword')
		.get(patient.search);
};
