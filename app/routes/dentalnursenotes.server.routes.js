'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var dentalnursenotes = require('../../app/controllers/dentalnursenotes.server.controller');

	app.route('/dentalnursenotes')
		.get(dentalnursenotes.list)
		.post(dentalnursenotes.create);

	app.route('/notes/reasonforcoming/:reason')
		.get(dentalnursenotes.getReasons);

	app.route('/dentalnursenotes/:mr_number')
		.get(dentalnursenotes.getbyMrNo);
		
	app.route('/getAppointmentDentalNurseNotes/:appintment_id')
		.get(dentalnursenotes.getAppointmentDentalNurseNotes)
		.put(dentalnursenotes.updateAppointmentDentalNurseNotes);
		
};