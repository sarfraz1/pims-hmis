'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var facility = require('../../app/controllers/facility.server.controller');

	// Facility Routes
	app.route('/facility')
		.post(facility.create)
		.get(facility.list);
	
	app.route('/facilitylastNodes')
		.get(facility.lastNodes);
		
	app.route('/facility/:node')
		.get(facility.read)
		.delete(facility.removeNode);
		
	app.route('/removeFacility')
		.post(facility.delete);


};
