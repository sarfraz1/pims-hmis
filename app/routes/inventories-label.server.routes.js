'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryLabel = require('../../app/controllers/inventories-label.server.controller');

	app.route('/inventory-label')
		.get(inventoryLabel.list)
		.post(inventoryLabel.create);

	app.route('/inventory-label/:labelId')
		.get(inventoryLabel.read)
		.put(inventoryLabel.update)
		.delete(inventoryLabel.delete);

	app.route('/inventory-label/:searchType/:keyword')
		.get(inventoryLabel.search);
};