'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var radiologyOrder = require('../../app/controllers/radiology-order.server.controller');

	app.route('/radiology-order')
		.get(radiologyOrder.list)
		.post(radiologyOrder.create);

	app.route('/radiology-order/:labOrderId')
		.get(radiologyOrder.read)
		.put(radiologyOrder.update)
		.delete(radiologyOrder.delete);
		
	app.route('/radiologyimage')
		.post(radiologyOrder.createImage);

	app.route('/radiology-order/:labOrderId/:description/:status')
		.put(radiologyOrder.updateStatus);

	app.route('/radiology-orders-date/:fromDate/:toDate')
		.get(radiologyOrder.getByDate);

};