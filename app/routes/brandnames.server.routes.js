'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var brandnames = require('../../app/controllers/BrandNames.server.controller');

	app.route('/brandnames')
		.get(brandnames.list)
		.post(brandnames.create);

	app.route('/brandnames/:medicineId')
		.get(brandnames.read)
		.put(brandnames.update)
		.delete(brandnames.delete);

	app.route('/brandnames/:searchType/:keyword')
		.get(brandnames.search);

	app.route('/brandnamesRoutine')
		.get(brandnames.importfrommedicine);
	
};