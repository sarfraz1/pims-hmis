'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryStock = require('../../app/controllers/storeinventories-stock.server.controller');

	app.route('/storeinventory-stock')
		.get(inventoryStock.list)
		.post(inventoryStock.create);

	app.route('/storeinventory-stock/:stockId')
		.get(inventoryStock.read)
		.put(inventoryStock.update)
		.delete(inventoryStock.delete);

	app.route('/storeinventory-stockreport')
		.get(inventoryStock.stockreport)
	
	app.route('/storeinventory-stockaverageprice/:supplier')
		.get(inventoryStock.stockaverageprice)

	app.route('/storeinventory-lowstockreport')
		.get(inventoryStock.lowstockreport)			
		
	app.route('store/inventory-stock/adjust/:stockId')
		.put(inventoryStock.adjust);

	app.route('/storeinventory-stock/adjustReturn/:stockId')
		.put(inventoryStock.adjustReturn);

	app.route('/storeinventory-stock/check/:stockId')
		.get(inventoryStock.check);

	app.route('/storeinventory-stock/:searchType/:keyword')
		.get(inventoryStock.search);
		
	app.route('/getStoreInventoryStock/description/:keyword')
		.get(inventoryStock.keywordSearch);
		
	app.route('/getStoreInventorybyStore/:keyword/:store')
		.get(inventoryStock.getbyStore);


	app.route('/storeinventory-supplier-stock-list/:supplierId')
		.get(inventoryStock.supplierBasedStock);


};