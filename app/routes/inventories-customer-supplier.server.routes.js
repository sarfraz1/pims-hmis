'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryCustomerSupplier = require('../../app/controllers/inventories-customer-supplier.server.controller');

	app.route('/inventory-customer-supplier')
		.get(inventoryCustomerSupplier.list)
		.post(inventoryCustomerSupplier.create);

	app.route('/inventory-customer-supplier/:customerSupplierId')
		.get(inventoryCustomerSupplier.read)
		.put(inventoryCustomerSupplier.update)
		.delete(inventoryCustomerSupplier.delete);

	app.route('/inventory-customer-supplier/:searchType/:keyword')
		.get(inventoryCustomerSupplier.search);
		
	app.route('/inventory-getmanufacturers')
		.get(inventoryCustomerSupplier.getmanufacturers);
		
};