'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryTransactionCategory = require('../../app/controllers/storeinventories-transaction-category.server.controller');

	app.route('/storeinventory-transaction-category')
		.get(inventoryTransactionCategory.list)
		.post(inventoryTransactionCategory.create);

	app.route('/storeinventory-transaction-category/:transactionCategoryId')
		.get(inventoryTransactionCategory.read)
		.put(inventoryTransactionCategory.update)
		.delete(inventoryTransactionCategory.delete);

	app.route('/storeinventory-transaction-category/:searchType/:keyword')
		.get(inventoryTransactionCategory.search);
};