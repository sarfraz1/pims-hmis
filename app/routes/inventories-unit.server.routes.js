'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryUnit = require('../../app/controllers/inventories-unit.server.controller');

	app.route('/inventory-unit')
		.get(inventoryUnit.list)
		.post(inventoryUnit.create);

	app.route('/inventory-unit/:unitId')
		.get(inventoryUnit.read)
		.put(inventoryUnit.update)
		.delete(inventoryUnit.delete);

	app.route('/inventory-unit/:searchType/:keyword')
		.get(inventoryUnit.search);
};