'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseReturn = require('../../app/controllers/storepurchaseReturn.server.controller');

	app.route('/storePurchaseReturnById/:PR_ID')
		.get(purchaseReturn.PurchaseReturnById);

	app.route('/UpdateStorePurchaseReturn')
		.post(purchaseReturn.UpdatePurchaseReturn);

	app.route('/DeleteStorePurchaseReturn/:obj')
		.get(purchaseReturn.DeletePurchaseReturn);
		

	// Inventories Routes
	app.route('/storepurchaseReturn')
		.get(purchaseReturn.list)
		.post(purchaseReturn.create);




};
