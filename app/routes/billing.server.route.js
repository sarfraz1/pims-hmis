'use strict';
var users = require('../../app/controllers/users.server.controller');
var billing = require('../../app/controllers/billing.server.controller');

module.exports = function(app) {

	// Inventories Routes
	app.route('/bill')
		.get(billing.list)
		.post(billing.create);

	app.route('/patientBill/:MRN')
		.get(billing.patientBill);

	app.route('/patientBillRP/:MRN')
		.get(billing.patientBillRP);

	app.route('/refundBill/:billNum')
		.post(billing.refundBill);

	app.route('/patientHistory/:MRN')
		.get(billing.patientHistory);

	app.route('/billReport/:doctorName/:fromDate/:toDate')
		.get(billing.report);

	app.route('/doctorServicesReport/:doctorName/:doctorId/:fromDate/:toDate')
		.get(billing.servicesReport);

	app.route('/doctorPayableReport/:doctorId/:doctorName/:fromDate/:toDate')
		.get(billing.payableReport);

	app.route('/otaPayableReport/:otaName/:fromDate/:toDate')
		.get(billing.otaPayableReport);

	app.route('/revenueReport/:fromDate/:toDate')
		.get(billing.totalRevenueReport);

	app.route('/revenueReportPatientWise/:fromDate/:toDate/:receptionist')
		.get(billing.revenueReportPatientWise);

	app.route('/panelReport/:fromDate/:toDate/:panel')
		.get(billing.panelReport);

	app.route('/updatePanelBillsStatus')
		.post(billing.updatePanelBillsStatus)

	app.route('/discountReportPatientWise/:fromDate/:toDate/:receptionist')
		.get(billing.discountReportPatientWise);

	app.route('/refundReportPatientWise/:fromDate/:toDate/:receptionist')
		.get(billing.refundReportPatientWise);

	app.route('/addservicestobill')
		.post(billing.addservicestobill);

	app.route('/addservicebynametobill')
		.post(billing.addservicebynametobill);

	app.route('/addservicesbynametobill')
		.post(billing.addservicesbynametobill);

	app.route('/updatePanelBillsStatus')
		.post(billing.updatePanelBillsStatus);

	app.route('/revenueForChart/:fromDate/:toDate')
		.get(billing.getRevenueForChart);

	app.route('/departmentReport/:department/:fromDate/:toDate')
		.get(billing.departmentReport);

	app.route('/billsFiltered/:receptionistName/:fromDate/:toDate/:pageNumber')
		.get(billing.listFilteredBills);
};
