'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseOrder = require('../../app/controllers/purchaseOrder.server.controller');

	// Inventories Routes
	app.route('/purchaseOrder')
		.get(purchaseOrder.list)
		.post(purchaseOrder.create);

	app.route('/getIdPurchaseOrder')
		.get(purchaseOrder.getPurchaseOrderId);

	app.route('/purchaseOrder/:pageNo')
		.get(purchaseOrder.listAll);

	app.route('/purchaseOrder/:purchaseOrderId')
		.put(purchaseOrder.updateOrder);

	app.route('/deletePurchaseOrder')
		.post(purchaseOrder.delete);

	app.route('/purchaseOrder/:searchType/:keyword')
		.get(purchaseOrder.search);
};