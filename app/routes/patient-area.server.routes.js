'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var area = require('../../app/controllers/patient-area.server.controller');

	app.route('/areas')
		.get(area.list)
		.post(area.create);

	app.route('/areas/:areaID')
		.get(area.read)
		.put(area.update)
		.delete(area.delete);
};