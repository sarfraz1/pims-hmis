'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryStore = require('../../app/controllers/inventories-store.server.controller');

	app.route('/inventory-store')
		.get(inventoryStore.list)
		.post(inventoryStore.create);

	app.route('/inventory-store/:storeId')
		.get(inventoryStore.read)
		.put(inventoryStore.update)
		.delete(inventoryStore.delete);

	app.route('/inventory-store/description/:keyword')
		.get(inventoryStore.search);
};