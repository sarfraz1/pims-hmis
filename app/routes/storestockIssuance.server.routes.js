'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var stockIssuance = require('../../app/controllers/storestockIssuance.server.controller');

	// Inventories Routes
	app.route('/storestockIssuance')
		.get(stockIssuance.list)
		.post(stockIssuance.create)
		.put(stockIssuance.update);

	app.route('/storestockIssuance/:pageNo')
		.get(stockIssuance.listAll);

	app.route('/storestockIssuancebyNumber/:number')
		.get(stockIssuance.getByNumber);

	app.route('/storestockIssuance/:id')
		.delete(stockIssuance.delete);

	app.route('/storestockIssuance/:searchType/:keyword')
		.get(stockIssuance.search);
		
};