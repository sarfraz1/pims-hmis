'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var patient = require('../../app/controllers/patient.server.controller');

	// Inventories Routes
	app.route('/patient')
		.get(patient.list)
		.post(patient.create);

	app.route('/patient/:fromdate/:todate')
		.get(patient.list)

	app.route('/patient/:key')
		.get(patient.read)
		.put(patient.update)
		.delete(patient.delete);

	app.route('/patientReport/:fromDate/:toDate/:area')
		.get(patient.patientReport);

	app.route('/getPatient/:keyword')
		.get(patient.search);
};
