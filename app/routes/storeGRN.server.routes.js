'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var GRN = require('../../app/controllers/storeGRN.server.controller');

	// Inventories Routes
	app.route('/storeGRN')
		.get(GRN.list)
		.post(GRN.create)
		.put(GRN.update);

	app.route('/storeGRN/:pageNo')
		.get(GRN.listAll);

	app.route('/StoreGRNbyNumber/:number')
		.get(GRN.getByNumber);

	app.route('/storeGRN/:GRNId')
		.delete(GRN.delete);

	app.route('/storeGRN/:searchType/:keyword')
		.get(GRN.search);
		
	app.route('/storemed-supplier/:medId')
		.get(GRN.getSupplierInventory);
};