'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryLabel = require('../../app/controllers/storeinventories-label.server.controller');

	app.route('/storeinventory-label')
		.get(inventoryLabel.list)
		.post(inventoryLabel.create);

	app.route('/storeinventory-label/:labelId')
		.get(inventoryLabel.read)
		.put(inventoryLabel.update)
		.delete(inventoryLabel.delete);

	app.route('/storeinventory-label/:searchType/:keyword')
		.get(inventoryLabel.search);
};