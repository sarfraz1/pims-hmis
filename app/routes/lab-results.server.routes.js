'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var labResults = require('../../app/controllers/lab-results.server.controller');

	app.route('/lab-result')
		.get(labResults.list)
		.post(labResults.create);

	app.route('/lab-result/:labResultsId')
		.get(labResults.read)
		.put(labResults.update)
		.delete(labResults.delete);

	app.route('/getlabsbyMRN/:patientMRN')
		.get(labResults.getLabsbyMRN);

	app.route('/lab-result/:patientMRN/:testDescription')
		.get(labResults.getPatientTest);

	app.route('/lab-result/:patientMRN/:testDescription/:labOrderId')
		.get(labResults.getPatientTestResult);

	app.route('/patient-test-results/:patientMRN/:testDescription')
		.get(labResults.getPatientTests);

};
