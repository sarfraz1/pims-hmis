'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var prescriptionPref = require('../../app/controllers/prescriptionPref.server.controller');

	app.route('/prescriptionPref')
		.get(prescriptionPref.list)
		.post(prescriptionPref.create);

	app.route('/prescriptionPref/:doctorId')
		.get(prescriptionPref.read);
};