'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var GRN = require('../../app/controllers/GRN.server.controller');

	// Inventories Routes
	app.route('/GRN')
		.get(GRN.list)
		.post(GRN.create)
		.put(GRN.update);

	app.route('/GRN/:pageNo')
		.get(GRN.listAll);

	app.route('/GRNbyNumber/:number')
		.get(GRN.getByNumber);

	app.route('/GRN/:GRNId')
		.delete(GRN.delete);

	app.route('/GRN/:searchType/:keyword')
		.get(GRN.search);
		
	app.route('/med-supplier/:medId')
		.get(GRN.getSupplierInventory);

	app.route('/purchaseReport/:fromDate/:toDate/:pageNumber')
		.get(GRN.purchaseReport);
};