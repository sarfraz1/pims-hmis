'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseRequisition = require('../../app/controllers/storepurchaseRequisition.server.controller');

	// Inventories Routes
	app.route('/storepurchaseRequisition')
		.get(purchaseRequisition.list)
		.post(purchaseRequisition.create);

	app.route('/storepurchaseRequisition/:pageNo')
		.get(purchaseRequisition.listAll);

	app.route('/storepurchaseRequisitionUpdate')
		.post(purchaseRequisition.update);

	app.route('/storepurchaseRequisition/:searchType/:keyword')
		.get(purchaseRequisition.search);

	app.route('/storepurchaseRequisition/:purchaseRequisitionId')
		.delete(purchaseRequisition.delete);
};
