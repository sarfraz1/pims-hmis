'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryPricing = require('../../app/controllers/storeinventories-pricing.server.controller');

	app.route('/storeinventory-pricing')
		.get(inventoryPricing.list)
		.post(inventoryPricing.create);

	app.route('/storeinventory-pricing/:inventoryCode')
		.get(inventoryPricing.read)
		.put(inventoryPricing.create)
		.delete(inventoryPricing.delete);
		
	app.route('/storeinventory-pricing-pos/:inventoryCode')
		.get(inventoryPricing.readpos);

	app.route('/storeinventory-pricing-supplier/:supplierId')
		.get(inventoryPricing.listSupplierInventory);
		
	app.route('/storeinventory-pricing-supplier-stock/:supplierId')
		.get(inventoryPricing.listSupplierInventorywithStock);
		
};