'use strict';
var users = require('../../app/controllers/users.server.controller');
var expenseManagement = require('../../app/controllers/expenseManagement.server.controller');

module.exports = function(app) {
	
	// Inventories Routes
	app.route('/hospitalExpense')
		.get(expenseManagement.listAll)
		.post(expenseManagement.create);

	app.route('/hospitalExpense/:pageNo/:keyword')
		.get(expenseManagement.list);
		
	app.route('/gettags')
		.get(expenseManagement.getTags);

};
