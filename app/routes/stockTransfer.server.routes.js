'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var stockTransfer = require('../../app/controllers/stockTransfer.server.controller');

	// Inventories Routes
	app.route('/stockTransfer')
		.get(stockTransfer.list)
		.post(stockTransfer.create)
		.put(stockTransfer.update);

	app.route('/stockTransfer/:pageNo')
		.get(stockTransfer.listAll);

	app.route('/stockTransferbyNumber/:number')
		.get(stockTransfer.getByNumber);

	app.route('/stockTransfer/:id')
		.delete(stockTransfer.delete);
		
	app.route('/stockTransferReport/:fromdate/:todate')
		.get(stockTransfer.getReport);
		
	app.route('/stockTransferReportwithValue/:fromdate/:todate')
		.get(stockTransfer.getReportwithValue);

	app.route('/stockTransfer/:searchType/:keyword')
		.get(stockTransfer.search);
		
};