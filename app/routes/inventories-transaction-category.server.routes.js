'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryTransactionCategory = require('../../app/controllers/inventories-transaction-category.server.controller');

	app.route('/inventory-transaction-category')
		.get(inventoryTransactionCategory.list)
		.post(inventoryTransactionCategory.create);

	app.route('/inventory-transaction-category/:transactionCategoryId')
		.get(inventoryTransactionCategory.read)
		.put(inventoryTransactionCategory.update)
		.delete(inventoryTransactionCategory.delete);

	app.route('/inventory-transaction-category/:searchType/:keyword')
		.get(inventoryTransactionCategory.search);
};