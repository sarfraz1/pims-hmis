'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var doctor = require('../../app/controllers/doctor.server.controller');

	// Inventories Routes
	app.route('/doctor')
		.get(doctor.list)
		.post(doctor.create);

		app.route('/doctornames')
			.get(doctor.get_doctor_names);

	app.route('/doctor/:key')
		.get(doctor.read)
		.put(doctor.update)
		.delete(doctor.delete);

	app.route('/doctorbyname/:username')
		.get(doctor.readbyname);

	app.route('/getDoctor/:keyword')
		.get(doctor.search);
};
