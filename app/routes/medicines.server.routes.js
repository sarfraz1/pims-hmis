'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var medicines = require('../../app/controllers/medicines.server.controller');

	app.route('/medicines')
		.get(medicines.list)
		.post(medicines.create);

	app.route('/medicines/:medicineId')
		.get(medicines.read)
		.put(medicines.update)
		.delete(medicines.delete);

	app.route('/medicines/:searchType/:keyword')
		.get(medicines.search);
};