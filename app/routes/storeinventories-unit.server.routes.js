'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryUnit = require('../../app/controllers/storeinventories-unit.server.controller');

	app.route('/storeinventory-unit')
		.get(inventoryUnit.list)
		.post(inventoryUnit.create);

	app.route('/storeinventory-unit/:unitId')
		.get(inventoryUnit.read)
		.put(inventoryUnit.update)
		.delete(inventoryUnit.delete);

	app.route('/storeinventory-unit/:searchType/:keyword')
		.get(inventoryUnit.search);
};