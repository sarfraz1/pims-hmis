'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventories = require('../../app/controllers/inventories.server.controller');

	// Inventories Routes
	app.route('/inventories')
		.get(inventories.list)
		.post(inventories.create);

	app.route('/getInventoryItemCode')
		.get(inventories.getItemCode);

	app.route('/getInventoryItemExpiryStatus/:code')
		.get(inventories.getItemExpiryStatus);

	app.route('/getInventory/:description')
		.get(inventories.getItemByDescription);

	app.route('/inventoryImage')
		.post(inventories.createImage);

	app.route('/getInventory/:searchType/:keyword')
		.get(inventories.getItem);

	app.route('/getInventoryByDescription/:description')
		.get(inventories.getItemByDescription);

	app.route('/inventories/:inventoryId')
		.get(inventories.read)
		.put(inventories.update)
		.delete(inventories.delete);

	app.route('/getInventoryDetailsByCode/:code/:storedesc/:store/:fromdate/:todate')
		.get(inventories.getItemLedger);
};
