'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var facility = require('../../app/controllers/ward.server.controller');

	// Facility Routes
	app.route('/ward')
		.post(facility.create)
		.get(facility.list);

		app.route('/updateWard')
			.post(facility.update);

	app.route('/wardlastNodes')
		.get(facility.lastNodes);

	app.route('/ward/:node')
		.get(facility.read)
		.delete(facility.removeNode);

		app.route('/removeWard')
		.post(facility.delete);


};
