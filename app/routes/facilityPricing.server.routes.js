'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var facilityPricing = require('../../app/controllers/facilityPricing.server.controller');

	// Facility Routes
	app.route('/facility-pricing')
		.get(facilityPricing.list)
		.post(facilityPricing.create);

	app.route('/facility-pricing/aggregatebycategory')
		.get(facilityPricing.aggregate_by_category)
		
	app.route('/facility-pricing/:category')
		.get(facilityPricing.read);
		
	app.route('/facility-pricing/:category/:service_name')
		.get(facilityPricing.getbynameandcategory);
	
	app.route('/facility-pricingsearch/:category/:service_name')
		.get(facilityPricing.searchbynameandcategory);
		
	app.route('/facility-pricing/:category/:service_name')
		.get(facilityPricing.getbynameandcategory);
		
	app.route('/facility-pricing/getbycategory/:category')
		.get(facilityPricing.getbycategory);
		
	app.route('/facility-pricing/:categoryName')
		.delete(facilityPricing.delete);

};
