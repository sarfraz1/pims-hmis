'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseOrder = require('../../app/controllers/storepurchaseOrder.server.controller');

	// Inventories Routes
	app.route('/storepurchaseOrder')
		.get(purchaseOrder.list)
		.post(purchaseOrder.create);

	app.route('/getIdStorePurchaseOrder')
		.get(purchaseOrder.getPurchaseOrderId);

	app.route('/storepurchaseOrder/:pageNo')
		.get(purchaseOrder.listAll);

	app.route('/storepurchaseOrder/:purchaseOrderId')
		.put(purchaseOrder.updateOrder);

	app.route('/deleteStorePurchaseOrder')
		.post(purchaseOrder.delete);

	app.route('/storepurchaseOrder/:searchType/:keyword')
		.get(purchaseOrder.search);
};