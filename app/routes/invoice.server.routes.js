'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var invoices = require('../../app/controllers/invoices.server.controller');

	app.route('/invoices')
		.get(invoices.list)
		.post(invoices.create);

	app.route('/invoicesFiltered/:salesmanName/:fromDate/:toDate/:fromTime/:toTime/:pageNumber')
		.get(invoices.listFiltered);
		
	app.route('/invoicesFiltered-Total/:salesmanName/:fromDate/:toDate/:fromTime/:toTime/:pageNumber')
		.get(invoices.listFilteredTotal);

	app.route('/invoices/:invoiceId')
		.delete(invoices.delete)
		.get(invoices.read);

	app.route('/invoices/update')
		.post(invoices.update);
		
	app.route('/invoices/updateorderlist')
		.post(invoices.updateorderlist);

	app.route('/invoices/updatepurchaseprice/:invoiceId')
		.post(invoices.updatepurchaseprice);

	app.route('/invoiceReport/:salesmanName/:fromDate/:toDate/:fromTime/:toTime/:pageNumber')
		.get(invoices.report);

	app.route('/invoiceReport/:salesmanName/:storeDesc/:fromDate/:toDate/:fromTime/:toTime/:pageNumber')
		.get(invoices.salesreport);

	app.route('/invoiceCostReport/:fromDate/:toDate')
		.get(invoices.costreport);

	app.route('/invoiceReportNarcotics/:salesmanName/:fromDate/:toDate/:fromTime/:toTime/:pageNumber')
		.get(invoices.reportNarcotics);

	app.route('/invoiceReciptReport/:salesmanName/:fromReceipt/:toReceipt')
		.get(invoices.receiptReport);

	app.route('/invoiceReportAmount/:salesmanName/:fromDate/:toDate/:fromTime/:toTime')
		.get(invoices.reportSalesAmount);
	
	app.route('/reportReceiptSalesAmount/:salesmanName/:fromReceipt/:toReceipt')
		.get(invoices.reportReceiptSalesAmount);

	app.route('/invoiceReceiptRange/:salesmanName/:fromReceipt/:toReceipt')
		.get(invoices.invoiceReceiptRange);
};