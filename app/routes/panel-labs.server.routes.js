'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var panelLab = require('../../app/controllers/panel-labs.server.controller');

	app.route('/panel-lab')
		.get(panelLab.list)
		.post(panelLab.create);

	app.route('/panel-lab/:panelLabID')
		.get(panelLab.read)
		.put(panelLab.update)
		.delete(panelLab.delete);
};