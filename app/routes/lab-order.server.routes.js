'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var labOrder = require('../../app/controllers/lab-order.server.controller');

	app.route('/lab-order')
		.get(labOrder.list)
		.post(labOrder.create);

	app.route('/lab-order/:labOrderId')
		.get(labOrder.read)
		.put(labOrder.update)
		.delete(labOrder.delete);
		
	app.route('/labimage')
		.post(labOrder.createImage);
		
	app.route('/labordersbyMRN/:mr_number')
		.get(labOrder.getbyMRN);

	app.route('/lab-order/:labOrderId/:description/:status')
		.put(labOrder.updateStatus);

	app.route('/lab-orders-date/:fromDate/:toDate')
		.get(labOrder.getByDate);

	app.route('/panelLabReport/:fromDate/:toDate/:panelLab')
		.get(labOrder.getPanelLabReport);

};