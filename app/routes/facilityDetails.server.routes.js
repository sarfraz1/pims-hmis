'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var facilityDetails = require('../../app/controllers/facilityDetails.server.controller');

	// Facility Routes
	app.route('/facility-details')
		.get(facilityDetails.list)
		.post(facilityDetails.create);
	
	app.route('/facility-details/:description/:category')
		.get(facilityDetails.read);

};
