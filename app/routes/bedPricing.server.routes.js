'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var bedPricing = require('../../app/controllers/bedPricing.server.controller');

	// Bed Routes
	app.route('/bed-pricing')
		.get(bedPricing.list)
		.post(bedPricing.create);
		
	app.route('/bed-pricing/:category')
		.get(bedPricing.read)
		.put(bedPricing.update);
		
	app.route('/bed-pricingbycategory/:category')
		.get(bedPricing.getbedsbycategory);
		
	app.route('/bed-pricing/:category/:service_name')
		.get(bedPricing.getbynameandcategory);
		
	app.route('/bed-pricing/:categoryName')
		.delete(bedPricing.delete);

};
