'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var prescription = require('../../app/controllers/prescription.server.controller');

	app.route('/prescription')
		.get(prescription.list)
		.post(prescription.create);

	app.route('/prescription/:doctorName')
		.get(prescription.list);

	app.route('/patientPrescriptions/:mr_number')
		.get(prescription.patientHistory);
		
	app.route('/patientPrescriptionbyappointmentId/:appointment_id')
		.get(prescription.getPrescriptionbyappointmentId);

	app.route('/prescriptionsForPharmacy')
		.get(prescription.listNotHandled);

	app.route('/prescription/pharmacy/:prescriptionid')
		.put(prescription.handledByPharmacy);

	app.route('/prescription/complaint/:doctorUsername/:complaint')
		.get(prescription.getComplaints);

	app.route('/prescription/complaint/:doctorUsername')
		.get(prescription.getComplaints);
		
	app.route('/prescription/procedure/:doctorUsername/:procedure')
		.get(prescription.getProcedures);

	app.route('/prescription/procedure/:doctorUsername')
		.get(prescription.getProcedures);


	app.route('/prescription/diagnosis/:doctorUsername/:diagnosis')
		.get(prescription.getDiagnosis);

	app.route('/prescription/diagnosis/:doctorUsername')
		.get(prescription.getDiagnosis);


	app.route('/prescription/advice/:doctorUsername/:advice')
		.get(prescription.getAdvice);

	app.route('/prescription/advice/:doctorUsername')
		.get(prescription.getAdvice);
};