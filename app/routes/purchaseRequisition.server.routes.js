'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseRequisition = require('../../app/controllers/purchaseRequisition.server.controller');

	// Inventories Routes
	app.route('/purchaseRequisition')
		.get(purchaseRequisition.list)
		.post(purchaseRequisition.create);

	app.route('/purchaseRequisition/:pageNo')
		.get(purchaseRequisition.listAll);

	app.route('/purchaseRequisitionUpdate')
		.post(purchaseRequisition.update);

	app.route('/purchaseRequisition/:searchType/:keyword')
		.get(purchaseRequisition.search);

	app.route('/purchaseRequisition/:purchaseRequisitionId')
		.delete(purchaseRequisition.delete);
};
