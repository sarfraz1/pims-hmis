'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryStore = require('../../app/controllers/storeinventories-store.server.controller');

	app.route('/storeinventory-store')
		.get(inventoryStore.list)
		.post(inventoryStore.create);

	app.route('/storeinventory-store/:storeId')
		.get(inventoryStore.read)
		.put(inventoryStore.update)
		.delete(inventoryStore.delete);

	app.route('/storeinventory-store/description/:keyword')
		.get(inventoryStore.search);
};