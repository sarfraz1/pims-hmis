'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var stockTransfer = require('../../app/controllers/storestockTransfer.server.controller');

	// Inventories Routes
	app.route('/storestockTransfer')
		.get(stockTransfer.list)
		.post(stockTransfer.create)
		.put(stockTransfer.update);

	app.route('/storestockTransfer/:pageNo')
		.get(stockTransfer.listAll);

	app.route('/storestockTransferbyNumber/:number')
		.get(stockTransfer.getByNumber);

	app.route('/storestockTransfer/:id')
		.delete(stockTransfer.delete);

	app.route('/storestockTransfer/:searchType/:keyword')
		.get(stockTransfer.search);
		
};