'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var radiologyTest = require('../../app/controllers/radiology-tests.server.controller');

	app.route('/radiology-test')
		.get(radiologyTest.list)
		.post(radiologyTest.create);

	app.route('/radiology-test/:labTestId')
		.get(radiologyTest.read)
		.put(radiologyTest.update)
		.delete(radiologyTest.delete);

	app.route('/radiology-test/:testDescription')
		.get(radiologyTest.getByDescAndCategory);

};