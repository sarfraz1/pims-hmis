'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var tokenNumber = require('../../app/controllers/tokenNumber.server.controller');

	app.route('/tokenNumber')
		.post(tokenNumber.create)
		.get(tokenNumber.list);

	app.route('/tokenNumber/:service_name')
		.get(tokenNumber.read)
		.put(tokenNumber.update)
		.delete(tokenNumber.delete);
};