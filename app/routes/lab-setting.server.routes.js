'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var labSetting = require('../../app/controllers/lab-setting.server.controller');

	// Inventories Routes
	app.route('/lab-setting')
		.get(labSetting.read)
		.post(labSetting.create);

	app.route('/lab-print-image')
		.post(labSetting.createImage);
};
