'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var purchaseReturn = require('../../app/controllers/purchaseReturn.server.controller');

	app.route('/PurchaseReturnById/:PR_ID')
		.get(purchaseReturn.PurchaseReturnById);

	app.route('/UpdatePurchaseReturn')
		.post(purchaseReturn.UpdatePurchaseReturn);

	app.route('/DeletePurchaseReturn/:code')
		.get(purchaseReturn.DeletePurchaseReturn);
		
	// Inventories Routes
	app.route('/purchaseReturn')
		.get(purchaseReturn.list)
		.post(purchaseReturn.create);
};
