'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var packages = require('../../app/controllers/packages.server.controller');

	// Inventories Routes
	app.route('/package')
		.get(packages.list)
		.post(packages.create);

	app.route('/package/:key')
		.get(packages.read)
		.put(packages.update)
		.delete(packages.delete);

	app.route('/getPackage/:keyword')
		.get(packages.search);

		app.route('/packagedetail/:keyword')
			.get(packages.getdetail);

		app.route('/getPackageNames')
			.get(packages.get_package_names);

};
