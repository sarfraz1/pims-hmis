'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryCustomerSupplier = require('../../app/controllers/storeinventories-customer-supplier.server.controller');

	app.route('/storeinventory-customer-supplier')
		.get(inventoryCustomerSupplier.list)
		.post(inventoryCustomerSupplier.create);

	app.route('/storeinventory-customer-supplier/:customerSupplierId')
		.get(inventoryCustomerSupplier.read)
		.put(inventoryCustomerSupplier.update)
		.delete(inventoryCustomerSupplier.delete);

	app.route('/storeinventory-customer-supplier/:searchType/:keyword')
		.get(inventoryCustomerSupplier.search);
		
	app.route('/storeinventory-getmanufacturers')
		.get(inventoryCustomerSupplier.getmanufacturers);
		
};