'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var labTest = require('../../app/controllers/lab-tests.server.controller');

	app.route('/lab-test')
		.get(labTest.list)
		.post(labTest.create);

	app.route('/lab-test/:labTestId')
		.get(labTest.read)
		.put(labTest.update)
		.delete(labTest.delete);

	app.route('/lab-test/:testDescription/:testCategory')
		.get(labTest.getByDescAndCategory);

};