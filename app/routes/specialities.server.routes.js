'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var speciality = require('../../app/controllers/specialities.server.controller');

	app.route('/specialities')
		.get(speciality.list)
		.post(speciality.create);

	app.route('/specialities/:specialityID')
		.get(speciality.read)
		.put(speciality.update)
		.delete(speciality.delete);
};