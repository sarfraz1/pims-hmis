'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var nursenotes = require('../../app/controllers/nursenotes.server.controller');

	app.route('/nursenotes')
		.get(nursenotes.list)
		.post(nursenotes.create);

	app.route('/nursenotes/:mr_number')
		.get(nursenotes.getbyMrNo);
		
	app.route('/getAppointmentNurseNotes/:appintment_id')
		.get(nursenotes.getAppointmentNurseNotes)
		.put(nursenotes.updateAppointmentNurseNotes);
		
	/*app.route('/msgnurseforvitals')
		.post(nursenotes.sendmsgforvitals);
		
	app.route('/msgnurseformedication')
		.post(nursenotes.sendmsgformedication);

	app.route('/msgdoctorformedication')
		.post(nursenotes.sendmsgformedicationdoctor);*/
		
};