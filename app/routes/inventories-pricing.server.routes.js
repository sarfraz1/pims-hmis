'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryPricing = require('../../app/controllers/inventories-pricing.server.controller');

	app.route('/inventory-pricing')
		.get(inventoryPricing.list)
		.post(inventoryPricing.create);

	app.route('/inventory-pricing/:inventoryCode')
		.get(inventoryPricing.read)
		.put(inventoryPricing.create)
		.delete(inventoryPricing.delete);
		
	app.route('/inventory-pricing-pos/:inventoryCode')
		.get(inventoryPricing.readpos);

	app.route('/inventory-pricing-supplier/:supplierId')
		.get(inventoryPricing.listSupplierInventory);
		
	app.route('/inventory-pricing-supplier-stock/:supplierId')
		.get(inventoryPricing.listSupplierInventorywithStock);
		
};