'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventories = require('../../app/controllers/storeinventories.server.controller');

	// Store Inventories Routes
	app.route('/storeinventories')
		.get(inventories.list)
		.post(inventories.create);

	app.route('/getStoreInventoryItemCode')
		.get(inventories.getItemCode);

	app.route('/getStoreInventoryItemExpiryStatus/:code')
		.get(inventories.getItemExpiryStatus);

	app.route('/getStoreInventory/:description')
		.get(inventories.getItemByDescription);

	app.route('/storeinventoryImage')
		.post(inventories.createImage);

	app.route('/getStoreInventory/:searchType/:keyword')
		.get(inventories.getItem);

	app.route('/getStoreInventoryByDescription/:description')
		.get(inventories.getItemByDescription);

	app.route('/storeinventories/:inventoryId')
		.get(inventories.read)
		.put(inventories.update)
		.delete(inventories.delete);

	app.route('/getStoreInventoryDetailsByCode/:code')
		.get(inventories.getDetails);
};
