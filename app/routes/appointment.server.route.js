'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var appointment = require('../../app/controllers/appointment.server.controller');

	// Inventories Routes
	app.route('/appointment')
		.get(appointment.list)
		.post(appointment.create);

	app.route('/appointment/:appointment_id')
		.get(appointment.read)
		.put(appointment.update)
		.delete(appointment.delete);
		
	app.route('/createwalkinappointment')
		.post(appointment.create_walkin_appointment);
	
	app.route('/updateappointmentonregister')
		.post(appointment.update_on_register);
		
	app.route('/updateappointmentvitals')
		.post(appointment.update_vitals_status);
	
	app.route('/updateappoinmentstatus')
		.post(appointment.update_appointment_status);
	
	app.route('/viewAppointments/:doctor_id/:date')
		.get(appointment.viewappointments);
		
	app.route('/viewAppointmentsbyUsername/:username/:date')
		.get(appointment.viewappointmentsbyusername);
		
	app.route('/getDoctorslots/:doctor_id/:date')
		.get(appointment.getslots);
	
	app.route('/getAppointment/:keyword')
		.get(appointment.search);
};
