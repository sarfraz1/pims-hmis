'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var hospital = require('../../app/controllers/hospitals.server.controller');

	app.route('/hospital')
		.get(hospital.list)
		.post(hospital.create);

	app.route('/hospital/:hospitalId')
		.get(hospital.read)
		.put(hospital.update)
		.delete(hospital.delete);
};