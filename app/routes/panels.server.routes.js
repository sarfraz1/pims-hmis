'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var panel = require('../../app/controllers/panels.server.controller');

	app.route('/panel')
		.get(panel.list)
		.post(panel.create);

	app.route('/panel/:panelID')
		.get(panel.read)
		.put(panel.update)
		.delete(panel.delete);

	app.route('/panel/:searchType/:keyword')
		.get(panel.search);
	
	app.route('/getPanelList')
		.get(panel.getPaneList);
	
	app.route('/getDiscount/:panelID/:category/:description')
		.get(panel.getDiscount);
};