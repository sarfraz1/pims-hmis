'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var radiologyResults = require('../../app/controllers/radiology-results.server.controller');

	app.route('/radiology-result')
		.get(radiologyResults.list)
		.post(radiologyResults.create);

	app.route('/radiology-result/:labResultsId')
		.get(radiologyResults.read)
		.put(radiologyResults.update)
		.delete(radiologyResults.delete);
	
	app.route('/getradiologybyMRN/:patientMRN')
		.get(radiologyResults.getRadiologybyMRN);
		
	app.route('/radiology-result/:patientMRN/:testDescription')
		.get(radiologyResults.getPatientTest);

	app.route('/radiology-resultbyorderid/:orderId/:testDescription')
		.get(radiologyResults.getPatientTestbyOrderId);		
		
	app.route('/radiology-test-results/:patientMRN/:testDescription')
		.get(radiologyResults.getPatientTests);

};