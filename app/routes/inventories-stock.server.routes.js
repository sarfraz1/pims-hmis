'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var inventoryStock = require('../../app/controllers/inventories-stock.server.controller');

	app.route('/inventory-stock')
		.get(inventoryStock.list)
		.post(inventoryStock.create);

	app.route('/inventory-stock/:stockId')
		.get(inventoryStock.read)
		.put(inventoryStock.update)
		.delete(inventoryStock.delete);
		
	app.route('/inventory-stockname/:code/:name')
		.get(inventoryStock.updatename)

	app.route('/inventory-stockreport')
		.get(inventoryStock.stockreport);
	
	app.route('/inventory-stockaverageprice/:supplier')
		.get(inventoryStock.stockaverageprice);

	app.route('/inventory-lowstockreport')
		.get(inventoryStock.lowstockreport);

	app.route('/inventory-stockbyCode/:code')
		.get(inventoryStock.readByCode)		
		
	app.route('/inventory-stock/adjust/:stockId')
		.put(inventoryStock.adjust);

	app.route('/inventory-store-stock/adjust/:stockId')
		.put(inventoryStock.adjustInStore);

	app.route('/inventory-stock/adjustReturn/:stockId')
		.put(inventoryStock.adjustReturn);

	app.route('/inventory-stock/check/:stockId')
		.get(inventoryStock.check);

	app.route('/inventory-stock/:searchType/:keyword')
		.get(inventoryStock.search);
		
	app.route('/getInventoryStock/description/:keyword')
		.get(inventoryStock.keywordSearch);
		
	app.route('/getInventorybyStore/:keyword/:store')
		.get(inventoryStock.getbyStore);

	app.route('/getInventorybyStoreName/:keyword/:store')
		.get(inventoryStock.getbyStoreName);


	app.route('/inventory-supplier-stock-list/:supplierId/:store')
		.get(inventoryStock.supplierBasedStock);


};