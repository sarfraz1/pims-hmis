'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Packages = mongoose.model('Packages'),
	FacilityPricing = mongoose.model('FacilityPricing'),
	_ = require('lodash');


/**
 * Add new package
 */
exports.create = function(req, res) {

	req.checkBody('name', 'Name is required').notEmpty();
	var errors = req.validationErrors();

	if(errors) {
		return res.status(500).send(errors);
	} else {

		var packages = new Packages(req.body);


		packages.save(function(err, pkg) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err),
					code : err.code
				});
			} else {

				var service = {
						'description' : pkg.name,
						'price' : 0,
						'category' : 'Hospital-IPD-Packages'
				};

			/*	FacilityPricing.findOneAndUpdate( {'description': service.description,'category': service.category}, service, {upsert: true}).exec(function(err, facility_pricing) {
					if (err) {
						console.log(err);
					} else {
						console.log('facility pricing created');
					}
				});*/
				res.status(200).send(pkg);
			}
		});
	}
};

/**
 * Retrieve packages based on id
 */
exports.get_package_names = function(req, res) {
	Packages.find({}, {name:1, _id:1}).exec(function(err,packages) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(packages);
		}
	});
};

/**
 * Retrieve packages based on id
 */
exports.read = function(req, res) {
	var query;
	if(req.params.key) {
		query = Packages.find({_id: req.params.key}, {_id:0});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, pkg){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(pkg);
		}
	});
};


exports.getdetail = function(req, res) {
	var query;
	if(req.params.keyword) {
		query = Packages.findOne({name: req.params.keyword});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, pkg){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(pkg);
		}
	});
};



/**
 * Update a package
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('key', 'Id is required').notEmpty();
	var errors = req.validationErrors();

	var item = req.body;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Packages.findOneAndUpdate({'_id': req.params.key}, item, {upsert: true}, function(err, pkg) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(pkg);
			}
		});
	}
};

/**
 * Delete a Package
 */
exports.delete = function(req, res) {
	Packages.remove({'_id':req.params.key} , function(err,pkg) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(pkg);
		}
	});
};

/**
 * List of Packages
 */
exports.list = function(req, res) {
	Packages.find().exec(function(err, pkg) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(pkg);
		}
	});
};


var keywordSearch = function(req, res) {
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Packages.find({'name':{$regex : regex}},'-__v').limit(10).exec(function(err, pkg) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).json(pkg);
		}
	});
};

exports.search = function(req, res) {
	keywordSearch(req, res);
};
