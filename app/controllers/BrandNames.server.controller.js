'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Medicine = mongoose.model('Medicine'),
	BrandName = mongoose.model('BrandName'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var brandname = new BrandName(req.body);
	brandname.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(brandname);
		}
	});
};

/**
 * Show the current Medicine
 */
exports.read = function(req, res) {
	BrandName.findOne({'_id': req.params.medicineId}).exec(function(err, medicine) {
		if (medicine) {
			res.json(medicine);
		} else {
			err = 'Brand not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Medicine
 */
exports.update = function(req, res) {
	BrandName.findOneAndUpdate({'_id': req.params.medicineId}, req.body).exec(function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * Delete a Medicine
 */
exports.delete = function(req, res) {
	BrandName.remove({'_id': req.params.medicineId}, function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * List of Medicines
 */
exports.list = function(req, res) {
	BrandName.find().exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(medicines);
		}
	});
};

var findBrandNameExists = function(index,brandname,callback){
	
	if(brandname.tradeName[index]!==undefined){

		BrandName.findOne({'tradeName': brandname.tradeName[index].description}).exec(function(err, medicine) {
			if (medicine) {
				console.log(brandname.formula);
				console.log(medicine);
				
				if(medicine.formula.indexOf(brandname.formula))
					medicine.formula = medicine.formula + '+' + brandname.formula;
				medicine.save(function(err){
					if(err){
						callback();
					}
					else {
						index++;
						findBrandNameExists(index,brandname,callback);
					}

				});

			} else {
				
				var brandnameNew = new BrandName({'tradeName': brandname.tradeName[index].description,'formula': brandname.formula });
				brandnameNew.save(function (err) {
					if (err) {
						callback();
					} else {
						index++;
						findBrandNameExists(index,brandname,callback);
					}
				});
			}
		});
		
	}else {
		callback();
	}
};

var findandAddBrandName = function(index,medicines,callback){
	if(medicines[index]!==undefined){

		findBrandNameExists(0,medicines[index],function(){
			index++;
			findandAddBrandName(index,medicines,callback);
		});
		
	}else {
		callback();
	}
};

var importfrommedicine = function(req, res) {
	Medicine.find().exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
  
			findandAddBrandName(0,medicines,function(){
				res.json('sucess');
			});
			
		}
	});
};

var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	BrandName.find({'tradeName':{$regex : regex}},'-__v').exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

	
			var dataToSort = [];
            for(var i = 0 ; i < medicines.length; i++){
              var temp = medicines[i];
              temp.rank = medicines[i].tradeName.toLowerCase().indexOf(keyword.toLowerCase());
              dataToSort.push(temp);
            }
            dataToSort.sort(function(a, b) {
              return Number(a.rank) - Number(b.rank);
            });
            
            res.json(dataToSort.slice(0,10));
		}
	});
};

exports.search = function(req, res) {
	searchByName(req, res);
};

exports.importfrommedicine = function(req, res) {
	//searchByName(req, res);
	importfrommedicine(req, res);

};


