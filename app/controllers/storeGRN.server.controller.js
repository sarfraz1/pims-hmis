'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	GRN = mongoose.model('GRN'),
	InventoryStock = mongoose.model('StoreInventoryStock'),
	Inventory = mongoose.model('StoreInventory'),
	InventoryPricing = mongoose.model('StoreInventoryPricing'),
	purchaseOrder = mongoose.model('StorepurchaseOrder'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash');

/**
 * Create a Inventory
 */

var dateConverter = function(dateinput){
	try{
		var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
		utcDate = utcDate.toUTCString();
		return utcDate;
	}
	catch(error){
		return dateinput;
	}
};

var checkData = function(itemsList,i,callback){
	if(itemsList[i]!==undefined){
		purchaseOrder.findOne({'purchaseOrderNumber':itemsList[i].purchaseOrderNumber}).exec(function(err, purchaseOrderObj) {
			var errorProduced = false;
		
			if(purchaseOrderObj!==null){
				for(var j=0;j<purchaseOrderObj.itemsList.length;j++){
					if(purchaseOrderObj.itemsList[j].code===itemsList[i].code){
						purchaseOrderObj.itemsList[j].handledQty = purchaseOrderObj.itemsList[j].handledQty + itemsList[i].quantity;
						if(purchaseOrderObj.itemsList[j].quantity>=itemsList[i].quantity)
							purchaseOrderObj.itemsList[j].quantity = purchaseOrderObj.itemsList[j].quantity - itemsList[i].quantity;
						else{
							callback('Items Quantity is Greator than Purchase Order');
							errorProduced = true;
						}
					}
					if(purchaseOrderObj.itemsList[j].quantity === 0){
						purchaseOrderObj.itemsList[j].completed = true;
					}
				}
				
			}
			if(errorProduced === false){
				i++;
				checkData(itemsList,i,callback);
			}
		});
		

	}else {
		callback(null);
	}

};

var updatePurchaseOrder = function(itemsList,i){
	if(itemsList[i]!==undefined){
		purchaseOrder.findOne({'purchaseOrderNumber':itemsList[i].purchaseOrderNumber}).exec(function(err, purchaseOrderObj) {
			if(purchaseOrderObj!==null){
				for(var j=0;j<purchaseOrderObj.itemsList.length;j++){
					if(purchaseOrderObj.itemsList[j].code===itemsList[i].code){
						purchaseOrderObj.itemsList[j].handledQty = purchaseOrderObj.itemsList[j].handledQty + itemsList[i].quantity;
						if(purchaseOrderObj.itemsList[j].quantity>=itemsList[i].quantity)
							purchaseOrderObj.itemsList[j].quantity = purchaseOrderObj.itemsList[j].quantity - itemsList[i].quantity;
					}
					if(purchaseOrderObj.itemsList[j].quantity === 0){
						purchaseOrderObj.itemsList[j].completed = true;
					} else (purchaseOrderObj.itemsList[j].quantity>0)
						purchaseOrderObj.itemsList[j].completed = false;
				}
				purchaseOrderObj.save(function(err,data) {
					i++;
					updatePurchaseOrder(itemsList,i);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};


var updateInventoryStock = function(itemsList,i,grnNumber,storeId,storeDescription,supplierId){
	if(itemsList[i]!==undefined){
		
		Inventory.findOne({'code':itemsList[i].code}).exec(function(err, inventory) {
			if (err) {
				console.log(err);
			} else {
				inventory.grn_created = true;
				inventory.save(function(err,saved){
					if(err){
						console.log(err);
					}
				});
				var newInventoryData = {
					'inventoryCode' : itemsList[i].code,
					'pricingType' : 'Sale',
					'discountType' : 'percent',
					'discountAmount' : 0,
					'sellingPrice' : itemsList[i].sellingPrice,
					'purchasingPrice' : itemsList[i].rate,
					'retailPrice' : itemsList[i].sellingPrice,
					'dateTransaction' : new Date(),
					'effectiveDate' : dateConverter(new Date())
				};
				if(supplierId){
					newInventoryData.supplierId = supplierId;
				}
				InventoryPricing.findOneAndUpdate({'inventoryCode': itemsList[i].code,'sellingPrice':itemsList[i].sellingPrice,'purchasingPrice':itemsList[i].rate},newInventoryData, {upsert: true,sort: { '_id': -1 }}).exec(function(err, inventoryPricing) {
					if (err) {
						console.log(err);
					}
					
					var query = {
						'id': itemsList[i].code
					};
					var update = {
						'description' :  itemsList[i].description,
						$push: {
							'batch': {
								'batchID': itemsList[i].batchID, 
								'quantity': inventory.storage_to_selling*itemsList[i].quantity,
								//'quantity': itemsList[i].quantity,
								'purchaseRate': itemsList[i].rate/inventory.storage_to_selling,
								'expiryDate' : itemsList[i].expiryDate,
								'grnNumber': grnNumber,
								'storeId' : storeId,
								'storeDescription' : storeDescription

							}
						}
					};

					InventoryStock.findOneAndUpdate(query, update, {upsert: true}).exec(function (err, response) {
						if (err) {
							console.log(err);
						} else {
							//itemsList[i].quantity = itemsList[i].quantity/inventory.storage_to_selling;
					
							i++;
							updateInventoryStock(itemsList,i,grnNumber,storeId,storeDescription,supplierId);
							//console.log(response);
						}
					});
				});
			}
		});
	}
	else {
		GRN.findOne({'GRNNumber':grnNumber}).exec(function(err, grnUpdateObj) {
			if(grnUpdateObj){
				grnUpdateObj.itemsList = itemsList;
				grnUpdateObj.save(function(err,saved){
					if(err){
						console.log(err);
					}
				});
			}
			else {
				console.log(err);
			}
		});
		
	}
};


exports.create = function(req, res) {
    var grn = new GRN(req.body);
    checkData(req.body.itemsList,0,function(err){
    	if(err){
			return res.status(400).send({
				message: err
			});
		}else{
		    updatePurchaseOrder(req.body.itemsList,0);
			
			GRN.findOne().sort({_id:-1}).exec(function(err, grnObj) {
				if(req.body.transcationCategory==='Default'){
					if(grnObj){
						var code = Number(grnObj.GRNNumber)+1;
						code = String(code);
						
						grn.GRNNumber = code;
					} else if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} 
					else {
						grn.GRNNumber = '10000001';
					}

					var grnNumber = grn.GRNNumber;
					grn.save(function(err,data) {
						if (err) {
							console.log(err);
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							res.json(data);
							updateInventoryStock(req.body.itemsList,0,grnNumber,req.body.storeId,req.body.storeDescription, req.body.supplierId);
						}
					});
				}
				else {
					// Code for generating Number on category Basis with 
				}
			});   
		}
	}); 
    
};

/**
 * Method to filter inventory stocks against GRNs that have not been processed
 */
var filterStock = function(grn, req, res) {
	InventoryStock.aggregate([
	{ "$match": {'batch.grnNumber': grn.GRNNumber}},
	{
	    $lookup: {
            from: "inventories",
            localField: "id",
            foreignField: "code",
            as: "aggre"
        }
	}]).exec(function(err, stocks) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(stocks.length>0){
			var data = [];
			//for (var i = 0; i < grns.length; i++) {
				var processed = false;
				for (var j = 0; j < grn.itemsList.length; j++) {
					var stockIndex = stocks.map(function(stock) { 
						return stock.id; }).indexOf(grn.itemsList[j].code);
					//console.log(stockIndex);
					if(stockIndex !== -1) {
						var batch = stocks[stockIndex].batch;
						var inventoryObj = stocks[stockIndex].aggre[0];
						var itemIndex = -1;
						if(batch) {
							itemIndex = batch.map(function(item) { return item.grnNumber; }).indexOf(grn.GRNNumber);
						}
						if(inventoryObj) {
							if (itemIndex !== -1 && batch[itemIndex].quantity/inventoryObj.storage_to_selling !== grn.itemsList[j].quantity) {
								console.log('setting grn processed true');
								grn.processed = true;
								break;
							}
						}
					}
				}
				//if(!processed) {
					//data.push(grns[i]);
				/*} else {
					var obj = grns[i];
					//obj.processed = true;
					var o = _.extend({'processed': true}, obj);
					console.log(o);
					data.push(o);
				})*/
		//	}
		console.log(grn);
			res.json(grn);
		}
	});
};

/**
 * List GRNs
 */

exports.list = function(req, res) { 
	GRN.find({},{GRNNumber:1,supplierDescription:1,supplierChalanNumber:1,date:1,remarks:1}).lean().exec(function(err, grns) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//var grnarray = new Array();
			//grnarray = grns;
			//filterStock(grnarray, req, res);
			res.json(grns);
		}
	});
};

exports.listAll = function(req, res) { 
	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = GRN.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

/**
 * Update GRN
 */
var updateInventoryStockByGRN = function(itemsList,i,grnNumber,storeId,storeDescription,supplierId) {
	if(itemsList[i]!==undefined) {
		Inventory.findOne({'code':itemsList[i].code}).exec(function(err, inventory) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var newInventoryData = {
					'inventoryCode' : itemsList[i].code,
					'pricingType' : 'Sale',
					'discountType' : 'percent',
					'discountAmount' : 0,
					'sellingPrice' : itemsList[i].sellingPrice,
					'purchasingPrice' : itemsList[i].rate,
					'retailPrice' : itemsList[i].sellingPrice,
					'dateTransaction' : new Date(),
					'effectiveDate' : new Date(),
					'supplierId' : supplierId
				};
				InventoryPricing.findOneAndUpdate({'inventoryCode': itemsList[i].code,'sellingPrice':itemsList[i].sellingPrice,'purchasingPrice':itemsList[i].rate},newInventoryData, {upsert: true,sort: { '_id': -1 }}).exec(function(err, inventoryPricing) {
					if (err) {
						console.log(err);
					}

					InventoryStock.findOne({'id': itemsList[i].code}).exec(function (err, response) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							if (response === null) {
								
							} else {
								var exists = false;
								for (var j = 0; j < response.batch.length; j++) {
									if (response.batch[j].grnNumber === grnNumber) {
										exists = true;
										response.batch[j].batchID = itemsList[i].batchID;
										response.batch[j].quantity = response.batch[j].quantity + (inventory.storage_to_selling * itemsList[i].quantity);
										//response.batch[j].quantity = response.batch[j].quantity +  itemsList[i].quantity;
										response.batch[j].purchaseRate = inventory.storage_to_selling/itemsList[i].rate;
										response.batch[j].expiryDate = itemsList[i].expiryDate;
										response.batch[j].storeId = storeId;
										response.batch[j].storeDescription = storeDescription;
									}
								}
								response.save(function(err, data) {
									if (err) {
										console.log(err);
									} else {
										i++
										updateInventoryStockByGRN(itemsList, i, grnNumber,storeId,storeDescription,supplierId);
									}
								});
							}
						}
					});
				});
			}
		});		
	}
};

var deleteInventoryStockByGRN = function(stockToDelete, i, grnNumber) {
	if (stockToDelete[i] !== undefined) {
		InventoryStock.findOne({'id': stockToDelete[i].code}).exec(function (err, response) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				for (var j = 0; j < response.batch.length; j++) {
					if (response.batch[j].grnNumber === grnNumber) {
						response.batch.splice(j, 1);
					}
				}
				response.save(function(err, data) {
					if (err) {
						console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						i++
						deleteInventoryStockByGRN(stockToDelete, i, grnNumber);
					}
				});
			}
		});
	}
};

var itemslistUpdatedInfo = function(updatedlist,savedList){
	var finalList = [];
	for(var i =0;i<updatedlist.length;i++){
		for(var j=0;j<savedList.length;j++){
			if(updatedlist[i].code === savedList[j].code){
				updatedlist[i].quantity = updatedlist[i].quantity - savedList[j].quantity;
				
			}
		}
	};
	for(var i =0;i<savedList.length;i++){
		var found = false;
		for(var j=0;j<updatedlist.length;j++){
			if(updatedlist[j].code === savedList[i].code){
				found = true;
			}
		}
		if(found===false){
			savedList[i].quantity = - savedList[i].quantity;
			updatedlist.push(savedList[i]);

		}
	};
	//console.log(updatedlist);
	return updatedlist;
};

exports.update = function(req, res) {
	var newList = req.body.itemsList;
	var GRNData = req.body;
    GRN.findOne({'GRNNumber':req.body.GRNNumber}).exec(function(err, data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var dumyData = JSON.parse(JSON.stringify(req.body.itemsList));
			var tempList = itemslistUpdatedInfo(dumyData,data.itemsList);
			
			checkData(tempList,0,function(err){
		    	if(err){
					return res.status(400).send({
						message: err
					});
				}else{
					updatePurchaseOrder(tempList,0);
				
					GRN.findOneAndUpdate({'GRNNumber':req.body.GRNNumber},GRNData).exec(function(err,grnSaveResponse){
						if(err){
							console.log(err);
						}
						else {
							var originalList = data.itemsList;
							var stockToDelete = [];
							var deleted = true;
							for (var i = 0; i < originalList.length; i++) {
								deleted = true;
								for (var j = 0; j < newList.length; j++) {
									if (originalList[i].code === newList[j].code) {
										deleted = false;
										break;
									}
								}
								if (deleted === true) {
									stockToDelete.push({'code': originalList[i].code});
								}
							}
							if (stockToDelete.length > 0) {
								deleteInventoryStockByGRN(stockToDelete, 0, req.body.GRNNumber);
							}
							res.json(grnSaveResponse);
							updateInventoryStockByGRN(tempList, 0, req.body.GRNNumber,req.body.storeId,req.body.storeDescription,req.body.supplierId);
						}
					});
						
				}
			});
		}
	
	});
	
};

/**
 * Get GRN by Number
 */
exports.getByNumber = function(req, res) {
	GRN.findOne({'GRNNumber': req.params.number}).lean().exec(function (err, grn) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
		//	res.json(grn);
			filterStock(grn, req, res)
		}
	});
};


/**
 * Delete GRN
 */
exports.delete = function(req, res) {
	GRN.findOne({'_id': req.params.GRNId}).exec(function (err, response) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var itemsList = response.itemsList;
			var grnNumber = response.GRNNumber;
			GRN.remove({'_id': req.params.GRNId}, function(err, grnObj) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var stockToDelete = [];
					for (var i = 0; i < itemsList.length; i++) {
						stockToDelete.push({'code': itemsList[i].code});
					}
					deleteInventoryStockByGRN(stockToDelete, 0, grnNumber);
					res.json(grnObj);
				}
			});
		}
	});
};

/**
 * Retrieve array of Purchase Orders
 */
var searchByNumber = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	GRN.find({ $or: [{'GRNNumber':{$regex : regex}}, {'supplierDescription':{$regex : regex}}, {'supplierChalanNumber':{$regex : regex}} ]},{GRNNumber:1,supplierDescription:1,supplierChalanNumber:1,date:1,remarks:1}).lean().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			/*var compare = function(a, b) {
				if (a.GRNNumber.indexOf(keyword) < b.GRNNumber.indexOf(keyword))
					return -1;
				if (a.GRNNumber.indexOf(keyword) > b.GRNNumber.indexOf(keyword))
					return 1;
				return 0;
			};
			data.sort(compare);*/
			//filterStock(data, req, res);
			res.json(data);
		}
	});
};

var searchAll = function(req, res) {
	GRN.find().exec(function (err, data) {
		if (err) {
			return res.status(400).send({
				message:errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'GRNNumber') {
		searchByNumber(req, res);
	} else if (req.params.searchType === 'all') {
		searchAll(req, res);
	}
};

/**
 * Get supplier for medicine
 */
exports.getSupplierInventory = function(req, res) {
	GRN.findOne({'itemsList.code': req.params.medId}).sort({'date': -1}).exec(function(err, inventoryPricings) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(inventoryPricings) {
			res.jsonp(inventoryPricings.supplierDescription);
		} else {
			res.jsonp('');
		}
	})
};

/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
