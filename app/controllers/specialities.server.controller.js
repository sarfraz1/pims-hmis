'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Speciality = mongoose.model('Speciality'),
    _ = require('lodash');

/**
 * Create a Speciality
 */
exports.create = function(req, res) {
	req.checkBody('description', 'Description is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var speciality = new Speciality(req.body);
		speciality.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(speciality);
			}
		});
	}
};

/**
 * Show the current Speciality
 */
exports.read = function(req, res) {
	if (!req.params.specialityID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		Speciality.findOne({'_id': req.params.specialityID}).exec(function(err, speciality) {
		if (speciality) {
			res.json(speciality);
		} else {
			err = 'Speciality not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
	}
};

/**
 * Update a Speciality
 */
exports.update = function(req, res) {
	if (!req.params.specialityID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		req.checkBody('description', 'Description is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			Speciality.findOneAndUpdate({'_id': req.params.specialityID}, req.body).exec(function (err, speciality) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(speciality);
				}
			});
		}
	}
};

/**
 * Delete an Speciality
 */
exports.delete = function(req, res) {
	if (!req.params.specialityID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		Speciality.remove({'_id': req.params.specialityID}).exec(function (err, speciality) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(speciality);
			}
		});
	}
};

/**
 * List of Specialities
 */
exports.list = function(req, res) {
	Speciality.find().exec(function(err, specialities) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(specialities);
		}
	});
};