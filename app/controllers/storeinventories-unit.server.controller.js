'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryUnit = mongoose.model('StoreInventoryUnit'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories unit
 */
exports.create = function(req, res) {
	var inventoryUnit = new InventoryUnit(req.body);
	if (inventoryUnit.defaultOption === false) {
		inventoryUnit.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)	
				});
			} else {
				res.json(inventoryUnit);
			}
		});
	} else {
		InventoryUnit.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, unit) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				inventoryUnit.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(inventoryUnit);
					}
				});			
			}
		});
	}
};

/**
 * Show the current Inventories unit
 */
exports.read = function(req, res) {
	InventoryUnit.findOne({'_id': req.params.unitId}).exec(function(err, unit) {
		if (unit) {
			res.json(unit);
		} else {
			err = 'Inventory unit not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories unit
 */
exports.update = function(req, res) {
	if (req.params.defaultOption === false) {
		InventoryUnit.findOneAndUpdate({'_id': req.params.unitId}, req.body).exec(function(err, unit){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(unit);
			}
		});
	} else {
		InventoryUnit.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, unit) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				InventoryUnit.findOneAndUpdate({'_id': req.params.unitId}, req.body).exec(function(err, unit){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(unit);
					}
				});		
			}
		});
	}
};

/**
 * Delete an Inventories unit
 */
exports.delete = function(req, res) {
	InventoryUnit.remove({'_id': req.params.unitId}, function(err, unit) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(unit);
		}
	});
};

/**
 * List of Inventories units
 */
exports.list = function(req, res) {
	InventoryUnit.find().exec(function(err, units) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(units);
		}
	});
};

/**
 * Retrieve array of Inventories Units
 */
var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryUnit.find({'name':{$regex : regex}},'-__v').exec(function(err, units) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.name.indexOf(keyword) < b.name.indexOf(keyword))
					return -1;
				if (a.name.indexOf(keyword) > b.name.indexOf(keyword))
					return 1;
				return 0;
			};
			units.sort(compare);
			res.json(units);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'name') {
		searchByName(req, res);
	}
};