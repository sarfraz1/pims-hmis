'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryStock = mongoose.model('StoreInventoryStock'),
	InventoryPricing = mongoose.model('StoreInventoryPricing'),
	GRN = mongoose.model('StoreGRN'),
	Inventory = mongoose.model('StoreInventory'),
	multiparty = require('multiparty'),
	async = require('async'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories stock
 */
exports.create = function(req, res) {
	var inventoryStock = new InventoryStock(req.body);
	inventoryStock.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventoryStock);
		}
	});
};

/**
 * Show the current Inventories stock
 */
exports.read = function(req, res) {
	InventoryStock.findOne({'_id': req.params.stockId}).exec(function(err, stock) {
		if (stock) {
			res.json(stock);
		} else {
			err = 'Inventory stock not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update an Inventories stock
 */
exports.update = function(req, res) {
	InventoryStock.findOneAndUpdate({'_id': req.params.stockId}, req.body).exec(function(err, stock) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(stock);
		}
	});
};

/**
 * Delete an Inventories stock
 */
exports.delete = function(req, res) {
	InventoryStock.remove({'_id': req.params.stockId}, function(err, stock) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(stock);
		}
	});
};

/**
 * List of Inventories stocks
 */
exports.list = function(req, res) {
	InventoryStock.find().exec(function(err, stocks) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(stocks);
		}
	});
};


/**
 * Get Average purchase price for stock
 */
exports.stockaverageprice = function(req, res) {
	var supplier = req.params.supplier;
	var query = '';
	var datatosend = [];

	if(supplier !== 'All') {
		query = { "$match": { "supplierId": supplier}};
	} else {
		
		query = {};
	}
	
	InventoryStock.aggregate([
		{ "$lookup": {
            from: "inventories",
            localField: "id",
            foreignField: "code",
            as: "inven"
        }
		}
	]).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var count = 1;
			async.forEach(inventories, function(inventory, callback) {
				//var avgprice = getAveragePrice(inventory.batch, inventory.id);
				async.forEach(inventory.batch, function(item, callback1) {
					if(item.purchasePrice) {
						callback();
					} else {
						GRN.findOne({"GRNNumber": item.grnNumber}).exec(function(err, grn) {
							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
								callback1();
							} else if(grn) {
								for(var i=0;i<grn.itemsList.length;i++) {
									if(grn.itemsList[i].code == inventory.id) {
										item.purchasePrice = grn.itemsList[i].rate;
										break;
									}
								}
								callback1();
							} else {
								item.purchasePrice = 0;
								callback1();
							}
						});
					}
				}, function(err) {
					if(err) {
						console.log(err);
						return next(err);
					}
					else {
						//console.log(count);
						var price = 0;
						var total = 0;
						for(var j=0;j<inventory.batch.length;j++) {
							if(inventory.batch[j].quantity > 0) {
								price+=inventory.batch[j].purchasePrice/inventory.inven[0].storage_to_selling;
								total++;
							}
						}
						price = Math.round(price/total);
						inventory.purchasePrice = price;
						datatosend.push({'id': inventory.id,
								'averagePrice': price});
								
						callback();
					}
				});
				
			}, function(err) {

				res.json(datatosend);
			})
			
			
		}
	});
};

/**
 * Get Stock report with price
 */
exports.stockreport = function(req, res) {
	InventoryStock.aggregate([
   {
      $lookup:
         {
            from: "inventorypricings",
            localField: "id",
            foreignField: "inventoryCode",
            as: "inventory_pricing"
        }
   }
]).exec(function(err, stocks) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(stocks);
		}
	});
};

/**
 * Get Stock where leve is lower then minimum
 */
exports.lowstockreport = function(req, res) {
	InventoryStock.aggregate([
   {
      $lookup:
         {
            from: "inventories",
            localField: "id",
            foreignField: "code",
            as: "inventory"
        }
   }
]).exec(function(err, stocks) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var itemstosend = [];
			for(var i=0;i<stocks.length;i++) {
				if(stocks[i].inventory[0].reorder_level>0) {
					var quantity = 0;
					
					for(var j=0;j<stocks[i].batch.length;j++) {
						quantity+=stocks[i].batch[j].quantity;
					}
					quantity = quantity/stocks[i].inventory[0].purchase_to_storage;
					quantity = quantity/stocks[i].inventory[0].storage_to_selling;
					quantity = Math.floor(quantity);
					
					if(quantity < stocks[i].inventory[0].reorder_level) {
						itemstosend.push({id: stocks[i].id,
										  description: stocks[i].description,
										  quantity: quantity,
										  minimum: stocks[i].inventory[0].reorder_level})
					}
				}
			}
			res.jsonp(itemstosend);
		}
	});
};

/**
 * Retrieve array of Inventories Stocks
 * To Do: Add to one function if model stays same
 */
var searchByInventory = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryStock.find({'id':{$regex : regex}},'-__v').exec(function(err, stocks) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.id.indexOf(keyword) < b.id.indexOf(keyword))
					return -1;
				if (a.id.indexOf(keyword) > b.id.indexOf(keyword))
					return 1;
				return 0;
			};
			stocks.sort(compare);
			res.json(stocks);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'inventory') {	
		searchByInventory(req, res);
	}
};

exports.getbyStore = function(req, res) { 
	var keyword = req.params.keyword;
	var store = req.params.store;
	var regex = new RegExp(keyword, 'i');
	
	var query = '';
	query = {'description':{$regex : regex}, 'batch.storeId': store};
	InventoryStock.find(query,'-__v').limit(20).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
			    return -1;
			  if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
			    return 1;
			  return 0;
			};
			
			inventories.sort(compare);
			var itemstosend = [];
			for(var i=0;i<inventories.length;i++) {
				var totalstock = 0;
				for(var j=0;j<inventories[i].batch.length;j++) {
					if(inventories[i].batch[j].storeId == store) {
						totalstock+= inventories[i].batch[j].quantity;
					}
				}
				if(totalstock > 0) {
					itemstosend.push({id: inventories[i].id,
								  description: inventories[i].description,
								  totalstock: totalstock});
				}
			}
			
			

			res.json(itemstosend);
		}
	});
};

exports.keywordSearch = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');
	
	var query = '';
	query = {'description':{$regex : regex}};
	InventoryStock.find(query,'-__v').limit(20).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
			    return -1;
			  if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
			    return 1;
			  return 0;
			};
			
			inventories.sort(compare);
			var itemstosend = [];
			for(var i=0;i<inventories.length;i++) {
				var totalstock = 0;
				for(var j=0;j<inventories[i].batch.length;j++) {
					totalstock+= inventories[i].batch[j].quantity;
				}
				itemstosend.push({id: inventories[i].id,
								  description: inventories[i].description,
								  totalstock: totalstock});
			}
			
			

			res.json(itemstosend);
		}
	});
};

/**
 * Retrieve Stock with smallest batch number but quantity greater than 0
 * Update accordingly.
 */
var deductInventory = function (inventoryArray, quantity) {
	var remaining = 0;
	for (var i = 0; i < inventoryArray.length; i++) {
		//console.log(inventoryArray[i]);
		if (inventoryArray[i].quantity > 0) {
			if (inventoryArray[i].quantity - quantity < 0) {
				remaining = quantity - inventoryArray[i].quantity;
				inventoryArray[i].quantity = 0;
			} else {
				inventoryArray[i].quantity = inventoryArray[i].quantity - quantity;
				return inventoryArray;
			}
			if (remaining > 0) {
				inventoryArray = deductInventory(inventoryArray, remaining);
				return inventoryArray;
			}
		}
	}
};

exports.adjust = function(req, res) {
	InventoryStock.findOne({'id': req.params.stockId}).exec(function(err, stock) {
		if (stock) {
			var inventoryStock = stock;
			inventoryStock.batch.sort(function (a, b) {return a - b;});
			inventoryStock.batch = deductInventory(inventoryStock.batch, req.body.quantity);
			inventoryStock.save(function (err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventoryStock);
				}
			});
		} else {
			err = 'Inventory stock not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

var addInventory = function(inventoryArray, quantity) {
	for (var i = 0; i < inventoryArray.length; i++) {
		if (inventoryArray[i].quantity > 0) {
			console.log('breaking: ' + i);
			break;
		}
	}
	if (i === inventoryArray.length) {
		// quantity is 0
		inventoryArray[inventoryArray.length - 1].quantity = inventoryArray[inventoryArray.length - 1].quantity + quantity;
	} else {
		// previous quantity is updated
		inventoryArray[i].quantity = inventoryArray[i].quantity + quantity;
	}
	return inventoryArray;
};

exports.adjustReturn = function(req, res) {
	InventoryStock.findOne({'id': req.params.stockId}).exec(function(err, stock) {
		if (stock) {
			var inventoryStock = stock;
			inventoryStock.batch.sort(function (a, b) {return a - b;});
			inventoryStock.batch = addInventory(inventoryStock.batch, req.body.quantity);
			inventoryStock.save(function (err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventoryStock);
				}
			});
		} else {
			err = 'Inventory stock not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Check quantity and expiry in Inventory Stock
 */
exports.check = function(req, res) {
	var searched = [];
	var quantity = 0;
	if (req.params.stockId === 'getQuantity') {
		InventoryStock.find().exec(function (err, stocks) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				for (var key in stocks) {
					quantity = 0;
					for (var key2 in stocks[key].batch) {
						if (stocks[key].batch[key2].quantity !== undefined) {
							quantity = quantity + stocks[key].batch[key2].quantity;
						}
					}
					if (quantity < 100) {
						searched.push({'code': stocks[key].id, 'description': stocks[key].description, 'remaining': quantity});
					}
				}
				res.jsonp(searched);
			}
		});
	} else if (req.params.stockId === 'getExpiry') {
		var dateToday = new Date();
		var expiryDate;
		InventoryStock.find().exec(function (err, stocks) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				for (var key in stocks) {
					for (var key2 in stocks[key].batch) {
						if (stocks[key].batch[key2].expiryDate !== undefined) {
							if (stocks[key].batch[key2].quantity > 0) {
								if (stocks[key].batch[key2].expiryDate !== null) {
									var timeDiff = stocks[key].batch[key2].expiryDate.getTime() - dateToday.getTime();
									var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
									if (diffDays < 180) {
										searched.push({'id': stocks[key].id, 'description': stocks[key].description, 'batch': stocks[key].batch[key2].batchID, 'expiryIn': diffDays});
									}
								}
							}
						}
					}
				}
				res.jsonp(searched);
			}
		});
	} else {
		InventoryStock.findOne({'id': req.params.stockId}).exec(function (err, stock) {
			if(stock){
				quantity = 0;
				for (var key in stock.batch) {
					if (stock.batch[key].quantity !== undefined) {
						quantity = quantity + stock.batch[key].quantity;
					}
				}
				searched.push({'id': req.params.stockId, 'remaining': quantity});
				res.jsonp(searched);
			}

			else if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				searched.push({'id': req.params.stockId, 'remaining': 0});
				res.json(searched);
			}
		});
	}
};


exports.supplierBasedStock = function(req, res) {
	InventoryPricing.aggregate([
		{ "$match": { "supplierId": req.params.supplierId}},
		
		{ "$lookup": {
            from: "inventorystocks",
            localField: "inventoryCode",
            foreignField: "id",
            as: "aggre"
        }
	}]).exec(function(err, stocks) {
		if (err) {
			console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log(stocks);
			var totalInStock = 0;
			var supplierItems = [];
			
			for(var i=0;i<stocks.length;i++){

				for(var j=0;j<stocks[i].aggre[0].batch.length;j++){
					if(stocks[i].aggre[0].batch[j].storeDescription !== 'Stock Adjustment') {
						totalInStock += stocks[i].aggre[0].batch[j].quantity;
					}
					//console.log(stocks[i].aggre[0].batch[j].quantity);
				}
              	
              	stocks[i].totalInStock = totalInStock;
				stocks[i].description = stocks[i].aggre[0].description;
				stocks[i].id = stocks[i].aggre[0].id;
                supplierItems.push(stocks[i]);
		        totalInStock = 0;
		    }

			res.json(supplierItems);
		}
	});
};


