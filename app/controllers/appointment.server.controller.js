'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Appointment = mongoose.model('Appointment'),
	Doctor = mongoose.model('Doctor'),
	_ = require('lodash'),
	moment = require('moment'),
	request = require('request');


/**
 * Create new Appointment
 */

var sendSmsForPatientAppointment = function(number , messagedata){
	
	if(number.charAt(0) == '0' && number.substring(0 , 3) != '051'){
	   number = number.substring(1);
	   number = '92' + number;
	}
	
    var stringApiPath = 'http://smsctp1.eocean.us:24555/api?action=sendmessage';
    var stringData = "&username=" + '8655_Sarfaraz' + "&password=" + 'SaRfArAz12' + "&recipient=" + number + "&originator=" + "8655" + "&messagedata=" + messagedata;
    var string = stringApiPath + stringData;
    var options = {
        uri: string,
        method: 'GET'
    };

    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            result = body;
        } else {
            var result = 'Not Found';
        }
    });
};
 
exports.create = function(req, res) {
	
	req.checkBody('patient_name', 'Patient Name is required').notEmpty();
	req.checkBody('doctor_id', 'Doctor id is required').notEmpty();
	req.checkBody('phone', 'Phone number is required').notEmpty();
	req.checkBody('appointment_date', 'Appointment date is required').notEmpty();
	req.checkBody('time', 'Appointment time is required').notEmpty();

	var errors = req.validationErrors();
	
	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {
		var appointment = new Appointment(req.body);
		
		appointment.save(function(err, appointment) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(appointment);
				//send sms to patient
				var msg = 'Dear '+ appointment.patient_name +'\nYour appointment with '+ appointment.doctor_name+
				' is confirmed at '+appointment.appointment_date+'@'+appointment.time+
				'\nAli Medical Clinics\n'+
				'Royal Suites, Ground Floor, E-11/2'
				'051-8746777';
				sendSmsForPatientAppointment(appointment.phone, msg);
			}
		});	
	}
};

/**
 * Register new Walkin Appointment
 */
exports.create_walkin_appointment = function(req, res) {

	req.checkBody('patient_name', 'Patient Name is required').notEmpty();
	req.checkBody('doctor_id', 'Doctor id is required').notEmpty();
	req.checkBody('phone', 'Phone number is required').notEmpty();
	//req.checkBody('mr_number', 'Mr Number is required').notEmpty();
	req.checkBody('doctor_name', 'Doctor name is required').notEmpty();
	req.checkBody('appointment_date', 'Appointment date is required').notEmpty();

	var errors = req.validationErrors();
	var query;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		//check if any available slots and assign
		/*query = Appointment.find({appointment_date: req.body.appointment_date, doctor_id: req.body.doctor_id});
		query.exec(function(err, appointments){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var filled_slots = [];
				
		
				for(var i=0; i<appointments.length;i++)
					filled_slots.push(appointments[i].time);
				}
				var dt1   = parseInt(req.body.appointment_date.substring(0,2));
				var mon1  = parseInt(req.body.appointment_date.substring(3,5));
				var yr1   = parseInt(req.body.appointment_date.substring(6,10));
				var date1 = new Date(yr1, mon1-1, dt1);
				var weekday=new Array(7);
				weekday[0]="Sunday";
				weekday[1]="Monday";
				weekday[2]="Tuesday";
				weekday[3]="Wednesday";
				weekday[4]="Thursday";
				weekday[5]="Friday";
				weekday[6]="Saturday";
				var minutes = moment().minutes();
				var hours = moment().hours();
				var timeSlot;
				
				var day = weekday[date1.getDay()];
				var slots = [];
				query = Doctor.findById(req.body.doctor_id);
				query.exec(function(err, doctor){	
					if(err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						var index = _.findIndex(doctor.schedule, function(o) { return o.day == day; });
						if(index == -1) {
							res.status(200).send('Not available on this day');
							return;
						}
						var timings1 = doctor.schedule[index].timings;
						var timings = _.chain(timings1)
						  .sortBy('starthrs')
						  .sortBy('start_meridian')
						  .value();
						  console.log(timings);
						var slot_duration = doctor.slot_duration;
						
						for(var i=0; i<timings.length; i++) {
							var starttime;
							var endtime;
							if(timings[i].start_meridian == 'PM' && timings[i].starthrs !== '12') {
								starttime = (parseInt(timings[i].starthrs)+12)+':'+timings[i].startmins;
							} else {
								starttime = timings[i].starthrs+':'+timings[i].startmins;
							}
							if(timings[i].end_meridian == 'PM' && timings[i].endhrs !== '12') {
								endtime = (parseInt(timings[i].endhrs)+12)+':'+timings[i].endmins;
							} else {
								endtime = timings[i].endhrs+':'+timings[i].endmins;
							}
							
							var originalTimingTo = 0;
							var originalTimingFrom = 0;
							var tohrs = (timings[i].end_meridian=='PM' && parseInt(timings[i].endhrs)!=12) ? (parseInt(timings[i].endhrs)+12) : (parseInt(timings[i].endhrs));
							var fromhrs =  (timings[i].start_meridian=='PM' && parseInt(timings[i].starthrs)!=12) ? (parseInt(timings[i].starthrs)+12) : (parseInt(timings[i].starthrs));
							originalTimingTo = (tohrs * 60) + parseInt(timings[i].endmins);
							originalTimingFrom = (fromhrs * 60) + parseInt(timings[i].startmins);

							var totalslots = (originalTimingTo - originalTimingFrom)/slot_duration;
							var st = moment.duration(starttime);

							for(var j=0;j<totalslots;j++) {
								var temp = moment.duration(st).add(Number(slot_duration) * (j), 'm');
								var timeToAdd;
								//console.log(temp._data.hours);
								if(temp._data.hours > 12) {
									if(temp._data.minutes == 0) {
										timeToAdd = (temp._data.hours-12).toString() + ":" + temp._data.minutes.toString() + '0 PM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = (temp._data.hours-12).toString() + ":0" + temp._data.minutes.toString() + ' PM';
									} else {
										timeToAdd = (temp._data.hours-12).toString() + ":" + temp._data.minutes.toString() + ' PM';
									}
								} else if(temp._data.hours == 12) {
									if(temp._data.minutes == 0) {
										timeToAdd = (temp._data.hours).toString() + ":" + temp._data.minutes.toString() + '0 PM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = (temp._data.hours).toString() + ":0" + temp._data.minutes.toString() + ' PM';
									} else {
										timeToAdd = (temp._data.hours).toString() + ":" + temp._data.minutes.toString() + ' PM';
									}
								} else if(temp._data.hours == 0) {
									if(temp._data.minutes == 0) {
										timeToAdd = 12 + ":" + temp._data.minutes.toString() + '0 AM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = 12 + ":0" + temp._data.minutes.toString() + ' AM';
									} else {
										timeToAdd = 12 + ":" + temp._data.minutes.toString() + ' AM';
									}
								} else {
									if(temp._data.minutes == 0) {
										timeToAdd = temp._data.hours.toString() + ":" + temp._data.minutes.toString() + '0 AM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = temp._data.hours.toString() + ":0" + temp._data.minutes.toString() + ' AM';
									} else {
										timeToAdd = temp._data.hours.toString() + ":" + temp._data.minutes.toString() + ' AM';
									}
								}
								if(filled_slots.indexOf(timeToAdd) == -1) {
									if((temp._data.hours == hours && temp._data.minutes > minutes) || (temp._data.hours>hours)) {
										timeSlot = timeToAdd;
										break;
									}
								} 
							}
						}*/
						
						var appointment = new Appointment(req.body);
						/*if(timeSlot) {
							appointment.time = timeSlot;
							appointment.save(function(err, appointment) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									res.status(200).send(appointment);
								}
							});
						} else {*/
							appointment.time = '';
							query = Appointment.findOne({appointment_date: req.body.appointment_date, doctor_id: req.body.doctor_id}, {number_in_queue: 1}).sort({number_in_queue:-1});
							query.exec(function(err, appoint){
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									var number = 1;
									if(appoint) {
										if(appoint.number_in_queue) {
											number = appoint.number_in_queue + 1;
										} 
									}
									appointment.number_in_queue = number;
									appointment.save(function(err, appointment) {
										if (err) {
											return res.status(400).send({
												message: errorHandler.getErrorMessage(err)
											});
										} else {
											res.status(200).send(appointment);
										}
									});
								}
							});
						}						
					//}
				//});
	//	});
		
		//////////////////////////////////////////
		
/*		 if(!req.body.time) { //if time is not specified book on waiting
			var appointment = new Appointment(req.body);	
			appointment.time = '';
			
			//get current max queue number if any
			query = Appointment.findOne({appointment_date: req.body.appointment_date, doctor_id: req.params.doctor_id}, {number_in_queue: 1}).sort({number_in_queue:-1});
				query.exec(function(err, appointment){
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var number;
					if(appointment.number_in_queue) {
						number = appointment.number_in_queue + 1;
					} else {
						number = 1;
					}
					appointment.number_in_queue = number;
					appointment.save(function(err, appointment) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							res.status(200).send(appointment);
						}
					});
				}
			});
		} else {
				appointment.save(function(err, appointment) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.status(200).send(appointment);
				}
			});
		}*/
	};
	

/**
 * Retrieve appointments by id
 */
exports.read = function(req, res) {
	var query;
	if(req.params.appointment_id) {
		query = Appointment.findById(req.params.appointment_id);
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
	query.exec(function(err, appointment){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(appointment);
		}
	});
};

/**
 * Retrieve appointments by doctor id and date
 */
exports.viewappointments = function(req, res) {
	var query;
	if(req.params.doctor_id && req.params.date) {
		if(req.params.doctor_id == 'all') {
			query = Appointment.find({appointment_date: req.params.date});
		} else {
			query = Appointment.find({appointment_date: req.params.date, doctor_id: req.params.doctor_id});
		}
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
	query.exec(function(err, appointments){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).send(appointments);
		}
	});
};

/**
 * Retrieve appointments by doctor username and date
 */
exports.viewappointmentsbyusername = function(req, res) {
	var query;
	if(req.params.username && req.params.date) {
		
		query = Doctor.findOne({username: req.params.username}, {_id:1});
		query.exec(function(err, doctor){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else if(doctor) {
				query = Appointment.find({appointment_date: req.params.date, doctor_id: doctor._id});
				query.exec(function(err, appointments){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {

						res.status(200).send(appointments);
					}
				});
			}
		});		
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
	
};

/**
 * Get doctor slots by docotr id and date
 */
exports.getslots = function(req, res) {
	var query;
	var valueToSend = [];
	if(req.params.doctor_id && req.params.date) {
		query = Appointment.find({appointment_date: req.params.date, doctor_id: req.params.doctor_id});
		query.exec(function(err, appointments){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var filled_slots = [];
				for(var i=0; i<appointments.length;i++)
					filled_slots.push(appointments[i].time);
				}
			
			var dt1   = parseInt(req.params.date.substring(0,2));
			var mon1  = parseInt(req.params.date.substring(3,5));
			var yr1   = parseInt(req.params.date.substring(6,10));
			var date1 = new Date(yr1, mon1-1, dt1);
			var weekday=new Array(7);
				weekday[0]="Sunday";
				weekday[1]="Monday";
				weekday[2]="Tuesday";
				weekday[3]="Wednesday";
				weekday[4]="Thursday";
				weekday[5]="Friday";
				weekday[6]="Saturday";

				var day = weekday[date1.getDay()];
				var slots = [];
				query = Doctor.findById(req.params.doctor_id);
				query.exec(function(err, doctor){	
					if(err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						var index = _.findIndex(doctor.schedule, function(o) { return o.day == day; });
						if(index == -1) {
							res.status(200).send('Not available on this day');
							return;
						}
						
						var timings1 = doctor.schedule[index].timings;
						var timings = _.chain(timings1)
						  .sortBy('starthrs')
						  .sortBy('start_meridian')
						  .value();
						var slot_duration = doctor.slot_duration;
						
						for(var i=0; i<timings.length; i++) {
							var starttime;
							var endtime;
							if(timings[i].start_meridian == 'PM' && timings[i].starthrs !== '12') {
								starttime = (parseInt(timings[i].starthrs)+12)+':'+timings[i].startmins;
							} else {
								starttime = timings[i].starthrs+':'+timings[i].startmins;
							}
							if(timings[i].end_meridian == 'PM' && timings[i].endhrs !== '12') {
								endtime = (parseInt(timings[i].endhrs)+12)+':'+timings[i].endmins;
							} else {
								endtime = timings[i].endhrs+':'+timings[i].endmins;
							}
							
							var originalTimingTo = 0;
							var originalTimingFrom = 0;
							var tohrs = (timings[i].end_meridian=='PM' && parseInt(timings[i].endhrs)!=12) ? (parseInt(timings[i].endhrs)+12) : (parseInt(timings[i].endhrs));
							var fromhrs =  (timings[i].start_meridian=='PM' && parseInt(timings[i].starthrs)!=12) ? (parseInt(timings[i].starthrs)+12) : (parseInt(timings[i].starthrs));
							originalTimingTo = (tohrs * 60) + parseInt(timings[i].endmins);
							originalTimingFrom = (fromhrs * 60) + parseInt(timings[i].startmins);

							var totalslots = (originalTimingTo - originalTimingFrom)/slot_duration;
							var st = moment.duration(starttime);

							for(var j=0;j<totalslots;j++) {
								var temp = moment.duration(st).add(Number(slot_duration) * (j), 'm');
								var timeToAdd;
								//console.log(temp._data.hours);
								if(temp._data.hours > 12) {
									if(temp._data.minutes == 0) {
										timeToAdd = (temp._data.hours-12).toString() + ":" + temp._data.minutes.toString() + '0 PM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = (temp._data.hours-12).toString() + ":0" + temp._data.minutes.toString() + ' PM';
									} else {
										timeToAdd = (temp._data.hours-12).toString() + ":" + temp._data.minutes.toString() + ' PM';
									}
								} else if(temp._data.hours == 12) {
									if(temp._data.minutes == 0) {
										timeToAdd = (temp._data.hours).toString() + ":" + temp._data.minutes.toString() + '0 PM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = (temp._data.hours).toString() + ":0" + temp._data.minutes.toString() + ' PM';
									} else {
										timeToAdd = (temp._data.hours).toString() + ":" + temp._data.minutes.toString() + ' PM';
									}
								} else if(temp._data.hours == 0) {
									if(temp._data.minutes == 0) {
										timeToAdd = 12 + ":" + temp._data.minutes.toString() + '0 AM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = 12 + ":0" + temp._data.minutes.toString() + ' AM';
									} else {
										timeToAdd = 12 + ":" + temp._data.minutes.toString() + ' AM';
									}
								} else {
									if(temp._data.minutes == 0) {
										timeToAdd = temp._data.hours.toString() + ":" + temp._data.minutes.toString() + '0 AM';
									} else if(temp._data.minutes < 10) {
										timeToAdd = temp._data.hours.toString() + ":0" + temp._data.minutes.toString() + ' AM';
									} else {
										timeToAdd = temp._data.hours.toString() + ":" + temp._data.minutes.toString() + ' AM';
									}
								}
								if(filled_slots.indexOf(timeToAdd) == -1) {
									valueToSend.push({slot: timeToAdd, status: 'Available'});
								} else {
									valueToSend.push({slot: timeToAdd, status: 'Booked'});
								}
							}
						}
						//var sorted =_.sortBy(valueToSend, "slot");
						
						res.status(200).send(valueToSend);
					}
				});
		});
		
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}	
};

/**
 * List of Appointments
 */
exports.list = function(req, res) {
	Appointment.find({}).exec(function(err, appointments) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(appointments);
		}
	});
};

/**
 * Update an appointment
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('appointment_id', 'Appointment id is required').notEmpty();
	var errors = req.validationErrors();
	
	var item = req.body;
	
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Appointment.findOneAndUpdate({'_id': req.params.appointment_id}, item, {upsert: true}, function(err, appointment) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(appointment);
			}
		});
	}
};

exports.update_vitals_status = function(req, res) {
	delete req.body.__v;
	req.checkBody('appointment_id', 'Appointment id is required').notEmpty();

	var errors = req.validationErrors();
	
	var item = req.body;
	
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Appointment.findOne({'_id': req.body.appointment_id}, function(err, appointment) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				appointment.vitals_taken = true;
				appointment.save(function(err, appointment) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.status(200).send(appointment);
					}
				});	
			}
		});
	}
};

exports.update_on_register = function(req, res) {
	delete req.body.__v;
	req.checkBody('appointment_id', 'Appointment id is required').notEmpty();
	req.checkBody('patient_name', 'Patient name is required').notEmpty();
	req.checkBody('mr_number', 'Mr number is required').notEmpty();
	req.checkBody('phone', 'Phone is required').notEmpty();

	var errors = req.validationErrors();
	
	var item = req.body;
	
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Appointment.findOneAndUpdate({'_id': req.body.appointment_id}, item, function(err, appointment) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(appointment);
			}
		});
	}
};

/**
 * Update an appointment
 */
exports.update_appointment_status = function(req, res) {
	delete req.body.__v;
	req.checkBody('appointment_id', 'Appointment id is required').notEmpty();
	req.checkBody('status', 'Appointment status is required').notEmpty();

	var errors = req.validationErrors();
	
	var item = req.body;
	
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Appointment.findOneAndUpdate({'_id': req.body.appointment_id}, {appointment_status: item.status, modified: new Date()}, function(err, appointment) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(appointment);
			}
		});
	}
};


/**
 * Delete an appointment
 */
exports.delete = function(req, res) {
	Appointment.remove({'_id':req.params.appointment_id} , function(err,appointment) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {	
			res.sendStatus(200).send(appointment);
		}
	});
};

var keywordSearch = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');
	Appointment.find({'patient_name':{$regex : regex}},{_id:1,name:1}).limit(10).exec(function(err, doctors) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.name.indexOf(keyword) < b.name.indexOf(keyword))
			    return -1;
			  if (a.name.indexOf(keyword) > b.name.indexOf(keyword))
			    return 1;
			  return 0;
			};

			doctors.sort(compare);
			res.status(200).json(doctors);
		}
	});
};

exports.search = function(req, res) {
	keywordSearch(req, res);
};