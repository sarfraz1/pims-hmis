'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Panel = mongoose.model('Panel'),
    _ = require('lodash');

/**
 * Create a Panel
 */
exports.create = function(req, res) {
	req.checkBody('description', 'Description is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var panel = new Panel(req.body);
		panel.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(panel);
			}
		});
	}
};

/**
 * Show the current Panel
 */
exports.read = function(req, res) {
	console.log(req.params.panelID);
	if (!req.params.panelID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		Panel.findOne({'_id': req.params.panelID}).exec(function(err, panel) {
			if(err){
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(panel);
			}
		});
	}
};

exports.getDiscount = function(req, res) {
	if (!req.params.panelID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		var sent = false;
		Panel.findOne({'description': req.params.panelID}).exec(function(err, panel) {
			if (panel) {
				for(var i=0;i<panel.coverages.length;i++){
					if(panel.coverages[i].category.indexOf(req.params.category)>-1){
						for(var j=0;j<panel.coverages[i].facilities.length;j++){
							if(panel.coverages[i].facilities[j].description === req.params.description){
								res.json(panel.coverages[i].facilities[j]);
								sent = true;
								break;
							}
						}
					}
				}
				if(sent===false)
					res.json(0);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};
/**
 * Update a Panel
 */
exports.update = function(req, res) {
	if (!req.params.panelID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		req.checkBody('description', 'Description is required').notEmpty();
		req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			Panel.findOneAndUpdate({'_id': req.params.panelID}, req.body).exec(function (err, panel) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(panel);
				}
			});
		}
	}
};

/**
 * Delete a Panel
 */
exports.delete = function(req, res) {
	if (!req.params.panelID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		Panel.remove({'_id': req.params.panelID}).exec(function (err, panel) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(panel);
			}
		});
	}
};



/**
 * List of Panels
 */
exports.getPaneList = function(req, res) {
	Panel.find({},{description: 1}).exec(function(err, panels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(panels);
		}
	});
};


/**
 * List of Panels
 */
exports.list = function(req, res) {
	Panel.find().exec(function(err, panels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(panels);
		}
	});
};

/**
 * Search Panels
 */
var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Panel.find({'description':{$regex : regex}},'-__v').exec(function(err, panels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
					return -1;
				if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
					return 1;
				return 0;
			};
			panels.sort(compare);
			res.json(panels);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'description') {
		searchByName(req, res);
	}
};