'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	FacilityPricing = mongoose.model('FacilityPricing'),
	_ = require('lodash'),
	moment = require('moment');


/**
 * Create a new node
 */
 exports.create = function(req, res) {
	var errors = req.validationErrors();
	
	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {
		console.log(req.body);
		//var facilityPricing = new FacilityPricing(req.body);
		FacilityPricing.findOneAndUpdate({ $or: [{'_id': req.body._id}, {'description':req.body.description,'category':req.body.category}] }, req.body, {upsert: true}).exec(function(err, facility_pricing) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(facility_pricing);
			}
		});
	}
};

/**
 * Delete a Node
 */
exports.delete = function(req, res) {
	var regex = new RegExp(req.params.categoryName, 'i');
	FacilityPricing.remove({'category':regex}, function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {	
			res.sendStatus(200).send(facility);
		}
	});
};


/**
 * Retrieve immediate children of a node
 */
exports.read = function(req, res) {
	var query;
	if(req.params.category) {
		var query;
		query = FacilityPricing.find({category:req.params.category},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);				
			} 
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
};

/**
 * Retrieve immediate children of a node
 */
exports.getbynameandcategory = function(req, res) {
	var query;
	if(req.params.category && req.params.service_name) {
		var query;
		query = FacilityPricing.findOne({category:req.params.category, description:req.params.service_name},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);				
			} 
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
};

exports.searchbynameandcategory = function(req, res) {
	var query;
	if(req.params.category) {
		var pcat = new RegExp(req.params.category, 'i');
		if(!req.params.service_name)
			req.params.service_name='';
		var pval = new RegExp(req.params.service_name, 'i');
		var query;
	query = {'category':{$regex : pcat}, 'description': {$regex : pval}};
	FacilityPricing.find(query,'-__v').limit(15).exec(function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.description.indexOf(req.params.service_name) < b.description.indexOf(req.params.service_name))
			    return -1;
			  if (a.description.indexOf(req.params.service_name) > b.description.indexOf(req.params.service_name))
			    return 1;
			  return 0;
			};
			
			facility.sort(compare);
			//console.log(compare);
			res.json(facility);
		}
	});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
	
};

/**
 * Retrieve services by category
 */
exports.getbycategory = function(req, res) {
	var query;

	if(req.params.category) {
		var query;
		query = FacilityPricing.find({category:req.params.category},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);				
			} 
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}
};

/**
 * List of Patients
 */
exports.list = function(req, res) {
	FacilityPricing.find({}).exec(function(err, facility_pricing) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(facility_pricing);
		}
	});
};

exports.aggregate_by_category = function(req, res) {
	FacilityPricing.aggregate(
		[ { $group : { _id : "$category" ,
		facilities: { $push:  { description: "$description", price: "$price", category: "$category", discount: 0
	,discountType: "percent" }}
						 }}
			]
	).exec(function(err, facility_pricing) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(facility_pricing);
		}
	});
};


