'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryCustomerSupplier = mongoose.model('StoreInventoryCustomerSupplier'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories customer supplier
 */
exports.create = function(req, res) {
	var inventoryCustomerSupplier = new InventoryCustomerSupplier(req.body);
	inventoryCustomerSupplier.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventoryCustomerSupplier);
		}
	});
};

/**
 * Show the current Inventories customer supplier
 */
exports.read = function(req, res) {
	InventoryCustomerSupplier.findOne({'_id': req.params.customerSupplierId}).exec(function(err, customerSupplier) {
		if (customerSupplier) {
			res.json(customerSupplier);
		} else {
			err = 'Inventory customer supplier not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Get all manufacturers
 */
exports.getmanufacturers = function(req, res) {
	InventoryCustomerSupplier.find({'customerSupplierType': 'manufacturer'}).exec(function(err, manufacturers) {
		if (manufacturers) {
			res.json(manufacturers);
		} else {
			err = 'Manufacturers not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories customer supplier
 */
exports.update = function(req, res) {
	InventoryCustomerSupplier.findOneAndUpdate({'_id': req.params.customerSupplierId}, req.body).exec(function(err, customerSupplier){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(customerSupplier);
		}
	});
};

/**
 * Delete an Inventories customer supplier
 */
exports.delete = function(req, res) {
	InventoryCustomerSupplier.remove({'_id': req.params.customerSupplierId}, function(err, customerSupplierId) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(customerSupplierId);
		}
	});
};

/**
 * List of Inventories customer suppliers
 */
exports.list = function(req, res) {
	InventoryCustomerSupplier.find().exec(function(err, customerSuppliers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(customerSuppliers);
		}
	});
};

/**
 * Search by description
 */
var searchByDescription = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryCustomerSupplier.find({'description':{$regex : regex}},'-__v').exec(function(err, customerSuppliers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
					return -1;
				if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
					return 1;
				return 0;
			};
			customerSuppliers.sort(compare);
			res.json(customerSuppliers);
		}
	});
};

var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryCustomerSupplier.find({'name':{$regex : regex}},'-__v').exec(function(err, customerSuppliers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.name.indexOf(keyword) < b.name.indexOf(keyword))
					return -1;
				if (a.name.indexOf(keyword) > b.name.indexOf(keyword))
					return 1;
				return 0;
			};
			customerSuppliers.sort(compare);
			res.json(customerSuppliers);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'name') {	
		searchByName(req, res);
	}
	else if (req.params.searchType === 'description') {
		searchByDescription(req, res);
	}
};