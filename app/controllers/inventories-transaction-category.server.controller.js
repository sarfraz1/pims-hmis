'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryTransactionCategory = mongoose.model('InventoryTransactionCategory'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories transaction category
 */
exports.create = function(req, res) {
	var inventoryTransactionCategory = new InventoryTransactionCategory(req.body);
	inventoryTransactionCategory.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventoryTransactionCategory);
		}
	});
};

/**
 * Show the current Inventories transaction category
 */
exports.read = function(req, res) {
	InventoryTransactionCategory.findOne({'_id': req.params.transactionCategoryId}).exec(function(err, transactionCategory) {
		if (transactionCategory) {
			res.json(transactionCategory);
		} else {
			err = 'Inventory transactionCategory not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories transaction category
 */
exports.update = function(req, res) {
	InventoryTransactionCategory.findOneAndUpdate({'_id': req.params.transactionCategoryId}, req.body).exec(function(err, transactionCategory){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(transactionCategory);
		}
	});
};

/**
 * Delete an Inventories transaction category
 */
exports.delete = function(req, res) {
	InventoryTransactionCategory.remove({'_id': req.params.transactionCategoryId}, function(err, transactionCategory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(transactionCategory);
		}
	});
};

/**
 * List of Inventories transaction categories
 */
exports.list = function(req, res) {
	InventoryTransactionCategory.find().exec(function(err, stores) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(stores);
		}
	});
};

/**
 * Retrieve array of Inventories Transaction Category
 * To Do: Merge both search functions into one if functionality remains the same
 */
var searchByType = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryTransactionCategory.find({'purchaseType':{$regex : regex}},'-__v').exec(function(err, transactionCategories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.purchaseType.indexOf(keyword) < b.purchaseType.indexOf(keyword))
					return -1;
				if (a.purchaseType.indexOf(keyword) > b.purchaseType.indexOf(keyword))
					return 1;
				return 0;
			};
			transactionCategories.sort(compare);
			res.json(transactionCategories);
		}
	});
};

var searchByPrefix = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryTransactionCategory.find({'prefix':{$regex : regex}},'-__v').exec(function(err, transactionCategories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.prefix.indexOf(keyword) < b.prefix.indexOf(keyword))
					return -1;
				if (a.prefix.indexOf(keyword) > b.prefix.indexOf(keyword))
					return 1;
				return 0;
			};
			transactionCategories.sort(compare);
			res.json(transactionCategories);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'type'){	
		searchByType(req, res);
	}
	else if (req.params.searchType === 'prefix'){
		searchByPrefix(req, res);
	}
};