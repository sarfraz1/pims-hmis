'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Prescription = mongoose.model('Prescription'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var prescription = new Prescription(req.body);
	prescription.save(function (err,data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};



/**
 * List Prescriptions
 */
exports.list = function(req, res) {
	Prescription.find().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * List Prescriptions not handled by Pharmacy
 */
exports.patientHistory = function(req, res) {
	Prescription.find({MRNo: req.params.mr_number}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

/**
 * Get prescription by appointment id
 */
exports.getPrescriptionbyappointmentId = function(req, res) {
	Prescription.findOne({appointment_id: req.params.appointment_id}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.getComplaints = function(req, res) {
	//console.log(req.params.complaint);
    var regex = new RegExp(req.params.complaint, "i");
    //console.log(regex);
    var query;
    if(req.params.complaint) {
        query = Prescription.distinct('complaint',{doctorUsername:req.params.doctorUsername,complaint:{$regex : regex}});
    } else {
    	req.params.complaint = '';
    	query = Prescription.distinct('complaint',{doctorUsername:req.params.doctorUsername});
    }
    query.exec(function(err , prescriptions){
        if(err){return next(err);} else {
          if(req.params.complaint) {
			  var dataToSort = [];
			  for(var i = 0 ; i < prescriptions.length; i++){
				var temp = {};
				temp.rank = prescriptions[i].toLowerCase().indexOf(req.params.complaint.toLowerCase());
				temp.val = prescriptions[i];
				dataToSort.push(temp);
			  }
			  dataToSort.sort(function(a, b) {
				return Number(a.rank) - Number(b.rank);
			  });
			  dataToSort = dataToSort.slice(0, 5);
			  res.json(dataToSort);
			}
			else {
				var dataToSort = [];
				for(var i = 0 ; i < prescriptions.length; i++){
					var temp = {};
					temp.val = prescriptions[i];
					dataToSort.push(temp);
				}
				res.json(dataToSort);
			}
		}
        
    });
};

exports.getProcedures = function(req, res) {
    var regex = new RegExp(req.params.procedure, "i");
    //console.log(regex);
    var query;
    if(req.params.procedure) {
        query = Prescription.distinct('procedures',{doctorUsername:req.params.doctorUsername,procedures:{$regex : regex}});
    } else {
    	req.params.procedure = '';
    	query = Prescription.distinct('procedures',{doctorUsername:req.params.doctorUsername});
    }
    query.exec(function(err , prescriptions){
        if(err){return next(err);} else {
			if(req.params.procedure) {
			  var dataToSort = [];
			  for(var i = 0 ; i < prescriptions.length; i++){
				var temp = {};
				temp.rank = prescriptions[i].toLowerCase().indexOf(req.params.procedure.toLowerCase());
				temp.val = prescriptions[i];
				dataToSort.push(temp);
			  }
			  dataToSort.sort(function(a, b) {
				return Number(a.rank) - Number(b.rank);
			  });
			  dataToSort = dataToSort.slice(0, 5);
			  res.json(dataToSort);
			} else {
				var dataToSort = [];
				for(var i = 0 ; i < prescriptions.length; i++){
					var temp = {};
					temp.val = prescriptions[i];
					dataToSort.push(temp);
				}
				res.json(dataToSort);
			}
        }
        
    });
};

exports.getDiagnosis = function(req, res) {
	//console.log(req.params.complaint);
    var regex = new RegExp(req.params.diagnosis, "i");
    //console.log(regex);
    var query;
    if(req.params.diagnosis) {
        query = Prescription.distinct('diagnosis',{doctorUsername:req.params.doctorUsername,diagnosis:{$regex : regex}});
    } else {
    	req.params.diagnosis = '';
    	query = Prescription.distinct('diagnosis',{doctorUsername:req.params.doctorUsername});
    }
    query.exec(function(err , prescriptions){
        if(err){return next(err);} else {
			if(req.params.diagnosis) {
				  var dataToSort = [];
				  for(var i = 0 ; i < prescriptions.length; i++){
					var temp = {};
					temp.rank = prescriptions[i].toLowerCase().indexOf(req.params.diagnosis.toLowerCase());
					temp.val = prescriptions[i];
					dataToSort.push(temp);
				  }
				  dataToSort.sort(function(a, b) {
					return Number(a.rank) - Number(b.rank);
				  });
				  dataToSort = dataToSort.slice(0, 5);
				  res.json(dataToSort);
			} else {
				var dataToSort = [];
				for(var i = 0 ; i < prescriptions.length; i++){
					var temp = {};
					temp.val = prescriptions[i];
					dataToSort.push(temp);
				}
				res.json(dataToSort);
			}
        }
        
    });
};

exports.getAdvice = function(req, res) {
	//console.log(req.params.complaint);
    var regex = new RegExp(req.params.advice, "i");
    //console.log(regex);
    var query;
    if(req.params.advice) {
        query = Prescription.distinct('advice',{doctorUsername:req.params.doctorUsername,advice:{$regex : regex}});
    } else {
    	req.params.advice = '';
    	query = Prescription.distinct('advice',{doctorUsername:req.params.doctorUsername});
    }
    query.exec(function(err , prescriptions){
        if(err){return next(err);} else {
          var dataToSort = [];
          for(var i = 0 ; i < prescriptions.length; i++){
            var temp = {};
            temp.rank = prescriptions[i].toLowerCase().indexOf(req.params.advice.toLowerCase());
            temp.val = prescriptions[i];
            dataToSort.push(temp);
          }
          dataToSort.sort(function(a, b) {
            return Number(a.rank) - Number(b.rank);
          });
          dataToSort = dataToSort.slice(0, 5);
          res.json(dataToSort);
        }
        
    });
};


exports.listNotHandled = function(req, res) {
	Prescription.find({handledByPharmacy: false, "medicines.0": {$exists: true}}).sort({_id:-1}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * Update Prescription from Pharmacy
 */
exports.handledByPharmacy = function(req, res) {
	Prescription.findOneAndUpdate({'_id': req.params.prescriptionid}, {$set: {'handledByPharmacy': true}}).exec(function(err, store){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(store);
		}
	});
};