'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller.js'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User');

/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables
	var user = req.user;
	var message = null;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		//user.displayName = user.firstName + ' ' + user.lastName;

		user.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};

exports.createUser = function(req, res) {
	// Init Variables
	var user = new User(req.body);
	user.provider = 'local';

	user.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			res.json(user);

		}
	});
};

exports.updateUser = function(req, res) {
	// Init Variables
	if(req.body.password !== undefined){
		User.findOne({'_id': req.params.userid}).exec(function(err, user) {
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else if(user){
			user.password = req.body.password;
			user.roles = req.body.roles;
			user.displayName = req.body.displayName;
			user.store = req.body.store;
			user.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					// Remove sensitive data before login
					user.password = undefined;
					user.salt = undefined;

					res.json(user);

				}
			});
		} else {
			res.json(null);
		}
	});
	} else {
		User.findByIdAndUpdate(req.body._id, req.body, {new: true}, function(err, user) {
		    if (err) {
		    	return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
		    } else {
		    	res.json(user);
		    }
  
  	});
	}
	
};

exports.getaUser = function(req, res) {
	// Init Variables
	User.findOne({'_id': req.params.userid}).exec(function(err, user) {
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else if(user){
			res.json(user);

		} else {
			res.json(null);
		}
	});
};

exports.getallUsers = function(req, res) {
	// Init Variables
	User.find({},{displayName:1,username:1,_id:1, roles:1}).exec(function(err, userList) {
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else if(userList){
			res.jsonp(userList);

		} else {
			res.json(null);
		}
	});
};

exports.getReceptionist = function(req,res){
	User.find({'roles': 'pharmacy salesman'},{'salt':0, 'password':0}).exec(function(err, usersList) {
		if (usersList) {
			res.json(usersList);
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

exports.getPathologistSignature = function(req,res){
	User.findOne({'roles': 'lab pathologist'}).exec(function(err, usersList) {
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else if(usersList){
			res.json(usersList.signature);
		} else {
			res.json(null);
		}
	});
};

exports.getOPDReceptionists = function(req, res) {
	User.find({'$or': [{'roles': 'opd receptionist'}, {'roles': 'ipd receptionist'}]}, {'salt': 0, 'password': 0}).exec(function (err, usersList) {
		if (usersList) {
			res.json(usersList);
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

exports.getOTAs = function(req, res) {
	User.find({'roles': 'OTA'}, {'salt': 0, 'password': 0}).exec(function (err, usersList) {
		if (usersList) {
			res.json(usersList);
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

exports.getDoctors = function(req,res){
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	User.find({'displayName':{$regex : regex},'roles': 'doctor'},{'salt':0, 'password':0}).exec(function(err, usersList) {
		//console.log(usersList);
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			var compare = function(a, b) {
				if (a.displayName.indexOf(keyword) < b.displayName.indexOf(keyword))
					return -1;
				if (a.displayName.indexOf(keyword) > b.displayName.indexOf(keyword))
					return 1;
				return 0;
			};
			usersList.sort(compare);
			res.json(usersList);
		}
	});
};

/**
 * Send User
 */
exports.me = function(req, res) {
	res.json(req.user || null);
};
