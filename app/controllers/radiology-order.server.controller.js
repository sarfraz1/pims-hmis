'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	RadiologyOrder = mongoose.model('RadiologyOrder'),
	multiparty = require('multiparty'),
	moment = require('moment'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Radiology Order
 */
exports.create = function(req, res) {
	req.checkBody('patientMRN', 'Patient MR Number is required').notEmpty();
	req.checkBody('billNumber', 'Bill Number is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {

		var radiologyOrder = new RadiologyOrder(req.body);
		RadiologyOrder.findOne({'billNumber': req.body.billNumber}).exec(function(err, reslabOrder) {
			if(err){
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else if(reslabOrder){
				reslabOrder.radiolgyTests = req.body.radiologyTests;
				reslabOrder.created_date = req.body.created_date;
				reslabOrder.time = req.body.time;
				reslabOrder.lastModified.push({lastModifiedBy: req.body.lastModifiedBy}); 
				reslabOrder.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(radiologyOrder);
					}
				});
			}
			else {
				radiologyOrder.lastModified = [];
				radiologyOrder.lastModified.push({lastModifiedBy: req.body.lastModifiedBy}); 
				radiologyOrder.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(radiologyOrder);
					}
				});
			}
		});

	}
};

exports.createImage = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {

        req.body = body;
        
        if(body.file){

            var picPath = body.imageLoc[0];
            
            var temp = body.file[0];
            temp = temp.split(',');
            
            var b64string = temp[1];
			var buf = new Buffer(b64string, 'base64');
            fs.writeFile(picPath , buf , function (err , success) {
                if(err){
                    return res.status(400).send({
                        'error': err
                    });
                }
                else {
					res.json('saved');
				}
            });
        }
        
	});
};

/**
 * Show the current orders
 */

exports.getByDate = function(req, res) {
	var fromDate = req.params.fromDate,
		toDate = req.params.toDate,
		query;

	if(fromDate==='undefined' &&  toDate==='undefined'){
		query =  RadiologyOrder.find();
	} else if(toDate==='undefined') {
		toDate = moment(fromDate).add(23, 'h');
		query =  RadiologyOrder.find({'created_date': { '$gte' : new Date(fromDate),'$lte' : new Date(toDate)}})
		//query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)}});
	} else {
		query =  RadiologyOrder.find({'created_date':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
		//query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
	}

	query.exec(function(err, radiologyOrder) {
		if (radiologyOrder) {
			res.json(radiologyOrder);
		} else {
			err = 'Panel not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

exports.read = function(req, res) {
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyOrder.findOne({'_id': req.params.labOrderId}).exec(function(err, radiologyOrder) {
			if (radiologyOrder) {
				res.json(radiologyOrder);
			} else {
				err = 'Order not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};


exports.update = function(req, res) {
	delete req.body._id;
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		//req.checkBody('description', 'Description is required').notEmpty();
		//req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			RadiologyOrder.findOneAndUpdate({'_id': req.params.labOrderId}, req.body).exec(function (err, radiologyOrder) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(radiologyOrder);
				}
			});
		}
	}
};

exports.updateStatus = function(req, res) {
	if (!req.params.labOrderId || !req.params.description || !req.params.status ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			RadiologyOrder.findOne({'_id': req.params.labOrderId}).exec(function (err, radiologyOrder) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					
					for(var i=0;i<radiologyOrder.radiologyTests.length;i++){
						if(radiologyOrder.radiologyTests[i].testDescription===req.params.description){
							radiologyOrder.radiologyTests[i].status = req.params.status;
							break;
						}
					}

					var allTrue = true;
					var alltestCompleted = true;
					for(i=0;i<radiologyOrder.radiologyTests.length;i++){
						if(radiologyOrder.radiologyTests[i].status !== 'Completed'){
							allTrue = false;
						}
						
						if(radiologyOrder.radiologyTests[i].status !== 'Test Completed'){
							alltestCompleted = false;
						}
					}
					
					if(allTrue === true){
						radiologyOrder.orderStatus = 'Completed';
					} else if(alltestCompleted === true){
						radiologyOrder.orderStatus = 'Test Completed';
					} else if(req.params.status==='Resample'){
						radiologyOrder.orderStatus = 'Resample';
						for(var i=0;i<radiologyOrder.radiologyTests.length;i++){
							if(radiologyOrder.radiologyTests[i].testDescription===req.params.description){
								radiologyOrder.radiologyTests[i].status = 'Resample';
							}
						}
					} else {
						radiologyOrder.orderStatus = 'In Progress';
					}

					radiologyOrder.save(function(err,resLabOrder){
						if(err){
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						}else {
							res.json(resLabOrder);	
						}
						
					});
					//res.json(labOrder);
				}
			});
		}
	}
};

/**
 * Delete a Panel
 */
exports.delete = function(req, res) {
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyOrder.remove({'_id': req.params.labOrderId}).exec(function (err, radiologyOrder) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(radiologyOrder);
			}
		});
	}
};

/**
 * List of Panels
 */
exports.list = function(req, res) {
	RadiologyOrder.find().exec(function(err, radiologyOrder) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(radiologyOrder);
		}
	});
};
