'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Doctor = mongoose.model('Doctor'),
	User = mongoose.model('User'),
	FacilityPricing = mongoose.model('FacilityPricing'),
	FacilityDetails = mongoose.model('FacilityDetails'),
	_ = require('lodash'),
	moment = require('moment');


/**
 * Register new doctor
 */
exports.create = function(req, res) {

	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('speciality', 'Speciality is required').notEmpty();
	req.checkBody('consultation_fee', 'Consultation fee is required').notEmpty();
	req.checkBody('slot_duration', 'Slot duration is required').notEmpty();

	var errors = req.validationErrors();

	if(errors) {
		return res.status(500).send(errors);
	} else {

		var doctor = new Doctor();
		doctor.name = req.body.name;
		doctor.speciality = req.body.speciality;
		doctor.consultation_fee = req.body.consultation_fee;
		doctor.percentage_fee = req.body.percentage_fee;
		doctor.slot_duration = req.body.slot_duration;
		doctor.pmdc_number = req.body.pmdc_number;
		doctor.qualification = req.body.qualification;
		doctor.phone = req.body.phone;
		doctor.email = req.body.email;
		doctor.cnic = req.body.cnic;
		doctor.pmdc_expiry = req.body.pmdc_expiry;
		doctor.schedule = [];
		doctor.username = req.body.username;
		var schedule = req.body.schedule;
		for(var i=0;i<schedule.length;i++) {
			var timing = {};

			timing.starthrs = schedule[i].starthrs;
			timing.endhrs = schedule[i].endhrs;
			timing.start_meridian = schedule[i].start_meridian;
			timing.end_meridian = schedule[i].end_meridian;
			timing.startmins = schedule[i].startmins;
			timing.endmins = schedule[i].endmins;

			for(var j=0; j<schedule[i].days.length; j++) {
				var index = _.findIndex(doctor.schedule, function(o) { return o.day == schedule[i].days[j].name; });
				var obj = {};
				if(index == -1) {
					obj.day = schedule[i].days[j].name;
					obj.timings = [];
					obj.timings.push(timing);;
					doctor.schedule.push(obj);
				} else {
					doctor.schedule[index].timings.push(timing);
				}
			}
		}
		doctor.save(function(err, doctor) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				// Create user
				var user = new User({
					displayName: doctor.name,
					username: doctor.username,
					password: 'hmis-123',
					roles: 'doctor',
					provider: 'local'
				});

				user.save(function(err) {
					if (err) {
						console.log(err);
					} else {
						console.log('created doctor');
					}
				});

				//Create OPD-Consultation service for doctor
				var service = {
						'description' : doctor.name,
						'price' : doctor.consultation_fee,
						'category' : 'Hospital-OPD-Consultation'
				};

				FacilityPricing.findOneAndUpdate( {'description': service.description,'category': service.category}, service, {upsert: true}).exec(function(err, facility_pricing) {
					if (err) {
						console.log(err);
						/*return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});*/
					} else {
						//res.json(facility_pricing);
						console.log('facility pricing created');
					}
				});

				//Create facility details if doctor has a percentage
				if(doctor.percentage_fee) {
					var serviceDetails = {
								description : doctor.name,
								category : 'Hospital-OPD-Consultation',
								expenses : [],
								shares : [{
											doctorId : doctor._id,
											doctorName : doctor.name,
											commissionPerc : doctor.percentage_fee
										}]
						};

			FacilityDetails.findOneAndUpdate( {'description':serviceDetails.description,'category':serviceDetails.category}, serviceDetails, {upsert: true}).exec(function(err, facility_details) {
			if (err) {
				console.log(err);

			} else {
				console.log('facility_details created');
			}
		});
				}
				res.status(200).send(doctor);
			}
		});

	}
};

/**
 * Retrieve doctor by id
 */
exports.read = function(req, res) {
	var query;
	if(req.params.key) {
		query = Doctor.findById(req.params.key);
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, doctor){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).send(doctor);
		}
	});
};

/**
 * Retrieve doctor by username
 */
exports.readbyname = function(req, res) {
	var query;
	if(req.params.username) {
		query = Doctor.findOne({'username': req.params.username});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, doctor){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).send(doctor);
		}
	});
};

/**
 * List of Patients
 */
exports.list = function(req, res) {
	Doctor.find({},{_id:1,name:1,speciality:1,consultation_fee:1,percentage_fee:1}).exec(function(err, doctors) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(doctors);
		}
	});
};

/**
 * Update a doctor
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('key', 'Doctor id is required').notEmpty();
	var errors = req.validationErrors();

	var item = req.body;
	if(item.schedule) {

		var schedule = item.schedule;
		item.schedule = [];
		for(var i=0;i<schedule.length;i++) {
			var timing = {};

			timing.starthrs = schedule[i].starthrs;
			timing.endhrs = schedule[i].endhrs;
			timing.start_meridian = schedule[i].start_meridian;
			timing.end_meridian = schedule[i].end_meridian;
			timing.startmins = schedule[i].startmins;
			timing.endmins = schedule[i].endmins;

			for(var j=0; j<schedule[i].days.length; j++) {
				var index = _.findIndex(item.schedule, function(o) { return o.day == schedule[i].days[j].name; });
				var obj = {};
				if(index == -1) {
					obj.day = schedule[i].days[j].name;
					obj.timings = [];
					obj.timings.push(timing);;
					item.schedule.push(obj);
				} else {
					item.schedule[index].timings.push(timing);
				}
			}
		}
	}
	if(errors) {
		return res.status(500).send(errors);
	} else {

		Doctor.findOne({'_id': req.params.key}, function(err, doctor) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
				else {
					var origdoctorname = doctor.name;
					
					Doctor.findOneAndUpdate({'_id': req.params.key}, item, function(err, doctor1) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
												//Create OPD-Consultation service for doctor
					var service = {
							'description' : doctor1.name,
							'price' : doctor1.consultation_fee,
							'category' : 'Hospital-OPD-Consultation'
					};

					FacilityPricing.findOneAndUpdate( {'description': origdoctorname,'category': service.category}, service).exec(function(err, facility_pricing) {
						if (err) {
							console.log(err);
							/*return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});*/
						} else {
							//res.json(facility_pricing);
							console.log('facility pricing created');
						}
					});

					//Create facility details if doctor has a percentage
					if(doctor.percentage_fee) {
						var serviceDetails = {
									description : doctor1.name,
									category : 'Hospital-OPD-Consultation',
									expenses : [],
									shares : [{
												doctorId : doctor._id,
												doctorName : doctor1.name,
												commissionPerc : doctor1.percentage_fee
											}]
							};

						FacilityDetails.findOneAndUpdate( {'description':origdoctorname,'category':serviceDetails.category}, serviceDetails, {upsert: true}).exec(function(err, facility_details) {
							if (err) {
								console.log(err);

							} else {
								console.log('facility_details created');
							}
						});
				}
				res.status(200).send(doctor);
						}
					});

			}
		});
	}
};

/**
 * Delete a doctor
 */
exports.delete = function(req, res) {
	Doctor.remove({'_id':req.params.key} , function(err,doctor) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.sendStatus(200).send(doctor);
		}
	});
};

exports.get_doctor_names = function(req, res) {
	Doctor.find({}, {name:1, _id:1}).exec(function(err,doctor) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(doctor);
		}
	});
};


var keywordSearch = function(req, res) {
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');
	Doctor.find({'name':{$regex : regex}},{_id:1,name:1,speciality:1}).limit(10).exec(function(err, doctors) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.name.indexOf(keyword) < b.name.indexOf(keyword))
			    return -1;
			  if (a.name.indexOf(keyword) > b.name.indexOf(keyword))
			    return 1;
			  return 0;
			};

			doctors.sort(compare);
			res.status(200).send(doctors);
		}
	});
};

exports.search = function(req, res) {
	keywordSearch(req, res);
};
