'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	RadiologyTest = mongoose.model('RadiologyTest'),
    _ = require('lodash');

/**
 * Create a Radiology Test
 */
exports.create = function(req, res) {


	req.checkBody('title', 'Test title is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var radiologyTest = new RadiologyTest(req.body);
		radiologyTest.save(function (err, radtest) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(radiologyTest);
			}
		});
	}
};

exports.read = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyTest.findOne({'_id': req.params.labTestId}).exec(function(err, radiologyTest) {
			if (radiologyTest) {
				res.json(radiologyTest);
			} else {
				err = 'Radiology Test not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getByDescAndCategory = function(req, res) {


	if (!req.params.testDescription) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyTest.findOne({'title': req.params.testDescription}).exec(function(err, radiologyTest) {
			if (radiologyTest) {
				res.json(radiologyTest);
			} else {
				err = 'Radiology test not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.update = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			RadiologyTest.findOneAndUpdate({'_id': req.params.labTestId}, req.body).exec(function (err, radiologyTest) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(radiologyTest);
				}
			});
		}
	}
};

exports.delete = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyTest.remove({'_id': req.params.labTestId}).exec(function (err, radiologyTest) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(radiologyTest);
			}
		});
	}
};

exports.list = function(req, res) {
	RadiologyTest.find().exec(function(err, radiologyTest) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(radiologyTest);
		}
	});
};
