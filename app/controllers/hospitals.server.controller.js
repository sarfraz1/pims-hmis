'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Hospital = mongoose.model('Hospital'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Hospital
 */
exports.create = function(req, res) {
	var hospital = new Hospital(req.body);
	hospital.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(hospital);
		}
	});
};

/**
 * Show the current Hospital
 */
exports.read = function(req, res) {
	Hospital.findOne({'_id': req.params.hospitalId}).exec(function(err, hospital) {
		if (hospital) {
			res.json(hospital);
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Hospital
 */
exports.update = function(req, res) {
	Hospital.findOneAndUpdate({'_id': req.params.hospitalId}, req.body).exec(function(err, hospital) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(hospital);
		}
	});
};

/**
 * Delete an Hospital
 */
exports.delete = function(req, res) {
	Hospital.remove({'_id': req.params.hospitalId}, function(err, hospital) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(hospital);
		}
	});
};

/**
 * List of Hospitals
 */
exports.list = function(req, res) {
	Hospital.find().exec(function(err, hospitals) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(hospitals);
		}
	});
};