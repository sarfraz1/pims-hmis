'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	StockTransfer = mongoose.model('StockTransfer'),
	InventoryStock = mongoose.model('InventoryStock'),
	InventoryPricing = mongoose.model('InventoryPricing'),
	Inventory = mongoose.model('Inventory'),
	async = require('async'),
	_ = require('lodash');

var dateConverter = function(dateinput){
	try{
		var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
		utcDate = utcDate.toUTCString();
		return utcDate;
	}
	catch(error){
		return dateinput;
	}
};

exports.create = function(req, res) {
    
	var stocktransfer = new StockTransfer(req.body);
	StockTransfer.findOne().sort({_id:-1}).exec(function(err, transferObj) {
		if(true){
			if(transferObj){
				var code = Number(transferObj.transferNumber)+1;
				code = String(code);
				
				stocktransfer.transferNumber = code;
			} else if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} 
			else {
				stocktransfer.transferNumber = '10000001';
			}
			console.log(stocktransfer);
			async.forEach(stocktransfer.itemsList, function(med, callback) {
				InventoryStock.findOne({'id': med.code}).exec(function (err, stock) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
							callback();
						} else if(stock) {
							var remtransfer = med.quantity;
							
							if(stocktransfer.fromStoreDescription !== 'Stock Adjustment') {
						
								for(var i=0;i<stock.batch.length;i++) {
									if(stock.batch[i].storeId == stocktransfer.fromStoreId && stock.batch[i].quantity > 0) {
										console.log('in if 1');
										var currtransfer = 0; 
										if(stock.batch[i].quantity < remtransfer) {
											console.log(stock.batch[i].quantity);
											console.log(stock.batch[i].remtransfer);
											currtransfer = stock.batch[i].quantity;
											stock.batch[i].quantity = 0;
											console.log('in if 2');
											console.log('in if 2 currtransfer: '+currtransfer);
										} else {
											currtransfer = remtransfer;
											console.log('in if 3 remtransfer: '+remtransfer);
											stock.batch[i].quantity -= currtransfer;
											console.log(stock.batch[i].quantity);
										}
										
										remtransfer = remtransfer - currtransfer;
										console.log('outer remtransfer: '+remtransfer);
										var found = false;
										
										for(var j=0;j<stock.batch.length;j++) {
											if(stock.batch[j].batchID == stock.batch[i].batchID && stock.batch[j].storeId == stocktransfer.toStoreId ) {
												found = true;
												console.log('found true: '+stock.batch[j]);
												stock.batch[j].quantity += currtransfer;
												break;
											}
										}
										
										if(found == false) {
											console.log('found false: ');
											stock.batch.push({  expiryDate: stock.batch[i].expiryDate,
																storeDescription: stocktransfer.toStoreDescription,
																storeId: stocktransfer.toStoreId,
																grnNumber: stock.batch[i].grnNumber,
																enteredDate: new Date(),
																quantity: currtransfer,
																batchID: stock.batch[i].batchID});
										}
										
										if(remtransfer <= 0) {
											console.log('breaking');
											break;
										}
										
									} 	
								}
							} else {
												
								var currtransfer = remtransfer; 
								remtransfer = remtransfer - currtransfer;
								var found = false;
								for(var j=stock.batch.length-1;j>=0;j--) {
									if(stock.batch[j].storeId == stocktransfer.toStoreId) {
										stock.batch[j].quantity += currtransfer;
										found = true;
										break;
									}
								}
								
								if(found == false) {
									stock.batch.push({  expiryDate: new Date('2020-01-01'),
														storeDescription: stocktransfer.toStoreDescription,
														storeId: stocktransfer.toStoreId,
														grnNumber: '111',
														enteredDate: new Date(),
														quantity: currtransfer,
									batchID: '0'});
								};
			
							}
							console.log('saving: '+stock);
							stock.save(function(err,data) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									callback();
								}
							});
						}
				});
			}, function(err) {
				if (err) return next(err);
				
				stocktransfer.save(function(err,data) {
					if (err) {
						console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(data);
					}
				});
			});
			

			
		}
		else {
			// Code for generating Number on category Basis with 
		}
	});   

    
};


/**
 * List GRNs
 */

exports.list = function(req, res) { 
	StockTransfer.find({}).lean().exec(function(err, grns) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(grns);
		}
	});
};

exports.listAll = function(req, res) { 
/*	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = GRN.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});*/
};

exports.update = function(req, res) {
/*	var newList = req.body.itemsList;
	var GRNData = req.body;
    GRN.findOne({'GRNNumber':req.body.GRNNumber}).exec(function(err, data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var dumyData = JSON.parse(JSON.stringify(req.body.itemsList));
			var tempList = itemslistUpdatedInfo(dumyData,data.itemsList);
			
			checkData(tempList,0,function(err){
		    	if(err){
					return res.status(400).send({
						message: err
					});
				}else{
					updatePurchaseOrder(tempList,0);
				
					GRN.findOneAndUpdate({'GRNNumber':req.body.GRNNumber},GRNData).exec(function(err,grnSaveResponse){
						if(err){
							console.log(err);
						}
						else {
							var originalList = data.itemsList;
							var stockToDelete = [];
							var deleted = true;
							for (var i = 0; i < originalList.length; i++) {
								deleted = true;
								for (var j = 0; j < newList.length; j++) {
									if (originalList[i].code === newList[j].code) {
										deleted = false;
										break;
									}
								}
								if (deleted === true) {
									stockToDelete.push({'code': originalList[i].code});
								}
							}
							if (stockToDelete.length > 0) {
								deleteInventoryStockByGRN(stockToDelete, 0, req.body.GRNNumber);
							}
							res.json(grnSaveResponse);
							updateInventoryStockByGRN(tempList, 0, req.body.GRNNumber,req.body.storeId,req.body.storeDescription,req.body.supplierId);
						}
					});
						
				}
			});
		}
	
	});*/
	
};

/**
 * Get GRN by Number
 */
exports.getByNumber = function(req, res) {
/*	GRN.findOne({'GRNNumber': req.params.number}).lean().exec(function (err, grn) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
		//	res.json(grn);
			filterStock(grn, req, res)
		}
	});*/
};


/**
 * Delete GRN
 */
exports.delete = function(req, res) {
/*	GRN.findOne({'_id': req.params.GRNId}).exec(function (err, response) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var itemsList = response.itemsList;
			var grnNumber = response.GRNNumber;
			GRN.remove({'_id': req.params.GRNId}, function(err, grnObj) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var stockToDelete = [];
					for (var i = 0; i < itemsList.length; i++) {
						stockToDelete.push({'code': itemsList[i].code});
					}
					deleteInventoryStockByGRN(stockToDelete, 0, grnNumber);
					res.json(grnObj);
				}
			});
		}
	});*/
};

exports.getReport = function(req, res) {
	
	var fromDate = req.params.fromdate;
	var toDate = req.params.todate;
		
	toDate = new Date(toDate);
	toDate.setDate(toDate.getDate() + 1);
	toDate = dateConverter(toDate);
	
	var query;
	query =  StockTransfer.find({'date':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
		

	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			
			res.json(invoices);
		}
	});
};

exports.getReportwithValue = function(req, res) {
	
	var fromDate = req.params.fromdate;
	var toDate = req.params.todate;
		
	toDate = new Date(toDate);
	toDate.setDate(toDate.getDate() + 1);
	toDate = dateConverter(toDate);
	
	var query;
	query =  StockTransfer.find({'date':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}, $or: [{"fromStoreDescription": 'Stock Adjustment'}, {"toStoreDescription": 'Stock Adjustment'}]}).lean();
	query.exec(function(err, transfers) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			async.forEach(transfers, function(transfer, callback) {
				//var avgprice = getAveragePrice(inventory.batch, inventory.id);
					async.forEach(transfer.itemsList, function(item, callback1) {
						if(item.netValue) {
							callback1();
						} else {
							InventoryPricing.findOne({'inventoryCode': item.code, 'effectiveDate': {$lte : new Date()}}).sort({'effectiveDate': -1}).exec(function(err, pricing) {
								if(transfer.fromStoreDescription === 'Stock Adjustment') {
									Inventory.findOne({'code': item.code}).exec(function(err, inventory) {
										if(inventory) {
											item.netValue = Number((pricing.purchasingPrice/inventory.storage_to_selling).toFixed(2))*item.quantity;
										} else {
											item.netValue = 0;
										}
										callback1();
									});
								} else {
									if (pricing) {
										item.netValue = Number((pricing.sellingPrice*item.quantity).toFixed(2));
									} else {
										item.netValue = 0;
									}
									callback1();
								}
							});
						}
					}, function(err) {
						callback();
					});
				
			}, function(err) {

				res.json(transfers);
			});		
			
			
			//res.json(invoices);
		}
	});
};


var searchByNumber = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	StockTransfer.find({ $or: [{'GRNNumber':{$regex : regex}}, {'supplierDescription':{$regex : regex}}, {'supplierChalanNumber':{$regex : regex}} ]},{GRNNumber:1,supplierDescription:1,supplierChalanNumber:1,date:1,remarks:1}).lean().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			/*var compare = function(a, b) {
				if (a.GRNNumber.indexOf(keyword) < b.GRNNumber.indexOf(keyword))
					return -1;
				if (a.GRNNumber.indexOf(keyword) > b.GRNNumber.indexOf(keyword))
					return 1;
				return 0;
			};
			data.sort(compare);*/
			//filterStock(data, req, res);
			res.json(data);
		}
	});
};

var searchAll = function(req, res) {
	/*GRN.find().exec(function (err, data) {
		if (err) {
			return res.status(400).send({
				message:errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});*/
};

exports.search = function(req, res) {
	if (req.params.searchType === 'GRNNumber') {
		searchByNumber(req, res);
	} else if (req.params.searchType === 'all') {
		searchAll(req, res);
	}
};

/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
