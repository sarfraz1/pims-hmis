'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	PanelLab = mongoose.model('PanelLab'),
    _ = require('lodash');

/**
 * Create a Panel lab
 */
exports.create = function(req, res) {
	req.checkBody('description', 'Description is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var panelLab = new PanelLab(req.body);
		panelLab.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(panelLab);
			}
		});
	}
};

/**
 * Show the current Panel lab
 */
exports.read = function(req, res) {
	if (!req.params.panelLabID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		PanelLab.findOne({'_id': req.params.panelLabID}).exec(function(err, panelLab) {
			if (panel) {
				res.json(panelLab);
			} else {
				err = 'Panel Lab not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

/**
 * Update a Panel lab
 */
exports.update = function(req, res) {
	if (!req.params.panelLabID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		req.checkBody('description', 'Description is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			PanelLab.findOneAndUpdate({'_id': req.params.panelLabID}, req.body).exec(function (err, panelLab) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(panelLab);
				}
			});
		}
	}
};

/**
 * Delete an Panel lab
 */
exports.delete = function(req, res) {
	if (!req.params.panelLabID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		PanelLab.remove({'_id': req.params.panelLabID}).exec(function (err, panelLab) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(panelLab);
			}
		});
	}
};

/**
 * List of Panel labs
 */
exports.list = function(req, res) {
	PanelLab.find().exec(function(err, panelLabs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(panelLabs);
		}
	});
};