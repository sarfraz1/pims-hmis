'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	LabSetting = mongoose.model('LabSetting'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash');

/**
 * Create a Inventory
 */


exports.create = function(req, res) {
	
    var lab_setting = req.body;
    LabSetting.findOneAndUpdate({'hospital_name': req.body.hospital_name}, lab_setting, {upsert: true}, function(err, labSetting) {
        if (err) {
            return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
        }
        else {
        	console.log(labSetting);
        	res.json(labSetting);
        }
    });
};


exports.createImage = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {

        req.body = body;
        
        if(body.file){

            var picPath = body.imageLoc[0];
            
            var temp = body.file[0];
            temp = temp.split(',');
            
            var b64string = temp[1];
			var buf = new Buffer(b64string, 'base64');
            fs.writeFile(picPath , buf , function (err , success) {
                if(err){
                    return res.status(400).send({
                        'error': err
                    });
                }
                else {
					res.json('saved');
				}
            });
        }
        
	});
};


exports.read = function(req, res) {
	LabSetting.findOne().exec(function(err, labSetting) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(labSetting);
		}
		
	});
};

/**
 * Update a Inventory
 */
exports.update = function(req, res) {
	delete req.body.__v;
	var item = req.body;
	Inventory.findOneAndUpdate({'code': req.params.inventoryId}, item, {upsert: true}, function(err, inventory) {
        if (err) {
            return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
        }
        else {
        	res.json(inventory);
        }
    });
};

/**
 * Delete an Inventory
 */
exports.delete = function(req, res) {
	Inventory.remove({'code':req.params.inventoryId} , function(err,inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			InventoryPricing.remove({'inventoryID': req.params.inventoryId}, function(err, pricing) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventory);
				}
			});
		}
	});
};





/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

