'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	PatientArea = mongoose.model('PatientArea'),
    _ = require('lodash');

/**
 * Create an Area
 */
exports.create = function(req, res) {
	req.checkBody('description', 'Description is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var area = new PatientArea(req.body);
		area.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(area);
			}
		});
	}
};

/**
 * Show the current area
 */
exports.read = function(req, res) {
	if (!req.params.specialityID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		PatientArea.findOne({'_id': req.params.areaID}).exec(function(err, area) {
		if (area) {
			res.json(area);
		} else {
			err = 'Area not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
	}
};

/**
 * Update an area
 */
exports.update = function(req, res) {
	if (!req.params.areaID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		req.checkBody('description', 'Description is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			PatientArea.findOneAndUpdate({'_id': req.params.areaID}, req.body).exec(function (err, area) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(area);
				}
			});
		}
	}
};

/**
 * Delete an area
 */
exports.delete = function(req, res) {
	if (!req.params.areaID) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		PatientArea.remove({'_id': req.params.areaID}).exec(function (err, area) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(area);
			}
		});
	}
};

/**
 * List of Areas
 */
exports.list = function(req, res) {
	PatientArea.find().exec(function(err, areas) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(areas);
		}
	});
};