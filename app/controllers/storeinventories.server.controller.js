'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Inventory = mongoose.model('StoreInventory'),
	InventoryPricing = mongoose.model('StoreInventoryPricing'),
	InventoryStock = mongoose.model('StoreInventoryStock'),
	PurchaseRequisition = mongoose.model('StorepurchaseRequisition'),
	PurchaseOrder = mongoose.model('StorepurchaseOrder'),
	PurchaseReturn = mongoose.model('StorepurchaseReturn'),
	GRN = mongoose.model('StoreGRN'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash'),
	isPharmacy = false;

/**
 * Create a Inventory
 */


exports.create = function(req, res) {
	
    var inventory = new Inventory(req.body); 

	Inventory.findOne().sort({_id:-1}).exec(function(err, grnObj) {
		if(grnObj){
			var code = Number(grnObj.code)+1;
			code = String(code);
			
			inventory.code = code;

		} else if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} 
		else {
			inventory.code = '10000001';
		}
		inventory.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				res.json(inventory);
			}
		});

	});
};

exports.createImage = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {

        req.body = body;
        
        if(body.file){

            var picPath = body.imageLoc[0];
            
            var temp = body.file[0];
            temp = temp.split(',');
            
            var b64string = temp[1];
			var buf = new Buffer(b64string, 'base64');
            fs.writeFile(picPath , buf , function (err , success) {
                if(err){
                    return res.status(400).send({
                        'error': err
                    });
                }
                else {
					res.json('saved');
				}
            });
        }
        
	});
};

exports.getItemByDescription = function(req, res) {

	Inventory.findOne({'description': req.params.description}).exec(function(err, inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventory);
		}
	});
};

/**
 * Show the current Inventory
 */

exports.getItemCode = function(req, res) {
	
	Inventory.findOne().sort({_id:-1}).limit(1).exec(function(err, inventory) {
		if(inventory){
			var code = Number(inventory.code)+1;
			code = String(code);
			
			// var pad =function (n) {
			// 	var zeros = '0';
			// 	while(0<n-1){ n--; zeros = zeros+'0'; }
			// 	return zeros;
			// };
			//code = pad(7-code.length) + code;
			
			res.json(code);
		
		} else if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} 
		else {
			res.json('10000001');
		}
		
	});
    
};


exports.read = function(req, res) {
	Inventory.findOne({'code': req.params.inventoryId}).exec(function(err, inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventory);
		}
		
	});
};

/**
 * Update a Inventory
 */
exports.update = function(req, res) {
	delete req.body.__v;
	var item = req.body;
	Inventory.findOneAndUpdate({'code': req.params.inventoryId}, item, {upsert: true}, function(err, inventory) {
        if (err) {
            return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
        }
        else {
        	res.json(inventory);
        }
    });
};

/**
 * Delete an Inventory
 */
exports.delete = function(req, res) {
	Inventory.remove({'code':req.params.inventoryId} , function(err,inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			InventoryPricing.remove({'inventoryID': req.params.inventoryId}, function(err, pricing) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventory);
				}
			});
		}
	});
};

/**
 * List of Inventories
 */
exports.list = function(req, res) { 
	Inventory.find().exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};


var keywordSearch = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');
	
	var query = '';
	if(isPharmacy)
		query = {'description':{$regex : regex},'grn_created':true};
	else 
		query = {'description':{$regex : regex}};
	Inventory.find(query,'-__v').limit(20).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
			    return -1;
			  if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
			    return 1;
			  return 0;
			};
			
			inventories.sort(compare);

			res.json(inventories);
		}
	});
};

var getByBarcode = function(req, res){
	Inventory.findOne({'barcode':req.params.keyword}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};

var getByCode = function(req, res){
	Inventory.findOne({'code':req.params.keyword}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};

exports.getItem = function(req, res) { 
	if(req.params.searchType==='code'){	
		getByCode(req, res);
	}
	else if(req.params.searchType==='barcode'){
		getByBarcode(req, res);
	}
	else if(req.params.searchType==='description'){
		keywordSearch(req, res);
	}
};

exports.getItemExpiryStatus = function(req, res) { 
	Inventory.findOne({'code':req.params.code}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories.expiry_date_enabled);
		}
	});
	
};

exports.getItemByDescription = function(req, res) { 
	Inventory.findOne({'description':req.params.description}).exec(function(err, item) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(item);
		}
	});
	
};



/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

/**
 * Get all details related to inventory item
 */
exports.getDetails = function(req, res) {
	var itemDetails = {};
	var itemCode = req.params.code;
	itemDetails.documents = [];
	Inventory.findOne({'code': req.params.code}).exec(function(err, item) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			itemDetails.code = item.code;
			itemDetails.description = item.description;
			PurchaseRequisition.find({}).exec(function(err, purchaseRequisitions) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					for (var i = 0; i < purchaseRequisitions.length; i++) {
						for (var j = 0; j < purchaseRequisitions[i].itemsList.length; j++) {
							if (purchaseRequisitions[i].itemsList[j].code === itemCode) {
								if (purchaseRequisitions[i].status === 'Accepted') {
									itemDetails.documents.push({
										quantity: purchaseRequisitions[i].itemsList[j].quantity,
										rate: purchaseRequisitions[i].itemsList[j].estRate,
										created: purchaseRequisitions[i].created,
										netValue: purchaseRequisitions[i].itemsList[j].quantity * purchaseRequisitions[i].itemsList[j].estRate,
										code: purchaseRequisitions[i].code,
										documentType: 'Purchase Requisition'
									});
								}
							}
						}
					}
					PurchaseOrder.find({}).exec(function(err, purchaseOrders) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							for (var i = 0; i < purchaseOrders.length; i++) {
								for (var j = 0; j < purchaseOrders[i].itemsList.length; j++) {
									if (purchaseOrders[i].itemsList[j].code === itemCode) {
										itemDetails.documents.push({
											netValue: purchaseOrders[i].itemsList[j].netValue,
											rate: purchaseOrders[i].itemsList[j].rate,
											quantity: purchaseOrders[i].itemsList[j].quantity,
											created: purchaseOrders[i].created,
											code: purchaseOrders[i].purchaseOrderNumber,
											documentType: 'Purchase Order'
										});
									}
								}
							}
							GRN.find({}).exec(function(err, grns) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									for (var i = 0; i < grns.length; i++) {
										for (var j = 0; j < grns[i].itemsList.length; j++) {
											if (grns[i].itemsList[j].code === itemCode) {
												itemDetails.documents.push({
													netValue: grns[i].itemsList[j].netValue,
													quantity: grns[i].itemsList[j].quantity,
													rate: grns[i].itemsList[j].rate,
													created: grns[i].date,
													code: grns[i].GRNNumber,
													documentType: 'GRN'
												});
											}
										}
									}
									PurchaseReturn.find({}).exec(function (err, purchaseReturns) {
										if (err) {
											return res.status(400).send({
												message: errorHandler.getErrorMessage(err)
											});
										} else {
											for (var i = 0; i < purchaseReturns.length; i++) {
												for (var j = 0; j < purchaseReturns[i].itemsList.length; j++) {
													if (purchaseReturns[i].itemsList[j].code === itemCode) {
														itemDetails.documents.push({
															rate: purchaseReturns[i].itemsList[j].rate,
															quantity: purchaseReturns[i].itemsList[j].quantity,
															netValue: purchaseReturns[i].itemsList[j].netValue,
															created: purchaseReturns[i].date,
															code: purchaseReturns[i].purchaseReturnNumber,
															documentType: 'Purchase Return'
														})
													}
												}
											}
											InventoryStock.findOne({'id': itemCode}).exec(function (err, stock) {
												if (err) {
													return res.status(400).send({
														message: errorHandler.getErrorMessage(err)
													});
												} else {
													var inStock = 0;
													if (stock) {
														for (var i = 0; i < stock.batch.length; i++) {
															inStock = inStock + stock.batch[i].quantity;
														}
													}
													itemDetails.inStock = inStock;
													res.json(itemDetails);
												}
											});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
};
