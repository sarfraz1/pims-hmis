'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Medicine = mongoose.model('Medicine'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var medicine = new Medicine(req.body);
	medicine.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * Show the current Medicine
 */
exports.read = function(req, res) {
	Medicine.findOne({'_id': req.params.medicineId}).exec(function(err, medicine) {
		if (medicine) {
			res.json(medicine);
		} else {
			err = 'Medicine not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Medicine
 */
exports.update = function(req, res) {
	Medicine.findOneAndUpdate({'_id': req.params.medicineId}, req.body).exec(function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * Delete a Medicine
 */
exports.delete = function(req, res) {
	Medicine.remove({'_id': req.params.medicineId}, function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * List of Medicines
 */
exports.list = function(req, res) {
	Medicine.find().exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(medicines);
		}
	});
};

/**
 * Retrieve array of Medicines
 * To Do: Merge both search functions into one if functionality remains the same
 */
var searchByFormula = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Medicine.find({'formula':{$regex : regex}},'-__v').exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.formula.indexOf(keyword) < b.formula.indexOf(keyword))
					return -1;
				if (a.formula.indexOf(keyword) > b.formula.indexOf(keyword))
					return 1;
				return 0;
			};
			medicines.sort(compare);
			res.json(medicines);
		}
	});
};

var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Medicine.find({'tradeName.description':{$regex : regex}},'-__v').exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.tradeName.indexOf(keyword) < b.tradeName.indexOf(keyword))
					return -1;
				if (a.tradeName.indexOf(keyword) > b.tradeName.indexOf(keyword))
					return 1;
				return 0;
			};
			medicines.sort(compare);
			res.json(medicines);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'name'){	
		searchByName(req, res);
	}
	else if (req.params.searchType === 'formula'){
		searchByFormula(req, res);
	}
};