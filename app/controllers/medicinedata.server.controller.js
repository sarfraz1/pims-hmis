'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	MedicineData = mongoose.model('MedicineData'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var medicine = new MedicineData(req.body);
	medicine.save(function (err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * Show the current Medicine
 */
exports.read = function(req, res) {
	MedicineData.findOne({'_id': req.params.medicineId}).exec(function(err, medicine) {
		if (medicine) {
			res.json(medicine);
		} else {
			err = 'Medicine not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Medicine
 */
exports.update = function(req, res) {
	MedicineData.findOneAndUpdate({'_id': req.params.medicineId}, req.body).exec(function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * Delete a Medicine
 */
exports.delete = function(req, res) {
	MedicineData.remove({'_id': req.params.medicineId}, function(err, medicine) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(medicine);
		}
	});
};

/**
 * List of Medicines
 */
exports.list = function(req, res) {
	MedicineData.find().exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(medicines);
		}
	});
};

var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	MedicineData.find({'brandname':{$regex : regex}},'-__v').exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			
			var dataToSort = [];
            for(var i = 0 ; i < medicines.length; i++){
              var temp = medicines[i];
              temp.rank = medicines[i].brandname.toLowerCase().indexOf(keyword.toLowerCase());
              dataToSort.push(temp);
            }
            dataToSort.sort(function(a, b) {
              return Number(a.rank) - Number(b.rank);
            });
            
            res.json(dataToSort.slice(0,10));
		}
	});
};

exports.search = function(req, res) {
	searchByName(req, res);
};

exports.getbrandnames = function(req, res) {
	var brandname = req.params.brandname;
	MedicineData.findOne({'alternatebrands':brandname},'-__v').exec(function(err, medicines) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if(medicines) {
            
            res.json(medicines.alternatebrands);
			}
			else {
				res.json(0);
			}
		}
	});
};


