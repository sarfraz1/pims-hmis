'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Nursenotes = mongoose.model('dentalNurseNotes'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

exports.create = function(req, res) {
	var nursenotes = new Nursenotes(req.body);
	nursenotes.save(function (err,data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.getReasons = function(req, res) {
	var regex = new RegExp(req.params.reason, "i");
    var query;
    if(req.params.reason!=='All') {
        query = Nursenotes.distinct('reason',{reason:{$regex : regex}});
    } else {
    	req.params.reason = '';
    	query = Nursenotes.distinct('reason',{});
    }
    query.exec(function(err , reasons){
        if(err){return next(err);} else {
			if(req.params.reason) {
			  var dataToSort = [];
			  for(var i = 0 ; i < reasons.length; i++){
				var temp = {};
				temp.rank = reasons[i].toLowerCase().indexOf(req.params.reason.toLowerCase());
				temp.val = reasons[i];
				dataToSort.push(temp);
			  }
			  dataToSort.sort(function(a, b) {
				return Number(a.rank) - Number(b.rank);
			  });
			  dataToSort = dataToSort.slice(0, 5);
			  res.json(dataToSort);
			} else {
				var dataToSort = [];
				for(var i = 0 ; i < reasons.length; i++){
					var temp = {};
					temp.val = reasons[i];
					dataToSort.push(temp);
				}
				//dataToSort.shift();
				res.json(dataToSort);
			}
        }
        
    });
};

/**
 * List Prescriptions
 */
exports.list = function(req, res) {
	Nursenotes.find().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * List Prescriptions
 */
exports.getbyMrNo = function(req, res) {
	Nursenotes.find({MRNumber: req.params.mr_number}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * List Prescriptions not handled by Pharmacy
 */
exports.getAppointmentDentalNurseNotes = function(req, res) {
	Nursenotes.findOne({appintmentId: req.params.appintment_id}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.updateAppointmentDentalNurseNotes = function(req, res) {
	Nursenotes.findOneAndUpdate({appintmentId: req.params.appintment_id}, req.body, function(err, notes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else {
			res.status(200).send(notes);
		}
	});		
};