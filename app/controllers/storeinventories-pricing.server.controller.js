'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryPricing = mongoose.model('StoreInventoryPricing'),
	Inventory = mongoose.model('StoreInventory'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create an Inventories pricing
 */
exports.create = function(req, res) {
	
	Inventory.findOne({'code': req.body.inventoryCode}).exec(function(err, inventory) {
		if (inventory) {
			delete req.body.__v;
			delete req.body._id;
			
			InventoryPricing.findOneAndUpdate({'inventoryCode': req.body.inventoryCode,'effectiveDate': req.body.effectiveDate},req.body , {upsert: true},
				function (err,inventorypricing) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventorypricing);
				}
			});
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Show the current Inventories pricing
 */
exports.read = function(req, res) {
	InventoryPricing.findOne({'inventoryCode': req.params.inventoryCode, 'effectiveDate': {$lte : new Date()}}).sort({'effectiveDate': -1}).exec(function(err, pricing) {
		if (pricing) {
			res.json(pricing);
		} else {
			err = 'Inventory pricing not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Show the current Inventories pricing with inventory details
 */
exports.readpos = function(req, res) {
	//InventoryPricing.findOne({'inventoryCode': req.params.inventoryCode, 'effectiveDate': {$lte : new Date()}}).sort({'effectiveDate': -1}).exec(function(err, pricing) {
	InventoryPricing.aggregate([
		{ "$match": {'inventoryCode': req.params.inventoryCode, 'effectiveDate': {$lte : new Date()}}},
		
		{ "$lookup": {
            from: "inventories",
            localField: "inventoryCode",
            foreignField: "code",
            as: "aggre"
        }
	}]).sort({'effectiveDate': -1}).exec(function(err, pricing) {
	if (pricing) {
			res.json(pricing);
		} else {
			err = 'Inventory pricing not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories pricing
 */
exports.update = function(req, res) {
	delete req.body.__v;
	InventoryPricing.findOneAndUpdate({'inventoryCode': req.params.inventoryCode}, req.body,{upsert:true}).exec(function(err, pricing){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(pricing);
		}
	});
};

/**
 * Delete an Inventories pricing
 */
exports.delete = function(req, res) {
	InventoryPricing.remove({'inventoryCode': req.params.inventoryCode}, function(err, pricing) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(pricing);
		}
	});
};

/**
 * List of Inventories pricings
 */
exports.list = function(req, res) {
	InventoryPricing.find().exec(function(err, inventoryPricings) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(inventoryPricings);
		}
	});
};

/**
 * List of Inventory Pricings against Suppliers
 */
exports.listSupplierInventory = function(req, res) {
	InventoryPricing.find({'supplierId': req.params.supplierId}).exec(function(err, inventoryPricings) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(inventoryPricings);
		}
	})
};

/**
 * List of Inventory Pricings against Suppliers
 */
exports.listSupplierInventorywithStock = function(req, res) {
	InventoryPricing.find({'supplierId': req.params.supplierId})
	InventoryPricing.aggregate([
	{ "$match": {'supplierId': req.params.supplierId}},
	
	{ "$lookup": {
		from: "inventorystocks",
		localField: "inventoryCode",
		foreignField: "id",
		as: "stock"
	}
	}]).exec(function(err, inventoryPricings) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(inventoryPricings);
		}
	})
};

