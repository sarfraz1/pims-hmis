'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	purchaseOrder = mongoose.model('purchaseOrder'),
	purchaseRequisition = mongoose.model('purchaseRequisition'),
	

	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash');

/**
 * Create a Inventory
 */


var checkData = function(itemsList,i,callback){
	if(itemsList[i]!==undefined){
		purchaseRequisition.findOne({'code':itemsList[i].purchaseRequisitionCode}).exec(function(err, purchaseRequi) {
			var errorProduced = false;
		
			if(purchaseRequi!==null){
				
				for(var j=0;j<purchaseRequi.itemsList.length;j++){
					if(purchaseRequi.itemsList[j].code===itemsList[i].code){
						purchaseRequi.itemsList[j].handledQty = purchaseRequi.itemsList[j].handledQty + itemsList[i].quantity;
						if(purchaseRequi.itemsList[j].quantity>=itemsList[i].quantity)
							purchaseRequi.itemsList[j].quantity = purchaseRequi.itemsList[j].quantity - itemsList[i].quantity;
						else{
							callback(purchaseRequi.itemsList[j].description+' quantity is greater than approved quantity.');
							errorProduced = true;
						}
					}
					if(purchaseRequi.itemsList[j].quantity === 0){
						purchaseRequi.itemsList[j].completed = true;
					}
				}	
			}
			if(errorProduced === false){
				i++;
				checkData(itemsList,i,callback);
			}
		});
		
	}else {
		callback(null);
	}
};

var updatePurchaseRequisition = function(itemsList,i){
	//console.log(itemsList[i].purchaseRequisitionCode);
	if(itemsList[i]!==undefined){
		purchaseRequisition.findOne({'code':itemsList[i].purchaseRequisitionCode}).exec(function(err, purchaseRequi) {
			if(purchaseRequi!==null){
				for(var j=0;j<purchaseRequi.itemsList.length;j++){
					if(purchaseRequi.itemsList[j].code===itemsList[i].code){
						purchaseRequi.itemsList[j].handledQty = purchaseRequi.itemsList[j].handledQty + itemsList[i].quantity;
						if(purchaseRequi.itemsList[j].quantity>=itemsList[i].quantity)
							purchaseRequi.itemsList[j].quantity = purchaseRequi.itemsList[j].quantity - itemsList[i].quantity;
					}
					if(purchaseRequi.itemsList[j].quantity === 0){
						purchaseRequi.itemsList[j].completed = true;
					}
				}
				purchaseRequi.save(function(err,data) {
					//console.log(data);
				
					i++;
					updatePurchaseRequisition(itemsList,i);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};

exports.create = function(req, res) {
	var PurchaseOrder = new purchaseOrder(req.body);
	//console.log(req.body.itemsList);

	checkData(req.body.itemsList,0,function(err){
		if(err){
			return res.status(400).send({
				message: err
			});
		}else{
			updatePurchaseRequisition(req.body.itemsList,0);
	
			purchaseOrder.findOne({'transcationCategory':req.body.transcationCategory}).sort({purchaseOrderNumber:-1}).exec(function(err, purchaseOrderObj) {
				if(req.body.transcationCategory==='Default'){
					if(purchaseOrderObj){
						var code = Number(purchaseOrderObj.purchaseOrderNumber)+1;
						code = String(code);
						
						PurchaseOrder.purchaseOrderNumber = code;
					} else if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} 
					else {
						PurchaseOrder.purchaseOrderNumber = '10000001';
					}

				 	PurchaseOrder.save(function(err,data) {
					 	if (err) {
					 		console.log(err);
					 		return res.status(400).send({
					 			message: errorHandler.getErrorMessage(err)
					 		});
					 	} else {

					 		res.json(data);
					 	}
					});
				}
				else {
					// Code for generating Number on category Basis with 
				}
				});
		}
	});

	
};

exports.getPurchaseOrderId = function(req, res) {
	
    var purchase_requisition = new purchaseRequisition(req.body);       
    purchase_requisition.save(function(err,data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.updateOrder = function(req, res) {
    purchaseOrder.findOneAndUpdate({'purchaseOrderNumber':req.body.purchaseOrderNumber}, req.body).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

/**
 * Method to retrieve purchase orders that have not been handled yet
 */
exports.list = function(req, res) { 
	purchaseOrder.find({}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var indexes = [];
			var handled = false;
			for (var i = 0; i < data.length; i++) {
				handled = false;
				for (var j = 0; j < data[i].itemsList.length; j++) {
					if (data[i].itemsList[j].completed === true) {
						handled = true;
						break;
					}
				}
				if (handled === false) {
					indexes.push(data[i]);
				}
			}
			res.json(indexes);
		}
	});
};

exports.listAll = function(req, res) { 
	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = purchaseOrder.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

var deletePurchaseRequisition = function(itemsList,i){
	//console.log(itemsList[i].purchaseRequisitionCode);
	if(itemsList[i]!==undefined){
		purchaseRequisition.findOne({'code':itemsList[i].purchaseRequisitionCode}).exec(function(err, purchaseRequi) {
			if(purchaseRequi!==null){
				for(var j=0;j<purchaseRequi.itemsList.length;j++){
					if(purchaseRequi.itemsList[j].code===itemsList[i].code){
						purchaseRequi.itemsList[j].quantity = purchaseRequi.itemsList[j].quantity + itemsList[j].quantity;
						purchaseRequi.itemsList[j].handledQty = purchaseRequi.itemsList[j].handledQty - itemsList[j].quantity;
						// purchaseRequi.itemsList[j].handledQty = purchaseRequi.itemsList[j].handledQty + itemsList[i].quantity;
						// if(purchaseRequi.itemsList[j].quantity>=itemsList[i].quantity)
						// 	purchaseRequi.itemsList[j].quantity = purchaseRequi.itemsList[j].quantity - itemsList[i].quantity;
					}
					//if(purchaseRequi.itemsList[j].quantity === 0){
					purchaseRequi.itemsList[j].completed = false;
					//}
				}
				purchaseRequi.save(function(err,data) {
					//console.log(data);
				
					i++;
					deletePurchaseRequisition(itemsList,i);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};


exports.delete = function(req, res) {
	deletePurchaseRequisition(req.body.itemsList,0);

	purchaseOrder.remove({'purchaseOrderNumber':req.body.purchaseOrderNumber}, function(err, purchaseOrderObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(purchaseOrderObj);
		}
	});
};


/**
 * Retrieve array of Purchase Orders
 */
var searchByNumber = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	purchaseOrder.find({'purchaseOrderNumber':{$regex : regex}},'-__v').exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.purchaseOrderNumber.indexOf(keyword) < b.purchaseOrderNumber.indexOf(keyword))
					return -1;
				if (a.purchaseOrderNumber.indexOf(keyword) > b.purchaseOrderNumber.indexOf(keyword))
					return 1;
				return 0;
			};
			data.sort(compare);
			var indexes = [];
			var handled = false;
			for (var i = 0; i < data.length; i++) {
				handled = false;
				for (var j = 0; j < data[i].itemsList.length; j++) {
					if (data[i].itemsList[j].handledQty > 0) {
						handled = true;
						break;
					}
				}
				if (handled === false) {
					indexes.push(data[i]);
				}
			}
			res.json(indexes);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'number') {
		searchByNumber(req, res);
	}
};

/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
