'use strict';


var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
/**
 * Module dependencies.
 */

var sendEmailMethod = function(msg,html,emailAdress,subject) {
    var transporter = nodemailer.createTransport(
        smtpTransport('smtps://capitalinternationalhospital@gmail.com:abbas1234567@smtp.gmail.com')
    );
    var mailOptions = {
        from: 'TalkHealth.me <talkhealthme@gmail.com>',
        to: emailAdress,
        subject: subject,
        text : msg,
        html: html
    };
    transporter.sendMail(mailOptions, function(error, info){
    });
};

exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req
	});
};

exports.emailHtml = function(req, res) {
	//console.log(req.body);
	var html = req.body.html;
	html = html.replace(/class="ng-hide"/g, 'style="display:none"');
	html = html.replace(/class="ng-binding ng-hide"/g,'style="display:none"');
	sendEmailMethod('Send me the items',html,req.body.emailAddress, req.body.subject);
	res.json('');
};
