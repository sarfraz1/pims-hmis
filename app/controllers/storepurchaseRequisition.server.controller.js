'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	purchaseRequisition = mongoose.model('StorepurchaseRequisition'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash');

/**
 * Create a Inventory
 */
exports.create = function(req, res) {
    var purchase_requisition = new purchaseRequisition(req.body);
    purchaseRequisition.findOne({'transcationCategory':req.body.transcationCategory}, {code:1}).sort({code:-1}).exec(function(err, purchaseRequiObj) {
		if(req.body.transcationCategory==='Default'){
			if(purchaseRequiObj){
				var code = Number(purchaseRequiObj.code)+1;
				code = String(code);
				
				purchase_requisition.code = code;
			} else if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} 
			else {
				purchase_requisition.code = '10000001';
			}

			purchase_requisition.save(function(err,data) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(data);
				}
			});
		} else {
			// Code for generating Number on category Basis with 
		} 
	    
	});
};

exports.list = function(req, res) { 
	purchaseRequisition.find({'itemsList.completed':false,'status':'Accepted'}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.listAll = function(req, res) { 
	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = purchaseRequisition.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.update = function(req, res) {
    purchaseRequisition.findOneAndUpdate({'code':req.body.code},req.body).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.delete = function(req, res) {
	purchaseRequisition.remove({'_id': req.params.purchaseRequisitionId}, function(err, purchaseRequiObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(purchaseRequiObj);
		}
	});
};


/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

/**
 * Purchase Requisition Search Page
 */
var searchByStatus = function(req, res) {
	var keyword = req.params.keyword;
	purchaseRequisition.find({'status': keyword}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
}

var searchByCode = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	purchaseRequisition.find({'code':{$regex : regex}, 'status': 'Pending'},'-__v').exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.code.indexOf(keyword) < b.code.indexOf(keyword))
					return -1;
				if (a.code.indexOf(keyword) > b.code.indexOf(keyword))
					return 1;
				return 0;
			};
			data.sort(compare);
			res.json(data);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'status') {
		searchByStatus(req, res);
	} else if (req.params.searchType === 'code') {
		searchByCode(req, res);
	}
};
