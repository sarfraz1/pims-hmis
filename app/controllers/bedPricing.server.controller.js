'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	BedPricing = mongoose.model('BedPricing'),
	_ = require('lodash'),
	moment = require('moment');


/**
 * Create a new node
 */
 exports.create = function(req, res) {
	var errors = req.validationErrors();

	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {
		console.log(req.body);
		//var facilityPricing = new FacilityPricing(req.body);
		BedPricing.findOneAndUpdate({ $or: [{'_id': req.body._id}, {'bed_no':req.body.bed_no, 'description':req.body.description,'category':req.body.category}] }, req.body, {upsert: true}).exec(function(err, facility_pricing) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(facility_pricing);
			}
		});
	}
};

/**
 * Delete a Node
 */
exports.delete = function(req, res) {
	var regex = new RegExp(req.params.categoryName, 'i');
	console.log(regex)
	BedPricing.remove({'category':regex}, function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.sendStatus(200).send(facility);
		}
	});
};


/**
 * Update a bed record
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('category', 'MR number is required').notEmpty();
	var errors = req.validationErrors();

	var item = req.body;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		BedPricing.findOneAndUpdate({'_id': req.params.category}, item, {upsert: true}, function(err, patient) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(patient);
			}
		});
	}
};

/**
 * Retrieve immediate children of a node
 */
exports.read = function(req, res) {
	var query;
	if(req.params.category) {
		var query;
		query = BedPricing.find({category:req.params.category},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);
			}
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

};

/**
 * Retrieve immediate children of a node
 */
exports.getbynameandcategory = function(req, res) {
	var query;
	if(req.params.category && req.params.service_name) {
		var query;
		query = BedPricing.findOne({category:req.params.category, description:req.params.service_name},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);
			}
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

};

exports.getbedsbycategory = function(req, res) {
	var query;
	if(req.params.category) {
		var query;
		var keyword = req.params.category;
		var regex = new RegExp(keyword, 'i');
		query = BedPricing.find({'category':{$regex : regex}},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);
			}
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

};


/**
 * List of Patients
 */
exports.list = function(req, res) {
	BedPricing.find({}).exec(function(err, facility_pricing) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(facility_pricing);
		}
	});
};
