'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Nursenotes = mongoose.model('Nursenotes'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var nursenotes = new Nursenotes(req.body);
	nursenotes.save(function (err,data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

/**
 * List Prescriptions
 */
exports.list = function(req, res) {
	Nursenotes.find().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * List Prescriptions
 */
exports.getbyMrNo = function(req, res) {
	Nursenotes.find({MRNumber: req.params.mr_number}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * List Prescriptions not handled by Pharmacy
 */
exports.getAppointmentNurseNotes = function(req, res) {
	Nursenotes.findOne({appintmentId: req.params.appintment_id}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.updateAppointmentNurseNotes = function(req, res) {
	Nursenotes.findOneAndUpdate({appintmentId: req.params.appintment_id}, req.body, function(err, notes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		else {
			res.status(200).send(notes);
		}
	});		
};

exports.listNotHandled = function(req, res) {
	Nursenotes.find({handledByPharmacy: false}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

/**
 * Update Prescription from Pharmacy
 */
exports.handledByPharmacy = function(req, res) {
	Prescription.findOneAndUpdate({'_id': req.params.prescriptionid}, {$set: {'handledByPharmacy': true}}).exec(function(err, store){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(store);
		}
	});
};