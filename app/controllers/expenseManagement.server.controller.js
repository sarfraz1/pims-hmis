'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	ExpenseManagement = mongoose.model('ExpenseManagement'),
	ExpenseTags = mongoose.model('ExpenseTags'),
	_ = require('lodash'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	moment = require('moment');


/**
 * Create new Appointment
 */

var saveImage = function(body,pathUrl){
	
	var picPath = pathUrl;
	
	var temp = body.file[0];
	temp = temp.split(',');
	
	var b64string = temp[1];
	var buf = new Buffer(b64string, 'base64');
	fs.writeFile(picPath , buf , function (err , success) {
		if(err){
			return res.status(400).send({
				'error': err
			});
		}
	});
};

var saveTags = function(tagstoadd) {
	ExpenseTags.find().exec(function(err, exptags) {
		if (err) {
			console.log(err);
			return;
		} else if(exptags.tags){
			for(var i=0;i<tagstoadd.length;i++) {
				if(exptags.tags.indexOf(tagstoadd[i]) !== -1) {
					exptags.tags.push(tagstoadd[i]);
				}
			}
			
			exptags.save(function(err, resExpense) {
				if (err) {
					console.log(err);
					return;
				}
			});
		} else {
			var exp = new ExpenseTags();
			exp.tags = tagstoadd;
			
			exp.save(function(err, resExpense) {
				if (err) {
					console.log(err);
					return;
				}
			});
		}
	});
	
};

exports.create = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {
		req.body = {
			expenseNumber : body.expenseNumber[0],
			expenseType : body.expenseType[0],
			description : body.description[0],
			department : body.department[0],
			amount : Number(body.amount[0]),
			tags : JSON.parse(body.tag2[0]),
			fileUrl : '',
			created : body.created[0]
		};

		if(body.repeat[0]==='true'){
			req.body.repeat = true;
		}else {
			req.body.repeat = false;
		}
		
		if(req.body.repeat){
			req.body.repeatNum = Number(body.repeatNum[0]);
			req.body.repeatFrequency = body.repeatFrequency[0];
		}
		
		if(body.fileUrl[0]!=='' && body.fileUrl!==undefined){
			req.body.fileUrl = body.fileUrl[0];
			
		}
		
        ExpenseManagement.findOne().sort({_id:-1}).limit(1).exec(function(err, expenses) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var expenseManagement = new ExpenseManagement(req.body);
				if(expenses) {
					var code = Number(expenses.expenseNumber)+1;
					expenseManagement.expenseNumber = String(code);
					
					ExpenseManagement.findOne({'expenseNumber':req.body.expenseNumber}).sort({_id:-1}).exec(function(err, resExpense) {
						if(err){
							console.log(err);
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							if(resExpense){
								
								resExpense.expenseType = req.body.expenseType;
								resExpense.description = req.body.description;
								resExpense.amount = req.body.amount;
								resExpense.department = req.bod.department;
								resExpense.tags = req.body.tags;
								if(body.file){
									//resExpense.fileFormat = body.fileFormat[0];
									var filePath = './public/images/'+resExpense.expenseNumber+body.fileFormat[0];
									resExpense.fileUrl = filePath;
									
									saveImage(body,filePath);
								}
								//  = req.body.fileUrl;
								resExpense.repeat = req.body.repeat;
								resExpense.repeatNum  = req.body.repeatNum;
								resExpense.repeatFrequency = req.body.repeatFrequency;
								resExpense.created = req.body.created;
							} else {
								resExpense = expenseManagement;
								if(body.file){
									var filePath = './public/images/'+resExpense.expenseNumber+body.fileFormat[0];
									resExpense.fileUrl = filePath;
									saveImage(body,filePath);
								}
							}
							
							resExpense.save(function(err, resExpense) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									res.status(200).send(resExpense);
									saveTags(resExpense.tags);
								}
							});
						}
					});
				}
				else {
					expenseManagement.expenseNumber = '10000001';
					if(body.file){
						var filePath = './public/images/'+expenseManagement.expenseNumber+body.fileFormat[0];
						expenseManagement.fileUrl = filePath;
						saveImage(body,filePath);
					}
					expenseManagement.save(function(err, resExpense) {
						if (err) {
							console.log(err);
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							res.status(200).send(resExpense);
							saveTags(resExpense.tags);
						}
					});
				}

				
			}	
		});
        
        
	});
	
};

exports.getTags = function(req, res) {
	ExpenseTags.find({}).exec(function(err, expenses){
		//console.log(expenses);
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if(expenses.length)
				res.status(200).send(expenses[0].tags);
			else
				res.status(200).send(null);
		}
	});
};

exports.listAll = function(req, res) {
	ExpenseManagement.find({}).exec(function(err, expenses){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(expenses);
		}
	});
};

exports.list = function(req, res) {
	var perPage = 10,
	pageNumber = req.params.pageNo,
	regex = new RegExp(req.params.keyword, 'i'),
	query;
	console.log(req.params.keyword);
	if(req.params.keyword!=='all'){
		query = ExpenseManagement.find({$or: [{'description':{$regex : regex}},{'tags': {$regex : regex}}]}).sort({'_id':-1}).skip(pageNumber * perPage).limit(perPage);
		
	}else {
	query = ExpenseManagement.find().sort({'_id':-1}).skip(pageNumber * perPage).limit(perPage);
	}
	query.exec(function(err, expenses){
		if (err) {
			console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(expenses);
		}
	});
};

			



// exports.read = function(req, res) {
// 	var query;
// 	if(req.params.appointment_id) {
// 		query = Appointment.findById(req.params.appointment_id);
// 	} else {
// 		return res.status(500).send({msg: "Incomplete parameters"});
// 	}
	
// 	query.exec(function(err, appointment){
// 		if (err) {
// 			return res.status(400).send({
// 				message: errorHandler.getErrorMessage(err)
// 			});
// 		} else {
// 			res.status(200).send(appointment);
// 		}
// 	});
// };



exports.delete = function(req, res) {
	ExpenseManagement.remove({'expenseNumber':req.params.expenseNumber} , function(err,resExpense) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {	
			res.sendStatus(200).send(resExpense);
		}
	});
};
