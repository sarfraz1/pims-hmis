'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	LabOrder = mongoose.model('LabOrder'),
	multiparty = require('multiparty'),
	Patient = mongoose.model('Patient'),
	request = require('request'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Panel
 */
exports.create = function(req, res) {
	req.checkBody('patientMRN', 'Patient MR Number is required').notEmpty();
	req.checkBody('billNumber', 'Bill Number is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {

		var labOrder = new LabOrder(req.body);
		LabOrder.findOne({'billNumber': req.body.billNumber}).exec(function(err, reslabOrder) {
			if(err){
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else if(reslabOrder){
				reslabOrder.labTests = req.body.labTests;
				reslabOrder.created_date = req.body.created_date;
				reslabOrder.time = req.body.time;
				reslabOrder.lastModified.push({lastModifiedBy: req.body.lastModifiedBy}); 
				reslabOrder.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(labOrder);
					}
				});
			}
			else {
				labOrder.lastModified = [];
				labOrder.lastModified.push({lastModifiedBy: req.body.lastModifiedBy}); 
				labOrder.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(labOrder);
					}
				});
			}
		});

	}
};

exports.createImage = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {

        req.body = body;
        
        if(body.file){

            var picPath = body.imageLoc[0];
            
            var temp = body.file[0];
            temp = temp.split(',');
            
            var b64string = temp[1];
			var buf = new Buffer(b64string, 'base64');
            fs.writeFile(picPath , buf , function (err , success) {
                if(err){
                    return res.status(400).send({
                        'error': err
                    });
                }
                else {
					res.json('saved');
				}
            });
        }
        
	});
};

/**
 * Show the current Panel
 */

exports.getByDate = function(req, res) {
	var fromDate = req.params.fromDate,
		toDate = req.params.toDate,
		query;

	if(fromDate==='undefined' &&  toDate==='undefined'){
		query =  LabOrder.find();
	} else if(toDate==='undefined') {
		query =  LabOrder.find({'created_date': { '$gte' : new Date(fromDate)}})
		//query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)}});
	} else {
		query =  LabOrder.find({'created_date':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
		//query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
	}

	query.exec(function(err, labOrder) {
		if (labOrder) {
			res.json(labOrder);
		} else {
			err = 'Panel not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

exports.read = function(req, res) {
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabOrder.findOne({'_id': req.params.labOrderId}).exec(function(err, labOrder) {
			if (labOrder) {
				res.json(labOrder);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getbyMRN = function(req, res) {
	if (!req.params.mr_number) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabOrder.find({'patientMRN': req.params.mr_number}).exec(function(err, labOrder) {
			if (labOrder) {
				res.json(labOrder);
			} else {
				err = 'Labs not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};


exports.update = function(req, res) {
	delete req.body._id;
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		//req.checkBody('description', 'Description is required').notEmpty();
		//req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			LabOrder.findOneAndUpdate({'_id': req.params.labOrderId}, req.body).exec(function (err, labOrder) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(labOrder);
				}
			});
		}
	}
};

var sendSmsForPatientResults = function(number , messagedata){
	
	if(number.charAt(0) == '0' && number.substring(0 , 3) != '051'){
	   number = number.substring(1);
	   number = '92' + number;
	}
	
    var stringApiPath = 'http://smsctp1.eocean.us:24555/api?action=sendmessage';
    var stringData = "&username=" + '8655_Sarfaraz' + "&password=" + 'SaRfArAz12' + "&recipient=" + number + "&originator=" + "8655" + "&messagedata=" + messagedata;
    var string = stringApiPath + stringData;
    var options = {
        uri: string,
        method: 'GET'
    };

    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            result = body;
        } else {
            var result = 'Not Found';
        }
    });
};

exports.updateStatus = function(req, res) {
	if (!req.params.labOrderId || !req.params.description || !req.params.status ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		//req.checkBody('description', 'Description is required').notEmpty();
		//req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			LabOrder.findOne({'_id': req.params.labOrderId}).exec(function (err, labOrder) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					
					for(var i=0;i<labOrder.labTests.length;i++){
						if(labOrder.labTests[i].testDescription===req.params.description){
							labOrder.labTests[i].status = req.params.status;
							break;
						}
					}

					var allTrue = true;
					for(i=0;i<labOrder.labTests.length;i++){
						if(labOrder.labTests[i].status !== 'Completed'){
							allTrue = false;
						}
					}
					
					if(allTrue === true){
						labOrder.orderStatus = 'Completed';
					} else if(req.params.status==='Resample'){
						labOrder.orderStatus = 'Resample';
						for(var i=0;i<labOrder.labTests.length;i++){
							if(labOrder.labTests[i].testDescription===req.params.description){
								labOrder.labTests[i].status = 'Resample';
							}
						}
					} else {
						labOrder.orderStatus = 'In Progress';
					}

					labOrder.save(function(err,resLabOrder){
						if(err){
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						}else {
							res.json(resLabOrder);
							/*if(resLabOrder.orderStatus == 'Completed') {
								Patient.findOne({'mr_number':resLabOrder.patientMRN} , function(err,patient) {
									if (err) {
									
									} else if (patient) {	
										//send sms to patient
										var msg = 'Dear '+ resLabOrder.patientName +'\nYour test results against order number '+ resLabOrder.billNumber+
										' are ready. Please visit'+
										'\nAL SAFIYA\nMedical and Diagnostics Center\n'+
										'051-2340101\n051-2340111';
									//	sendSmsForPatientResults(patient.phone, msg);
									}
								});
							}*/
						}
						
					});
					//res.json(labOrder);
				}
			});
		}
	}
};

/**
 * Delete a Panel
 */
exports.delete = function(req, res) {
	if (!req.params.labOrderId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabOrder.remove({'_id': req.params.labOrderId}).exec(function (err, labOrder) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(labOrder);
			}
		});
	}
};

/**
 * List of Panels
 */
exports.list = function(req, res) {
	LabOrder.find().exec(function(err, labOrder) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(labOrder);
		}
	});
};

exports.getPanelLabReport = (req, res) => {
	let fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);

		fromDate = new Date(fromDate);
		fromDate.setDate(fromDate.getDate());
	}
	let query;
	let dataToSend = [];
	if(req.params.panelLab !== 'All' && req.params.panelLab !== 'undefined') {
		query = LabOrder.find({created_date: {$lte : toDate, $gte: fromDate}, labTests: {$elemMatch: {transferredTo: req.params.panelLab}}});
	} else {
		query = LabOrder.find({created_date: {$lte : toDate, $gte: fromDate},labTests: {$elemMatch: {transferredTo: {$ne: ''}}}});
	}
	query.exec((err, orders) => {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
		orders.forEach(order => {
			const {patientMRN, patientName, created_date, labTests} = order;
			labTests.forEach((test) => {
				let sendTest = {
					created_date,
					patientMRN,
					patientName,
					testDescription: test.testDescription,
					panelLab: test.transferredTo,
					sampleNo: test.sampleNo,
					status: test.status
				}
				dataToSend.push(sendTest);
			});
		});
		res.json(dataToSend);
	});
};
