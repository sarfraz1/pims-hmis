'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	RadiologyResults = mongoose.model('RadiologyResults'),
	RadiologyTest = mongoose.model('RadiologyTest'),
	Patient = mongoose.model('Patient'),
    _ = require('lodash');

/**
 * Create a radiology result
 */

var calculateAge = function(birthday) { // birthday is a date
 	var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};

exports.create = function(req, res) {
	
	var radiologyResults = new RadiologyResults(req.body);
	
	radiologyResults.save(function(err, radiologyResultsRes){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
		
			res.status(200).send(radiologyResultsRes);
		}
	});
			
};

exports.read = function(req, res) {
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabResults.findOne({'_id': req.params.labResultsId}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTest = function(req, res) {
	if (!req.params.patientMRN || !req.params.testDescription ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		RadiologyResults.findOne({'patientMRN':req.params.patientMRN,'description':req.params.testDescription}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Radiology test not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTestbyOrderId = function(req, res) {
	if (!req.params.orderId || !req.params.testDescription ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		RadiologyResults.findOne({'radiologyOrderId':req.params.orderId,'description':req.params.testDescription}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Radiology test not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getRadiologybyMRN = function(req, res) {
	if (!req.params.patientMRN) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		RadiologyResults.find({'patientMRN':req.params.patientMRN}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Tests not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTests = function(req, res) {
	if (!req.params.patientMRN || !req.params.testDescription ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		RadiologyResults.find({'patientMRN':req.params.patientMRN,'description':req.params.testDescription}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Patient not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.update = function(req, res) {
	delete req.body._id;
	
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			RadiologyResults.findOneAndUpdate({'_id': req.params.labResultsId}, req.body).exec(function (err, labResults) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(labResults);
				}
			});
		}
	}
};

exports.delete = function(req, res) {
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		RadiologyResults.remove({'_id': req.params.labResultsId}).exec(function (err, labResults) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(labResults);
			}
		});
	}
};

exports.list = function(req, res) {
	RadiologyResults.find().exec(function(err, labResults) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(labResults);
		}
	});
};
