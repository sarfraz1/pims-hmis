'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Patient = mongoose.model('Patient'),
	Billing = mongoose.model('Billing'),
	_ = require('lodash'),
	moment = require('moment'),
	request = require('request');

var sendSmsForPatientRegistration = function(number , messagedata){

	if(number.charAt(0) == '0' && number.substring(0 , 3) != '051'){
	   number = number.substring(1);
	   number = '92' + number;
	}

    var stringApiPath = 'http://smsctp1.eocean.us:24555/api?action=sendmessage';
    var stringData = "&username=" + '8655_Sarfaraz' + "&password=" + 'SaRfArAz12' + "&recipient=" + number + "&originator=" + "8655" + "&messagedata=" + messagedata;
    var string = stringApiPath + stringData;
    var options = {
        uri: string,
        method: 'GET'
    };

    request(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            result = body;
        } else {
            var result = 'Not Found';
        }
    });
};

var dateConverter = function(dateinput) {
	try{
		var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
		utcDate = utcDate.toUTCString();
		return utcDate;
	}
	catch(error){
		return dateinput;
	}
};

/**
 * Register new patient
 */
exports.create = function(req, res) {

	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('phone', 'Phone number is required').notEmpty();
	var errors = req.validationErrors();

	if(errors) {
		return res.status(500).send(errors);
	} else {

		var patient = new Patient(req.body);
		var mr_number;
		var query = Patient.find({}, {mr_number:1, _id:0}).sort({mr_number: -1}).limit(1);
		query.exec(function(err, maxmrnumber){
			if (err) {
				return err;
			} else {
				if(maxmrnumber[0]) {
					patient.mr_number = parseInt(maxmrnumber[0].mr_number) + 1;
				} else {
					patient.mr_number = '100010';
				}

				patient.save(function(err, patient) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err),
							code : err.code
						});
					} else {
						res.status(200).send(patient);
						//send sms to patient
						var msg = 'Dear '+ patient.name +'\nWelcome to Ali Medical Clinics\n'+
						'Your MR Number is '+patient.mr_number;
						sendSmsForPatientRegistration(patient.phone,msg);
					}
				});
			}
		});
	}
};

/**
 * Retrieve patient based on mr_no or phone
 */
exports.read = function(req, res) {
	var query;
	if(req.params.key) {
		query = Patient.find({$or: [{mr_number: req.params.key}, {phone: req.params.key}]}, {_id:0});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, patient){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(patient);
		}
	});
};

/**
 * Update a patient record
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('key', 'MR number is required').notEmpty();
	var errors = req.validationErrors();

	var item = req.body;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Patient.findOneAndUpdate({'mr_number': req.params.key}, item, {upsert: true}, function(err, patient) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				//Update all patient bills
				Billing.update({'patientInfo.mr_number': req.params.key}, {$set: {'patientInfo.name': patient.name,'patientInfo.phone': patient.phone}}, function(err, patient) {



						res.status(200).send(patient);
				});
				res.status(200).send(patient);
			}
		});
	}
};

/**
 * Delete a Patient
 */
exports.delete = function(req, res) {
	Patient.remove({'mr_number':req.params.key} , function(err,patient) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(patient);
		}
	});
};

/**
 * List of Patients
 */
exports.list = function(req, res) {
	Patient.find().exec(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).json(patients);
		}
	});
};

exports.list = function(req, res) {
	Patient.find({'created': {$lte : req.params.todate, $gte: req.params.fromdate}}).count(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log(patients);
			res.status(200).json(patients);
		}
	});
};


var keywordSearch = function(req, res) {
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Patient.find({$or: [{'mr_number':{$regex : regex}},{'phone':{$regex : regex}},{'name': {$regex : regex}}]},'-__v').limit(10).exec(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).json(patients);
		}
	});
};

exports.patientReport = function(req, res) {
	var fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		//console.log('frm date: '+ fromDate);
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);

		var tdate = moment(toDate).add(5, 'hours');
		toDate = new Date(tdate.format());

		var fdate = moment(fromDate).subtract(5, 'hours');
		fromDate = new Date(fdate.format());
	}

	var query;

	query = Patient.find({'created': {$lte : toDate, $gte: fromDate}, 'patient_area': req.params.area});

	query.exec(function(err, patients){
		var total = 0;
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).send(patients);
		}
	});
}

exports.search = function(req, res) {
	keywordSearch(req, res);
};
