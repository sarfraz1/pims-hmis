'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Invoice = mongoose.model('Invoice'),
	multiparty = require('multiparty'),
	fs = require('fs'),
	moment = require('moment'),
    _ = require('lodash');

/**
 * Create an Invoice
 */
exports.create = function(req, res) {
	var latest = 1;
	var invoiceDate = new Date();

	req.body.invoiceDate = dateConverter(invoiceDate);
	Invoice.findOne().sort({invoiceNumber:-1}).limit(1).exec(function(err, invoiceLatest) {
		if (invoiceLatest) {
			latest = invoiceLatest.invoiceNumber;
			if (invoiceLatest.orderList.length === 0) {
				res.json(invoiceLatest);
				return false;
			} else {
				req.body.invoiceNumber = latest + 1;
			}
		} else {
			req.body.invoiceNumber = latest;
		}
		var invoice = new Invoice(req.body);
		invoice.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				console.log('invoice saved successfully');
				res.json(invoice);
			}
		});
	});
};

/**
 * Update an Invoice
 */

 var dateConverter = function(dateinput){
    try{
            var todayTime = Date();
            var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate(), todayTime.getHours(), todayTime.getMinutes(), todayTime.getSeconds()));
            utcDate = utcDate.toUTCString();
            return utcDate;
        } catch(error){
         	return dateinput;
        }
};

exports.update = function(req, res) {
	var invoiceDate = new Date();

	req.body.invoiceDate = dateConverter(invoiceDate);
	Invoice.findOne({'invoiceNumber': req.body.invoiceNumber}).exec(function (err, invoice) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(invoice){
			if(!invoice.orderList || invoice.orderList.length == 0) {
				Invoice.findOneAndUpdate({'invoiceNumber': req.body.invoiceNumber}, req.body).exec(function (err, invoice) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						//console.log(invoice);
						res.json(invoice);
					}
				});
			} else {
				var latest = 1
				Invoice.findOne().sort({invoiceNumber:-1}).limit(1).exec(function(err, invoiceLatest) {
					if (invoiceLatest) {
						latest = invoiceLatest.invoiceNumber;
						req.body.invoiceNumber = latest + 1;
						
					} else {
						req.body.invoiceNumber = latest;
					}
					var invoice = new Invoice(req.body);
					invoice.save(function(err) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							console.log('invoice saved successfully');
							res.json(invoice);
						}
					});
				});
			}
			
		}
	});
};

exports.updatepurchaseprice = function(req, res) {

	Invoice.findOne({'invoiceNumber': req.params.invoiceId}).exec(function (err, invoice) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			for(var i=0;i<req.body.itemList.length;i++) {
				for(var j=0;j<invoice.orderList.length;j++) {
					if(invoice.orderList[j].id === req.body.itemList[i].itemId) {
						invoice.orderList[j].purchasePrice = req.body.itemList[i].price;
					}
				}
			}
			invoice.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
				} else {
					res.json(invoice);
				}
			});
		}
	});
};

exports.updateorderlist = function(req, res) {

	Invoice.findOne({'invoiceNumber': req.body.invoiceNumber}).exec(function (err, invoice) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			invoice.orderList = req.body.orderList;
			invoice.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
				} else {
					res.json(invoice);
				}
			});
		}
	});
};

/**
 * Show the current Invoice
 */
exports.read = function(req, res) {
	Invoice.findOne({'invoiceNumber': req.params.invoiceId}).exec(function(err, invoice) {
		if (invoice) {
			res.json(invoice);
		} else {
			err = 'Invoice not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

// Compare times return true if inv time is greater
var comparetimes = function(frmTime, invtime) {
	
	var frmres = frmTime.split(":");
	var frmhr = frmres[0];
	var frmres1 = frmres[1].split(" ");
	var frmmer = frmres1[1];
	
	var cmpres = invtime.split(":");
	var cmphr = cmpres[0];
	var cmpres1 = cmpres[1].split(" ");
	var cmpmer = cmpres1[1];

	if(frmmer == 'PM') {
		if(cmpmer == 'AM') {
			return false;
		} else {
			if(Number(cmphr) >= Number(frmhr)) {
				return true;
			} else {
				return false;
			}
		}
	} else {
		if(cmpmer == 'PM') {
			return true;
		} else {
			if(Number(cmphr) >= Number(frmhr)) {
				return true;
			} else {
				return false;
			}			
		}
	}
}

/**
 * List of Invoices
 */
exports.list = function(req, res) {
	Invoice.find().exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(invoices);
		}
	});
};

/**
 * List of Filtered Invoices
 */
exports.listFiltered = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var frmdate = moment(fromDate).toISOString();
	var tdate = moment(toDate).toISOString();
	var fromTime = req.params.fromTime;
	var toTime = req.params.toTime;
	if(toTime == 0)toTime='undefined';

	var salesmanName = req.params.salesmanName;
	var query;
	if(req.params.pageNumber){
		var perPage = 10;
		var pageNumber = Number(req.params.pageNumber);
		if (salesmanName === 'All') {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}}).skip(pageNumber * perPage).limit(perPage);
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$lte' : new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate), '$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		} else {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName}).skip(pageNumber * perPage).limit(perPage);
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else{
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate), '$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		}
	}
	else {
		if (salesmanName === 'All') {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}});
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$lte' : new Date(toDate)}});
			} else {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate), '$lte': new Date(toDate)}});
			}
		} else {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName});
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$lte': new Date(toDate)}});
			} else{
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate), '$lte': new Date(toDate)}});
			}
		}
	}
	query.exec(function (err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var invtosend = [];
			for(var i=0;i<invoices.length;i++){
				
				if(invoices[i].invoiceDate.toISOString() == frmdate) {
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
					//console.log('invoice Time: '+invoices[i].invoiceTime);
					if(fromTime !== 'undefined') {
						if(comparetimes(fromTime,invoices[i].invoiceTime) == false) 
							continue;
					}
				}
				if(invoices[i].invoiceDate.toISOString() == tdate) {
					
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
					
					if(toTime !== 'undefined') {
						if(comparetimes(toTime,invoices[i].invoiceTime) == true)
							continue;
					}
				}
				invtosend.push(invoices[i]);
			}
			
			res.jsonp(invtosend);
		}
	});
};

/**
 * List of Filtered Invoices Total
 */
exports.listFilteredTotal = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var frmdate = moment(fromDate).toISOString();
	var tdate = moment(toDate).toISOString();
	var fromTime = req.params.fromTime;
	var toTime = req.params.toTime;
	if(toTime == 0)toTime='undefined';

	var salesmanName = req.params.salesmanName;
	var query;

		if (salesmanName === 'All') {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}});
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$lte' : new Date(toDate)}});
			} else {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceDate': { '$gte' : new Date(fromDate), '$lte': new Date(toDate)}});
			}
		} else {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName});
			} else if (toDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$lte': new Date(toDate)}});
			} else{
				query = Invoice.find({'orderList.0': {$exists: true}, 'invoiceSalesman': salesmanName, 'invoiceDate': {'$gte': new Date(fromDate), '$lte': new Date(toDate)}});
			}
		}

	query.exec(function (err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var invtosend = [];
			var totalsale = 0;
			var creditsale = 0;
			var returnsale = 0;
			for(var i=0;i<invoices.length;i++){
				
				if(invoices[i].invoiceDate.toISOString() == frmdate) {
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
					//console.log('invoice Time: '+invoices[i].invoiceTime);
					if(fromTime !== 'undefined') {
						if(comparetimes(fromTime,invoices[i].invoiceTime) == false) 
							continue;
					}
				}
				if(invoices[i].invoiceDate.toISOString() == tdate) {
					
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
					
					if(toTime !== 'undefined') {
						if(comparetimes(toTime,invoices[i].invoiceTime) == true)
							continue;
					}
				}
				
				var subtotal = 0;
				for (var j = 0; j < invoices[i].orderList.length; j++) {
                    subtotal = subtotal + invoices[i].orderList[j].price * invoices[i].orderList[j].quantity;
                }
				
				if(invoices[i].isReturn) {
					console.log('isreturn');
					returnsale+=subtotal;
				} else if(invoices[i].issalemrno) {
					console.log('issalemrno');
					creditsale+=subtotal;
				} else {
					console.log('total');
					totalsale += subtotal;
				}
			}
			
			var total_sales =  {
				'totalsale': totalsale,
				'creditsale': creditsale,
				'returnsale': returnsale
			};
			
			//console.log(total_sales);
			
			res.jsonp(total_sales);
		}
	});
};

exports.report = function(req, res) {
	
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
		
	var salesmanName = req.params.salesmanName;
	var query;
/*	if(req.params.pageNumber) {
		var perPage = 100;
		var pageNumber = Number(req.params.pageNumber);
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find().skip(pageNumber * perPage).limit(perPage);
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName}).skip(pageNumber * perPage).limit(perPage);
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		}
	}*/
	//else {
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({"invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,"invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
	//}


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var salesReport = [];
			for(var i=0;i<invoices.length;i++){
				
				/*if(invoices[i].invoiceDate.toISOString() == frmdate) {

					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}

					if(fromTime !== 'undefined') {
						if(comparetimes(fromTime,invoices[i].invoiceTime) == false) 
							continue;
					}
				}
				if(invoices[i].invoiceDate.toISOString() == tdate) {
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}					
					if(toTime !== 'undefined') {
						if(comparetimes(toTime,invoices[i].invoiceTime) == true)
							continue;
					}
				}*/
					salesReport.push(invoices[i]);
					//console.log('in if');
					/*for(var j=0;j<invoices[i].orderList.length;j++){
						var itemExists = false;
						
						for(var k=0;k<salesReport.length;k++){
							if(salesReport[k].id===invoices[i].orderList[j].id){
								itemExists = true;
								if(invoices[i].isReturn) {
									salesReport[k].quantity = salesReport[k].quantity-invoices[i].orderList[j].quantity;
								} else {
									salesReport[k].quantity = salesReport[k].quantity+invoices[i].orderList[j].quantity;
								}
							}
						}
						if(itemExists===false){
							if(invoices[i].isReturn) {
								for(var l=0;l<invoices[i].orderList.length;l++) {
									invoices[i].orderList[l].quantity = invoices[i].orderList[l].quantity*-1;
								}
							}
							salesReport.push(invoices[i].orderList[j]);
						}
					}*/
			}
			res.json(salesReport);
		}
	});
};

exports.salesreport = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
		
	var salesmanName = req.params.salesmanName;
	//var storeDesc = req.params.storeDesc;
	var tempDesc = req.params.storeDesc;
	var storeDesc = tempDesc.replace(/%20/g, " ");
	var query;
	//console.log(req.params.salesmanName);
	if(storeDesc==='All'){
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({"invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,"invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
	} else {
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({"storeDesc": storeDesc, "invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'storeDesc': storeDesc, 'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'storeDesc': storeDesc,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'storeDesc': storeDesc,'invoiceSalesman':req.params.salesmanName,"invoiceTime": { $exists: true}});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'storeDesc': storeDesc,'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
			} else {
				query =  Invoice.find({'storeDesc': storeDesc,'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
			}
		}
	}


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log(invoices);
			var salesReport = [];
			for(var i=0;i<invoices.length;i++){
				salesReport.push(invoices[i]);
			}
			res.json(salesReport);
		}
	});
};

exports.costreport = function(req, res) {
	
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	
	var query;

	if(fromDate==='undefined' &&  toDate==='undefined'){
		query =  Invoice.find({"invoiceTime": { $exists: true}});
	} else if(toDate==='undefined') {
		query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)},"invoiceTime": { $exists: true}});
	} else {
		query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"invoiceTime": { $exists: true}});
	}
		

	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var salesReport = [];
			for(var i=0;i<invoices.length;i++){
				
					salesReport.push(invoices[i]);
					
			}
			res.json(salesReport);
		}
	});
};

exports.reportSalesAmount = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var salesmanName = req.params.salesmanName;
	var query;
	if(salesmanName==='All'){
		if(fromDate==='undefined' &&  toDate==='undefined'){
			query =  Invoice.find();
		} else if(toDate==='undefined') {
			query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)}});
		} else {
			query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
		}
	}
	else {
		if(fromDate==='undefined' &&  toDate==='undefined'){
			query =  Invoice.find({'invoiceSalesman':req.params.salesmanName});
		} else if(toDate==='undefined') {
			query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)}});
		} else {
			query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}});
		}
	}


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var totalAmount = 0;
			for(var i=0;i<invoices.length;i++){
				for(var j=0;j<invoices[i].orderList.length;j++){
					if(invoices[i].isReturn) {
						totalAmount -= invoices[i].orderList[j].price*invoices[i].orderList[j].quantity;
					} else {
						totalAmount += invoices[i].orderList[j].price*invoices[i].orderList[j].quantity;
					}
					
				}
			}
			res.json(totalAmount);
		}
	});
};

/**
 * Delete Invoice
 */
exports.delete = function(req, res) {
	Invoice.remove({'invoiceNumber': req.params.invoiceId}, function(err, invoice) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invoice);
		}
	});
};


/**
 * Find Invoice in Receipt Range
 */
exports.invoiceReceiptRange = function(req, res) {
	var fromReceipt = req.params.fromReceipt;
	var toReceipt = req.params.toReceipt;
	var salesmanName = req.params.salesmanName;
	var query;
			
	if(salesmanName=='All'){
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt}});
	}
	else{		
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt} , 'invoiceSalesman': salesmanName});
	}	


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invoices);
		}
	});
};

/**
 * Invoice Report in Receipt Range
 */

exports.receiptReport = function(req, res) {
	
	var fromReceipt = req.params.fromReceipt;
	var toReceipt = req.params.toReceipt;
	var salesmanName = req.params.salesmanName;
	var query;

	if(salesmanName=='All'){
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt}});
	}
	else{		
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt} , 'invoiceSalesman': salesmanName});
	}	


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var salesReport = [];
			for(var i=0;i<invoices.length;i++){
				for(var j=0;j<invoices[i].orderList.length;j++){
					var itemExists = false;
					for(var k=0;k<salesReport.length;k++){
						if(salesReport[k].id===invoices[i].orderList[j].id){
							itemExists = true;
							if(invoices[i].isReturn) {
								salesReport[k].quantity = salesReport[k].quantity-invoices[i].orderList[j].quantity;
							} else {
								salesReport[k].quantity = salesReport[k].quantity+invoices[i].orderList[j].quantity;
							}
						}
					}
					if(itemExists===false){
						salesReport.push(invoices[i].orderList[j]);
					}
				}
			}
			res.json(salesReport);
		}
	});
};




/**
 * Invoice Sales Amount in Receipt Range
 */

exports.reportReceiptSalesAmount = function(req, res) {
	var fromReceipt = req.params.fromReceipt;
	var toReceipt = req.params.toReceipt;
	var salesmanName = req.params.salesmanName;
	var query;

	if(salesmanName=='All'){
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt}});
	}
	else{		
		query =  Invoice.find({'invoiceNumber':{'$gte': fromReceipt , '$lte': toReceipt} , 'invoiceSalesman': salesmanName});
	}	

	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var totalAmount = 0;
			for(var i=0;i<invoices.length;i++){
				for(var j=0;j<invoices[i].orderList.length;j++){
					if(invoices[i].isReturn) {
						totalAmount -= invoices[i].orderList[j].price*invoices[i].orderList[j].quantity;
					} else {
						totalAmount += invoices[i].orderList[j].price*invoices[i].orderList[j].quantity;
					}
					
				}
			}
			res.json(Math.round(totalAmount));
		}
	});
};

exports.reportNarcotics = function(req, res) {
	
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var frmdate = moment(fromDate).toISOString();
	var tdate = moment(toDate).toISOString();
	var fromTime = req.params.fromTime;
	var toTime = req.params.toTime;
	if(toTime == 0)toTime='undefined';
	var salesmanName = req.params.salesmanName;
	var query;
	if(req.params.pageNumber && req.params.pageNumber!=='undefined') {		
		var perPage = 100;
		var pageNumber = Number(req.params.pageNumber);
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find().skip(pageNumber * perPage).limit(perPage);
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)},"doctorName": { $exists: true, $ne: null }}).skip(pageNumber * perPage).limit(perPage);
			} else {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)},"doctorName": { $exists: true, $ne: null }}).skip(pageNumber * perPage).limit(perPage);
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName, "doctorName": { $exists: true, $ne: null }}).skip(pageNumber * perPage).limit(perPage);
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)}, "doctorName": { $exists: true, $ne: null }}).skip(pageNumber * perPage).limit(perPage);
			} else {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}, "doctorName": { $exists: true, $ne: null }}).skip(pageNumber * perPage).limit(perPage);
			}
		}
	}
	else {
		if(salesmanName==='All'){
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find();
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate)}, "orderList.narcotics": true});
			} else {
				
				query =  Invoice.find({'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}, "orderList.narcotics": true});
			}
		}
		else {
			if(fromDate==='undefined' &&  toDate==='undefined'){
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName, "orderList.narcotics": true});
			} else if(toDate==='undefined') {
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate)}, "orderList.narcotics": true});
			} else {
				console.log('this is quer');
				query =  Invoice.find({'invoiceSalesman':req.params.salesmanName,'invoiceDate':{ '$gte' : new Date(fromDate) ,'$lte' : new Date(toDate)}, "orderList.narcotics": true});
			}
		}
	}


	query.exec(function(err, invoices) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log("found invoices: " +invoices.length);
			var salesReport = [];
			for(var i=0;i<invoices.length;i++){
				
				if(invoices[i].invoiceDate.toISOString() == frmdate) {

					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}

					if(fromTime !== 'undefined') {
						if(comparetimes(fromTime,invoices[i].invoiceTime) == false) 
							continue;
					}
				}
				if(invoices[i].invoiceDate.toISOString() == tdate) {
					if(invoices[i].invoiceTime.length<8) {	
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}					
					if(toTime !== 'undefined') {
						if(comparetimes(toTime,invoices[i].invoiceTime) == true)
							continue;
					}
				}
					//console.log('in if');
					for(var j=0;j<invoices[i].orderList.length;j++){
						var itemExists = false;

						if(invoices[i].orderList[j].narcotics){
							if(!invoices[i].isReturn) {
								//var ind=-1;
								
							salesReport.push({"id": invoices[i].orderList[j].id,
											"description": invoices[i].orderList[j].description,
											"quantity": invoices[i].orderList[j].quantity - invoices[i].orderList[j].returned,
											"price": invoices[i].orderList[j].price,
											"doctorName": invoices[i].doctorName,
											"patient_name": invoices[i].patient_name,
											"invoiceDate": invoices[i].invoiceDate,
											"invoiceTime": invoices[i].invoiceTime,
											"invoiceNumber": invoices[i].invoiceNumber});
							}
						}
					}
			}
			res.json(salesReport);
		}
	});
};