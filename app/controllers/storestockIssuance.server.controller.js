'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	StockIssuance = mongoose.model('StoreStockIssuance'),
	InventoryStock = mongoose.model('StoreInventoryStock'),
	Inventory = mongoose.model('StoreInventory'),
	async = require('async'),
	_ = require('lodash');

var dateConverter = function(dateinput){
	try{
		var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
		utcDate = utcDate.toUTCString();
		return utcDate;
	}
	catch(error){
		return dateinput;
	}
};

exports.create = function(req, res) {
    
	var stockIssuance = new StockIssuance(req.body);
	StockIssuance.findOne().sort({_id:-1}).exec(function(err, transferObj) {
		if(true){
			if(transferObj){
				var code = Number(transferObj.issuanceNumber)+1;
				code = String(code);
				
				stockIssuance.issuanceNumber = code;
			} else if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} 
			else {
				stockIssuance.issuanceNumber = '10000001';
			}
			
			async.forEach(stockIssuance.itemsList, function(med, callback) {
				InventoryStock.findOne({'id': med.code}).exec(function (err, stock) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
							callback();
						} else if(stock) {
							var remtransfer = med.quantity;
							
							for(var i=0;i<stock.batch.length;i++) {
								if(stock.batch[i].storeId == stockIssuance.fromStoreId) {
									
									var currtransfer = 0; 
									if(stock.batch[i].quantity < remtransfer) {
										currtransfer = remtransfer - stock.batch[i].quantity;
										stock.batch[i].quantity = 0;
									} else {
										currtransfer = remtransfer;
										stock.batch[i].quantity -= currtransfer;
									}
									
									remtransfer = remtransfer - currtransfer;
									var found = false;
									
									if(remtransfer <= 0) {
										break;
									}
									
								} 	
							}
							
							stock.save(function(err,data) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									callback();
								}
							});
						}
				});
			}, function(err) {
				if (err) return next(err);
				
				stockIssuance.save(function(err,data) {
					if (err) {
						console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(data);
					}
				});
			});
			

			
		}
		else {
			// Code for generating Number on category Basis with 
		}
	});   

    
};


/**
 * List GRNs
 */

exports.list = function(req, res) { 
	/*GRN.find({},{GRNNumber:1,supplierDescription:1,supplierChalanNumber:1,date:1,remarks:1}).lean().exec(function(err, grns) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//var grnarray = new Array();
			//grnarray = grns;
			//filterStock(grnarray, req, res);
			res.json(grns);
		}
	});*/
};

exports.listAll = function(req, res) { 
/*	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = GRN.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});*/
};

exports.update = function(req, res) {
/*	var newList = req.body.itemsList;
	var GRNData = req.body;
    GRN.findOne({'GRNNumber':req.body.GRNNumber}).exec(function(err, data) {
		
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var dumyData = JSON.parse(JSON.stringify(req.body.itemsList));
			var tempList = itemslistUpdatedInfo(dumyData,data.itemsList);
			
			checkData(tempList,0,function(err){
		    	if(err){
					return res.status(400).send({
						message: err
					});
				}else{
					updatePurchaseOrder(tempList,0);
				
					GRN.findOneAndUpdate({'GRNNumber':req.body.GRNNumber},GRNData).exec(function(err,grnSaveResponse){
						if(err){
							console.log(err);
						}
						else {
							var originalList = data.itemsList;
							var stockToDelete = [];
							var deleted = true;
							for (var i = 0; i < originalList.length; i++) {
								deleted = true;
								for (var j = 0; j < newList.length; j++) {
									if (originalList[i].code === newList[j].code) {
										deleted = false;
										break;
									}
								}
								if (deleted === true) {
									stockToDelete.push({'code': originalList[i].code});
								}
							}
							if (stockToDelete.length > 0) {
								deleteInventoryStockByGRN(stockToDelete, 0, req.body.GRNNumber);
							}
							res.json(grnSaveResponse);
							updateInventoryStockByGRN(tempList, 0, req.body.GRNNumber,req.body.storeId,req.body.storeDescription,req.body.supplierId);
						}
					});
						
				}
			});
		}
	
	});*/
	
};

/**
 * Get GRN by Number
 */
exports.getByNumber = function(req, res) {
/*	GRN.findOne({'GRNNumber': req.params.number}).lean().exec(function (err, grn) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
		//	res.json(grn);
			filterStock(grn, req, res)
		}
	});*/
};


/**
 * Delete GRN
 */
exports.delete = function(req, res) {
/*	GRN.findOne({'_id': req.params.GRNId}).exec(function (err, response) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var itemsList = response.itemsList;
			var grnNumber = response.GRNNumber;
			GRN.remove({'_id': req.params.GRNId}, function(err, grnObj) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					var stockToDelete = [];
					for (var i = 0; i < itemsList.length; i++) {
						stockToDelete.push({'code': itemsList[i].code});
					}
					deleteInventoryStockByGRN(stockToDelete, 0, grnNumber);
					res.json(grnObj);
				}
			});
		}
	});*/
};


var searchByNumber = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	StockTransfer.find({ $or: [{'GRNNumber':{$regex : regex}}, {'supplierDescription':{$regex : regex}}, {'supplierChalanNumber':{$regex : regex}} ]},{GRNNumber:1,supplierDescription:1,supplierChalanNumber:1,date:1,remarks:1}).lean().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			/*var compare = function(a, b) {
				if (a.GRNNumber.indexOf(keyword) < b.GRNNumber.indexOf(keyword))
					return -1;
				if (a.GRNNumber.indexOf(keyword) > b.GRNNumber.indexOf(keyword))
					return 1;
				return 0;
			};
			data.sort(compare);*/
			//filterStock(data, req, res);
			res.json(data);
		}
	});
};

var searchAll = function(req, res) {
	/*GRN.find().exec(function (err, data) {
		if (err) {
			return res.status(400).send({
				message:errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});*/
};

exports.search = function(req, res) {
	if (req.params.searchType === 'GRNNumber') {
		searchByNumber(req, res);
	} else if (req.params.searchType === 'all') {
		searchAll(req, res);
	}
};

/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
