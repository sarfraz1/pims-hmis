'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	PrescriptionPref = mongoose.model('PrescriptionPref'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Medicine
 */
exports.create = function(req, res) {
	var prescriptionPref = req.body;
	delete prescriptionPref._id;
	delete prescriptionPref.__v;
	PrescriptionPref.findOneAndUpdate({doctorName:prescriptionPref.doctorName}, prescriptionPref, {upsert: true}).exec(function (err, response) {
		if (err) {
			console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log(response);
			res.json(response);
		}
	});
	
};


exports.list = function(req, res) {
	PrescriptionPref.find().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};

exports.read = function(req, res) {
	PrescriptionPref.findOne({doctorId:req.params.doctorId}).exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(data);
		}
	});
};
