'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	tokenNumber = mongoose.model('tokenNumber'),
	moment = require('moment'),
    _ = require('lodash');

/**
 * Create or update a token
 */
exports.create = function(req, res) {
	
	var service_name = req.body.service_name;
	var date_str = moment().format('DD-MM-YYYY');
	tokenNumber.findOne({'service_name': service_name}).exec(function(err, token) {
		if (token) {
			//res.json(hospital);
			if(token.token_date !== date_str) {
				token.token_date = date_str;
				token.token_number = 1;
			} else {
				token.token_number = token.token_number + 1;
			}
			
			token.save(function (err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(token);
				}
			});
			
		} else {
			var token = new tokenNumber();
			token.service_name = service_name;
			token.token_number = 1;
			token.token_date = date_str;
			
			token.save(function (err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(token);
				}
			});
		}
	});
};

/**
 * Get token number by service name
 */
exports.read = function(req, res) {
	tokenNumber.findOne({'service_name': req.params.service_name}).exec(function(err, token) {
		if (token) {
			res.json(token);
		} else {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Hospital
 */
exports.update = function(req, res) {
	Hospital.findOneAndUpdate({'_id': req.params.hospitalId}, req.body).exec(function(err, hospital) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(hospital);
		}
	});
};

/**
 * Delete a token entry
 */
exports.delete = function(req, res) {
	tokenNumber.remove({'service_name': req.params.service_name}, function(err, token) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(token);
		}
	});
};

/**
 * List of Services with token numbers
 */
exports.list = function(req, res) {
	tokenNumber.find().exec(function(err, tokens) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tokens);
		}
	});
};