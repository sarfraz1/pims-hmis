'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Patient = mongoose.model('IPDPatient'),
	BedPricing = mongoose.model('BedPricing'),
	_ = require('lodash');


/**
 * Register new patient
 */
exports.create = function(req, res) {

	var errors = req.validationErrors();

	if(errors) {
		return res.status(500).send(errors);
	} else {

		var patient = new Patient(req.body);
		patient.save(function(err, patient) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err),
					code : err.code
				});
			} else {

				res.status(200).send(patient);
			}
		});
	}

};

/**
 * Retrieve patient based on mr_no or phone
 */
exports.read = function(req, res) {
	var query;
	if(req.params.key) {
		query = Patient.find({$or: [{mr_number: req.params.key}, {phone: req.params.key}]}, {_id:0});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, patient){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(patient);
		}
	});
};

/**
 * Update a patient record
 */
exports.update = function(req, res) {
	delete req.body.__v;
	req.checkParams('key', 'MR number is required').notEmpty();
	var errors = req.validationErrors();

	var item = req.body;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Patient.findOneAndUpdate({'_id': req.params.key}, item, {upsert: true}, function(err, patient) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				res.status(200).send(patient);
			}
		});
	}
};

/**
 * Delete a Patient
 */
exports.delete = function(req, res) {
	Patient.remove({'mr_number':req.params.key} , function(err,patient) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(patient);
		}
	});
};

/**
 * Update medicine for in-patient
 */
exports.updatemedicine = function(req, res) {
	//req.checkParams('key', 'MR number is required').notEmpty();

	console.log('in update medicine');
	console.log(req.body.mr_number);
	console.log(req.body.medicine);
	var errors = req.validationErrors();

	var item = req.body;
	if(errors) {
		return res.status(500).send(errors);
	} else {
		Patient.findOne({'mr_number': req.body.mr_number}, function(err, patient) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
			else {
				console.log('patient found');
				patient.medicine = req.body.medicine;
				console.log('Patient Medicine: '+ patient.medicine);
				patient.save(function(err, patient) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err),
							code : err.code
						});
					} else {

						res.status(200).send(patient);
					}
				});
			}
		});
	}
};

/**
 * List of Patients
 */
exports.list = function(req, res) {
	Patient.find().exec(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log('returning patients');
			//console.log(patients);
			res.status(200).send(patients);
		}
	});
};

exports.list_admitted = function(req, res) {
	Patient.find({'status':'Admitted' }).exec(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log('returning admitted patients');
		//	console.log(patients);
			res.status(200).send(patients);
		}
	});
};


var keywordSearch = function(req, res) {
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	Patient.find({$or: [{'mr_number':{$regex : regex}},{'phone':{$regex : regex}},{'name': {$regex : regex}}]},'-__v').limit(10).exec(function(err, patients) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).json(patients);
		}
	});
};

exports.search = function(req, res) {
	keywordSearch(req, res);
};
