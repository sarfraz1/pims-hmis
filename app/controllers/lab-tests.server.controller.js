'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	LabTest = mongoose.model('LabTest'),
    _ = require('lodash');

/**
 * Create a Panel
 */
exports.create = function(req, res) {


	req.checkBody('description', 'Test Description is required').notEmpty();
	req.checkBody('category', 'Test Category is required').notEmpty();
	var errors = req.validationErrors();
	if (errors) {
		return res.status(500).send(errors);
	} else {
		var labTest = new LabTest(req.body);
		labTest.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(labTest);
			}
		});
	}
};

exports.read = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabTest.findOne({'_id': req.params.labTestId}).exec(function(err, labTest) {
			if (labTest) {
				res.json(labTest);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getByDescAndCategory = function(req, res) {


	if (!req.params.testDescription || !req.params.testCategory) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabTest.findOne({'description': req.params.testDescription,'category': req.params.testCategory}).exec(function(err, labTest) {
			if (labTest) {
				res.json(labTest);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.update = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		//req.checkBody('description', 'Description is required').notEmpty();
		//req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			LabTest.findOneAndUpdate({'_id': req.params.labTestId}, req.body).exec(function (err, labTest) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(labTest);
				}
			});
		}
	}
};

exports.delete = function(req, res) {
	if (!req.params.labTestId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabTest.remove({'_id': req.params.labTestId}).exec(function (err, labTest) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(labTest);
			}
		});
	}
};

exports.list = function(req, res) {
	LabTest.find().exec(function(err, labTest) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(labTest);
		}
	});
};
