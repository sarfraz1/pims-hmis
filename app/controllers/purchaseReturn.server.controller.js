'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	purchaseReturn = mongoose.model('purchaseReturn'),
	InventoryStock = mongoose.model('InventoryStock'),
	
	multiparty = require('multiparty'),
	fs = require('fs'),
	_ = require('lodash');

/**
 * Create a Purchase Return
 */

var checkData = function(itemsList,i,callback){
	
	if(itemsList[i]!==undefined){
		InventoryStock.findOne({'id':itemsList[i].code}).exec(function(err, inventoryStockItem) {
			var errorProduced = false;
			if(inventoryStockItem!==null){
				for(var j=0;j<inventoryStockItem.batch.length;j++){
					if(itemsList[i].batchId===inventoryStockItem.batch[j].batchID){
						var totalQuantity = 0;
						var matchIndexes = [];
						for(var k=j;k<inventoryStockItem.batch.length;k++){
							if(itemsList[i].batchId===inventoryStockItem.batch[k].batchID){
								totalQuantity+=inventoryStockItem.batch[k].quantity;
								matchIndexes.push(k);
							}
						}
						
						if(totalQuantity>=itemsList[i].quantity){
							var remQuantity = itemsList[i].quantity;
							for(var p=0;p<matchIndexes.length;p++) {
								if(inventoryStockItem.batch[matchIndexes[p]].quantity >= remQuantity) {
									inventoryStockItem.batch[matchIndexes[p]].quantity = inventoryStockItem.batch[matchIndexes[p]].quantity-remQuantity;
									itemsList[i].quantity = 0;
									break;
								} else if(inventoryStockItem.batch[matchIndexes[p]].quantity>0) {
									var qty = inventoryStockItem.batch[matchIndexes[p]].quantity;
									inventoryStockItem.batch[matchIndexes[p]].quantity = inventoryStockItem.batch[matchIndexes[p]].quantity-remQuantity;
									remQuantity = remQuantity - qty;
								}
							}
						}
						else {
							callback('Item(s) Quantity is Greater than avaliable stock');
							errorProduced = true;
						}
						break;
						
					} /*else if(itemsList[i].GRNNumber===undefined){
						if(inventoryStockItem.batch[j].quantity>=itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity-itemsList[i].quantity;
							itemsList[i].quantity = 0;
							break;
						}
						else if(inventoryStockItem.batch[j].quantity<itemsList[i].quantity){
							itemsList[i].quantity = itemsList[i].quantity - inventoryStockItem.batch[j].quantity;
						}
					
					} */
				}
				if(itemsList[i].quantity!==0 && errorProduced === false){
					console.log('callback qty 0');
					callback('Items Quantity is Greater than avaliable stock');
					errorProduced = true;
				}
				
			}
			if(errorProduced === false){
				i++;
			 	checkData(itemsList,i,callback);
			}
		});
	}else {
		console.log('callback Final');
		callback(null);
	}
};


var updateInventoryStock = function(itemsList,i){
	
	if(itemsList[i]!==undefined){
		InventoryStock.findOne({'id':itemsList[i].code}).exec(function(err, inventoryStockItem) {
			if(inventoryStockItem!==null){
				for(var j=0;j<inventoryStockItem.batch.length;j++){

					if(itemsList[i].batchId===inventoryStockItem.batch[j].batchID){
						var totalQuantity = 0;
						var matchIndexes = [];
						for(var k=j;k<inventoryStockItem.batch.length;k++){
							if(itemsList[i].batchId===inventoryStockItem.batch[k].batchID){
								totalQuantity+=inventoryStockItem.batch[k].quantity;
								matchIndexes.push(k);
							}
						}
						
						var remQuantity = itemsList[i].quantity;
						for(var p=0;p<matchIndexes.length;p++) {
							if(inventoryStockItem.batch[matchIndexes[p]].quantity >= remQuantity) {
								inventoryStockItem.batch[matchIndexes[p]].quantity = inventoryStockItem.batch[matchIndexes[p]].quantity-remQuantity;
								break;
							} else if(inventoryStockItem.batch[matchIndexes[p]].quantity>0) {
								var qty = inventoryStockItem.batch[matchIndexes[p]].quantity;
								inventoryStockItem.batch[matchIndexes[p]].quantity = inventoryStockItem.batch[matchIndexes[p]].quantity-remQuantity;
								remQuantity = remQuantity - qty;
							}
						}
						
						break;
					}
					/*else if(itemsList[i].GRNNumber===undefined){
						if(inventoryStockItem.batch[j].quantity>=itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity-itemsList[i].quantity;
							break;
						}
						else if(inventoryStockItem.batch[j].quantity<itemsList[i].quantity){
							itemsList[i].quantity = itemsList[i].quantity - inventoryStockItem.batch[j].quantity;
							inventoryStockItem.batch[j].quantity = 0;
						}
					
					}*/
				}
				inventoryStockItem.save(function(err,data) {
					
					i++;
				 	updateInventoryStock(itemsList,i);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};

exports.create = function(req, res) {
	var purchasereturn = new purchaseReturn(req.body);
	
	var itemlistCopy = JSON.parse(JSON.stringify(req.body.itemsList));
	
	checkData(itemlistCopy,0,function(err){
		if(err){
			return res.status(400).send({
				message: err
			});
		}else{

			updateInventoryStock(req.body.itemsList,0);
			
			purchaseReturn.findOne({'transcationCategory':req.body.transcationCategory}).sort({_id:-1}).exec(function(err, purchaseReturnObj) {
				if(req.body.transcationCategory==='Default'){
					
					if(purchaseReturnObj){
						var code = Number(purchaseReturnObj.purchaseReturnNumber)+1;
						code = String(code);
						
						purchasereturn.purchaseReturnNumber = code;
					} else if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} 
					else {
						purchasereturn.purchaseReturnNumber = '10000001';
					}
					purchasereturn.save(function(err,data) {
					 	if (err) {
					 		console.log(err);
					 		return res.status(400).send({
					 			message: errorHandler.getErrorMessage(err)
					 		});
					 	} else {

					 		res.json(data);
					 	}
					});
				}
				else {
					// Code for generating Number on category Basis with 
				}
		 	});
		}
	});
};

exports.getPurchaseOrderId = function(req, res) {
	
    var purchase_requisition = new purchaseRequisition(req.body);       
    purchase_requisition.save(function(err,data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.update = function(req, res) {
	var PurchaseOrder = new purchaseOrder(req.body);
	
	purchaseOrder.findOne().sort({_id:-1}).exec(function(err, purchaseOrderObj) {
		
		if(purchaseOrderObj){
			var code = Number(purchaseOrderObj.purchaseOrderNumber)+1;
			code = String(code);
			
			PurchaseOrder.purchaseOrderNumber = code;
		} else if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} 
		else {
			PurchaseOrder.purchaseOrderNumber = '10000001';
		}

	 	PurchaseOrder.save(function(err,data) {
		 	if (err) {
		 		console.log(err);
		 		return res.status(400).send({
		 			message: errorHandler.getErrorMessage(err)
		 		});
		 	} else {

		 		res.json(data);
		 	}
		});
 	});
};

exports.list = function(req, res) { 
	purchaseReturn.find().exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};

exports.listAll = function(req, res) { 
	var perPage = 10;
    var pageNo =  Number(req.params.pageNo);

	var query = purchaseOrder.find({}).sort({'_id':'-1'}).skip(pageNo * perPage);//.limit(perPage);
	query.exec(function(err, data) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(data);
		}
	});
};



/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

/**
  * find Purchase Return By Id
  */
 exports.PurchaseReturnById = function(req, res) {
	
	var ID = req.params.PR_ID;
	var	query =  purchaseReturn.find({'purchaseReturnNumber': ID})

	query.exec(function(err, purchaseReturnObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(purchaseReturnObj);
		}
	});
};

/**
  * Update Purchase Return
  */
exports.UpdatePurchaseReturn = function(req, res) {
	
	var Obj = req.body;
	var query;
	
	var itemlistCopy = JSON.parse(JSON.stringify(Obj.itemsList));

	
	query =  purchaseReturn.find({'purchaseReturnNumber': Obj.purchaseReturnNumber})
		
	query.exec(function(err, purchaseReturnObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			
			for (var i = 0; i < Obj.itemsList.length; i++) {
				updateAddInventoryStock(purchaseReturnObj[0].itemsList, Obj.itemsList, i);
			};
			
			 console.log("--------------------------------");
			 console.log(purchaseReturnObj[0].itemsList);
             console.log(Obj.itemsList.length);
             console.log(Obj.itemsList);
			 console.log("--------------------------------");
						
			purchaseReturnObj[0].itemsList = itemlistCopyM(purchaseReturnObj[0].itemsList , Obj.itemsList);
			

			purchaseReturnObj[0].save(function(err,data) {
					
					if (err) {
				 		console.log(err);
				 	}
			}) ;
			
			res.json(purchaseReturnObj);
		}
	});
};

var itemlistCopyM = function(Obj_M ,Obj){
	Obj_M = Obj;
	return Obj_M;
};

var updateAddInventoryStock = function(PitemsList, itemsList ,i ){

	
	if(itemsList[i]!==undefined){
		InventoryStock.findOne({'id':itemsList[i].code}).exec(function(err, inventoryStockItem) {
			if(inventoryStockItem !== null){
				for(var j=0;j<inventoryStockItem.batch.length;j++){
					
					if(itemsList[i].GRNNumber === inventoryStockItem.batch[j].grnNumber){
						
						console.log("--------------------------------");
						console.log(PitemsList[i].quantity +" "+ itemsList[i].quantity + " "+i);
						console.log("--------------------------------");
						
						if(PitemsList[i].quantity > itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity + (PitemsList[i].quantity - itemsList[i].quantity);	
						}

						if(PitemsList[i].quantity < itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity - (itemsList[i].quantity - PitemsList[i].quantity);	
						}
						break;
					}
					else if(itemsList[i].GRNNumber === undefined){
						
						if(PitemsList[i].quantity > itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity + (PitemsList[i].quantity - itemsList[i].quantity);	
						}

						if(PitemsList[i].quantity < itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity - (itemsList[i].quantity - PitemsList[i].quantity);	
						}
						break;
					
					}
				}
				inventoryStockItem.save(function(err,data) {
					
					//i++;
				 	//updateInventoryStock(itemsList, i  ,PitemsList);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};


/**
  * Get All Purchase Return 
   */
  exports.GetPurchaseReturn = function(req, res) {
	
	var ID = req.params.PR_ID;
	var query;
	
		query =  purchaseReturn.find({'purchaseReturnNumber': ID})
		


	query.exec(function(err, purchaseReturnObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(purchaseReturnObj);
		}
	});
};


var updateInventoryStockReturn = function(itemsList,i){
	
	if(itemsList[i]!==undefined){
		InventoryStock.findOne({'id':itemsList[i].code}).exec(function(err, inventoryStockItem) {
			if(inventoryStockItem!==null){
				for(var j=0;j<inventoryStockItem.batch.length;j++){

					if(itemsList[i].batchId===inventoryStockItem.batch[j].batchID){
						
						inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity+itemsList[i].quantity;	
						
						break;
					}
					/*else if(itemsList[i].GRNNumber===undefined){
						if(inventoryStockItem.batch[j].quantity>=itemsList[i].quantity){
							inventoryStockItem.batch[j].quantity = inventoryStockItem.batch[j].quantity-itemsList[i].quantity;
							break;
						}
						else if(inventoryStockItem.batch[j].quantity<itemsList[i].quantity){
							itemsList[i].quantity = itemsList[i].quantity - inventoryStockItem.batch[j].quantity;
							inventoryStockItem.batch[j].quantity = 0;
						}
					
					}*/
				}
				inventoryStockItem.save(function(err,data) {
					
					i++;
				 	updateInventoryStockReturn(itemsList,i);
				 	if (err) {
				 		console.log(err);
				 	} 
				});
			}
		});
	}
};

/**
  * Delete Purchase Return 
   */
  exports.DeletePurchaseReturn = function(req, res) {
	
	var	query =  purchaseReturn.findOne({'purchaseReturnNumber': req.params.code}).lean()

	query.exec(function(err, purchaseReturnObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			
			updateInventoryStockReturn(purchaseReturnObj.itemsList,0);
			
			purchaseReturn.remove({'purchaseReturnNumber': req.params.code}, function(err, preturn) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(preturn);
				}
			});
		}
	});
 };

