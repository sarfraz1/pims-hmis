'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryStore = mongoose.model('InventoryStore'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories store
 */
exports.create = function(req, res) {
	var inventoryStore = new InventoryStore(req.body);
	if (inventoryStore.defaultOption === false) {
		inventoryStore.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(inventoryStore);
			}
		});
	} else {
		InventoryStore.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, store) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				inventoryStore.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(inventoryStore);
					}
				});			
			}
		});
	}
};

/**
 * Show the current Inventories store
 */
exports.read = function(req, res) {
	InventoryStore.findOne({'_id': req.params.storeId}).exec(function(err, store) {
		if (store) {
			res.json(store);
		} else {
			err = 'Inventory store not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories store
 */
exports.update = function(req, res) {
	if (req.params.defaultOption === false) {
		InventoryStore.findOneAndUpdate({'_id': req.params.storeId}, req.body).exec(function(err, store){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(store);
			}
		});
	} else {
		InventoryStore.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, store) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				InventoryStore.findOneAndUpdate({'_id': req.params.storeId}, req.body).exec(function(err, store){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(store);
					}
				});		
			}
		});
	}
};

/**
 * Delete an Inventories store
 */
exports.delete = function(req, res) {
	InventoryStore.remove({'_id': req.params.storeId}, function(err, store) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(store);
		}
	});
};

/**
 * List of Inventories stores
 */
exports.list = function(req, res) {
	InventoryStore.find().exec(function(err, stores) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(stores);
		}
	});
};

/**
 * Retrieve array of Inventories Stores
 * To Do: Merge both search functions into one if functionality remains the same
 */
var searchByDescription = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryStore.find({'description':{$regex : regex}},'-__v').exec(function(err, stores) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
					return -1;
				if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
					return 1;
				return 0;
			};
			stores.sort(compare);
			res.json(stores);
		}
	});
};

exports.search = function(req, res) {
	searchByDescription(req, res);
};