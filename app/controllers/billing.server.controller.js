'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Billing = mongoose.model('Billing'),
	facilityDetails = mongoose.model('FacilityDetails'),
	Facility = mongoose.model('Facility'),
	FacilityPricing = mongoose.model('FacilityPricing'),
	tokenNumber = mongoose.model('tokenNumber'),
	Appointment = mongoose.model('Appointment'),
	User = mongoose.model('User'),
	_ = require('lodash'),
	async = require('async'),
	moment = require('moment'),
	facilityList,
	count;


/**
 * Create new Appointment
 */
exports.create = function(req, res) {

	req.checkBody('grandTotal', 'Grand Total is required').notEmpty();
	req.checkBody('patientInfo', 'Patient is required').notEmpty();
	req.checkBody('servicesInfo', 'Service is required').notEmpty();
	req.checkBody('patientInfo.mr_number', 'Patient MR No is required').notEmpty();
	req.checkBody('paymentMethod','Payment Method not specified').notEmpty();
	var errors = req.validationErrors();

	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {

		async.forEach(req.body.servicesInfo, function(service, callback) {

			if(service.category == 'Hospital-OPD-Consultation' && !service.token_number) {
			var date_str = moment().format('DD-MM-YYYY');
			tokenNumber.findOne({'service_name': service.description}).exec(function(err, token) {
				if (token) {
					//res.json(hospital);
					if(token.token_date !== date_str) {
						token.token_date = date_str;
						token.token_number = 1;
					} else {
						token.token_number = token.token_number + 1;
					}

					token.save(function (err) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							service.token_number = token.token_number;
							var dt = moment().format('DD-MM-YYYY');
							Appointment.findOne({appointment_date: dt,'doctor_name': service.description,'mr_number':req.body.patientInfo.mr_number, token_number: { $exists: false }}).exec(function(err, appt) {
								if(appt) {
									appt.token_number =  token.token_number;
									appt.save(function (err) {
										if(!err) {
											console.log('appt saved with token');
										}
									});
								} else {
									console.log('appt not found');
								}
								callback();
							});
						}
					});

				} else {
					var token = new tokenNumber();
					token.service_name = service.description;
					token.token_number = 1;
					token.token_date = date_str;

					token.save(function (err) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							service.token_number = token.token_number;
							var dt = moment().format('DD-MM-YYYY');
							Appointment.findOne({appointment_date: dt,'doctor_name': service.description,'mr_number':req.body.patientInfo.mr_number}).exec(function(err, appt) {
								if(appt) {
									appt.token_number =  token.token_number;
									appt.save(function (err) {
										if(!err) {
											console.log('appt saved with token');
										}
									});
								} else {
									console.log('appt not found');
								}
								callback();
							});
						}
					});
				}
			});
			} else {
				callback();
			}		//db.delete('messages', messageId, callback);
		}, function(err) {
			if (err) return next(err);

			Billing.findOne().sort({_id:-1}).limit(1).exec(function(err, bill) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				var newBill = new Billing(req.body);
				if(bill) {
					var code = Number(bill.billNumber)+1;
					newBill.billNumber = String(code);
					Billing.find({'patientInfo.mr_number':req.body.patientInfo.mr_number,'status':'Not billed'}).sort({_id:-1}).exec(function(err, patientbill) {
						if(err){
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else if(patientbill){
							var index=-1;
							var isPackage = false;

							//console.log(req.body.servicesInfo);
							if(req.body.servicesInfo[0].category){
								if(req.body.servicesInfo[0].category === "Hospital-IPD-Packages")
									isPackage = true;
							}

							if(isPackage){
								for(var i=0; i<patientbill.length; i++){
									for(var j=0;j<patientbill[i].servicesInfo.length;j++)
									if(patientbill[i].servicesInfo[j].category){
										if(patientbill[i].servicesInfo[j].category === "Hospital-IPD-Packages"){
											index = i;
										}
									}
								}
							} else {
								for(var i=0; i<patientbill.length; i++){
									for(var j=0;j<patientbill[i].servicesInfo.length;j++)
									if(patientbill[i].servicesInfo[j].category){
										if(patientbill[i].servicesInfo[j].category !== "Hospital-IPD-Packages"){
											index = i;
										}
									}
								}
							}
							
							if(index === -1) {
								//console.log(index);
								newBill.invoice.push({
									'recieved' : req.body.recievedCash,
									'cardInfo' : req.body.cardInfo,
									'chequeInfo' : req.body.chequeInfo,
									'paymentMethod' : req.body.paymentMethod,
									'handlerName' : req.body.handlerName
								});
								if(req.body.grandTotal !== req.body.recievedCash){
									newBill.status = 'Not billed';
								}
								newBill.save(function(err, resBill) {
									if (err) {
										console.log(err);
										return res.status(400).send({
											message: errorHandler.getErrorMessage(err)
										});
									} else {
										res.status(200).send(resBill);
									}
								});
							} else {
								//console.log(index);
								if(req.body.status === 'billed'){
									patientbill[index].servicesInfo = req.body.servicesInfo;
									patientbill[index].grandTotal = req.body.grandTotal;
									patientbill[index].recievedCash += req.body.recievedCash;
									if(req.body.grandTotal <= patientbill[index].recievedCash){
										patientbill[index].status = req.body.status;
									}
									patientbill[index].invoice.push({
										'recieved' : req.body.recievedCash,
										'cardInfo' : req.body.cardInfo,
										'chequeInfo' : req.body.chequeInfo,
										'paymentMethod' : req.body.paymentMethod,
										'handlerName' : req.body.handlerName
									});
								}
								else {
									for (var i=0; i<req.body.servicesInfo.length; i++){
									patientbill[index].servicesInfo.push(req.body.servicesInfo[i]);
								}
								}

								patientbill[index].referedDoctor = req.body.referedDoctor;
								patientbill[index].referingDoctor = req.body.referingDoctor;
								// patientbill.paymentMethod = req.body.paymentMethod;
								// patientbill.cardInfo = req.body.cardInfo;


								patientbill[index].save(function(err, resBill) {
									if (err) {
										console.log(err);
										return res.status(400).send({
											message: errorHandler.getErrorMessage(err)
										});
									} else {
										res.json(resBill);
									}
								});
						}
						}else {
							newBill.invoice.push({
								'recieved' : req.body.recievedCash,
								'cardInfo' : req.body.cardInfo,
								'chequeInfo' : req.body.chequeInfo,
								'paymentMethod' : req.body.paymentMethod,
								'handlerName' : req.body.handlerName
							});
							if(req.body.grandTotal !== req.body.recievedCash){
								newBill.status = 'Not billed';
							}
							newBill.save(function(err, resBill) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									res.status(200).send(resBill);
								}
							});
						}
					});
				}
				else {
					newBill.billNumber = '100010';
					if(req.body.grandTotal !== req.body.recievedCash){
						newBill.status = 'Not billed';
					}
					newBill.invoice.push({
						'recieved' : req.body.recievedCash,
						'cardInfo' : req.body.cardInfo,
						'chequeInfo' : req.body.chequeInfo,
						'paymentMethod' : req.body.paymentMethod,
						'handlerName' : req.body.handlerName
					});
					newBill.save(function(err, resBill) {
						if (err) {
							console.log(err);
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							res.status(200).send(resBill);
						}
					});
				}


			}
		});

		});

	}
};

exports.addservicestobill = function(req, res) {
	Billing.findOne({'patientInfo.mr_number':req.body.mr_number,'status':'Not billed'}).sort({_id:-1}).exec(function(err, patientbill) {
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(patientbill){
			var servtoadd = req.body.servicesInfo;
			for(var j=0;j<servtoadd.length;j++) {
				patientbill.servicesInfo.push(servtoadd[j]);
				patientbill.grandTotal+=servtoadd[j].fee;
			}

				patientbill.save(function(err, resBill) {
					if (err) {
						console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.status(200).send(resBill);
					}
				});
		}
	});
};

exports.addservicesbynametobill = function(req, res) {
	var query;
	var facilities = [];
	async.forEach(req.body.services, function(service, callback) {
		var regex = new RegExp(service.category, 'i');
		query = FacilityPricing.findOne({category:{$regex: regex}, description:service.service_name},'-__v');
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				facilities.push(facility);
				callback();
			}
		});
	}, function(err) {
			if (err)
				return next(err);

			else {
				Billing.findOne({'patientInfo.mr_number':req.body.mr_number,'status':'Not billed'}).sort({_id:-1}).exec(function(err, patientbill) {
					if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else if(patientbill){

					for(var i=0;i<facilities.length;i++) {
						var servtoadd = {
							'service_id': facilities[i]._id,
							'description': facilities[i].description,
							'category': facilities[i].category,
							'fee': facilities[i].price,
							'quantity': 1,
							'discount': 0
						};
						patientbill.servicesInfo.push(servtoadd);
						patientbill.grandTotal+=servtoadd.fee;
					}

						if(!patientbill.referingDoctor) {
							patientbill.referingDoctor = {
								'name': req.body.doctor_name,
								'_id': req.body.doctor_id,
								'speciality': req.body.speciality
							};
						}
						patientbill.save(function(err, resBill) {
							if (err) {
								console.log(err);
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								res.status(200).send(resBill);
							}
						});
				} else {

					Billing.findOne().sort({_id:-1}).limit(1).exec(function(err, bill) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							var newBill = new Billing();

								newBill.grandTotal = 0;
								newBill.recievedCash = 0;
								newBill.discount =  0;
								newBill.status = 'Not billed';
								newBill.patientInfo = {
									'mr_number': req.body.mr_number,
									'name': req.body.name,
									'phone': req.body.phone,
								};
								newBill.servicesInfo = [];

								for(var i=0;i<facilities.length;i++) {
									var servtoadd = {
										'service_id': facilities[i]._id,
										'description': facilities[i].description,
										'category': facilities[i].category,
										'fee': facilities[i].price,
										'quantity': 1,
										'discount': 0
									};
									newBill.servicesInfo.push(servtoadd);
									newBill.grandTotal+=servtoadd.fee;
								}

								newBill.referingDoctor = {
									'name': req.body.doctor_name,
									'_id': req.body.doctor_id,
									'speciality': req.body.speciality
								};

							if(bill) {
								var code = Number(bill.billNumber)+1;
								newBill.billNumber = String(code);
							}
							newBill.save(function(err, resBill) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									res.status(200).send(resBill);
								}
							});
						}
					});
				}
			});
		}
	});
};

exports.addservicebynametobill = function(req, res) {
	//console.log(req.body);
	var query;
	var regex = new RegExp(req.body.category, 'i');
	query = FacilityPricing.findOne({category:{$regex: regex}, description:req.body.service_name},'-__v');
	query.exec(function(err, facility){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(facility){
			Billing.findOne({'patientInfo.mr_number':req.body.mr_number,'status':'Not billed'}).sort({_id:-1}).exec(function(err, patientbill) {
				if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else if(patientbill){
					var servtoadd = {
						'service_id': facility._id,
						'description': facility.description,
						'category': facility.category,
						'fee': facility.price,
						'quantity': 1,
						'discount': 0
					};

						patientbill.servicesInfo.push(servtoadd);
						patientbill.grandTotal+=servtoadd.fee;
						patientbill.referingDoctor = {
							'name': req.body.doctor_name,
							'_id': req.body.doctor_id,
							'speciality': req.body.speciality
						};
						patientbill.save(function(err, resBill) {
							if (err) {
								console.log(err);
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								res.status(200).send(resBill);
							}
						});
				} else {

					Billing.findOne().sort({_id:-1}).limit(1).exec(function(err, bill) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							var newBill = new Billing();

								newBill.grandTotal =  facility.price;
								newBill.recievedCash = 0;
								newBill.discount =  0;
								newBill.status = 'Not billed';
								newBill.patientInfo = {
									'mr_number': req.body.mr_number,
									'name': req.body.name,
									'phone': req.body.phone,
								};
								newBill.servicesInfo = [{
									'service_id': facility._id,
									'description': facility.description,
									'category': facility.category,
									'fee': facility.price,
									'quantity': 1,
									'discount': 0
								}];

								newBill.referingDoctor = {
									'name': req.body.doctor_name,
									'_id': req.body.doctor_id,
									'speciality': req.body.speciality
								};

							if(bill) {
								var code = Number(bill.billNumber)+1;
								newBill.billNumber = String(code);
							}
							newBill.save(function(err, resBill) {
								if (err) {
									console.log(err);
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									res.status(200).send(resBill);
								}
							});
						}
					});
				}
			});
		}
	});
};

var dateConverter = function(dateinput) {
	try{
		var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
		utcDate = utcDate.toUTCString();
		return utcDate;
	}
	catch(error){
		return dateinput;
	}
};

var getFacilitiesArray = function(facilityName,callback){
	Facility.findOne({'name' : facilityName}).exec(function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log(count);
			count--;
			if(facility.children.length===0){
				facilityList.push(facilityName);
			} else {

				for(var i=0;i<facility.children.length;i++){
					getFacilitiesArray(facility.children[i],callback);
				}
			}

			if(count===0){
				callback();
			}
		}
	});
};

exports.discountReportPatientWise = function(req, res) {
	var fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
	}

	var query;

	if(req.params.receptionist !== 'All') {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate},'servicesInfo.discount': {$gt:0}, 'invoice.handlerName': req.params.receptionist});
	} else {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate},'servicesInfo.discount': {$gt:0}});
	}
	query.exec(function(err, bills){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var datatosend = [];
			for(var i=0; i<bills.length; i++) {
				for(var j=0; j<bills[i].servicesInfo.length;j++) {
					if(bills[i].servicesInfo[j].discount) {
						datatosend.push({
						'billNumber': bills[i].billNumber,
						'patientInfo': bills[i].patientInfo,
						'handlerName': bills[i].invoice[bills[i].invoice.length-1].handlerName,
						'created': bills[i].created,
						'description': bills[i].servicesInfo[j].description,
						'fee': bills[i].servicesInfo[j].fee,
						'discount': bills[i].servicesInfo[j].discount

						});
					}
				}
			}
			res.status(200).send(datatosend);
		}
	});
};

exports.refundReportPatientWise = function(req, res) {
	var fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
	}

	var query;

	if(req.params.receptionist !== 'All') {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate},'invoice.recieved': {$lt:0}, 'invoice.handlerName': req.params.receptionist});
	} else {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate},'invoice.recieved': {$lt:0}});
	}
	query.exec(function(err, bills){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var datatosend = [];
			for(var i=0; i<bills.length; i++) {
				for(var j=0; j<bills[i].servicesInfo.length;j++) {
					if(bills[i].servicesInfo[j].refund) {
						datatosend.push({
						'billNumber': bills[i].billNumber,
						'patientInfo': bills[i].patientInfo,
						'handlerName': bills[i].invoice[bills[i].invoice.length-1].handlerName,
						'created': bills[i].created,
						'description': bills[i].servicesInfo[j].description,
						'fee': bills[i].servicesInfo[j].fee,
						'refund': bills[i].servicesInfo[j].refund,
						'refundReason': bills[i].servicesInfo[j].refundReason

						});
					}
				}
			}
			res.status(200).send(datatosend);
		}
	});
};

exports.revenueReportPatientWise = function(req, res) {
	var fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		//console.log('Toooo date: '+ toDate);
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
		//console.log('tttooo Date '+ toDate);

		var tdate = moment(toDate).add(5, 'hours');
		toDate = new Date(tdate.format());

		var fdate = moment(fromDate).subtract(5, 'hours');
		fromDate = new Date(fdate.format());
	}

	var query;

	if(req.params.receptionist !== 'All') {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate}, 'invoice.handlerName': req.params.receptionist, 'status': 'billed'});
	} else {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate}, 'status': 'billed'});
	}

	query.exec(function(err, bills){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log(bills);

			Facility.count().exec(function(err, c) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {

						User.find({},{username:1, roles:1}).exec(function(err, userList) {
							if(err){
								return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
						var userRole ='';
						var consultationindex;
						var refundindex;
						var balanceindex;
						var discountindex;
						var ipdIndex;
						facilityList = [];
						count = c;
						getFacilitiesArray('Hospital',function(){
							var facilities = facilityList;
							facilities.unshift('Registration');
							facilities.push('Refund');
							facilities.push('Discount');

							var facilityAmounts = [];
							var report = [];
							for(var i=0;i<facilities.length;i++){
								facilityAmounts.push({
									facilityName : facilities[i],
									amount : 0
								});

								if(facilities[i] == "Consultation") {
									consultationindex = i;
								} else if(facilities[i] == "Refund") {
									refundindex = i;
								} else if(facilities[i] == "Discount") {
									discountindex = i;
								} else if(facilities[i] == "IPD"){
									ipdIndex = i;
								}

							}


							for(i=0;i<bills.length;i++){
								var date = dateConverter(bills[i].created);
								report.push({
									'MRNo' : bills[i].patientInfo.mr_number,
									'Date' : moment(date).format("DD-MM-YYYY"),
									'actDate' : bills[i].created,
									'Name' : bills[i].patientInfo.name,
									'panel_id' : bills[i].patientInfo.panel_id,
									'referedDoctor' : bills[i].referedDoctor.name,
									'referingDoctor' : bills[i].referingDoctor.name,
									'grandTotal' : bills[i].grandTotal,
									'receivedCash' : bills[i].recievedCash,
									'balance' : (bills[i].grandTotal-bills[i].recievedCash),
									'facilities' : JSON.parse(JSON.stringify(facilityAmounts))
								});

								for(var j=0;j<bills[i].servicesInfo.length;j++){
									var registrationDone = false;
									var consultationDone = false;
									for(var k=0;k<report[i].facilities.length;k++){
										if(bills[i].servicesInfo[j].category === undefined && bills[i].servicesInfo[j].service_id==='registration'){
											if(registrationDone===false){
												registrationDone = true;
												//console.log(" in registration category: " + bills[i].servicesInfo[j].category);
												report[i].facilities[0].amount +=  (bills[i].servicesInfo[j].fee*bills[i].servicesInfo[j].quantity);

												if(bills[i].servicesInfo[j].discount>0) {
													report[i].facilities[discountindex].amount +=bills[i].servicesInfo[j].discount;
												}
												//report[i].grandTotal+=report[i].facilities[0].amount;
											}
										} else if(bills[i].servicesInfo[j].service_id == 'consultation') {
											if(consultationDone == false) {
												consultationDone = true;
												//console.log(" in consultation category: " + bills[i].servicesInfo[j].category);
												report[i].facilities[consultationindex].amount += bills[i].servicesInfo[j].quantity*(bills[i].servicesInfo[j].fee);

												if(bills[i].servicesInfo[j].discount>0) {
													report[i].facilities[discountindex].amount +=bills[i].servicesInfo[j].discount;
												}
											}

										}
										else if(bills[i].servicesInfo[j].category){
											if(bills[i].servicesInfo[j].category.indexOf(report[i].facilities[k].facilityName)>-1){
												//console.log(" category: " + bills[i].servicesInfo[j].category);
												//console.log(" facilityName: " + report[i].facilities[k].facilityName);
												var n = bills[i].servicesInfo[j].category.lastIndexOf("-");
												var str = bills[i].servicesInfo[j].category.substr(n+1,bills[i].servicesInfo[j].category.length);
												//console.log('str: '+str);
												if(str == report[i].facilities[k].facilityName) {
													report[i].facilities[k].amount += bills[i].servicesInfo[j].quantity*(bills[i].servicesInfo[j].fee);
													if(bills[i].servicesInfo[j].discount>0) {
														report[i].facilities[discountindex].amount +=bills[i].servicesInfo[j].discount;
													}
												}
												//report[i].grandTotal+=report[i].facilities[k].amount;
											}
										}
									}

								}

								for(var k=0;k<bills[i].invoice.length;k++){
									if(bills[i].invoice[k].recieved < 0) {
										report[i].facilities[refundindex].amount += (bills[i].invoice[k].recieved*-1);
									}
								}

								for(var l=0;l<bills[i].servicesInfo.length;l++){

									if(bills[i].servicesInfo[l].category!==undefined){
										if(bills[i].servicesInfo[l].category === "Hospital-IPD-Packages") {
											report[i].facilities[ipdIndex].amount =bills[i].recievedCash;
								}
							} 
						}
							}
							res.status(200).send(report);
						});
					}
					});
				}
			});
		}
	});
};



exports.panelReport = function(req, res) {
	var fromDate = req.params.fromDate,
	 	toDate = req.params.toDate;

	if (fromDate === 'undefined' || fromDate==='null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate==='null'){
		toDate = new Date();
	}
	else {
		//console.log('frm date: '+ fromDate);
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
		fromDate = dateConverter(fromDate);
	//	var tdate = moment(toDate).add(5, 'hours');
	//	toDate = new Date(tdate.format());

	//	var fdate = moment(fromDate).subtract(5, 'hours');
	//	fromDate = new Date(fdate.format());
	}

	var query;

	var datatosend = [];
	if(req.params.panel !== 'All') {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate}, 'status': 'billed', 'patientInfo.panel_id': req.params.panel});
	} else {
		query = Billing.find({'created': {$lte : toDate, $gte: fromDate}, 'status': 'billed', 'patientInfo.panel_id': {$exists: true , $ne: 'None'}});
	}
	query.exec(function(err, bills){
		var total = 0,
			totalPanelClaim = 0;
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			for(var i=0;i<bills.length;i++) {
				for(var j=0;j<bills[i].servicesInfo.length;j++) {
					if(bills[i].servicesInfo[j].discount) {
						datatosend.push({
									billNumber: bills[i].billNumber,
									mr_number: bills[i].patientInfo.mr_number,
									panelCardNumber: bills[i].patientInfo.panelCardNumber,
									name: bills[i].patientInfo.name,
									created: bills[i].created,
									service: bills[i].servicesInfo[j].description,
									total: bills[i].servicesInfo[j].fee*bills[i].servicesInfo[j].quantity,
									panelClaim: bills[i].servicesInfo[j].discount,
									panelPaymentReceived: bills[i].panelPaymentReceived
							});
							total = total+bills[i].servicesInfo[j].discount;
							if(!bills[i].panelPaymentReceived) {
								totalPanelClaim = totalPanelClaim + bills[i].servicesInfo[j].discount;
							}
					}
				}
			}
		}
		var tosend = {};
		tosend.bills = datatosend;
		tosend.total = total;
		tosend.totalPanelClaim = totalPanelClaim;
		res.status(200).send(tosend);
	});
};

exports.totalRevenueReport = function(req, res) {
	var fromDate = req.params.fromDate,
		toDate = req.params.toDate;



	if (fromDate === 'undefined' || fromDate === 'null')
		fromDate = new Date();
	if (toDate === 'undefined' || toDate === 'null'){
		toDate = new Date();
	} else {
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
	}

	Billing.find({'created': {$lte : toDate, $gte: fromDate}}).exec(function(err, bills){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var dateWiseRevenue = [];
			for(var i=0;i<bills.length;i++){
				var dateFound = false;
				for(var j=0;j<dateWiseRevenue.length;j++){
					if(dateWiseRevenue[j].createdDate === dateConverter(bills[i].created)){
						dateFound = true;
						for(var k=0;k<bills[i].servicesInfo.length;k++){
							dateWiseRevenue[j].service.push(bills[i].servicesInfo[k]);
						}
						break;
					}
				}
				if(!dateFound){
					dateWiseRevenue.push({
						createdDate : dateConverter(bills[i].created),
						service : []
					});
					for(var k=0;k<bills[i].servicesInfo.length;k++){
						dateWiseRevenue[dateWiseRevenue.length-1].service.push(bills[i].servicesInfo[k]);
					}
				}
			}

			for(i=0;i<dateWiseRevenue.length;i++){
				for(j=0;j<dateWiseRevenue[i].service.length;j++){
					for(k=j+1;k<dateWiseRevenue[i].service.length;k++){
						if(dateWiseRevenue[i].service[j].service_id === dateWiseRevenue[i].service[k].service_id){
							dateWiseRevenue[i].service[j].fee += dateWiseRevenue[i].service[k].fee-(dateWiseRevenue[i].service[k].fee*(dateWiseRevenue[i].service[k].discount/100));
							dateWiseRevenue[i].service[j].quantity += dateWiseRevenue[i].service[k].quantity;
							dateWiseRevenue[i].service.splice(k,1);
							k--;

						}
					}
				}
			}
			//console.log(dateWiseRevenue);
			res.status(200).send(dateWiseRevenue);
		}
	});
};

exports.list = function(req, res) {
	Billing.find({}).exec(function(err, bills){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log(bills);
			res.status(200).send(bills);
		}
	});
};

exports.patientBill = function(req, res) {
	var query;
	if(req.params.MRN) {
		//console.log(req.params.MRN);
		query = Billing.findOne({'patientInfo.mr_number':req.params.MRN,'status':'Not billed'}).sort({'created': -1});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, billing){
		//console.log(billing);
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(billing);
		}
	});
};

exports.patientBillRP = function(req, res) {
	var query;
	if(req.params.MRN) {
		//console.log(req.params.MRN);
		query = Billing.findOne({'servicesInfo.category': { $ne: 'Hospital-IPD-Packages'},'patientInfo.mr_number':req.params.MRN,'status':'Not billed'}).sort({'created': -1});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, billing){
		//console.log(billing);
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(billing);
		}
	});
};

exports.patientHistory = function(req, res) {
	var query;
	if(req.params.MRN) {
		query = Billing.find({'patientInfo.mr_number':req.params.MRN,'status':'billed'}).sort({'created': -1});
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, billing){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(billing);
		}
	});
};



exports.read = function(req, res) {
	var query;
	if(req.params.appointment_id) {
		query = Appointment.findById(req.params.appointment_id);
	} else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

	query.exec(function(err, appointment){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(appointment);
		}
	});
};

/**
 * Save refund on a bill
 */

 exports.refundBill = function(req, res) {
	Billing.findOne({'billNumber':req.params.billNum} , function(err,billRes) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			if(req.body.refundAmount<=billRes.recievedCash){
				billRes.recievedCash = billRes.recievedCash - req.body.refundAmount;
				billRes.invoice.push({
					'recieved' :-req.body.refundAmount,
					'refundReason' : req.body.refundRemarks,
					'handlerName': req.body.handler,
					'cardInfo' : req.body.cardInfo,
					'chequeInfo' : req.body.chequeInfo,
					'paymentMethod' : req.body.paymentMethod
				});

				billRes.servicesInfo = req.body.servicesInfo;

				billRes.save(function(err, resBill) {
					if (err) {
						console.log(err);
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.status(200).send(billRes);
					}
				});
			}
			else {
				return res.status(400).send({
					message: 'Amount is greator than recieved cash!'
				});
			}
			//res.sendStatus(200).send(appointment);
		}
	});
};

exports.delete = function(req, res) {
	Appointment.remove({'_id':req.params.appointment_id} , function(err,appointment) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.sendStatus(200).send(appointment);
		}
	});
};

/**
 * Retrieve billings specific to doctor and from/to dates
 */
exports.report = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var doctorName = req.params.doctorName;
	var query;
	var regex = new RegExp(req.params.doctorName, 'i');
	if (doctorName === 'All'){
		if((fromDate === 'undefined' || fromDate === 'null') && (toDate === 'undefined' || toDate === 'null')) {
			query = Billing.find();
		} else if(toDate === 'undefined' || toDate === 'null') {
			query = Billing.find({'created': {'$gte' : new Date(fromDate)}});
		} else {
			query = Billing.find({'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
		}
	}
	else {
		if ((fromDate === 'undefined' || fromDate === 'null') && (toDate === 'undefined' || toDate === 'null')) {
			query =  Billing.find({'$or' : [{'servicesInfo.description': {$regex : regex}},{'referedDoctor.name':doctorName}]});
		} else if(toDate==='undefined' || toDate==='null') {
			query =  Billing.find({'$or' : [{'servicesInfo.description': {$regex : regex}},{'referedDoctor.name':doctorName}], 'created': {'$gte' : new Date(fromDate)}});
		} else {
			query =  Billing.find({'$or' : [{'servicesInfo.description': {$regex : regex}},{'referedDoctor.name':doctorName}], 'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
		}
	}

	query.exec(function(err, bills) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//console.log(bills);
			res.json(bills);
		}
	});
};

var getAllServicesDetail = function(services,index,doctorId,callback){
	if(services[index]){
		var query =  facilityDetails.findOne({'description':services[index].description,'category':services[index].category,'shares.doctorId' : doctorId});
		query.exec(function(err, service) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				if(service){
					var doctorShare = false;
					for(var i=0;i<service.shares.length;i++){
						if(service.shares[i].doctorId===doctorId){
							services[index].docCommPercentage = service.shares[i].commissionPerc;
							doctorShare = true;
						}
					}

					if(doctorShare === false){
						services[index].docCommPercentage = 0;
					}
				} else {
					services[index].docCommPercentage = 0;
				}

				index++;
				getAllServicesDetail(services,index,doctorId,callback)

			}
		});
	} else {
		callback(services);
	}
};

var getAllBilledServices = function(bills,index,servicesList,callback){
	if(bills[index]){
		for(var i=0;i<bills[index].servicesInfo.length;i++){
			bills[index].servicesInfo[i].docCommPercentage = 0;
			var temp = {
				description : bills[index].servicesInfo[i].description,
				fee : bills[index].servicesInfo[i].fee,
				quantity : bills[index].servicesInfo[i].quantity,
				discount : bills[index].servicesInfo[i].discount,
				service_id : bills[index].servicesInfo[i].service_id,
				category : bills[index].servicesInfo[i].category,
				docCommPercentage : 0,
			};
			servicesList.push(temp);
		}
		index++;
		getAllBilledServices(bills,index,servicesList,callback);
	} else {
		callback(servicesList);
	}
};

exports.servicesReport = function(req, res) {
	var fromDate = req.params.fromDate,
		toDate = req.params.toDate,
		doctorId = req.params.doctorId,
		doctorName = req.params.doctorName,
		query;

	if (doctorId === 'All'){
		if(fromDate !== 'undefined' && fromDate !== 'null' && toDate !== 'undefined' && toDate !== 'null') {
			toDate = new Date(toDate);
			toDate.setDate(toDate.getDate() + 1);
			toDate = dateConverter(toDate);
			query = Billing.find({'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});

		} else if(fromDate !== 'undefined' && fromDate !== 'null') {
			query = Billing.find({'created': {'$gte' : new Date(fromDate)}});
		} else {
			query = Billing.find();
		}
	}
	else {
		if (fromDate !== 'undefined' && fromDate !== 'null' && toDate !== 'undefined' && toDate !== 'null') {
			toDate = new Date(toDate);
			toDate.setDate(toDate.getDate() + 1);
			toDate = dateConverter(toDate);
			query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referedDoctor._id': doctorId}], 'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
		} else if(fromDate !== 'undefined' && fromDate !== 'null') {
			query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referedDoctor._id': doctorId}], 'created': {'$gte' : new Date(fromDate)}});
		} else {
			query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referedDoctor._id': doctorId}]});
		}
	}


	query.exec(function(err, bills) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var responseServicesList = [];
			getAllBilledServices(bills,0,[],function(servicesList){
				for(var i=0;i<servicesList.length;i++){
					for(var j=i+1;j<servicesList.length;j++){
						if(servicesList[i].service_id===servicesList[j].service_id){
							servicesList[i].quantity +=  servicesList[j].quantity;
							servicesList.splice(j,1);
							j--;
						}
					}
				}
				responseServicesList = servicesList;
			//	console.log(servicesList);
				getAllServicesDetail(responseServicesList,0,doctorId,function(serviceRes){
					//console.log(serviceRes);
					for(var j=0;j<serviceRes.length;j++){
						if(serviceRes[j].docCommPercentage===0){
							serviceRes.splice(j,1);
							j--;
						}
					}
					res.json(serviceRes);
				});
			});

		}
	});
};

	var getIndex = function(array, prop1, prop2, value1, value2) {
		var index = -1;
		for (var x = 0; x < array.length; x++) {
			if (array[x][prop1] === value1 && array[x][prop2] === value2) {
				index = x;
			}
		}
		return index;
	};

exports.updatePanelBillsStatus = function(req, res) {
	var bills = req.body.bills,
	falseBills = [],
	trueBills = [];

	for(var i=0;i<bills.length;i++) {
		if(bills[i].panelPaymentReceived) {
			trueBills.push(bills[i].billNumber);
		} else {
			falseBills.push(bills[i].billNumber);
		}
	}

	if(trueBills.length && falseBills.length) {
		Billing.update({billNumber: {$in: trueBills}}, {$set: {panelPaymentReceived: true}},{ multi: true })
		.exec(function(err, bills) {
			if(!err) {
				Billing.update({billNumber: {$in: falseBills}}, {$set: {panelPaymentReceived: false}},{ multi: true })
				.exec(function(err, bills) {
					if(!err) {
						return res.status(200).send({
							message: 'success'
						});
					} else {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					}
				})
			} else {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}

		});
	} else if(trueBills.length) {
		Billing.update({billNumber: {$in: trueBills}}, {$set: {panelPaymentReceived: true}},{ multi: true })
		.exec(function(err, bills) {
			if(!err) {
				return res.status(200).send({
					message: 'success'
				});
			} else {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	} else if (falseBills.length) {
		Billing.update({billNumber: {$in: falseBills}}, {$set: {panelPaymentReceived: false}},{ multi: true })
				.exec(function(err, bills) {
					if(!err) {
						return res.status(200).send({
							message: 'success'
						});
					} else {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					}

		});
	}
}

exports.otaPayableReport = function(req, res){
	
			var fromDate = req.params.fromDate,
				toDate = req.params.toDate,
				otaName = req.params.otaName,
				query;
			var datatosend = [];

			if (fromDate !== 'undefined' && fromDate !== 'null' && toDate !== 'undefined' && toDate !== 'null') {
				toDate = new Date(toDate);
				toDate.setDate(toDate.getDate() + 1);
				toDate = dateConverter(toDate);
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}], 'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
			} else if(fromDate !== 'undefined' && fromDate !== 'null') {
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}], 'created': {'$gte' : new Date(fromDate)}});
			} else {
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}]});
			}

			query.exec(function(err, result){
				if (err) {
					return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
					});
				} else {
					//console.log(result);
					for(var i=0;i<result.length;i++){
						var docshare=0;
						var refund=0;
						var totalRecieved=result[i].recievedCash;
						var recentHandler=result[i].invoice[result[i].invoice.length-1].handlerName;
						for(var j=0; j<result[i].servicesInfo.length;j++){

								if(result[i].servicesInfo[j].refund) {
									refund+= result[i].servicesInfo[j].refund;
								}
								if(result[i].servicesInfo[j].OTA.name===otaName){
								datatosend.push({
									'billNumber': result[i].billNumber,
									'patientInfo': result[i].patientInfo,
									'handlerName': recentHandler,
									'created': result[i].created,
									'description': result[i].servicesInfo[j].description,
									'fee': result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity,
									'refund': refund,
									'discount': result[i].servicesInfo[j].discount,
									'total': totalRecieved,
									'docshare': Math.round((result[i].servicesInfo[j].OTA.fee/(result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity))*totalRecieved)
								});
							}
						}
					}
					//console.log(datatosend);
					res.json(datatosend);
				}
			});
		}

exports.payableReport = function(req, res) {

			var userRole ='';
			var fromDate = req.params.fromDate,
				toDate = req.params.toDate,
				doctorId = req.params.doctorId,
				doctorName = req.params.doctorName,
				query;
			var datatosend = [];

			if (fromDate !== 'undefined' && fromDate !== 'null' && toDate !== 'undefined' && toDate !== 'null') {
				toDate = new Date(toDate);
				toDate.setDate(toDate.getDate() + 1);
				toDate = dateConverter(toDate);
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}], 'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
			} else if(fromDate !== 'undefined' && fromDate !== 'null') {
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}], 'created': {'$gte' : new Date(fromDate)}});
			} else {
				query =  Billing.find({'$or' : [{'servicesInfo.category': "Hospital-IPD-Packages"}]});
			}

			query.exec(function(err, result){
				if (err) {
					return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
					});
				} else {
					for(var i=0;i<result.length;i++){
						var docshare=0;
						var refund=0;
						var totalRecieved=0;
						var recentHandler;
						for(var i=0;i<result.length;i++){
						var docshare=0;
						var refund=0;
						var totalRecieved=result[i].recievedCash;
						var recentHandler=result[i].invoice[result[i].invoice.length-1].handlerName;
						for(var j=0; j<result[i].servicesInfo.length;j++){

								if(result[i].servicesInfo[j].refund) {
									refund+= result[i].servicesInfo[j].refund;
								}
								if(result[i].servicesInfo[j].Surgeon.name===doctorName){
								datatosend.push({
									'billNumber': result[i].billNumber,
									'patientInfo': result[i].patientInfo,
									'handlerName': recentHandler,
									'created': result[i].created,
									'description': result[i].servicesInfo[j].description,
									'fee': result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity,
									'refund': refund,
									'discount': result[i].servicesInfo[j].discount,
									'total': totalRecieved,
									'docshare': Math.round((result[i].servicesInfo[j].Surgeon.fee/(result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity))*totalRecieved)
								});
							} else if(result[i].servicesInfo[j].Anesthetist.name===doctorName){
								datatosend.push({
									'billNumber': result[i].billNumber,
									'patientInfo': result[i].patientInfo,
									'handlerName': recentHandler,
									'created': result[i].created,
									'description': result[i].servicesInfo[j].description,
									'fee': result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity,
									'refund': refund,
									'discount': result[i].servicesInfo[j].discount,
									'total': totalRecieved,
									'docshare': Math.round((result[i].servicesInfo[j].Anesthetist.fee/(result[i].servicesInfo[j].fee*result[i].servicesInfo[j].quantity))*totalRecieved)
								});
							}
						}
					}
				}
					//console.log("this is result " + datatosend.length);
			}
		});

			query =  facilityDetails.find({'shares.doctorId': doctorId});

			query.exec(function(err, details) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else if(details) {
					//console.log('found details' + details);
					if (fromDate !== 'undefined' && fromDate !== 'null' && toDate !== 'undefined' && toDate !== 'null') {
						toDate = new Date(toDate);
						toDate.setDate(toDate.getDate() + 1);
						toDate = dateConverter(toDate);
						query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referingDoctor._id': doctorId}], 'status': 'billed', 'created': {'$gte' : new Date(fromDate), '$lte' : new Date(toDate)}});
					} else if(fromDate !== 'undefined' && fromDate !== 'null') {
						query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referingDoctor._id': doctorId}], 'status': 'billed', 'created': {'$gte' : new Date(fromDate)}});
					} else {
						query =  Billing.find({'$or' : [{'servicesInfo.description': doctorName},{'referingDoctor._id': doctorId}], 'status': 'billed'});
					}

					query.exec(function(err, bills) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							//console.log('bills query'+ bills);
							for(var i=0; i<bills.length; i++) {
								for(var j=0; j<bills[i].servicesInfo.length; j++) {
									var share=0;
									var refund=0;
									var ind = getIndex(details, 'description', 'category', bills[i].servicesInfo[j].description, bills[i].servicesInfo[j].category);
									if(ind !== -1) {
										//console.log('bills[i]: '+bills[i]);
										for(var k=0;k<details[ind].shares.length;k++) {
											if(details[ind].shares[k].doctorId == doctorId) {
												//console.log('doctor ids match');
												var charges = (bills[i].servicesInfo[j].fee*bills[i].servicesInfo[j].quantity) - bills[i].servicesInfo[j].discount - (bills[i].servicesInfo[j].refund?bills[i].servicesInfo[j].refund:0);
												//subtract expenses if any before calculating share
												if(charges > 0) {
													//console.log('charges > 0 in first');
													//console.log(charges);
													//console.log(bills[i].servicesInfo[j].description);
													for(var l=0;l<details[ind].expenses.length;l++) {
														if(details[ind].expenses[l].cost)
															charges= charges-(details[ind].expenses[l].cost*bills[i].servicesInfo[j].quantity);
													}//console.log(charges);

													/*for(var c=0;c<bills[i].invoice.length;c++) {
														if(bills[i].invoice[c].recieved < 0) {
															charges= charges+bills[i].invoice[c].recieved;
															refund+=(-1*(bills[i].invoice[c].recieved));
														}
													}*/
													if(bills[i].servicesInfo[j].refund) {
														//charges= charges-bills[i].servicesInfo[j].refund;
														refund+= bills[i].servicesInfo[j].refund;
													}

													if(charges > 0) { //now calculate shares
														var docshare = Math.round(charges * (details[ind].shares[k].commissionPerc/100));
														//console.log('charges > 0 in second');
														datatosend.push({'billNumber': bills[i].billNumber,
															'patientInfo': bills[i].patientInfo,
															'handlerName': bills[i].invoice[bills[i].invoice.length-1].handlerName,
															'created': bills[i].created,
															'description': bills[i].servicesInfo[j].description,
															'fee': bills[i].servicesInfo[j].fee*bills[i].servicesInfo[j].quantity,
															'refund': refund,
															'discount': bills[i].servicesInfo[j].discount,
															'total': charges,
															'docshare': docshare
														});
													}
												}
											}
										}
									}
								}
							}

						}
						res.json(datatosend);

					});

				}
			});
		};


exports.departmentReport = function(req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var department = req.params.department;
	var query;
	if (fromDate === 'undefined' && toDate === 'undefined') {
		query = Billing.find({'status': 'billed'});
	} else if (toDate === 'undefined') {
		query = Billing.find({'created': {'$gte' : new Date(fromDate)}, 'status': 'billed'});
	} else {

		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);

		query = Billing.find({'created': {'$gte' : new Date(fromDate), '$lte': new Date(toDate)}, 'status': 'billed'});
	}
	query.exec(function(err, bills) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var filteredBills = [];
			for (var i = 0; i < bills.length; i++) {
				for (var j = 0; j < bills[i].servicesInfo.length; j++) {
					if (department === 'All') {
						filteredBills.push(bills[i].servicesInfo[j]);
					} /*else if (department.toLowerCase() === 'Consultation'.toLowerCase()) {
						if (bills[i].servicesInfo[j].description.toLowerCase() === 'Consultation'.toLowerCase()) {
							filteredBills.push(bills[i].servicesInfo[j]);
						}
					}*/ else if (department === 'Registration') {
						if (bills[i].servicesInfo[j].description.toLowerCase() === 'Registration'.toLowerCase()) {
							filteredBills.push(bills[i].servicesInfo[j]);
						}
					}else if (bills[i].servicesInfo[j].category === undefined && bills[i].servicesInfo[j].description === 'Consultation') {
							if (department.toLowerCase() === 'consultation') {
								filteredBills.push(bills[i].servicesInfo[j]);
							}
					} else if (bills[i].servicesInfo[j].category !== undefined) {
							if (bills[i].servicesInfo[j].category.toLowerCase().indexOf(department.toLowerCase()) !== -1) {
								filteredBills.push(bills[i].servicesInfo[j]);
							}
					}
				}
			}
			res.json(filteredBills);
		}
	});
};

/**
 * getRevenueForChart
 */

exports.getRevenueForChart = function (req, res) {
	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;
	var query;
	if (fromDate === 'undefined' && toDate === 'undefined') {
		query = Billing.find({'status': 'billed'});
	} else if (toDate === 'undefined') {
		query = Billing.find({'created': {'$gte' : new Date(fromDate)}, 'status': 'billed'});
	} else {
		toDate = new Date(toDate);
		toDate.setDate(toDate.getDate() + 1);
		toDate = dateConverter(toDate);
		query = Billing.find({'created': {'$gte' : new Date(fromDate), '$lte': new Date(toDate)}, 'status': 'billed'});
	}
	query.exec(function(err, bills) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(bills);
		}
	});
};

/**
 * List of Filtered Invoices
 */
exports.listFilteredBills = function(req, res) {

	var fromDate = req.params.fromDate;
	var toDate = req.params.toDate;

	var receptionistName = req.params.receptionistName;
	var query;
	if(req.params.pageNumber){

		var perPage = 10;
		var pageNumber = Number(req.params.pageNumber);
		if (receptionistName === 'All') {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Billing.find({'status': 'billed'}).skip(pageNumber * perPage).limit(perPage);
			} else if (toDate === 'undefined') {
				query = Billing.find({'status': 'billed', 'created': {'$gte' : new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else if (fromDate === 'undefined') {

					toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'created': {'$lte' : new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else {

					toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'created': {'$gte' : new Date(fromDate), '$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		} else {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}}).skip(pageNumber * perPage).limit(perPage);
			} else if (toDate === 'undefined') {
				console.log('this is the query');
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$gte': new Date(fromDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else if (fromDate === 'undefined') {
				toDate = new Date(toDate);
				toDate.setDate(toDate.getDate() + 1);
				toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			} else{

				toDate = new Date(toDate);
				toDate.setDate(toDate.getDate() + 1);
				toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$gte': new Date(fromDate), '$lte': new Date(toDate)}}).skip(pageNumber * perPage).limit(perPage);
			}
		}
	} else {
		if (receptionistName === 'All') {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Billing.find({'status': 'billed'});
			} else if (toDate === 'undefined') {
				query = Billing.find({'status': 'billed', 'created': {'$gte' : new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
					toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'created': {'$lte' : new Date(toDate)}});
			} else {
					toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);

				query = Billing.find({'status': 'billed', 'created': {'$gte' : new Date(fromDate), '$lte': new Date(toDate)}});
			}
		} else {
			if (fromDate === 'undefined' && toDate === 'undefined') {
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}});
			} else if (toDate === 'undefined') {
				console.log('this is the query');
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$gte': new Date(fromDate)}});
			} else if (fromDate === 'undefined') {
				toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$lte': new Date(toDate)}});
			} else{
				toDate = new Date(toDate);
					toDate.setDate(toDate.getDate() + 1);
					toDate = dateConverter(toDate);
				query = Billing.find({'status': 'billed', 'invoice': {$elemMatch: {handlerName: receptionistName}}, 'created': {'$gte': new Date(fromDate), '$lte': new Date(toDate)}});
			}
		}
	}
	query.exec(function (err, bills) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(bills);
		}
	});
};
