'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	FacilityDetails = mongoose.model('FacilityDetails'),
	_ = require('lodash'),
	moment = require('moment');


/**
 * Create a new node
 */
 exports.create = function(req, res) {
	var errors = req.validationErrors();
	
	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {
		FacilityDetails.findOneAndUpdate( {'description':req.body.description,'category':req.body.category}, req.body, {upsert: true}).exec(function(err, facility_details) {
			if (err) {
				console.log(err);
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(facility_details);
			}
		});
	}
};


/**
 * List of Patients
 */
exports.list = function(req, res) {
	FacilityDetails.find({}).exec(function(err, facility_details) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(facility_details);
		}
	});
};

exports.read = function(req, res) {
	console.log(req.params);
	FacilityDetails.findOne({'description':req.params.description,'category':req.params.category}).exec(function(err, facility_details) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log(facility_details);
			res.status(200).send(facility_details);
		}
	});
};



