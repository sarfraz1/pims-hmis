'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Inventory = mongoose.model('Inventory'),
	InventoryPricing = mongoose.model('InventoryPricing'),
	InventoryStock = mongoose.model('InventoryStock'),
	PurchaseRequisition = mongoose.model('purchaseRequisition'),
	PurchaseOrder = mongoose.model('purchaseOrder'),
	PurchaseReturn = mongoose.model('purchaseReturn'),
	StockTransfer = mongoose.model('StockTransfer'),
	GRN = mongoose.model('GRN'),
	InventoryStock = mongoose.model('InventoryStock'),
	Invoice = mongoose.model('Invoice'),
	Medicine = mongoose.model('Medicine'),
	multiparty = require('multiparty'),
	async = require('async'),
	fs = require('fs'),
	moment = require('moment'),
	_ = require('lodash'),
	isPharmacy = false;

/**
 * Create a Inventory
 */


exports.create = function(req, res) {
	
    var inventory = new Inventory(req.body); 

	Inventory.findOne().sort({_id:-1}).exec(function(err, grnObj) {
		if(grnObj){
			var code = Number(grnObj.code)+1;
			code = String(code);
			
			inventory.code = code;

		} else if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} 
		else {
			inventory.code = '10000001';
		}
		inventory.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {

				Medicine.findOne({'formula':req.body.formula,'tradeName.description':req.body.description}).exec(function(err, Medicines) {
					if(Medicines!==null){
						for(var i = 0;i<Medicines.tradeName.length;i++){
							if(Medicines.tradeName[i].description===req.body.description){
								Medicines.tradeName[i].inStock = true;
							}
						}
						Medicines.save(function(err,data) {
							if (err) {
								console.log(err);
							} 
						});
					}

				});

				res.json(inventory);
			}
		});

	});
};

exports.createImage = function(req, res) {
	var form = new multiparty.Form();
    form.parse(req, function(err, body , files) {

        req.body = body;
        
        if(body.file){

            var picPath = body.imageLoc[0];
            
            var temp = body.file[0];
            temp = temp.split(',');
            
            var b64string = temp[1];
			var buf = new Buffer(b64string, 'base64');
            fs.writeFile(picPath , buf , function (err , success) {
                if(err){
                    return res.status(400).send({
                        'error': err
                    });
                }
                else {
					res.json('saved');
				}
            });
        }
        
	});
};

exports.getItemByDescription = function(req, res) {

	Inventory.findOne({'description': req.params.description}).exec(function(err, inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventory);
		}
	});
};

/**
 * Show the current Inventory
 */

exports.getItemCode = function(req, res) {
	
	Inventory.findOne().sort({_id:-1}).limit(1).exec(function(err, inventory) {
		if(inventory){
			var code = Number(inventory.code)+1;
			code = String(code);
			
			// var pad =function (n) {
			// 	var zeros = '0';
			// 	while(0<n-1){ n--; zeros = zeros+'0'; }
			// 	return zeros;
			// };
			//code = pad(7-code.length) + code;
			
			res.json(code);
		
		} else if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} 
		else {
			res.json('10000001');
		}
		
	});
    
};


exports.read = function(req, res) {
	Inventory.findOne({'code': req.params.inventoryId}).exec(function(err, inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventory);
		}
		
	});
};

/**
 * Update a Inventory
 */
exports.update = function(req, res) {
	delete req.body.__v;
	var item = req.body;
	Inventory.findOneAndUpdate({'code': req.params.inventoryId}, item, {upsert: true}, function(err, inventory) {
        if (err) {
            return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
        }
        else {
        	res.json(inventory);
        }
    });
};

/**
 * Delete an Inventory
 */
exports.delete = function(req, res) {
	Inventory.remove({'code':req.params.inventoryId} , function(err,inventory) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			InventoryPricing.remove({'inventoryID': req.params.inventoryId}, function(err, pricing) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(inventory);
				}
			});
		}
	});
};

/**
 * List of Inventories
 */
exports.list = function(req, res) { 
	Inventory.find().exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};


var keywordSearch = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');
	
	var query = '';
	if(isPharmacy)
		query = {'description':{$regex : regex},'grn_created':true};
	else 
		query = {'description':{$regex : regex}};
	Inventory.find(query,'-__v').limit(20).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare= function(a,b) {
			  if (a.description.indexOf(keyword) < b.description.indexOf(keyword))
			    return -1;
			  if (a.description.indexOf(keyword) > b.description.indexOf(keyword))
			    return 1;
			  return 0;
			};
			
			inventories.sort(compare);

			res.json(inventories);
		}
	});
};

var getByBarcode = function(req, res){
	Inventory.findOne({'barcode':req.params.keyword}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};

var getByCode = function(req, res){
	Inventory.findOne({'code':req.params.keyword}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories);
		}
	});
};

exports.getItem = function(req, res) { 
	if(req.params.searchType==='code'){	
		getByCode(req, res);
	}
	else if(req.params.searchType==='barcode'){
		getByBarcode(req, res);
	}
	else if(req.params.searchType==='description'){
		keywordSearch(req, res);
	}
};

exports.getItemExpiryStatus = function(req, res) { 
	Inventory.findOne({'code':req.params.code}).exec(function(err, inventories) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(inventories.expiry_date_enabled);
		}
	});
	
};

exports.getItemByDescription = function(req, res) { 
	Inventory.findOne({'description':req.params.description}).exec(function(err, item) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(item);
		}
	});
	
};



/**
 * Inventory authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.inventory.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

/**
 * Get all details related to inventory item
 */
exports.getDetails = function(req, res) {
	var itemDetails = {};
	var itemCode = req.params.code;
	itemDetails.documents = [];
	Inventory.findOne({'code': req.params.code}).exec(function(err, item) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			itemDetails.code = item.code;
			itemDetails.description = item.description;
			
			PurchaseRequisition.find({}).exec(function(err, purchaseRequisitions) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					for (var i = 0; i < purchaseRequisitions.length; i++) {
						for (var j = 0; j < purchaseRequisitions[i].itemsList.length; j++) {
							if (purchaseRequisitions[i].itemsList[j].code === itemCode) {
								if (purchaseRequisitions[i].status === 'Accepted') {
									itemDetails.documents.push({
										quantity: purchaseRequisitions[i].itemsList[j].quantity,
										rate: purchaseRequisitions[i].itemsList[j].estRate,
										created: purchaseRequisitions[i].created,
										netValue: purchaseRequisitions[i].itemsList[j].quantity * purchaseRequisitions[i].itemsList[j].estRate,
										code: purchaseRequisitions[i].code,
										documentType: 'Purchase Requisition'
									});
								}
							}
						}
					}
					PurchaseOrder.find({}).exec(function(err, purchaseOrders) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							for (var i = 0; i < purchaseOrders.length; i++) {
								for (var j = 0; j < purchaseOrders[i].itemsList.length; j++) {
									if (purchaseOrders[i].itemsList[j].code === itemCode) {
										itemDetails.documents.push({
											netValue: purchaseOrders[i].itemsList[j].netValue,
											rate: purchaseOrders[i].itemsList[j].rate,
											quantity: purchaseOrders[i].itemsList[j].quantity,
											created: purchaseOrders[i].created,
											code: purchaseOrders[i].purchaseOrderNumber,
											documentType: 'Purchase Order'
										});
									}
								}
							}
							GRN.find({}).exec(function(err, grns) {
								if (err) {
									return res.status(400).send({
										message: errorHandler.getErrorMessage(err)
									});
								} else {
									for (var i = 0; i < grns.length; i++) {
										for (var j = 0; j < grns[i].itemsList.length; j++) {
											if (grns[i].itemsList[j].code === itemCode) {
												itemDetails.documents.push({
													netValue: grns[i].itemsList[j].netValue,
													quantity: grns[i].itemsList[j].quantity,
													rate: grns[i].itemsList[j].rate,
													created: grns[i].date,
													code: grns[i].GRNNumber,
													documentType: 'GRN'
												});
											}
										}
									}
									PurchaseReturn.find({}).exec(function (err, purchaseReturns) {
										if (err) {
											return res.status(400).send({
												message: errorHandler.getErrorMessage(err)
											});
										} else {
											for (var i = 0; i < purchaseReturns.length; i++) {
												for (var j = 0; j < purchaseReturns[i].itemsList.length; j++) {
													if (purchaseReturns[i].itemsList[j].code === itemCode) {
														itemDetails.documents.push({
															rate: purchaseReturns[i].itemsList[j].rate,
															quantity: purchaseReturns[i].itemsList[j].quantity,
															netValue: purchaseReturns[i].itemsList[j].netValue,
															created: purchaseReturns[i].date,
															code: purchaseReturns[i].purchaseReturnNumber,
															documentType: 'Purchase Return'
														})
													}
												}
											}
											InventoryStock.findOne({'id': itemCode}).exec(function (err, stock) {
												if (err) {
													return res.status(400).send({
														message: errorHandler.getErrorMessage(err)
													});
												} else {
													var inStock = 0;
													if (stock) {
														for (var i = 0; i < stock.batch.length; i++) {
															inStock = inStock + stock.batch[i].quantity;
														}
													}
													itemDetails.inStock = inStock;
													res.json(itemDetails);
												}
											});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
};


/**
 * Get all details related to inventory item
 */
exports.getItemLedger = function(req, res) {
	var itemDetails = {};
	var itemCode = req.params.code;
	var stockentered = 0;
	var stockreturned = 0;
	var stocktransferedin = 0;
	var stocktransferedout = 0;
	var stocksold = 0;
	var stocksoldreturned = 0;
	
	itemDetails.documents = [];
	Inventory.findOne({'code': req.params.code}).exec(function(err, item) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			itemDetails.code = item.code;
			itemDetails.description = item.description;
			//Gather all documents
			async.parallel([
			
			//Gather GRNs 
			function(callback) {
				GRN.find({"storeId":req.params.store,"itemsList.code": itemCode}).exec(function(err, grns) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
						callback();
					} else {
						//get conversion units 
						
						Inventory.findOne({'code':itemCode}).exec(function(err, inventory) {
							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
								callback();
							} else {
										
								var fromdate = moment(req.params.fromdate);
								var todate = moment(req.param.todate);
								
								for (var i = 0; i < grns.length; i++) {
									var grndate = moment(grns[i].date);
									
									if(grndate.isBefore(fromdate)) { //calculate entered stock for starting balance if before from date
										for (var j = 0; j < grns[i].itemsList.length; j++) {
											if(grns[i].itemsList[j].code == itemCode) {
												if(grns[i].itemsList[j].loosepacking) {
													stockentered = stockentered+grns[i].itemsList[j].quantity+(grns[i].itemsList[j].bonus?grns[i].itemsList[j].bonus:0);
												} else {
													stockentered = stockentered+grns[i].itemsList[j].quantity*inventory.storage_to_selling+(grns[i].itemsList[j].bonus?grns[i].itemsList[j].bonus:0)*inventory.storage_to_selling;
												}
												break;
											}
										}
									} else if(grndate.isBefore(todate)) { // if grn lies in date range
										for (var j = 0; j < grns[i].itemsList.length; j++) {
											if (grns[i].itemsList[j].code === itemCode) {
												if(grns[i].itemsList[j].loosepacking) {
													itemDetails.documents.push({
														netValue: grns[i].itemsList[j].netValue,
														quantity: grns[i].itemsList[j].quantity+(grns[i].itemsList[j].bonus?grns[i].itemsList[j].bonus:0),
														rate: grns[i].itemsList[j].rate,
														created: grns[i].date,
														code: grns[i].GRNNumber,
														documentType: 'GRN',
														transactionType: 'IN'
													});
												} else {
													itemDetails.documents.push({
														netValue: grns[i].itemsList[j].netValue,
														quantity: grns[i].itemsList[j].quantity*inventory.storage_to_selling+(grns[i].itemsList[j].bonus?grns[i].itemsList[j].bonus:0)*inventory.storage_to_selling,
														rate: grns[i].itemsList[j].rate,
														created: grns[i].date,
														code: grns[i].GRNNumber,
														documentType: 'GRN',
														transactionType: 'IN'
													});
												}
												break;
											}
										}
									}
								}
								callback();								
								
							}
						});
						
					} //end else
				});
			}, 
			
			//Gather purchase returns
			function(callback) {
				PurchaseReturn.find({"storeId":req.params.store, "itemsList.code": itemCode}).exec(function (err, purchaseReturns) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
						callback();
					} else {
						var fromdate = moment(req.params.fromdate);
						var todate = moment(req.param.todate);
						
						for (var i = 0; i < purchaseReturns.length; i++) {
							var docdate = moment(purchaseReturns[i].date);
							
							if(docdate.isBefore(fromdate)) { //calculate returned stock for starting balance if before from date
								for (var j = 0; j < purchaseReturns[i].itemsList.length; j++) {
									if(purchaseReturns[i].itemsList[j].code == itemCode) {
										stockreturned = stockreturned + purchaseReturns[i].itemsList[j].quantity;
										break;
									}
								}
							} else if(docdate.isBefore(todate)) { // if return lies in date range
								for (var j = 0; j < purchaseReturns[i].itemsList.length; j++) {
									if (purchaseReturns[i].itemsList[j].code === itemCode) {
										itemDetails.documents.push({
											rate: purchaseReturns[i].itemsList[j].rate,
											quantity: purchaseReturns[i].itemsList[j].quantity,
											netValue: purchaseReturns[i].itemsList[j].netValue,
											created: purchaseReturns[i].date,
											code: purchaseReturns[i].purchaseReturnNumber,
											documentType: 'Purchase Return',
											transactionType: 'OUT'
										});
										break;
									}
								}
							}							
							
						}
						
						callback();
					}
					
				});
			},
			
			//gather Sales 
			function(callback) {
				var tempDesc = req.params.storedesc;
				var storeDesc = tempDesc.replace(/%20/g, " ");
				Invoice.find({"storeDesc":storeDesc, "orderList.id": itemCode}).exec(function (err, invoices) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
						callback();
					} else {
						//console.log(invoices);
						//console.log(req.params.store);
						var fromdate = moment(req.params.fromdate);
						var todate = moment(req.param.todate);
						
						for (var i = 0; i < invoices.length; i++) {
							var docdate = moment(invoices[i].invoiceDate);
							
							if(docdate.isBefore(fromdate)) { //calculate sold stock for starting balance if before from date
								for (var j = 0; j < invoices[i].orderList.length; j++) {
									if(invoices[i].orderList[j].id == itemCode) {
										if(invoices[i].isReturn == false) {
											stocksold = stocksold + invoices[i].orderList[j].quantity;
										} else {
											stocksoldreturned = stocksoldreturned +  invoices[i].orderList[j].quantity;
										}
										break;
									}
								}
							} else if(docdate.isBefore(todate)) { // if invoice lies in date range
								for (var j = 0; j < invoices[i].orderList.length; j++) {
									if (invoices[i].orderList[j].id === itemCode) {
										if(invoices[i].isReturn == false) {
											itemDetails.documents.push({
												rate: invoices[i].orderList[j].price,
												quantity: invoices[i].orderList[j].quantity,
												netValue: invoices[i].orderList[j].quantity*invoices[i].orderList[j].price,
												created: invoices[i].invoiceDate,
												code: invoices[i].invoiceNumber,
												documentType: 'Sales Invoice',
												transactionType: 'OUT'
											});
										} else {
											itemDetails.documents.push({
												rate: invoices[i].orderList[j].price,
												quantity: invoices[i].orderList[j].quantity,
												netValue: invoices[i].orderList[j].quantity*invoices[i].orderList[j].price,
												created: invoices[i].invoiceDate,
												code: invoices[i].invoiceNumber,
												documentType: 'Sales Invoice (Return)',
												transactionType: 'IN'
											});
										}
										break;
									}
								}
							}							
							
						}
						
						callback();
					}
					
				});
				
			},
			
			//Gather Stock Transfers 
			function(callback) {
				StockTransfer.find({"itemsList.code": itemCode, $or: [{"fromStoreId": req.params.store}, {"toStoreId": req.params.store}]}).exec(function (err, transfers) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
						callback();
					} else {
						var fromdate = moment(req.params.fromdate);
						var todate = moment(req.param.todate);
						
						for (var i = 0; i < transfers.length; i++) {
							var docdate = moment(transfers[i].date);
							
							if(docdate.isBefore(fromdate)) { //calculate returned stock for starting balance if before from date
								for (var j = 0; j < transfers[i].itemsList.length; j++) {
									if(transfers[i].itemsList[j].code == itemCode) {
										if(transfers[i].fromStoreId == req.params.store) {
											stocktransferedout = stocktransferedout + transfers[i].itemsList[j].quantity;
										} else if(transfers[i].toStoreId == req.params.store) {
											stocktransferedin = stocktransferedin + transfers[i].itemsList[j].quantity;
										}
										break;
									}
								}
							} else if(docdate.isBefore(todate)) { // if return lies in date range
								for (var j = 0; j < transfers[i].itemsList.length; j++) {
									if (transfers[i].itemsList[j].code === itemCode) {
										itemDetails.documents.push({
											rate: 'N/A',
											quantity: transfers[i].itemsList[j].quantity,
											netValue: 'N/A',
											created: transfers[i].date,
											code: transfers[i].transferNumber,
											documentType: 'Stock Transfer',
											transactionType: transfers[i].fromStoreId==req.params.store?'OUT':'IN'
										});
										break;
									}
								}
							}							
							
						}
						
						callback();
					}
					
				});
			}
			], function(err) {
				if(err) {
					return next(err);
				}
				else {
					var datatosend = {};

					var startingbalance = stockentered - stockreturned + stocktransferedin - stocktransferedout - stocksold ;
					datatosend.itemDetails = itemDetails;
					datatosend.startingbalance = startingbalance;
					
					res.json(datatosend);
				}
				
			});
		}
	});
};