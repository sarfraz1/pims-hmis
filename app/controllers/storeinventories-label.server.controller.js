'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	InventoryLabel = mongoose.model('StoreInventoryLabel'),
	multiparty = require('multiparty'),
	fs = require('fs'),
    _ = require('lodash');

/**
 * Create a Inventories label
 */
exports.create = function(req, res) {
	var inventoryLabel = new InventoryLabel(req.body);
	if (inventoryLabel.defaultOption === false) {
		inventoryLabel.save(function (err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(inventoryLabel);
			}
		});
	} else {
		InventoryLabel.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, label) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				inventoryLabel.save(function (err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(inventoryLabel);
					}
				});			
			}
		});
	}
};
/**
 * Show the current Inventories label
 */
exports.read = function(req, res) {
	InventoryLabel.findOne({'_id': req.params.labelId}).exec(function(err, label) {
		if (label) {
			res.json(label);
		} else {
			err = 'Inventory label not found';
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}
	});
};

/**
 * Update a Inventories label
 */
exports.update = function(req, res) {
	if (req.params.defaultOption === false) {
		InventoryLabel.findOneAndUpdate({'_id': req.params.labelId}, req.body).exec(function(err, label){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(label);
			}
		});
	} else {
		InventoryLabel.findOneAndUpdate({'defaultOption': true}, {$set: {'defaultOption': false}}).exec(function(err, label) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				InventoryLabel.findOneAndUpdate({'_id': req.params.labelId}, req.body).exec(function(err, label){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(label);
					}
				});		
			}
		});
	}
};

/**
 * Delete an Inventories label
 */
exports.delete = function(req, res) {
	InventoryLabel.remove({'_id': req.params.labelId}, function(err, label) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(label);
		}
	});
};

/**
 * List of Inventories labels
 */
exports.list = function(req, res) {
	InventoryLabel.find().exec(function(err, labels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(labels);
		}
	});
};

/**
 * Retrieve array of Inventories Labels
 */
var searchByName = function(req, res) { 
	var keyword = req.params.keyword;
	var regex = new RegExp(keyword, 'i');

	InventoryLabel.find({'name':{$regex : regex}},'-__v').exec(function(err, labels) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var compare = function(a, b) {
				if (a.name.indexOf(keyword) < b.name.indexOf(keyword))
					return -1;
				if (a.name.indexOf(keyword) > b.name.indexOf(keyword))
					return 1;
				return 0;
			};
			labels.sort(compare);
			res.json(labels);
		}
	});
};

exports.search = function(req, res) {
	if (req.params.searchType === 'name') {	
		searchByName(req, res);
	}
};