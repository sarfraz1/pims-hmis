'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Ward = mongoose.model('Ward'),
	_ = require('lodash'),
	moment = require('moment'),
	facilityList,
	count;

/**
 * Create a new node
 */
 exports.create = function(req, res) {
	req.checkBody('name', 'Node is required').notEmpty();

	var errors = req.validationErrors();

	if(errors) {
		console.log(errors);
		return res.status(500).send(errors);
	} else {

		//check if node already exists
		var query;
		query = Ward.findOne({name: req.body.name});
		query.exec(function(err, facility){
		if (err) {
			console.log(err);
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else if(!facility){
			//just create the relationship
			var facility = new Ward();
			facility.name = req.body.name;
			facility.children = [];
			facility.save(function(err, faicility) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					if(req.body.parentNode) {
						query = Ward.findOneAndUpdate({'name': req.body.parentNode},{  $addToSet:{children:req.body.name}});
						query.exec(function(err, facility){
							if (err) {
								return res.status(400).send({
									message: errorHandler.getErrorMessage(err)
								});
							} else {
								res.status(200).send(facility);
							}
						});
					} else {
						res.status(200).send(facility);
					}
				}
			});
		} else {
			if(req.body.parentNode) {
				query = Ward.findOneAndUpdate({'name': req.body.parentNode},{  $addToSet:{childs:req.body.name}});
				query.exec(function(err, facility){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.status(200).send(facility);
					}
				});
			}
		}
	});
	}
};

/**
 * Delete a Node
 */
var removeAllChildren = function(child){
	var query;
	query = Ward.findOne({name: child});
	query.exec(function(err, facility){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			for(var i=0;i<facility.children.length;i++)
				removeAllChildren(facility.children[i]);

			facility.remove(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					//res.json(article);
				}
			});
			//res.status(200).send(facility);
		}
	});
};


exports.delete = function(req, res) {
	var regex = new RegExp(req.body.name, 'i');

	Ward.findOneAndUpdate({'name':req.body.name} ,  {'children': []}, function(err, facility) {

		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var regex = new RegExp(req.body.name, 'i');

			for(var i=0;i<req.body.nodes.length;i++){
				removeAllChildren(req.body.nodes[i].name);
			}
			//res.json(true);
		}
	});

};

exports.removeNode = function(req, res){
	var query;
	query = Ward.remove({name: req.params.node});
	query.exec(function(err, facility){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {

			res.status(200).send(facility);
		}
	});
};

exports.update = function(req, res){
	console.log("Update Called");
	//res.status(200).end();
	var query;
		console.log(req.body.name);
		console.log(req.body.NoOfBeds);
		console.log(req.body.bedPrice);


	//query = Ward.findOneAndUpdate({'name': req.body.parentNode},{  $addToSet:{children:req.body.name}});
	query = Ward.findOneAndUpdate({'name': req.body.name}, { $set: {'NoOfBeds':req.body.NoOfBeds, 'bedPrice': req.body.bedPrice}});

	query.exec(function(err, facility){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log("Update success");
			 res.json(facility);
	}
});
};


/**
 * Retrieve immediate children of a node
 */
exports.read = function(req, res) {
	var query;
	if(req.params.node) {
		var query;
		query = Ward.findOne({name: req.params.node});
		query.exec(function(err, facility){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.status(200).send(facility);
			}
		});
	}else {
		return res.status(500).send({msg: "Incomplete parameters"});
	}

};

/**
 * List of Patients
 */
exports.list = function(req, res) {
	Ward.find({}).populate('nodes.name').exec(function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.status(200).send(facility);
		}
	});
};


/**
 * Last Nodes
 */

var getFacilitiesArray = function(facilityName,callback){
	Ward.findOne({'name' : facilityName}).exec(function(err, facility) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			console.log(count);
			count--;
			if(facility.children.length===0){
				facilityList.push(facilityName);
			} else {

				for(var i=0;i<facility.children.length;i++){
					getFacilitiesArray(facility.children[i],callback);
				}
			}

			if(count===0){
				console.log("callingback");
				callback();
			}
		}
	});
};

exports.lastNodes = function(req, res) {
	Ward.count().exec(function(err, c) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			facilityList = [];
			count = c;
			getFacilitiesArray('Hospital',function(){
				res.status(200).send(facilityList);
			});
		}
	});
};
