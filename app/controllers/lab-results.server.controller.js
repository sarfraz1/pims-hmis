'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	LabResults = mongoose.model('LabResults'),
	LabTest = mongoose.model('LabTest'),
	Patient = mongoose.model('Patient'),
    _ = require('lodash');

/**
 * Create a Panel
 */
var convertAge = (age,ageUnit) => {
	age = parseInt(age);
	if(ageUnit === "Years") {
			return age*365;
	}
	else if(ageUnit === "Months") {
			return age*30;
	}
	else {
			return age;
	}
};

var generateResult = function(labTest,patient,callback){
	var age = calculateAge(patient.dob); // in Days
	var subTests = [];
 	for(var i=0;i<labTest.referenceValue.length;i++){
		let minAge = convertAge(labTest.referenceValue[i].minAge, labTest.referenceValue[i].ageUnits);
		let maxAge = convertAge(labTest.referenceValue[i].maxAge, labTest.referenceValue[i].ageUnits);
		 if(!labTest.referenceValue[i].isSubHeading){
			if((minAge <= age && age <= maxAge) && (labTest.referenceValue[i].gender.toUpperCase()===patient.gender.toUpperCase() || labTest.referenceValue[i].gender.toUpperCase()==='BOTH')){
				//console.log(labTest.referenceValue[i].valueDescription);
				var temp = {
					patientAge: age,
					reference : {
						valueDescription : labTest.referenceValue[i].valueDescription,
						unit : labTest.referenceValue[i].unit,
						isRange : labTest.referenceValue[i].isRange,
						isSubHeading : labTest.referenceValue[i].isSubHeading,
						minValue : labTest.referenceValue[i].minValue,
						maxValue : labTest.referenceValue[i].maxValue,
						valueDetails : labTest.referenceValue[i].valueDetails,
						expectedValue : labTest.referenceValue[i].expectedValue,
						expValue:labTest.referenceValue[i].expValue
					}
				};
				subTests.push(temp);
			}
		} else if(labTest.referenceValue[i].isSubHeading){
			var temp = {
				patientAge: age,
				reference : {
					valueDescription : labTest.referenceValue[i].valueDescription,
					unit : labTest.referenceValue[i].unit,
					isRange : labTest.referenceValue[i].isRange,
					isSubHeading : labTest.referenceValue[i].isSubHeading,
					minValue : labTest.referenceValue[i].minValue,
					maxValue : labTest.referenceValue[i].maxValue,
					valueDetails : labTest.referenceValue[i].valueDetails,
					expectedValue : labTest.referenceValue[i].expectedValue,
					expValue:labTest.referenceValue[i].expValue
				}
			};
			subTests.push(temp);
		}
 	}
 	var resultObj = {
 		patientMRN : patient.mr_number,
		description : labTest.description,
		category : labTest.category,
		tests : [{
			subTests : subTests
		}]
 	};
 	//resultObj.tests.push(subTests);
 	callback(resultObj);
	// LabResults = new LabResults(resultObj);
	// console.log(LabResults);
};

var calculateAge = function(birthday) { // birthday is a date
 	// var ageDifMs = Date.now() - birthday.getTime();
  //   var ageDate = new Date(ageDifMs); // miliseconds from epoch
	//   return Math.abs(ageDate.getUTCFullYear() - 1970);
	var date1 = new Date(birthday);
	var date2 = new Date();
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	return diffDays;
};

exports.create = function(req, res) {
	LabResults.findOne({'description': req.body.testInfo.testDescription,'patientMRN': req.body.orderInfo.patientMRN}).exec(function(err, labResults){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var saveResult = function(resultObj){
				//console.log(resultObj.tests[0]);
				//console.log(labResults.tests[0]);
				resultObj.tests[0].laborderId = req.body.orderInfo._id;
				if(labResults){
					labResults.tests.push(resultObj.tests[0]);
				} else {
					labResults = new LabResults(resultObj);
				}
				labResults.save(function(err, labResultsRes){
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.status(200).send(labResultsRes);
					}
				});
			};

			LabTest.findOne({'description': req.body.testInfo.testDescription}).exec(function (err, labTest) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					Patient.findOne({'mr_number': req.body.orderInfo.patientMRN}).exec(function(err, patient){
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							generateResult(labTest,patient,saveResult);
						}
					});
				}
			});
		}
	});
};

exports.read = function(req, res) {
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabResults.findOne({'_id': req.params.labResultsId}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTestResult = function(req, res) {
	if (!req.params.patientMRN || !req.params.testDescription || !req.params.labOrderId ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		LabResults.findOne({'patientMRN':req.params.patientMRN,'description':req.params.testDescription, 'tests': {'$elemMatch': {'laborderId': req.params.labOrderId}}}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTest = function(req, res) {
	if (!req.params.patientMRN || !req.params.testDescription ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		LabResults.findOne({'patientMRN':req.params.patientMRN,'description':req.params.testDescription}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getLabsbyMRN = function(req, res) {
	if (!req.params.patientMRN) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		LabResults.find({'patientMRN':req.params.patientMRN}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Panel not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.getPatientTests = function(req, res) {
	if (!req.params.patientMRN || !req.params.testDescription ) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {

		LabResults.find({'patientMRN':req.params.patientMRN,'description':req.params.testDescription}).exec(function(err, labResults) {
			if (labResults) {
				res.json(labResults);
			} else {
				err = 'Patient not found';
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			}
		});
	}
};

exports.update = function(req, res) {
	delete req.body._id;
	if(req.body.tests[req.body.tests.length - 1].testStatus === "Retest") {
		delete req.body.tests[req.body.tests.length - 1].testStatus;
		req.body.tests[req.body.tests.length - 1].updated = [];
		for (let i = 0; i < req.body.tests[req.body.tests.length - 1].subTests.length; i++) {
			req.body.tests[req.body.tests.length - 1].subTests[i].resultValue = '';
			req.body.tests[req.body.tests.length - 1].subTests[i].resultStatus = '';
			req.body.tests[req.body.tests.length - 1].subTests[i].resultRemarks = '';
		}
	}
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		//req.checkBody('description', 'Description is required').notEmpty();
		//req.checkBody('name', 'Name is required').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			return res.status(500).send(errors);
		} else {
			LabResults.findOneAndUpdate({'_id': req.params.labResultsId}, req.body).exec(function (err, labResults) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(labResults);
				}
			});
		}
	}
};

exports.delete = function(req, res) {
	if (!req.params.labResultsId) {
		return res.status(500).send({msg: 'Incomplete parameters'});
	} else {
		LabResults.remove({'_id': req.params.labResultsId}).exec(function (err, labResults) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(labResults);
			}
		});
	}
};

exports.list = function(req, res) {
	LabResults.find().exec(function(err, labResults) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(labResults);
		}
	});
};
