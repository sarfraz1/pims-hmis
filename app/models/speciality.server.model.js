'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Speciality Schema
 */
var SpecialitySchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	}
});

mongoose.model('Speciality', SpecialitySchema);