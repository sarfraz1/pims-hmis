'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Panel Schema
 */
var LabTestSchema = new Schema({
	description: {
		type: String,
		required: 'Test description is required',
		unique : true,
		minlength: 2
	},
	category: {
		type: String,
		default: '',
		required: 'Test category is required'
	},
	referenceValue: [{
		valueDescription : {
			type: String,
			required: 'Value description is required'
		},
		expValue:[{
			addexpectedValue:{
				type:String,
			}
		}],
		isSubHeading : {
			type: Boolean,
			default: false
		},
		isRange : {
			type: Boolean,
			default: true
		},
		gender : {
			type: String,
			required: 'Gender is required'
		},
		minAge : {
			type: Number
		},
		maxAge : {
			type: Number
		},
		ageUnits : {
			type: String,
			default: "Years"
		},
		minValue : {
			type: Number
		},
		maxValue : {
			type: Number
		},
		expectedValue: {
			type: String,
			default: ''
		},
		valueDetails: {
			type: String,
			default: ''
		},
		unit : {
			type: String,
			default: ''
		}
	}]
});

mongoose.model('LabTest', LabTestSchema);
