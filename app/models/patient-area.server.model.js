'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Patient Area Schema
 */
var PatientAreaSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	}
});

mongoose.model('PatientArea', PatientAreaSchema);