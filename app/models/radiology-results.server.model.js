'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Radiology Results Schema
 */
var RadiologyResultsSchema = new Schema({
	patientMRN: {
		type: String,
		required: 'Patient MRN is required'
	},
	patientName: {
		type: String,
		required: 'Patient name is required'
	},
	radiologyOrderId: {
		type: String
	},
	description : {
		type: String,
		required: 'Test description is required'
	},
	title : {
		type: String,
		required: 'Test title is required'
	},	
	category : {
		type: String
	},
	testSections: [{
		heading : {
			type: String,
			default: ''
		},
		sectionText: {
			type: String,
			default: ''
		}
	}],
	radiologistUsername : {
		type: String
	},
	radiologistSignature: {
		type: String,
		default: ''
	},
	updated : [{
		by : {
			type: String
		},
		Date : {
			type: Date,
			default: Date.now()
		}
	}],
	Created : {
		type: Date,
		default: Date.now()
	}
	
	
});

mongoose.model('RadiologyResults', RadiologyResultsSchema);