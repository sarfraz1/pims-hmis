'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Inventory Schema
 */
var LabSettingSchema = new Schema({
	hospital_name : {
		type: String,
		default: '',
		unique: true
	},
	header_image_url : {
		type: String,
		default: ''
	},
	footer_image_url : {
		type: String,
		default: ''
	},
	enableHeader : {
		type: Boolean,
		default: false
	},
	enableFooter : {
		type: Boolean,
		default: false
	},
	enableSignatue : {
		type: Boolean,
		default: false
	},
	margin_top : {
		type: Number,
		default: 0
	}
});

mongoose.model('LabSetting', LabSettingSchema);
