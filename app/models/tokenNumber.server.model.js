'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Token Number Schema
 */
var tokenNumberSchema = new Schema({
	service_name: {
		type: String,
	},
	token_number: {
		type: Number,
	},
	token_date: {
		type: String,
	}
});

mongoose.model('tokenNumber', tokenNumberSchema);