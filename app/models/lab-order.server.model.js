'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Panel Schema
 */
var LabOrderSchema = new Schema({
	patientMRN: {
		type: String,
		required: 'Test patient MRN is required',
	},
	patientName : {
		type: String,
		required: 'Test patient Name is required',
	},
	created_date: {
		type: Date,
		default: Date.now()
	},
	time: {
		type: String,
		default: '00:00',
		required: 'Test time is required',
	},
	billNumber: {
		type: String,
		required: 'Bill number is required',
	},
	orderStatus : {
		type: String,
		default: 'Pending'
	},
	labTests: [{
		testDescriptionId : {
			type: String
		},
		testDescription : {
			type: String
		},
		testCategory : {
			type: String
		},
		status: {
			type: String,
			default: 'Pending'
		},
		sampleNo: {
			type: String,
			default: ''
		},
		transferredTo : {
			type: String,
			default: ''
		},
		reportUrl: {
			type: String
		}
	}],
	lastModified: [{
		lastModifiedOn: {
			type: Date,
			default: Date.now()
		},
		lastModifiedBy: {
			type: String,
			default: '',
			required: 'Last Modifier name is required',
		}
	}]
});

mongoose.model('LabOrder', LabOrderSchema);