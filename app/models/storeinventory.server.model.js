'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Store Inventory Schema
 */
var StoreInventorySchema = new Schema({
	code : {
		type: String,
		default: '',
		unique: true
	},
	barcode : {
		type: String,
		default: ''
	},
	consumption : {
		type: String,
		default: ''
	},
	description : {
		type: String,
		default: ''
	},
	manufacturer : {
		type: String
	},
	effective_date : {
		type: Date,
		default: Date.now
	},
	tax_inclusive_in_retail : {
		type: Boolean,
		default: true
	},
	label : {
		type: String,
		default: ''
	},
	price : {
		type: Number,
		default: 0
	},
	price_date : {
		type: Date,
		default: Date.now
	},
	purchase_price : {
		type: Number,
		default: 0
	},
	purchase_to_storage : {
		type: Number,
		default: 0
	},
	purchase_unit : {
		type: String,
		default: ''
	},
	retail_price : {
		type: Number,
		default: 0
	},
	sales_tax_applicale : {
		type: Boolean,
		default: false
	},
	selling_price : {
		type: Number,
		default: 0
	},
	unit : {
		type: String,
		default: ''
	},
	selling_unit : {
		type: String,
		default: ''
	},
	storage_to_selling : {
		type: Number,
		default: 0
	},
	storage_unit : {
		type: String,
		default: ''
	},
	tax_sales : {
		type: Number,
		default: 0
	},
	expiry_date_enabled: {
		type: Boolean,
		default: true
	},
	par_level: {
		type: Number
	},
	reorder_level: {
		type: Number
	},
	type : {
		type: String,
		default: ''
	},
	remarks : {
		type: String,
		default: ''
	},
	image_url : {
		type: String,
		default: ''
	},
	grn_created : {
		type: Boolean,
		default: false
	},
	supplierId: {
		type: String,
		default: ''
	}
});

mongoose.model('StoreInventory', StoreInventorySchema);