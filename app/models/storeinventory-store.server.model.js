'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * InventoryStore Schema
 */
var StoreInventoryStoreSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	},
	defaultOption: {
		type: Boolean,
		default: false
	}
});

mongoose.model('StoreInventoryStore', StoreInventoryStoreSchema);