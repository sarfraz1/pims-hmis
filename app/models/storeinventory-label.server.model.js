'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Store InventoryLabel Schema
 */
var StoreInventoryLabelSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	},
	defaultOption: {
		type: Boolean,
		default: false
	}
});

mongoose.model('StoreInventoryLabel', StoreInventoryLabelSchema);