'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Store purchase requisition Schema
 */
var StorepurchaseRequisitionSchema = new Schema({
	code : {
		type: String,
		unique: true
	},
	transcationCategory : {
		type: String,
		default: ''
	},
	date : {
		type: Date,
		default: Date.now() 
	},
	created : {
		type: Date,
		default: Date.now() 
	},
	modified : {
		type: Date,
		default: Date.now() 
	},
	remarks : {
		type: String,
		default: ''
	},
	status : {
		type: String,
		default: 'Pending'
	},
	rejectRemarks : {
		type: String,
		default: ''
	},
	
	itemsList : [{
		code : {
			type: String,
			default: ''
		},
		estDeliveryDate : {
			type: Date
		},
		estRate: {
			type: Number,
			default: 0
		},
		description: {
			type: String
		},
		quantity: {
			type: Number
		},
		handledQty: {
			type: Number,
			default: 0
		},
		remarks: {
			type: String,
			default: ''
		},
		requestedBy: {
			type: String,
			default: ''
		},
		completed : {
			type: Boolean,
			default: false
		}
	}]
});

StorepurchaseRequisitionSchema.index({code:1}, {unique: true});

mongoose.model('StorepurchaseRequisition', StorepurchaseRequisitionSchema);