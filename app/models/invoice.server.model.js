'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Invoice Schema
 */
var InvoiceSchema = new Schema({
	posID: {
		type: String
	},
	posName: {
		type: String
	},
	invoiceNumber: {
		type: Number,
		required: 'Please enter invoice number'
	},
	invoiceSalesman: {
		type: String,
		default: ''
	},
	isReturn: {
		type: Boolean,
		default: false
	},
	returnAgainst: {
		type: Number
	},
	issalemrno: {
		type: Boolean,
		default: false
	},
	issalecredit: {
		type: Boolean,
		default: false
	},
	creditreceiptnumber : {
		type: Number
	},
	patient_name: {
		type: String
	},
	mr_number: {
		type: String
	},
	mr_prefix: {
		type: String,
		maxlength: 3
	},
	invoiceDate: {
		type: Date,
		default: Date.now()
	},
	invoiceTime: {
		type: String
	},
	orderList: [{
		id: {
			type: String,
		},
		description: {
			type: String,
			default: ''
		},
		quantity: {
			type: Number,
			default: 0
		},
		price: {
			type: Number,
			default: 0
		},
		purchasePrice: {
			type: Number,
			default: 0
		},
		returned: {
			type: Number,
			default: 0
		},
		narcotics: {
			type: Boolean
		}
	}],
	orderDiscount: {
		type: Number,
		default: 0
	},
	orderDiscountType: {
		type: String,
		default: 'percent'
	},
	orderCash: {
		type: Number,
		default: 0
	},
	remarks: {
		type: String
	},
	doctorName: {
		type: String,
		default: ''
	},
	storeDesc: {
		type: String
	}
});

mongoose.model('Invoice', InvoiceSchema);