'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Stock Transfer Schema
 */
var StockTransferSchema = new Schema({

	transferNumber: {
		type: String,
		unique: true
	},
	fromStoreId: {
		type: String,
	},
	fromStoreDescription: {
		type: String,
		default: ''
	},
	toStoreId: {
		type: String,
	},
	toStoreDescription: {
		type: String,
	},
	date: {
		type: Date,
		default: Date.now()
	},
	remarks:  {
			type: String,
	},
	itemsList: [{ 
		code: {
			type: String,
			default: ''
		},
       	description:  {
			type: String,
			default: ''
		},
       	quantity: {
			type: Number
		},
		netValue: {
			type: Number
		}
	}],
	transferedBy : {
		type: String
	}
});

mongoose.model('StockTransfer', StockTransferSchema);