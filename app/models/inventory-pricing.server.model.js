'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * InventoryPricing Schema
 */
var InventoryPricingSchema = new Schema({
	inventoryCode: {
		type: String,
		default: ''
	},
	pricingType: {
		type: String,
		default: ''
	},
	discountType: {
		type: String,
		default: ''
	},
	dateTransaction: {
		type: Date,
		default: Date.now()
	},
	effectiveDate: {
		type: Date,
		default: Date.now()
	},
	discountAmount: {
		type: Number,
		default: 0
	},
	sellingPrice: {
		type: Number,
		default: 0
	},
	purchasingPrice: {
		type: Number,
		default: 0
	},
	retailPrice: {
		type: Number,
		default: 0
	},
	supplierId: {
		type: String,
		default: ''
	}
});

mongoose.model('InventoryPricing', InventoryPricingSchema);