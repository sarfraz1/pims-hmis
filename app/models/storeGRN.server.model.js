'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var StoreGRNSchema = new Schema({

	GRNNumber: {
		type: String,
		default: '',
		unique: true
	},
	type: {
		type: String,
		default: ''
	},
	supplierChalanNumber: {
		type: String,
		default: ''
	},
	supplierChalanDate: {
		type: Date,
		default: Date.now() 
	},
	gatePassNumber: {
		type: String,
		default: ''
	},
	transcationCategory: {
		type: String,
		default: ''
	},
	supplierId: {
		type: String,
		default: ''
	},
	supplierDescription: {
		type: String,
		default: ''
	},
	storeId: {
		type: String,
		default: ''
	},
	storeDescription: {
		type: String,
		default: ''
	},
	date: {
		type: Date,
		default: Date.now()
	},
	purchaseOrderNumber : {
		type: String
	},
	purchaseOrderDate : {
		type: Date
	},	
	itemsList: [{ 
		code: {
			type: String,
			default: ''
		},
       	purchaseOrderNumber: {
			type: String,
			default: ''
		},
       	date: {
			type: Date,
			default: Date.now() 
		},
		discountType : {
			type: String,
			default: ''
		},
		discount : {
			type: Number,
			default: 0
		},
       	description:  {
			type: String,
			default: ''
		},
       	estDeliveryDate: {
			type: Date,
			default: Date.now() 
		},
       	rate: {
			type: Number,
			default: ''
		},
		sellingPrice: {
			type: Number
		},
       	quantity: {
			type: Number,
			default: ''
		},
       	remarks:  {
			type: String,
			default: ''
		},
       	requestedBy:  {
			type: String,
			default: ''
		},
		taxType : {
			type: String,
			default: ''
		},
       	saleTaxType:  {
			type: String,
			default: ''
		},
       	saleTax:  {
			type: Number,
			default: 0
		},
       	supplierQuotationDate: {
			type: Date,
			default: Date.now() 
		},
       	supplierQuotationNo:  {
			type: String,
			default: ''
		},
		expiryDate: {
			type: Date
		},
		expiry_date_enabled : {
			type: Boolean
		},
		netValue : {
			type: Number,
			default: 0	
		},
		batchID: {
			type: String,
			default: '0' 
		}
	}]
});

mongoose.model('StoreGRN', StoreGRNSchema);