'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * BrandName Schema
 */
var BrandNameSchema = new Schema({
	tradeName: 
		{ type: String,
		unique: true
                     },
	formula: {
		type: String,
		
	}
});

mongoose.model('BrandName', BrandNameSchema);