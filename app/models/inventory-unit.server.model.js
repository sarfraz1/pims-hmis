'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * InventoryUnit Schema
 */
var InventoryUnitSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	},
	defaultOption: {
		type: Boolean,
		default: false
	}
});

mongoose.model('InventoryUnit', InventoryUnitSchema);