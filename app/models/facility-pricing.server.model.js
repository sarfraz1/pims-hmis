'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var FacilityPricingSchema = new Schema({
	description : {
		type: String
	},
	price : {
		type: Number
	},
	category : {
		type: String
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('FacilityPricing', FacilityPricingSchema);