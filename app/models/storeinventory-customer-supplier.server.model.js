'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Store InventoryCustomerSupplier Schema
 */
var StoreInventoryCustomerSupplierSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true
	},
	customerSupplierType: {
		type: String,
		default: ''
	},
	postalAddress: [{
		address: {
			type: String,
			default: ''
		}
	}],
	paymentMode: {
		type: String
	},
	contactNumber: [{
		number: {
			type: String,
			default: ''
		}
	}],
	emailAddress: {
		type: String,
		default: ''
	},
	salesTaxRegistrationNumber: {
		type: String,
		default: ''
	},
	NTN: {
		type: String,
		default: ''
	},
	CNIC: {
		type: String,
		default: ''
	}
});

mongoose.model('StoreInventoryCustomerSupplier', StoreInventoryCustomerSupplierSchema);