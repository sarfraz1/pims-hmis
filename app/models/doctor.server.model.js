'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var DoctorSchema = new Schema({
	name: {
		type: String,
		unique: true,
		trim: true,
		minlength: 3,
		required: 'Doctor name is required'
	},
	pmdc_number: {
		type: String,
		trim: true,
		minlength: 3
	},
	username: {
		type: String,
		trim: true,
		minlength: 3
	},
	schedule: [{
		day: {
			type: String,
			trim: true,
			required: 'Schedule day is required'
		},
		timings: [{
			starthrs: {
				type:String
			},
			startmins: { 
				type: String
			},
			start_meridian: {
				type: String
			},
			endhrs: {
				type:String
			},
			endmins: { 
				type: String
			},
			end_meridian: {
				type: String
			}
		}]
	}],
	speciality: {
		type: String,
		trim: true,
	},
	qualification: {
		type: String,
		trim: true,
	},
	consultation_fee: {
		type: Number
	},
	percentage_fee : {
		type: Number
	},
	slot_duration: {
		type: Number
	},
	phone: {
		type: String,
		trim: true,
		minlength: 11,
		required: 'Patient phone number is required'
	},
	email: {
		type: String,
		minlength: 8
	},
	cnic: {
		type: String,
		trim: true,
		minlength: 15
	},
	pmdc_expiry: {
		type: Date,
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Doctor', DoctorSchema);