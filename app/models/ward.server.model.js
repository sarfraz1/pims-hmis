'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Ward hierarchy Schema
 */
var WardSchema = new Schema({
	children: [{
		type: String
	}],
	name: {
		type: String,
		unique: true
	},
		bedPrice:{
			type: Number,
		},
		NoOfBeds:{
			type: Number,
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Ward', WardSchema);
