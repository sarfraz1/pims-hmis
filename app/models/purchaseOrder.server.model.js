'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Inventory Schema
 */
var purchaseOrderSchema = new Schema({
	purchaseOrderNumber : {
		type: String,
		default: '',
		unique: true
	},
	date : {
		type: Date,
		default: Date.now() 
	},
	created : {
		type: Date,
		default: Date.now() 
	},
	modified : {
		type: Date,
		default: Date.now() 
	},
	supplierQuotationNo : {
		type: String,
		default: ''
	},
	supplierQuotationDate : {
		type: Date,
		default: Date.now() 
	},
	supplierId: {
		type: String,
		default: ''
	},
	supplierDescription: {
		type: String,
		default: ''
	},
	paymentMode: {
		type: String
	},
	remarks : {
		type: String,
		default: ''
	},
	transcationCategory : {
		type: String,
		default: ''
	},
	itemsList : [{
		code : {
			type: String,
			default: ''
		},
		date : {
			type: Date,
			default: Date.now()
		},
		description : {
			type: String,
			default: ''
		},
		discountType : {
			type: String,
			default: ''
		},
		discount : {
			type: Number,
			default: 0
		},
		estDeliveryDate : {
			type: Date,
			default: Date.now()
		},
		purchaseRequisitionCode : {
			type: String,
			default: ''
		},
		purchaseRequisitionRemarks : {
			type: String,
			default: ''
		},
		quantity : {
			type: Number,
			default: 0
		},
		handledQty: {
			type: Number,
			default: 0
		},
		rate : {
			type: Number,
			default: 0
		},
		remarks : {
			type: String,
			default: ''
		},
		requestedBy : {
			type: String,
			default: ''
		},
		taxType : {
			type: String,
			default: ''
		},
		saleTax : {
			type: Number,
			default: ''
		},
		saleTaxType : {
			type: String,
			default: ''
		},
		netValue : {
			type: Number,
			default: 0	
		},
		completed : {
			type: Boolean,
			default: false
		}
	}]
});

purchaseOrderSchema.index({purchaseOrderNumber:1}, {unique: true});


mongoose.model('purchaseOrder', purchaseOrderSchema);