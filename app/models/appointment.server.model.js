'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Appointment Schema
 */
var AppointmentSchema = new Schema({
	doctor_id: {
		type: String,
		trim: true,
		required: 'Doctor id is required'
	},
	doctor_name: {
		type: String,
		trim: true,
		minlength: 3
	},
	mr_number: {
		type: String
	},
	mr_prefix: {
		type: String,
		maxlength: 3,
		default: 'PRT'
	},
	patient_name: {
		type: String,
		trim: true,
		minlength: 3,
		required: 'Patient name is required'
	},
	phone: {
		type: String,
		trim: true,
		minlength: 11,
		required: 'Patient phone number is required'
	},
	appointment_date: {
		type: String,
		required: 'Appointment date is required'
	},
	time: {
		type: String
	},
	number_in_queue: {
		type: Number
	},
	appointment_status: {
		type: String,
		default: 'Scheduled'
	},
	token_number : {
		type: Number
	},
	vitals_taken : {
		type: Boolean,
		default: false
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Appointment', AppointmentSchema);