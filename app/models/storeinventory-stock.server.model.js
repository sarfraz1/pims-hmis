'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * InventoryStock Schema
 */
var StoreInventoryStockSchema = new Schema({
	id: {
		type: String,
		default: '',
		required: true
	},
	description: {
		type: String,
		default: ''
	},
	batch: [{
		batchID: {
			type: String,
			default: '',
		},
		quantity: {
			type: Number,
			default: 0,
			required: true
		},
		enteredDate: {
			type: Date,
			default: Date.now()
		},
		grnNumber: {
			type: String,
			default: ''
		},
		purchaseRate : {
			type: Number
		},
		expiryDate: {
			type: Date
		},
		storeId: {
			type: String,
			default: ''
		},
		storeDescription: {
			type: String,
			default: ''
		}
	}]
});

mongoose.model('StoreInventoryStock', StoreInventoryStockSchema);