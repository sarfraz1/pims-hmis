'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * InventoryTransactionCategory Schema
 */
var StoreInventoryTransactionCategorySchema = new Schema({
	purchaseType: {
		type: String,
		default: '',
		required: true
	},
	prefix: {
		type: String,
		default: ''
	},
	category: {
		type: String,
		required: true,
		unique: true
	},
	numberingType: {
		type: String,
		default: 'manual'
	},
	automaticType: {
		type: String,
		default: ''
	}
});

mongoose.model('StoreInventoryTransactionCategory', StoreInventoryTransactionCategorySchema);