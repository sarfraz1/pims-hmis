'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Medicine Schema
 */
var NursenotesSchema = new Schema({
	MRNumber : {
		type : String
	},
	appintmentId : {
		type : String
	},
	diastolicBP : {
		type : Number
	},
	height : {
		type: Number
	},
	patientName : {
		type : String
	},
	pulse : {
		type : Number
	},
	pulseOximetry : {
		type : Number
	},
	remarks : {
		type : String
	},
	patientHistory : {
		type : String
	},
	respiratoryRate : {
		type : Number
	},
	systolicBP : {
		type : Number
	},
	bloodSugar : {
		type : Number
	},
	headcircumference : {
		type : Number
	},
	temperature : {
		type : Number
	},
	weight : {
		type : Number
	},
	created : {
		type: Date,
		default: Date.now()
	}
});

mongoose.model('Nursenotes', NursenotesSchema);