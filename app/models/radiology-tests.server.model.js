'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Radiology Test Schema
 */
var RadiologyTestSchema = new Schema({
	description: {
		type: String,
		unique : true
	},
	title: {
		type: String,
		required: 'Test title is required',
		minlength: 2
	},
	testSections: [{
		heading : {
			type: String,
			default: ''
		},
		sectionText: {
			type: String,
			default: ''
		}
	}]
});

mongoose.model('RadiologyTest', RadiologyTestSchema);