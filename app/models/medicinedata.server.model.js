'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * BrandName Schema
 */
var MedicineDataSchema = new Schema({
	brandname: { 
		type: String,
		unique: true
    },
	pharmaceutical: { 
		type: String,
    },
	formulaNames: [{
		type: String,
	}],
	alternatebrands: [{
		type: String,
	}],
	forms: [{
		type: {
			type:String
		},
		packaging: { 
			type: String
        },
        potency: {
        	type: String
        },
		price: {
        	type: Number
        }
	}],
	
});

mongoose.model('MedicineData', MedicineDataSchema);