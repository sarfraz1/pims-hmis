'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Packages Schema
 */
var PackagesSchema = new Schema({
	name: {
		type: String,
		trim: true,
		minlength: 3,
		required: 'Package name is required'
	},
	bed_info: {
		category : {
			type: String,
		},
		days : {
			type: Number
		}
	},
	equipment_limit : {
		type: Number
	},
	medicine_limit : {
		type: Number
	},
	anesthetist_charges : [{
		name : {
			type: String
		},
		_id : {
			type: String
		},
		fee : {
			type: Number
		}
	}],
	total_charges : {
		type: Number
	},
	OTA_charges : [{
		name : {
			type: String
		},
		_id : {
			type: String
		},
		fee : {
			type: Number
		}
	}],
	OT_charges : {
		type: Number
	},
	shares : [{
		doctorId : {
			type: String
		},
		doctorName : {
			type: String
		},
		commissionPerc : {
			type: Number
		}
	}],
	surgeon_charges : [{
		name : {
			type: String
		},
		_id : {
			type: String
	},
		fee : {
			type: Number
		}
	}],
	servicesData : [{
		description : {
			type: String
		},
		fee : {
			type: Number
		},
		quantity : {
			type: Number
		},
		total : {
			type: Number
		}
	}],
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Packages', PackagesSchema);
