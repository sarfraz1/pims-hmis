'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Beds Schema
 */
var BedPricingSchema = new Schema({
	bed_no : {
		type: Number
	},
	description : {
		type: String
	},
	price : {
		type: Number
	},
	category : {
		type: String
	},
	current_status : {
		type: String,
		default: 'Free'
	},
	occupant_details : {
		patient_name: {
			type: String
		},
		mr_number : {
			type: String,
		},
		mr_prefix: {
			type: String,
			maxlength: 3
		},
		occupied_on : {
			type: Date,
			default: Date.now
		}
	},
	bed_type : {
		type: String
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('BedPricing', BedPricingSchema);
