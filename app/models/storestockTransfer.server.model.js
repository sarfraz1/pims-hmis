'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Stock Transfer Schema
 */
var StoreStockTransferSchema = new Schema({

	transferNumber: {
		type: String,
		unique: true
	},
	fromStoreId: {
		type: String,
	},
	fromStoreDescription: {
		type: String,
		default: ''
	},
	toStoreId: {
		type: String,
	},
	toStoreDescription: {
		type: String,
	},
	date: {
		type: Date,
		default: Date.now()
	},
	remarks:  {
			type: String,
	},
	itemsList: [{ 
		code: {
			type: String,
			default: ''
		},
       	description:  {
			type: String,
			default: ''
		},
       	quantity: {
			type: Number,
			default: ''
		}
	}]
});

mongoose.model('StoreStockTransfer', StoreStockTransferSchema);