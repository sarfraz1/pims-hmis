'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Panel Schema
 */
var LabResultsSchema = new Schema({
	patientMRN: {
		type: String,
		required: 'Patient MRN is required'
	},
	description : {
		type: String,
		required: 'Test description is required'
	},
	category : {
		type: String,
		required: 'Test category is required'
	},
	tests : [{
		laborderId : {
			type: String
		},
		subTests : [{
			patientAge : {
				type: Number
			},
			resultValue : {
				type: String
			},
			resultStatus : {
				type: String
			},
			resultRemarks : {
				type: String
			},
			reference: {
				valueDescription : {
					type: String
				},
				unit : {
					type: String
				},
				isRange : {
					type: Boolean,
					default: true
				},
				expValue:[{
					addexpectedValue:{
						type:String,
					}
				}],
				isSubHeading : {
					type: Boolean,
					default: false
				},
				minValue : {
					type: Number
				},
				maxValue : {
					type: Number
				},
				expectedValue: {
					type: String,
					default: ''
				},
				valueDetails: {
					type: String,
					default: ''
				}
			}
		}],
		remarks : {
			type: String
		},
		pathologistUsername : {
			type: String
		},
		testStatus: {
			type: String
		},
		updated : [{
			by : {
				type: String
			},
			Date : {
				type: Date,
				default: Date.now()
			}
		}],
		Created : {
			type: Date,
			default: Date.now()
		}
	}],


});

mongoose.model('LabResults', LabResultsSchema);
