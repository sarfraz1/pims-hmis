'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Appointment Schema
 */
var BillingSchema = new Schema({
	billNumber : {
		type: String,
		unique: true
	},
	discount : {
		type: Number
	},
	grandTotal : {
		type: Number,
		required: 'Grand total is required.'
	},
	recievedCash : {
		type: Number
	},
	doctorCut : {
		type: Number
	},
	remarks : {
		type: String
	},
	patientInfo : {

		mr_number : {
			type: String,
			required: 'Patient MR No is required.'
		},
		mr_prefix: {
			type: String,
			maxlength: 3,
			default: 'PRT'
		},
		name : {
			type: String,
			required: 'Patient Name is required'
		},
		panel_id : {
			type: String
		},
		panelCardNumber: {
			type: String
		},
		phone : {
			type: String,
			required: 'Patient Phone No is required'
		},
		date : {
			type: Date,
			default: Date.now()
		}
	},
	referedDoctor : {
		_id : {
			type: String
		},
		speciality : {
			type: String
		},
		name : {
			type: String
		}
	},
	referingDoctor : {
		_id : {
			type: String
		},
		speciality : {
			type: String
		},
		name : {
			type: String
		}
	},


	servicesInfo : [{
		service_id : {
			type: String
			//required: 'Service is required'
		},
		description : {
			type: String
			//required: 'Service is required'
		},
		discount : {
			type: Number
		},
		fee : {
			type: Number
			//required: 'Service is required'
		},
		quantity : {
			type: Number
			//required: 'Service Qty is required'
		},
		category : {
			type: String
		},
		refund: {
			type: Number
		},
		refund_by: {
			type: String
		},
		token_number : {
			type: Number
		},
		Surgeon:{
			_id : {
				type: String
			},
			name : {
				type: String
			},
			fee : {
				type: Number
			},
			discount : {
				type: Number
			}
		},
		Anesthetist:{
			_id : {
				type: String
			},
			name : {
				type: String
			},
			fee : {
				type: Number
			},
			discount : {
				type: Number
			}
		},
		OTA:{
			_id : {
				type: String
			},
			name : {
				type: String
			},
			fee : {
				type: Number
			},
			discount : {
				type: Number
			}
		},
	}],

	status : {
		type: String,
		default: 'Not billed'
	},
	panelPaymentReceived : {
		type: Boolean
	},
	invoice : [{
		// grandTotal : {
		// 	type: Number
		// },
		cardInfo : {
			number : {
				type: String
			}
		},
		chequeInfo : {
			instrumentNumber : {
				type: String
			},
			date : {
				type: Date
			},
			bankName : {
				type: String
			}
		},
		paymentMethod : {
			type: String
		},
		recieved : {
			type: Number
		},
		refundReason : {
			type: String
		},
		handlerName : {
			type: String
		},
		created: {
			type: Date,
			default: Date.now
		}
	}],
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Billing', BillingSchema);
