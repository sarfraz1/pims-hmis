'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Dental Nurse Notes Schema
 */
var dentalNurseNotesSchema = new Schema({
	MRNumber : {
		type : String
	},
	appintmentId : {
		type : String
	},
	diastolicBP : {
		type : Number
	},
	patientName : {
		type : String
	},
	pulse : {
		type : Number
	},
	pulseOximetry : {
		type : Number
	},
	remarks : {
		type : String
	},
	patientHistory : {
		type : String
	},
	medicalConditions : {
		type : String
	},
	smoking : {
		type : String,
		default: "No"
	},
	reason : {
		type : String
	},
	extraoral : {
		asymmetry : {
			type: Boolean,
			default: false
		},
		swelling : {
			type: Boolean,
			default: false
		},
		erythema : {
			type: Boolean,
			default: false
		},
		pain : {
			type: Boolean,
			default: false
		},
		parathesia : {
			type: Boolean,
			default: false
		},
		tmj : {
			type: Boolean,
			default: false
		},
		other : {
			type: Boolean,
			default: false
		},
		otherreason : {
			type : String
		}
	},
	intraoral : {
		exudate : {
			type: Boolean,
			default: false
		},
		swelling : {
			type: Boolean,
			default: false
		},
		hemorrhage : {
			type: Boolean,
			default: false
		},
		pain : {
			type: Boolean,
			default: false
		},
		mobility : {
			type: Boolean,
			default: false
		},
		occlusion : {
			type: Boolean,
			default: false
		},
		biotype : {
			type: Boolean,
			default: false
		},
		hardTissues : {
			type: Boolean,
			default: false
		},
		other : {
			type: Boolean,
			default: false
		},
		otherreason : {
			type : String
		}
	},
	medications : {
		type : String
	},
	allergies : {
		type : String
	},
	hr : {
		type : Number
	},
	systolicBP : {
		type : Number
	},
	bloodSugar : {
		type : Number
	},
	temperature : {
		type : Number
	},
	created : {
		type: Date,
		default: Date.now()
	}
});

mongoose.model('dentalNurseNotes', dentalNurseNotesSchema);