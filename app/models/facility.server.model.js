'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var FacilitySchema = new Schema({
	children: [{
		type: String
	}],
	name: {
		type: String,
		unique: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Facility', FacilitySchema);