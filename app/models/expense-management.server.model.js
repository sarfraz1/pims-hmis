'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Appointment Schema
 */
var ExpenseManagementSchema = new Schema({
	expenseNumber : {
		type: String,
		unique: true,
		required: 'Expense Number is required.'
	},
	expenseType : {
		type: String,
		required: 'Expense type is required.'
	},
	description : {
		type: String,
		required: 'Expense description is required.'
	},
	amount : {
		type: Number,
		required: 'Expense amount is required.'
	},
	department : {
		type: String,
		required: 'Department is required.'
	},
	tags : [{
		type: String
	}],
	fileUrl : {
		type: String,
		default: ''
	},
	repeat : {
		type: Boolean,
		required: 'Expense repeat bool is required.'
	},
	repeatNum : {
		type : Number
	},
	repeatFrequency : {
		type : String,
		default: 'Month'
	},
	created: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('ExpenseManagement', ExpenseManagementSchema);