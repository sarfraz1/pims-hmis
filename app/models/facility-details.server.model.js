'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Doctor Schema
 */
var FacilityDetailsSchema = new Schema({
	description : {
		type: String
	},
	category : {
		type: String
	},
	expenses : [{
		description : {
			type: String
		},
		cost : {
			type: Number
		}
	}],
	shares : [{
		doctorId : {
			type: String
		},
		doctorName : {
			type: String
		},
		commissionPerc : {
			type: Number
		}
	}],
	created: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('FacilityDetails', FacilityDetailsSchema);