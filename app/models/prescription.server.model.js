 'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Medicine Schema
 */
var PrescriptionSchema = new Schema({

	advice : {
		type : String
	},
	complaint : {
		type : String
	},
	date : {
		type : Date,
		default: Date.now()
	},
	nurseNotesId : {
		type: String
	},
	diagnosis : {
		type : String
	},
	procedures : {
		type : String
	},
	followupDate : {
		type : Date
	},
	instructions : {
		type : String
	},
	doctorDisplayName : {
		type : String
	},
	patientName : {
		type : String
	},
	doctorUsername : {
		type: String
	},
	MRNo : {
		type : String
	},
	labTest : [{
		description : {
			type:String
		},
		remarks : {
			type:String
		}
	}],
	medicineReminder : {
		type : Boolean
	},
	handledByPharmacy : {
		type: Boolean,
		default: false
	},
	medicines : [{
		days : {
			type: Number
		},
		dosage: {
			type: String
		},
		meal: {
			type: String
		},
		name: {
			type: String
		},
		timing : {
			type: String
		},
		instructions : {
			type: String
		}
	}],
	notes : {
		type : String
	},
	appointment_id: {
		type : String
	},
	patientstate : {
		state : {
			type:String
		},
		remarks : {
			type:String
		}
	},
	transferto : {
		ward : {
			type:String
		},
		remarks : {
			type:String
		}
	},
	movisit: {
		type: Number
	},
	nursevisit: {
		type: Number
	},
	radiology : [{
		description : {
			type:String
		},
		remarks : {
			type:String
		}
	}]
});

mongoose.model('Prescription', PrescriptionSchema);