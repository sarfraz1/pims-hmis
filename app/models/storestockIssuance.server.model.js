'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Stock Issuance Schema
 */
var StoreStockIssuanceSchema = new Schema({

	issuanceNumber: {
		type: String,
		unique: true
	},
	fromStoreId: {
		type: String,
	},
	fromStoreDescription: {
		type: String,
		default: ''
	},
	issuedTo: {
		type: String,
	},
	date: {
		type: Date,
		default: Date.now()
	},
	remarks:  {
			type: String,
	},
	itemsList: [{ 
		code: {
			type: String,
			default: ''
		},
       	description:  {
			type: String,
			default: ''
		},
       	quantity: {
			type: Number,
			default: ''
		}
	}]
});

mongoose.model('StoreStockIssuance', StoreStockIssuanceSchema);