'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * PanelLab Schema
 */
var PanelLabSchema = new Schema({
	description: {
		type: String,
		default: '',
		required: true,
		unique: true
	},
	address: {
		type: String,
		default: ''
	},
	contact: {
		type: String,
		default: ''
	},
	tests: [{
		description: {
			type: String
		},
		category: {
			type: String
		}
	}]
});

mongoose.model('PanelLab', PanelLabSchema);