'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Medicine Schema
 */
var PrescriptionPrefSchema = new Schema({
	'doctorName' : {
		type: String,
		unique: true
	},
    'doctorId' : {
        type : String,
        unique : true
    },
	'logo' : {
		type: Boolean,
		default: true
	},
	'complaint' : {
		type: Boolean,
		default: false
	},
	'advice' : {
		type: Boolean,
		default: true
	},
	'diagnosis' : {
		type: Boolean,
		default: true
	},
	'proceduresPerformed' : {
		type: Boolean,
		default: true
	},
	'medInstructions' : {
		type: Boolean,
		default: false
	},
	'personalNotes' : {
		type: Boolean,
		default: true
	},
	'mealOptions' : {
		type: Boolean,
		default: false
	},
	'printVitals' : {
		type: Boolean,
		default: false
	},	
	'timingEnglish' : {
		type: Boolean,
		default: true
	},
	'precrPadTop' : {
		type: Number,
		default: 0
	},
});

mongoose.model('PrescriptionPref', PrescriptionPrefSchema);