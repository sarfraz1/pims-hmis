'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Medicine Schema
 */
var MedicineSchema = new Schema({
	tradeName: [{
		description: { 
			type: String
        },
        inStock: {
        	type: Boolean,
            default: false
        }
	}],
	formula: {
		type: String
	}
});

mongoose.model('Medicine', MedicineSchema);