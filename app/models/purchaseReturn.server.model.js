'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Purchase Return Schema
 */

var purchaseReturnSchema = new Schema({
	purchaseReturnNumber : {
		type: String,
		default: '',
		unique: true
	},
	date : {
		type: Date,
		default: Date.now() 
	},
	transcationCategory : {
		type: String,
		default: ''
	},
	remarks : {
		type: String
	},
	storeId : {
		type: String
	},
	storeDescription : {
		type: String
	},
	itemsList : [{
		code : {
			type: String,
			default: ''
		},
		date : {
			type: Date,
			default: Date.now()
		},
		expiryDate : {
			type: Date,
			default: Date.now()
		},
		description : {
			type: String,
			default: ''
		},
		batchId : {
			type: String,
			default: ''
		},
		rate : {
			type: Number,
			default: 0
		},
		quantity : {
			type: Number,
			default: 0
		},
		netValue : {
			type: Number,
			default: 0
		}
	}]
});

mongoose.model('purchaseReturn', purchaseReturnSchema);