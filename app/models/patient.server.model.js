'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Patient Schema
 */
var PatientSchema = new Schema({
	mr_number: {
		type: String,
		unique: true,
		trim: true,
		maxlength: 12,
		required: 'Patient name is required'
	},
	name: {
		type: String,
		trim: true,
		minlength: 3,
		required: 'Patient name is required'
	},
	phone: {
		type: String,
		trim: true,
		minlength: 11,
		required: 'Patient phone number is required'
	},
	cnic: {
		type: String,
		trim: true,
		minlength: 15
	},
	address: {
		type: String,
	},
	patient_area: {
		type: String
	},
	panel_id: {
		type: String,
		minlength: 8
	},
	panelCardNumber: {
		type: String
	},
	mr_prefix: {
		type: String,
		maxlength: 3,
		default: 'PRT'
	},
	gender: {
		type: String,
		minlength: 4
	},
	email: {
		type: String,
		minlength: 8
	},
	dob: {
		type: Date,
	},
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

PatientSchema.index({mr_number:1}, {unique: true});

mongoose.model('Patient', PatientSchema);