'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Panel Schema
 */
var PanelSchema = new Schema({
	description: {
		type: String,
		unique: true,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	pocName: {
		type: String,
		default: ''
	},
	pocNumber: {
		type: String,
		default: ''
	},
	mrNumberPrefix: {
		type: String
	},
	address: {
		type: String,
		default: ''
	},
	remarks: {
		type: String,
		default: ''
	},
	overallLimit: {
		type: Number
	},
	roomLimit: {
		type: Number
	},		
	coverages: [{
			category: {
				type: String
			},
			overallDiscount: {
				type: Number
			},
			overallDiscountType: {
				type: String
			},
			facilities: [{
					description: {
						type: String
					},
					price: {
						type: Number
					},
					discount: {
						type: Number
					},
					discountType: {
						type: String
					}
				}
			]
		}
	]
});

mongoose.model('Panel', PanelSchema);