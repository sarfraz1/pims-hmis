'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Hospital Schema
 */
var HospitalSchema = new Schema({
	name: {
		type: String,
		default: ''
	},
	phone: [{
		number: {
			type: String,
			default: ''
		}
	}],
	fax: [{
		number: {
			type: String,
			default: ''
		}
	}],
	addressLineOne: {
		type: String,
		default: ''
	},
	addressLineTwo: {
		type: String,
		default: ''
	}
});

mongoose.model('Hospital', HospitalSchema);