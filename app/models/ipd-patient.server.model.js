'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * IPD Patient Schema
 */
var IPDPatientSchema = new Schema({

	name: {
		type: String
	},
	mr_number : {
		type: String
	},
	mr_prefix : {
		type: String
	},
	phone : {
		type: String
	},
	status: {
		type: String,
		default: 'Admitted'
	},
	transfertoward : {
		type: String
	},
	bed_details: {
		bed_id: {
			type: String
		},
		bed_description: {
			type: String
		},
		ward: {
			type: String
		}
	},
	bill_number : {
		type: String
	},

	refering_doctor: {
		name: {
			type: String
		},
		doc_id: {
			type: String
		}
	},
	assigned_doctor: {
		name: {
			type: String
		},
		doc_id: {
			type: String
		}
	},

surgeon: {
		name: {
			type: String
		},
		doc_id: {
			type: String
		}
	},

	anesthetist: {
			name: {
				type: String
			},
			id: {
				type: String
			}
		},
		OTA: {
				name: {
					type: String
				},
				id: {
					type: String
				}
			},

	package_details : {
		name: {
			type: String,
		},
		package_id : {
			type: String
		}
	},
	food : {
		type: String
	},
	medicine : [{
		days : {
			type: Number
		},
		dosage: {
			type: Number
		},
		meal: {
			type: String
		},
		name: {
			type: String
		},
		timing : {
			type: String
		},
		last_adminsitered : {
			type: Date,
		},
		next_administer : {
			type: Date,
		},
		start_datetime : {
			type: Date,
			default: Date.now
		}
	}],
	created: {
		type: Date,
		default: Date.now
	},
	modified: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('IPDPatient', IPDPatientSchema);
