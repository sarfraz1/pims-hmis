'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Expense Tags Schema
 */
var ExpenseTagsSchema = new Schema({
	tags : [{
		type: String
	}],
});

mongoose.model('ExpenseTags', ExpenseTagsSchema);