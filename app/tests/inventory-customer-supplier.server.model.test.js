'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	InventoryCustomerSupplier = mongoose.model('InventoryCustomerSupplier');

/**
 * Globals
 */
var user, inventoryCustomerSupplier;

/**
 * Unit tests
 */
describe('Inventory customer supplier Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			inventoryCustomerSupplier = new InventoryCustomerSupplier({
				// Add model fields
				// ...
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return inventoryCustomerSupplier.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		InventoryCustomerSupplier.remove().exec();
		User.remove().exec();

		done();
	});
});