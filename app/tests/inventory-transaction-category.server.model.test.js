'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	InventoryTransactionCategory = mongoose.model('InventoryTransactionCategory');

/**
 * Globals
 */
var user, inventoryTransactionCategory;

/**
 * Unit tests
 */
describe('Inventory transaction category Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			inventoryTransactionCategory = new InventoryTransactionCategory({
				// Add model fields
				// ...
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return inventoryTransactionCategory.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		InventoryTransactionCategory.remove().exec();
		User.remove().exec();

		done();
	});
});