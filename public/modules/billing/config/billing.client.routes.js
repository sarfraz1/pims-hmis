'use strict';

//Setting up route
angular.module('billing').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('generate_bill', {
			url: '/generate_bill/:MRN',
			templateUrl: 'modules/billing/views/generate_bill.client.view.html',
			access: ['opd receptionist']
		});
	}
]);