'use strict';

angular.module('billing').controller('GenerateBillController', ['$scope', '$http', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll','opd_service','hospitalAdmin_service','orderByFilter','billing_service','print_service',
	function($scope, $http, $stateParams, $location, $timeout,Authentication, ngToast, $anchorScroll,opd_service,hospitalAdmin_service,orderBy,billing_service,print_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.searchPatientKeyword ='';
        $scope.showPatients = false;
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getFaciltyPricing = function(){
            hospitalAdmin_service.get_facilities_and_price().then(function (response) {
                $scope.facilities = angular.copy(response.data);

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        var calculateGrandTotal = function(){
            var grandTotal = 0;
            for(var i=0;i<$scope.servicesData.length;i++){
                grandTotal = grandTotal + $scope.servicesData[i].fee*$scope.servicesData[i].quantity-$scope.servicesData[i].discount;
            }

            return grandTotal;
        };

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };

        var getDoctors = function(){
            hospitalAdmin_service.get_doctors().then(function (response) {
                $scope.doctors = response.data;
            });
        };

        var getBill = function(MRN){
            billing_service.get_bill(MRN).then(function (response) {
                if(response.data){
                    $scope.patient = response.data.patientInfo;

                    if(response.data.referedDoctor)
                        $scope.referedDoctor = JSON.stringify(response.data.referedDoctor);
                    if(response.data.referingDoctor)
                        $scope.referingDoctor = JSON.stringify(response.data.referingDoctor);

                    $scope.servicesData = response.data.servicesInfo;
                    //if(response.data.paymentMethod==='cheque'){
                    //}
                    //if()
                    var invoiceLen = response.data.invoice.length-1;
                    $scope.chequeInfo = response.data.invoice[invoiceLen].chequeInfo;
                    if($scope.chequeInfo)
                        $scope.chequeInfo.date = new Date($scope.chequeInfo.date);
                    $scope.cardInfo = response.data.invoice[invoiceLen].cardInfo;

                    $scope.selectPaymentMethod(response.data.invoice[invoiceLen].paymentMethod);

                    $scope.grandTotal = calculateGrandTotal();
                    $scope.recievedTemp = response.data.recievedCash;
                    $scope.balance = $scope.grandTotal-response.data.recievedCash;

                    if($scope.recieved>0){
                        $scope.disableEditing = true;
                    }else {
                        $scope.disableEditing = false;
                    }
                    //$scope.discount = response.data.discount;

                }
            });
        };

        var getHistoryBills = function(MRN){
            billing_service.get_history_bills(MRN).then(function (response) {
                $scope.billHistory = response.data;
            });
        };

        var getPanel = function(panelID){
            billing_service.get_panel(panelID).then(function (response) {
                console.log(response);
            });
        };

        $scope.confirmRefund = function(){
            $scope.processing = true;
            billing_service.save_refund($scope.refundDetails.billNumber,{'refundAmount':$scope.refundAmount,'refundRemarks':$scope.refundRemarks}).then(function (response) {
                //$scope.billHistory = response.data;
                print_service.print('/modules/billing/views/billing-refund-print.client.view.html',response.data,
                function(){
                    $scope.processing = false;
                    $scope.refundPopUp = false;
                    ngToast.create({
                        className: 'success',
                        content: 'Refund saved!',
                        dismissButton: true
                    });
                });

            }).catch(function(response){
                ngToast.create({
                    className: 'danger',
                    content: response.data.message,
                    dismissButton: true
                });
            });
        };

        $scope.refundClick = function(index){
            $scope.refundDetails = angular.copy($scope.billHistory[index]);
            console.log($scope.refundDetails);
            $scope.refundPopUp = true;
        };

        $scope.printoldBill = function(index){
						$scope.billHistory[index].hospitallogo = $scope.hosDetail[0].image_url;
						$scope.billHistory[index].hospitalname = $scope.hosDetail[0].title;
						$scope.billHistory[index].hospitalAddress = $scope.hosDetail[0].address;
						$scope.billHistory[index].hospitalPHNumber = $scope.hosDetail[0].number;
            print_service.print('/modules/billing/views/billing-print.client.view.html',$scope.billHistory[index],
            function(){
            });
        };

        $scope.updateBalance = function(){
            if($scope.balance>0)
                $scope.balance = $scope.grandTotal - $scope.recieved - $scope.recievedTemp;
            else
                $scope.balance = $scope.grandTotal - $scope.recieved;
        };

        $scope.selectFacility = function(index,service){
            service.service_id = $scope.facilities[index]._id;
            service.description = $scope.facilities[index].description;
            service.fee = $scope.facilities[index].price;
            service.quantity = 1;

            $scope.grandTotal = calculateGrandTotal();
            $scope.updateBalance();

            $scope.dropdownFlag = undefined;
        };

        $scope.disocuntChange = function(){
            $scope.grandTotal = calculateGrandTotal();
            $scope.updateBalance();
            //$scope.balance = $scope.grandTotal-$scope.recieved;
        };

        $scope.callbackpatient = function(selected_patient) {
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
            $scope.printAppointment.phoneNumber = $scope.patientData[patientIndex].phone;
            $scope.printAppointment.patientName = $scope.patientData[patientIndex].name;
            $scope.patient = $scope.patientData[patientIndex];
            $scope.showPatients = false;
            $scope.searchPatientKeyword = '';
            getBill(selected_patient[0].mr_number);
            getHistoryBills(selected_patient[0].mr_number);
            $scope.updateBalance();
        };

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
            if ($scope.searchPatientKeyword.length > 0) {
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    }
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Warning: Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };


        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.appointments = orderBy($scope.appointments, $scope.propertyName, $scope.reverse);
        };

        $scope.selectPaymentMethod = function(selected){
            if(selected==='cash'){
                $scope.cash = true;
                $scope.cheque = false;
                $scope.card = false;
            }
            else if(selected==='cheque'){
                $scope.cash = false;
                $scope.cheque = true;
                $scope.card = false;
            }else if(selected==='card'){
                $scope.cash = false;
                $scope.cheque = false;
                $scope.card = true;
            }

            selected = true;
        };

        $scope.addService = function(){
            $scope.servicesData.push({
                'description' : '',
                'fee' : 0,
                'quantity' : 0,
                'discount' : 0
            });
        };

        $scope.removeService = function(index){
            $scope.servicesData.splice(index,1);
        };

        $scope.openDropdown = function(index){

            // if($scope.dropdownFlag === index)
            //     $scope.dropdownFlag = undefined;
            // else{
                $scope.dropdownFlag = index;
            //}
        };

        $scope.closeDropdown = function(){
            $timeout(function() {
                $scope.dropdownFlag = undefined;
            },300);

        };

        var KeyCodes = {
            BACKSPACE : 8,
            TABKEY : 9,
            RETURNKEY : 13,
            ESCAPE : 27,
            SPACEBAR : 32,
            LEFTARROW : 37,
            UPARROW : 38,
            RIGHTARROW : 39,
            DOWNARROW : 40,
        };

        $scope.onKeydown = function(item, $event) {
            var e = $event;
            var $target = $(e.target);
            var nextTab;
            switch (e.keyCode) {
                case KeyCodes.ESCAPE:
                    $target.blur();
                    break;
                case KeyCodes.UPARROW:
                    nextTab = - 1;
                    break;
                case KeyCodes.RETURNKEY: e.preventDefault();
                case KeyCodes.DOWNARROW:
                    nextTab = 1;
                    break;
            }
            if (nextTab != undefined) {
                // do this outside the current $digest cycle
                // focus the next element by tabindex
                if (parseInt($target.attr('tabindex')) + nextTab !== 0)
                    $timeout(function() { $('[tabindex=' + (parseInt($target.attr('tabindex')) + nextTab) + ']').focus()});
            }
        };

        $scope.onFocus = function(item, $event) {
            // clear all other items
            angular.forEach($scope.facilities, function(item) {
                item.selected = undefined;
            });

            // select this one
            item.selected = 'selected';
        };

        $scope.submit = function(){
            $scope.processing = true;
            var referedDoctor,referingDoctor;

            if($scope.referedDoctor){
                try {
                    referedDoctor = JSON.parse($scope.referedDoctor);
                } catch(err){
                    console.log(err);
                }
            }
            if($scope.referingDoctor){
                try {
                    referingDoctor = JSON.parse($scope.referingDoctor);
                } catch(err){
                    console.log(err);
                }
            }

            var billingDetails = {
                'patientInfo' : $scope.patient,
                'referedDoctor' : referedDoctor,
                'referingDoctor' : referingDoctor,
                'servicesInfo' : $scope.servicesData,
                'grandTotal' : $scope.grandTotal,
                'recievedCash' : $scope.recieved,
                'discount' : $scope.discount,
            };

            billingDetails.status = 'billed';

            if($scope.cheque){
                billingDetails.chequeInfo = angular.copy($scope.chequeInfo);
                billingDetails.chequeInfo.date = dateConverter(billingDetails.chequeInfo.date);
                billingDetails.paymentMethod = 'cheque';
            } else if($scope.card) {
                billingDetails.cardInfo = $scope.cardInfo;
                billingDetails.paymentMethod = 'card';
            }
            else {
                billingDetails.paymentMethod = 'cash';
            }

            billingDetails.handlerName = $scope.authentication.user.username;

            billing_service.save_bill(billingDetails).then(function (response) {
							response.data.hospitallogo = $scope.hosDetail[0].image_url;
							response.data.hospitalname = $scope.hosDetail[0].title;
							response.data.hospitalAddress = $scope.hosDetail[0].address;
							response.data.hospitalPHNumber = $scope.hosDetail[0].number;
                print_service.print('/modules/billing/views/billing-print.client.view.html',response.data,
                function(){

                    $scope.reset();
                    $location.path('/reception');
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });

            //console.log(billingDetails);
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }

            $scope.cash = true;
            $scope.cheque = false;
            $scope.card = false;

            $scope.servicesData = [{
                'description' : '',
                'fee' : 0,
                'discount' : 0,
                'quantity' : 1
            }];

            $scope.referedDoctor = {};
            $scope.referingDoctor = {};

            panelDetails = {};
            $scope.patient = {};
            $scope.grandTotal = 0;
            $scope.recieved = 0;
            $scope.discount = 0;
            $scope.balance = 0;
            $scope.recievedTemp = 0;
            $scope.processing = false;
            $scope.dropdownFlag = undefined;
            $scope.disableEditing = false;
            $scope.refundPopUp = false;
            $scope.todayDate = new Date();
            $scope.todayDate = dateConverter($scope.todayDate);
            $scope.billHistory = [];

            $scope.refundDetails = {};
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();

        };

        $scope.init = function(){
            getFaciltyPricing();
            getDoctors();
            if($stateParams.MRN)
                getBill($stateParams.MRN);
        };

        var patient = {};
        var panelDetails = {};
        $scope.printAppointment = {};
        $scope.init();
        $scope.reset();
    }
]);
