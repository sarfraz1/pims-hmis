'use strict';

angular.module('billing').factory('billing_service', ['$http', 'config_service',
	function($http, config_service) {
		/* 
		 * Methods for Billing
		 */
		
		var srvr_address = config_service.serverAddress;
		this.save_bill = function(bill) {
			return $http.post(srvr_address+'/bill',bill);
		};

		this.get_bill = function(patientMrn) {
			return $http.get(srvr_address+'/patientBill/'+patientMrn);
		};

		this.get_history_bills = function(patientMrn) {
			return $http.get(srvr_address+'/patientHistory/'+patientMrn);
		};

		this.get_panel = function(panelID){
			return $http.get(srvr_address+'/panel/'+panelID);
		};

		this.save_refund = function(billNum,RefundInfo) {
			return $http.post(srvr_address+'/refundBill/'+billNum,RefundInfo);
		};



		

		return this;
	}
]);