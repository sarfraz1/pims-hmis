'use strict';

//Inventories service used to communicate Inventories REST endpoints
angular.module('storeinventories').factory('storeinventory_service', ['$http','Upload', '$q', 'config_service',
	function($http, Upload, $q, config_service) {
        var allMedicines = [];
		var srvr_address = config_service.serverAddress;

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        /**
         * Methods for Inventory Stock
         */
         this.create_storeinventory_stock = function(item) {
            return $http.post(srvr_address+'/storeinventory-stock', item);
         };

         this.list_storeinventory_stock = function() {
            return $http.get(srvr_address+'/storeinventory-stock');
         };

         this.list_supplier_storeinventory_stock = function(supplierId) {
            return $http.get(srvr_address+'/storeinventory-supplier-stock-list/'+supplierId);
         };

         this.delete_storeinventory_stock = function(item) {
            return $http.delete(srvr_address+'/storeinventory-stock/' + item._id);
         };

         this.update_storeinventory_stock = function(item) {
            return $http.put(srvr_address+'/storeinventory-stock/' + item._id, item);
         };

         this.search_storestock_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-stock/inventory/' + keyword);
         };
		 
		 
		var search_storeinventory_name_store = function(keyword, store) {
			return $http.get(srvr_address+'/getStoreInventorybyStore/' + keyword+'/'+store);
		};
		
		this.search_storeinventory_by_name_store = function(keyword, store) {
			var deferred = $q.defer();
			var searched_medicines = [];
			search_storeinventory_name_store(keyword, store).then(function (response) {
				for (var j = 0; j < response.data.length; j++) {
					searched_medicines.push({code: response.data[j].id, description: response.data[j].description, totalstock: response.data[j].totalstock});
				}
				deferred.resolve(searched_medicines);
			}).catch(function (err) {
				return deferred.reject();
			});
			return deferred.promise;
		};

        /**
         * Methods for Inventory Customer Supplier
         */
         this.search_storecustomer_supplier_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-customer-supplier/name/' + keyword);
         };

         this.list_storeinventory_customer_supplier = function() {
            return $http.get(srvr_address+'/storeinventory-customer-supplier');
         };

         this.create_storeinventory_customer_supplier = function(item) {
            return $http.post(srvr_address+'/storeinventory-customer-supplier', item);
         };
		 
         this.create_storestocktransfer = function(item) {
            return $http.post(srvr_address+'/storestockTransfer', item);
         };
		 
         this.create_storestockissuance = function(item) {
            return $http.post(srvr_address+'/storestockIssuance', item);
         };

         this.delete_storeinventory_customer_supplier = function(item) {
            return $http.delete(srvr_address+'/storeinventory-customer-supplier/' + item._id);
         };

         this.update_storeinventory_customer_supplier = function(item) {
            return $http.put(srvr_address+'/storeinventory-customer-supplier/' + item._id, item);
         };

         this.get_storeinventory_customer_suppliers = function(item) {
            return $http.get(srvr_address+'/storeinventory-customer-supplier');
         };

         this.get_storeinventory_customer_supplier = function(id) {
            return $http.get(srvr_address+'/storeinventory-customer-supplier/' + id);
         };
		 
         this.get_all_manufacturers = function() {
            return $http.get(srvr_address+'/storeinventory-getmanufacturers');
         };

        /**
         * Methods for Inventory Transaction Category
         */
        this.search_storetransaction_category_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-transaction-category/type/' + keyword);
        };

        this.list_storeinventory_transaction_category = function() {
            return $http.get(srvr_address+'/storeinventory-transaction-category');
        };

        this.create_storeinventory_transaction_category = function(item) {
            return $http.post(srvr_address+'/storeinventory-transaction-category', item);
        };

        this.delete_storeinventory_transaction_category = function(item) {
            return $http.delete(srvr_address+'/storeinventory-transaction-category/' + item._id);
        };

        this.update_inventory_transaction_category = function(item) {
            return $http.put(srvr_address+'/storeinventory-transaction-category/' + item._id, item);
        };

        /**
         * Methods for Inventory Store
         */
        this.search_store_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-store/description/' + keyword);
        };

        this.list_storeinventory_store = function() {
            return $http.get(srvr_address+'/storeinventory-store');
        };

        this.create_storeinventory_store = function(item) {
            return $http.post(srvr_address+'/storeinventory-store', item);
        };

        this.delete_storeinventory_store = function(item) {
            return $http.delete(srvr_address+'/storeinventory-store/' + item._id);
        };

        this.update_storeinventory_store = function(item) {
            return $http.put(srvr_address+'/storeinventory-store/' + item._id, item);
        };

        this.get_storeinventory_stores = function(item) {
            return $http.get(srvr_address+'/storeinventory-store/');
        };

        this.get_storeinventory_store = function(id) {
            return $http.get(srvr_address+'/storeinventory-store/' + id);
        };

        /**
         * Methods for Inventory Label
         */
        this.search_storelabel_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-label/name/' + keyword);
        };

        this.list_storeinventory_label = function() {
            return $http.get(srvr_address+'/storeinventory-label');
        };

        this.create_storeinventory_label = function(item) {
            return $http.post(srvr_address+'/storeinventory-label', item);
        };

        this.delete_storeinventory_label = function(item) {
            return $http.delete(srvr_address+'/storeinventory-label/' + item._id);
        };

        this.update_storeinventory_label = function(item) {
            return $http.put(srvr_address+'/storeinventory-label/' + item._id, item);
        };

        /**
         * Methods for Inventory Unit
         */
        this.search_storeunit_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/storeinventory-unit/name/' + keyword);
        };

        this.list_storeinventory_unit = function() {
            return $http.get(srvr_address+'/storeinventory-unit');
        };

        this.create_storeinventory_unit = function(item) {
            return $http.post(srvr_address+'/storeinventory-unit', item);
        };

        this.delete_storeinventory_unit = function(item) {
            return $http.delete(srvr_address+'/storeinventory-unit/' + item._id);
        };

        this.update_storeinventory_unit = function(item) {
            return $http.put(srvr_address+'/storeinventory-unit/' + item._id, item);
        };

        /**
         * Methods for Inventory Pricing
         */
    	this.create_storeinventory_pricing = function(item) {
    		return $http.post(srvr_address+'/storeinventory-pricing', item);
    	};

    	this.delete_storeinventory_pricing = function(item) {
    		return $http.delete(srvr_address+'/storeinventory-pricing/'+item.code);
    	};

    	this.update_storeinventory_pricing = function(item) {
    		return $http.put(srvr_address+'/storeinventory-pricing/'+item.inventoryID, item);
    	};

    	this.view_storeinventory_pricing = function(item) {
            return $http.get(srvr_address+'/storeinventory-pricing/'+item.code)
                .then(function(response) {
                    if (typeof response.data === 'object') {
                        return response.data;
                    } else {
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    return $q.reject(response.data);
                });
    	};

    	this.list_inventory_pricing = function() {
    		return $http.get(srvr_address+'/storeinventory-pricing');
    	};

        /**
         * Methods for Inventory
         */
    	this.create_storeinventory = function(item){
    		inventoryItem = item;
            allMedicines.push(item);
	    	return $http.post(srvr_address+'/storeinventories', item);
    	};

    	this.delete_storeinventory = function(item){
    		inventoryItem = item;
            var itemIndex = getIndex(allMedicines, 'code', item.code);
            allMedicines.splice(itemIndex, 1);
	    	return $http.delete(srvr_address+'/storeinventories/'+item.code);
    	};

    	this.get_storeinventory_by_code = function(code){
	    	return $http.get(srvr_address+'/storeinventories/'+code);
    	};
		
    	this.update_storeinventory = function(item){
    		inventoryItem = item;
	    	return $http.put(srvr_address+'/storeinventories/'+item.code , item);
    	};

    	this.search_store_by_keyword = function(keyword){
    		return $http.get(srvr_address+'/getStoreInventory/description/'+keyword);
    	};

        this.getStoreItemCode = function(){
            return $http.get(srvr_address+'/getStoreInventoryItemCode');
        };

        this.getStoreUnits = function(){
            return $http.get(srvr_address+'/storeinventory-unit');
        };

        this.getStoreLabels = function(){
            return $http.get(srvr_address+'/storeinventory-label');
        };

        this.getStoreTransactionCategories = function(){
            return $http.get(srvr_address+'/storeinventory-transaction-category');
        };

        this.getStoreGRN = function() {
            return $http.get(srvr_address+'/storeGRN');
        };
		
        this.getStoreGRNByNumber = function(keyword) {
            return $http.get(srvr_address+'/storeGRNbyNumber/' + keyword);
        };

        this.getSearchedStoreGRN = function(keyword) {
            return $http.get(srvr_address+'/storeGRN/GRNNumber/' + keyword);
        };

        this.getAllStoreGRN = function() {
            return $http.get(srvr_address+'/storeGRN/all');
        };

        //////////////Purchase Return///////////////////
        this.DeleteStorePurchaseReturn= function(obj) {
            return $http.get(srvr_address+'/DeleteStorePurchaseReturn/' + obj);
        };
        
        this.getSearchedStorePurchaseReturn= function(keyword) {
            return $http.get(srvr_address+'/StorePurchaseReturnById/' + keyword);
        };
		
        this.getStoreStockReport= function() {
            return $http.get(srvr_address+'/storeinventory-stockreport');
        };
		
		this.getAverageStoreStockPrice= function(supplier) {
			return $http.get(srvr_address+'/storeinventory-stockaverageprice/'+supplier);
		};

        this.getLowStoreStockReport= function() {
            return $http.get(srvr_address+'/storeinventory-lowstockreport');
        };		

        this.UpdateStorePurchaseReturn= function(Obj) {
            return $http.post(srvr_address+'/UpdateStorePurchaseReturn' , Obj );
        };


        this.getStorePurchaseReturn = function() {
            return $http.get(srvr_address+'/storepurchaseReturn');
        };
        //////////////////////////////////////////////////

       

        this.getPendingStorePurchaseRequisitions = function() {
            return $http.get(srvr_address+'/storepurchaseRequisition/status/Pending');
        };

        this.getSearchedPendingStorePurchaseRequisitions = function(keyword) {
            return $http.get(srvr_address+'/storepurchaseRequisition/code/' + keyword);
        };

        this.getStorePurchaseRequisitions = function(){
            return $http.get(srvr_address+'/storepurchaseRequisition');
        };

        this.getStorePurchaseOrders = function(){
            return $http.get(srvr_address+'/storepurchaseOrder');
        };

        this.getSearchedStorePurchaseOrders = function (keyword) {
            return $http.get(srvr_address+'/storepurchaseOrder/number/' + keyword);
        };

        this.listAllItems = function() {
            var deferred = $q.defer();
            if (allMedicines.length === 0) {
                $http.get(srvr_address+'/storeinventories').then(function (response) {
                    allMedicines = response.data;
                    deferred.resolve(allMedicines);
                });
            } else {
                deferred.resolve(allMedicines);
            }
            return deferred.promise;
        };

        this.getItemExpirayStatus = function(itemCode){
            return $http.get(srvr_address+'/getStoreInventoryItemExpiryStatus/'+itemCode);
            
        };

        this.uplaodImage = function(item,file){
            return Upload.upload({
                url: '/storeinventoryImage',
                fields: item,
                file: file
            }).success(function (data, status, headers, config) {
                console.log('Inventory Item is Saved!');
             }).error(function (data, status, headers, config) {
                            });            
        };

        this.create_StoreGRN = function(grn_obj){
            return $http.post('/storeGRN' , grn_obj);
        };

        this.update_StoreGRN = function(grn_obj) {
            return $http.put(srvr_address+'/storeGRN', grn_obj);
        };

        this.delete_StoreGRN = function(grn_obj) {
            return $http.delete(srvr_address+'/storeGRN/' + grn_obj._id);
        };

        this.create_storepurchase_order = function(purchaseOrder){
            return $http.post(srvr_address+'/storepurchaseOrder' , purchaseOrder);
        };

        this.update_purchase_order = function(purchaseOrder) {
            return $http.put(srvr_address+'/storepurchaseOrder/' + purchaseOrder._id, purchaseOrder);
        };

        this.delete_storepurchase_order = function(purchaseOrder) {
            return $http.post(srvr_address+'/deleteStorePurchaseOrder',purchaseOrder);
        };

        this.create_storepurchase_requisition = function(purchaseRequisition){
            return $http.post(srvr_address+'/storepurchaseRequisition' , purchaseRequisition);
        };

        this.update_storepurchase_requisition = function(purchaseRequisition) {
            return $http.post(srvr_address+'/storepurchaseRequisitionUpdate', purchaseRequisition);
        };

        this.delete_storepurchase_requisition = function(purchaseRequisition) {
            return $http.delete(srvr_address+'/storepurchaseRequisition/' + purchaseRequisition._id);
        };
        
        this.create_storepurchase_return = function(purchaseReturn){
            return $http.post(srvr_address+'/storepurchaseReturn' , purchaseReturn);
        };

        this.get_hospital_details = function() {
            return $http.get(srvr_address+'/hospital');
        };

        this.get_storeinventory_pricing_supplier = function(supplierId) {
            return $http.get(srvr_address+'/storeinventory-pricing-supplier/' + supplierId);
        };
		
        this.get_storeinventory_pricing_supplier_stock = function(supplierId) {
            return $http.get(srvr_address+'/storeinventory-pricing-supplier-stock/' + supplierId);
        };

        this.update_storeinventory_grn_status = function(inventory_code){
            for(var i=0;i<allMedicines.length;i++){
                if(allMedicines[i].code===inventory_code){
                    allMedicines[i].grn_created = true;
                    return true;
                }
            }
        };

        this.get_storeinventory_details = function(code) {
            return $http.get(srvr_address+'/getStoreInventoryDetailsByCode/' + code);
        };

        /**
         * Method for passing data between controllers
         */
        var inventoryItem;

        this.return_storeinventory_data = function() {
            return inventoryItem;
        };

		return this;
	}
]);