'use strict';

// Inventories controller
angular.module('storeinventories').controller('StorepurchaseRequisitionController', ['$scope', '$http','$stateParams', '$location', '$timeout', 'Authentication', 'storeinventory_service','ngToast','$anchorScroll',
    function($scope,$http ,$stateParams, $location, $timeout, Authentication, storeinventory_service,ngToast,$anchorScroll) {
  
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.showAllItems = false;

        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showGrid = false;
        $scope.searchRequisitionKeyword = '';
        // medicines simple picklist
        $scope.allMedicines = []; // for data retrieval
        $scope.allItems = []; // to be passed to directive
        $scope.prefix = 'meddiv';
        $scope.headingsMedicines = [];
        $scope.headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 33});
        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
        $scope.headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
        // purchase requisitions simple picklist
        $scope.allRequisitions = [];
        $scope.samplejson = [];
        $scope.prefixRequisitions = 'prdiv';
        $scope.headingsRequisitions = [];
        $scope.headingsRequisitions.push({'alias': 'Purchase Requisition Code', 'name': 'code', 'width': 30});
        $scope.headingsRequisitions.push({'alias': 'Remarks', 'name': 'remarks', 'width': 50});
        $scope.headingsRequisitions.push({'alias': 'Date', 'name': 'date', 'width': 20});

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        };

        var pushItems = function(index) {
            var formula = '';
            if ($scope.allMedicines[index].formula === '' || $scope.allMedicines[index].formula == null)
                formula = 'No Formula';
            else
                formula = $scope.allMedicines[index].formula;
            $scope.allItems.push({'code': $scope.allMedicines[index].code, 'description': $scope.allMedicines[index].description, 'formula': formula});
        };

        $scope.loadMoreItems = function() {
            var length = $scope.allItems.length + 100;
            if (length < 1000) {
                for (var i = length - 100; i < length; i++) {
                    if ($scope.allMedicines[i] != undefined) {
                        pushItems(i);
                    }
                }
            }
        };

        $scope.searchMedicines = function(item) {
            $scope.allItems = [];
            for (var i = 0; i < $scope.allMedicines.length; i++) {
                if ($scope.allItems.length > 100) {
                    break;
                }
                if (item.length === 0) {
                    pushItems(i);
                } else {
                    for (var key in $scope.allMedicines[i]) {
                        for (var j = 0; j < $scope.headingsMedicines.length; j++) {
                            if (key === $scope.headingsMedicines[j].name) {
                                if ($scope.allMedicines[i][key].toLowerCase().includes(item.toLowerCase())) {
                                    pushItems(i);
                                }
                            }
                        }
                    }
                }
            }
        };

        var getAllItems = function(){
            $scope.processing = true;
            storeinventory_service.listAllItems()
                .then(function(response) {
                    if (response.length > 0) {
                        $scope.showItemButton = true;
                        $scope.allItems = [];
                        $scope.headingsMedicines = [];
                        $scope.headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
                        $scope.allMedicines = response;
                        var max;
                        if($scope.allMedicines.length>100){
                            max=100;
                        }else 
                            max = $scope.allMedicines.length;
                            
                        for (var i = 0; i < max; i++) {
                            pushItems(i);
                        }
                        $scope.processing = false;
                    } else {
                        $scope.showItemButton = false;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        var getItemPricing = function(index){
            
            if($scope.itemsList[index]!==undefined){
                storeinventory_service.view_storeinventory_pricing($scope.itemsList[index]).then(function(response) {
                  $scope.itemsList[index].estRate = response.purchasingPrice;
                  $scope.itemsList[index].requestedBy = $scope.authentication.user.displayName;
                  index++;
                  getItemPricing(index);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var prIndex = getIndex($scope.allRequisitions, 'code', selected_item[0].code);
            $scope.purchaseRequisition = $scope.allRequisitions[prIndex];
            $scope.purchaseRequisition.date = new Date($scope.allRequisitions[prIndex].date);
            $scope.itemsList = $scope.allRequisitions[prIndex].itemsList;
            for (var i = 0; i < $scope.allRequisitions[prIndex].itemsList.length; i++) {
                $scope.itemsList[i].estDeliveryDate = new Date($scope.allRequisitions[prIndex].itemsList[i].estDeliveryDate);
            }
            $scope.searchPurchaseKeyword = '';
            $scope.UpdateItem = true;
            $scope.showGrid = false;
        };

        var handleResponse = function(response) {
            if (response.data.length > 0) {
                $scope.showGrid = true;
                $scope.samplejson = [];
                $scope.allRequisitions = response.data;
                $scope.headingsRequisitions = [];
                $scope.headingsRequisitions.push({'alias': 'Purchase Requisition Code', 'name': 'code', 'width': 30});
                $scope.headingsRequisitions.push({'alias': 'Remarks', 'name': 'remarks', 'width': 50});
                $scope.headingsRequisitions.push({'alias': 'Date', 'name': 'date', 'width': 20});
                for (var i = 0; i < response.data.length; i++) {
                    var dateFormat = '';
                    var remarks = '';
                    var dateFormat = getDateFormat(response.data[i].date);
                    if (response.data[i].remarks === '') {
                        remarks = 'No Remarks';
                    } else {
                        remarks = response.data[i].remarks;
                    }
                    $scope.samplejson.push({'code': response.data[i].code, 'remarks': remarks, 'date': dateFormat});
                }
                var setFocus = angular.element(document.querySelector('#' + $scope.prefixRequisitions + $scope.prefixRequisitions));
                $timeout(function() {setFocus[0].focus()});
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'No results found.',
                    dismissButton: true
                });
            }
        };

        $scope.getPendingPurchaseRequisitions = function() {
            if ($scope.searchRequisitionKeyword.length === 0) {
                storeinventory_service.getPendingStorePurchaseRequisitions().then(function (response) {
                    handleResponse(response);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Purchase Requisitions.',
                        dismissButton: true
                    });
                });
            } else if ($scope.searchRequisitionKeyword.length > 0) {
                storeinventory_service.getSearchedPendingStorePurchaseRequisitions($scope.searchRequisitionKeyword).then(function (response) {
                    handleResponse(response);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Purchase Requisitions.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.createItemButton = function() {
            $location.path('/inventories/create');
        };

        $scope.reset = function(form){
            if (form) {
                form.$setPristine();
                form.$setUntouched();
                form.submitted = false;
            }
            
            $scope.processing = false;
            $scope.searchKeyword = '';
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.itemsList = [];
            $scope.UpdateItem = false;
            $scope.itemsQty = false;
            $scope.purchaseRequisition = {
                'code' : '',
                'transcationCategory': 'Default',
                'date': new Date(),
                'itemsList':[],
                'remarks':undefined
            };
            $scope.purchaseRequisition.date = new Date($scope.purchaseRequisition.date);
            //getAllItems();
            getTransactionCategories();
        };

        var getTransactionCategories = function(){
          storeinventory_service.getStoreTransactionCategories()
            .then(function(response) {
              $scope.transcationCategories = response.data;
              $scope.transcationCategories.unshift({'purchaseType': 'Default'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Document Category List.',
                    dismissButton: true
                });
            });
        };

        $scope.search_medicines = function() {
             if($scope.search_keyword.length>2){
                 $scope.show_dropdown = true;
             }
             else if($scope.search_keyword.length===0){
                 $scope.show_dropdown = false;
             }
        };

        $scope.add_medicine = function(med_obj) {

            for (var i = 0; i < med_obj.length; i++) {
                var medExists = false;
                for(var j=0;j<$scope.itemsList.length;j++){
                    if($scope.itemsList[j].code===med_obj[i].code){
                        medExists = true;
                    }
                }
                if(medExists === false){
                    var medIndex = getIndex($scope.allMedicines, 'code', med_obj[i].code);
                    $scope.itemsList.push($scope.allMedicines[medIndex]);
                }
            }
            $scope.showAllItems = false;
            $scope.show_dropdown = false;
            $scope.search_keyword = '';
            getItemPricing(0);

        };

        $scope.remove_medicine = function(index) {
             $scope.itemsList.splice(index,1); 
        };

        $scope.itemsSearchTypeahead = function(val){
            return inventory_service.search_by_keyword(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function(error) {
                console.log(error);
            });
        };

        $scope.selectedSearchItem = function(obj){
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===obj.code){
                    medExists = true;
                }
            }
            if(medExists === false){
                 storeinventory_service.view_storeinventory_pricing(obj).then(function(response) {

                  obj.estRate = response.purchasingPrice;
                  obj.requestedBy = $scope.authentication.user.displayName;
                  $scope.itemsList.push(obj);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
            $scope.searchKeyword = '';
        };

        $scope.delete_purchase_requisition = function() {
            $scope.confirmationPopup = 'confirmation-popup-right';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            storeinventory_service.delete_storepurchase_requisition($scope.purchaseRequisition).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Purchase Requisition Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Purchase Requisition.',
                    dismissButton: true
                });
            });
        };

        $scope.update_purchase_requisition = function(purchaseRequisition, form) {
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined){
                    $scope.itemsQty = true;
                    return false;
                } else if ($scope.itemsList[i].estDeliveryDate === null) {
                    $scope.itemsList[i].estDeliveryDate = new Date();
                }
                $scope.itemsList[i].estDeliveryDate = dateConverter($scope.itemsList[i].estDeliveryDate);
            }

            purchaseRequisition.itemsList = $scope.itemsList;

            if (purchaseRequisition.date === null) {
                purchaseRequisition.date = new Date();
            }
            
            if (purchaseRequisition.transcationCategory !== undefined && purchaseRequisition.date !== undefined && purchaseRequisition.itemsList !== 0) {
                purchaseRequisition.date = dateConverter(purchaseRequisition.date);
                $scope.processing = true;
                storeinventory_service.update_storepurchase_requisition(purchaseRequisition).then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Purchase Requisition Updated Successfully',
                        dismissButton: true
                    });
                    $scope.reset(form);
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Purchase Requisition.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.submit_purchase_requisition = function(purchaseRequisition,form){

            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined){
                    $scope.itemsQty = true;
                    return false;
                } else if ($scope.itemsList[i].estDeliveryDate === null) {
                    $scope.itemsList[i].estDeliveryDate = new Date();
                }
                $scope.itemsList[i].estDeliveryDate = dateConverter($scope.itemsList[i].estDeliveryDate);
            }

            purchaseRequisition.itemsList = $scope.itemsList;

            if (purchaseRequisition.date === null) {
                purchaseRequisition.date = new Date();
            }

            
            if(purchaseRequisition.transcationCategory!==undefined && purchaseRequisition.date!==undefined && purchaseRequisition.itemsList.length!==0){
                purchaseRequisition.date = dateConverter(purchaseRequisition.date);
                $scope.processing = true;
                storeinventory_service.create_storepurchase_requisition(purchaseRequisition).then(function(response){
                    ngToast.create({
                        className: 'success',
                        content: 'Purchase Requisition Created Successfully',
                        dismissButton: true
                    });
                    $scope.reset(form);
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to create Purchase Requisition.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.reset();

        $scope.openItemsList = function() {
            $scope.showAllItems = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };
    }
]);