'use strict';

angular.module('inventories').controller('StoreInventoriesTransactionCategoryController', ['$scope', '$stateParams', '$location', 'Authentication', 'storeinventory_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, storeinventory_service, ngToast, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Document Description', 'name': 'category', 'width': 49});
            $scope.headings.push({'alias': 'Document Type', 'name': 'purchaseType', 'width': 49});
            $scope.samplejson = [];
            $scope.samplejson = response.data;
            $scope.categoriesToShow = [];
            for (var i = 0; i < $scope.samplejson.length; i++) {
                $scope.categoriesToShow.push({'category': $scope.samplejson[i].category, 'purchaseType': $scope.samplejson[i].purchaseType});
            }
        };
        
        var getItems = function() {
            storeinventory_service.list_storeinventory_transaction_category()
                .then(function (response) {
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

        getItems();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var documentIndex = getIndex($scope.samplejson, 'category', selected_item[0].category);
            $scope.transactionCategory = $scope.samplejson[documentIndex];
            $scope.UpdateItem = true;
            $scope.transactionCategory.numberingType = $scope.samplejson[documentIndex].numberingType;
            $scope.transactionCategory.automaticType = $scope.samplejson[documentIndex].automaticType;
        };

        $scope.showTable = false;
		$scope.UpdateItem = false;
		$scope.showGrid = false;
		$scope.samplejson = [];
        $scope.transactionCategory = {};
        $scope.transactionCategory.numberingType = 'manual';
        $scope.transactionCategory.purchaseType = 'Purchase Requisition';
        $scope.transactionCategory.automaticType = 'Yearly';
        $scope.confirmationPopup = 'hide-popup';
        $scope.headings = [];
        $scope.headings.push({'alias': 'Document Description', 'name': 'category', 'width': 49});
        $scope.headings.push({'alias': 'Document Type', 'name': 'purchaseType', 'width': 49});
        $scope.categoriesToShow = [];

		var transactionCategory = {
			'purchaseType': 'Purchase Requisition',
			'prefix': '',
			'category': '',
			'numberingType': 'manual',
			'automaticType': ''
		};

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.transactionCategory = angular.copy(transactionCategory);
            getItems();
            $scope.confirmationPopup = 'hide-popup';
		};

        $scope.submit_transaction_category = function(transactionCategory, form) {
    		storeinventory_service.create_storeinventory_transaction_category(transactionCategory).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Document Category Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
                $scope.transactionCategory.automaticType = 'Yearly';
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Document Category exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add Document Category.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.delete_transaction_category = function() {
            $scope.confirmationPopup = 'confirmation-popup document-transaction-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            storeinventory_service.delete_storeinventory_transaction_category($scope.transactionCategory).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Document Category Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                $scope.transactionCategory.automaticType = 'Yearly';
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Document Category.',
                    dismissButton: true
                });
            });
        };

        $scope.update_transaction_category = function(transactionCategory, form) {
        	storeinventory_service.update_storeinventory_transaction_category(transactionCategory).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Document Category Updated Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
                $scope.transactionCategory.automaticType = 'Yearly';
            }).catch(function (error) {
                if (error.data.message.indexOf('already exists') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Document Category exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Document Category.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_transaction_category = function(form) {
            $scope.reset(form);
            $scope.transactionCategory.automaticType = 'Yearly';
        };
	}
]);