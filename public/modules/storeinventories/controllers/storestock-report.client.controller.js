'use strict';

angular.module('storeinventories').controller('StoreStockReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'storeinventory_service', 'print_service', '$anchorScroll','$timeout',
    function($scope, $http, $stateParams, $location, Authentication, ngToast, storeinventory_service, print_service, $anchorScroll,$timeout) {

        $scope.authentication = Authentication;
        $scope.selectedSupplier = 'all';

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        else {
            // if($scope.authentication.user.roles === 'admin') {
            //     //$location.path('/admin-main-page');
            // }
            // else{
            //     $location.path('/signin')
            // }
        }

        $scope.totalInStock = '';

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var getAllStock = function(){
			$scope.totalstockprice = 0;
            storeinventory_service.getStoreStockReport().then(function (response) {
                for(var i=0;i<response.data.length;i++){
                   var totalQuantity = 0;
				   if(response.data[i].batch) {
					   for(var j=0;j<response.data[i].batch.length;j++){
						   if(response.data[i].batch[j].storeDescription !== 'Stock Adjustment') {
								totalQuantity = totalQuantity + response.data[i].batch[j].quantity;
						   }
					   }
				   }

				   if(response.data[i].inventory_pricing.length>0) {
						response.data[i].stockprice = (totalQuantity*response.data[i].inventory_pricing[response.data[i].inventory_pricing.length-1].sellingPrice).toFixed(2);
					} else {
						response.data[i].stockprice = 0;
					}
				   response.data[i].totalInStock = totalQuantity;
				   //if(response.data[i].stockprice !== 'Not defined')
					$scope.totalstockprice+=Number(response.data[i].stockprice);
                }
				$scope.totalstockprice =  $scope.totalstockprice.toFixed(2);
                $scope.inventoryStock = response.data;
				storeinventory_service.getAverageStoreStockPrice('All').then(function (response) {
				$scope.purchaseprices = response.data;
					for(var i=0;i<$scope.inventoryStock.length;i++) {
						for(var j=0;j<$scope.purchaseprices.length;j++) {
							if($scope.inventoryStock[i].id == $scope.purchaseprices[j].id) {
								$scope.inventoryStock[i].averagePrice = $scope.purchaseprices[j].averagePrice;
								break;
							}
						}
					}
				});
            }).catch(function(err){
                console.log(err);
            });

        };

        var getSuppliers = function(){
            storeinventory_service.get_storeinventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
                    $timeout(function() {
                        $scope.selectedSupplier = 'all';
                    }, 100);

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier list.',
                    dismissButton: true
                });
            });
        };

		$scope.printreport = function() {
			var dt = moment(new Date());
			var ts = [];

			var obj = {
				'inventoryStock': $scope.inventoryStock,
				'totalAmounts' : $scope.totalstockprice,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
      obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/storeinventories/views/stock-report-print.client.view.html',obj,
			function(){
			});
		};

        $scope.updateReport = function(){
			$scope.totalstockprice = 0;
            if($scope.selectedSupplier === 'all'){
                getAllStock();
            }
            else {
                $scope.processing = true;
                storeinventory_service.list_supplier_storeinventory_stock($scope.selectedSupplier)
                    .then(function(response) {

						for(var i=0;i<response.data.length;i++){

						   response.data[i].stockprice = (response.data[i].totalInStock*response.data[i].sellingPrice).toFixed(2);

						   $scope.totalstockprice+=Number(response.data[i].stockprice);
						}
                    $scope.totalstockprice =   $scope.totalstockprice.toFixed(2);
					$scope.inventoryStock = response.data;
					$scope.processing = false;
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Customer/Supplier list.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.quantityFilter = function(item) {
            if ($scope.totalInStock === '')
                return item.totalInStock;
            else
                return item.totalInStock <= $scope.totalInStock;
        };


        $scope.init = function(){
            getAllStock();
            getSuppliers();
            $scope.processing = false;
        };

        $scope.init();

    }
]);
