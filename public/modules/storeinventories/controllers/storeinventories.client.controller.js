'use strict';

// Inventories controller
angular.module('storeinventories').controller('StoreInventoriesController', ['$scope', '$stateParams', '$location', '$timeout', 'Authentication', 'storeinventory_service','ngToast','$anchorScroll',

	function($scope, $stateParams, $location, $timeout, Authentication, storeinventory_service,ngToast,$anchorScroll) {
		
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if (!$scope.authentication.user) $location.path('/signin');

		var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };


		var upload_image = function(itemCode){
			var obj = {'imageLoc' : './public/images/item_images/'+itemCode+fileFormat};

        	storeinventory_service.uplaodImage(obj,$scope.item.itemImage)
	            .then(function(response) {
	                //console.log(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to upload image.',
                    dismissButton: true
                });
            });
		};

		var getItemCode = function(){
			storeinventory_service.getStoreItemCode()
	            .then(function(response) {
	            $scope.item.code = response.data;
	    
	        }).catch(function(error) {
    			console.log(error);
  			});
		};

		var getUnits = function(){
			storeinventory_service.getStoreUnits()
	            .then(function(response) {
	            	$scope.units = response.data;
	            	for (var i = response.data.length - 1; i >= 0; i--) {
	            		if(response.data[i].defaultOption===true){
							$scope.item.unit = response.data[i].description;
	            			$scope.item.purchase_unit = response.data[i].description;
							$scope.item.selling_unit = response.data[i].description;
							$scope.item.storage_unit = response.data[i].description;
	            		}
	            	}
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Units.',
                    dismissButton: true
                });
            });
		};

		var getLabels = function(){
			storeinventory_service.getStoreLabels()
	            .then(function(response) {
	            	$scope.labels = response.data;
	            	for (var i = response.data.length - 1; i >= 0; i--) {
	            		if(response.data[i].defaultOption===true){
							$scope.item.label = response.data[i].description;
	            		}
	            	}
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Labels.',
                    dismissButton: true
                });
            });
		};

		var getSuppliers = function(){
            storeinventory_service.get_storeinventory_customer_suppliers()
                .then(function(response) {
                	if (response.data.length > 0) {
                		$scope.showSupplierButton = true;
                    	$scope.suppliers = response.data;
                        $scope.headingsSuppliers = [];
                        $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
                        $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});
                        $scope.suppliersToShow = [];
                        for (var i = 0; i < response.data.length; i++) {
                            $scope.suppliersToShow.push({'description': response.data[i].description, 'NTN': response.data[i].NTN});
                        }
                    } else {
                    	$scope.showSupplierButton = false;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier list.',
                    dismissButton: true
                });
            });
        };

		var getManufacturers = function(){
            storeinventory_service.get_all_manufacturers()
                .then(function(response) {
                	if (response.data.length > 0) {
                    	$scope.manufacturers = response.data;
					}
            });
        };		
		
        var callSearchTimeout = function() {
            $scope.showSearchMessage = false;
        };

        $scope.purcPriceFrmPurcUnit = function(){
        	if($scope.item.storage_to_selling>=1 && $scope.item.pricing.sellingPricePurchaseUnit>0){
                
                $scope.item.pricing.purchasingPrice = $scope.item.pricing.sellingPricePurchaseUnit-($scope.item.pricing.sellingPricePurchaseUnit*0.15);
        	    $scope.item.pricing.sellingPrice = $scope.item.pricing.sellingPricePurchaseUnit/$scope.item.storage_to_selling;
                $scope.item.pricing.sellingPrice = Number($scope.item.pricing.sellingPrice.toFixed(2));
                $scope.item.pricing.purchasingPrice = Number($scope.item.pricing.purchasingPrice.toFixed(2));
            }
        };

        $scope.purcPriceFrmSellUnit = function(){
            if($scope.item.storage_to_selling>=1 && $scope.item.pricing.sellingPrice>0){
                $scope.item.pricing.sellingPricePurchaseUnit = $scope.item.pricing.sellingPrice*$scope.item.storage_to_selling;
                $scope.item.pricing.purchasingPrice = $scope.item.storage_to_selling * $scope.item.pricing.sellingPrice-($scope.item.storage_to_selling * $scope.item.pricing.sellingPrice*0.15);
                $scope.item.pricing.sellingPricePurchaseUnit = Number($scope.item.pricing.sellingPricePurchaseUnit.toFixed(2));
                $scope.item.pricing.purchasingPrice = Number($scope.item.pricing.purchasingPrice.toFixed(2));
            }
        };

        $scope.changeStorageUnit = function(){
        	$scope.item.storage_unit = $scope.item.purchase_unit;
        };

        $scope.createSupplierButton = function() {
        	$location.path('/inventory-customer-supplier');
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}

			$scope.processing = false;
			$scope.searchKeyword = '';
			$scope.UpdateItem = false;
			// $scope.supplierColumns = ['description', 'name', 'NTN'];
        
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
			$scope.item = angular.copy(item);
			getItemCode();
			getUnits();
			getLabels();
			getSuppliers();
			getManufacturers();
		};

		$scope.uploadFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.item.itemImage = image.target.result;
                        
                        if(file.type==='image/png'){
                        	fileFormat = '.png';
                        }
                    	else if(file.type==='image/jpeg') {
                    		fileFormat = '.jpeg';
                    	}
                        
                        //console.log(file.type);
                        //console.log(scope.myImage);
                    });
                };
                imageReader.readAsDataURL(file);
            }
        };

        $scope.submit_item = function(item,form){
        	$scope.item.pricing.retailPrice = $scope.item.pricing.sellingPrice;
        	if ($scope.item.pricing.dateTransaction === null) {
        		$scope.item.pricing.dateTransaction = new Date();
        	}
        	if ($scope.item.pricing.effectiveDate === null) {
        		$scope.item.pricing.effectiveDate = new Date();
        	}
    		var item_obj = item;

    		if(fileFormat!==''){
	        	item_obj.image_url = '/images/item_images/'+item_obj.code+fileFormat;
	        	upload_image(item_obj.code);

	        } else {
	        	item_obj.image_url = '';
	        }

	        $scope.item.pricing.effectiveDate = dateConverter($scope.item.pricing.effectiveDate);
	        $scope.item.pricing.dateTransaction = dateConverter($scope.item.pricing.dateTransaction);
	        
	        $scope.processing = true;
	        storeinventory_service.create_storeinventory(item_obj)
	            .then(function(response) {
	            	item_obj.pricing.inventoryCode = response.data.code;
  			
		  			storeinventory_service.create_storeinventory_pricing(item_obj.pricing).then(function(response) {
		
					}).catch(function (error) {
					    ngToast.create({
					        className: 'danger',
					        content: 'Error: Unable to add Inventory Pricing.',
					        dismissButton: true
					    });
					});


					ngToast.create({
					  className: 'success',
					  content: 'Inventory Added Successfully',
					  dismissButton: true
					});
					$scope.reset(form);
	                //$location.path('/inventory-pricing');
            }).catch(function (error) {
            	$scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add Inventory.',
                    dismissButton: true
                });
            });
  			
        };

        $scope.update_item = function(item,form){
        	$scope.processing = true;
        	$scope.item.pricing.retailPrice = $scope.item.pricing.sellingPrice;
        	
        	
        	if ($scope.item.pricing.dateTransaction === null) {
        		$scope.item.pricing.dateTransaction = new Date();
        	}
        	if ($scope.item.pricing.effectiveDate === null) {
        		$scope.item.pricing.effectiveDate = new Date();
        	}
    		var item_obj = item;
      		
      		if(fileFormat!==''){
      			item_obj.image_url = '/images/item_images/'+item_obj.code+fileFormat;
      			upload_image(item_obj.code);
      		}

      		$scope.item.pricing.effectiveDate = dateConverter($scope.item.pricing.effectiveDate);
	        $scope.item.pricing.dateTransaction = dateConverter($scope.item.pricing.dateTransaction);
	        

    		storeinventory_service.update_storeinventory(item_obj)
	            .then(function(response) {

	            	item_obj.pricing.inventoryCode = item_obj.code;
  			
		  			storeinventory_service.create_storeinventory_pricing(item_obj.pricing).then(function(response) {
		                
		            }).catch(function (error) {
		                ngToast.create({
		                    className: 'danger',
		                    content: 'Error: Unable to update Inventory Pricing.',
		                    dismissButton: true
		                });
		            });
		            ngToast.create({
					  className: 'success',
					  content: 'Inventory Updated Successfully',
					  dismissButton: true
					});
					$scope.reset(form);
					//$location.path('/inventory-pricing');
            }).catch(function (error) {
            	$scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update Inventory.',
                    dismissButton: true
                });
            });   
        };

        $scope.delete_item = function(){
        	$scope.confirmationPopup = 'confirmation-popup';
        };

        $scope.hide_delete_popup = function(){
        	$scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(item,form){
        	$scope.confirmationPopup = 'hide-popup';
        	var item_obj = item;

	        storeinventory_service.delete_storeinventory(item_obj)
	            .then(function(response) {
	            	ngToast.create({
					  className: 'success',
					  content: 'Inventory Deleted Successfully',
					  dismissButton: true
					});
					storeinventory_service.delete_storeinventory_pricing(item_obj)
						.then(function (response) {
							//console.log(response);
		            }).catch(function (error) {
		                ngToast.create({
		                    className: 'danger',
		                    content: 'Error: Unable to delete Inventory Pricing.',
		                    dismissButton: true
		                });
		            });					
            		$scope.reset(form);
	                //console.log(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Inventory.',
                    dismissButton: true
                });
            });
        };


        $scope.searchMedicines = function(item) {
            $scope.medicinesToShow = [];
            for (var i = 0; i < $scope.samplejson.length; i++) {
                if ($scope.medicinesToShow.length > 100) {
                    break;
                }
                if (item.length === 0) {
                    pushItems(i);
                } else {
                    for (var key in $scope.samplejson[i]) {
                        for (var j = 0; j < $scope.headingsMedicines.length; j++) {
                            if (key === $scope.headingsMedicines[j].name) {
                                if ($scope.samplejson[i][key].toLowerCase().includes(item.toLowerCase())) {
                                    pushItems(i);
                                }
                            }
                        }
                    }
                }
            }
        };

        var handleResponse = function(response) {
            $scope.samplejson = response;
            $scope.headingsMedicines = [];
            $scope.headingsMedicines.push({'alias': 'Code', 'name': 'code', 'width': 49});
            $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 49});
            $scope.medicinesToShow = [];
            // var max;
            // if($scope.samplejson.length>100){
            //     max=100;
            // }else 
            //     max = $scope.samplejson.length;
            for (var i = 0; i < 100; i++) {
                pushItems(i);
            }
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixMedicine + $scope.prefixMedicine));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.getItems = function() {
        	if ($scope.searchKeyword.length === 0) {
                $scope.processing = true;
        		storeinventory_service.listAllItems()
        			.then(function (response) {
                        if (response.length > 0) {
                            $scope.samplejson = [];
                            handleResponse(response);
                            $scope.showGrid = true;
                            $scope.processing = false;
                        } else {
                            ngToast.create({
                                className: 'warning',
                                content: 'No results found.',
                                dismissButton: true
                            });
                            $scope.processing = false;
                        }
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to retrieve Inventory Items.',
	                    dismissButton: true
	                });
	            });
        	}
        };

        $scope.itemsSearchTypeahead = function(val){
        	return storeinventory_service.search_store_by_keyword(val)
	            .then(function (response) {
	            	return response.data.map(function(item){
				        return item;
				    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
       /*     if (Array.isArray(selected_item)) {
                var itemIndex = getIndex($scope.samplejson, 'code', selected_item[0].code);
            } else {
                var itemIndex = getIndex($scope.samplejson, 'code', selected_item.code);
            }*/
            $scope.item = selected_item;
            $scope.searchKeyword = $scope.item.code;
	    	$scope.item.itemImage = '';
	    	storeinventory_service.view_storeinventory_pricing($scope.item).then(function(response) {
           		var pricing = response;
           		pricing.effectiveDate = new Date(pricing.effectiveDate);
           		pricing.dateTransaction = new Date(pricing.dateTransaction);
           		$scope.item.pricing = pricing;
           		$scope.item.pricing.purchasingPrice = Number($scope.item.pricing.purchasingPrice.toFixed(2));
                $scope.item.pricing.retailPrice = Number($scope.item.pricing.retailPrice.toFixed(2));
                $scope.item.pricing.sellingPrice = Number($scope.item.pricing.sellingPrice.toFixed(2));
                $scope.item.pricing.sellingPricePurchaseUnit = Number(($scope.item.pricing.sellingPrice*$scope.item.storage_to_selling).toFixed());
                if($scope.item.interactions.length==0) {
					$scope.item.interactions = [{'name': ''}];
				}
				
                storeinventory_service.get_storeinventory_customer_supplier($scope.item.pricing.supplierId).then(function(supplierResponse) {
	           		$scope.item.pricing.supplier = supplierResponse.data;
	      
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to retrieve Inventory Supplier.',
	                    dismissButton: true
	                });
	            });

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Pricing.',
                    dismissButton: true
                });
            });

            $scope.item.image_url = $scope.item.image_url + '?' + new Date().getTime();            
	    	$scope.UpdateItem = true;
	    	$scope.showGrid = false;
	 	};

	 	$scope.descChange = function(){

	 		if($scope.item.description===undefined){
	 			$scope.item.formula = '';
	 		}
	 	};

	 	$scope.tradeNameSearchTypeahead = function(val){
        };

	 	$scope.setTradeName = function(selected_item) {
	 		$scope.item.formula = selected_item.formula;
	 	};


	 	$scope.selectSupplier = function(supplierDetails){
            var supplierIndex = getIndex($scope.suppliers, 'NTN', supplierDetails[0].NTN);
            $scope.item.pricing.supplier = $scope.suppliers[supplierIndex];
	 		$scope.item.pricing.supplierId = $scope.suppliers[supplierIndex]._id;
	 		$scope.showSupplierList = false;
	 	};

	 	$scope.hide_directive = function(){
	 		$scope.showGrid = false;
	 	};
		
        $scope.addFormula = function(){
        	var tempMed = {
        		'name' : ''
        	};
        	$scope.item.interactions.push(tempMed);
        };
		
        $scope.removeFormula = function(obj,index) {
            obj.splice(index,1);
        };

        $scope.authentication = Authentication;

		$scope.options = [
    		{value: true, label: 'Yes'},
    		{value: false, label: 'No'}];

        var item = {
			'code' : '',
			'description' : '',
			'consumption' : 'Trading',
			'discount' : 0,
			'effective_date' : new Date(),
			'tax_inclusive_in_retail' : true,
			'label' : 'Label 1',
			'price' : undefined,
			'price_date' : undefined,
			'purchase_price' : undefined,
			'storage_to_selling' : 1,
			'purchase_unit' : undefined,
			'retail_price' : undefined,
			'tax_sales' : 0,
			'sales_tax_applicale' : true,
			'selling_price' : undefined,
			'selling_unit' : undefined,
			'purchase_to_storage' : 1,
			'storage_unit' : undefined,
			'tax_applicale_on' : 'Selling',
			'expiry_date_enabled' : true,
			'unit' : undefined,
			'image_url': '',
			'type' : 'Inventory Item',
			'itemImage' : '',
			'remarks' : '',
			'pricing' : {
				'pricingType' : 'Sale',
				'dateTransaction' : new Date(),
				'discountAmount' : 0,
				'discountType' : 'percent',
				'sellingPrice' : undefined,
				'purchasingPrice' : undefined,
				'retailPrice' : undefined,
				'effectiveDate' : new Date(),
				'supplierId' : ''
			}
		};

		$scope.reset();
        $scope.prefixSupplier = 'supplierDiv';
        $scope.prefixMedicine = 'medicineDiv';
        $scope.prefixBrand = 'brandDiv';
        $scope.suppliers = [];
        $scope.headingsSuppliers = [];
        $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});
        $scope.suppliersToShow = [];
        $scope.samplejson = [];
        $scope.headingsMedicines = [];
        $scope.headingsMedicines.push({'alias': 'Code', 'name': 'code', 'width': 49});
        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.medicinesToShow = [];
        $scope.brandNames = [];
        $scope.headingsBrandName = [];
        $scope.headingsBrandName.push({'alias': 'Description', 'name': 'description', 'width': 99});
        $scope.brandNamesToShow = [];
		$scope.mode = 'Basic';
		$scope.confirmationPopup = 'hide-popup';
		$scope.UpdateItem = false;
		$scope.showFormulaTradeName = false;
		$scope.showGrid = false;

		var fileFormat = '';

        $scope.openSupplierList = function() {
            $scope.showSupplierList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixSupplier + $scope.prefixSupplier));
            $timeout(function() {setFocus[0].focus()});
        };

	}
]);