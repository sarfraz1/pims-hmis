'use strict';

// Inventories controller
angular.module('storeinventories').controller('StoreGRNController', ['$scope', '$http', '$stateParams', '$location', '$timeout', 'Authentication', 'storeinventory_service', 'print_service', 'ngToast','$anchorScroll',
    function($scope, $http, $stateParams, $location, $timeout, Authentication, storeinventory_service, print_service, ngToast, $anchorScroll) {

        $scope.authentication = Authentication;

        if (!$scope.authentication.user) $location.path('/signin');

        $scope.mode = 'Advanced';
        $scope.showGrid = false;
        $scope.searchGRNKeyword = '';
        $scope.confirmationPopup = 'hide-popup';
        // medicines simple picklist
        $scope.allMedicines = []; // for data retrieval
        $scope.allItems = []; // to be passed to directive
        $scope.prefix = 'meddiv';
        $scope.headingsMedicines = [];
        $scope.headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 33});
        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
        $scope.headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
        // parameters for expandable picklist
        $scope.purchaseOrderData = [];
        $scope.PO = [];
        $scope.prefixPO = 'podiv';
        $scope.headingsPO = [];
        $scope.headingsPO.push({'alias': 'Purchase Order Number', 'name': 'purchaseOrderNumber', 'width': 50});
        $scope.headingsPO.push({'alias': 'Creation Date', 'name': 'date', 'width': 50});
        $scope.expandableHeadingPO = 'itemsList';
        $scope.expandableHeadingsPO = [];
        $scope.expandableHeadingsPO.push({'alias': 'Item Code', 'name': 'itemcode', width: 10});
        $scope.expandableHeadingsPO.push({'alias': 'Description', 'name': 'description', width: 20});
        $scope.expandableHeadingsPO.push({'alias': 'Discount', 'name': 'discount', width: 5});
        $scope.expandableHeadingsPO.push({'alias': 'Purchase Requisition Code', 'name': 'purchaseRequisitionCode', width: 15});
        $scope.expandableHeadingsPO.push({'alias': 'Purchase Requisition Remarks', 'name': 'purchaseRequisitionRemarks', width: 20});
        $scope.expandableHeadingsPO.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
        $scope.expandableHeadingsPO.push({'alias': 'Rate', 'name': 'rate', width: 10});
        $scope.expandableHeadingsPO.push({'alias': 'Remarks', 'name': 'remarks', width: 10});
        // GRN search simple picklist
        $scope.GRNData = [];
        $scope.prefixGRN = 'grndiv';
        $scope.headingsGRN = [];
        $scope.GRNs = [];
        $scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 20});
		$scope.headingsGRN.push({'alias': 'Supplier', 'name': 'supplierDescription', 'width': 20});
		$scope.headingsGRN.push({'alias': 'Supp Invoice Number', 'name': 'supplierChalanNumber', 'width': 20});
        $scope.headingsGRN.push({'alias': 'Date', 'name': 'date', 'width': 20});
        $scope.headingsGRN.push({'alias': 'Remarks', 'name': 'remarks', 'width': 20});
        // suppliers simple picklist
        $scope.suppliers = [];
        $scope.suppliersToShow = [];
        $scope.prefixSuppliers = 'suppliersdiv';
        $scope.headingsSuppliers = [];
        $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});
        // store simple picklist
        $scope.storeData = [];
        $scope.storesToShow = [];
        $scope.prefixStore = 'storediv';
        $scope.headingsStores = [];
        $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getTransactionCategories = function(){
          storeinventory_service.getStoreTransactionCategories()
            .then(function(response) {
              $scope.transcationCategories = response.data;
              $scope.transcationCategories.unshift({'purchaseType': 'Default'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Document Category List.',
                    dismissButton: true
                });
            });
        };

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        };

         var getItemPricing = function(index){

            if($scope.itemsList[index]!==undefined){
                storeinventory_service.view_storeinventory_pricing($scope.itemsList[index]).then(function(response) {
                  $scope.itemsList[index].rate = response.purchasingPrice;
                  $scope.itemsList[index].purchasingPrice = response.purchasingPrice;
                  $scope.itemsList[index].sellingPrice = response.sellingPrice;
                  //$scope.itemsList[index].requestedBy = $scope.authentication.user.displayName;
                  index++;
                  getItemPricing(index);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
        };

        var getPurchaseOrders = function(){
          storeinventory_service.getStorePurchaseOrders()
            .then(function(response) {
                $scope.purchaseOrderData = response.data;
                $scope.headingsPO = [];
                $scope.PO = [];
                $scope.expandableHeadingsPO = [];
                $scope.headingsPO.push({'alias': 'Purchase Order Number', 'name': 'purchaseOrderNumber', 'width': 50});
                $scope.headingsPO.push({'alias': 'Creation Date', 'name': 'date', 'width': 50});
                $scope.expandableHeadingsPO.push({'alias': 'Item Code', 'name': 'itemcode', width: 10});
                $scope.expandableHeadingsPO.push({'alias': 'Description', 'name': 'description', width: 20});
                $scope.expandableHeadingsPO.push({'alias': 'Discount', 'name': 'discount', width: 5});
                $scope.expandableHeadingsPO.push({'alias': 'Purchase Requisition Code', 'name': 'purchaseRequisitionCode', width: 15});
                $scope.expandableHeadingsPO.push({'alias': 'Purchase Requisition Remarks', 'name': 'purchaseRequisitionRemarks', width: 20});
                $scope.expandableHeadingsPO.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
                $scope.expandableHeadingsPO.push({'alias': 'Rate', 'name': 'rate', width: 10});
                $scope.expandableHeadingsPO.push({'alias': 'Remarks', 'name': 'remarks', width: 10});
                for (var i = 0; i < response.data.length; i++) {
                    var creationDate = getDateFormat(response.data[i].date);
                    $scope.PO.push({'purchaseOrderNumber': response.data[i].purchaseOrderNumber, 'date': creationDate,'itemsList': []});
                    for (var j = 0; j < response.data[i].itemsList.length; j++) {
                        var prRemarks = '';
                        var itemRemarks = '';
                        var prcode = '';
                        if (response.data[i].itemsList[j].purchaseRequisitionRemarks === '') {
                            prRemarks = 'No Remarks';
                        } else {
                            prRemarks = response.data[i].itemsList[j].purchaseRequisitionRemarks;
                        }
                        if (response.data[i].itemsList[j].remarks === '') {
                            itemRemarks = 'No Remarks';
                        } else {
                            itemRemarks = response.data[i].itemsList[j].remarks;
                        }
                        if (response.data[i].itemsList[j].purchaseRequisitionCode === '') {
                            prcode = 'None';
                        } else {
                            prcode = response.data[i].itemsList[j].purchaseRequisitionCode;
                        }
                        $scope.PO[i].itemsList.push({'code': response.data[i].itemsList[j].code, 'description': response.data[i].itemsList[j].description, 'discount': response.data[i].itemsList[j].discount, 'purchaseRequisitionCode': prcode, 'purchaseRequisitionRemarks': prRemarks, 'quantity': response.data[i].itemsList[j].quantity, 'rate': response.data[i].itemsList[j].rate, 'remarks': itemRemarks});
                    }
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Purchase Order List.',
                    dismissButton: true
                });
            });
        };

        var pushItems = function(index) {
            var formula = '';
            if ($scope.allMedicines[index].formula === '' || $scope.allMedicines[index].formula == null)
                formula = 'No Formula';
            else
                formula = $scope.allMedicines[index].formula;
            $scope.allItems.push({'code': $scope.allMedicines[index].code, 'description': $scope.allMedicines[index].description, 'formula': formula});
        };

        $scope.loadMoreItems = function() {
            var length = $scope.allItems.length + 100;
            if (length < 1000) {
                for (var i = length - 100; i < length; i++) {
                    if ($scope.allMedicines[i] != undefined) {
                        pushItems(i);
                    }
                }
            }
        };

        $scope.searchMedicines = function(item) {
            $scope.allItems = [];
            for (var i = 0; i < $scope.allMedicines.length; i++) {
                if ($scope.allItems.length > 100) {
                    break;
                }
                if (item.length === 0) {
                    pushItems(i);
                } else {
                    for (var key in $scope.allMedicines[i]) {
                        for (var j = 0; j < $scope.headingsMedicines.length; j++) {
                            if (key === $scope.headingsMedicines[j].name) {
                                if ($scope.allMedicines[i][key].toLowerCase().includes(item.toLowerCase())) {
                                    pushItems(i);
                                }
                            }
                        }
                    }
                }
            }
        };

        var getAllItems = function(){
            $scope.processing = true;
            storeinventory_service.listAllItems()
                .then(function(response) {
                    if (response.length > 0) {
                        $scope.showItemButton = true;
                        $scope.allItems = [];
                        $scope.headingsMedicines = [];
                        $scope.headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
                        $scope.allMedicines = response;

                        var max;
                        if($scope.allMedicines.length>100){
                            max = 100;
                        }else
                            max = $scope.allMedicines.length;


                        for (var i = 0; i < max; i++) {
                            pushItems(i);
                        }
                        $scope.processing = false;
                    } else {
                        $scope.showItemButton = false;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

		$scope.print = function(form) {

              $scope.printData.total = 0;
			  $scope.createdDate = moment($scope.printData.created).format('DD-MM-YYYY hh:mma');
			  $scope.printingDate = moment().format('DD-MM-YYYY hh:mma');
        $scope.hospitallogo = $scope.hosDetail[0].image_url;
        $scope.hospitalname = $scope.hosDetail[0].title;
        $scope.hospitalAddress = $scope.hosDetail[0].address;
        $scope.hospitalPHNumber = $scope.hosDetail[0].number;
        $scope.hospitalEmail = $scope.hosDetail[0].email;

              /*for(var k=0;k<$scope.printData.itemsList.length;k++){
                $scope.printData.total = $scope.printData.total + $scope.printData.itemsList[k].netValue;
              }*/
              print_service.printFromScope('/modules/inventories/views/grn-print.client.view.html',$scope,function(){

              });
				$scope.disableSubmit = true;
				$scope.processing = false;
				$scope.reset(form);
		};

        var getSuppliers = function(){
            storeinventory_service.get_storeinventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
                    $scope.headingsSuppliers = [];
                    $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
                    $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});
                    $scope.suppliersToShow = [];
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.suppliersToShow.push({'description': response.data[i].description, 'NTN': response.data[i].NTN});
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier List.',
                    dismissButton: true
                });
            });
        };

        var getStores = function(){
            storeinventory_service.get_storeinventory_stores()
                .then(function(response) {
                if (response.data.length > 0) {
                    $scope.storeData = [];
                    $scope.storeData = response.data;
                    $scope.storesToShow = [];
                    $scope.headingsStores = [];
                    $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.storesToShow.push({'description': response.data[i].description});
                    }
                    $scope.selectedStore = response.data[0];
                    $scope.GRN.storeId = response.data[0]._id;
                    $scope.GRN.storeDescription = response.data[0].description;
                    $scope.showStoresList = false;
                } else {
                    ngToast.create({
                        className: 'warning',
                        content: 'Warning: No Inventory Stores added.',
                        dismissButton: true
                    });
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

        var getAndSetItemsExpirayStatus = function(index){
            if(index<$scope.itemsList.length){
                storeinventory_service.getStoreItemExpirayStatus($scope.itemsList[index].code)
                .then(function(response) {
                    $scope.itemsList[index].expiry_date_enabled= response.data;
                    index++;
                    getAndSetItemsExpirayStatus(index);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Inventory Item Expiry Status.',
                        dismissButton: true
                    });
                });
            }
        };

        var calculate_total = function(){
          var totalAmount = 0;

          for(var i=0;i<$scope.itemsList.length;i++){
            var totalTax = 0;
            var totalDiscount = 0;
            if($scope.itemsList[i].discountType === 'PerItem'){
              totalDiscount = $scope.itemsList[i].quantity*$scope.itemsList[i].discount;
            }
            else if($scope.itemsList[i].discountType === 'perc'){
              totalDiscount = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].discount/100);
            }
            else if($scope.itemsList[i].discountType === 'lumsum'){
              totalDiscount = $scope.itemsList[i].discount;
            }else {
                $scope.itemsList[i].discountType = 'perc';
                $scope.itemsList[i].discount = 0;
            }

            if($scope.itemsList[i].taxType === 'perc') {
              totalTax = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].saleTax/100);
            }
            else if($scope.itemsList[i].taxType === 'lumsum'){
              totalTax = $scope.itemsList[i].saleTax;
            }
            else {
                $scope.itemsList[i].taxType  = 'perc';
                $scope.itemsList[i].saleTax = 0;
            }
            $scope.itemsList[i].netValue = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)+totalTax-totalDiscount;

            if(isNaN($scope.itemsList[i].netValue)){
              $scope.itemsList[i].netValue = 0;
            }

            totalAmount = totalAmount+$scope.itemsList[i].netValue;
            $scope.itemsList[i].netValue = Number($scope.itemsList[i].netValue.toFixed(2));
            totalAmount = Number(totalAmount.toFixed(2));
          }
          return totalAmount;
        };

        $scope.callbackfn = function(selecteditem) { //callback function to get object from directive
            //var itemIndex = getIndex($scope.GRNData, 'GRNNumber', selecteditem[0].GRNNumber);
            storeinventory_service.getStoreGRNByNumber(selecteditem[0].GRNNumber)
                .then(function(response) {
			var selected_item = response.data;
            $scope.GRN = selected_item;
			$scope.GRN.date = new Date($scope.GRN.date);
			$scope.printData = $scope.GRN;
            $scope.itemsList = selected_item.itemsList;

            for (var i = 0; i < $scope.itemsList.length; i++) {
                $scope.itemsList[i].expiryDate = new Date(selected_item.itemsList[i].expiryDate);
            }

            $scope.GRN.supplierChalanNumber = parseFloat(selected_item.supplierChalanNumber);
            $scope.GRN.gatePassNumber = parseFloat(selected_item.gatePassNumber);
            $scope.GRN.date = new Date(selected_item.date);
            $scope.GRN.supplierChalanDate = new Date(selected_item.supplierChalanDate);

            if (selected_item.supplierId !== '') {
                storeinventory_service.get_storeinventory_customer_supplier(selected_item.supplierId).then(function (response) {
                    $scope.selectedSupplier = response.data;
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Customer/Supplier List.',
                        dismissButton: true
                    });
                });
            }

            if (selected_item.storeId !== '') {
                storeinventory_service.get_storeinventory_store(selected_item.storeId).then(function (response) {
                    $scope.selectedStore = response.data;
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Inventory Stores.',
                        dismissButton: true
                    });
                });
            }

            $scope.totalAmount = calculate_total();
            getAndSetItemsExpirayStatus(0);
            $scope.UpdateItem = true;
            $scope.showGrid = false;
            $scope.searchGRNKeyword = '';
				});
        };

        var handleResponse = function(response) {
            if (response.data.length > 0) {
                $scope.showGrid = true;
                $scope.GRNData = [];
                $scope.GRNData = response.data;
                $scope.headingsGRN = [];
                $scope.GRNs = [];
				$scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 20});
				$scope.headingsGRN.push({'alias': 'Supplier', 'name': 'supplierDescription', 'width': 20});
				$scope.headingsGRN.push({'alias': 'Supp Invoice Number', 'name': 'supplierChalanNumber', 'width': 20});
				$scope.headingsGRN.push({'alias': 'Date', 'name': 'date', 'width': 20});
				$scope.headingsGRN.push({'alias': 'Remarks', 'name': 'remarks', 'width': 20});
                for (var i = 0; i < response.data.length; i++) {
                  var creationDate = getDateFormat(response.data[i].date);
                  var remarks = '';
                  if (response.data[i].remarks === '' || response.data[i].remarks === undefined || response.data[i].remarks === null) {
                    remarks = 'No Remarks';
                  } else {
                    remarks = response.data[i].remarks;
                  }
                  $scope.GRNs.push({'GRNNumber': response.data[i].GRNNumber, 'supplierDescription' : response.data[i].supplierDescription,
				  'supplierChalanNumber': response.data[i].supplierChalanNumber, 'date': creationDate, 'remarks': remarks});
                }
                var setFocus = angular.element(document.querySelector('#' + $scope.prefixGRN + $scope.prefixGRN));
                $timeout(function() {setFocus[0].focus()});
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'No results found.',
                    dismissButton: true
                });
            }
        };

        $scope.getGRN = function() {
          if ($scope.searchGRNKeyword.length === 0) {
            storeinventory_service.getStoreGRN().then(function (response) {
                handleResponse(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Goods Received Note.',
                    dismissButton: true
                });
            });
          } else if ($scope.searchGRNKeyword.length > 0) {
            storeinventory_service.getSearchedStoreGRN($scope.searchGRNKeyword).then(function (response) {
                handleResponse(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Goods Received Note.',
                    dismissButton: true
                });
            });
          }
        };

        $scope.calculate_total_net_amount = function(){
          $scope.totalAmount = calculate_total();
        };

        $scope.selectedItems = function(obj){
            for (var j = 0; j < obj.length; j++) {
                var objIndex = -1;
                for (var x = 0; x < $scope.purchaseOrderData.length; x++) {
                    if ($scope.purchaseOrderData[x].purchaseOrderNumber === obj[j].purchaseOrderNumber) {
                        objIndex = x;
                    }
                }
                var objtemp = $scope.purchaseOrderData[objIndex];
				$scope.purchaseOrderNumber = objtemp.purchaseOrderNumber;
				$scope.purchaseOrderDate = objtemp.date;
                var subobjIndex = -1;
                for (var y = 0; y < objtemp.itemsList.length; y++) {
                    if (objtemp.itemsList[y].code === obj[j].code) {
                        subobjIndex = y;
                    }
                }
                var subobjtemp = objtemp.itemsList[subobjIndex];
                var temp = {
                    'code' : subobjtemp.code,
                    'purchaseRequisitionCode' : subobjtemp.purchaseRequisitionCode,
                    'purchaseRequisitionRemarks' : subobjtemp.purchaseRequisitionRemarks,
                    'date' : subobjtemp.date,
                    'description' : subobjtemp.description,
                    'estDeliveryDate' : subobjtemp.estDeliveryDate,
                    'rate' : subobjtemp.rate,
                    'quantity' : subobjtemp.quantity,
                    'remarks' : subobjtemp.remarks,
                    'requestedBy' : subobjtemp.requestedBy,
                    'saleTaxType' : subobjtemp.saleTaxType,
                    'supplierQuotationDate' : objtemp.supplierQuotationDate,
                    'supplierQuotationNo' : objtemp.supplierQuotationNo,
                    'discount' : subobjtemp.discount,
                    'totalDiscount' : subobjtemp.totalDiscount,
                    'totalTax' : subobjtemp.totalTax,
                    'discountType' : subobjtemp.discountType,
                    'taxType' : subobjtemp.taxType,
                    'saleTax' : subobjtemp.saleTax,
                    'purchaseOrderNumber' : objtemp.purchaseOrderNumber
                };
                var medExists = false;
                for(var i=0;i<$scope.itemsList.length;i++){
                      if($scope.itemsList[i].code===temp.code && $scope.itemsList[i].purchaseRequisitionCode === temp.purchaseRequisitionCode){
                          medExists = true;
                      }
                }
                if(medExists === false){
                    $scope.itemsList.push(temp);
                    $scope.totalAmount = calculate_total();
                    getAndSetItemsExpirayStatus(0);
                }
            }
            $scope.showPurchaseOrders = false;
        };

        $scope.selectedSearchItem = function(obj){
            obj.discountType = 'perc';
            obj.discount = 0;
            obj.taxType = 'perc';
            obj.saleTax = 0;
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===obj.code){
                    medExists = true;
                }
            }
            if(medExists === false){
                storeinventory_service.view_storeinventory_pricing(obj).then(function(response) {
                  obj.rate = response.purchasingPrice;
                  obj.purchasePrice = response.purchasingPrice;
                  obj.sellingPrice = response.sellingPrice;
                  $scope.itemsList.unshift(obj);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
            $scope.searchKeyword = '';
        };

        $scope.remove_medicine = function(index) {
          $scope.itemsList.splice(index,1);
        };

        $scope.populateList = function(selectedItems){
            for (var i = 0; i < selectedItems.length; i++) {
                var medIndex = -1;
                for (var x = 0; x < $scope.allMedicines.length; x++) {
                    if ($scope.allMedicines[x].code === selectedItems[i].code) {
                        medIndex = x;
                    }
                }
                var medExists = false;
                for(var j=0;j<$scope.itemsList.length;j++){
                    if($scope.itemsList[j].code===$scope.allMedicines[medIndex].code){
                        medExists = true;
                    }
                }
                if(medExists === false){
                  $scope.itemsList.push($scope.allMedicines[medIndex]);
                }

            }
            $scope.showItemsList = false;
            getAndSetItemsExpirayStatus(0);
            getItemPricing(0);
            calculate_total();
        };

        $scope.selectSupplier = function(supplier){
            var supplierIndex = getIndex($scope.suppliers, 'description', supplier[0].description);
            $scope.selectedSupplier = $scope.suppliers[supplierIndex];
            $scope.GRN.supplierId = $scope.suppliers[supplierIndex]._id;
            $scope.GRN.supplierDescription = $scope.suppliers[supplierIndex].description;
            $scope.showSupplierList = false;
        };

        $scope.itemsSearchTypeahead = function(val){
            return storeinventory_service.search_store_by_keyword(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        $scope.selectStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.selectedStore = $scope.storeData[storeIndex];
            $scope.GRN.storeId = $scope.storeData[storeIndex]._id;
            $scope.GRN.storeDescription = $scope.storeData[storeIndex].description;
            $scope.showStoresList = false;
        };

        $scope.delete = function() {
            $scope.confirmationPopup = 'confirmation-popup-right';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            storeinventory_service.delete_StoreGRN($scope.GRN).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Goods Received Note Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Goods Received Note.',
                    dismissButton: true
                });
            });
        };

        $scope.update = function(grn_obj, form) {
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].expiry_date_enabled === true){
                    if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined || $scope.itemsList[i].rate < 1 || $scope.itemsList[i].rate===undefined){
                        $scope.itemsQty = true;
                        return false;
                    } else if ($scope.itemsList[i].expiryDate === null || $scope.itemsList[i].expiryDate === undefined ) {
                        $scope.itemsQty = true;
                        return false;
                    }
                    $scope.itemsList[i].expiryDate = dateConverter($scope.itemsList[i].expiryDate);
                }
            }

            grn_obj.itemsList = $scope.itemsList;

            if (grn_obj.date === null || grn_obj.date === undefined ) {
                grn_obj.date = new Date();
            }

            if (grn_obj.supplierChalanDate === null || grn_obj.supplierChalanDate === undefined) {
                grn_obj.supplierChalanDate = new Date();
            }

            if (grn_obj.GRNNumber !== undefined && $scope.itemsList.length !== 0) {
                grn_obj.date = dateConverter(grn_obj.date);
                grn_obj.supplierChalanDate = dateConverter(grn_obj.supplierChalanDate);
                $scope.processing = true;
                storeinventory_service.update_StoreGRN(grn_obj).then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Goods Received Note Updated Successfully',
                        dismissButton: true
                    });
                    $scope.printData = response.data;
					$scope.print(form);
					//$scope.reset(form);
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Goods Received Note.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.submit = function(grn_obj,form){
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].expiry_date_enabled === true){
                    if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined || $scope.itemsList[i].rate < 1 || $scope.itemsList[i].rate===undefined){
                        $scope.itemsQty = true;
                        return false;
                    } else if ($scope.itemsList[i].expiryDate === null || $scope.itemsList[i].expiryDate === undefined ) {
                        $scope.itemsQty = true;
                        return false;
                    }
                    $scope.itemsList[i].expiryDate = dateConverter($scope.itemsList[i].expiryDate);
                }

                storeinventory_service.update_storeinventory_grn_status($scope.itemsList[i].code);

            }

            grn_obj.itemsList = $scope.itemsList;

            if (grn_obj.date === null || grn_obj.date === undefined) {
                grn_obj.date = new Date();
            }

            if (grn_obj.supplierChalanDate === null || grn_obj.supplierChalanDate === undefined) {
                grn_obj.supplierChalanDate = new Date();
            }

            if(grn_obj.GRNNumber!==undefined && $scope.itemsList.length!==0){
                grn_obj.date = dateConverter(grn_obj.date);
                grn_obj.supplierChalanDate = dateConverter(grn_obj.supplierChalanDate);
				grn_obj.purchaseOrderNumber = $scope.purchaseOrderNumber;
				grn_obj.purchaseOrderDate = $scope.purchaseOrderDate;
                $scope.processing = true;
                storeinventory_service.create_StoreGRN(grn_obj).then(function(response){
                    ngToast.create({
                        className: 'success',
                        content: 'Goods Received Note Created Successfully',
                        dismissButton: true
                    });
                    grn_obj.GRNNumber = response.GRNNumber;
					$scope.printData = response.data;
					$scope.print(form);
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: '+error.data.message,
                        dismissButton: true
                    });
                });
            }
        };

        $scope.reset = function(form){
            if (form) {
                form.$setPristine();
                form.$setUntouched();
                form.submitted = false;
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.GRN = {
                'GRNNumber' : '',
                'type': 'Direct',
                'transcationCategory' : 'Default',
				'date' : new Date(),
                'supplierChalanDate' : new Date()
            };
            $scope.itemsList = [];
            $scope.totalAmount = 0;
            $scope.showItemsList = false;
            $scope.selectedSupplier = undefined;
            $scope.selectedStore = undefined;
            $scope.showPurchaseOrders = false;
            $scope.disableSubmit = false;
            $scope.itemsQty = false;
            $scope.UpdateItem = false;
            $scope.processing = false;

            getTransactionCategories();
            getPurchaseOrders();
            //getAllItems();
            getSuppliers();
            getStores();
        };

        $scope.openPurchaseOrders = function() {
            $scope.showPurchaseOrders = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixPO + $scope.prefixPO));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openSupplierList = function() {
            $scope.showSupplierList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixSuppliers + $scope.prefixSuppliers));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openStoreList = function() {
            $scope.showStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixStore + $scope.prefixStore));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openItemsList = function() {
            $scope.showItemsList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.reset();
    }
]);
