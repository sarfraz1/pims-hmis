'use strict';

angular.module('storeinventories').controller('StoreInventoryDetailsController', ['$scope', '$stateParams', '$location', 'Authentication', 'storeinventory_service', 'ngToast', '$anchorScroll', '$timeout',
	function($scope, $stateParams, $location, Authentication, storeinventory_service, ngToast, $anchorScroll, $timeout) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) 
        	$location.path('/signin');

        $scope.headingsInventory = [];
        $scope.headingsInventory.push({'alias': 'Code', 'name': 'code', 'width': 45});
        $scope.headingsInventory.push({'alias': 'Description', 'name': 'description', 'width': 48});
        $scope.inventoryToShow = [];
        $scope.showTable = false;
        $scope.showGrid = false;

        var pushItems = function(index) {
            $scope.inventoryToShow.push({'code': $scope.samplejson[index].code, 'description': $scope.samplejson[index].description});
        };

        $scope.loadMoreItems = function() {
          var length = $scope.inventoryToShow.length + 100;
          if (length < 1000) {
            for (var i = length - 100; i < length; i++) {
              if ($scope.samplejson[i] != undefined) {
                pushItems(i);
              }
            }
          }
        };

        $scope.searchMedicines = function(item) {
          $scope.inventoryToShow = [];
          for (var i = 0; i < $scope.samplejson.length; i++) {
            if ($scope.inventoryToShow.length > 100) {
              break;
            }
            if (item.length === 0) {
              pushItems(i);
            } else {
              for (var key in $scope.samplejson[i]) {
                for (var j = 0; j < $scope.headingsInventory.length; j++) {
                  if (key === $scope.headingsInventory[j].name) {
                    if ($scope.samplejson[i][key].toLowerCase().includes(item.toLowerCase())) {
                      pushItems(i);
                    }
                  }
                }
              }
            }
          }
        };

        var handleResponse = function(response) {
			$scope.samplejson = [];
            $scope.samplejson = response;
            $scope.headingsInventory = [];
            $scope.headingsInventory.push({'alias': 'Code', 'name': 'code', 'width': 45});
            $scope.headingsInventory.push({'alias': 'Description', 'name': 'description', 'width': 48});
            $scope.inventoryToShow = [];
            var max;
            if($scope.samplejson.length>100){
                max=100;
            }else 
                max = $scope.samplejson.length;
            for (var i = 0; i < max; i++) {
                pushItems(i);
            }
            $timeout(function() {
            	$scope.processing = false;
            	$scope.showTable = true;
            }, 3000);
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        var get_inventory = function() {
        	$scope.processing = true;
        	storeinventory_service.listAllItems().then(function (response) {
        		if (response.length > 0) {
        			handleResponse(response);
        		} else {
        			ngToast.create({
        				className: 'warning',
        				content: 'No inventory found.',
        				dismissButton: true
        			});
        			$scope.showTable = false;
        		}
        	});
        };

        get_inventory();

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.sortBy('created');

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            if (Array.isArray(selected_item)) {
                var itemIndex = getIndex($scope.samplejson, 'code', selected_item[0].code);
            } else {
                var itemIndex = getIndex($scope.samplejson, 'code', selected_item.code);
            }
            $scope.processing = true;
            storeinventory_service.get_storeinventory_details($scope.samplejson[itemIndex].code).then(function (response) {
            	$scope.processing = false;
            	$scope.item = response.data;
                $scope.showGrid = true;
            })
        };
	}
]);