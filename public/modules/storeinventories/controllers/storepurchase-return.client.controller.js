'use strict';

// Inventories controller
angular.module('storeinventories').controller('StorepurchaseReturnController', ['$scope', '$http','$stateParams', '$location', '$timeout', 'Authentication', 'storeinventory_service','ngToast','$anchorScroll','print_service',
    function($scope,$http ,$stateParams, $location, $timeout, Authentication, storeinventory_service,ngToast,$anchorScroll,print_service) {
  
        $scope.authentication = Authentication;

        if (!$scope.authentication.user) $location.path('/signin');

        // GRNs expandable picklist
        $scope.GRNData = [];
        $scope.GRNs = [];
        $scope.prefixGRN = 'grndiv';
        $scope.headingsGRN = [];
        $scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 49});
        $scope.headingsGRN.push({'alias': 'Creation Date', 'name': 'date', 'width': 49});
        $scope.expandableHeadingGRN = 'itemsList';
        $scope.expandableHeadingsGRN = [];
        $scope.expandableHeadingsGRN.push({'alias': 'Item Code', 'name': 'code', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Description', 'name': 'description', width: 20});
        $scope.expandableHeadingsGRN.push({'alias': 'Batch', 'name': 'batchID', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Date', 'name': 'date', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Expiry Date', 'name': 'expiryDate', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Expiry Enabled', 'name': 'expiry_date_enabled', width: 20});
        $scope.expandableHeadingsGRN.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Rate', 'name': 'rate', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Net Value', 'name': 'netValue', width: 20});
        
        // medicines simple picklist
        $scope.allMedicines = []; // for data retrieval
        $scope.allItems = []; // to be passed to directive
        $scope.prefix = 'meddiv';

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };
        
        var getTransactionCategories = function(){
          storeinventory_service.getStoreTransactionCategories()
            .then(function(response) {
              $scope.transcationCategories = response.data;
              $scope.transcationCategories.unshift({'purchaseType': 'Default'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Document Category List.',
                    dismissButton: true
                });
            });
        };

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        };

        var getGRN = function(){
          storeinventory_service.getAllStoreGRN()
            .then(function(response) {
              $scope.GRNData = [];
              $scope.GRNData = response.data;
              $scope.GRNs = [];
              $scope.headingsGRN = [];
              $scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 49});
              $scope.headingsGRN.push({'alias': 'Creation Date', 'name': 'date', 'width': 49});
              $scope.expandableHeadingsGRN = [];
              $scope.expandableHeadingsGRN.push({'alias': 'Item Code', 'name': 'code', width: 15});
              $scope.expandableHeadingsGRN.push({'alias': 'Description', 'name': 'description', width: 25});
              $scope.expandableHeadingsGRN.push({'alias': 'Batch', 'name': 'batchID', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Expiry Date', 'name': 'expiryDate', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Expiry Enabled', 'name': 'expiry_date_enabled', width: 20});
              $scope.expandableHeadingsGRN.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Rate', 'name': 'rate', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Net Value', 'name': 'netValue', width: 20});
              for (var i = 0; i < response.data.length; i++) {
                var creationDate = getDateFormat(response.data[i].date);
                $scope.GRNs.push({'GRNNumber': response.data[i].GRNNumber, 'date': creationDate, 'itemsList': []});
                for (var j = 0; j < response.data[i].itemsList.length; j++) {
                  // no check for expiry date enabled/disabled
                  var expiryDate = '';
                  if (response.data[i].itemsList[j].expiryDate === null || response.data[i].itemsList[j].expiryDate === undefined || response.data[i].itemsList[j].expiryDate === '')
                    expiryDate = 'Disabled';
                  else {
                   expiryDate = getDateFormat(response.data[i].itemsList[j].expiryDate);
                  }
                  $scope.GRNs[i].itemsList.push({'code': response.data[i].itemsList[j].code, 'description': response.data[i].itemsList[j].description, 'batchID': response.data[i].itemsList[j].batchID, 'expiryDate': expiryDate,'expiry_date_enabled': response.data[i].itemsList[j].expiry_date_enabled, 'quantity': response.data[i].itemsList[j].quantity, 'rate': response.data[i].itemsList[j].rate, 'netValue': response.data[i].itemsList[j].netValue});
                }
              }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve GRN List.',
                    dismissButton: true
                });
            });
        };

        var pushItems = function(index) {
            var formula = '';
            if ($scope.allMedicines[index].formula === '' || $scope.allMedicines[index].formula == null)
                formula = 'No Formula';
            else
                formula = $scope.allMedicines[index].formula;
            $scope.allItems.push({'code': $scope.allMedicines[index].code, 'description': $scope.allMedicines[index].description, 'formula': formula});
        };

        $scope.loadMoreItems = function() {
            var length = $scope.allItems.length + 100;
            if (length < 1000) {
                for (var i = length - 100; i < length; i++) {
                    if ($scope.allMedicines[i] != undefined) {
                        pushItems(i);
                    }
                }
            }
        };

        $scope.searchMedicines = function(item) {
            $scope.allItems = [];
            for (var i = 0; i < $scope.allMedicines.length; i++) {
                if ($scope.allItems.length > 100) {
                    break;
                }
                if (item.length === 0) {
                    pushItems(i);
                } else {
                    for (var key in $scope.allMedicines[i]) {
                        for (var j = 0; j < $scope.headingsMedicines.length; j++) {
                            if (key === $scope.headingsMedicines[j].name) {
                                if ($scope.allMedicines[i][key].toLowerCase().includes(item.toLowerCase())) {
                                    pushItems(i);
                                }
                            }
                        }
                    }
                }
            }
        };

        var getAllItems = function(){
        /*    $scope.processing = true;
            inventory_service.listAllItems()
                .then(function(response) {
                    if (response.length > 0) {
                        $scope.showItemButton = true;
                        $scope.allItems = [];
                        $scope.headingsMedicines = [];
                        $scope.headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
                        $scope.headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
                        $scope.allMedicines = response;
                        var max;
                        if($scope.allMedicines.length>100){
                            max = 100;
                        }else 
                            max = $scope.allMedicines.length;


                        for (var i = 0; i < max; i++) {
                            pushItems(i);
                        }
                        $scope.processing = false;
                    } else {
                        $scope.showItemButton = false;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
                $scope.processing = false;
            });*/
        };

        var calculate_total = function(){
          var totalAmount = 0;
          
          for(var i=0;i<$scope.itemsList.length;i++){
            var totalTax = 0;
            var totalDiscount = 0;
            if($scope.itemsList[i].discountType === 'PerItem'){
              totalDiscount = $scope.itemsList[i].quantity*$scope.itemsList[i].discount;
            }
            else if($scope.itemsList[i].discountType === 'perc'){
              totalDiscount = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].discount/100);
            }
            else if($scope.itemsList[i].discountType === 'lumsum'){
              totalDiscount = $scope.itemsList[i].discount;
            }

            if($scope.itemsList[i].taxType === 'perc') {
              totalTax = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].saleTax/100);
            }
            else if($scope.itemsList[i].taxType === 'lumsum'){
              totalTax = $scope.itemsList[i].saleTax;
            }
            $scope.itemsList[i].netValue = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)+totalTax-totalDiscount;
            if(isNaN($scope.itemsList[i].netValue)){
              $scope.itemsList[i].netValue = 0;
            }
            totalAmount = totalAmount+$scope.itemsList[i].netValue;
          }
          return totalAmount;
        };

        $scope.calculate_total_net_amount = function(){
          $scope.totalAmount = calculate_total();
        };

        $scope.selectedItems = function(obj){
            

            for (var j = 0; j < obj.length; j++) {
                var objIndex = -1;
                for (var x = 0; x < $scope.GRNData.length; x++) {
                    if ($scope.GRNData[x].GRNNumber === obj[j].GRNNumber) {
                        objIndex = x;
                    }
                }
                var objtemp = $scope.GRNData[objIndex];
                var subobjIndex = -1;
                for (var y = 0; y < objtemp.itemsList.length; y++) {
                    if (objtemp.itemsList[y].code === obj[j].code) {
                        subobjIndex = y;
                    }
                }
                var subobjtemp = objtemp.itemsList[subobjIndex];
                var dateObj;
                if(subobjtemp.expiryDate!==undefined)
                  dateObj = new Date(subobjtemp.expiryDate);
                else 
                  dateObj = undefined;
                var temp = {
                  'code' : subobjtemp.code,
                  'GRNNumber' : objtemp.GRNNumber,
                  'date' : objtemp.date,
                  'expiryDate' : dateObj,
                  'description' : subobjtemp.description,
                  'rate' : subobjtemp.rate,
                  'quantity' : subobjtemp.quantity,
                  'expiry_date_enabled' : subobjtemp.expiry_date_enabled
                };
                var medExists = false;
                for (var i = 0; i < $scope.itemsList.length; i++) {
                    if ($scope.itemsList[i].code === temp.code && $scope.itemsList[i].GRNNumber === temp.GRNNumber){
                        $scope.itemsList[i] = temp;
                        medExists = true;
                    }
                }
                if (medExists === false){
                    $scope.itemsList.push(temp);
                    $scope.totalAmount = calculate_total();
                }
            }
            $scope.showGRNs = false;
        };

        $scope.populateList = function(selectedItems){
            for (var i = 0; i < selectedItems.length; i++) {
              var medExists = false;
                for (var j = 0;j < $scope.itemsList.length; j++){
                    if($scope.itemsList[j].code === selectedItems[i].code) {
                        medExists = true;
                    }
                }
                if(medExists === false){
                  var medIndex = getIndex($scope.allMedicines, 'code', selectedItems[i].code);
                  $scope.itemsList.push($scope.allMedicines[medIndex]);
                }
            }
            for (var i = $scope.allMedicines.length - 1; i >= 0; i--) {
              $scope.allMedicines[i].discountType = 'perc';
              $scope.allMedicines[i].discount = 0;
              $scope.allMedicines[i].taxType = 'perc';
              $scope.allMedicines[i].saleTax = 0;
              $scope.allMedicines[i].netValue = undefined;
            }
            $scope.showAllItems = false;
        };

        $scope.add_medicine = function(med_obj) {
          var alreadyExists = false;
          for (var i = 0; i < $scope.itemsList.length; i++) {
            if ($scope.itemsList[i].code === med_obj.code) {
              $scope.itemsList[i].quantity++;
              alreadyExists = true;
            }
          }
          if (!alreadyExists)
            $scope.itemsList.push(med_obj);

          $scope.totalAmount = calculate_total();
          $scope.show_dropdown = false;
          $scope.search_keyword = '';
        };

        $scope.itemsSearchTypeahead = function(val){
            return storeinventory_service.search_by_keyword(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        $scope.selectedSearchItem = function(obj){
            obj.discountType = 'perc';
            obj.discount = 0;
            obj.taxType = 'perc';
            obj.saleTax = 0;
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===obj.code){
                    medExists = true;
                }
            }
            if(medExists === false)
              $scope.itemsList.push(obj);
            $scope.searchKeyword = '';
        };

        $scope.remove_medicine = function(index) {
          $scope.itemsList.splice(index,1); 
        };

        $scope.reset = function(form){
        
        $scope.btnType = "Save";
        $scope.btnDelshow = false;

          if (form) {
            form.$setPristine();
            form.$setUntouched();
            form.submitted = false;
          }
          $location.hash('headerid');
          $anchorScroll.yOffset = 100;
          $anchorScroll();
          
          $scope.purchaseOrder = {
            'purchaseReturnNumber' : '',
            'request' : 'Direct',
            'date' : new Date(),
            'transcationCategory' : 'Default'
          };

          $scope.processing = false;
          $scope.totalAmount = 0;
          $scope.itemsList = [];
          $scope.showGRNs = false;
          $scope.showAllItems = false;
          $scope.itemsQty = false;
          
          getTransactionCategories();
          //getGRN();
          //getAllItems();
        };

        $scope.submit_purchase_order = function(purchaseOrder,form){
          
          purchaseOrder.itemsList = $scope.itemsList;
          purchaseOrder.date = dateConverter(purchaseOrder.date);
          for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined || $scope.itemsList[i].rate < 1 || $scope.itemsList[i].rate===undefined){
                    $scope.itemsQty = true;
                    return false;
                }
                $scope.itemsList[i].expiryDate = dateConverter($scope.itemsList[i].expiryDate);
                 
            }

          if(purchaseOrder.request!==undefined && purchaseOrder.date!==undefined && $scope.itemsList.length!==0){
            $scope.processing = true;
            storeinventory_service.create_storepurchase_return(purchaseOrder).then(function(response){
              ngToast.create({
                className: 'success',
                content: 'Purchase Return Created Successfully',
                dismissButton: true
              });
              
              $scope.reset(form);
              
            }).catch(function (error) {
              $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+error.data.message,
                    dismissButton: true
                });
            });
          }
        };

        $scope.getItems = function(){
          storeinventory_service.search_store_by_keyword($scope.searchKeyword)
            .then(function(response) {
              $scope.showGrid = true;
              $scope.samplejson = [];
              $scope.samplejson = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };
        
        ///////////handle Submit Request///////////
         $scope.handle_Submit_Request = function(purchaseOrder,form){
             
             if( $scope.btnType== "Save"){
                $scope.submit_purchase_order(purchaseOrder,form)
                 
             }
             else{
                 $scope.UpdatePurchaseReturnObj = {};
                 $scope.UpdatePurchaseReturnObj.purchaseReturnNumber= $scope.purchaseReturnNumber;
                 $scope.UpdatePurchaseReturnObj.itemsList = $scope.itemsList;
                 
                 $scope.Update_purchase_return($scope.UpdatePurchaseReturnObj);
            }


         };
        
        /////////Update Purchase Return///////////
         $scope.Update_purchase_return =function(Obj){
             
             
              storeinventory_service.UpdateStorePurchaseReturn(Obj)
                .then(function(response) {
                       console.log(response.data);
                       $scope.reset();
                
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to Update Purchase Return.',
                        dismissButton: true
                    });
                });

               
         };
        ///////////////////////////////////////////
        
        ////////Search by Purchase Return//////////
        $scope.btnType= "Save";
        $scope.searchPurchaseReturnKeyword ="";
         $scope.btnDelshow = false;

        $scope.get_purchase_return =function(){
            
            if($scope.searchPurchaseReturnKeyword == "")
            {
                 $scope.get_Purchase_Return_PickList();
            }
            else
            {
                $scope.searched_purchase_return($scope.searchPurchaseReturnKeyword);
                $scope.purchaseReturnNumber = $scope.searchPurchaseReturnKeyword;
            }
            
            
             $scope.searchPurchaseReturnKeyword ="";
        };

         $scope.searched_purchase_return =function(id){
             
              storeinventory_service.getSearchedStorePurchaseReturn(id)
                .then(function(response) {
                
                $scope.itemsList = response.data[0].itemsList;
                $scope.btnType= "Update";
                $scope.btnDelshow = true;
                
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Purchase Return.',
                        dismissButton: true
                    });
                });


         };
        
        //////////////////////////////////////////
        /////////Purchase Return PickList/////////
        $scope.showAllPurchaseReturns = false;
        $scope.headingsPurchaseReturns ="";
        $scope.allPurchaseReturns ="";
        
        
        $scope.get_Purchase_Return_PickList = function(){
                $scope.processing = true;
                $scope.showAllPurchaseReturns = true;
                
                storeinventory_service.getStorePurchaseReturn()
                    .then(function(response) {
                       
                            
                            $scope.allPurchaseReturns =[];
                          
                            $scope.headingsPurchaseReturns =[];
                            
                           
                            $scope.headingsPurchaseReturns.push({'alias': 'Purchase Return No', 'name': 'purchaseReturnNumber', 'width': 33});
                            $scope.headingsPurchaseReturns.push({'alias': 'Date', 'name': 'date', 'width': 33});
                            $scope.headingsPurchaseReturns.push({'alias': 'Category', 'name': 'transcationCategory', 'width': 33});
                            
                              
                            for(var i =0 ; i< response.data.length ; i++)
                            {
                                $scope.allPurchaseReturns.push({
                                    'purchaseReturnNumber' : response.data[i].purchaseReturnNumber,
                                                    'date' : moment(response.data[i].date).format('DD/MM/YYYY'),
                                     'transcationCategory' : response.data[i].transcationCategory
                                               //'itemsList' : response.data[i].itemsList
                                 });

                            }
                           // console.log($scope.allPurchaseReturns);
                           
                            
                            $scope.processing = false;
                       
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Purchase Return.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            };

             $scope.Purchase_Return_CallBack = function(selItem){
                 
                 $scope.purchaseReturnNumber = selItem[0].purchaseReturnNumber;
                 
                 
                 $scope.searched_purchase_return(selItem[0].purchaseReturnNumber);
                 $scope.btnType= "Update";
                 $scope.btnDelshow = true;
                 $scope.showAllPurchaseReturns = false;

            };


        //////////////////////////////////////////
        ////////////Delete Purchase Return////////
        $scope.del_purchase_return =function(obj){
             
              storeinventory_service.DeleteStorePurchaseReturn($scope.itemsList)
                .then(function(response) {
                    
                    console.log(response.data);
                
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to Delete Purchase Return.',
                        dismissButton: true
                    });
                });


         };
        
        //////////////////////////////////////////
        $scope.reset();
        

        $scope.openItemsList = function() {
            $scope.showAllItems = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openGRNs = function() {
            $scope.showGRNs = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixGRN + $scope.prefixGRN));
            $timeout(function() {setFocus[0].focus()});
        };
      }
]);




