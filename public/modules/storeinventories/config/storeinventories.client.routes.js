'use strict';

//Setting up route
angular.module('storeinventories').config(['$stateProvider',
	function($stateProvider) {
		// Inventories state routing
		$stateProvider.
		state('storeinventory-details', {
			url: '/storeinventory-details',
			templateUrl: 'modules/storeinventories/views/storeinventory-details.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-stock', {
			url: '/storeinventory-stock',
			templateUrl: 'modules/storeinventories/views/storeinventory-stock.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-customer-supplier', {
			url: '/storeinventory-customer-supplier',
			templateUrl: 'modules/storeinventories/views/storeinventory-customer-supplier.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-transaction-category', {
			url: '/storeinventory-transaction-category',
			templateUrl: 'modules/storeinventories/views/storeinventory-transaction-category.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-store', {
			url: '/storeinventory-store',
			templateUrl: 'modules/storeinventories/views/storeinventory-store.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-label', {
			url: '/storeinventory-label',
			templateUrl: 'modules/storeinventories/views/storeinventory-label.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-unit', {
			url: '/storeinventory-unit',
			templateUrl: 'modules/storeinventories/views/storeinventory-unit.client.view.html',
			access: ['No one']
		}).
		state('storeinventory-pricing', {
			url: '/inventory-pricing',
			templateUrl: 'modules/inventories/views/storeinventory-pricing.client.view.html',
			access: ['No one']
		}).
		state('storecreateInventory', {
			url: '/storeinventories/create',
			templateUrl: 'modules/storeinventories/views/create-storeinventory.client.view.html',
			access: ['No one']
		}).
		state('storepurchaseRequisition', {
			url: '/storepurchaseRequisition',
			templateUrl: 'modules/storeinventories/views/storepurchase-requisition.client.view.html',
			access: ['No one']
		}).
		state('storepurchaseOrder', {
			url: '/storepurchaseOrder',
			templateUrl: 'modules/storeinventories/views/storepurchase-order.client.view.html',
			access: ['No one']
		}).
		state('storestockIssuance', {
			url: '/storestockIssuance',
			templateUrl: 'modules/storeinventories/views/storestock-issuance.client.view.html',
			access: ['No one']
		}).
		state('storestockTransfer', {
			url: '/storestockTransfer',
			templateUrl: 'modules/storeinventories/views/storestock-transfer.client.view.html',
			access: ['No one']
		}).
		state('storepurchaseReturn', {
			url: '/storepurchaseReturn',
			templateUrl: 'modules/storeinventories/views/storepurchase-return.client.view.html',
			access: ['No one']
		}).
		state('storeGRN', {
			url: '/storeGRN',
			templateUrl: 'modules/storeinventories/views/storeGRN.client.view.html',
			access: ['No one']
		}).
		state('storestockReport', {
			url: '/storestockReport',
			templateUrl: 'modules/storeinventories/views/storestock-report.client.view.html',
			access: ['No one']
		}).
		state('storelowstockReport', {
			url: '/storelowstockReport',
			templateUrl: 'modules/storeinventories/views/storestockreorder-report.client.view.html',
			access: ['No one']
		});
	}
]);
