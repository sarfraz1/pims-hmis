'use strict';

//Setting up route
angular.module('pos').config(['$stateProvider',
	function($stateProvider) {
		// Pos state routing
		$stateProvider.
		state('pos', {
			url: '/pos',
			templateUrl: 'modules/pos/views/pos.client.view.html',
			access: ['pharmacy salesman']
		});
	}
]);

angular
  .module('pos')
  .config(['ngToastProvider', function(ngToast) {
    ngToast.configure({
      horizontalPosition: 'left',
      maxNumber: 3
    });
  }]);