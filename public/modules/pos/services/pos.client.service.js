'use strict';

angular.module('pos').factory('Pos', ['$http', '$q', 'config_service',
	function($http, $q, config_service) {
		var total_length = 1;
		var i = 0;
		var searched_medicines = [];
		var item = {};
		
		var srvr_address = config_service.serverAddress;
		
		this.create_invoice = function(invoice) {
	    	return $http.post(srvr_address+'/invoices', invoice);
		};

		this.update_invoice = function(invoice) {
	    	return $http.post(srvr_address+'/invoices/update', invoice);
		};
		
		this.update_orderlist = function(invoice) {
	    	return $http.post(srvr_address+'/invoices/updateorderlist', invoice);
		};

		this.update_purchase_price = function(invoiceNum, itemList) {
	    	return $http.post(srvr_address+'/invoices/updatepurchaseprice/'+invoiceNum, {itemList: itemList});
		};

		this.list_invoices = function() {
			return true;
		};

		this.get_invoice =  function(invoiceId) {
			return $http.get(srvr_address+'/invoices/' + invoiceId);
		};

		this.delete_invoice = function(invoiceId) {
			return $http.delete(srvr_address+'/invoices/' + invoiceId);
		};

		var search_inventory_barcode = function(keyword) {
			return $http.get(srvr_address+'/getInventory/barcode/' + keyword);
		};

		var search_inventory_code = function(keyword) {
			return $http.get(srvr_address+'/getInventory/code/' + keyword);
		};

		var search_inventory_name = function(keyword) {
			return $http.get(srvr_address+'/getInventory/description/' + keyword);
		};
		
		var search_inventory_name_stock = function(keyword) {
			return $http.get(srvr_address+'/getInventoryStock/description/' + keyword);
		};

		var search_inventory_name_store_stock = function(keyword, store) {
			return $http.get(srvr_address+'/getInventorybyStoreName/' + keyword + '/' + store);
		};

		var get_item_by_description = function(keyword) {
			return $http.get(srvr_address+'/getInventory/' + keyword);
		};

		var search_inventory_pricing_by_code = function(keyword) {
			return $http.get(srvr_address+'/inventory-pricing/' + keyword);
		};
		
		var search_inventory_pricing_by_code_for_pos = function(keyword) {
			return $http.get(srvr_address+'/inventory-pricing-pos/' + keyword);
		};

		this.check_inventory_stock = function(keyword) {
			return $http.get(srvr_address+'/inventory-stock/check/' + keyword);
		};

		this.get_hospital_details = function() {
			return $http.get(srvr_address+'/hospital');
		};

		this.get_item_by_id = function(itemCode) {
			return $http.get(srvr_address+'/inventories/'+itemCode);
		};

		this.search_formula = function(keyword) {
			return $http.get(srvr_address+'/medicines/formula/' + keyword);
		};

		this.adjust_inventory_stock = function(itemList, store) {
			var deferred = $q.defer();
			var totalCalls = itemList.length;
			var results = [];
			var called = 0;
			if(store) {
				angular.forEach(itemList, function(item, key) {
					$http.put(srvr_address+'/inventory-store-stock/adjust/' + item.id, {quantity: item.quantity, store: store}).then(function (response) {
						results.push(response.data);
						called++;
						if (called === totalCalls) {
							deferred.resolve(results);
						}
					});
				});
			} else {
				angular.forEach(itemList, function(item, key) {
					$http.put(srvr_address+'/inventory-stock/adjust/' + item.id, {quantity: item.quantity}).then(function (response) {
						results.push(response.data);
						called++;
						if (called === totalCalls) {
							deferred.resolve(results);
						}
					});
				});
			}
			return deferred.promise;
		};

		this.adjust_return_inventory_stock = function(itemList, store) {
			var deferred = $q.defer();
			var totalCalls = itemList.length;
			var results = [];
			var called = 0;
			angular.forEach(itemList, function(item, key) {
				$http.put(srvr_address+'/inventory-stock/adjustReturn/' + item.id, {quantity: item.quantity, store: store}).then(function (response) {
					results.push(response);
					called++;
					if (called === totalCalls) {
						deferred.resolve(results);
					}
				});
			});
			return deferred.promise;
		};
		
		this.get_medicine_supplier = function(id) {
			return $http.get(srvr_address+'/med-supplier/' + id);
		};

		this.get_remaining_stock = function(id) {
			return $http.get(srvr_address+'/inventory-stock/check/' + id);
		};

		this.list_inventory_stock = function(itemList) {
			var deferred = $q.defer();
			var totalCalls = itemList.length;
			var results = [];
			var called = 0;
			angular.forEach(itemList, function(item, key) {
				$http.get(srvr_address+'/inventory-stock/check/' + item.id).then(function (response) {
					results.push(response);
					called++;
					if (called === totalCalls) {
						deferred.resolve(results);
					}
				});
			});
			return deferred.promise;
		};

		this.get_inventory_by_name = function(keyword) {
			var deferred = $q.defer();
			var searched_medicine = {};
			get_item_by_description(keyword).then(function (response) {
				if (response.data === null) {
					return deferred.reject('Could not find medicine in inventory.');
				}
				searched_medicine.id = response.data.code;
				searched_medicine.description = response.data.description;
				searched_medicine.unit = response.data.selling_unit;
				searched_medicine.label = response.data.label;
				searched_medicine.narcotics = response.data.narcotics;
				searched_medicine.price = 0;
				searched_medicine.storage_to_selling = response.data.storage_to_selling;
				search_inventory_pricing_by_code(searched_medicine.id).then(function (responsePrice) {
					searched_medicine.quantity = 1;
					if (responsePrice.data.discountType === 'percent')
						searched_medicine.price = responsePrice.data.sellingPrice - (responsePrice.data.sellingPrice * responsePrice.data.discountAmount / 100);
					else if (responsePrice.data.discountType === 'amount')
							searched_medicine.price = responsePrice.data.sellingPrice - responsePrice.data.discountAmount;
					
					// if(searched_medicine.price)
					// 	searched_medicine.price = Number(searched_medicine.price.toFixed(2));

					deferred.resolve(searched_medicine);
				}).catch(function (err) {
					return deferred.reject();
				});
			}).catch(function (err) {
				return deferred.reject();
			});
			return deferred.promise;
		};

		this.search_inventory_by_name = function(keyword) {
			var deferred = $q.defer();
			var searched_medicines = [];
			search_inventory_name(keyword).then(function (response) {
				for (var j = 0; j < response.data.length; j++) {
					searched_medicines.push({id: response.data[j].code, description: response.data[j].description, formula: response.data[j].formula, unit: response.data[j].selling_unit, label: response.data[j].label, storage_to_selling: response.data[j].storage_to_selling, narcotics: response.data[j].narcotics, remarks: response.data[j].remarks});
				}
				deferred.resolve(searched_medicines);
			}).catch(function (err) {
				return deferred.reject();
			});
			return deferred.promise;
		};
		
		this.search_inventory_by_name_stock = function (keyword, store) {
			var deferred = $q.defer();
			var searched_medicines = [];
			if (store) {
				search_inventory_name_store_stock(keyword,store).then(function (response) {
					for (var j = 0; j < response.data.length; j++) {
						searched_medicines.push({ id: response.data[j].id, description: response.data[j].description, totalstock: response.data[j].totalstock });
					}
					deferred.resolve(searched_medicines);
				}).catch(function (err) {
					return deferred.reject();
				});
				return deferred.promise;
			} else {
				search_inventory_name_stock(keyword).then(function (response) {
					for (var j = 0; j < response.data.length; j++) {
						searched_medicines.push({ id: response.data[j].id, description: response.data[j].description, totalstock: response.data[j].totalstock });
					}
					deferred.resolve(searched_medicines);
				}).catch(function (err) {
					return deferred.reject();
				});
				return deferred.promise;
			}
		};

		var remaining_stock = function(id) {
			return $http.get(srvr_address+'/inventory-stock/check/' + id);
		};

		this.get_remaining_stocks = function(itemList) {
			var deferred = $q.defer();
			var count = 0;
			var searched_medicines = [];
			for (var i = 0; i < itemList.length; i++) {
				remaining_stock(itemList[i].id).then(function (response) {
					searched_medicines.push({'id': response.data[0].id, 'remaining': response.data[0].remaining});
					count++;
					if (count === itemList.length) {
						deferred.resolve(searched_medicines);
					}
				}).catch(function (err) {
					return deferred.reject();
				});
			}
			return deferred.promise;
		};

		this.check_inventory_pricing_by_code = function(itemList) {
			var deferred = $q.defer();
			var totalPrice = 0;
			var count = 0;
			var searched_medicines = [];
			for (var i = 0; i < itemList.length; i++) {
				search_inventory_pricing_by_code(itemList[i].id).then(function (response) {
					searched_medicines.push({'id': response.data.inventoryCode, 'purchasingPrice': response.data.purchasingPrice});
					count++;
					if (count === itemList.length) {
						deferred.resolve(searched_medicines);
					}
				}).catch(function (err) {
					return deferred.reject();
				});
			}
			return deferred.promise;
		};

		this.search_inventory_by_code = function(keyword) {
			var deferred = $q.defer();
			var searched_medicine = {};
			search_inventory_pricing_by_code_for_pos(keyword).then(function (responsePrice) {
				if (responsePrice.data[0].discountType === 'percent') {
                   searched_medicine.price = responsePrice.data[0].sellingPrice - (responsePrice.data[0].sellingPrice * responsePrice.data[0].discountAmount / 100);
                } else if (responsePrice.data[0].discountType === 'amount') {
                    searched_medicine.price = responsePrice.data[0].sellingPrice - responsePrice.data[0].discountAmount;
                }
				searched_medicine.quantity = 1;

				searched_medicine.formula = responsePrice.data[0].aggre[0].formula;
				searched_medicine.interactions = responsePrice.data[0].aggre[0].interactions || [];
				searched_medicine.unit = responsePrice.data[0].aggre[0].selling_unit;
				searched_medicine.label = responsePrice.data[0].aggre[0].label;
				searched_medicine.storage_to_selling = responsePrice.data[0].aggre[0].storage_to_selling;
				searched_medicine.narcotics = responsePrice.data[0].aggre[0].narcotics;
				searched_medicine.remarks = responsePrice.data[0].aggre[0].remarks;
				// if(searched_medicine.price)
				// 	searched_medicine.price = Number(searched_medicine.price.toFixed(2));

				deferred.resolve(searched_medicine);
			});
			return deferred.promise;
		};

		this.search_inventory_by_barcode = function(keyword) {
			var deferred = $q.defer();
			var searched_medicine = {};
			search_inventory_barcode(keyword).then(function (response) {
				searched_medicine.id = response.data.code;
				searched_medicine.description = response.data.description;
				searched_medicine.label = response.data.label;
				searched_medicine.narcotics = response.data.narcotics;
				searched_medicine.unit = response.data.selling_unit;
				searched_medicine.storage_to_selling = response.data.storage_to_selling;
				search_inventory_pricing_by_code(response.data.code).then(function (responsePrice) {
					
					if (responsePrice.data.discountType === 'percent'){
	                   searched_medicine.price = responsePrice.data.sellingPrice - (responsePrice.data.sellingPrice * responsePrice.data.discountAmount/100);
	                } else if (responsePrice.data.discountType==='amount') {
	                    searched_medicine.price = responsePrice.data.sellingPrice - responsePrice.data.discountAmount;
	                }
					
					// if(searched_medicine.price)
					// 	searched_medicine.price = Number(searched_medicine.price.toFixed(2));

					searched_medicine.quantity = 1;
					deferred.resolve(searched_medicine);
				});
			});
			return deferred.promise;
		};

		this.get_prescriptions = function() {
			return $http.get(srvr_address+'/prescriptionsForPharmacy');
		};

		this.get_prescriptions_mrnumber = function(mr_number) {
			return $http.get(srvr_address+'/patientPrescriptions/'+mr_number);
		};
		
		this.update_prescription = function(prescription) {
			return $http.put(srvr_address+'/prescription/pharmacy/' + prescription._id)
		};

		return this;
	}
]);