'use strict';

angular.module('pos').config(function ($locationProvider) {
    $locationProvider.hashPrefix('!');
});

angular.module('pos').controller('PosController', ['$scope', '$http', '$location', '$timeout', '$window', 'Authentication', 'ngToast', 'Pos', 'opd_service', 'print_service', 'hospitalAdmin_service', function($scope, $http, $location, $timeout, $window, Authentication, ngToast, Pos, opd_service, print_service, hospitalAdmin_service) {

    $scope.authentication = Authentication;
    if (!$scope.authentication.user)
        $location.path('/signin');
    else
        if ($scope.authentication.user.roles === 'admin')
            $location.path('/signin');

var userstore = $scope.authentication.user.store;
console.log($scope.authentication.user.store);
	$scope.showdaysintable = true;
    $scope.processType = "Order";
	$scope.showPatients = false;
    $scope.returnMedBtn = 0;
    $scope.reqMedBtn = 1;
    $scope.in_patient_popup = false;
    $scope.show_dropdown = false;
    $scope.show_formula_dropdown = false;
    $scope.showDiscountType = false;
    $scope.showDynamicTable = false;
    $scope.freezed_orders = [];
	$scope.labelPrint = [];
    $scope.invoiceDetails = {};
    var hospitalDetails = {};
    var getHospitalDetails = {};
    $scope.objectItems = {};
    $scope.hideSale = false;
    $scope.addDoctor = false;
    var invoiceItem = {};
    var posID = 1; // to be changed
    var posName = 'POS 1'; // to be changed
    var restored = false; // check if order was restored or not
    $scope.searched_medicines = [];
    $scope.returnMedicine = false;

	// patient simple picklist
	$scope.patientData = [];
	$scope.patients = [];
	$scope.headingsPatient = [];
	$scope.prefixPatient = 'patientdiv';
	$scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
	$scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

    // prescription functionality start
    $scope.prescriptions = [];
    var prescriptionsLength = 0;
    $scope.showPrescription = false;
    $scope.showPrescriptionAlert = false;

    // polling start
    var getPrescriptions = function() {
        Pos.get_prescriptions().then(function (response) {
            var difference = 0;
            difference = response.data.length - prescriptionsLength;
            $scope.prescriptions = response.data;
            if (difference > 0) {
                if ($scope.showPrescriptionAlert) {
                    $scope.showPrescriptionAlert = false;
                }
                if (difference === 1) {
                    $scope.addedPrescriptions = difference + ' new prescription added.'
                } else {
                    $scope.addedPrescriptions = difference + ' new prescriptions added.'
                }
                $scope.showPrescriptionAlert = true;
            }
            for (var i = 0; i < $scope.prescriptions.length; i++) {
                $scope.prescriptions[i].selected = 'no';
                if ($scope.showPrescription) {
                    if ($scope.prescriptions[i]._id === $scope.displayPrescription._id) {
                        $scope.prescriptions[i].selected = 'yes';
                    }
                }
                for (var j = 0; j < $scope.prescriptions[i].medicines.length; j++) {
                    if ($scope.prescriptions[i].medicines[j].timing === 'sos') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '0+0+0+0';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'od') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+0+0+0';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'bd') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+0+0+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'tds') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+1+0+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'qid') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+1+1+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'stat') {
                        $scope.prescriptions[i].medicines[j].timingToShow = 'Imm';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'HS') {
                        $scope.prescriptions[i].medicines[j].timingToShow = 'Before Sleep';
                    }
                }
            }
            $scope.prescriptionInterval = 10000;
            prescriptionsLength = $scope.prescriptions.length;
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve prescriptions.',
                dismissOnTimeout: false
            });
            $scope.prescriptionInterval += $scope.prescriptionInterval;
            if ($scope.prescriptionInterval > 60000) {
                $scope.prescriptionInterval = 10000;
            }
        });
    };
    $scope.prescriptionInterval = 5000;

    var interval = setInterval(getPrescriptions, $scope.prescriptionInterval);

    $scope.$on("$destroy", function(event) {clearInterval(interval)});
    // polling end

	$scope.changePassword = function() {
		$http.post('/users/password',$scope.credentials).success(function(response) {
			//console.log(response);
			$scope.credentials = {};
			$scope.changePass = false;
			ngToast.create({
				className: 'success',
				content: response.message,
				dismissButton: true
			});
		}).error(function(response) {
			ngToast.create({
				className: 'danger',
				content: response.message,
				dismissButton: true
			});
			$scope.error = response.message;
		});
	};

	$scope.gotoSalesReport = function() {
		var landingUrl = 'http://'+$window.location.host + "/#!/salesReport";
		$window.open(landingUrl);
	};

	$scope.gotoSummaryReport = function() {
		var landingUrl = 'http://'+$window.location.host + "/#!/summarySalesReport";
		$window.open(landingUrl);
	};


    var get_inventories_by_name = function(medicineList, index, doctorName) {
        if (index === medicineList.length) {
            return false;
        } else {
            var name = medicineList[index].name;
            Pos.get_inventory_by_name(name).then(function (response) {
                if (response) {
                    $scope.add_medicine(response, doctorName, medicineList[index].dosage, medicineList[index].timing);
                }
                index++;
                get_inventories_by_name(medicineList, index, doctorName);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Could not add medicine from prescription.',
                    dismissOnTimeout: false
                });
                index++;
                get_inventories_by_name(medicineList, index, doctorName);
            });
        }
    };

	// callback functions
	$scope.callbackpatient = function(selected_patient) {
		var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
		$scope.patientData[patientIndex].dob = new Date($scope.patientData[patientIndex].dob);

		$scope.patient = $scope.patientData[patientIndex];
	//	$scope.patient.age = calculateAge($scope.patient.dob);
		$scope.showPatients = false;
		$scope.showMRNumber = true;
		$scope.issalemrno = true;
		$scope.searchPatientKeyword = '';
	};

	$scope.callbackpres = function(selected_patient) {

        Pos.get_prescriptions_mrnumber(selected_patient[0].mr_number).then(function (response) {

            $scope.prescriptions = response.data;

            for (var i = 0; i < $scope.prescriptions.length; i++) {
                $scope.prescriptions[i].selected = 'no';
                if ($scope.showPrescription) {
                    if ($scope.prescriptions[i]._id === $scope.displayPrescription._id) {
                        $scope.prescriptions[i].selected = 'yes';
                    }
                }
                for (var j = 0; j < $scope.prescriptions[i].medicines.length; j++) {
                    if ($scope.prescriptions[i].medicines[j].timing === 'sos') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '0+0+0+0';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'od') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+0+0+0';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'bd') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+0+0+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'tds') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+1+0+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'qid') {
                        $scope.prescriptions[i].medicines[j].timingToShow = '1+1+1+1';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'stat') {
                        $scope.prescriptions[i].medicines[j].timingToShow = 'Imm';
                    } else if ($scope.prescriptions[i].medicines[j].timing === 'HS') {
                        $scope.prescriptions[i].medicines[j].timingToShow = 'Before Sleep';
                    }
                }
            }
            prescriptionsLength = $scope.prescriptions.length;
			clearInterval(interval);
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve prescriptions.',
                dismissOnTimeout: false
            });
		});

		$scope.showPrescriptions = false;
		$scope.searchPatientKeyword = '';
	};

    $scope.callbackPrescriptions = function(item, index) {
        if (!$scope.returnMedicine) {
            if ($scope.showPrescription) {
                $scope.cancel_order();
            }
            //var restoreIndex = _.findIndex($scope.freezed_orders, function(o) {return (o.prescriptionid === item._id); })
            var restoreIndex = undefined;
    		for(var k = 0; k < $scope.freezed_orders.length; k++) {
    			if($scope.freezed_orders[k].prescriptionid === item._id) {
    				restoreIndex = k;
    				break;
    			}
    		}
    		if (restoreIndex !== undefined) {
                $scope.restore_order($scope.freezed_orders[restoreIndex], restoreIndex);
            } else {
              //  var index = _.findIndex($scope.prescriptions, function(o) { return (o.selected === 'yes'); });
    			var index = undefined;
    			for(var k = 0; k < $scope.prescriptions.length; k++) {
    				if($scope.prescriptions[k].selected === 'yes') {
    					index = k;
    					break;
    				}
    			}
                if (index !== undefined) {
                    $scope.prescriptions[index].selected = 'no';
                }
                item.selected = 'yes';
                $scope.displayPrescription = item;
                $scope.showPrescription = true;
                get_inventories_by_name(item.medicines, 0, item.doctorDisplayName)
            }
        }
    };
    // prescription functionality end

    angular.element(document.getElementById('leftbartemplate')).addClass('ng-hide');
    angular.element(document.getElementById('headertemplate')).addClass('ng-hide');
    angular.element(document.getElementById('changepagecontent')).addClass('reduce-page-content');

    // code to detect barcode reading anywhere on
    // page without focus on barcode input field
    var $doc = angular.element(document);

    var pressed = false;
    var chars = [];
    var barcode = '';
    $doc.on('keydown', function(e) {
        pressed = true;
        if (pressed === true) {
            if (e.which >= 48 && e.which <= 57) {
                chars.push(String.fromCharCode(e.which));
            }
            barcode = chars.join('');
            $timeout(function() {
                if (barcode.length >= 8 && $scope.hideSale === false) {
                    $scope.barcode_search_keyword = barcode;
                    $scope.add_barcode_medicine();
                }
                chars = [];
                barcode = '';
                pressed = false;
            }, 1000);
        }
    });

    var KeyCodes = {
        BACKSPACE : 8,
        TABKEY : 9,
        RETURNKEY : 13,
        ESCAPE : 27,
        SPACEBAR : 32,
        LEFTARROW : 37,
        UPARROW : 38,
        RIGHTARROW : 39,
        DOWNARROW : 40,
    };

    $scope.onKeydown = function(item, $event) {
        var e = $event;
        var $target = $(e.target);
        var nextTab;

        switch (e.keyCode) {
            case KeyCodes.ESCAPE:
                $target.blur();
				$scope.name_search_keyword = '';
				$scope.show_dropdown = false;
				nextTab = 0;
                break;
            case KeyCodes.UPARROW:
				e.preventDefault();
				if(parseInt($target.attr('tabindex')) == 4)
					nextTab = -3;
                else
					nextTab = -1;
					break;
            case KeyCodes.RETURNKEY:
				e.preventDefault();
				nextTab = 1;
				break;
            case KeyCodes.DOWNARROW:
				e.preventDefault();
				if(parseInt($target.attr('tabindex')) == 1 || parseInt($target.attr('tabindex')) == 25 || parseInt($target.attr('tabindex')) == 26)
					nextTab = 3;
                else
					nextTab = 1;
                break;
        }
        if (nextTab != undefined) {
            // do this outside the current $digest cycle
            // focus the next element by tabindex
            if (parseInt($target.attr('tabindex')) + nextTab !== 0)
                $timeout(function() { $('[tabindex=' + (parseInt($target.attr('tabindex')) + nextTab) + ']').focus()});
        }
    };

    $scope.onFocus = function(item, $event) {
        // clear all other items
        angular.forEach($scope.searched_medicines, function(item) {
            item.selected = undefined;
        });

        // select this one
        item.selected = 'selected';
    };

    // functionality for time
    $scope.clock = 'Loading clock...'; // initialise the time variable
    $scope.tickInterval = 1000; //ms

    var tick = function () {
        $scope.clock = Date.now(); // get the current time
        $timeout(tick, $scope.tickInterval); // reset the timer
    };

    // Start the timer
    $timeout(tick, $scope.tickInterval);

    var calculate_total = function() {
        $scope.total = 0;
        if($scope.objectItems.orderDiscount) {
            $scope.objectItems.orderDiscount = Number($scope.objectItems.orderDiscount.toFixed(2));
			var total = 0;
			if (!$scope.showDiscountType) {
				total = $scope.subtotal - (($scope.objectItems.orderDiscount/100)*$scope.subtotal);
			} else {
				total = $scope.subtotal - $scope.objectItems.orderDiscount;
			}

			if ($scope.authentication.user.roles === 'pharmacy admin') {
				if((($scope.subtotal-total)/$scope.subtotal)*100 > 15) {
					ngToast.create({
						className: 'warning',
						content: 'Warning: Maximum 15% discount allowed.',
						dismissButton: true
					});
					$scope.objectItems.orderDiscount = 0;
				}
			} else if ($scope.authentication.user.roles === 'pharmacy salesman') {
				if((($scope.subtotal-total)/$scope.subtotal)*100 > 7) {
					$scope.objectItems.orderDiscount = 0;
					ngToast.create({
						className: 'warning',
						content: 'Warning: Maximum 7% discount allowed.',
						dismissButton: true
					});
				}
			}
		}

        if (!$scope.showDiscountType) {
            $scope.total = $scope.subtotal - (($scope.objectItems.orderDiscount/100)*$scope.subtotal);
        } else {
            $scope.total = $scope.subtotal - $scope.objectItems.orderDiscount;
        }

        $scope.total = Number($scope.total.toFixed(2));
        $scope.total = Math.round($scope.total);
    };

    var calculate_subtotal = function() {
        $scope.subtotal = 0;
        var countNarcotics = 0;
        for (var i = 0; i < $scope.objectItems.orderList.length; i++) {
            $scope.subtotal = $scope.subtotal + $scope.objectItems.orderList[i].price * $scope.objectItems.orderList[i].quantity;
            $scope.subtotal = Number($scope.subtotal.toFixed(2));
            // check for narcotics
            if ($scope.objectItems.orderList[i].label === 'Narcotics' || $scope.objectItems.orderList[i].narcotics) {
                countNarcotics ++;
                if ($scope.objectItems.doctorName === '')
                    $scope.objectItems.prescribingDoctorRequired = true;
            }
            $scope.objectItems.orderList[i].price = Number($scope.objectItems.orderList[i].price.toFixed(2));
        }
        if (countNarcotics != 0 && $scope.objectItems.doctorName === '')
            $scope.objectItems.prescribingDoctorRequired = true;
        if (countNarcotics === 0) {
            $scope.objectItems.doctorName = '';
            $scope.objectItems.prescribingDoctorRequired = false;
        }
         $scope.subtotal = Math.round($scope.subtotal);
    };

    var get_time = function() {
        var d = new Date();
        var hr = d.getHours();
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var ampm = hr < 12 ? 'AM' : 'PM';
        if (hr > 12) {
            hr = hr - 12;
        }
        return (hr + ':' + min + ' ' + ampm);
    };

    var get_date = function() {
        var d = new Date();
        return (d.toLocaleDateString());
    };

    Pos.get_hospital_details().then(function (response) {
        if (response.data.length > 0) {
            getHospitalDetails = response.data[0];
            hospitalDetails = {
                hospitalName: getHospitalDetails.name,
                hospitalAddressOne: getHospitalDetails.addressLineOne,
                hospitalAddressTwo: getHospitalDetails.addressLineTwo,
                hospitalPhone: getHospitalDetails.phone[0].number
            };
        } else {
            ngToast.create({
                className: 'warning',
                content: 'Hospital details do not exist.',
                dismissOnTimeout: false
            });
            getHospitalDetails = {
                name: 'Hospital',
                addressLineOne: 'Address Line One',
                addressLineTwo: 'Address Line Two',
                phone: [{
                    number: '0000000'
                }]
            };
        }
    }).catch(function (error) {
        ngToast.create({
            className: 'danger',
            content: 'Error: Unable to retrieve Hospital details.',
            dismissOnTimeout: false
        });
    });

    // Refresh data
    var new_order = function(isforreturn) {
		if(!isforreturn) {
			$scope.returnMedicine = false;
		}
		$scope.issalemrno = false;
        $scope.patient = undefined;
        $scope.issalecredit = undefined;
        //console.log(invoiceItem);
        Pos.create_invoice(invoiceItem).then(function (response) {
            $scope.addDoctor = false;
            $scope.invoiceDetails = {
                invoiceNumber: response.data.invoiceNumber,
                invoiceSalesman: $scope.authentication.user.displayName,
                invoiceDate: get_date(),
                invoiceTime: moment().format('hh:mm A')
            };
            $scope.orderDetails = {
                orderList: [],
                orderDiscount: 0,
                orderDiscountType: $scope.showDiscountType?'amount':'percent',
                orderCash: 0,
				remarks: '',
                doctorName: '',
                prescribingDoctorRequired: false
            };
            $scope.objectItems = {
                invoiceSalesman: $scope.invoiceDetails.invoiceSalesman,
                invoiceNumber: $scope.invoiceDetails.invoiceNumber,
                invoiceDate: $scope.invoiceDetails.invoiceDate,
                invoiceTime: $scope.invoiceDetails.invoiceTime,
                orderList: $scope.orderDetails.orderList,
                orderDiscount: $scope.orderDetails.orderDiscount,
                orderDiscountType: $scope.orderDetails.orderDiscountType,
                orderCash: $scope.orderDetails.orderCash,
				remarks: $scope.orderDetails.remarks,
                doctorName: $scope.orderDetails.doctorName,
                prescribingDoctorRequired: $scope.orderDetails.prescribingDoctorRequired

            };

			if(isforreturn == true) {
				for(var i=0;i<$scope.origOrderList.length;i++) {
				 if($scope.origOrderList[i].returned < $scope.origOrderList[i].quantity) {
					 if($scope.origOrderList[i].narcotics) {
						$scope.objectItems.orderList.push({id: $scope.origOrderList[i].id,
														description: $scope.origOrderList[i].description,
														quantity: $scope.origOrderList[i].quantity-$scope.origOrderList[i].returned,
														price: $scope.origOrderList[i].price,
														narcotics: true});

						} else {
							$scope.objectItems.orderList.push({id: $scope.origOrderList[i].id,
														description: $scope.origOrderList[i].description,
														quantity: $scope.origOrderList[i].quantity-$scope.origOrderList[i].returned,
														price: $scope.origOrderList[i].price});
						}
				 }
			 }
			}
            calculate_subtotal();
            calculate_total();
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to create new order.',
                dismissOnTimeout: false
            });
        });
    };

    $scope.logoutUser = function() {
        $http.get('/auth/signout',$scope.authentication.user).success(function(response) {
            // If successful we assign the response to the global user model
            angular.element(document.getElementById('leftbartemplate')).addClass('ng-hide');
            angular.element(document.getElementById('headertemplate')).addClass('ng-hide');
            angular.element(document.getElementById('changepagecontent')).addClass('reduce-page-content');

            $scope.authentication.user = undefined;
            $location.path('/signin');

        }).error(function(response) {
            $scope.error = response.message;
        });
    };

    new_order(false);

    $scope.return_medicine = function() {
        $scope.returnMedicine = !$scope.returnMedicine;
         $scope.reqMedBtn = !$scope.reqMedBtn;
         if($scope.processType == "Order")
         {
             $scope.processType = "Return";
			 new_order(true);
			 //populate order with the amount of items allowed to be returned
			// $scope.objectItems.orderList = [];


         }
         else
         {
             $scope.processType = "Order";
			 cancel_order();
         }

    };

    $scope.add_search_medicine = function(med_obj) {
		$scope.name_search_keyword = '';
        Pos.search_inventory_by_code(med_obj.id).then(function (response) {
            med_obj.price = response.price;
            med_obj.quantity = response.quantity;
			med_obj.formula = response.formula;
			med_obj.unit = response.unit;
			med_obj.label = response.label;
			med_obj.interactions = response.interactions;
			med_obj.storage_to_selling = response.storage_to_selling;
			med_obj.narcotics = response.narcotics;
			med_obj.remarks = response.remarks;
            if ((med_obj.label === 'Narcotics' || med_obj.narcotics) && $scope.objectItems.doctorName === '') {
                $scope.objectItems.prescribingDoctorRequired = true;
                ngToast.create({
                    className: 'warning',
                    content: med_obj.description + ' should not be sold without prescription.',
                    dismissOnTimeout: false
                });
            }
            var available = true;
            if (!med_obj.price) {
                available = false;
                $scope.addDoctor = false;
                ngToast.create({
                    className: 'danger',
                    content: med_obj.description + ' is out of stock.',
                    dismissOnTimeout: false
                });
                $scope.focusNameSearch();
                $scope.show_dropdown = false;
                $scope.barcode_search_keyword = '';
            }
            if (available) {
                var alreadyExists = false;
                for (var i = 0; i < $scope.orderDetails.orderList.length; i++) {
                    if ($scope.orderDetails.orderList[i].id === med_obj.id) {
                        $scope.orderDetails.orderList[i].quantity++;
                        alreadyExists = true;
                    }

					for(var j=0;j<med_obj.interactions.length;j++) {
						if(med_obj.interactions[j].name && $scope.orderDetails.orderList[i].formula.indexOf(med_obj.interactions[j].name) >=0) {
							ngToast.create({
								className: 'danger',
								content: med_obj.description+' interacts with '+$scope.orderDetails.orderList[i].description,
								dismissButton: true
							});
						}
					}
                  //  $scope.orderDetails.orderList[i].price = Math.round($scope.orderDetails.orderList[i].price);
                }
                if (!alreadyExists) {
					Pos.get_medicine_supplier(med_obj.id).then(function (response) {
						med_obj.supplier = response.data;
					});
                    Pos.check_inventory_stock(med_obj.id).then(function (response) {
                        if (response.data[0].remaining < 100 && response.data[0].remaining > 0) {
                            ngToast.create({
                                className: 'warning',
                                content: response.data[0].remaining + ' left in ' + med_obj.description + ' stock.',
                                dismissOnTimeout: false
                            });
                            $scope.orderDetails.orderList.push(med_obj);

                        } else if (!$scope.returnMedicine && response.data[0].remaining === 0) {
                            $scope.addDoctor = false;
                            ngToast.create({
                                className: 'danger',
                                content: med_obj.description + ' is out of stock.',
                                dismissOnTimeout: false
                            });
                            $scope.focusNameSearch();
                        } else {
                            med_obj.quantity = 1;
                            $scope.orderDetails.orderList.push(med_obj);

                        }
                        calculate_subtotal();
                        calculate_total();
                    }).catch(function (error) {
                        $scope.addDoctor = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve Inventory Stock details.',
                            dismissOnTimeout: false
                        });
                    });
                }
                calculate_subtotal();
                calculate_total();
                $scope.show_dropdown = false;
                $scope.barcode_search_keyword = '';
            }
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve inventory item details.',
                dismissOnTimeout: false
            });
        });
    };

    $scope.add_medicine = function(med_obj, doctorName, dosage, timing) {
        $scope.name_search_keyword = '';
		med_obj.dosage = dosage;
		med_obj.timing = timing;
        if ((med_obj.label === 'Narcotics' || med_obj.narcotics) && $scope.objectItems.doctorName === '') {
            if (doctorName === '' || doctorName === undefined || doctorName === null) {
                $scope.objectItems.prescribingDoctorRequired = true;
                ngToast.create({
                    className: 'warning',
                    content: med_obj.description + ' should not be sold without prescription.',
                    dismissOnTimeout: false
                });
            } else {
                $scope.objectItems.doctorName = doctorName;
            }
        }
        var available = true;
        if (!med_obj.price) {
            available = false;
            ngToast.create({
                className: 'danger',
                content: med_obj.description + ' is out of stock.',
                dismissOnTimeout: false
            });
            $scope.show_dropdown = false;
            $scope.barcode_search_keyword = '';
        }
        if (available) {
            var alreadyExists = false;
            for (var i = 0; i < $scope.orderDetails.orderList.length; i++) {
                if ($scope.orderDetails.orderList[i].id === med_obj.id) {
                    $scope.orderDetails.orderList[i].quantity++;
                    alreadyExists = true;
                }
                // $scope.orderDetails.orderList[i].price = Math.round($scope.orderDetails.orderList[i].price);
            }
            if (!alreadyExists) {
                Pos.check_inventory_stock(med_obj.id).then(function (response) {
                    if (response.data[0].remaining < 100 && response.data[0].remaining > 0) {
                        ngToast.create({
                            className: 'warning',
                            content: response.data[0].remaining + ' left in ' + med_obj.description + ' stock.',
                            dismissOnTimeout: false
                        });
                        $scope.orderDetails.orderList.push(med_obj);

                    } else if (response.data[0].remaining === 0) {
                        ngToast.create({
                            className: 'danger',
                            content: med_obj.description + ' is out of stock.',
                            dismissOnTimeout: false
                        });
                    } else {
                        $scope.orderDetails.orderList.push(med_obj);

                    }
                    calculate_subtotal();
                    calculate_total();
                }).catch(function (error) {
                    calculate_subtotal();
                    calculate_total();
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Inventory Stock details.',
                        dismissOnTimeout: false
                    });
                });
            }
            $scope.show_dropdown = false;
            $scope.barcode_search_keyword = '';
        }
    };

    $scope.hide_dropdown = function() {
        $scope.show_dropdown = false;
    };

    $scope.search_receipt = function() {
        Pos.get_invoice($scope.receipt_keyword).then(function (response) {
            $scope.objectItems = response.data;
            $scope.objectItems.invoiceDate = new Date(response.data.invoiceDate).toLocaleDateString();
			//save current invoice number;
			$scope.origInvoicenumber = response.data.invoiceNumber;
            $scope.invoiceDetails.invoiceNumber = response.data.invoiceNumber;
            $scope.invoiceDetails.invoiceTime = response.data.invoiceTime;
            $scope.orderDetails.orderList = response.data.orderList;
			$scope.origOrderList = response.data.orderList;
            $scope.orderDetails.orderDiscount = response.data.orderDiscount;
            $scope.orderDetails.orderDiscountType = response.data.orderDiscountType;
            $scope.orderDetails.orderCash = response.data.orderCash;
			$scope.orderDetails.remarks = response.data.remarks;
			$scope.ifsearchedreturn = response.data.isReturn;
            $scope.returnMedBtn = true;
            $scope.issalecredit = response.data.issalecredit;
            calculate_subtotal();
            calculate_total();
            $scope.receipt_keyword = '';
            restored = true;
			$scope.restored = true;
            //$scope.hideSale = true;
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Receipt not found.',
                dismissOnTimeout: false
            });
        });
    };

    $scope.add_barcode_medicine = function() {
        Pos.search_inventory_by_barcode($scope.barcode_search_keyword).then(function (response) {
            $scope.add_medicine(response);
            $scope.barcode_search_keyword = '';
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Inventory details.',
                dismissOnTimeout: false
            });
        });
    };

    // med_obj contains description, inStock bool, formula
    // need to search it to get inventory code and pricing
    $scope.add_formula_medicine = function(med_obj) {
        Pos.get_inventory_by_name(med_obj.description).then(function (response) {
            //for (var key in response) {
            //    if (med_obj.description === response[key].description) {
                    $scope.add_medicine(response);
                    $scope.formula_search_keyword = '';
            //    }
            //}
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Inventory details.',
                dismissOnTimeout: false
            });
        });
        $scope.show_formula_dropdown = false;
        $scope.formula_search_keyword = '';
    };

    var restore_order_object = function(orderObject) {
        if (orderObject.prescriptionid) {
            var prescriptionIndex = getIndex($scope.prescriptions, '_id', orderObject.prescriptionid);
            if (prescriptionIndex !== -1) {
                $scope.displayPrescription = $scope.prescriptions[prescriptionIndex];
                $scope.displayPrescription.selected = 'yes';
                $scope.showPrescription = true;
            }
        }
        $scope.invoiceDetails = {
            invoiceNumber: $scope.invoiceDetails.invoiceNumber,
            invoiceSalesman: orderObject.invoiceSalesman,
            invoiceDate: orderObject.invoiceDate,
            invoiceTime: orderObject.invoiceTime
        };
        hospitalDetails = {
            hospitalName: getHospitalDetails.name,
            hospitalAddressOne: getHospitalDetails.addressLineOne,
            hospitalAddressTwo: getHospitalDetails.addressLineTwo,
            hospitalPhone: getHospitalDetails.phone[0].number
        };
        $scope.orderDetails = {
            orderList: orderObject.orderList,
            orderDiscount: orderObject.orderDiscount,
            orderDiscountType: orderObject.orderDiscountType,
            orderCash: orderObject.orderCash,
			remarks: orderObject.remarks,
            doctorName: orderObject.doctorName,
			patient_name: orderObject.patient_name,
            prescribingDoctorRequired: orderObject.prescribingDoctorRequired
        };
        $scope.objectItems = {
            hospitalName: getHospitalDetails.name,
            hospitalAddressOne: getHospitalDetails.addressLineOne,
            hospitalAddressTwo: getHospitalDetails.addressLineTwo,
            hospitalPhone: getHospitalDetails.phone[0].number,
            invoiceSalesman: $scope.invoiceDetails.invoiceSalesman,
            invoiceNumber: $scope.invoiceDetails.invoiceNumber,
            invoiceDate: $scope.invoiceDetails.invoiceDate,
            invoiceTime: $scope.invoiceDetails.invoiceTime,
            orderList: $scope.orderDetails.orderList,
            orderDiscount: $scope.orderDetails.orderDiscount,
            orderDiscountType: $scope.orderDetails.orderDiscountType,
            orderCash: $scope.orderDetails.orderCash,
			remarks: $scope.orderDetails.remarks,
            doctorName: orderObject.doctorName,
            patient_name: orderObject.patient_name,
            prescribingDoctorRequired: orderObject.prescribingDoctorRequired
        };
        Pos.get_remaining_stocks($scope.orderDetails.orderList).then(function (response) {
            for (var i = 0; i < $scope.orderDetails.orderList.length; i++) {
                if ($scope.orderDetails.orderList[i].id === response[i].id) {
                    if (response[i].remaining === 0) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Not enough ' + $scope.orderDetails.orderList[i].description + ' in stock.',
                            dismissOnTimeout: false
                        });
                        $scope.orderDetails.orderList.splice(i, 1);
                    } else if ($scope.orderDetails.orderList[i].quantity > response[i].remaining) {
                        $scope.orderDetails.orderList[i].quantity = response[i].remaining;
                        ngToast.create({
                            className: 'danger',
                            content: 'Not enough ' + $scope.orderDetails.orderList[i].description + ' in stock.',
                            dismissOnTimeout: false
                        });
                    }
                }
            }
            calculate_subtotal();
            calculate_total();
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Inventory Stock details.',
                dismissOnTimeout: false
            });
        });
    };

    $scope.freeze_order =function() {
        if ($scope.orderDetails.orderList.length !== 0) {
            update_order();
            if ($scope.showPrescription) {
                $scope.objectItems.prescriptionid = $scope.displayPrescription._id;
                $scope.displayPrescription.selected = 'no';
                $scope.showPrescription = false;
            }
            $scope.addDoctor = false;
            $scope.freezed_orders.push($scope.objectItems);
            new_order(false);
        }
    };

    $scope.restore_order = function(order_obj,index) {
        update_order();
        if ($scope.orderDetails.orderList.length !== 0) {
            $scope.freezed_orders.push($scope.objectItems);
        }
        restore_order_object(order_obj);
        $scope.freezed_orders.splice(index,1);
        calculate_subtotal();
        calculate_total();
    };

    $scope.remove_medicine = function(index) {
        $scope.objectItems.orderList[index].quantity = 1;
        $scope.objectItems.orderList.splice(index,1);
		$scope.labelPrint.splice(index, 1);
        calculate_subtotal();
        calculate_total();
    };

    var check_item_discount = function() {
        Pos.check_inventory_pricing_by_code($scope.objectItems.orderList).then(function (response) {
            var totalPrice = 0;
            for (var i = 0; i < $scope.objectItems.orderList.length; i++) {
                var itemPrice = 0;
                if ($scope.objectItems.orderList[i].id === response[i].id) {
                    itemPrice = $scope.objectItems.orderList[i].quantity * (response[i].purchasingPrice / $scope.objectItems.orderList[i].storage_to_selling);
                    totalPrice += itemPrice;
                }
            }
            if (totalPrice > $scope.total) {
                ngToast.create({
                    className: 'danger',
                    content: 'Total price should not be less than Rs.' + totalPrice + '.',
                    dismissOnTimeout: false
                });
            }
        }).catch(function(err) {
            console.log(err);
        });
    };

    $scope.qty_change = function() {
        calculate_subtotal();
        calculate_total();
        check_item_discount();
    };

    var update_order = function() {
        if ($scope.objectItems.orderDiscount === undefined || $scope.objectItems.orderDiscount === null)
            $scope.objectItems.orderDiscount = 0;
        if ($scope.objectItems.orderCash === undefined || $scope.objectItems.orderCash === null)
            $scope.objectItems.orderCash = 0;
        $scope.objectItems = {
            hospitalName: getHospitalDetails.name,
            hospitalAddressOne: getHospitalDetails.addressLineOne,
            hospitalAddressTwo: getHospitalDetails.addressLineTwo,
            hospitalPhone: getHospitalDetails.phone[0].number,
            invoiceSalesman: $scope.invoiceDetails.invoiceSalesman,
            invoiceNumber: $scope.invoiceDetails.invoiceNumber,
            invoiceDate: $scope.invoiceDetails.invoiceDate,
            invoiceTime: $scope.invoiceDetails.invoiceTime,
            orderList: $scope.objectItems.orderList,
            orderDiscount: $scope.objectItems.orderDiscount,
            orderDiscountType: $scope.objectItems.orderDiscountType,
            orderCash: $scope.objectItems.orderCash,
			remarks: $scope.objectItems.remarks,
            doctorName: $scope.objectItems.doctorName,
			patient_name: $scope.objectItems.patient_name,
            prescribingDoctorRequired: $scope.objectItems.prescribingDoctorRequired,
            issalecredit: $scope.issalecredit,
            creditreceiptnumber: $scope.objectItems.creditreceiptnumber
        };
        invoiceItem = {
            posID: posID,
            posName: posName,
            invoiceNumber: $scope.invoiceDetails.invoiceNumber,
            invoiceDate: $scope.invoiceDetails.invoiceDate,
            invoiceTime: $scope.invoiceDetails.invoiceTime,
            invoiceSalesman: $scope.invoiceDetails.invoiceSalesman,
            orderCash: $scope.objectItems.orderCash,
			remarks: $scope.objectItems.remarks,
            orderDiscountType: $scope.objectItems.orderDiscountType,
            orderDiscount: $scope.objectItems.orderDiscount,
            orderList: $scope.objectItems.orderList,
            doctorName: $scope.objectItems.doctorName,
            patient_name: $scope.objectItems.patient_name,
            issalecredit: $scope.issalecredit,
            creditreceiptnumber: $scope.objectItems.creditreceiptnumber,
            storeDesc: $scope.authentication.user.store
        };
    };

    var getIndex = function(array, prop, value) {
        var index = -1;
        for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
                index = x;
            }
        }
        return index;
    };

    $scope.print_order = function() {
        $scope.objectItems.from = 'pos';
        $scope.objectItems.hospitalName = getHospitalDetails.name;
        $scope.objectItems.hospitalAddressOne = getHospitalDetails.addressLineOne;
        $scope.objectItems.hospitalAddressTwo = getHospitalDetails.addressLineTwo;
        $scope.objectItems.hospitalPhone = getHospitalDetails.phone[0].number;
        chrome.runtime.sendMessage('chklklmchflbhdhlheeddigflianadik', $scope.objectItems);
		var printlabeldata = {};
		var pdata = [];
		for(var i=0;i<$scope.objectItems.orderList.length;i++) {
			if($scope.objectItems.orderList[i].timing && $scope.objectItems.orderList[i].dosage && $scope.objectItems.orderList[i].timing!=='none') {
				pdata.push({'date': moment().format('DD/MM/YYYY'),
							'description': $scope.objectItems.orderList[i].description,
							'formula': $scope.objectItems.orderList[i].formula,
							'remarks': $scope.objectItems.orderList[i].remarks,
							'dose': getdosageString($scope.objectItems.orderList[i].timing,$scope.objectItems.orderList[i].dosage)});
			}
		}
		printlabeldata.pdata = pdata;
		if(pdata.length > 0) {
			print_service.print('/modules/pos/views/pos-label-print.client.view.html',printlabeldata,
				function(){
					$scope.labelPrint = [];
				});
		}


        new_order(false);
        invoiceItem = null;
		restored = false;
		$scope.restored = false;
		$scope.returnMedBtn = 0;
        $scope.showPrescription = false;
        $scope.hideSale = false;
    };

    $scope.printLabel = function() {
        var printlabeldata = {};
		var pdata = [];
		for(var i=0;i<$scope.objectItems.orderList.length;i++) {
			if($scope.objectItems.orderList[i].timing && $scope.objectItems.orderList[i].dosage && $scope.objectItems.orderList[i].timing!=='none') {
				pdata.push({'date': moment().format('DD/MM/YYYY'),
							'description': $scope.objectItems.orderList[i].description,
							'formula': $scope.objectItems.orderList[i].formula,
							'remarks': $scope.objectItems.orderList[i].remarks,
							'dose': $scope.objectItems.orderList[i].labelStr});
			}
		}
		printlabeldata.pdata = pdata;
		if(pdata.length > 0) {
			print_service.print('/modules/pos/views/pos-label-print.client.view.html',printlabeldata,
				function(){
					$scope.labelPrint = [];
				});
		}
    }

    $scope.check_quantity = function(med) {
        //console.log(med);
        if ($scope.returnMedicine) {
            if (med.quantity < 0) {
                med.quantity = 1;
            }
        } else {
            Pos.get_remaining_stock(med.id).then(function (response) {
                //console.log(response);
                if (response.data[0].remaining < med.quantity) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Not enough ' + med.description + ' in stock.',
                        dismissOnTimeout: false
                    });
                    med.quantity = response.data[0].remaining;
                }
                $scope.qty_change();
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stock details.',
                    dismissOnTimeout: false
                });
            });
        }
    };

	var getdosageString = function(timing, dosage) {
		var str = '';
		if (timing === 'sos') {
			str = '   As required';
		} else if (timing === 'od') {
			str = '   '+dosage+'  +  0  +  0  ';
		} else if (timing === 'bd') {
			str = '   '+dosage+'  +  0  +  '+dosage;
		} else if (timing === 'tds') {
			str = '   '+dosage+'  +  '+dosage+'  +  '+dosage;
		} else if (timing === 'qid') {
			str = '   '+dosage+'  +  '+dosage+'  +  '+dosage+'  +  '+dosage;
		} else if (timing === 'stat') {
			str = '   '+dosage+ ' Immediately';
		} else if (timing === 'hs') {
			str = '   '+dosage+ ' (Before Sleep)';
		} else if (timing === 'prn') {
			str = '   '+dosage+' (As needed)';
		} else if (timing === 'q4hourly') {
			str = '    '+dosage+'  +  '+dosage+'  +  '+dosage+'  +  '+dosage+'  +  '+dosage+'  +  '+dosage;
		}

		return str;
	};

	$scope.getLabelString = function(med) {
		if(med.timing && med.dosage) {
			med.labelStr = getdosageString(med.timing, med.dosage);
		}
	};

    $scope.complete_order = function() {


        if($scope.reqMedBtn == 0)
        {
            $scope.processType = "Order";
            $scope.reqMedBtn = 1;
            $scope.returnMedBtn = 1;
        }

        var check = true;
        if ($scope.orderDetails.orderList.length !== 0) {
            update_order();
			if($scope.returnMedBtn && $scope.returnMedicine) {

				//if it is a return check that quantity being returned is valid
				for(var i=0;i<$scope.objectItems.orderList.length;i++) {
					var medIndex = getIndex($scope.origOrderList, 'description', $scope.objectItems.orderList[i].description);
					if(medIndex == -1) {
						ngToast.create({
							className: 'danger',
							content: 'Can not return medicine '+$scope.objectItems.orderList[i].description,
							dismissOnTimeout: false
						});
						return;
					} else {
						var qty_valid = $scope.origOrderList[medIndex].quantity - $scope.origOrderList[medIndex].returned;
						if($scope.objectItems.orderList[i].quantity > qty_valid) {
							ngToast.create({
								className: 'danger',
								content: $scope.objectItems.orderList[i].description + ' quantity being returned is greater then sold' ,
								dismissOnTimeout: false
							});
							return;
						} else {
							$scope.origOrderList[medIndex].returned += $scope.objectItems.orderList[i].quantity;
						}
					}
				}

					invoiceItem.isReturn = true;
					$scope.objectItems.isReturn = true;
					invoiceItem.returnAgainst = $scope.origInvoicenumber;
					invoiceItem.invoiceTime = moment().format('hh:mm A');
					$scope.objectItems.returnAgainst = $scope.origInvoicenumber;
                    $scope.savinginvoice = true;
                    invoiceItem.issalecredit = $scope.issalecredit;
					Pos.update_invoice(invoiceItem).then(function (invoice) {
						restored = false;
						$scope.restored = false;
						if($scope.issalemrno) {
							$scope.in_patient_order();
						} else {

							Pos.adjust_return_inventory_stock(invoiceItem.orderList, $scope.authentication.user.store).then(function (response) {

								$scope.savinginvoice = false;
								$scope.returnMedBtn = 0;
								$scope.hideSale = false;
								$scope.objectItems.from = 'pos';
								$scope.objectItems.invoiceNumber = invoice.data.invoiceNumber;
								$scope.objectItems.invoiceTime = invoice.data.invoiceTime;
								chrome.runtime.sendMessage('chklklmchflbhdhlheeddigflianadik', $scope.objectItems);
								ngToast.create({
									className: 'success',
									content: 'Order processed successfully.',
									dismissOnTimeout: false
								});
								var obj = {invoiceNumber : $scope.origInvoicenumber,
									orderList: $scope.origOrderList};
								Pos.update_orderlist(obj).then(function (response) {
									$scope.origInvoicenumber = undefined;
									$scope.origOrderList = undefined;
								});

								delete invoiceItem.isReturn;
								delete invoiceItem.returnAgainst;
								invoiceItem.orderList = [];

								new_order(false);
								$timeout(function() { $('[tabindex=1]').focus()});
							}).catch(function (error) {
								ngToast.create({
									className: 'danger',
									content: 'Error: Unable to process order.',
									dismissOnTimeout: false
								});
								$scope.savinginvoice = false;
							});
						}

					}).catch(function (error) {
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to complete order.',
							dismissOnTimeout: false
						});
						$scope.savinginvoice = false;
					});

			}
			else {
				for (var i = 0; i < $scope.orderDetails.orderList.length; i++) {
					if ($scope.orderDetails.orderList[i].quantity === null || $scope.orderDetails.orderList[i].quantity === undefined || $scope.orderDetails.orderList[i].quantity === 0) {
						ngToast.create({
							className: 'warning',
							content: 'Please enter quantity for ' + $scope.orderDetails.orderList[i].description + '.',
							dismissOnTimeout: false
						});
						check = false;
					}
				}
				if (check) {
					update_order();

					if($scope.returnMedicine) {
						invoiceItem.isReturn = true;
						$scope.objectItems.isReturn = true;
					}

					if($scope.issalemrno && $scope.patient.name) {
						invoiceItem.issalemrno = true;
						invoiceItem.patient_name = $scope.patient.name;
						invoiceItem.mr_number = $scope.patient.mr_number;
					}


					$scope.savinginvoice = true;
					invoiceItem.invoiceTime = moment().format('hh:mm A');
					Pos.update_invoice(invoiceItem).then(function (response) {
						if(response.data) {
						if(response.data.orderList.length>0) {
							restored = false;
							$scope.restored = false;
							$scope.objectItems.from = 'pos';

							Pos.adjust_inventory_stock(invoiceItem.orderList, $scope.authentication.user.store).then(function (res) {
								ngToast.create({
									className: 'success',
									content: 'Order processed successfully.',
									dismissOnTimeout: false
								});
                                //window.postMessage("hello", "*");
                                Pos.update_purchase_price(response.data.invoiceNumber, res).then(function(res) {

                                });

								$timeout(function() { $('[tabindex=1]').focus()});
							}).catch(function (error) {
								ngToast.create({
									className: 'danger',
									content: 'Error: Unable to process order.',
									dismissOnTimeout: false
								});
							});

						$scope.objectItems.invoiceNumber = response.data.invoiceNumber;
						$scope.objectItems.invoiceTime = response.data.invoiceTime;
						chrome.runtime.sendMessage('chklklmchflbhdhlheeddigflianadik', $scope.objectItems);
						var printlabeldata = {};
						var pdata = [];
						for(var i=0;i<invoiceItem.orderList.length;i++) {
							if(invoiceItem.orderList[i].timing && invoiceItem.orderList[i].dosage && invoiceItem.orderList[i].timing!=='none') {
								pdata.push({'date': moment().format('DD/MM/YYYY'),
											'description': invoiceItem.orderList[i].description,
											'formula': invoiceItem.orderList[i].formula,
											'remarks': invoiceItem.orderList[i].remarks,
											'dose': invoiceItem.orderList[i].labelStr});
							}
						}

						printlabeldata.pdata = pdata;
						if(pdata.length > 0) {
						print_service.print('/modules/pos/views/pos-label-print.client.view.html',printlabeldata,
							function(){
								$scope.labelPrint = [];
							});
						}

						invoiceItem = null;

							if($scope.issalemrno) {
								$scope.in_patient_order();
							} else {
								new_order(false);
							}
							$scope.hideSale = false;
							$scope.savinginvoice = false
						} else {
							$scope.savinginvoice = false
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to complete order.',
								dismissOnTimeout: false
						});
						}
					} else {
						$scope.savinginvoice = false
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to complete order.',
							dismissOnTimeout: false
						});
					}
						//$scope.savinginvoice = false;

					}).catch(function (error) {
						$scope.savinginvoice = false
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to complete order.',
							dismissOnTimeout: false
						});
					});
					/*if ($scope.returnMedicine) {
						Pos.adjust_return_inventory_stock(invoiceItem.orderList).then(function (response) {
							ngToast.create({
								className: 'success',
								content: 'Order processed successfully.',
								dismissOnTimeout: false
							});
							$timeout(function() { $('[tabindex=1]').focus()});
						}).catch(function (error) {
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to process order.',
								dismissOnTimeout: false
							});
						});
					} else {*/

					//}


					if ($scope.showPrescription) {
						Pos.update_prescription($scope.displayPrescription).then(function (response) {
							$scope.displayPrescription.selected = 'no';
							$scope.showPrescription = false;
						}).catch(function (error) {
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to update prescription.',
								dismissOnTimeout: false
							});
						});
					}
				}
			}
        }
    };

    $scope.cancel_order = function() {
        if ($scope.showPrescription) {
            //var index = _.findIndex($scope.prescriptions, function(o) { return (o.selected === 'yes'); });
            var index = undefined;
			for(var k=0;k<$scope.prescriptions.length;k++) {
				if($scope.prescriptions[k].selected === 'yes') {
					index = k;
					break;
				}
			}

			if (index !== undefined) {
                $scope.prescriptions[index].selected = 'no';
            }
        }
        if ($scope.orderDetails.orderList.length !== 0) {
            new_order(false);
        }
        $scope.returnMedicine = false;
		$scope.restored = false;
		restored = false;
		$scope.returnMedBtn = false;
		$scope.ifsearchedreturn = false;
		$scope.processType = "Order";
        $scope.name_search_keyword = '';
		$scope.patient = undefined;
		$scope.issalemrno = false;
        $scope.showPrescription = false;
        $scope.hideSale = false;
        $scope.issalecredit = undefined;
    };

    $scope.complete_in_patient_order = function() {
        new_order(false);
    };

    $scope.in_patient_order = function() {
		hospitalAdmin_service.get_facilities('Pharmacy').then(function (fac) {

                var billingDetails;

                if($scope.patient.panel_id && $scope.patient.panel_id !== 'None') {
                    opd_service.get_service_discount($scope.patient.panel_id,'Hospital-Pharmacy','Pharmacy').then(function (response) {
                        var discount;
                        if(response.data.discountType == 'amount') {
                            discount = response.data.discount;
                        } else if(response.data.discount) {
                           discount = Math.round(response.data.discount/100*$scope.total);
                        }

                        billingDetails = {

                            'patientInfo' : {
                                'mr_number' : $scope.patient.mr_number,
                                'name' : $scope.patient.name,
                                'phone' : $scope.patient.phone,
                                'panel_id': $scope.patient.panel_id
                            },
                            'servicesInfo' : [{
                                'service_id' : fac.data._id,
                                'description' : 'Pharmacy',
                                'category': 'Hospital-Pharmacy',
                                'fee' : $scope.total,
                                'discount' : discount,
                                'quantity' : 1
                            }],
                            'grandTotal': $scope.total-discount,
                            'recievedCash' : 0,
                            'discount' : discount,
                            'paymentMethod' : 'cash',
                        };

                        opd_service.save_bill(billingDetails).then(function (response) {
                            ngToast.create({
                                className: 'success',
                                content: 'Patient bill updated.',
                                dismissOnTimeout: true
                            });
                            new_order(false);
                        });

                    }).catch(function(response){
                        //errrr panel discount not found
                    });
                }
else {
                billingDetails = {

                    'patientInfo' : {
                        'mr_number' : $scope.patient.mr_number,
                        'name' : $scope.patient.name,
                        'phone' : $scope.patient.phone
                    },
                    'servicesInfo' : [{
                        'service_id' : fac.data._id,
                        'description' : 'Pharmacy',
						'category': 'Hospital-Pharmacy',
                        'fee' : $scope.total,
                        'discount' : 0,
                        'quantity' : 1
                    }],
					'grandTotal': $scope.total,
                    'recievedCash' : 0,
                    'discount' : 0,
                    'paymentMethod' : 'cash',
                };


                opd_service.save_bill(billingDetails).then(function (response) {
					ngToast.create({
						className: 'success',
						content: 'Patient bill updated.',
						dismissOnTimeout: true
					});
					new_order(false);
                });
            }
			});
    };

    $scope.find_patient = function() {

    };

    $scope.toggle_popup = function() {
        if($scope.in_patient_popup === true)
            $scope.in_patient_popup = false;
    };

    $scope.search_medicines = function() {
        if ($scope.name_search_keyword.length > 0) {
            Pos.search_inventory_by_name($scope.name_search_keyword).then(function (response) {
                //console.log(response);
                $scope.searched_medicines = response;
                $scope.show_dropdown = true;
            });
        }

        if ($scope.name_search_keyword.length === 0) {
            $scope.show_dropdown = false;
        }
    };

    $scope.search_medicines_stock = function() {
        if ($scope.name_search_keyword.length > 0) {
            Pos.search_inventory_by_name_stock($scope.name_search_keyword, $scope.authentication.user.store).then(function (response) {
                $scope.searched_medicines = response;
                //console.log(response);
                $scope.show_dropdown = true;
            });
        }

        if ($scope.name_search_keyword.length === 0) {
            $scope.show_dropdown = false;
        }
    };

    $scope.formula_search_medicines = function() {
        $scope.searched_medicines = [];
        if ($scope.formula_search_keyword.length > 3) {
            Pos.search_formula($scope.formula_search_keyword).then(function (response) {
                for (var key in response.data) {
                    for (var key2 in response.data[key].tradeName) {
                        $scope.searched_medicines.push({'formula': response.data[key].formula, 'description': response.data[key].tradeName[key2].description, 'inStock': response.data[key].tradeName[key2].inStock});
                    }
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory details.',
                    dismissOnTimeout: false
                });
            });
        }
        $scope.show_formula_dropdown = true;

        if ($scope.formula_search_keyword.length < 4) {
            $scope.show_formula_dropdown = false;
        }
    };

    $scope.changeDiscountType = function() {
        if (!$scope.hideSale) {
            $scope.showDiscountType = !$scope.showDiscountType;
            if ($scope.showDiscountType)
                $scope.objectItems.orderDiscountType = 'amount';
            else
                $scope.objectItems.orderDiscountType = 'percent';
            calculate_subtotal();
            calculate_total();
        }
    };

    $scope.focusNameSearch = function() {
        var setFocus = angular.element(document.querySelector('#namesearch'));
        $timeout(function() {setFocus[0].focus()});
    };

    $scope.add_doctor = function() {
        if ($scope.objectItems.prescribingDoctorRequired)
            $scope.objectItems.prescribingDoctorRequired = false;
    };

	// retrieve data for patient searching
	$scope.getPatients = function(searchType) {
		if ($scope.searchPatientKeyword.length > 0) {
			$scope.processing = true;
			opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
				$scope.patientData = [];
				$scope.patientData = response.data;
				$scope.patients = [];
				$scope.headingsPatient = [];
				$scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
				$scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
				$scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
				for (var i = 0; i < response.data.length; i++) {
					$scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
				}
				if (searchType === 'patients')
					$scope.showPatients = true;
				else if (searchType === 'prescriptions') {
					$scope.showPrescriptions = true;
				} else if (searchType === 'walkin') {
					$scope.showWalkIn = true;
				}
				var setFocus = angular.element(document.querySelector('#' + $scope.prefixPatient + $scope.prefixPatient));
				$timeout(function() {setFocus[0].focus()});
				$scope.searchPatientKeyword = '';
				$scope.processing = false;
			}).catch(function (error) {
				$scope.processing = false;
				ngToast.create({
					className: 'danger',
					content: 'Error: Unable to retrieve patients.',
					dismissButton: true
				});
			});
		} else {
			ngToast.create({
				className: 'warning',
				content: 'Please enter patient phone number or MR number to search.',
				dismissButton: true
			});
		}
	};
}]);
