'use strict';

//Setting up route
angular.module('main-page').config(['$stateProvider',
	function($stateProvider) {
		// Main page state routing
		$stateProvider.
		state('ota-bills-report', {
			url: '/ota-bills-report',
			templateUrl: 'modules/main-page/views/ota-bills-report.client.view.html',
			access: ['opd admin', 'super admin', 'reports']
		}).
		state('billings-report', {
			url: '/billings-report',
			templateUrl: 'modules/main-page/views/billings-report.client.view.html',
			access: ['super admin', 'opd admin', 'opd receptionist', 'reports']
		}).
		state('category-report', {
			url: '/category-report',
			templateUrl: 'modules/main-page/views/category-report.client.view.html',
			access: ['super admin', 'opd admin', 'reports']
		}).
		state('departmentwise-all-report', {
			url: '/departmentwise-all-report',
			templateUrl: 'modules/main-page/views/summary-all-reports.client.view.html',
			access: ['super admin', 'opd admin', 'reports']
		}).
		state('invoices-report', {
			url: '/invoices-report',
			templateUrl: 'modules/main-page/views/invoices-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		}).
		state('revenue-report', {
			url: '/revenue-report',
			templateUrl: 'modules/main-page/views/revenue-report.client.view.html',
			access: ['opd admin', 'super admin', 'opd receptionist', 'reports']
		}).
		state('patients-report', {
			url: '/patients-report',
			templateUrl: 'modules/main-page/views/patients-report.client.view.html',
			access: ['opd admin', 'super admin', 'opd receptionist', 'reports']
		}).
		state('panel-report', {
			url: '/panel-report',
			templateUrl: 'modules/main-page/views/panel-report.client.view.html',
			access: ['opd admin', 'super admin', 'opd receptionist', 'reports']
		}).
		state('panel-lab-report', {
			url: '/panel-lab-report',
			templateUrl: 'modules/main-page/views/panel-lab-report.client.view.html',
			access: ['lab admin', 'super admin', 'lab receptionist', 'reports']
		}).
		state('discount-report', {
			url: '/discount-report',
			templateUrl: 'modules/main-page/views/discount-report.client.view.html',
			access: ['opd admin', 'super admin', 'reports']
		}).
		state('refund-report', {
			url: '/refund-report',
			templateUrl: 'modules/main-page/views/refund-report.client.view.html',
			access: ['opd admin', 'super admin', 'reports']
		}).
		state('doctor-bills-report', {
			url: '/doctor-bills-report',
			templateUrl: 'modules/main-page/views/doctor-bills-report.client.view.html',
			access: ['opd admin', 'super admin', 'reports']
		}).
		state('storemain-page', {
			url: '/storemain-page',
			templateUrl: 'modules/main-page/views/storemain-page.client.view.html',
			access: ['store admin', 'reports']
		}).
		state('main-page', {
			url: '/main-page',
			templateUrl: 'modules/main-page/views/main-page.client.view.html',
			access: ['pharmacy admin', 'reports']
		})
		.state('admin-main-page', {
			url: '/admin-main-page',
			templateUrl: 'modules/main-page/views/admin-main-page.client.view.html',
			access: ['pharmacy admin', 'super admin']
		})
		.state('dashboard-page', {
			url: '/dashboard-page',
			templateUrl: 'modules/main-page/views/dashboard-page.client.view.html',
			access: ['pharmacy admin', 'super admin']
		})
		.state('salesReport', {
			url: '/salesReport',
			templateUrl: 'modules/main-page/views/sales-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports', 'pharmacy salesman']
		})
		.state('purchaseReport', {
			url: '/purchaseReport',
			templateUrl: 'modules/main-page/views/purchase-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		})
		.state('summarySalesReport', {
			url: '/summarySalesReport',
			templateUrl: 'modules/main-page/views/summary-sales-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports', 'pharmacy salesman']
		})
		.state('costSalesReport', {
			url: '/costSalesReport',
			templateUrl: 'modules/main-page/views/cost-sales-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		})
		.state('incdecReport', {
			url: '/incdecReport',
			templateUrl: 'modules/main-page/views/increase-decrease-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports', 'pharmacy salesman']
		})
		.state('narcoticssalesReport', {
			url: '/narcoticssalesReport',
			templateUrl: 'modules/main-page/views/narcotics-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		});
	}
]);
