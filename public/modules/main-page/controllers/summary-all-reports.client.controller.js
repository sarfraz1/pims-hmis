'use strict';

angular.module('main-page').controller('SummaryAllReportsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

		if($scope.authentication.user.roles=='pharmacy salesman') {
			$scope.report = {};
			$scope.report.pharmacist = $scope.authentication.user.username;
		}

		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];
        // If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');
        else
            if ($scope.authentication.user.roles === 'employee')
                $location.path('/signin');

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

				var calculateTotal = function() {
			var total = 0;
			var feeTotal = 0;
			var discountTotal = 0;
			var refundTotal = 0;
			var quantityTotal =0;

            for (var i = 0; i < $scope.datatoShow.length; i++) {
				feeTotal+=$scope.datatoShow[i].amount;
				discountTotal+=$scope.datatoShow[i].discount;
				refundTotal+=$scope.datatoShow[i].refund;
				quantityTotal+=$scope.datatoShow[i].quantity;
               total+= ($scope.datatoShow[i].amount - $scope.datatoShow[i].discount - $scope.datatoShow[i].refund);
            }
			$scope.total = total;
			$scope.feeTotal = feeTotal;
			$scope.discountTotal = discountTotal;
			$scope.refundTotal = refundTotal;
			$scope.quantityTotal = quantityTotal;
        };

				var getDepartments = function() {
						main_page_service.get_facilities().then(function (response) {
							for (var i = 0; i < response.data.length; i++) {
								if (response.data[i].name === 'Hospital') {
									response.data.splice(i, 1);
									i--;
								}
							}
								$scope.departments = angular.copy(response.data);
								$scope.departments.unshift({name: 'Consultation'});
								$scope.departments.unshift({name: 'Registration'});
								$scope.departments.unshift({name: 'All'});
						}).catch(function (error) {
								ngToast.create({
										className: 'danger',
										content: 'Error: Unable to retrieve doctors.',
										dismissButton: true
								});
						});
				};

        $scope.btn_Apply = function() {
            get_patients();
        };

				var get_patients = function(){
					var toDate = new Date();
					var fromDate = new Date();
					fromDate.setHours(0,0,0,0);
					main_page_service.get_patient(fromDate, toDate).then(function(response){
						console.log(response.data);
					//	$scope.total_patients = response.data.length;
						//console.log($scope.total_patients);
					});
				}

	$scope.exportfile = function(){
		var filestring = '';

		//create_header
		var header = "Invoice ID,Date,Time,Item ID,Description,Quantity,Discount,Total\n";
		filestring +=header;
		for(var i=0;i<$scope.sales.length;i++) {
			for(var j=0;j<$scope.sales[i].orderList.length;j++) {
				var line = $scope.sales[i].invoiceNumber;
				line+= ','+ moment($scope.sales[i].invoiceDate).format('DD-MM-YYYY');
				line+= ','+ $scope.sales[i].invoiceTime;
				line+= ','+ $scope.sales[i].orderList[j].id;
				line+= ','+ $scope.sales[i].orderList[j].description;

				if($scope.sales[i].isReturn)
					line+= ','+ $scope.sales[i].orderList[j].quantity*-1;
				else
					line+= ','+ $scope.sales[i].orderList[j].quantity;
				line+= ','+ $scope.sales[i].orderList[j].disc;
				if($scope.sales[i].isReturn)
					line+= ','+ Math.round($scope.sales[i].orderList[j].quantity*$scope.sales[i].orderList[j].price-$scope.sales[i].orderList[j].disc)*-1;
				else
					line+= ','+ Math.round($scope.sales[i].orderList[j].quantity*$scope.sales[i].orderList[j].price-$scope.sales[i].orderList[j].disc);
				filestring+=line+'\n';
			}

		}

		var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
		return saveAs(data, ['PharmacySales-Report' + '.csv']);
	};

	$scope.printreport = function() {
		var dt = moment(new Date());
		var ts = [];
		var stocktrans = [];
		for(var i=0;i<$scope.sales.length;i++) {
			for(var j=0;j<$scope.sales[i].orderList.length;j++) {
				ts.push({ invoiceNumber: $scope.sales[i].invoiceNumber,
						  isReturn: $scope.sales[i].isReturn,
						  invoiceDate: $scope.sales[i].invoiceDate,
						  invoiceTime: $scope.sales[i].invoiceTime,
						  id: $scope.sales[i].orderList[j].id,
						  description: $scope.sales[i].orderList[j].description,
						  quantity: $scope.sales[i].orderList[j].quantity,
						  disc: $scope.sales[i].orderList[j].disc,
						  price: $scope.sales[i].orderList[j].price
				});
			}
		}

		for(var i=0;i<$scope.stocktransfers.length;i++) {
			for(var j=0;j<$scope.stocktransfers[i].itemsList.length;j++) {
				stocktrans.push({ transferNumber: $scope.stocktransfers[i].transferNumber,
						  date: moment($scope.stocktransfers[i].date).format('DD-MM-YYYY'),
						  fromStore: $scope.stocktransfers[i].fromStoreDescription,
						  toStore: $scope.stocktransfers[i].toStoreDescription,
						  transferedBy: $scope.stocktransfers[i].transferedBy,
						  id: $scope.stocktransfers[i].itemsList[j].code,
						  description: $scope.stocktransfers[i].itemsList[j].description,
						  quantity: $scope.stocktransfers[i].itemsList[j].quantity

				});
			}
		}

		var obj = {
			'invoices': ts,
			'stocktransfer': stocktrans,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'salesman': $scope.report.pharmacist,
			'totalAmounts' : $scope.totalSalesAmount,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a')
		};
		print_service.print('/modules/main-page/views/sales-report-print.client.view.html',obj,
		function(){
		});
	};

	$scope.init = function(){
		getDepartments();

		$scope.report = {
			'department' : 'All'
		};
	};

    $scope.init();

    }
]);
