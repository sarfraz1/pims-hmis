"use strict";

angular.module("main-page").controller("DashboardPageController", [
  "$scope",
  "$http",
  "$stateParams",
  "$location",
  "Authentication",
  "ngToast",
  "main_page_service",
  "print_service",
  "$anchorScroll",
  function(
    $scope,
    $http,
    $stateParams,
    $location,
    Authentication,
    ngToast,
    main_page_service,
    print_service,
    $anchorScroll
  ) {
    $scope.authentication = Authentication;

    // If user is signed in then redirect back home
    if (!$scope.authentication.user) $location.path("/signin");

    $scope.ismedicure = false;

    var start = function() {
      $http.get("/hospital/logoname.json").then(function(response) {
        $scope.hosDetail = response.data.user;
      });
    };

    start();

    var dateConverter = function(dateinput) {
      try {
        var utcDate = new Date(
          Date.UTC(
            dateinput.getFullYear(),
            dateinput.getMonth(),
            dateinput.getDate()
          )
        );
        utcDate = utcDate.toUTCString();
        return utcDate;
      } catch (error) {
        return dateinput;
      }
    };
    let departments = [];
    var calculateTotal = function(data) {
      var total = 0;
      var feeTotal = 0;
      var discountTotal = 0;
      var refundTotal = 0;
      var quantityTotal = 0;

      for (var i = 0; i < data.length; i++) {
        feeTotal += data[i].amount;
        discountTotal += data[i].discount;
        refundTotal += data[i].refund;
        quantityTotal += data[i].quantity;
        total += data[i].amount - data[i].discount - data[i].refund;
      }
      return {
        total,
        feeTotal,
        discountTotal,
        refundTotal,
        quantityTotal
      };
    };

    var getDepartments = function() {
      main_page_service
        .get_facilities()
        .then(function(response) {
          for (var i = 0; i < response.data.length; i++) {
            if (response.data[i].name === "Hospital") {
              departments.push("Registration");
              departments.push(...response.data[i].children);
              departments.splice(departments.indexOf("Pharmacy"), 1);
              response.data.splice(i, 1);
              i--;
            }
          }

          $scope.departments = angular.copy(response.data);
          $scope.departments.unshift({ name: "Registration" });
          $scope.departments.unshift({ name: "Consultation" });
          $scope.departments.unshift({ name: "All" });
        })
        .catch(function(error) {
          ngToast.create({
            className: "danger",
            content: "Error: Unable to retrieve doctors.",
            dismissButton: true
          });
        });
    };

    var getIndex = function(array, prop, value) {
      var index = -1;
      for (var x = 0; x < array.length; x++) {
        if (array[x][prop] === value) {
          index = x;
        }
      }
      return index;
    };

    var getSpecificCategoriesReport = function(
      department,
      fromDate,
      toDate,
      callback
    ) {
      var toDate = dateConverter(toDate);
      var fromDate = dateConverter(fromDate);
      main_page_service
        .get_department_report(department, fromDate, toDate)
        .then(function(response) {
          $scope.bills = response.data;
          var datatoShow = [];
          for (var i = 0; i < $scope.bills.length; i++) {
            var ind = getIndex(
              datatoShow,
              "description",
              $scope.bills[i].description
            );
            if (ind == -1) {
              datatoShow.push({
                description: $scope.bills[i].description,
                amount: $scope.bills[i].quantity * $scope.bills[i].fee,
                quantity: $scope.bills[i].quantity,
                refund: $scope.bills[i].refund ? $scope.bills[i].refund : 0,
                discount: $scope.bills[i].discount
              });
            } else {
              datatoShow[ind].amount +=
                $scope.bills[i].quantity * $scope.bills[i].fee;
              datatoShow[ind].quantity += $scope.bills[i].quantity;
              datatoShow[ind].discount += $scope.bills[i].discount;
              if ($scope.bills[i].refund) {
                datatoShow[ind].refund += $scope.bills[i].refund;
              }
            }
          }
          callback(datatoShow);
        })
        .catch(function(err) {
          console.log(err);
        });
    };

    var calculateTotalByDepartment = function(department) {
      let filteredBills = [];
      let bills = $scope.bills || [];
      for (let i = 0; i < bills.length; i++) {
        if (department === "All") {
          filteredBills.push(bills[i]);
        } else if (department === "Registration") {
          if (
            bills[i].description.toLowerCase() === "Registration".toLowerCase()
          ) {
            filteredBills.push(bills[i]);
          }
        } else if (
          bills[i].category === undefined &&
          bills[i].description === "Consultation"
        ) {
          if (department.toLowerCase() === "consultation") {
            filteredBills.push(bills[i]);
          }
        } else if (bills[i].category !== undefined) {
          if (
            bills[i].category
              .toLowerCase()
              .indexOf(department.toLowerCase()) !== -1
          ) {
            filteredBills.push(bills[i]);
          }
        }
      }
      var datatoShow = [];
      for (var i = 0; i < filteredBills.length; i++) {
        var ind = getIndex(
          datatoShow,
          "description",
          filteredBills[i].description
        );
        if (ind == -1) {
          datatoShow.push({
            description: filteredBills[i].description,
            amount: filteredBills[i].quantity * filteredBills[i].fee,
            quantity: filteredBills[i].quantity,
            refund: filteredBills[i].refund ? filteredBills[i].refund : 0,
            discount: filteredBills[i].discount
          });
        } else {
          datatoShow[ind].amount +=
            filteredBills[i].quantity * filteredBills[i].fee;
          datatoShow[ind].quantity += filteredBills[i].quantity;
          datatoShow[ind].discount += filteredBills[i].discount;
          if (filteredBills[i].refund) {
            datatoShow[ind].refund += filteredBills[i].refund;
          }
        }
      }
      return calculateTotal(datatoShow);
    };

    var calculateRevenue = bills => {
      let department = $scope.report.department;
      var filteredBills = [];
      for (var i = 0; i < bills.length; i++) {
        for (var j = 0; j < bills[i].servicesInfo.length; j++) {
          if (department === "All") {
            filteredBills.push(bills[i].servicesInfo[j]);
          } else if (department === "Registration") {
            if (
              bills[i].servicesInfo[j].description.toLowerCase() ===
              "Registration".toLowerCase()
            ) {
              filteredBills.push(bills[i].servicesInfo[j]);
            }
          } else if (
            bills[i].servicesInfo[j].category === undefined &&
            bills[i].servicesInfo[j].description === "Consultation"
          ) {
            if (department.toLowerCase() === "consultation") {
              filteredBills.push(bills[i].servicesInfo[j]);
            }
          } else if (bills[i].servicesInfo[j].category !== undefined) {
            if (
              bills[i].servicesInfo[j].category
                .toLowerCase()
                .indexOf(department.toLowerCase()) !== -1
            ) {
              filteredBills.push(bills[i].servicesInfo[j]);
            }
          }
        }
      }
      var datatoShow = [];
      for (var i = 0; i < filteredBills.length; i++) {
        var ind = getIndex(
          datatoShow,
          "description",
          filteredBills[i].description
        );
        if (ind == -1) {
          datatoShow.push({
            description: filteredBills[i].description,
            amount: filteredBills[i].quantity * filteredBills[i].fee,
            quantity: filteredBills[i].quantity,
            refund: filteredBills[i].refund ? filteredBills[i].refund : 0,
            discount: filteredBills[i].discount
          });
        } else {
          datatoShow[ind].amount +=
            filteredBills[i].quantity * filteredBills[i].fee;
          datatoShow[ind].quantity += filteredBills[i].quantity;
          datatoShow[ind].discount += filteredBills[i].discount;
          if (filteredBills[i].refund) {
            datatoShow[ind].refund += filteredBills[i].refund;
          }
        }
      }
      return calculateTotal(datatoShow);
    };

    var filterRevenue = function(graphBills, fromTime, toTime) {
      let filteredBillsByDate = [];
      for (let i = 0; i < graphBills.length; i++) {
        if (
          new Date(graphBills[i].created) >= fromTime &&
          new Date(graphBills[i].created) < +toTime
        ) {
          filteredBillsByDate.push(graphBills[i]);
        }
      }
      return calculateRevenue(filteredBillsByDate);
    };

    var getRevenueChartData = function(graphBills) {
      $scope.chartData = [];
      let monthNames = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ];
      let fromDate = new Date($scope.report.fromDate);
      fromDate = new Date(
        fromDate.getFullYear(),
        fromDate.getMonth(),
        fromDate.getDate()
      );
      let toDate = new Date($scope.report.toDate);
      toDate = new Date(
        toDate.getFullYear(),
        toDate.getMonth(),
        toDate.getDate() + 1
      );
      if ($scope.report.showBy === "Hourly") {
        let fromTime = fromDate;
        let toTime = new Date(
          fromTime.getFullYear(),
          fromTime.getMonth(),
          fromTime.getDate(),
          fromTime.getHours() + 1
        );
        while (fromTime < toDate) {
          let pointData = filterRevenue(graphBills, fromTime, toTime);
          $scope.chartData.push({
            label: fromTime.getHours() + ":00",
            value: pointData.total
          });
          fromTime = toTime;
          toTime = new Date(
            fromTime.getFullYear(),
            fromTime.getMonth(),
            fromTime.getDate(),
            fromTime.getHours() + 1
          );
        }
        $scope.chartXexis = "00:00-23:59";
      }
      if ($scope.report.showBy === "Daily") {
        let fromTime = fromDate;
        let toTime = new Date(
          fromTime.getFullYear(),
          fromTime.getMonth(),
          fromTime.getDate() + 1
        );
        while (fromTime < toDate) {
          let pointData = filterRevenue(graphBills, fromTime, toTime);
          $scope.chartData.push({
            label: fromTime.getDate() + " " + monthNames[fromTime.getMonth()],
            value: pointData.total
          });
          fromTime = toTime;
          toTime = new Date(
            fromTime.getFullYear(),
            fromTime.getMonth(),
            fromTime.getDate() + 1
          );
        }
        $scope.chartXexis =
          monthNames[fromDate.getMonth()] +
          ", " +
          fromDate.getFullYear() +
          "-" +
          monthNames[toDate.getMonth()] +
          ", " +
          toDate.getFullYear();
      }
      if ($scope.report.showBy === "Monthly") {
        let fromTime = fromDate;
        let toTime = new Date(fromTime.getFullYear(), fromTime.getMonth() + 1);
        while (fromTime < toDate) {
          let pointData = filterRevenue(graphBills, fromTime, toTime);
          $scope.chartData.push({
            label:
              monthNames[fromTime.getMonth()] + ", " + fromTime.getFullYear(),
            value: pointData.total
          });
          fromTime = toTime;
          toTime = new Date(fromTime.getFullYear(), fromTime.getMonth() + 1);
        }
        $scope.chartXexis =
          monthNames[fromDate.getMonth()] +
          ", " +
          fromDate.getFullYear() +
          "-" +
          monthNames[toDate.getMonth()] +
          ", " +
          toDate.getFullYear();
      }
    };

    var registrationRecord = function() {
      $scope.registrations = calculateTotalByDepartment("Registration");
    };

    var consultationRecord = function() {
      $scope.consultations = calculateTotalByDepartment("Consultation");
    };

    var labRecord = function() {
      $scope.labs = calculateTotalByDepartment("Lab");
    };

    var radiologyRecord = function() {
      $scope.radiology = calculateTotalByDepartment("Radiology");
    };

    var drawRevenueChart = function() {
      $scope.options = {
        chart: {
          type: "discreteBarChart",
          height: 450,
          margin: {
            top: 20,
            right: 20,
            bottom: 60,
            left: 55
          },
          x: function(d) {
            return d.label;
          },
          y: function(d) {
            return d.value;
          },
          showValues: true,
          valueFormat: function(d) {
            return d3.format(",.0f")(d);
          },
          transitionDuration: 500,
          xAxis: {
            axisLabel: $scope.chartXexis
          },
          yAxis: {
            axisLabel: "Rs.",
            axisLabelDistance: 30
          }
        },
        title: {
          enable: true,
          text: "Revenue Graph",
          className: "h4",
          css: {
            width: "nullpx",
            textAlign: "center"
          }
        }
      };

      $scope.data = [
        {
          key: "Cumulative Return",
          values: $scope.chartData
        }
      ];
    };

    var drawDepartmentChart = function() {
      $scope.departmentOptions = {
        chart: {
          type: "pieChart",
          height: 800,
          x: function(d) {
            return d.key;
          },
          y: function(d) {
            return d.y;
          },
          showLabels: true,
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 5,
              right: 35,
              bottom: 5,
              left: 0
            }
          },
          legendPosition: "bottom"
        }
      };

      $scope.departmentData = [];
      $scope.departmentsRevenue = [];
      for (let i = 0; i < departments.length; i++) {
        let data = calculateTotalByDepartment(departments[i]).total;
        $scope.departmentsRevenue.push({ name: departments[i], revenue: data });
        if (data !== 0) {
          $scope.departmentData.push({
            key: departments[i],
            y: data
          });
        }
      }
    };

    var revenueChart = function() {
      main_page_service
        .get_revenue_for_chart($scope.report.fromDate, $scope.report.toDate)
        .then(response => {
          $scope.graphBills = response.data;
          $scope.updateGraphView();
        });
    };

    $scope.updateGraphView = () => {
      getRevenueChartData($scope.graphBills);
      drawRevenueChart();
    };

    $scope.update = function() {
      getSpecificCategoriesReport(
        "All",
        $scope.report.fromDate,
        $scope.report.toDate,
        data => {
          $scope.total = calculateTotal(data);
          registrationRecord();
          consultationRecord();
          labRecord();
          radiologyRecord();
          drawDepartmentChart();
        }
      );
      if (
        +new Date(
          $scope.report.fromDate.getFullYear(),
          $scope.report.fromDate.getMonth(),
          $scope.report.fromDate.getDate()
        ) ===
        +new Date(
          $scope.report.toDate.getFullYear(),
          $scope.report.toDate.getMonth(),
          $scope.report.toDate.getDate()
        )
      ) {
        $scope.showChart = ["Hourly"];
        $scope.report.showBy = $scope.showChart[0];
      } else if (
        Math.ceil(
          Math.abs(
            $scope.report.toDate.getTime() - $scope.report.fromDate.getTime()
          ) /
            (1000 * 3600 * 24)
        ) <= 30
      ) {
        $scope.showChart = ["Daily", "Monthly"];
        $scope.report.showBy = $scope.showChart[0];
      } else {
        $scope.showChart = ["Daily", "Monthly"];
        $scope.report.showBy = $scope.showChart[1];
      }
      revenueChart();
      getSalesReport();
      getPurchaseReport();
    };

    $scope.init = function() {
      getDepartments();
      let now = new Date();
      $scope.report = {
        department: "All",
        toDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()),
        fromDate: new Date(now.getFullYear(), now.getMonth(), now.getDate())
      };
      $scope.update();
    };
    $scope.init();

    var calculateTotalSales = function(sales) {
      let total = 0;
      let creditotal = 0;
      $scope.creditsales = [];
      $scope.sales = [];
      for (let i = 0; i < sales.length; i++) {
        if (!sales[i].issalemrno && !sales[i].issalecredit) {
          for (let j = 0; j < sales[i].orderList.length; j++) {
            sales[i].orderList[j].disc = 0;
            if (sales[i].orderDiscount > 0) {
              if (sales[i].orderDiscountType == "percent") {
                sales[i].orderList[j].disc = Math.round(
                  sales[i].orderList[j].price *
                    sales[i].orderList[j].quantity *
                    (sales[i].orderDiscount / 100)
                );
              } else {
                sales[i].orderList[j].disc = Math.round(
                  sales[i].orderDiscount / sales[i].orderList.length
                );
              }

              if (sales[i].isReturn) {
                total =
                  total -
                  (sales[i].orderList[j].quantity *
                    sales[i].orderList[j].price -
                    sales[i].orderList[j].disc);
              } else {
                total =
                  total +
                  (sales[i].orderList[j].quantity *
                    sales[i].orderList[j].price -
                    sales[i].orderList[j].disc);
              }
            } else {
              if (sales[i].isReturn) {
                total -=
                  sales[i].orderList[j].quantity * sales[i].orderList[j].price;
              } else {
                total +=
                  sales[i].orderList[j].quantity * sales[i].orderList[j].price;
              }
            }
          }
          $scope.sales.push(sales[i]);
        } else {
          for (let j = 0; j < sales[i].orderList.length; j++) {
            sales[i].orderList[j].disc = 0;
            if (sales[i].orderDiscount > 0) {
              if (sales[i].orderDiscountType == "percent") {
                sales[i].orderList[j].disc = Math.round(
                  sales[i].orderList[j].price *
                    sales[i].orderList[j].quantity *
                    (sales[i].orderDiscount / 100)
                );
              } else {
                sales[i].orderList[j].disc = Math.round(
                  sales[i].orderDiscount / sales[i].orderList.length
                );
              }

              if (sales[i].isReturn) {
                creditotal =
                  creditotal -
                  (sales[i].orderList[j].quantity *
                    sales[i].orderList[j].price -
                    sales[i].orderList[j].disc);
              } else {
                creditotal =
                  creditotal +
                  (sales[i].orderList[j].quantity *
                    sales[i].orderList[j].price -
                    sales[i].orderList[j].disc);
              }
            } else {
              if (sales[i].isReturn) {
                creditotal -=
                  sales[i].orderList[j].quantity * sales[i].orderList[j].price;
              } else {
                creditotal +=
                  sales[i].orderList[j].quantity * sales[i].orderList[j].price;
              }
            }
          }
          $scope.creditsales.push(sales[i]);
        }
      }
      $scope.totalCreditSalesAmount = Math.round(creditotal);
      $scope.totalSalesAmount = total;
    };

    let filterinvoicesbytime = function(invoices) {
      let invtosend = [];
      for (let i = 0; i < invoices.length; i++) {
        if (invoices[i].invoiceTime.length < 8) {
          invoices[i].invoiceTime = "0" + invoices[i].invoiceTime;
        }
        let invdate = invoices[i].invoiceTime;

        if (
          moment(invoices[i].invoiceDate).format("DD-MM-YYYY") ==
          moment($scope.report.fromDate).format("DD-MM-YYYY")
        ) {
          let invtime = moment(invdate, "hh:mm A");
          if ($scope.fromTime !== "undefined") {
            let frT = moment($scope.fromTime, "hh:mm A");
            if (invtime.isBefore(frT) == true) continue;
          }
        }

        if (
          moment(invoices[i].invoiceDate).format("DD-MM-YYYY") ==
          moment($scope.report.toDate).format("DD-MM-YYYY")
        ) {
          let invtime = moment(invdate, "hh:mm A");

          if ($scope.toTime !== "undefined") {
            let toT = moment($scope.toTime, "hh:mm A");
            if (toT.isBefore(invtime) == true) continue;
          }
        }
        invtosend.push(invoices[i]);
      }
      return invtosend;
    };

    function getSalesReport() {
      let toDate = dateConverter($scope.report.toDate);
      let fromDate = dateConverter($scope.report.fromDate);

      if (toDate === null) {
        toDate = undefined;
      }
      if (fromDate === null) {
        fromDate = undefined;
      }
      main_page_service
        .get_sales_report(
          "All",
          fromDate,
          toDate,
          $scope.report.fromTime,
          $scope.report.toTime,
          $scope.currentPage
        )
        .then(function(response) {
          let fromTime = $scope.report.fromTime;
          let toTime = $scope.report.toTime;
          let sales;
          if (fromTime || toTime) {
            sales = filterinvoicesbytime(response.data);
          } else {
            sales = angular.copy(response.data);
          }

          calculateTotalSales(sales);
        })
        .catch(function(err) {
          console.log(err);
        });
    }

    var calculateTotalPurchase = function(purchase) {
      var total = 0;
      for (var i = 0; i < purchase.length; i++) {
        total += purchase[i].netValue;
      }
      $scope.totalPurchaseAmount = total;
    };

    function getPurchaseReport(move) {
      var toDate = dateConverter($scope.report.toDate);
      var fromDate = dateConverter($scope.report.fromDate);

      if (toDate === null) {
        toDate = undefined;
      }
      if (fromDate === null) {
        fromDate = undefined;
      }

      if (fromDate && toDate) {
        main_page_service
          .get_purchase_report(fromDate, toDate, $scope.currentPage)
          .then(function(response) {
            let purchase = angular.copy(response.data);
            calculateTotalPurchase(purchase);
          })
          .catch(function(err) {
            console.log(err);
          });
      }
    };
  }
]);
