'use strict';

angular.module('main-page').controller('AdminMainPageController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'inventory_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, ngToast, main_page_service, inventory_service, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        else {
            if($scope.authentication.user.roles === 'super admin') {
                $location.path('/admin-main-page');
            }
            else{
                $location.path('/signin')
            }
        }

        var getAllRequistions = function(pageNo){
            main_page_service.get_all_requisitions(pageNo).then(function (response) {
                for(var i = 0;i<response.data.length;i++){
                    //response.data[i].date = new Date();
                    $scope.purchaseRequisitions.push(response.data[i]);    
                }
               
            }).catch(function(err){
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to get purchase requisitions list.',
                    dismissButton: true
                });
            }); 
        };

        var getAllPurchaseOrders = function(pageNo){
            main_page_service.get_all_purchase_order(pageNo).then(function (response) {
                $scope.purchaseOrder.push(response.data);
                   
            }).catch(function(err){
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to get purchase Orders list.',
                    dismissButton: true
                });
            }); 
        };

        var getAllGRN = function(pageNo){
            main_page_service.get_all_GRN(pageNo).then(function (response) {
                $scope.GRN.push(response.data);

            }).catch(function(err){
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to get GRN list.',
                    dismissButton: true
                });
            }); 
        };

        $scope.purchaseRequisitionRowClick = function(purchaseRequisition){
            $scope.selectedPurchaseRequisition = purchaseRequisition;
            $scope.showRequisition = true;
        };

        $scope.purchaseRequisitionDecision = function(purchaseRequisition,status){
			purchaseRequisition.status = status;
            main_page_service.set_requisitions_status(purchaseRequisition).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Success: Approved Successfully.',
                    dismissButton: true
                });
            
            }).catch(function(err){
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save. Please try again.',
                    dismissButton: true
                });
            });  
        };
		
		$scope.updateRequisiton = function(purchaseRequisition, status) {
			purchaseRequisition.status = status;
			$scope.processing = true;
			inventory_service.update_purchase_requisition(purchaseRequisition).then(function (response) {
				ngToast.create({
					className: 'success',
					content: 'Purchase Requisition Updated Successfully',
					dismissButton: true
				});
				$scope.processing = false;
			}).catch(function (error) {
				$scope.processing = false;
				ngToast.create({
					className: 'danger',
					content: 'Error: Unable to update Purchase Requisition.',
					dismissButton: true
				});
			});
		};
		
        $scope.remove_medicine = function(index) {
             $scope.selectedPurchaseRequisition.itemsList.splice(index,1); 
        };

        $scope.purchaseOrderRowClick = function(purchaseOrder){
            $scope.showPurchaseOrder = true;
            $scope.selectedPurchaseOrder = purchaseOrder;
        };

        $scope.GRNRowClick = function(GRN){
            $scope.showGRN = true;
            $scope.selectedPurchaseGRN = GRN;
        };

        

        $scope.init = function(){

            $scope.showRequisition = false;
            $scope.showPurchaseOrder = false;
            $scope.showGRN = false;
            
            $scope.confirmationPopup = 'ng-hide';

            $scope.purchaseRequisitions = [];
            $scope.purchaseOrder = [];
            $scope.GRN = [];

            getAllRequistions(0);
            getAllPurchaseOrders(0);
            getAllGRN(0);
            
        };

        $scope.init();
        
    }
]);