'use strict';

angular.module('main-page').controller('CategoryReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

		var codes = ['LB0001', 'LB0002', 'LB0004', 'LB0005', 'LB0011', 'LB0012', 'LB0025', 'LB0026', 'LB0029', 'LB0035',
					'LB0036', 'LB0045', 'LB0048', 'LB0049', 'LB0051', 'LB0052', 'LB0072', 'LB0077', 'LB0087', 'LB0088',
					'LB0111', 'LB0125', 'LB0130', 'LB0133', 'LB0137', 'LB0148', 'LB0149', 'LB0163', 'LB0166', 'LB0169',
					'LB0171', 'LB0180', 'LB0183', 'LB0185', 'LB0191', 'LB0195', 'LB0196', 'LB0201', 'LB0218', 'LB0220',
					'LB0222', 'LB0223', 'LB0242', 'LB0244', 'LB0247', 'LB0249', 'LB0252', 'LB0516'];

		$scope.ismedicure = false;

		var start = function (){
			$http.get('/hospital/logoname.json').then(function(response){
			$scope.hosDetail= response.data.user;
		});
		};

		start();

        var dateConverter = function(dateinput) {
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var calculateTotal = function() {
			var total = 0;
			var feeTotal = 0;
			var discountTotal = 0;
			var refundTotal = 0;
			var quantityTotal =0;

            for (var i = 0; i < $scope.datatoShow.length; i++) {
				feeTotal+=$scope.datatoShow[i].amount;
				discountTotal+=$scope.datatoShow[i].discount;
				refundTotal+=$scope.datatoShow[i].refund;
				quantityTotal+=$scope.datatoShow[i].quantity;
               total+= ($scope.datatoShow[i].amount - $scope.datatoShow[i].discount - $scope.datatoShow[i].refund);
            }
			$scope.total = total;
			$scope.feeTotal = feeTotal;
			$scope.discountTotal = discountTotal;
			$scope.refundTotal = refundTotal;
			$scope.quantityTotal = quantityTotal;
        };

		//only for medicure TODO: change this approach to do this from panel labs
        var calculateShares = function() {
			var total = 0;
			$scope.totalshare = 0;
            for (var i = 0; i < $scope.datatoShow.length; i++) {
				if($scope.datatoShow[i].description.indexOf("( KIH LAB )") > 0) {
				   var total = ($scope.datatoShow[i].amount - $scope.datatoShow[i].discount - $scope.datatoShow[i].refund);
				   var share = 0;
				   for(var j=0;j<codes.length;j++) {
				     if($scope.datatoShow[i].description.indexOf(codes[j]) > 0) { //share is 50-50
						share = Math.round(total*0.5);
					 }
				   }
				   if(share == 0) {
					   share = Math.round(total*0.62);
				   }

				   $scope.datatoShow[i].share = share;
			   } else {
				   $scope.datatoShow[i].share = 0;
			   }

			   $scope.totalshare+=$scope.datatoShow[i].share;
            }
        };


        var getDepartments = function() {
            main_page_service.get_facilities().then(function (response) {
            	for (var i = 0; i < response.data.length; i++) {
            		if (response.data[i].name === 'Hospital') {
            			response.data.splice(i, 1);
            			i--;
            		}
            	}
                $scope.departments = angular.copy(response.data);
                $scope.departments.unshift({name: 'Consultation'});
                $scope.departments.unshift({name: 'Registration'});
                $scope.departments.unshift({name: 'All'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        var getAllCategoriesReport = function() {
            main_page_service.get_department_report('All', undefined, undefined).then(function (response) {
            	$scope.bills = response.data;
				var datatoShow = [];
				for(var i=0;i<$scope.bills.length;i++) {
					var ind = getIndex(datatoShow, 'description', $scope.bills[i].description);
					if(ind == -1) {
							datatoShow.push({
								description : $scope.bills[i].description,
								amount : ($scope.bills[i].quantity*$scope.bills[i].fee),
								quantity: $scope.bills[i].quantity,
								refund: $scope.bills[i].refund ? $scope.bills[i].refund:0,
								discount : $scope.bills[i].discount
							});
					} else {
						datatoShow[ind].amount += (($scope.bills[i].quantity*$scope.bills[i].fee));
						datatoShow[ind].quantity += $scope.bills[i].quantity;
						datatoShow[ind].discount += $scope.bills[i].discount;
						if($scope.bills[i].refund) {
							datatoShow[ind].refund += $scope.bills[i].refund;
						}

					}
				}

				$scope.datatoShow = datatoShow;
				calculateTotal();

            }).catch(function(err){
                console.log(err);
            });
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

		$scope.printreport = function() {
			var dt = moment(new Date());
			var obj = {
				'datatoShow': $scope.datatoShow,
				'totalAmounts' : $scope.total,
				'feeTotal': $scope.feeTotal,
				'discountTotal': $scope.discountTotal,
				'refundTotal': $scope.refundTotal,
				'quantityTotal': $scope.quantityTotal,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a'),
				'showshares': $scope.showshares,
				'totalshare': $scope.totalshare
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/departmentwise-report-print.client.view.html',obj,
            function(){
            });
		};

		$scope.getSpecificCategoriesReport = function() {
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
		    main_page_service.get_department_report($scope.report.department, fromDate, toDate).then(function (response) {
		    	$scope.bills = response.data;
				var datatoShow = [];
				for(var i=0;i<$scope.bills.length;i++) {
					var ind = getIndex(datatoShow, 'description', $scope.bills[i].description);
					if(ind == -1) {
						datatoShow.push({
							description : $scope.bills[i].description,
							amount : ($scope.bills[i].quantity*$scope.bills[i].fee),
							quantity: $scope.bills[i].quantity,
							refund: $scope.bills[i].refund ? $scope.bills[i].refund:0,
							discount : $scope.bills[i].discount,
						});
					} else {
						datatoShow[ind].amount += (($scope.bills[i].quantity*$scope.bills[i].fee));
						datatoShow[ind].quantity += $scope.bills[i].quantity;
						datatoShow[ind].discount += $scope.bills[i].discount;
						if($scope.bills[i].refund) {
							datatoShow[ind].refund += $scope.bills[i].refund;
						}
					}
				}

				if($scope.ismedicure && $scope.report.department == 'Lab')
					$scope.showshares = true;

				$scope.datatoShow = datatoShow;
				calculateTotal();
				if($scope.showshares) {
					calculateShares();
				}

		    }).catch(function(err){
		        console.log(err);
		    });
		};


        $scope.init = function() {
        	getDepartments();
            $scope.report = {
                department : 'All'
			};
			
			if($location.search()['fromDate']) {	
				$scope.report.department = $location.search()['department'];
				let fromDate = $location.search()['fromDate'];
				let toDate = $location.search()['toDate'];
				$scope.report.fromDate = new Date(fromDate);
				$scope.report.toDate = new Date(toDate);
				$scope.getSpecificCategoriesReport();
			} else {
				getAllCategoriesReport();
			}
			
        };

        $scope.init();

    }
]);
