'use strict';

angular.module('main-page').controller('CostSalesReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

		// If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');

						var start = function (){
		 				 $http.get('/hospital/logoname.json').then(function(response){
		 				 $scope.hosDetail= response.data.user;
		 			 });
		 			 };

		 			 start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        $scope.btn_Apply = function() {

            $scope.getReport();

        };

	var getIndex = function(array, prop, value) {
		var index = -1;
		for (var x = 0; x < array.length; x++) {
			if (array[x][prop] === value) {
				index = x;
			}
		}
		return index;
	};

	var calculateDateWiseTotal = function(sales) {
		var total = 0;
		var purchasetotal = 0;
		var creditotal = 0;
		$scope.creditsales = [];
		$scope.sales = [];
		for (var i=0;i<sales.length;i++) {
			sales[i].disc = 0;
			sales[i].total = 0;
			sales[i].purchasetotal =0;
				for(var j=0;j<sales[i].orderList.length;j++) {
					var disc = 0;

					if(sales[i].isReturn) {
						sales[i].total= sales[i].total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
						sales[i].purchasetotal= sales[i].purchasetotal-(sales[i].orderList[j].quantity*(sales[i].orderList[j].price - 0.15*sales[i].orderList[j].price));
					} else {
						sales[i].total= sales[i].total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
						sales[i].purchasetotal= sales[i].purchasetotal+(sales[i].orderList[j].quantity*sales[i].orderList[j].purchasePrice);

					}

				}
				if(sales[i].orderDiscount > 0) {
					if(sales[i].orderDiscountType == 'percent') {
						sales[i].disc = Math.round(sales[i].total*(sales[i].orderDiscount/100));
					} else {
						sales[i].disc = Math.round(sales[i].orderDiscount);
					}
				}
				if(sales[i].isReturn) {
					sales[i].total = sales[i].total-sales[i].disc;
				} else {
					sales[i].total = sales[i].total-sales[i].disc;
				}

				total+=sales[i].total;
				purchasetotal+=sales[i].purchasetotal;
				var dt = moment(sales[i].invoiceDate).format("DD-MM-YYYY");
				var ind = getIndex($scope.sales,'date',dt);
				if(ind === -1) {
					$scope.sales.push({date: dt, disc: sales[i].disc, total: sales[i].total, purchasetotal: sales[i].purchasetotal});
				} else {
					$scope.sales[ind].disc+=sales[i].disc;
					$scope.sales[ind].total+=sales[i].total;
					$scope.sales[ind].purchasetotal+=sales[i].purchasetotal;
				}

		}
		$scope.totalSalesAmount = total;
		$scope.totalCostAmount = purchasetotal;
	};

    $scope.getReport = function(move){
		var toDate = dateConverter($scope.report.toDate);
		var fromDate = dateConverter($scope.report.fromDate);

		if(toDate===null){
			toDate = undefined;
		}
		if(fromDate===null){
			fromDate = undefined;
		}
		main_page_service.get_cost_sales_report(fromDate,toDate).then(function (response) {

			var sales;
			sales = angular.copy(response.data);

			calculateDateWiseTotal(sales);

		}).catch(function(err){
			console.log(err);
		});

    };

	$scope.printreport = function() {
		var dt = moment(new Date());
		var ts = [];

		var obj = {
			'invoices': $scope.sales,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'totalAmounts' : $scope.totalSalesAmount,
			'totalCostAmount' : $scope.totalCostAmount,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a')
				};
				obj.hospitallogo = $scope.hosDetail[0].image_url;
		print_service.print('/modules/main-page/views/cost-sales-report-print.client.view.html',obj,
		function(){
		});
	};


	$scope.init = function(){

	};

    $scope.init();

    }
]);
