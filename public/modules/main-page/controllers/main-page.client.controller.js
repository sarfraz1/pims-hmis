'use strict';

angular.module('main-page').controller('MainPageController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

                $scope.authentication = Authentication;

                // If user is signed in then redirect back home
                if (!$scope.authentication.user) $location.path('/signin');

                $scope.showExpiry = false;
                $scope.showQuantity = false;

                main_page_service.get_expired_medicines().then(function (response) {
                	$scope.expiryMedicines = response.data;
                        if ($scope.expiryMedicines.length > 0) {
                                for (var i = 0; i < $scope.expiryMedicines.length; i++) {
                                        if ($scope.expiryMedicines[i].expiryIn < 1) {
                                                $scope.expiryMedicines[i].expiryIn = 'Expired';
                                        }
                                }
                                $scope.showExpiry = true;
                        }
                }).catch(function (error) {
                        ngToast.create({
                                className: 'danger',
                                content: 'Error: Unable to retrieve Medicines near expiry.',
                                dismissButton: true
                        });
                });

                main_page_service.get_quantity_medicines().then(function (response) {
                	$scope.quantityMedicines = response.data;
                        if ($scope.quantityMedicines.length > 0) {
                                $scope.showQuantity = true;
                        }
                }).catch(function (error) {
                        ngToast.create({
                                className: 'danger',
                                content: 'Error: Unable to retrieve Medicines with low count.',
                                dismissButton: true
                        });
                });

								var start = function (){
				          $http.get('/hospital/logoname.json').then(function(response){
				          $scope.hosDetail= response.data.user;
				        });
				        };

				        start();

		$scope.printShortExpiry = function() {
			var dt = moment(new Date());
			var obj = {
				'expiryMedicines': $scope.expiryMedicines,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('hh:mm a')
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/medicine-expiry-print.client.view.html',obj,
            function(){
            });
		}
	}
]);
