'use strict';

angular.module('main-page').controller('RevenueReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll','hospitalAdmin_service',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service,$anchorScroll,hospitalAdmin_service) {

        $scope.authentication = Authentication;

		if($scope.authentication.user.roles=='opd receptionist') {
			$scope.receptionist = $scope.authentication.user.username;
		}

        $scope.revenueStreams = [];
		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];

						 var start = function (){
		           $http.get('/hospital/logoname.json').then(function(response){
		           $scope.hosDetail= response.data.user;
		         });
		         };

		         start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            } catch(error) {
                return dateinput;
            }
        };

        var getDepartments = function(){
            hospitalAdmin_service.get_last_nodes_facilities().then(function(response){
                //$scope.revenueStreams.push({CategoryName:'',Category});
                $scope.revenueStreams = response.data;
                $scope.revenueStreams.unshift('Registration');
            });
        };

		main_page_service.get_all_doctors().then(function (response) {
			$scope.doctors = response.data;
		});

		hospitalAdmin_service.getPanels().then(function (response) {
			$scope.panels = response.data;
		});

        var getAllOPDReceptionists = function() {
            $scope.processing = true;
            main_page_service.get_all_opd_receptionists().then(function (response) {
                $scope.receptionists = response.data;
                $scope.receptionists.unshift({username:'All'});
                $scope.processing = false;
            }).catch(function(err){
                console.log(err);
            });
        };

        var calculateRevenue = function() {
            $scope.report = {
                total_amount : 0,
                total_visits : $scope.bills.length
            };
            for (var i = 0; i < $scope.bills.length; i++) {
                if ($scope.bills[i].discount && $scope.bills[i].doctorCut)
                    $scope.report.total_amount = $scope.report.total_amount + (($scope.bills[i].grandTotal - $scope.bills[i].discount) - $scope.bills[i].doctorCut);
                else
                    $scope.report.total_amount = $scope.report.total_amount + $scope.bills[i].grandTotal;
            }
        };

		var filterinvoicesbytime = function(invoices) {
			var invtosend = [];
			for(var i=0;i<invoices.length;i++){
				var invdate = moment(invoices[i].actDate);
				invoices[i].Date = invdate.format("DD-MM-YYYY");
				invoices[i].invTime = invdate.format('hh:mm A');

				if(invoices[i].Date == moment($scope.fromDate).format("DD-MM-YYYY")) {
					var invtime = invdate.format('hh:mm A');
					//var invtime = '12:46 AM';
					invoices[i].invTime = invtime;

					//console.log('invoice Time: '+invoices[i].invoiceTime);
					if($scope.fromTime !== 'undefined') {
						var frT = moment($scope.fromTime, 'hh:mm A');
						var invT = moment(invtime, 'hh:mm A');
						if(invT.isBefore(frT) == true)
							continue;
					}
				}

		//		console.log(invdate.format("YYYY-MM-DD"));
		//		console.log(moment($scope.toDate).format("YYYY-MM-DD"));
				if(invoices[i].Date == moment($scope.toDate).format("DD-MM-YYYY")) {

					var invtime = invdate.format('hh:mm A');

					if($scope.toTime !== 'undefined') {
						var toT = moment($scope.toTime, 'hh:mm A');
						var invT = moment(invtime, 'hh:mm A');
						if(toT.isBefore(invT) == true)
							continue;

					}
				}
				invtosend.push(invoices[i]);
			}
			return invtosend;
		};

        var getPatientWiseRevenue = function() {
            var toDate = dateConverter($scope.toDate);
		    var fromDate = dateConverter($scope.fromDate);

			if(!$scope.receptionist) {
				$scope.receptionist = 'All';
			}
			var alltotal = 0;
			var allbalance = 0;
			var allreceived = 0;
		    main_page_service.get_revenue_patient_wise(fromDate, toDate, $scope.receptionist).then(function (response) {
		    	var reportTemp;

				if($scope.fromTime || $scope.toTime) {
					reportTemp = filterinvoicesbytime(response.data);
				} else {
					reportTemp = response.data;
				}

                for(var i=0;i<reportTemp.length;i++){
                  /*  for(var j=i+1;j<reportTemp.length;j++){
                        if(reportTemp[i].Date === reportTemp[j].Date && reportTemp[i].MRNo === reportTemp[j].MRNo){
                            for(var k=0;k<reportTemp[i].facilities.length;k++){
                                reportTemp[i].facilities[k].amount += reportTemp[j].facilities[k].amount;
                            }
							reportTemp[i].grandTotal+=reportTemp[j].grandTotal;
							reportTemp[i].receivedCash+=reportTemp[j].receivedCash;
							reportTemp[i].balance+=reportTemp[j].balance;
                            reportTemp.splice(j,1);
                            j--;
                        }
                    }*/
					alltotal += reportTemp[i].grandTotal;
					allreceived += reportTemp[i].receivedCash;
					allbalance += reportTemp[i].balance;
                }
				$scope.alltotal = alltotal;
				$scope.allreceived = allreceived;
				$scope.allbalance = allbalance;

				if(reportTemp.length>0){
					$scope.revenueStreams = [];
					for(var j =0;j<reportTemp[0].facilities.length;j++) {
						$scope.revenueStreams.push(reportTemp[0].facilities[j].facilityName);
					}
					$scope.totalAmounts = angular.copy(reportTemp[0].facilities);
				}
				$scope.referingDoctor = '';
                $scope.reports = angular.copy(reportTemp);

                for(var i=1;i<reportTemp.length;i++){
                    for(var k=0;k<reportTemp[i].facilities.length;k++){
						if($scope.totalAmounts[k].amount) {
							$scope.totalAmounts[k].amount += reportTemp[i].facilities[k].amount;
						} else {
							$scope.totalAmounts[k].amount = reportTemp[i].facilities[k].amount;
						}

                    }

                }
		    }).catch(function(err) {
		        console.log(err);
		    });
		};

		$scope.calculateTotalPanel = function() {
			var alltotal=0;
			var allreceived = 0;
			var allbalance = 0;

			for(var i=0;i<$scope.reports.length;i++){

				if($scope.selectedpanel == $scope.reports[i].panel_id) {
					alltotal += $scope.reports[i].grandTotal;
					allreceived += $scope.reports[i].receivedCash;
					allbalance += $scope.reports[i].balance;
				}
			}

			$scope.alltotal = alltotal;
			$scope.allreceived = allreceived;
			$scope.allbalance = allbalance;

			$scope.totalAmounts = angular.copy($scope.reports[0].facilities);

			for(var j=0;j<$scope.totalAmounts.length;j++) {
				$scope.totalAmounts[j].amount = 0;
			}

			for(var i=1;i<$scope.reports.length;i++){
				if($scope.reports[i].panel_id == $scope.selectedpanel) {
					for(var k=0;k<$scope.reports[i].facilities.length;k++){

						if($scope.totalAmounts[k].amount) {
							$scope.totalAmounts[k].amount += $scope.reports[i].facilities[k].amount;
						} else {
							$scope.totalAmounts[k].amount = $scope.reports[i].facilities[k].amount;
						}
					}
				}

			}
		}

		$scope.calculateTotalReferingDoctor = function() {
			var alltotal=0;
			var allreceived = 0;
			var allbalance = 0;

			for(var i=0;i<$scope.reports.length;i++){

				if($scope.referingDoctor == $scope.reports[i].referingDoctor) {
					alltotal += $scope.reports[i].grandTotal;
					allreceived += $scope.reports[i].receivedCash;
					allbalance += $scope.reports[i].balance;
				}
			}

			$scope.alltotal = alltotal;
			$scope.allreceived = allreceived;
			$scope.allbalance = allbalance;

			$scope.totalAmounts = angular.copy($scope.reports[0].facilities);

			for(var j=0;j<$scope.totalAmounts.length;j++) {
				$scope.totalAmounts[j].amount = 0;
			}

			for(var i=1;i<$scope.reports.length;i++){
				if($scope.reports[i].referingDoctor == $scope.referingDoctor) {
					for(var k=0;k<$scope.reports[i].facilities.length;k++){

						if($scope.totalAmounts[k].amount) {
							$scope.totalAmounts[k].amount += $scope.reports[i].facilities[k].amount;
						} else {
							$scope.totalAmounts[k].amount = $scope.reports[i].facilities[k].amount;
						}
					}
				}

			}
		}

		$scope.exportfile = function(){
			var filestring = '';

			//create_header
			var header = "Date,MR No,Name,Doctor Name";
			for(var i=0; i<$scope.revenueStreams.length; i++) {
				header+= ','+$scope.revenueStreams[i];
			}
			//header +=',Grand Total,Received,Balance';
			header +=',Total';
			header +='\n';
			filestring +=header;
			for(var j=0;j<$scope.reports.length;j++) {
				var line = $scope.reports[j].Date;
				line+= ','+ $scope.reports[j].MRNo;
				line+= ','+ $scope.reports[j].Name;
				line+= ','+ $scope.reports[j].referedDoctor;

				for(var k=0;k<$scope.reports[j].facilities.length; k++) {
					line+=','+$scope.reports[j].facilities[k].amount;
				}
//				line+=','+$scope.reports[j].grandTotal;
				line+=','+$scope.reports[j].receivedCash;
//				line+=','+$scope.reports[j].balance;

				filestring+=line+'\n';

			}

			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Revenue-Report' + '.csv']);
		};

		$scope.printreport = function() {
			var dt = moment(new Date());
			var obj = {
				'revenueStreams': $scope.revenueStreams,
				'reports': $scope.reports,
				'departmentdetails': $scope.departmentdetails,
				'totalAmounts' : $scope.totalAmounts,
				'allreceived' : $scope.allreceived,
				'fromdate': moment($scope.fromDate).format('DD-MM-YYYY'),
				'todate': moment($scope.toDate).format('DD-MM-YYYY'),
				'salesman': $scope.receptionist,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/revenue-report-print.client.view.html',obj,
            function(){
            });
		};

        $scope.getRevenue = function() {
            // $scope.reports = {};
		    // var toDate = dateConverter($scope.toDate);
		    // var fromDate = dateConverter($scope.fromDate);
		    // main_page_service.get_revenue(fromDate, toDate).then(function (response) {
		    // 	$scope.revenueReport = response.data;

                getPatientWiseRevenue();
                //calculateRevenue();
		    // }).catch(function(err) {
		    //     console.log(err);
		    // });
		};

        getDepartments();
		getAllOPDReceptionists();


    }
]);
