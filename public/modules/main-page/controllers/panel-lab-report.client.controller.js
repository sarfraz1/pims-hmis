"use strict";

angular.module("main-page").controller("PanelLabReportController", [
  "$scope",
  "$http",
  "$stateParams",
  "$location",
  "Authentication",
  "ngToast",
  "main_page_service",
  "print_service",
  "$anchorScroll",
  "hospitalAdmin_service",
  function(
    $scope,
    $http,
    $stateParams,
    $location,
    Authentication,
    ngToast,
    main_page_service,
    print_service,
    $anchorScroll,
    hospitalAdmin_service
  ) {
    $scope.authentication = Authentication;

    if ($scope.authentication.user.roles == "lab receptionist") {
      $scope.receptionist = $scope.authentication.user.username;
    }

    $scope.billsChanged = [];

    var start = function() {
      $http.get("/hospital/logoname.json").then(function(response) {
        $scope.hosDetail = response.data.user;
      });
    };

    start();

    var dateConverter = function(dateinput) {
      try {
        var utcDate = new Date(
          Date.UTC(
            dateinput.getFullYear(),
            dateinput.getMonth(),
            dateinput.getDate()
          )
        );
        utcDate = utcDate.toUTCString();
        return utcDate;
      } catch (error) {
        return dateinput;
      }
    };

    main_page_service.list_panel_labs().then(function(response) {
      $scope.panelLabs = response.data;
    });

    $scope.getPanelLabReport = function() {
      console.log("Apply")
      var toDate = dateConverter($scope.toDate);
      var fromDate = dateConverter($scope.fromDate);

      main_page_service
        .get_panel_lab_report(fromDate, toDate, $scope.selectedpanellab)
        .then(function(response) {
          console.log(response.data);
          $scope.panelLabTests = response.data;
        })
        .catch(function(err) {
          console.log(err);
        });
    };

    $scope.exportfile = function() {
      var filestring = "";
      //create_header
      var header = "Date,Patient MRN,Patient Name,Test Description,Panel Lab,Sample No.,Status";
      header += "\n";
      filestring += header;
      for (var j = 0; j < $scope.panelLabTests.length; j++) {
        var line = $scope.panelLabTests[j].created_date;
        line += "," + $scope.panelLabTests[j].PatientMRN;
        line += "," + $scope.panelLabTests[j].patientName;
        line += "," + $scope.panelLabTests[j].testDescription;
        line += "," + $scope.reports[j].panelLab;
        line += "," + $scope.reports[j].sampleNo;
        line += "," + $scope.reports[j].status;

        filestring += line + "\n";
      }

      var data = new Blob([filestring], { type: "text/plain;charset=utf-8" });
      return saveAs(data, ["Panel-Lab-Report" + ".csv"]);
    };

    $scope.printreport = function() {
      var dt = moment(new Date());
      var obj = {
        panelLabTests: $scope.panelLabTests,
        selectedpanellab: $scope.selectedpanellab,
        date: dt.format("DD-MM-YYYY"),
        todate: moment($scope.toDate).format("DD-MM-YYYY"),
        fromdate: moment($scope.fromDate).format("DD-MM-YYYY"),
        time: dt.format("h:mm a")
      };
      obj.hospitallogo = $scope.hosDetail[0].image_url;
      print_service.print(
        "/modules/main-page/views/panel-lab-report-print.client.view.html",
        obj,
        function() {}
      );
    };
  }
]);
