'use strict';

angular.module('main-page').controller('DoctorBillsReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
		var calculateBill = [];
		$scope.bills = [];
        $scope.totalAmount = 0;
		$scope.grandTotal = 0;
		$scope.grandConsultTotal = 0;
        var doctorId = '',
        doctorPerctage = 0;

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

		var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getAllDoctors = function(){
            main_page_service.get_all_doctors().then(function (response) {
                $scope.doctors = response.data;
				$scope.doctors.unshift({
					consultation_fee: 0,
					name: "All",
					speciality: "",
					_id: "All"
				});
            }).catch(function(err) {
                console.log(err);
            });
        };

        var calculateTotal = function() {
            var totalAmount = 0;
            for (var i = 0; i < $scope.bills.length; i++) {
               totalAmount += $scope.bills[i].docshare;
        	}
            $scope.totalAmount = totalAmount;
        };

		$scope.getReport = function() {
			if($scope.report.doctor !== 'All') {
				$scope.getServicesReport();
				$scope.showall = false;
			} else {
				$scope.getServicesReportforAll();
				$scope.showall = true;
			}

		};

        $scope.getServicesReport = function() {
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
			var docName;
			for(var i=0;i<$scope.doctors.length;i++) {
				if($scope.report.doctor == $scope.doctors[i]._id) {
					docName = $scope.doctors[i].name;
				}
			}
			$scope.docName = docName;
            main_page_service.get_doctor_payable_report($scope.report.doctor,docName, fromDate, toDate).then(function (response) {
		        $scope.bills = [];
                $scope.bills = angular.copy(response.data);
				calculateTotal();
		    }).catch(function(err) {
		        console.log(err);
		    });
		};

        $scope.getServicesReport = function() {
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
			var docName;
			for(var i=0;i<$scope.doctors.length;i++) {
				if($scope.report.doctor == $scope.doctors[i]._id) {
					docName = $scope.doctors[i].name;
				}
			}
			$scope.docName = docName;
            main_page_service.get_doctor_payable_report($scope.report.doctor,docName, fromDate, toDate).then(function (response) {
		        $scope.bills = [];
                $scope.bills = angular.copy(response.data);
				calculateTotal();
		    }).catch(function(err) {
		        console.log(err);
		    });
		};

        $scope.getServicesReportforAll = function() {
			$scope.allData = [];

			for(var i=0;i<$scope.doctors.length;i++) {
				if($scope.doctors[i]._id !== 'All') {
					$scope.getSingleDoctorData($scope.doctors[i]._id, $scope.doctors[i].name);
				}
			}
		//	$scope.docName = docName;

		};

		$scope.getSingleDoctorData = function(doctorid, doctorname) {
			var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
			main_page_service.get_doctor_payable_report(doctorid,doctorname, fromDate, toDate).then(function (response) {
			var totalAmount = 0;
			var consultTotal = 0;
            for (var i = 0; i < response.data.length; i++) {
               totalAmount += response.data[i].docshare;
			   consultTotal += response.data[i].total;
        	}
            $scope.totalAmount = totalAmount;
				$scope.allData.push({ bills: response.data,
									  name: doctorname,
									  totalamount: totalAmount,
									  consultTotal: consultTotal
									  });
			$scope.grandTotal += $scope.totalAmount;
			$scope.grandConsultTotal += consultTotal;
				//calculateTotal();
			}).catch(function(err) {
				console.log(err);
			});
		};

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

		$scope.exportfile = function(){
			var filestring = '';

			//create_header
			var header = "Date,MR No,Name,Bill Number,Handler,Service,Amount,Refund,Discount,Total,Payable";
			header +='\n';
			filestring +=header;
			for(var j=0;j<$scope.bills.length;j++) {
				var line = moment($scope.bills[j].created).format('DD-MM-YYYY');
				line+= ','+ $scope.bills[j].patientInfo.mr_number;
				line+= ','+ $scope.bills[j].patientInfo.name;
				line+= ','+ $scope.bills[j].billNumber;
				line+=','+$scope.bills[j].handlerName;
				if($scope.docName !== $scope.bills[j].description) {
					line+=','+$scope.bills[j].description;
				} else {
					line+=',Consultation';
				}
				line+=','+$scope.bills[j].fee;
				line+=','+$scope.bills[j].refund;
				line+=','+$scope.bills[j].discount;
				line+=','+$scope.bills[j].total;
				line+=','+$scope.bills[j].docshare;

				filestring+=line+'\n';

			}

			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Payable-Report-'+$scope.docName+ '.csv']);
		};

		$scope.printreport = function() {
			var dt = moment(new Date());
			var objforbills = [];
			if(!$scope.showall) {
				objforbills.push({
					bills: $scope.bills,
					name: $scope.docName,
					totalamount: $scope.totalAmount
				});
			} else {
				objforbills = $scope.allData;
			}
			var obj = {
				'bills': objforbills,
				'grandTotal': $scope.grandTotal,
				'grandConsultTotal': $scope.grandConsultTotal,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('hh:mm a')
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/payable-report-print.client.view.html',obj,
            function(){
            });
		};

        $scope.init = function() {
            getAllDoctors();
            $scope.report = {
            };
            //$scope.getReport();
        };

        $scope.init();

    }
]);
