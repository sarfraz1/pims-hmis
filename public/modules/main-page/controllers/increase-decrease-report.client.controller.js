'use strict';

angular.module('main-page').controller('IncDecReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;


        // If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');
        else
            if ($scope.authentication.user.roles === 'employee')
                $location.path('/signin');

								var start = function (){
				          $http.get('/hospital/logoname.json').then(function(response){
				          $scope.hosDetail= response.data.user;
				        });
				        };

				        start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        $scope.btn_Apply = function() {
			$scope.getReport();
        };

		var calculateTotal = function() {
			var increaseTotal = 0;
			var decreaseTotal = 0;
			for(var i=0;i<$scope.stocktransfers.length;i++) {
				for(var j=0;j<$scope.stocktransfers[i].itemsList.length;j++) {
					if($scope.stocktransfers[i].fromStoreDescription=='Stock Adjustment') {
						increaseTotal+=$scope.stocktransfers[i].itemsList[j].netValue;
					} else {
						decreaseTotal+=$scope.stocktransfers[i].itemsList[j].netValue;
					}
				}
			}
			$scope.increaseTotal = increaseTotal;
			$scope.decreaseTotal = decreaseTotal;
		};

    $scope.getReport = function(move){
		var toDate = dateConverter($scope.report.toDate);
		var fromDate = dateConverter($scope.report.fromDate);

		//Get Stock transfers
		main_page_service.get_stocktransfer_report_withvalue(fromDate,toDate).then(function (response) {
			$scope.stocktransfers = angular.copy(response.data);
			calculateTotal();
		}).catch(function(err){
			console.log(err);
		});
    };

	$scope.printreport = function() {
		var dt = moment(new Date());
		var ts = [];
		var stocktrans = [];

		for(var i=0;i<$scope.stocktransfers.length;i++) {
			for(var j=0;j<$scope.stocktransfers[i].itemsList.length;j++) {
				stocktrans.push({ transferNumber: $scope.stocktransfers[i].transferNumber,
						  date: moment($scope.stocktransfers[i].date).format('DD-MM-YYYY'),
						  fromStore: $scope.stocktransfers[i].fromStoreDescription,
						  toStore: $scope.stocktransfers[i].toStoreDescription,
						  transferedBy: $scope.stocktransfers[i].transferedBy,
						  id: $scope.stocktransfers[i].itemsList[j].code,
						  description: $scope.stocktransfers[i].itemsList[j].description,
						  quantity: $scope.stocktransfers[i].itemsList[j].quantity,
						  netValue: $scope.stocktransfers[i].itemsList[j].netValue

				});
			}
		}

		var obj = {
			'stocktransfer': stocktrans,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'increaseTotal' : $scope.increaseTotal,
			'decreaseTotal' : $scope.decreaseTotal,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a')
		};
		obj.hospitallogo = $scope.hosDetail[0].image_url;
		print_service.print('/modules/main-page/views/incdec-report-print.client.view.html',obj,
		function(){
		});
	};

	$scope.init = function(){

		$scope.report = {
			'pharmacist' : 'All'
		};
	};

    $scope.init();

    }
]);
