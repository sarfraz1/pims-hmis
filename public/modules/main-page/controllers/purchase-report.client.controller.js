'use strict';

angular.module('main-page').controller('PurchaseReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

		// If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');

						var start = function (){
		 				 $http.get('/hospital/logoname.json').then(function(response){
		 				 $scope.hosDetail= response.data.user;
		 			 });
		 			 };

		 			 start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };


        $scope.btn_Apply = function() {

            $scope.getReport();

        };

	var calculateTotal = function(purchase) {
		var total = 0;
		for (var i=0;i<purchase.length;i++) {
			total+= purchase[i].netValue;
		}
		$scope.totalPurchaseAmount = total;
	};

	var getIndex = function(array, prop, value) {
		var index = -1;
		for (var x = 0; x < array.length; x++) {
			if (array[x][prop] === value) {
				index = x;
			}
		}
		return index;
	};

    $scope.getReport = function(move){
		var toDate = dateConverter($scope.report.toDate);
		var fromDate = dateConverter($scope.report.fromDate);

		if(toDate===null){
			toDate = undefined;
		}
		if(fromDate===null){
			fromDate = undefined;
		}

		if(fromDate && toDate) {
			main_page_service.get_purchase_report(fromDate,toDate,$scope.currentPage).then(function (response) {

				$scope.purchase = angular.copy(response.data);


				calculateTotal($scope.purchase);

			}).catch(function(err){
				console.log(err);
			});
		}

    };

	$scope.printreport = function() {
		var dt = moment(new Date());

		var obj = {
			'purchase': $scope.purchase,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'totalPurchaseAmount' : $scope.totalPurchaseAmount,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a'),
			'hidedetails': $scope.hidedetails
		};
		obj.hospitallogo = $scope.hosDetail[0].image_url;
		print_service.print('/modules/main-page/views/purchase-report-print.client.view.html',obj,
		function(){
		});
	};

    }
]);
