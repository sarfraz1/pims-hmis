'use strict';

angular.module('main-page').controller('NarcoticsSalesReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];

		// If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');
        else
            if ($scope.authentication.user.roles === 'employee')
                $location.path('/signin');

        $scope.currentPage = 0;

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getAllReceptionists = function(){
            main_page_service.get_all_sales_receptionists().then(function (response) {
                //$scope.sales = response.data;
                $scope.pharmacists = response.data;
                $scope.pharmacists.unshift({username:'All'});
            }).catch(function(err){
                console.log(err);
            });
        };

        /////////////////////Form Reset////////////////////////////
        $scope.fromReceipt = "";
        $scope.toReceipt = "";

        $scope.From_Reset = function(){
            $scope.fromReceipt = "";
            $scope.toReceipt = "";
            $scope.report.fromDate = undefined;
            $scope.report.toDate = undefined;
			$scope.report.fromTime = undefined;
			$scope.report.toDate = undefined;
         };

        $scope.btn_Apply = function() {
            $scope.getReport();
        };


       $scope.getReport = function(move){
            var toDate = dateConverter($scope.report.toDate);
            var fromDate = dateConverter($scope.report.fromDate);

            if(toDate===null){
                toDate = undefined;
            }
            if(fromDate===null){
                fromDate = undefined;
            }

			main_page_service.get_sales_report_narcotics($scope.report.pharmacist,fromDate,toDate,$scope.report.fromTime,$scope.report.toTime,undefined).then(function (response) {
                if(response.data.length === 0){
                    $scope.sales = [];

                } else {
					$scope.sales = response.data;
                }
            }).catch(function(err){
                console.log(err);
            });

       };

		$scope.printReport = function() {
			var dt = moment(new Date());

			var data = angular.copy($scope.sales);

			for(var i=0;i<data.length;i++) {
				data[i].invoiceDate = moment(data[i].invoiceDate).format('DD-MM-YYYY');
				data[i].totalvalue = (data[i].quantity*data[i].price).toFixed(2);
			}

			var obj = {
				'sales': data,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a'),
				'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
				'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
				'salesman': $scope.report.pharmacist
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/narcotics-report-print.client.view.html',obj,
			function(){
			});
		};

        $scope.init = function(){
            getAllReceptionists();
            $scope.report = {
                'pharmacist' : 'All'
            };
        };

        $scope.init();
    }
]);
