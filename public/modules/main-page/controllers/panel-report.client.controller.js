'use strict';

angular.module('main-page').controller('PanelReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll','hospitalAdmin_service',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service,$anchorScroll,hospitalAdmin_service) {

        $scope.authentication = Authentication;

		if($scope.authentication.user.roles=='opd receptionist') {
			$scope.receptionist = $scope.authentication.user.username;
		}

		$scope.billsChanged = [];

		var start = function (){
			$http.get('/hospital/logoname.json').then(function(response){
			$scope.hosDetail= response.data.user;
		});
		};

		start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            } catch(error) {
                return dateinput;
            }
        };

		hospitalAdmin_service.getPanels().then(function (response) {
			$scope.panels = response.data;
		});


       $scope.getPanelReport = function() {
            var toDate = dateConverter($scope.toDate);
		    var fromDate = dateConverter($scope.fromDate);


		    main_page_service.get_panel_report(fromDate, toDate, $scope.selectedpanel).then(function (response) {

				$scope.total = response.data.total;
				$scope.totalPanelClaim = response.data.totalPanelClaim;
				$scope.reports = response.data.bills;



		    }).catch(function(err) {
		        console.log(err);
		    });
		};

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
			  if (array[x][prop] === value) {
				index = x;
			  }
			}
			return index;
		};

		$scope.addToChanged = function(bill) {
			var ind = getIndex($scope.billsChanged, 'billNumber', bill.billNumber);
			if(ind == -1) {
				$scope.billsChanged.push({billNumber: bill.billNumber,
				panelPaymentReceived: bill.panelPaymentReceived});
			} else {
				$scope.billsChanged[ind].panelPaymentReceived = bill.panelPaymentReceived;
			}
		}

		$scope.updateBills = function() {
			main_page_service.update_panel_bills_status({bills: $scope.billsChanged}).then(function(response) {
				if(response.data.message == 'success') {
					$scope.getPanelReport();
					ngToast.create({
						className: 'success',
						content: 'Bill(s) updated Successfully'
					});
				} else {
					ngToast.create({
						className: 'danger',
						content: 'Error(s) updating bills'
					});
				}
			})
		}

		$scope.exportfile = function(){
			var filestring = '';

			//create_header
			var header = "Date,MR No,Name,Doctor Name";
			for(var i=0; i<$scope.revenueStreams.length; i++) {
				header+= ','+$scope.revenueStreams[i];
			}
			//header +=',Grand Total,Received,Balance';
			header +=',Total';
			header +='\n';
			filestring +=header;
			for(var j=0;j<$scope.reports.length;j++) {
				var line = $scope.reports[j].Date;
				line+= ','+ $scope.reports[j].MRNo;
				line+= ','+ $scope.reports[j].Name;
				line+= ','+ $scope.reports[j].referedDoctor;

				for(var k=0;k<$scope.reports[j].facilities.length; k++) {
					line+=','+$scope.reports[j].facilities[k].amount;
				}
//				line+=','+$scope.reports[j].grandTotal;
				line+=','+$scope.reports[j].receivedCash;
//				line+=','+$scope.reports[j].balance;

				filestring+=line+'\n';

			}

			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Revenue-Report' + '.csv']);
		};

		$scope.printreport = function() {
			var dt = moment(new Date());
			var obj = {
				'reports': $scope.reports,
				'total': $scope.total,
				'totalPanelClaim': $scope.totalPanelClaim,
				'selectedpanel': $scope.selectedpanel,
				'date': dt.format('DD-MM-YYYY'),
				'todate': moment($scope.toDate).format('DD-MM-YYYY'),
				'fromdate': moment($scope.fromDate).format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/panel-report-print.client.view.html',obj,
            function(){
            });
		};



    }
]);
