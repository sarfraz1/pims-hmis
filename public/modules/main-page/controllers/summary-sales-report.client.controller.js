'use strict';

angular.module('main-page').controller('SummarySalesReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll',
	function($scope, $stateParams, $http, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

		if($scope.authentication.user.roles=='pharmacy salesman') {
			$scope.report = {};
			$scope.report.pharmacist = $scope.authentication.user.displayName;
		}

		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];

		// If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');


        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getAllReceptionists = function(){
            main_page_service.get_all_sales_receptionists().then(function (response) {
                $scope.pharmacists = response.data;
                $scope.pharmacists.unshift({username:'All'});
            }).catch(function(err){
                console.log(err);
            });
        };


		var filterinvoicesbytime = function(invoices) {
			var invtosend = [];
			for(var i=0;i<invoices.length;i++){
				if(invoices[i].invoiceTime.length<8) {
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
				var invdate = invoices[i].invoiceTime;

				if(moment(invoices[i].invoiceDate).format("DD-MM-YYYY") == moment($scope.report.fromDate).format("DD-MM-YYYY")) {
					var invtime = moment(invdate, 'hh:mm A');
					if($scope.fromTime !== 'undefined') {
						var frT = moment($scope.fromTime, 'hh:mm A');
						if(invtime.isBefore(frT) == true)
							continue;
					}
				}

				if(moment(invoices[i].invoiceDate).format("DD-MM-YYYY") == moment($scope.report.toDate).format("DD-MM-YYYY")) {

					var invtime = moment(invdate, 'hh:mm A');

					if($scope.toTime !== 'undefined') {
						var toT = moment($scope.toTime, 'hh:mm A');
						if(toT.isBefore(invtime) == true)
							continue;
					}
				}
				invtosend.push(invoices[i]);
			}
			return invtosend;
		};

        $scope.btn_Apply = function() {

            $scope.getReport();

        };

	var calculateTotal = function(sales) {
		var total = 0;
		var creditotal = 0;
		$scope.creditsales = [];
		$scope.sales = [];
		for (var i=0;i<sales.length;i++) {
			sales[i].disc = 0;
			sales[i].total = 0;
			if(!sales[i].issalemrno && !sales[i].issalecredit) {
				for(var j=0;j<sales[i].orderList.length;j++) {
					var disc = 0;

					if(sales[i].isReturn) {
						sales[i].total= sales[i].total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					} else {
						sales[i].total= sales[i].total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					}

				}
				if(sales[i].orderDiscount > 0) {
					if(sales[i].orderDiscountType == 'percent') {
						sales[i].disc = Math.round(sales[i].total*(sales[i].orderDiscount/100));
					} else {
						sales[i].disc = Math.round(sales[i].orderDiscount);
					}
				}
				if(sales[i].isReturn) {
					sales[i].total = sales[i].total-sales[i].disc;
				} else {
					sales[i].total = sales[i].total-sales[i].disc;
				}
				total+=sales[i].total;
				$scope.sales.push(sales[i]);
			} else {

				for(var j=0;j<sales[i].orderList.length;j++) {
					var disc = 0;

					if(sales[i].isReturn) {
						sales[i].total= sales[i].total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					} else {
						sales[i].total= sales[i].total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					}

				}
				if(sales[i].orderDiscount > 0) {
					if(sales[i].orderDiscountType == 'percent') {
						sales[i].disc = Math.round(sales[i].total*(sales[i].orderDiscount/100));
					} else {
						sales[i].disc = Math.round(sales[i].orderDiscount);
					}
				}

				if(sales[i].isReturn) {
					sales[i].total = sales[i].total-sales[i].disc;
				} else {
					sales[i].total = sales[i].total-sales[i].disc;
				}

				creditotal+=sales[i].total;
				$scope.creditsales.push(sales[i]);
			}
		}
		$scope.totalCreditSalesAmount = Math.round(creditotal);
		$scope.totalSalesAmount = total;
	};

	var getIndex = function(array, prop, value) {
		var index = -1;
		for (var x = 0; x < array.length; x++) {
			if (array[x][prop] === value) {
				index = x;
			}
		}
		return index;
	};

	var calculateDateWiseTotal = function(sales) {
		var total = 0;
		var creditotal = 0;
		$scope.creditsales = [];
		$scope.sales = [];
		for (var i=0;i<sales.length;i++) {
			sales[i].disc = 0;
			sales[i].total = 0;
			if(!sales[i].issalemrno && !sales[i].issalecredit) {
				for(var j=0;j<sales[i].orderList.length;j++) {
					var disc = 0;

					if(sales[i].isReturn) {
						sales[i].total= sales[i].total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					} else {
						sales[i].total= sales[i].total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					}

				}
				if(sales[i].orderDiscount > 0) {
					if(sales[i].orderDiscountType == 'percent') {
						sales[i].disc = Math.round(sales[i].total*(sales[i].orderDiscount/100));
					} else {
						sales[i].disc = Math.round(sales[i].orderDiscount);
					}
				}
				if(sales[i].isReturn) {
					sales[i].total = sales[i].total-sales[i].disc;
				} else {
					sales[i].total = sales[i].total-sales[i].disc;
				}

				total+=sales[i].total;
				var dt = moment(sales[i].invoiceDate).format("DD-MM-YYYY");
				var ind = getIndex($scope.sales,'date',dt);
				if(ind === -1) {
					$scope.sales.push({date: dt, disc: sales[i].disc, total: sales[i].total});
				} else {
					$scope.sales[ind].disc+=sales[i].disc;
					$scope.sales[ind].total+=sales[i].total;
				}

			} else {

				for(var j=0;j<sales[i].orderList.length;j++) {
					var disc = 0;

					if(sales[i].isReturn) {
						sales[i].total= sales[i].total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					} else {
						sales[i].total= sales[i].total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price);
					}

				}
				if(sales[i].orderDiscount > 0) {
					if(sales[i].orderDiscountType == 'percent') {
						sales[i].disc = Math.round(sales[i].total*(sales[i].orderDiscount/100));
					} else {
						sales[i].disc = Math.round(sales[i].orderDiscount);
					}
				}
				if(sales[i].isReturn) {
					sales[i].total = sales[i].total-sales[i].disc;
				} else {
					sales[i].total = sales[i].total-sales[i].disc;
				}
				creditotal+=sales[i].total;
				var dt = moment(sales[i].invoiceDate).format("DD-MM-YYYY");
				var ind = getIndex($scope.creditsales,'date',dt);
				if(ind === -1) {
					$scope.creditsales.push({date: dt, disc: sales[i].disc, total: sales[i].total});
				} else {
					$scope.creditsales[ind].disc+=sales[i].disc;
					$scope.creditsales[ind].total+=sales[i].total;
				}
			}
		}
		$scope.totalCreditSalesAmount = Math.round(creditotal);
		$scope.totalSalesAmount = total;
	};

    $scope.getReport = function(move){
		var toDate = dateConverter($scope.report.toDate);
		var fromDate = dateConverter($scope.report.fromDate);

		if(toDate===null){
			toDate = undefined;
		}
		if(fromDate===null){
			fromDate = undefined;
		}
		main_page_service.get_sales_report($scope.report.pharmacist,fromDate,toDate,$scope.report.fromTime,$scope.report.toTime,$scope.currentPage).then(function (response) {
			$scope.fromTime = $scope.report.fromTime;
			$scope.toTime = $scope.report.toTime;
			var sales;
			if($scope.fromTime || $scope.toTime) {
				sales = filterinvoicesbytime(response.data);
			} else {
				sales = angular.copy(response.data);
			}

			if(!$scope.hidedetails) {
				calculateTotal(sales);
			} else {
				calculateDateWiseTotal(sales);
			}
		}).catch(function(err){
			console.log(err);
		});

		//Get Stock transfers
		main_page_service.get_stocktransfer_report(fromDate,toDate).then(function (response) {
			$scope.stocktransfers = angular.copy(response.data);

		}).catch(function(err){
			console.log(err);
		});
    };

	$scope.printreport = function() {
		var dt = moment(new Date());
		var ts = [];
		var stocktrans = [];


		for(var i=0;i<$scope.stocktransfers.length;i++) {
			for(var j=0;j<$scope.stocktransfers[i].itemsList.length;j++) {
				stocktrans.push({ transferNumber: $scope.stocktransfers[i].transferNumber,
						  date: moment($scope.stocktransfers[i].date).format('DD-MM-YYYY'),
						  fromStore: $scope.stocktransfers[i].fromStoreDescription,
						  toStore: $scope.stocktransfers[i].toStoreDescription,
						  transferedBy: $scope.stocktransfers[i].transferedBy,
						  id: $scope.stocktransfers[i].itemsList[j].code,
						  description: $scope.stocktransfers[i].itemsList[j].description,
						  quantity: $scope.stocktransfers[i].itemsList[j].quantity

				});
			}
		}

		var obj = {
			'invoices': $scope.sales,
			'creditinvoices': $scope.creditsales,
			'stocktransfer': stocktrans,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'salesman': $scope.report.pharmacist,
			'totalAmounts' : $scope.totalSalesAmount,
			'totalCreditSalesAmount' : $scope.totalCreditSalesAmount,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a'),
			'hidedetails': $scope.hidedetails
		};
		obj.hospitallogo = $scope.hosDetail[0].image_url;
		print_service.print('/modules/main-page/views/summary-sales-report-print.client.view.html',obj,
		function(){
		});
	};

	$scope.hidedetailsChanged = function() {
		$scope.sales = [];
		$scope.creditsales = [];
		$scope.totalSalesAmount = 0;
		$scope.totalCreditSalesAmount = 0;

	};

	$scope.init = function(){
		getAllReceptionists();

		$scope.report = {
			'pharmacist' : 'All'
		};
	};
		if($scope.authentication.user.roles!='pharmacy salesman') {
			$scope.init();
		}
    }
]);
