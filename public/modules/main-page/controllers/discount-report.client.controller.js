'use strict';

angular.module('main-page').controller('DiscountReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', '$anchorScroll','hospitalAdmin_service',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, $anchorScroll,hospitalAdmin_service) {

        $scope.authentication = Authentication;
		
        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            } catch(error) {
                return dateinput;
            }
        };
		
        var calculateTotal = function() {
            var totalAmount = 0;
            for (var i = 0; i < $scope.reports.length; i++) {
               totalAmount += $scope.reports[i].discount;
        	}
            $scope.totalAmount = totalAmount;
        };
		
        var getAllOPDReceptionists = function() {
            $scope.processing = true;
            main_page_service.get_all_opd_receptionists().then(function (response) {
                $scope.receptionists = response.data;
                $scope.receptionists.unshift({username:'All'});
                $scope.processing = false;
            }).catch(function(err){
                console.log(err);
            }); 
        };
		
        var getPatientWiseDiscount = function() {
            var toDate = dateConverter($scope.toDate);
		    var fromDate = dateConverter($scope.fromDate);
			if(!$scope.receptionist) {
				$scope.receptionist = 'All';
			}

		    main_page_service.get_discount_patient_wise(fromDate, toDate, $scope.receptionist).then(function (response) {
		    	var reportTemp;
				reportTemp = response.data;
				
                $scope.reports = angular.copy(reportTemp);
				calculateTotal();

		    }).catch(function(err) {
		        console.log(err);
		    }); 
		};   

		$scope.exportfile = function(){
			var filestring = '';
			
			//create_header
			var header = "Date,MR No,Name,Bill Number,Handler,Service,Total,Discount";
			header +='\n';
			filestring +=header;
			for(var j=0;j<$scope.reports.length;j++) {
				var line = moment($scope.reports[j].created).format('DD-MM-YYYY');
				line+= ','+ $scope.reports[j].patientInfo.mr_number;
				line+= ','+ $scope.reports[j].patientInfo.name;
				line+= ','+ $scope.reports[j].billNumber;
				line+=','+$scope.reports[j].handlerName;
				line+=','+$scope.reports[j].description;
				line+=','+$scope.reports[j].fee;
				line+=','+$scope.reports[j].discount;
				filestring+=line+'\n';
			}
			
			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Discount-Report' + '.csv']);
		};
        
		$scope.getDiscounts = function() {
 
                
                getPatientWiseDiscount();
                //calculateRevenue();
		    // }).catch(function(err) {
		    //     console.log(err);
		    // }); 
		};     

		getAllOPDReceptionists();

		     
    }
]);