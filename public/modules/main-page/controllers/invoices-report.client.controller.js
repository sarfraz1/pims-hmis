'use strict';

angular.module('main-page').controller('InvoicesReportController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', '$anchorScroll',
    function($scope, $stateParams, $location, Authentication, ngToast, main_page_service, $anchorScroll) {

        $scope.authentication = Authentication;
        $scope.invoices = [];
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) 
            $location.path('/signin');

		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];		
		
        var pageNumber = 0;
        $scope.disableNext = false;
        $scope.disablePrevious = false;
        $scope.processing = false;
        $scope.none = false;
        
        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var calculate_subtotal = function() {
        	for (var i = 0; i < $scope.invoices.length; i++) {
        		var subtotal = 0;
        		for (var j = 0; j < $scope.invoices[i].orderList.length; j++) {
        			subtotal = subtotal + $scope.invoices[i].orderList[j].price * $scope.invoices[i].orderList[j].quantity;
        		}
        		$scope.invoices[i].subtotal = subtotal;
        	}
        };

        var calculate_total = function() {
        	for (var i = 0; i < $scope.invoices.length; i++) {
        		var total = 0;
                
                $scope.invoices[i].invoiceDate = moment($scope.invoices[i].invoiceDate).format('MM/DD/YYYY');
        		
                if ($scope.invoices[i].orderDiscountType === 'percent') {
        			total = $scope.invoices[i].subtotal - (($scope.invoices[i].orderDiscount / 100) * $scope.invoices[i].subtotal);
        		} else {
        			total = $scope.invoices[i].subtotal - $scope.invoices[i].orderDiscount;
        		}
        		$scope.invoices[i].total = total;
        	}
        }

        var calculate_subtotal_report = function(ReportInvoices) {
            for (var i = 0; i < ReportInvoices.length; i++) {
                var subtotal = 0;
                for (var j = 0; j < ReportInvoices[i].orderList.length; j++) {
                    subtotal = subtotal + ReportInvoices[i].orderList[j].price * ReportInvoices[i].orderList[j].quantity;
                }
                ReportInvoices[i].subtotal = subtotal;
            }
            return ReportInvoices;
        };

        var calculate_total_report = function(ReportInvoices) {
			
            for (var i = 0; i < ReportInvoices.length; i++) {
                var total = 0;
                if (ReportInvoices[i].orderDiscountType === 'percent') {
                    total = ReportInvoices[i].subtotal - ((ReportInvoices[i].orderDiscount / 100) * ReportInvoices[i].subtotal);
                } else {
                    total = ReportInvoices[i].subtotal - ReportInvoices[i].orderDiscount;
                }
                ReportInvoices[i].total = total;

            }
			
            return ReportInvoices;
        };

        var getAllReceptionists = function() {
            $scope.processing = true;
            main_page_service.get_all_sales_receptionists().then(function (response) {
                $scope.pharmacists = response.data;
                $scope.pharmacists.unshift({username:'All'});
                $scope.processing = false;
            }).catch(function(err){
                console.log(err);
            }); 
        };

        var handleResponse = function(response) {
            $scope.invoices = [];
            $scope.invoices = angular.copy(response.data);
            calculate_subtotal();
            calculate_total();
            if (response.data.length === 0)
                $scope.none = true;
            else
                $scope.none = false;
	        if (response.data.length < 10)
	        	$scope.disableNext = true;
	        else
	        	$scope.disableNext = false;
	        if (pageNumber === 0)
	        	$scope.disablePrevious = true;
	        else
	        	$scope.disablePrevious = false;
            $scope.processing = false;
        };

        var handleError = function(err) {
            $scope.processing = false;
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Invoices.',
                dismissButton: true
            });
        };

        var getAllInvoices = function() {
            $scope.processing = true;
            main_page_service.get_invoices_filtered('All', undefined, undefined,undefined,undefined, 0).then(function (response) {
            	handleResponse(response);
            }).catch(function(err){
                handleError(err);
            });

			main_page_service.get_invoices_filtered_total('All', undefined, undefined,undefined,undefined, 0).then(function (response) {
            	$scope.totalsale = Math.round(response.data.totalsale);
				$scope.creditsale = Math.round(response.data.creditsale);
				$scope.returnsale = Math.round(response.data.returnsale);
            }).catch(function(err){
                handleError(err);
            }); 
        };

        var getInvoicesReport = function() {
            var toDate = undefined;
            var fromDate = undefined;
            if($scope.report.toDate){
                toDate = dateConverter($scope.report.toDate);
            }
            if($scope.report.fromDate){
                fromDate = dateConverter($scope.report.fromDate);
            }
            main_page_service.get_invoices_filtered($scope.report.pharmacist, fromDate, toDate, undefined).then(function (response) {
                //console.log(response.data);
                $scope.reportInvoices = calculate_subtotal_report(response.data);
                $scope.reportInvoices = calculate_total_report($scope.reportInvoices);
                console.log($scope.reportInvoices);
            }).catch(function(err) {
                handleError(err);
            });
        };

		$scope.getFilteredInvoice = function() {
            $scope.processing = true;
			pageNumber = 0;
		    var toDate = undefined;
		    var fromDate = undefined;
            if($scope.report.toDate){
                toDate = dateConverter($scope.report.toDate);
            }
            if($scope.report.fromDate){
                fromDate = dateConverter($scope.report.fromDate);
            }
		    main_page_service.get_invoices_filtered($scope.report.pharmacist, fromDate, toDate, $scope.report.fromTime, $scope.report.toTime, pageNumber).then(function (response) {
            	handleResponse(response);
		    }).catch(function(err) {
                handleError(err);
		    });
			
			main_page_service.get_invoices_filtered_total($scope.report.pharmacist, fromDate, toDate, $scope.report.fromTime, $scope.report.toTime, pageNumber).then(function (response) {
            	$scope.totalsale = Math.round(response.data.totalsale);
				$scope.creditsale = Math.round(response.data.creditsale);
				$scope.returnsale = Math.round(response.data.returnsale);
		    }).catch(function(err) {
                handleError(err);
		    });
			
         //   getInvoicesReport();
		};

		$scope.getFilteredInvoicePagination = function(check) {
			if (check === 'next') {
				pageNumber += 1;
			} else if (check === 'previous') {
				pageNumber -= 1;
			}
            $scope.processing = true;
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
		    main_page_service.get_invoices_filtered($scope.report.pharmacist, fromDate, toDate, $scope.report.fromTime, $scope.report.toTime, pageNumber).then(function (response) {
            	handleResponse(response);
		    }).catch(function(err) {
                handleError(err);
		    }); 
		};
        /////////////////////Form Reset////////////////////////////
        $scope.From_Reset = function(){
            $scope.fromReceipt = "";
            $scope.toReceipt = "";
            $scope.report.fromDate = "";
            $scope.report.toDate = "";
            $scope.getFilteredInvoice();
         };
        ///////////////////////////////////////////////////////////
        ///////////////Apply Button Handler////////////////////////
        $scope.btn_Apply = function() {

            if($scope.fromReceipt!="" || $scope.toReceipt!="")
            {
            
                $scope.report.fromDate = "";
                $scope.report.toDate = "";
                $scope.invoice_Receipt_Range();
            }
            else
            {
                 $scope.getFilteredInvoice();
            }

        };
        ////////////////////////////////////////////////////////////
        
        ////////////////Get Invoice in Receipt Range////////////////
        $scope.toReceipt ='';
        $scope.fromReceipt ='';
        
        $scope.invoice_Receipt_Range = function() {
            $scope.processing = true;
              
            main_page_service.get_Receipt_Range($scope.report.pharmacist, $scope.fromReceipt, $scope.toReceipt).then(function (response) {
        
                handleResponse(response);
		
            }).catch(function(err) {
                handleError(err);
            }); 

           
		};
        
        ////////////////////////////////////////////////////////////

        $scope.init = function() {

            
            getAllReceptionists();
            getAllInvoices();
            
            $scope.report = {
                'pharmacist' : 'All'
            };
            getInvoicesReport();
        };

        $scope.init();
        
    }
]);