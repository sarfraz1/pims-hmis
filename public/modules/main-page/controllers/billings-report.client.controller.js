'use strict';

angular.module('main-page').controller('BillingsReportController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', '$anchorScroll',
    function($scope, $stateParams, $location, Authentication, ngToast, main_page_service, $anchorScroll) {

        $scope.authentication = Authentication;

		if($scope.authentication.user.roles=='opd receptionist') {
			$scope.report = {};
			$scope.report.receptionist = $scope.authentication.user.username;
		}
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) 
            $location.path('/signin');

        var pageNumber = 0;
        $scope.disableNext = false;
        $scope.disablePrevious = false;
        $scope.processing = false;
        $scope.none = false;
        
        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getAllOPDReceptionists = function() {
            $scope.processing = true;
            main_page_service.get_all_opd_receptionists().then(function (response) {
                $scope.receptionists = response.data;
                $scope.receptionists.unshift({username:'All'});
                $scope.processing = false;
            }).catch(function(err){
                console.log(err);
            }); 
        };

        var handleResponse = function(response) {
            $scope.bills = response.data;
            // calculate_subtotal();
            // calculate_total();
            if (response.data.length === 0)
                $scope.none = true;
            else
                $scope.none = false;
	        if (response.data.length < 10)
	        	$scope.disableNext = true;
	        else
	        	$scope.disableNext = false;
	        if (pageNumber === 0)
	        	$scope.disablePrevious = true;
	        else
	        	$scope.disablePrevious = false;
            $scope.processing = false;
        };

        var handleError = function(err) {
            console.log(err);
            $scope.processing = false;
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Bills.',
                dismissButton: true
            });
        };

        var getAllBills = function() {
            $scope.processing = true;
            main_page_service.get_bills_filtered('All', undefined, undefined, 0).then(function (response) {
            	handleResponse(response);
            }).catch(function(err){
                handleError(err);
            }); 
        };

        var getAllBillsReport = function() {
            var toDate = undefined;
            var fromDate = undefined;
            if($scope.report.toDate){
                toDate = dateConverter($scope.report.toDate);
            }
            if($scope.report.fromDate){
                fromDate = dateConverter($scope.report.fromDate);
            }
            main_page_service.get_bills_filtered($scope.report.receptionist, fromDate, toDate, undefined).then(function (response) {
                $scope.reportExport = response.data;
                console.log($scope.reportExport);
            }).catch(function(err) {
                handleError(err);
            }); 
        };

		$scope.getFilteredBills = function() {
            $scope.processing = true;
			pageNumber = 0;
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
		    main_page_service.get_bills_filtered($scope.report.receptionist, fromDate, toDate, pageNumber).then(function (response) {
            	handleResponse(response);
		    }).catch(function(err) {
                handleError(err);
		    }); 
		};

		$scope.getFilteredBillsPagination = function(check) {
			if (check === 'next') {
				pageNumber += 1;
			} else if (check === 'previous') {
				pageNumber -= 1;
			}
            $scope.processing = true;
		    var toDate = dateConverter($scope.report.toDate);
		    var fromDate = dateConverter($scope.report.fromDate);
		    main_page_service.get_bills_filtered($scope.report.receptionist, fromDate, toDate, pageNumber).then(function (response) {
            	handleResponse(response);
		    }).catch(function(err) {
                handleError(err);
		    }); 
		};

        $scope.init = function() {
            getAllOPDReceptionists();
            //getAllBills();
			if($scope.authentication.user.roles !== 'opd receptionist') {
				$scope.report = {
					'receptionist' : 'All'
				};
			}
            //getAllBillsReport();
        };

        $scope.init();
        
    }
]);