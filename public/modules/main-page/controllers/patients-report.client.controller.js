'use strict';

angular.module('main-page').controller('PatientsReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll','opd_service',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service,$anchorScroll,opd_service) {

        $scope.authentication = Authentication;

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            } catch(error) {
                return dateinput;
            }
        };

        $scope.areas = [];

        var getAreas = function() {
            opd_service.get_areas().then(function (response) {
                $scope.areas = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Areas.',
                    dismissButton: true
                });
            });
        };

		getAreas();

       $scope.getPatientsReport = function() {
            var toDate = dateConverter($scope.toDate);
		    var fromDate = dateConverter($scope.fromDate);

		    main_page_service.get_patient_report(fromDate, toDate, $scope.selectedarea).then(function (response) {
				$scope.reports = response.data;

				for(var i=0; i<$scope.reports.length; i++){
					var str = moment($scope.reports[i].dob).format('YYYY-MM-DD');
					$scope.reports[i].dob = moment().diff(str, 'years');
				}

		    }).catch(function(err) {
		        console.log(err);
		    });
		};

		$scope.exportfile = function(){
			var filestring = '';

			//create_header
			var header = "Date,MR No,Name,Doctor Name";
			for(var i=0; i<$scope.revenueStreams.length; i++) {
				header+= ','+$scope.revenueStreams[i];
			}
			//header +=',Grand Total,Received,Balance';
			header +=',Total';
			header +='\n';
			filestring +=header;
			for(var j=0;j<$scope.reports.length;j++) {
				var line = $scope.reports[j].Date;
				line+= ','+ $scope.reports[j].MRNo;
				line+= ','+ $scope.reports[j].Name;
				line+= ','+ $scope.reports[j].referedDoctor;

				for(var k=0;k<$scope.reports[j].facilities.length; k++) {
					line+=','+$scope.reports[j].facilities[k].amount;
				}
//				line+=','+$scope.reports[j].grandTotal;
				line+=','+$scope.reports[j].receivedCash;
//				line+=','+$scope.reports[j].balance;

				filestring+=line+'\n';

			}

			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Revenue-Report' + '.csv']);
		};

		$scope.printreport = function() {
			var dt = moment(new Date());
			var obj = {

				'reports': $scope.reports,
				'area': $scope.selectedarea,
				'fromdate': moment($scope.fromDate).format("DD-MM-YYYY"),
				'todate': moment($scope.toDate).format("DD-MM-YYYY"),
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/main-page/views/patients-report-print.client.view.html',obj,
            function(){
            });
		};



    }
]);
