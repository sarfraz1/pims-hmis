'use strict';

angular.module('main-page').controller('SalesReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'main_page_service', 'print_service', '$anchorScroll', 'hospitalAdmin_service',
	function($scope, $http, $stateParams, $location, Authentication, ngToast, main_page_service, print_service, $anchorScroll, hospitalAdmin_service) {

        $scope.authentication = Authentication;
        $scope.totalSalesAmount = 0;

       $scope.report = {};
       $scope.stores = [];

		if($scope.authentication.user.roles=='pharmacy salesman') {
			
			$scope.report.pharmacist = $scope.authentication.user.displayName;
		}

		$scope.times = ["12:00 AM", "01:00 AM", "02:00 AM", "03:00 AM",
						"04:00 AM" , "05:00 AM", "06:00 AM", "07:00 AM",
						"08:00 AM" , "09:00 AM", "10:00 AM", "11:00 AM",
						"12:00 PM", "01:00 PM", "02:00 PM" , "03:00 PM" ,
						"04:00 PM" , "05:00 PM", "06:00 PM" , "07:00 PM" ,
						"08:00 PM" , "09:00 PM", "10:00 PM", "11:00 PM"
						 ];
        // If user is signed in then redirect back home
        if (!$scope.authentication.user)
            $location.path('/signin');
        else
            if ($scope.authentication.user.roles === 'employee')
                $location.path('/signin');

								var start = function (){
				          $http.get('/hospital/logoname.json').then(function(response){
				          $scope.hosDetail= response.data.user;
				        });
				        };

				        start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getAllReceptionists = function(){
            main_page_service.get_all_sales_receptionists().then(function (response) {
                $scope.pharmacists = response.data;
                $scope.pharmacists.unshift({username:'All'});
            }).catch(function(err){
                console.log(err);
            });
        };


		var filterinvoicesbytime = function(invoices) {
			var invtosend = [];
			for(var i=0;i<invoices.length;i++){
				if(invoices[i].invoiceTime.length<8) {
						invoices[i].invoiceTime = '0'+invoices[i].invoiceTime;
					}
				var invdate = invoices[i].invoiceTime;

				if(moment(invoices[i].invoiceDate).format("DD-MM-YYYY") == moment($scope.report.fromDate).format("DD-MM-YYYY")) {
					var invtime = moment(invdate, 'hh:mm A');
					if($scope.fromTime !== 'undefined') {
						var frT = moment($scope.fromTime, 'hh:mm A');
						if(invtime.isBefore(frT) == true)
							continue;
					}
				}

				if(moment(invoices[i].invoiceDate).format("DD-MM-YYYY") == moment($scope.report.toDate).format("DD-MM-YYYY")) {

					var invtime = moment(invdate, 'hh:mm A');

					if($scope.toTime !== 'undefined') {
						var toT = moment($scope.toTime, 'hh:mm A');
						if(toT.isBefore(invtime) == true)
							continue;
					}
				}
				invtosend.push(invoices[i]);
			}
			return invtosend;
		};

        $scope.fromReceipt = "";
        $scope.toReceipt = "";

        $scope.From_Reset = function(){
            $scope.fromReceipt = "";
            $scope.toReceipt = "";
			$scope.sales = [];
            $scope.report.fromDate = undefined;
            $scope.report.toDate = undefined;
			$scope.report.fromTime = undefined;
			$scope.report.toTime = undefined;
			$scope.report.toDate = undefined;
         };


        $scope.btn_Apply = function() {
            if($scope.fromReceipt!="" || $scope.toReceipt!="") {
                $scope.report.fromDate = undefined;
                $scope.report.toDate = undefined;
				$scope.report.toTime = undefined;
				$scope.report.toFrom = undefined;
                $scope.invoice_Receipt_Range();
            } else {
                 $scope.getReport();
            }
        };

        $scope.invoice_Receipt_Range = function() {
            $scope.processing = true;

			main_page_service.get_receipt_sales_report_amount($scope.report.pharmacist, $scope.fromReceipt, $scope.toReceipt).then(function (response) {
				$scope.totalSalesAmount = angular.copy(response.data);
			}).catch(function(err){
				console.log(err);
			});

			main_page_service.get_receipt_sales_report_excel($scope.report.pharmacist, $scope.fromReceipt, $scope.toReceipt).then(function (response) {
				$scope.exportJson = angular.copy(response.data);
				$scope.sales = angular.copy(response.data);
			}).catch(function(err){
				console.log(err);
			});
		};

	var calculateTotal = function(sales) {
		var total = 0;
		var creditotal = 0;
		$scope.creditsales = [];
		$scope.sales = [];
		for (var i=0;i<sales.length;i++) {
			if(!sales[i].issalemrno && !sales[i].issalecredit) {
				for(var j=0;j<sales[i].orderList.length;j++) {
					sales[i].orderList[j].disc = 0;
					if(sales[i].orderDiscount > 0) {
						if(sales[i].orderDiscountType == 'percent') {
							sales[i].orderList[j].disc = Math.round(sales[i].orderList[j].price*sales[i].orderList[j].quantity*(sales[i].orderDiscount/100))
						} else {
							sales[i].orderList[j].disc = Math.round(sales[i].orderDiscount/sales[i].orderList.length);
						}

						if(sales[i].isReturn) {
							total= total-(sales[i].orderList[j].quantity*sales[i].orderList[j].price-sales[i].orderList[j].disc);
						} else {
							total= total+(sales[i].orderList[j].quantity*sales[i].orderList[j].price-sales[i].orderList[j].disc);
						}
					} else {
						if(sales[i].isReturn) {
							total-=sales[i].orderList[j].quantity*sales[i].orderList[j].price;
						} else {
							total+=sales[i].orderList[j].quantity*sales[i].orderList[j].price;
						}
					}
				}
				$scope.sales.push(sales[i]);
			} else {

				for(var j=0;j<sales[i].orderList.length;j++) {
					sales[i].orderList[j].disc = 0;
					if(sales[i].orderDiscount > 0) {
						if(sales[i].orderDiscountType == 'percent') {
							sales[i].orderList[j].disc = Math.round(sales[i].orderList[j].price*sales[i].orderList[j].quantity*(sales[i].orderDiscount/100))
						} else {
							sales[i].orderList[j].disc = Math.round(sales[i].orderDiscount/sales[i].orderList.length);
						}

						if(sales[i].isReturn) {
							creditotal= creditotal-(sales[i].orderList[j].quantity*sales[i].orderList[j].price-sales[i].orderList[j].disc);
						} else {
							creditotal= creditotal+(sales[i].orderList[j].quantity*sales[i].orderList[j].price-sales[i].orderList[j].disc);
						}
					} else {
						if(sales[i].isReturn) {
							creditotal-=sales[i].orderList[j].quantity*sales[i].orderList[j].price;
						} else {
							creditotal+=sales[i].orderList[j].quantity*sales[i].orderList[j].price;
						}
					}
				}
				$scope.creditsales.push(sales[i]);
			}
		}
		$scope.totalCreditSalesAmount = Math.round(creditotal);
		$scope.totalSalesAmount = total;
	};

    $scope.getReport = function(move){
		var toDate = dateConverter($scope.report.toDate);
		var fromDate = dateConverter($scope.report.fromDate);
		var storeDesc = $scope.report.store;

		if(toDate===null){
			toDate = undefined;
		}
		if(fromDate===null){
			fromDate = undefined;
		}
		//console.log($scope.report.store);
		if($scope.report.store === undefined){
			storeDesc = "All";
		}
		main_page_service.get_sales_page_report($scope.report.pharmacist,storeDesc,fromDate,toDate,$scope.report.fromTime,$scope.report.toTime,$scope.currentPage).then(function (response) {
			$scope.fromTime = $scope.report.fromTime;
			$scope.toTime = $scope.report.toTime;
			var sales;
			if($scope.fromTime || $scope.toTime) {
				sales = filterinvoicesbytime(response.data);
			} else {
				sales = angular.copy(response.data);
			}
			
			calculateTotal(sales);

		}).catch(function(err){
			console.log(err);
		});

		//Get Stock transfers
		main_page_service.get_stocktransfer_report(fromDate,toDate).then(function (response) {
			$scope.stocktransfers = angular.copy(response.data);
		
		}).catch(function(err){
			console.log(err);
		});
    };

	$scope.exportfile = function(){
		var filestring = '';

		//create_header
		var header = "Invoice ID,Date,Time,Item ID,Description,Quantity,Discount,Total\n";
		filestring +=header;
		for(var i=0;i<$scope.sales.length;i++) {
			for(var j=0;j<$scope.sales[i].orderList.length;j++) {
				var line = $scope.sales[i].invoiceNumber;
				line+= ','+ moment($scope.sales[i].invoiceDate).format('DD-MM-YYYY');
				line+= ','+ $scope.sales[i].invoiceTime;
				line+= ','+ $scope.sales[i].orderList[j].id;
				line+= ','+ $scope.sales[i].orderList[j].description;

				if($scope.sales[i].isReturn)
					line+= ','+ $scope.sales[i].orderList[j].quantity*-1;
				else
					line+= ','+ $scope.sales[i].orderList[j].quantity;
				line+= ','+ $scope.sales[i].orderList[j].disc;
				if($scope.sales[i].isReturn)
					line+= ','+ Math.round($scope.sales[i].orderList[j].quantity*$scope.sales[i].orderList[j].price-$scope.sales[i].orderList[j].disc)*-1;
				else
					line+= ','+ Math.round($scope.sales[i].orderList[j].quantity*$scope.sales[i].orderList[j].price-$scope.sales[i].orderList[j].disc);
				filestring+=line+'\n';
			}

		}

		var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
		return saveAs(data, ['PharmacySales-Report' + '.csv']);
	};

	$scope.printreport = function() {
		var dt = moment(new Date());
		var ts = [];
		var stocktrans = [];
		for(var i=0;i<$scope.sales.length;i++) {
			for(var j=0;j<$scope.sales[i].orderList.length;j++) {
				ts.push({ invoiceNumber: $scope.sales[i].invoiceNumber,
						  isReturn: $scope.sales[i].isReturn,
						  invoiceDate: $scope.sales[i].invoiceDate,
						  invoiceTime: $scope.sales[i].invoiceTime,
						  id: $scope.sales[i].orderList[j].id,
						  description: $scope.sales[i].orderList[j].description,
						  quantity: $scope.sales[i].orderList[j].quantity,
						  disc: $scope.sales[i].orderList[j].disc,
						  price: $scope.sales[i].orderList[j].price
				});
			}
		}

		for(var i=0;i<$scope.stocktransfers.length;i++) {
			for(var j=0;j<$scope.stocktransfers[i].itemsList.length;j++) {
				stocktrans.push({ transferNumber: $scope.stocktransfers[i].transferNumber,
						  date: moment($scope.stocktransfers[i].date).format('DD-MM-YYYY'),
						  fromStore: $scope.stocktransfers[i].fromStoreDescription,
						  toStore: $scope.stocktransfers[i].toStoreDescription,
						  transferedBy: $scope.stocktransfers[i].transferedBy,
						  id: $scope.stocktransfers[i].itemsList[j].code,
						  description: $scope.stocktransfers[i].itemsList[j].description,
						  quantity: $scope.stocktransfers[i].itemsList[j].quantity

				});
			}
		}

		var obj = {
			'invoices': ts,
			'stocktransfer': stocktrans,
			'fromdate': moment($scope.report.fromDate).format('DD-MM-YYYY'),
			'todate': moment($scope.report.toDate).format('DD-MM-YYYY'),
			'salesman': $scope.report.pharmacist,
			'totalAmounts' : $scope.totalSalesAmount,
			'date': dt.format('DD-MM-YYYY'),
			'time': dt.format('h:mm a')
		};
		obj.hospitallogo = $scope.hosDetail[0].image_url;
		print_service.print('/modules/main-page/views/sales-report-print.client.view.html',obj,
		function(){
		});
	};

	(function getStores(){
            hospitalAdmin_service.list_stores().then((response) => {
                $scope.stores = response.data;
            });
        })();

	$scope.init = function(){
		getAllReceptionists();

		$scope.report = {
			'pharmacist' : 'All'
		};
	};
		if($scope.authentication.user.roles!='pharmacy salesman') {
			$scope.init();
		}
    }
]);
