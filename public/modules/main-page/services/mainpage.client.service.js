'use strict';

angular.module('main-page').factory('main_page_service', ['$http', 'config_service',
	function($http, config_service) {
		/**
		 * Methods for main page
		 */
		var srvr_address = config_service.serverAddress;

		this.get_expired_medicines = function() {
			return $http.get(srvr_address+'/inventory-stock/check/getExpiry');
		};

		this.get_quantity_medicines = function() {
			return $http.get(srvr_address+'/inventory-stock/check/getQuantity');
		};

		this.get_expired_items = function() {
			return $http.get(srvr_address+'/storeinventory-stock/check/getExpiry');
		};

		this.get_quantity_items = function() {
			return $http.get(srvr_address+'/storeinventory-stock/check/getQuantity');
		};


		this.set_requisitions_status = function(requisition) {
			return $http.post(srvr_address+'/purchaseRequisitionUpdate',requisition);
		};

		this.get_all_requisitions = function(pageNo) {
			return $http.get(srvr_address+'/purchaseRequisition/'+pageNo);
		};

		this.get_all_purchase_order = function(pageNo) {
			return $http.get(srvr_address+'/purchaseOrder/'+pageNo);
		};

		this.get_all_GRN = function(pageNo) {
			return $http.get(srvr_address+'/GRN/'+pageNo);
		};

		this.get_all_sales_receptionists = function() {
			return $http.get(srvr_address+'/getReceptionists');
		};

		this.get_all_opd_receptionists = function() {
			return $http.get(srvr_address+'/getOpdReceptionists');
		};

		this.get_sales_report = function(employeeId,fromDate,toDate,pageNo, fromTime, toTime) {
			return $http.get(srvr_address+'/invoiceReport/'+employeeId+'/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+pageNo);
		};

		this.get_sales_page_report = function(employeeId,storedesc,fromDate,toDate,pageNo, fromTime, toTime) {
			return $http.get(srvr_address+'/invoiceReport/'+employeeId+'/'+storedesc+'/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+pageNo);
		};

		this.get_cost_sales_report = function(fromDate,toDate) {
			return $http.get(srvr_address+'/invoiceCostReport/'+fromDate+'/'+toDate);
		}

		this.get_purchase_report = function(fromDate,toDate,pageNo) {
			return $http.get(srvr_address+'/purchaseReport/'+fromDate+'/'+toDate+'/'+pageNo);
		};

		this.get_sales_report_narcotics = function(employeeId,fromDate,toDate, fromTime, toTime, pageNo) {
			return $http.get(srvr_address+'/invoiceReportNarcotics/'+employeeId+'/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+pageNo);
		};

		this.get_sales_report_excel = function(employeeId,fromDate,toDate, fromTime, toTime) {
			var page = undefined;
			return $http.get(srvr_address+'/invoiceReport/'+employeeId+'/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+page);
		};

		this.get_sales_report_amount = function(employeeId,fromDate,toDate,fromTime,toTime) {
			return $http.get(srvr_address+'/invoiceReportAmount/'+employeeId+'/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime);
		};

		this.get_receipt_sales_report_excel = function(employeeId,fromReceipt,toReceipt) {
			var page = undefined;
			return $http.get(srvr_address+'/invoiceReciptReport/'+employeeId+'/'+ fromReceipt + '/' + toReceipt);
		};

		this.get_receipt_sales_report_amount = function(employeeId,fromReceipt,toReceipt) {
			return $http.get(srvr_address+'/reportReceiptSalesAmount/'+employeeId+'/'+ fromReceipt + '/' + toReceipt);
		};

		this.get_patient = function(fromDate, date) {
			return $http.get(srvr_address+'/patient/' + fromDate + '/' + date);
		};

		this.get_all_doctors = function () {
			return $http.get(srvr_address+'/doctor');
		};

		this.get_all_otas = function () {
			return $http.get(srvr_address+'/getOTAs');
		};

		this.get_doctor_bills_report = function(doctorName, fromDate, toDate) {
			return $http.get(srvr_address+'/billReport/' + doctorName + '/' + fromDate + '/' + toDate);
		};

		this.update_panel_bills_status = function(bills) {
			return $http.post(srvr_address+'/updatePanelBillsStatus', bills);
		};

		this.get_doctor_services_bill_report = function(doctorName,doctorId, fromDate, toDate) {
			return $http.get(srvr_address+'/doctorServicesReport/'+ doctorName + '/' + doctorId + '/' + fromDate + '/' + toDate);
		};

		this.get_doctor_payable_report = function(doctorId, docname, fromDate, toDate) {
			return $http.get(srvr_address+'/doctorPayableReport/' + doctorId + '/' + docname + '/' + fromDate + '/' + toDate);
		};

		this.get_ota_payable_report = function(otaname, fromDate, toDate) {
			return $http.get(srvr_address+'/otaPayableReport/' + otaname + '/' + fromDate + '/' + toDate);
		};

		this.get_revenue = function(fromDate, toDate) {
			return $http.get(srvr_address+'/revenueReport/' + fromDate + '/' + toDate);
		};

		this.get_revenue_patient_wise = function(fromDate, toDate, receptionist) {
			return $http.get(srvr_address+'/revenueReportPatientWise/' + fromDate + '/' + toDate + '/' + receptionist);
		};

		this.get_panel_report = function(fromDate, toDate, panel) {
			return $http.get(srvr_address+'/panelReport/' + fromDate + '/' + toDate + '/' + panel);
		};
		
		this.list_panel_labs = function() {
			return $http.get(srvr_address+'/panel-lab');
		};

		this.get_panel_lab_report = function(fromDate, toDate, panelLab) {
			return $http.get(srvr_address+'/panelLabReport/' + fromDate + '/' + toDate + '/' + panelLab);
		};

		this.get_patient_report = function(fromDate, toDate, area1) {
			return $http.get(srvr_address+'/patientReport/' + fromDate + '/' + toDate + '/' + area1);
		};

		this.get_discount_patient_wise = function(fromDate, toDate, receptionist) {
			return $http.get(srvr_address+'/discountReportPatientWise/' + fromDate + '/' + toDate + '/' + receptionist);
		};

		this.get_refund_patient_wise = function(fromDate, toDate, receptionist) {
			return $http.get(srvr_address+'/refundReportPatientWise/' + fromDate + '/' + toDate + '/' + receptionist);
		};

		this.get_invoices_filtered = function(employeeId, fromDate, toDate, fromTime, toTime, pageNumber) {
			return $http.get(srvr_address+'/invoicesFiltered/' + employeeId + '/' + fromDate + '/' + toDate + '/' + fromTime+'/'+toTime+'/'+  pageNumber);
		};

		this.get_invoices_filtered_total = function(employeeId, fromDate, toDate, fromTime, toTime, pageNumber) {
			return $http.get(srvr_address+'/invoicesFiltered-Total/' + employeeId + '/' + fromDate + '/' + toDate + '/' + fromTime+'/'+toTime+'/'+  pageNumber);
		};

		this.get_Receipt_Range = function(employeeId, fromReceipt, toReceipt) {
			return $http.get(srvr_address+'/invoiceReceiptRange/' + employeeId + '/' + fromReceipt + '/' + toReceipt );
		};

		this.get_facilities = function(category) {
			return $http.get(srvr_address+'/facility');
		};

		this.get_stocktransfer_report = function(fromDate, toDate) {
			return $http.get(srvr_address+'/stockTransferReport/' + fromDate + '/' + toDate);
		};

		this.get_stocktransfer_report_withvalue = function(fromDate, toDate) {
			return $http.get(srvr_address+'/stockTransferReportwithValue/' + fromDate + '/' + toDate);
		};

		this.get_department_report = function(department, fromDate, toDate) {
			return $http.get(srvr_address+'/departmentReport/' + department + '/' + fromDate + '/' + toDate);
		};

		this.get_revenue_for_chart = function(fromDate, toDate) {
			return $http.get(srvr_address+'/revenueForChart/' + fromDate + '/' + toDate);
		};

		this.get_bills_filtered = function(employee, fromDate, toDate, pageNumber) {
			return $http.get(srvr_address+'/billsFiltered/' + employee + '/' + fromDate + '/' + toDate + '/' + pageNumber);
		};

		return this;
	}
]);
