'use strict';

//Setting up route
angular.module('radiology').config(['$stateProvider',
	function($stateProvider) {
		// Lab state routing
		$stateProvider.
		state('radiology-orders-list', {
			url: '/radiology-orders-list',
			templateUrl: 'modules/radiology/views/radiology-orders-list.client.view.html',
			access: ['radiologist']
		}).
		state('radiology-order', {
			url: '/radiology-order/:patientMRN',
			templateUrl: 'modules/radiology/views/radiology-order.client.view.html',
			access: ['radiologist', 'radiology receptionist']
		}).
		state('radiology-tests', {
			url: '/radiology-tests-form',
			templateUrl: 'modules/radiology/views/radiology-tests.client.view.html',
			access: ['radiologist', 'super admin']
		}).
		state('radiology-results', {
			url: '/radiology-results/:patientMRN/:testDescription/:orderId',
			templateUrl: 'modules/radiology/views/radiology-results.client.view.html',
			access: ['radiologist', 'super admin']
		}).
		state('radiology-reception', {
			url: '/radiology-reception',
			templateUrl: 'modules/radiology/views/radiology-reception.client.view.html',
			access: ['radiologist', 'radiology receptionist', 'super admin']
		}).
		state('radiologist-signature', {
			url: '/radiologist-signature',
			templateUrl: 'modules/radiology/views/radiologist-signature.client.view.html',
			access: ['radiologist']
		});
	}
]);
