'use strict';

angular.module('radiology').factory('radiology_service', ['$http', 'config_service', 'Upload',
	function($http, config_service, Upload) {
		var srvr_address = config_service.serverAddress;

		this.get_facilities_and_price = function(category) {
			return $http.get(srvr_address+'/facility-pricing');
		};

		/**
		 * Methods for Lab Order
		 */
		this.list_radiology_orders = function() {
			return $http.get(srvr_address+'/radiology-order');
		};

		this.list_radiology_orders_by_date = function(fromDate,toDate) {
			return $http.get(srvr_address+'/radiology-orders-date/' + fromDate+'/'+toDate);
		};

		this.get_radiology_order = function(labOrderId) {
			return $http.get(srvr_address+'/radiology-order/' + labOrderId);
		};

		this.update_radiology_order = function(labOrder) {
			return $http.put(srvr_address+'/radiology-order/' + labOrder._id, labOrder);
		};

		this.update_radiology_order_status = function(labOrderId, description, status) {
			return $http.put(srvr_address+'/radiology-order/' + labOrderId + '/' + description + '/' + status);
		};

		this.get_doctor_signature = function(doctorUsername) {
			return $http.get(srvr_address+'/users/' + doctorUsername);
		};
		
        this.uplaodImage = function(item, file){
            return Upload.upload({
                url: '/labimage',
                fields: item,
                file: file
            }).success(function (data, status, headers, config) {
                console.log('Lab Item is Saved!');
             }).error(function (data, status, headers, config) {
                            });            
        };
		
		/**
		 * Methods for Lab Tests
		 */
		this.create_radiology_test = function(radiologyTest) {
			return $http.post(srvr_address+'/radiology-test', radiologyTest);
		};

		this.list_radiology_tests = function() {
			return $http.get(srvr_address+'/radiology-test');
		};

		this.get_radiology_test = function(test) {
			return $http.get(srvr_address+'/radiology-test/' + test._id);
		};

		this.get_radiology_test_by_description = function(description) {
			return $http.get(srvr_address+'/radiology-test/' + description);
		};

		this.update_radiology_test = function(test) {
			return $http.put(srvr_address+'/radiology-test/' + test._id, test);
		};

		this.delete_radiology_test = function(test) {
			return $http.delete(srvr_address+'/radiology-test/' + test._id);
		};
		
		this.get_patient_info = function(patientMRN) {
			return $http.get(srvr_address+'/patient/'+patientMRN);
		};
		
		this.get_radiology_test_result = function(patientMRN, description) {
			return $http.get(srvr_address+'/radiology-result/' + patientMRN + '/' + description);
		};
		
		this.get_radiology_result = function(orderId, description) {
			return $http.get(srvr_address+'/radiology-resultbyorderid/' + orderId + '/' + description);
		};

		this.create_radiology_test_result = function(labResult) {
			return $http.post(srvr_address+'/radiology-result',  labResult);
		};

		this.list_radiology_result = function() {
			return $http.get(srvr_address+'/radiology-result');
		};

		this.list_radiology_test_results = function(patientMRN,description) {
			return $http.get(srvr_address+'/radiology-test-results/'+patientMRN+'/'+description);
		};

		this.save_radiology_result = function(resultObj) {
			return $http.post(srvr_address+'/radiology-result',resultObj);
		};
		
		this.update_user = function(user) {
			return $http.put(srvr_address+'/users', user);
		};
		
		return this;
	}
]);