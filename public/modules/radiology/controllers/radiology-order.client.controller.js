'use strict';

angular.module('radiology').controller('RadiologyOrderController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'print_service','radiology_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, print_service, radiology_service) {

        $scope.authentication = Authentication;
		var fileFormat = '';
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};
		
		$scope.uploadFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.img = image.target.result;
                        
                        if(file.type==='image/png'){
                        	fileFormat = '.png';
                        }
                    	else if(file.type==='image/jpeg') {
                    		fileFormat = '.jpeg';
                    	}
                    });
                };
                imageReader.readAsDataURL(file);
            }
        };
		
		$scope.uploadLabResult = function(labtest, index) {
			$scope.labforupload = labtest;
			$scope.refundPopUp = true;
			
		};
		
		$scope.closePopup = function() {
			$scope.labforupload = {};
			$scope.refundPopUp = false;
		};
		
		
		$scope.completeUpload = function() {
			var dt = moment($scope.labOrder.created_date).format("DDMMYYYY");
			if(fileFormat!==''){
	        	$scope.image_url = 'images/lab_images/'+$scope.labOrder.patientMRN+'_'+dt+'_'+$scope.labforupload.testDescription+fileFormat;
	        	upload_image($scope.labOrder.patientMRN, $scope.labforupload.testDescription, dt);

	        } else {
	        	//item_obj.image_url = '';
	        }
		};
		
		$scope.addTestResult = function(test, index) {
			$location.path('/radiology-results/' + $scope.radiologyOrder.patientMRN + '/' + test.testDescription + '/' + $scope.radiologyOrder._id);
		};
		
		var upload_image = function(mr_number, description, date){
			var obj = {'imageLoc' : './public/images/lab_images/'+mr_number+'_'+date+'_'+description+fileFormat};

        	radiology_service.uplaodImage(obj,$scope.img)
	            .then(function(response) {
	                //console.log(response);
				var allcomplete = true;
				for(var i=0;i<$scope.radiologyOrder.radiologyTests.length;i++){
					if($scope.radiologyOrder.radiologyTests[i].testDescription == $scope.labforupload.testDescription){
						$scope.radiologyOrder.radiologyTests[i].reportUrl =$scope.image_url;
						$scope.radiologyOrder.radiologyTests[i].status = 'Completed';
					}
					
					if($scope.radiologyOrder.radiologyTests[i].status !== 'Completed') {
						allcomplete = false;
					}
				}
				
				if(allcomplete = true) {
					$scope.radiologyOrder.orderStatus = 'Completed';
				}
				
				radiology_service.update_radiology_order($scope.radiologyOrder).then(function (response) {
					ngToast.create({
						className: 'success',
						content: 'Radiology Order Updated Successfully',
						dismissButton: true
					});
					$scope.processing = false;
					$scope.refundPopUp = false;
					$scope.labforupload = {};
					$scope.image_url = '';
					$scope.img = undefined;
					
					}).catch(function (error) {
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to update Radiology Order.',
							dismissButton: true
						});
						$scope.processing = false;
					});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to upload image.',
                    dismissButton: true
                });
				$scope.processing = false;
            });
		};

        var getRadiologyOrder = function(patientMRN){
            radiology_service.get_radiology_order(patientMRN).then(function (response) {
                $scope.radiologyOrder = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Radiology Order',
                    dismissButton: true
                });
            });
        };

        var init = function() {
	        $scope.processing = false;
        };

        var updateRadiologyOrder = function() {
            $scope.processing = true;
            var allInprogress = false;
            for(var i=0;i<$scope.radiologyOrder.radiologyTests.length;i++){
                if($scope.radiologyOrder.radiologyTests[i].status!=='In Progress'){
                    allInprogress = true;
                }
            }
            if(allInprogress === false){
                $scope.radiologyOrder.orderStatus = 'In Progress';
            }

            radiology_service.update_radiology_order($scope.radiologyOrder).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Radiology Order Updated Successfully',
                    dismissButton: true
                });
                $scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update Radiology Order.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        init();

        $scope.returnToReception = function() {
            $location.path('/radiology-reception');
        };
		
		$scope.updateteststatus = function(test, status) {
            radiology_service.update_radiology_order_status($scope.radiologyOrder._id,test.testDescription,status).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Radiology Order Updated Successfully',
                    dismissButton: true
                });
                $scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update Radiology Order.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
		};
		
        var calculateAge = function(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };
		
        $scope.printResult = function(test){
			$scope.processing = true;
			radiology_service.get_patient_info($scope.radiologyOrder.patientMRN).then(function (resp) {
                $scope.patientDetails = angular.copy(resp.data[0]);
				
				radiology_service.get_radiology_result($scope.radiologyOrder._id, test.testDescription).then(function (response) {
				var data = {
					patientInfo : {
						name : $scope.patientDetails.name,
						mr_number : $scope.patientDetails.mr_number,
						panel : $scope.patientDetails.panel,
						age : calculateAge(new Date($scope.patientDetails.dob)),
						gender : $scope.patientDetails.gender,
						created : (new Date($scope.patientDetails.created)).toString()
						//phone : patientDetails.
					},
					date: moment().format('DD-MM-YYYY'),
					test : response.data
				};
					print_service.print('/modules/radiology/views/radiology-results-print.client.view.html',data,
						function(){
						  
					});
					$scope.processing = false;  
				});				
                //getPanel(patientDetails.panel_id);
            }).catch(function (error) {
				$scope.processing = false;  
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve patient data.',
                    dismissButton: true
				});

        });
		
		}

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        getRadiologyOrder($stateParams.patientMRN);
    }
]);