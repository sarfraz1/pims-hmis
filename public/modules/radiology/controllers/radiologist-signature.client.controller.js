'use strict';

angular.module('radiology').controller('RadiologistSignatureController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'inventory_service','radiology_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, inventory_service,radiology_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        
        var init = function() {
	        $scope.processing = false;
        };

        $scope.submit = function(){
            radiology_service.update_user($scope.authentication.user)
                .then(function(response) {
                    ngToast.create({
                    className: 'success',
                    content: 'Information Updated',
                    dismissButton: true
                });
               
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+ error,
                    dismissButton: true
                });
            });
        };

        init();
        
    }
]);