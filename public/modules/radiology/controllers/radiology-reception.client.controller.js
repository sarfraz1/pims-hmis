'use strict';

angular.module('radiology').controller('RadiologyReceptionController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'radiology_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, radiology_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        $scope.searchDate = new Date();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

		$scope.getRadiologyOrders = function() {
			$scope.processing = true;
            var tempDate = angular.copy($scope.searchDate);
			radiology_service.list_radiology_orders_by_date(dateConverter(tempDate)).then(function (response) {
				$scope.radiologyOrders = response.data;
				$scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Radiology Orders.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
		};

        $scope.reset = function(form) {
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.getRadiologyOrders();
	        $scope.processing = false;
        };

        $scope.viewRadiologyOrderDetails = function(radiologyOrder) {
        	$location.path('/radiology-order/' + radiologyOrder._id);
        };

        $scope.reset();
    }
]);