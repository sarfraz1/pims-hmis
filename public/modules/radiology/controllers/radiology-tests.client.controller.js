'use strict';

angular.module('radiology').controller('RadiologyTestsController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'radiology_service','print_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, radiology_service, print_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        $scope.dropdownFlag = false;
        $scope.facilitySelected = false;
        $scope.facilities = [];
        $scope.categoryDescription = '';
        $scope.updateItem = false;

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getRadiologyTests = function() {
            radiology_service.list_radiology_tests().then(function (response) {
                $scope.facilities = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve tests.',
                    dismissButton: true
                });
            });
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        var init = function() {
        	$scope.test = angular.copy(test);
            $scope.categoryDescription = '';
	        $scope.processing = false;
	        $scope.dropdownFlag = false;
	        $scope.facilitySelected = false;
            $scope.updateItem = false;
            getRadiologyTests();
        };

		var test = {
			'description': '',
            'title': '',
			'testSections': [{
                'heading' : '',
				'sectionText': ''
			}]
		};

		
        $scope.selectTest = function(service) {
        	$scope.dropdownFlag = false;
        	$scope.facilitySelected = true;
            $scope.test = angular.copy(service);
			$scope.updateItem = true;
        };

        $scope.showFacilities = function() {
        	$scope.dropdownFlag = true;
            if (!e) var e = window.event;
                e.cancelBubble = true;
            if (e.stopPropagation) 
            	e.stopPropagation();
        };

        $scope.hideFacilities = function() {
        	$scope.dropdownFlag = false;
        };

        $scope.removeResult = function(index) {
        	$scope.test.testSections.splice(index, 1);
        };

        $scope.addResult = function() {
        	$scope.test.testSections.push({
                'heading' : '',
				'sectionText': ''
			});
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $scope.processing = false;
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            init();
        };

		
		$scope.printLabTest = function(form) {
			$scope.printData = {};
			$scope.printData.mr_number = 'tmp-211221';
			$scope.printData.name = 'Test Patient';
			$scope.printData.date = moment().format('DD-MM-YYYY hh:mma');
			$scope.printData.test = $scope.test;
			
                print_service.print('/modules/radiology/views/test-print.client.view.html',$scope.printData,
                function(){
                    $scope.processing = false;
                });
		};

        $scope.submitLabTest = function(form) {
            $scope.processing = true;

			radiology_service.create_radiology_test($scope.test).then(function (response) {
				ngToast.create({
					className: 'success',
					content: 'Radiology Test Created Successfully',
					dismissButton: true
				});
				$scope.reset(form);
			}).catch(function (error) {
				if (error.data.message.indexOf('duplicate') !== -1) {
					ngToast.create({
						className: 'danger',
						content: 'Error: Another Radiology Test exists with the same description.',
						dismissButton: true
					});
				} else {
					ngToast.create({
						className: 'danger',
						content: 'Error: Unable to add Radiology Test.',
						dismissButton: true
					});
				}
				$scope.processing = false;
			});
                                
        };

        $scope.updateLabTest = function(form) {
            $scope.processing = true;

			radiology_service.update_radiology_test($scope.test).then(function (response) {
				ngToast.create({
					className: 'success',
					content: 'Radiology Test Updated Successfully',
					dismissButton: true
				});
				$scope.reset(form);
			}).catch(function (error) {
				if (error.data.message.indexOf('duplicate') !== -1) {
					ngToast.create({
						className: 'danger',
						content: 'Error: Another Radiology Test exists with the same description.',
						dismissButton: true
					});
				} else {
					ngToast.create({
						className: 'danger',
						content: 'Error: Unable to update Radiology Test.',
						dismissButton: true
					});
				}
				$scope.processing = false;
			});
                                

        };

        $scope.deleteLabTest = function(form) {
            $scope.processing = true;
            radiology_service.delete_radiology_test($scope.test).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Lab Test Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Unable to delete Radiology Test.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        $scope.reset();
    }
]);