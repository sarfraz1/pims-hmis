'use strict';

angular.module('radiology').controller('RadiologyResultsController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'radiology_service','print_service','billing_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, radiology_service,print_service,billing_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // bool based on user logging in

        $scope.processing = false;

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getRadiologyTests = function() {
            radiology_service.list_radiology_tests().then(function (response) {
                $scope.radiologyTests = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve tests.',
                    dismissButton: true
                });
            });
        };

		$scope.testSelected = function() {
			var selected = JSON.parse($scope.selectedTest);
			$scope.radiologyResult.title = selected.title;
			$scope.radiologyResult.testSections = selected.testSections;
		};

        $scope.removeResult = function(index) {
        	$scope.radiologyResult.testSections.splice(index, 1);
        };

        $scope.addResult = function() {
        	$scope.radiologyResult.testSections.push({
                'heading' : '',
				'sectionText': ''
			});
        };

        var calculateAge = function(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        var get_patient_info = function(){
            radiology_service.get_patient_info($stateParams.patientMRN).then(function (response) {
                $scope.patientDetails = angular.copy(response.data[0]);
				$scope.radiologyResult.patientMRN = $scope.patientDetails.mr_number;
				$scope.radiologyResult.patientName = $scope.patientDetails.name;
                //getPanel(patientDetails.panel_id);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve patient data.',
                    dismissButton: true
                });
            });
        };

        $scope.$watch('pathologistUsername', function() {
            if($scope.pathologistUsername!==''){
                radiology_service.get_doctor_signature($scope.pathologistUsername)
                    .then(function(response) {
                      //  pathologistSignature = response.data;
                }).catch(function (error) {

                });
            }
        });

        var initialize = function() {
			$scope.radiologyResult = {};
            $scope.radiologyResult.description = $stateParams.testDescription;
			$scope.radiologyResult.radiologyOrderId = $stateParams.orderId;
			$scope.radiologyResult.title = '';
			$scope.radiologyResult.testSections = [{
					'heading' : '',
					'sectionText': ''
				}];

            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();

            $scope.processing = false;
            get_patient_info();
			getRadiologyTests();
        };

        initialize();

        $scope.saveResult = function(form) {
            $scope.processing = true;
            $scope.radiologyResult.radiologistUsername = $scope.authentication.user.username;
            $scope.radiologyResult.radiologistSignature = $scope.authentication.user.signature;
			radiology_service.create_radiology_test_result($scope.radiologyResult).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Result Saved Successfully',
                    dismissButton: true
                });

                $scope.printResult();
                radiology_service.update_radiology_order_status($stateParams.orderId, $stateParams.testDescription, 'Completed').then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Order Updated Successfully',
                        dismissButton: true
                    });
                    $scope.processing = false;

                    $location.path('/radiology-reception');
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Order status.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        $scope.returnOrderList = function() {
            $location.path('/radiology-reception');
        };

        $scope.printResult = function(){
            var data = {
                patientInfo : {
                    name : $scope.patientDetails.name,
                    mr_number : $scope.patientDetails.mr_number,
                    panel : $scope.patientDetails.panel,
                    age : calculateAge(new Date($scope.patientDetails.dob)),
                    gender : $scope.patientDetails.gender,
                    created : (new Date($scope.patientDetails.created)).toString()
                    //phone : patientDetails.
                },
               date: moment().format('DD-MM-YYYY'),
                test : $scope.radiologyResult
            };
			$scope.radiologyResult.radiologistUsername = $scope.authentication.user.username;
            $scope.radiologyResult.radiologistSignature = $scope.authentication.user.signature;
            print_service.print('/modules/radiology/views/radiology-results-print.client.view.html',data,
                function(){

            });
        };

    }
])

;
