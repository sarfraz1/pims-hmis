'use strict';

//Setting up route
angular.module('lab').config(['$stateProvider',
	function($stateProvider) {
		// Lab state routing
		$stateProvider.
		state('lab-orders-list', {
			url: '/lab-orders-list',
			templateUrl: 'modules/lab/views/lab-orders-list.client.view.html',
			access: ['lab pathologist', 'lab technician', 'super admin']
		}).
		state('lab-order', {
			url: '/lab-order/:patientMRN',
			templateUrl: 'modules/lab/views/lab-order.client.view.html',
			access: ['lab receptionist', 'super admin']
		}).
		state('lab-setting', {
			url: '/lab-setting',
			templateUrl: 'modules/lab/views/lab-setting.client.view.html',
			access: ['lab technician', 'super admin']
		}).
		state('lab-reception', {
			url: '/lab-reception',
			templateUrl: 'modules/lab/views/lab-reception.client.view.html',
			access: ['lab receptionist', 'super admin']
		}).
		state('lab-results', {
			url: '/lab-results/:patientMRN/:patientName/:testDescription/:labOrderId',
			templateUrl: 'modules/lab/views/lab-results.client.view.html',
			access: ['super admin', 'lab technician', 'lab pathologist']
		}).
		state('panel-labs', {
			url: '/panel-labs',
			templateUrl: 'modules/lab/views/panel-labs.client.view.html',
			access: ['super admin']
		}).
		state('lab-tests', {
			url: '/lab-tests-form',
			templateUrl: 'modules/lab/views/lab-tests.client.view.html',
			access: ['super admin', 'lab technician', 'lab pathologist']
		}).
		state('pathologist-signature', {
			url: '/pathologist-signature',
			templateUrl: 'modules/lab/views/pathologist-signature.client.view.html',
			access: ['lab pathologist', 'lab technician']
		});
	}
]);