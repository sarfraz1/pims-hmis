'use strict';

angular.module('lab').controller('LabTestsController', ['$scope', '$http', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service', 'print_service',
    function($scope, $http, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service, print_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        $scope.dropdownFlag = false;
        $scope.facilitySelected = false;
        $scope.facilities = [];
        $scope.categoryDescription = '';
        $scope.updateItem = false;

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getFaciltyPricing = function() {
            lab_service.get_facilities_and_price().then(function (response) {
                $scope.facilities = response.data;
                for (var i = 0; i < $scope.facilities.length; i++) {
                    if (!$scope.facilities[i].category.includes('Lab')) {
                        $scope.facilities.splice(i, 1);
                        i--;
                    }
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve facilities.',
                    dismissButton: true
                });
            });
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        var seperateSubtest = function(callback){
            var subtests = [];
            for(var i=0;i<$scope.test.referenceValue.length;i++){
                var found = false;
                for(var j=0;j<subtests.length;j++){
                    if(subtests[j].testName === $scope.test.referenceValue[i].valueDescription){
                        subtests[j].testDetails.push($scope.test.referenceValue[i]);
                        found = true;
                        break;
                    }
                }
                if(found === false){
                    subtests.push({
                        testName : $scope.test.referenceValue[i].valueDescription,
                        testDetails : [$scope.test.referenceValue[i]],
                    });
                }
            }
            callback(subtests);
        };

        var fillEmptyValues = function(callback){
            var callbackSent = false;
            for(var i=0;i<$scope.test.referenceValue.length;i++) {
                if(!$scope.test.referenceValue[i].isSubHeading){
                    if(!$scope.test.referenceValue[i].maxAge && $scope.test.referenceValue[i].minAge){
                        $scope.test.referenceValue[i].maxAge = 150;
                    }
                    else if(!$scope.test.referenceValue[i].minAge && $scope.test.referenceValue[i].maxAge){
                        $scope.test.referenceValue[i].minAge = 0;
                    } else if(!$scope.test.referenceValue[i].minAge && !$scope.test.referenceValue[i].maxAge){
                        $scope.test.referenceValue[i].minAge = 0;
                        $scope.test.referenceValue[i].maxAge = 150;
                    }
                    if(!$scope.test.referenceValue[i].ageUnits){
                        $scope.test.referenceValue[i].ageUnits = "Years";
                    }
                    if($scope.test.referenceValue[i].isRange){
                        if ($scope.test.referenceValue[i].minValue || $scope.test.referenceValue[i].maxValue){

                        } else {
                            callbackSent = true;
                            callback(false);
                        }
                    }
                }
            }
            if(callbackSent === false)
                callback(true);
        };

        var convertAge = (age,ageUnit) => {
            age = parseInt(age);
            if(ageUnit === "Years") {
                return age*365;
            }
            else if(ageUnit === "Months") {
                return age*30;
            }
            else {
                return age;
            }
        }

        var checkSubtestAttribCollisions = function(subTests,callback){
            var callbackSent = false;
            for(var i=0;i<subTests.length;i++){
                for(var j=0;j<subTests[i].testDetails.length;j++){
                    for(var k=j+1;k<subTests[i].testDetails.length;k++){


                        var ageUnit1 = subTests[i].testDetails[j].ageUnits;
                        var min1 = convertAge(subTests[i].testDetails[j].minAge,ageUnit1);
                        var max1 = convertAge(subTests[i].testDetails[j].maxAge,ageUnit1);
                        var gender1 = subTests[i].testDetails[j].gender;
                        var ageUnit2 = subTests[i].testDetails[k].ageUnits;
                        var min2 = convertAge(subTests[i].testDetails[k].minAge, ageUnit2);
                        var max2 = convertAge(subTests[i].testDetails[k].maxAge, ageUnit2);
                        var gender2 = subTests[i].testDetails[k].gender;
                        if(!subTests[i].testDetails[k].isSubHeading){
                            if((max1 > min2 && max1 < max2) && (gender1===gender2 || gender1==='Both')){
                                callback(subTests[i].testDetails[j].valueDescription);
                                callbackSent = true;
                                break;

                            }
                        }
                    }
                }
            }
            if(callbackSent === false)
                callback('');
        };

        var init = function() {
        	$scope.test = angular.copy(test);
            $scope.categoryDescription = '';
	        $scope.processing = false;
	        $scope.dropdownFlag = false;
	        $scope.facilitySelected = false;
            $scope.updateItem = false;
            getFaciltyPricing();
        };

		var test = {
			'description': '',
            'category': '',
			'referenceValue': [{
                'valueDescription' : '',
				'minAge': undefined,
                'maxAge': undefined,
                'ageUnits': "Years",
				'gender': 'male',
				'minValue': undefined,
				'maxValue': undefined,
                'unit': '',
				'expectedValue': '',
				'valueDetails': '',
                'isRange': true,
                'isSubHeading' : false,
                'expValue': [{
                  'addexpectedValue': 'Comment',
                }]
			}],
		};

        $scope.selectFacility = function(service) {
            $scope.processing = true;
        	$scope.dropdownFlag = false;
        	$scope.facilitySelected = true;
            $scope.test = angular.copy(test);
        	$scope.test.description = service.description;
            $scope.test.category = service.category;
            $scope.categoryDescription = $scope.test.category + '-' + $scope.test.description;
            lab_service.get_lab_test_by_description_category(service.description, service.category).then(function (response) {
                $scope.test = response.data;
                $scope.processing = false;
                $scope.updateItem = true;
            }).catch(function (error) {
                $scope.updateItem = false;
                $scope.processing = false;
            });
        };

        $scope.showFacilities = function() {
        	$scope.dropdownFlag = true;
            if (!e) var e = window.event;
                e.cancelBubble = true;
            if (e.stopPropagation)
            	e.stopPropagation();
        };

        $scope.hideFacilities = function() {
        	$scope.dropdownFlag = false;
        };

        $scope.removeResult = function(index) {
        	$scope.test.referenceValue.splice(index, 1);
        };

        $scope.removeValue = function(ind, index) {
        	$scope.test.referenceValue[ind].expValue.splice(index, 1);
        };

        $scope.addValue = function(i) {
        	$scope.test.referenceValue[i].expValue.push({
				'addexpectedValue': '',
			});
        };

        $scope.addResult = function() {
        	$scope.test.referenceValue.push({
                'valueDescription' : '',
				'minAge': undefined,
				'maxAge': undefined,
                'ageUnits': "Years",
				'gender': 'male',
				'minValue': undefined,
				'maxValue': undefined,
				'expectedValue': '',
				'valueDetails': '',
                'isRange': true,
                'isSubHeading' : false,
                'expValue': [{
                  'addexpectedValue': 'Comment',
                }],
			});
        };


	    $scope.PreviewLabTest = function(){
			var data = {
                labResult : $scope.test,
                marginTop : 1,
                headerImage : false,
                footerImage : false,
                enableHeader : false,
                enableFooter : false,
                enableSignatue : false,
                pathologistSignature : '',
                emailAddressHtml : patientDetails.email,
            };
            data.hospitallogo = $scope.hosDetail[0].image_url;
            data.hospitalname = $scope.hosDetail[0].title;
            data.hospitalAddress = $scope.hosDetail[0].address;
            data.hospitalPHNumber = $scope.hosDetail[0].number;
            print_service.print('/modules/lab/views/lab-results-print-preview.client.view.html',data,
                function(){

            });
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $scope.processing = false;
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            init();
        };

        var checkRange = function(callback) {
            for (var i = 0; i < $scope.test.referenceValue.length; i++) {
                if(!$scope.test.referenceValue[i].isSubHeading){
                    if ($scope.test.referenceValue[i].isRange) {
                        $scope.test.referenceValue[i].expectedValue = undefined;
                    } else if (!$scope.test.referenceValue[i].isRange) {
                        $scope.test.referenceValue[i].minValue = undefined;
                        $scope.test.referenceValue[i].maxValue = undefined;
                    }
                }
            }
            callback();
        };

        $scope.submitLabTest = function(form) {
            $scope.processing = true;
            fillEmptyValues(function(emptyValueCheck){
                if(emptyValueCheck===true){
                    checkRange(function(){
                        seperateSubtest(function(subsetResult){
                            checkSubtestAttribCollisions(subsetResult,function(subtestCollide){

                                if(subtestCollide === ''){
                                    lab_service.create_lab_test($scope.test).then(function (response) {
                                        ngToast.create({
                                            className: 'success',
                                            content: 'Lab Test Created Successfully',
                                            dismissButton: true
                                        });
                                        $scope.reset(form);
                                    }).catch(function (error) {
                                        if (error.data.message.indexOf('duplicate') !== -1) {
                                            ngToast.create({
                                                className: 'danger',
                                                content: 'Error: Another Lab Test exists with the same description.',
                                                dismissButton: true
                                            });
                                        } else {
                                            ngToast.create({
                                                className: 'danger',
                                                content: 'Error: Unable to add Lab Test.',
                                                dismissButton: true
                                            });
                                        }
                                        $scope.processing = false;
                                    });
                                } else {
                                    $scope.processing = false;
                                    ngToast.create({
                                        className: 'danger',
                                        content: subtestCollide+' test values are similar.',
                                        dismissButton: true
                                    });
                                }
                            });

                        });
                    });
                } else {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Min Max Values are missing!',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.updateLabTest = function(form) {
            //$scope.processing = true;
            fillEmptyValues(function(emptyValueCheck){
                if(emptyValueCheck===true){
                    checkRange(function(){
                        seperateSubtest(function(subsetResult){
                            checkSubtestAttribCollisions(subsetResult,function(subtestCollide){
                                if(subtestCollide === ''){
                                    lab_service.update_lab_test($scope.test).then(function (response) {
                                        ngToast.create({
                                            className: 'success',
                                            content: 'Lab Test Updated Successfully',
                                            dismissButton: true
                                        });
                                        $scope.reset(form);
                                    }).catch(function (error) {
                                        if (error.data.message.indexOf('duplicate') !== -1) {
                                            ngToast.create({
                                                className: 'danger',
                                                content: 'Error: Another Lab Test exists with the same description.',
                                                dismissButton: true
                                            });
                                        } else {
                                            ngToast.create({
                                                className: 'danger',
                                                content: 'Error: Unable to update Lab Test.',
                                                dismissButton: true
                                            });
                                        }
                                        $scope.processing = false;
                                    });
                                } else {
                                    ngToast.create({
                                        className: 'danger',
                                        content: subtestCollide+' test values are similar.',
                                        dismissButton: true
                                    });
                                }
                            });

                        });
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Min Max Values are missing!',
                        dismissButton: true
                    });
                }
            });

        };

        $scope.deleteLabTest = function(form) {
            $scope.processing = true;
            lab_service.delete_lab_test($scope.test).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Lab Test Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Unable to delete Lab Test.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        $scope.reset();
    }
]);
