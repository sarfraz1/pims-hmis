'use strict';

angular.module('lab').controller('LabSettingController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'inventory_service','lab_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, inventory_service,lab_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.options = [
            {value: true, label: 'Yes'},
            {value: false, label: 'No'}];


		$scope.uploadHeaderFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.headerImage = image.target.result;

                        if(file.type==='image/png'){
                            fileFormat1 = '.png';
                        }
                        else if(file.type==='image/jpeg') {
                            fileFormat1 = '.jpeg';
                        }

                    });
                };
                imageReader.readAsDataURL(file);
            }
        };

        $scope.uploadFooterFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.footerImage = image.target.result;

                        if(file.type==='image/png'){
                            fileFormat2 = '.png';
                        }
                        else if(file.type==='image/jpeg') {
                            fileFormat2 = '.jpeg';
                        }

                        //console.log(file.type);
                        //console.log(scope.myImage);
                    });
                };
                imageReader.readAsDataURL(file);
            }
        };

        var upload_image = function(obj,image){

            inventory_service.uplaodImage(obj,image)
                .then(function(response) {
                    //console.log(response)

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to upload image.',
                    dismissButton: true
                });
            });
        };

        var getLabSettings = function(){
            lab_service.get_lab_setting()
                .then(function(response) {
                    $scope.marginTop = response.data.margin_top;
                    $scope.headerImage = response.data.header_image_url;
                    $scope.footerImage = response.data.footer_image_url;
                    $scope.enableHeader = response.data.enableHeader;
                    $scope.enableFooter = response.data.enableFooter;
                    $scope.enableSignatue = response.data.enableSignatue;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+ error,
                    dismissButton: true
                });
            });
        };

        var init = function() {

	        $scope.processing = false;
            $scope.marginTop = 0;
            fileFormat1 = '';
            fileFormat2 = '';
            $scope.enableHeader = false;
            $scope.enableFooter = false;
            getLabSettings();
        };

        $scope.submit = function(){
            var labSettings = {
                hospital_name : 'hospital',
                header_image_url : '',
                footer_image_url : '',
                margin_top : $scope.marginTop,
                enableHeader : $scope.enableHeader,
                enableFooter : $scope.enableFooter,
                enableSignatue : $scope.enableSignatue
            };

            if(fileFormat1!=='') {
                var obj1 = {'imageLoc' : './public/images/item_images/headerImage'+fileFormat1};
                labSettings.header_image_url = './images/item_images/headerImage'+fileFormat1;
                upload_image(obj1,$scope.headerImage);
            }
            else {
                labSettings.header_image_url = $scope.headerImage;

            }

            if(fileFormat2!=='') {
                var obj2 = {'imageLoc' : './public/images/item_images/footerImage'+fileFormat2};
                labSettings.footer_image_url = './images/item_images/footerImage'+fileFormat2;
                upload_image(obj2,$scope.footerImage);

            } else {
                labSettings.footer_image_url = $scope.footerImage;
            }



            lab_service.save_lab_setting(labSettings)
                .then(function(response) {
                    ngToast.create({
                    className: 'success',
                    content: 'Information Updated',
                    dismissButton: true
                });

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+ error,
                    dismissButton: true
                });
            });
        };

        var fileFormat1;
        var fileFormat2;
        init();

    }
]);
