'use strict';

angular.module('lab').controller('LabOrdersListController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        //$scope.searchDate;
        var timeoutPromise;
        $scope.fromDate = new Date();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.getLabOrders = function() {
            $scope.processing = true;
            var fromDate = angular.copy($scope.fromDate),
                toDate = angular.copy($scope.toDate);

            lab_service.list_lab_orders_by_date(dateConverter(fromDate),dateConverter(toDate)).then(function (response) {
                $scope.labOrders = response.data;
                $scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Lab Orders.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
           timeoutPromise = $timeout(function() {
                $scope.getLabOrders();
            }, 10000);
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.reset = function(form) {
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.getLabOrders();
            $scope.processing = false;
        };
        $scope.viewLabResults = function(labOrder, labTest) {
            $location.path('/lab-results/' + labOrder.patientMRN + '/' + labOrder.patientName + '/' + labTest.testDescription + '/' + labOrder._id);
        };

        $scope.reset();

        $scope.$on('$destroy', function(){
            $timeout.cancel(timeoutPromise);
        });
    }
]);
