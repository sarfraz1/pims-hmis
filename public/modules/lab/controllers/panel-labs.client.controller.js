'use strict';

angular.module('lab').controller('PanelLabsController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        $scope.dropdownFlag = false;
        $scope.facilities = [];
        $scope.categoryDescription = '';
        $scope.confirmationPopup = 'hide-popup';
        $scope.updateItem = false;

        $scope.showTable = false;
        $scope.headings = [];
        $scope.headings.push({'alias': 'Panel Description', 'name': 'description', 'width': 99});
        $scope.panelLabsData = [];
        $scope.prefix = 'panelLab';
        $scope.panelLabsToShow = [];

        var get_panel_labs = function() {
        	$scope.processing = true;
        	lab_service.list_panel_labs().then(function (response) {
        		if (response.data.length === 0) {
        			$scope.processing = false;
        			$scope.showTable = false;
        		} else {
        			$scope.headings = [];
        			$scope.headings.push({'alias': 'Panel Description', 'name': 'description', 'width': 99});
        			$scope.panelLabsData = response.data;
        			$scope.panelLabsToShow = [];
        			for (var i = 0; i < $scope.panelLabsData.length; i++) {
        				$scope.panelLabsToShow.push({'description': $scope.panelLabsData[i].description});
        			}
        			$scope.showTable = true;
        			$scope.processing = false;
        		}
        	})
        }

        var get_facility_pricing = function() {
        	$scope.processing = true;
            lab_service.get_facilities_and_price().then(function (response) {
                $scope.facilities = response.data;
                $scope.processing = false;
            }).catch(function (error) {
            	$scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve facilities.',
                    dismissButton: true
                });
            });
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        var panelLab = {
        	'description': '',
        	'address': '',
        	'contact': '',
        	'tests': []
        };

        var init = function() {
        	$scope.panelLab = angular.copy(panelLab);
		    $scope.processing = false;
		    $scope.dropdownFlag = false;
		    $scope.updateItem = false;
		    $scope.facilities = [];
		    $scope.confirmationPopup = 'hide-popup';
            get_facility_pricing();
            get_panel_labs();
        };

        $scope.callbackfn = function(panelLab) {
        	var panelLabIndex = getIndex($scope.panelLabsData, 'description', panelLab[0].description);
        	$scope.panelLab = $scope.panelLabsData[panelLabIndex];
        	$scope.updateItem = true;
        }

        $scope.select_facility = function(facility, index) {
        	$scope.categoryDescription = '';
        	$scope.panelLab.tests.push({'description': facility.description, 'category': facility.category});
        	$scope.facilities.splice(index, 1);
        	$scope.dropdownFlag = false;
        };

        $scope.show_facilities = function() {
        	$scope.dropdownFlag = true;
            if (!e) var e = window.event;
                e.cancelBubble = true;
            if (e.stopPropagation)
            	e.stopPropagation();
        };

        $scope.hide_facilities = function() {
        	$scope.dropdownFlag = false;
        };

        $scope.remove_lab_test = function(facility, index) {
        	$scope.facilities.push(facility);
        	$scope.panelLab.tests.splice(index, 1);
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            init();
        };

        $scope.delete_panel_lab = function() {
            $scope.confirmationPopup = 'confirmation-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.update_panel_lab = function(form) {
        	$scope.processing = true;
        	lab_service.update_panel_lab($scope.panelLab).then(function (response) {
        		ngToast.create({
        			className: 'success',
        			content: 'Panel Lab Updated Successfully',
        			dismissButton: true
        		});
        		$scope.reset(form);
        	}).catch(function (error) {
        		ngToast.create({
        			className: 'danger',
        			content: 'Error: Unable to update Panel Lab.',
        			dismissButton: true
        		});
        	});
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            $scope.processing = true;
            lab_service.delete_panel_lab($scope.panelLab).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Panel Lab Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Panel Lab.',
                    dismissButton: true
                });
            });
        };

        $scope.submit_panel_lab = function(form) {
        	$scope.processing = true;
        	lab_service.create_panel_lab($scope.panelLab).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Panel Lab Created Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Panel Lab exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add Panel Lab.',
                        dismissButton: true
                    });
                }
                $scope.reset(form);
            });
        };

        $scope.reset();
    }
]);
