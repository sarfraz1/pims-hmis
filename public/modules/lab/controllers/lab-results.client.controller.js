'use strict';

angular.module('lab').controller('LabResultsController', ['$scope', '$http', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service','print_service','billing_service',
    function($scope, $http, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service,print_service,billing_service) {

        $scope.authentication = Authentication;
        var patientDetails = {};
        var labTestt = [];
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // bool based on user logging in
        $scope.isPathologist = false;
        $scope.pathologistUsername = '';
        $scope.enableSignatue = false;
        $scope.index = 0;
        var pathologistSignature = ''
        var pathologistSignaturePrint = '';
        if ($scope.authentication.user.roles === 'lab pathologist'){
            $scope.isPathologist = true;
        }
        else
            $scope.isPathologist = false;

        $scope.processing = false;

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var calculateAge = function(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        var getPanel = function(panelID){
            billing_service.get_panel(panelID).then(function (response) {
                if(response.data!==null)
                    patientDetails.panel = response.data;
            });
        };

        var get_result = function() {
            $scope.processing = true;
            lab_service.get_result($stateParams.patientMRN, $stateParams.testDescription, $stateParams.labOrderId).then(function (getResult) {
                $scope.processing = false;
                for(var j=0; j<getResult.data.tests.length; j++){
                  if(getResult.data.tests[j].laborderId == $stateParams.labOrderId){
                    $scope.index=j;
                    for (var i = 0; i < getResult.data.tests[j].subTests.length; i++) {
                        if(getResult.data.tests[j].subTests[i].reference.isRange)
                            getResult.data.tests[j].subTests[i].resultValue = parseFloat(getResult.data.tests[j].subTests[i].resultValue);
                    }
                  }
                }
                $scope.labResults = getResult.data;

                $scope.pathologistUsername = $scope.labResults.tests[$scope.labResults.tests.length-1].pathologistUsername;
                var createdDate = $scope.labResults.tests[$scope.labResults.tests.length - 1].Created;

                try {
                    var modifiedDate = $scope.labResults.tests[$scope.labResults.tests.length - 1].updated[$scope.labResults.tests[$scope.labResults.tests.length - 1].updated.length-1].Date;
                    $scope.modifiedDate = dateConverter(new Date(modifiedDate));
                } catch (error) {
                    console.log(error);
                }
                $scope.createdDate = dateConverter(new Date(createdDate));

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        var get_lab_test_result = function() {
            $scope.processing = true;
            lab_service.get_lab_test_result($stateParams.patientMRN, $stateParams.testDescription).then(function (response) {
                $scope.processing = false;
                for (var i = 0; i < response.data.tests[response.data.tests.length - 1].subTests.length; i++) {
                    if(response.data.tests[response.data.tests.length - 1].subTests[i].reference.isRange)
                        response.data.tests[response.data.tests.length - 1].subTests[i].resultValue = parseFloat(response.data.tests[response.data.tests.length - 1].subTests[i].resultValue);
                }
                $scope.labResult = response.data;

                $scope.pathologistUsername = $scope.labResult.tests[$scope.labResult.tests.length-1].pathologistUsername;
                var createdDate = $scope.labResult.tests[$scope.labResult.tests.length - 1].Created;

                try {
                    var modifiedDate = $scope.labResult.tests[$scope.labResult.tests.length - 1].updated[$scope.labResult.tests[$scope.labResult.tests.length - 1].updated.length-1].Date;
                    $scope.modifiedDate = dateConverter(new Date(modifiedDate));
                } catch (error) {
                    console.log(error);
                }
                $scope.createdDate = dateConverter(new Date(createdDate));
                $scope.testHistory = angular.copy(response.data);

                for(i=0;i<$scope.testHistory.tests.length;i++){
                    $scope.testHistory.tests[i].Created = new Date($scope.testHistory.tests[i].Created);
                    $scope.testHistory.tests[i].Created = dateConverter($scope.testHistory.tests[i].Created);
                }

                $scope.testHistory.tests.reverse();

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };



        var get_patient_info = function(){
            lab_service.get_patient_info($stateParams.patientMRN).then(function (response) {
                patientDetails = angular.copy(response.data[0]);
                getPanel(patientDetails.panel_id);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Test Result.',
                    dismissButton: true
                });
            });
        };

        var get_patient_test_results = function(){
            lab_service.list_patient_test_results($stateParams.patientMRN, $stateParams.testDescription).then(function (response) {
                $scope.testHistory = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Test Result.',
                    dismissButton: true
                });
            });
        };

        var getLabSettings = function(){
            lab_service.get_lab_setting()
                .then(function(response) {
                    $scope.marginTop = response.data.margin_top;
                    $scope.headerImage = response.data.header_image_url;
                    $scope.footerImage = response.data.footer_image_url;
                    $scope.enableHeader = response.data.enableHeader;
                    $scope.enableFooter = response.data.enableFooter;
                    //$scope.enableSignatue = response.data.enableSignatue;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+ error,
                    dismissButton: true
                });
            });
        };

        $scope.$watch('pathologistUsername', function() {
            if($scope.pathologistUsername!==''){
                lab_service.get_doctor_signature($scope.pathologistUsername)
                    .then(function(response) {
                        pathologistSignature = response.data;

                }).catch(function (error) {

                });
            }
        });

        var getPathologistSignature = function() {
                lab_service.get_doctor_signature('lab-p')
                    .then(function(response) {
                        pathologistSignaturePrint = response.data;
                }).catch(function (error) {

                });
        };



        var initialize = function() {
            $scope.patientName = $stateParams.patientName;
            //console.log($stateParams);
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.labResult = {};
            $scope.processing = false;
            get_lab_test_result();
            get_result();
            get_patient_info();
            getLabSettings();
            getPathologistSignature();
        };

        initialize();

        $scope.saveLabResultPathologist = function(form) {
            $scope.processing = true;
            $scope.labResult.tests[$scope.labResult.tests.length - 1].updated.push({by: $scope.authentication.user.username});
            $scope.labResult.tests[$scope.labResult.tests.length - 1].pathologistUsername = $scope.authentication.user.username;
            $scope.pathologistUsername = $scope.authentication.user.username;
            pathologistSignature = $scope.authentication.user.signature;
            lab_service.create_lab_test_result($scope.labResult).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Test Result Saved Successfully',
                    dismissButton: true
                });
                var status = '';
                var count = 0;
                for (var i = 0; i < $scope.labResult.tests[$scope.labResult.tests.length - 1].subTests.length; i++) {
                    if ($scope.labResult.tests[$scope.labResult.tests.length - 1].subTests[i].reference.isSubHeading || $scope.labResult.tests[$scope.labResult.tests.length - 1].subTests[i].resultStatus === 'Normal' || $scope.labResult.tests[$scope.labResult.tests.length - 1].subTests[i].resultStatus === 'Alert') {
                        count++;
                    }
                }
                if (count === $scope.labResult.tests[$scope.labResult.tests.length - 1].subTests.length) {
                    status = 'Completed';
                } else {
                    status = 'In Progress';
                }
                // $scope.printResult();
                lab_service.update_lab_order_status($stateParams.labOrderId, $stateParams.testDescription, status).then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Order Updated Successfully',
                        dismissButton: true
                    });
                    $scope.processing = false;

                    $location.path('/lab-orders-list');
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Lab Order.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        var getLabOrder = function(labOrderId){
          lab_service.get_lab_order(labOrderId).then(function (response) {
              $scope.labOrder = response.data;
              test_result($scope.labOrder.patientMRN, $scope.labOrder.labTests);

          }).catch(function (error) {
              ngToast.create({
                  className: 'danger',
                  content: 'Error: Unable to retrieve Panel Labs.',
                  dismissButton: true
              });
          });
      };

      var test_result = function(patientMRN, description) {
          $scope.processing = true;
          var k=0;
          for(var j=0;j<description.length;j++){
          lab_service.get_lab_test_result(patientMRN, description[j].testDescription).then(function (response) {
              $scope.processing = false;
              for (var i = 0; i < response.data.tests[response.data.tests.length - 1].subTests.length; i++) {
                  if(response.data.tests[response.data.tests.length - 1].subTests[i].reference.isRange)
                      response.data.tests[response.data.tests.length - 1].subTests[i].resultValue = parseFloat(response.data.tests[response.data.tests.length - 1].subTests[i].resultValue);
              }
              $scope.labResultt = response.data;
              labTestt[k++]=$scope.labResultt;
              var createdDate = $scope.labResultt.tests[$scope.labResultt.tests.length - 1].Created;

              try {
                  var modifiedDate = $scope.labResultt.tests[$scope.labResultt.tests.length - 1].updated[$scope.labResultt.tests[$scope.labResultt.tests.length - 1].updated.length-1].Date;
                  $scope.modifiedDate = dateConverter(new Date(modifiedDate));
              } catch (error) {
                  console.log(error);
              }
              $scope.createdDate = dateConverter(new Date(createdDate));
              $scope.testHistory = angular.copy(response.data);
              for(i=0;i<$scope.testHistory.tests.length;i++){
                  $scope.testHistory.tests[i].Created = new Date($scope.testHistory.tests[i].Created);
                  $scope.testHistory.tests[i].Created = dateConverter($scope.testHistory.tests[i].Created);
              }

              $scope.testHistory.tests.reverse();
              if(labTestt.length === j){
                $scope.printResultt();
                //console.log(labTestt);
              }

          }).catch(function (error) {
              ngToast.create({
                  className: 'danger',
                  content: 'Error: Unable to retrieve Test Result.',
                  dismissButton: true
              });
              $scope.processing = false;
          });
        }
      };

      $scope.showAllLabOrder = function() {
        getLabOrder($stateParams.labOrderId);
      }

        $scope.saveLabResultTechnician = function(form) {
            $scope.processing = true;
            $scope.labResults.tests[$scope.labResults.tests.length - 1].updated.push({by: $scope.authentication.user.username});
            lab_service.create_lab_test_result($scope.labResults).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Test Result Saved Successfully',
                    dismissButton: true
                });
                var status = '';
                var count = 0;
                var alertBool = false;
                for (var i = 0; i < $scope.labResults.tests[$scope.labResults.tests.length - 1].subTests.length; i++) {
                    if ($scope.labResults.tests[$scope.labResults.tests.length - 1].subTests[i].resultStatus === 'Alert') {
                        alertBool = true;
                    } else if ($scope.labResults.tests[$scope.labResults.tests.length - 1].subTests[i].resultStatus === 'Normal') {
                        count++;
                    }
                }
                if (alertBool) {
                    status = 'Alert';
                } else if (count === $scope.labResults.tests[$scope.labResults.tests.length - 1].subTests.length) {
                    status = 'Completed';
                } else {
                    status = 'In Progress';
                }
                // $scope.printResult();

                lab_service.update_lab_order_status($stateParams.labOrderId, $stateParams.testDescription, status).then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Order Updated Successfully',
                        dismissButton: true
                    });
                    $scope.processing = false;
                    $location.path('/lab-orders-list');
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Lab Order.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        $scope.checkResultValue = function(test) {
            if (test.resultValue === '' || test.resultValue === undefined) {
                test.resultStatus = '';
            } else {
                if (test.reference.isRange) {
                    if (test.resultValue === undefined) {
                        test.resultStatus = '';
                    }

                    if(test.reference.minValue && test.reference.maxValue){
                        if (test.resultValue < test.reference.minValue || test.resultValue > test.reference.maxValue) {
                            test.resultStatus = 'Alert';
                        } else {
                            test.resultStatus = 'Normal';
                        }
                    } else {
                        if(test.reference.minValue){
                            if (test.resultValue < test.reference.minValue) {
                                test.resultStatus = 'Alert';
                            } else {
                                test.resultStatus = 'Normal';
                            }
                        } else if(test.reference.maxValue){
                            if (test.resultValue > test.reference.maxValue) {
                                test.resultStatus = 'Alert';
                            } else {
                                test.resultStatus = 'Normal';
                            }
                        }
                    }
                } else if (test.reference.expectedValue.toLowerCase() === 'comment') {
                    test.resultStatus = 'Normal';
                } else {
                    if (test.resultValue.toLowerCase() !== test.reference.expectedValue.toLowerCase()) {
                        test.resultStatus = 'Alert';
                    } else {
                        test.resultStatus = 'Normal';
                    }
                }
            }
        };

        $scope.returnOrderList = function() {
            $location.path('/lab-orders-list');
        };

        $scope.resample = function(){
            $scope.processing = true;
            $scope.labResult.tests[$scope.labResult.tests.length - 1].updated.push({by: $scope.authentication.user.username});
            $scope.labResult.tests[$scope.labResult.tests.length - 1].testStatus = "Resample";
            lab_service.create_lab_test_result($scope.labResult).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Test Result Updated Successfully',
                    dismissButton: true
                });
                lab_service.update_lab_order_status($stateParams.labOrderId, $stateParams.testDescription, 'Resample').then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Order Updated Successfully',
                        dismissButton: true
                    });
                    $scope.processing = false;
                    $location.path('/lab-orders-list');
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Lab Order.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Update Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        $scope.retest = function () {
            $scope.processing = true;
            $scope.labResult.tests[$scope.labResult.tests.length - 1].updated.push({ by: $scope.authentication.user.username });
            $scope.labResult.tests[$scope.labResult.tests.length - 1].testStatus = "Retest";
            $scope.labResult.tests.push($scope.labResult.tests[$scope.labResult.tests.length - 1]);
            lab_service.create_lab_test_result($scope.labResult).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Test Result Updated Successfully',
                    dismissButton: true
                });
                lab_service.update_lab_order_status($stateParams.labOrderId, $stateParams.testDescription, 'Retest').then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Order Updated Successfully',
                        dismissButton: true
                    });
                    $scope.processing = false;

                    $location.path('/lab-orders-list');
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Lab Order.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Update Test Result.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        var boolSignature = function() {
            var singleTest = $scope.labResult.tests[$scope.labResult.tests.length - 1];


            if(!(isNaN(singleTest.subTests[singleTest.subTests.length - 1].resultValue)) && singleTest.updated.length > 0){
                $scope.enableSignatue = true;
            } else {
                $scope.enableSignatue = false;
            }

        };

        $scope.printResult = function(){
            //console.log("kkkk");
            boolSignature();
            getPathologistSignature();
            var data = {
                patientInfo : {
                    name : patientDetails.name,
                    mr_number : patientDetails.mr_number,
                    panel : patientDetails.panel,
                    age : calculateAge(new Date(patientDetails.dob)),
                    gender : patientDetails.gender,
                    created : (new Date(patientDetails.created)).toString()
                    //phone : patientDetails.
                },
                date: dateConverter(new Date()),
                index: $scope.index,
                labResult : $scope.labResult,
                marginTop : $scope.marginTop,
                headerImage : $scope.headerImage,
                footerImage : $scope.footerImage,
                enableHeader : $scope.enableHeader,
                enableFooter : $scope.enableFooter,
                enableSignatue : $scope.enableSignatue,
                pathologistSignature : pathologistSignaturePrint,
                emailAddressHtml : patientDetails.email,
            };
            data.hospitallogo = $scope.hosDetail[0].image_url;
            data.hospitalname = $scope.hosDetail[0].title;
            data.hospitalAddress = $scope.hosDetail[0].address;
            data.hospitalPHNumber = $scope.hosDetail[0].number;

            print_service.print('/modules/lab/views/lab-results-print.client.view.html',data,
                function(){

            });
        };

        $scope.printResultt = function(){
            var data = {
                patientInfo : {
                    name : patientDetails.name,
                    mr_number : patientDetails.mr_number,
                    panel : patientDetails.panel,
                    age : calculateAge(new Date(patientDetails.dob)),
                    gender : patientDetails.gender,
                    created : (new Date(patientDetails.created)).toString()
                    //phone : patientDetails.
                },
                date: dateConverter(new Date()),
                labResult : labTestt,
                marginTop : $scope.marginTop,
                headerImage : $scope.headerImage,
                footerImage : $scope.footerImage,
                enableHeader : $scope.enableHeader,
                enableFooter : $scope.enableFooter,
                enableSignatue : $scope.enableSignatue,
                pathologistSignature : pathologistSignature,
                emailAddressHtml : patientDetails.email,
            };
            print_service.print('/modules/lab/views/lab-orders-result-print.client.view.html',data,
                function(){

            });
        };
        $scope.printAndEmailResult = function(){
            boolSignature();
            getPathologistSignature();
            var data = {
                patientInfo : {
                    name : patientDetails.name,
                    mr_number : patientDetails.mr_number,
                    panel : patientDetails.panel,
                    age : calculateAge(new Date(patientDetails.dob)),
                    gender : patientDetails.gender,
                    created : (new Date(patientDetails.created)).toString()
                    //phone : patientDetails.
                },
                date: dateConverter(new Date()),
                labResult : $scope.labResult,
                marginTop : $scope.marginTop,
                headerImage : $scope.headerImage,
                footerImage : $scope.footerImage,
                enableHeader : $scope.enableHeader,
                enableFooter : $scope.enableFooter,
                enableSignatue : $scope.enableSignatue,
                pathologistSignature : pathologistSignaturePrint,
                emailAddressHtml : patientDetails.email
            };
            data.hospitallogo = $scope.hosDetail[0].image_url;
            data.hospitalname = $scope.hosDetail[0].title;
            data.hospitalAddress = $scope.hosDetail[0].address;
            data.hospitalPHNumber = $scope.hosDetail[0].number;

            print_service.printAndEmailData('/modules/lab/views/lab-results-print.client.view.html',data,'Lab Result',
                function(){

            });
        };

        $scope.printResultt = function(){
            var data = {
                patientInfo : {
                    name : patientDetails.name,
                    mr_number : patientDetails.mr_number,
                    panel : patientDetails.panel,
                    age : calculateAge(new Date(patientDetails.dob)),
                    gender : patientDetails.gender,
                    created : (new Date(patientDetails.created)).toString()
                    //phone : patientDetails.
                },
                date: dateConverter(new Date()),
                labResult : labTestt,
                marginTop : $scope.marginTop,
                headerImage : $scope.headerImage,
                footerImage : $scope.footerImage,
                enableHeader : $scope.enableHeader,
                enableFooter : $scope.enableFooter,
                enableSignatue : $scope.enableSignatue,
                pathologistSignature : pathologistSignature,
                emailAddressHtml : patientDetails.email
            };
            print_service.printAndEmailData('/modules/lab/views/lab-orders-result-print.client.view.html',data,'Lab Result',
                function(){

            });
        };
    }
])

;
