'use strict';

angular.module('lab').controller('LabReceptionController', ['$scope', '$http', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service', 'print_service', 'billing_service',
    function($scope, $http, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service, print_service, billing_service) {

        $scope.authentication = Authentication;
        var patientDetails = {};
        var labTestt=[];
        var pathologistSignaturePrint = '';
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.processing = false;
        $scope.searchDate = new Date();

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput) {
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error) {
                return dateinput;
            }
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };


      var calculateAge = function(birthday) { // birthday is a date
          var ageDifMs = Date.now() - birthday.getTime();
          var ageDate = new Date(ageDifMs); // miliseconds from epoch
          return Math.abs(ageDate.getUTCFullYear() - 1970);
      };

      var getPanel = function(panelID){
          billing_service.get_panel(panelID).then(function (response) {
              if(response.data!==null)
                  patientDetails.panel = response.data;
          });
      };

      var get_patient_info = function(patientMRN){
          lab_service.get_patient_info(patientMRN).then(function (response) {
              patientDetails = angular.copy(response.data[0]);
              getPanel(patientDetails.panel_id);
          }).catch(function (error) {
              ngToast.create({
                  className: 'danger',
                  content: 'Error: Unable to retrieve Test Result.',
                  dismissButton: true
              });
          });
      };

      var get_lab_test_result = function(patientMRN, description) {
          $scope.processing = true;
          var k=0;
          for(var j=0;j<description.length;j++){
          lab_service.get_lab_test_result(patientMRN, description[j].testDescription).then(function (response) {
              $scope.processing = false;
              for (var i = 0; i < response.data.tests[response.data.tests.length - 1].subTests.length; i++) {
                  if(response.data.tests[response.data.tests.length - 1].subTests[i].reference.isRange)
                      response.data.tests[response.data.tests.length - 1].subTests[i].resultValue = parseFloat(response.data.tests[response.data.tests.length - 1].subTests[i].resultValue);
              }
              $scope.labResult = response.data;
              labTestt[k++]=$scope.labResult;
              var createdDate = $scope.labResult.tests[$scope.labResult.tests.length - 1].Created;

              try {
                  var modifiedDate = $scope.labResult.tests[$scope.labResult.tests.length - 1].updated[$scope.labResult.tests[$scope.labResult.tests.length - 1].updated.length-1].Date;
                  $scope.modifiedDate = dateConverter(new Date(modifiedDate));
              } catch (error) {
                  console.log(error);
              }
              $scope.createdDate = dateConverter(new Date(createdDate));
              $scope.testHistory = angular.copy(response.data);
              for(i=0;i<$scope.testHistory.tests.length;i++){
                  $scope.testHistory.tests[i].Created = new Date($scope.testHistory.tests[i].Created);
                  $scope.testHistory.tests[i].Created = dateConverter($scope.testHistory.tests[i].Created);
              }

              $scope.testHistory.tests.reverse();
              if(labTestt.length === j){
                $scope.printResult();
                //console.log(labTestt);
              }

          }).catch(function (error) {
              ngToast.create({
                  className: 'danger',
                  content: 'Error: Unable to retrieve Test Result.',
                  dismissButton: true
              });
              $scope.processing = false;
          });
        }
      };

      var getPathologistSignature = function() {
                lab_service.get_doctor_signature('lab-p')
                    .then(function(response) {
                      //console.log(response.data);
                        pathologistSignaturePrint = response.data;
                        //console.log(pathologistSignaturePrint);
                }).catch(function (error) {

                });
        };

      $scope.printResult = function(patientMRN, description){
        //console.log(pathologistSignaturePrint);
          var data = {
              patientInfo : {
                  name : patientDetails.name,
                  mr_number : patientDetails.mr_number,
                  panel : patientDetails.panel,
                  age : calculateAge(new Date(patientDetails.dob)),
                  gender : patientDetails.gender,
                  created : (new Date(patientDetails.created)).toString()
                  //phone : patientDetails.
              },
              date: dateConverter(new Date()),
              labResult : labTestt,
              marginTop : $scope.marginTop,
              headerImage : $scope.headerImage,
              footerImage : $scope.footerImage,
              enableHeader : $scope.enableHeader,
              enableFooter : $scope.enableFooter,
              enableSignatue : true,
              pathologistSignature : pathologistSignaturePrint,
              emailAddressHtml : patientDetails.email,

          };
          data.hospitallogo = $scope.hosDetail[0].image_url;
          data.hospitalname = $scope.hosDetail[0].title;
          data.hospitalAddress = $scope.hosDetail[0].address;
          data.hospitalPHNumber = $scope.hosDetail[0].number;
          print_service.printAndEmailData('/modules/lab/views/lab-orders-result-print.client.view.html',data,'Lab Result',
              function(){

          });
          //console.log(data);
      };

      $scope.printData = function(patientMRN, description) {
      getPathologistSignature();
      get_patient_info(patientMRN);
      get_lab_test_result(patientMRN, description);
    };

		$scope.getLabOrders = function() {
			$scope.processing = true;
            var tempDate = angular.copy($scope.searchDate);
			lab_service.list_lab_orders_by_date(dateConverter(tempDate)).then(function (response) {
				$scope.labOrders = response.data;
				$scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Lab Orders.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
            $timeout(function() {
                $scope.getLabOrders();
            }, 10000);
		};

        $scope.reset = function(form) {
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.getLabOrders();
	        $scope.processing = false;
        };

        $scope.viewLabOrderDetails = function(labOrder) {
          //console.log(labOrder._id);
        	$location.path('/lab-order/' + labOrder._id);
        };

        $scope.reset();
    }
]);
