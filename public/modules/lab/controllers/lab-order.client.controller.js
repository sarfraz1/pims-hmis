'use strict';

angular.module('lab').controller('LabOrderController', ['$scope', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll', 'lab_service',
    function($scope, $stateParams, $location, $timeout, Authentication, ngToast, $anchorScroll, lab_service) {

        $scope.authentication = Authentication;
		$scope.labimgonly = false;
		var fileFormat = '';
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
		};

        $scope.showPanels = true;

		var getPanelLabs = function() {
			$scope.processing = true;
			lab_service.list_panel_labs().then(function (response) {
				$scope.processing = false;
                if (response.data.length > 0) {
				    $scope.panelLabs = response.data;
                    $scope.panelLabs.unshift({description: 'None'});
                } else {
                    $scope.showPanels = false;
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panel Labs.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
		};


		$scope.uploadFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.img = image.target.result;

                        if(file.type==='image/png'){
                        	fileFormat = '.png';
                        }
                    	else if(file.type==='image/jpeg') {
                    		fileFormat = '.jpeg';
                    	}
                    });
                };
                imageReader.readAsDataURL(file);
            }
        };

		$scope.uploadLabResult = function(labtest, index) {
			$scope.labforupload = labtest;
			$scope.refundPopUp = true;

		};

		$scope.closePopup = function() {
			$scope.labforupload = {};
			$scope.refundPopUp = false;
		};


		$scope.completeUpload = function() {
			var dt = moment($scope.labOrder.created_date).format("DDMMYYYY");
			if(fileFormat!==''){
	        	$scope.image_url = 'images/lab_images/'+$scope.labOrder.patientMRN+'_'+dt+'_'+$scope.labforupload.testDescription+fileFormat;
	        	upload_image($scope.labOrder.patientMRN, $scope.labforupload.testDescription, dt);

	        } else {
	        	//item_obj.image_url = '';
	        }
		};

		var upload_image = function(mr_number, description, date){
			var obj = {'imageLoc' : './public/images/lab_images/'+mr_number+'_'+date+'_'+description+fileFormat};

        	lab_service.uplaodImage(obj,$scope.img)
	            .then(function(response) {
	                //console.log(response);
				var allcomplete = true;
				for(var i=0;i<$scope.labOrder.labTests.length;i++){
					if($scope.labOrder.labTests[i].testDescription == $scope.labforupload.testDescription){
						$scope.labOrder.labTests[i].reportUrl =$scope.image_url;
						$scope.labOrder.labTests[i].status = 'Completed';
					}

					if($scope.labOrder.labTests[i].status !== 'Completed') {
						allcomplete = false;
					}
				}

				if(allcomplete = true) {
					$scope.labOrder.orderStatus = 'Completed';
				}

				lab_service.update_lab_order($scope.labOrder).then(function (response) {
					ngToast.create({
						className: 'success',
						content: 'Lab Order Updated Successfully',
						dismissButton: true
					});
					$scope.processing = false;
					$scope.refundPopUp = false;
					$scope.labforupload = {};
					$scope.image_url = '';
					$scope.img = undefined;

					}).catch(function (error) {
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to update Lab Order.',
							dismissButton: true
						});
						$scope.processing = false;
					});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to upload image.',
                    dismissButton: true
                });
				$scope.processing = false;
            });
		};

        var getLabOrder = function(patientMRN){
            lab_service.get_lab_order(patientMRN).then(function (response) {
                $scope.labOrder = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panel Labs.',
                    dismissButton: true
                });
            });
        };

        var init = function() {
            $scope.processing = false;
            getPanelLabs();
        };

        var updateLabOrder = function() {
            $scope.processing = true;
            var allInprogress = false;
            for(var i=0;i<$scope.labOrder.labTests.length;i++){
                //console.log("labTest Status: ", $scope.labOrder.labTests[i].status);
                if($scope.labOrder.labTests[i].status!=='In Progress' && $scope.labOrder.labTests[i].status!=='Transfered to Partner' && $scope.labOrder.labTests[i].status!=='Resampled'){
                    allInprogress = true;
                }
            }
            if(allInprogress === false){
                $scope.labOrder.orderStatus = 'In Progress';
            }
            //console.log("Update lab order", $scope.labOrder);
            lab_service.update_lab_order($scope.labOrder).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Lab Order Updated Successfully',
                    dismissButton: true
                });
                $scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update Lab Order.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        init();

        $scope.returnToReception = function() {
            $location.path('/lab-reception');
        };

        $scope.completeLabTestCollection = function() {
            for (var i = 0; i < $scope.labOrder.labTests.length; i++) {
                //$scope.labOrder.labTests[i].status = 'In Progress';
                $scope.printLabTestLabel($scope.labOrder.labTests[i],i);
            }
            //updateLabOrder();
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.printLabTestLabel = function(labTest, index) {
            var testObj = {
                testInfo : labTest,
                orderInfo: $scope.labOrder
            };
            //console.log("Print lab test label", testObj);
            if($scope.labOrder.labTests[index].status === 'Pending' || $scope.labOrder.labTests[index].status === 'Resample'){
                //console.log("Just to save", testObj);
                lab_service.save_lab_result(testObj).then(function (response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Lab Result Updated Successfully',
                        dismissButton: true
                    });

                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Lab Order.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            }
            if (labTest.status === 'Pending' && !labTest.transferStatus) {
        	   labTest.status = 'In Progress';
               // print functionality
        	   updateLabOrder();
            }
            else if (labTest.status === 'Pending' && labTest.transferStatus === true) {
                labTest.status = 'Transfered to Partner';
                // print functionality
                updateLabOrder();
            } else if (labTest.status === 'Resample' || labTest.status === 'Resampled') {
                labTest.status = 'Resampled';
                // print functionality
                updateLabOrder();
            } else {
                // print functionality
                ngToast.create({
                    className: 'success',
                    content: 'Lab Order Printed',
                    dismissButton: true
                });
            }
        };

        $scope.selectPanel = function(labTest) {
            if (labTest.transferredTo === 'None') {
                labTest.transferStatus = false;
                labTest.transferredTo = '';
                labTest.status = 'Pending';
            } else {
                labTest.transferStatus = true;
                //labTest.status = 'In Progress';
            }
            // $scope.printLabTestLabel(labTest);
        };

        getLabOrder($stateParams.patientMRN);
    }
]);
