'use strict';

angular.module('lab').factory('lab_service', ['$http', 'config_service', 'Upload',
	function($http, config_service, Upload) {
		var srvr_address = config_service.serverAddress;

		this.get_facilities_and_price = function(category) {
			return $http.get(srvr_address+'/facility-pricing');
		};

		/**
		 * Methods for Panel Labs
		 */
		this.list_panel_labs = function() {
			return $http.get(srvr_address+'/panel-lab');
		};

		this.create_panel_lab = function(panelLab) {
			return $http.post(srvr_address+'/panel-lab', panelLab);
		};

		this.get_panel_lab = function(panelLab) {
			return $http.get(srvr_address+'/panel-lab/' + panelLab._id);
		};

		this.update_panel_lab = function(panelLab) {
			return $http.put(srvr_address+'/panel-lab/' + panelLab._id, panelLab);
		};

		this.delete_panel_lab = function(panelLab) {
			return $http.delete(srvr_address+'/panel-lab/' + panelLab._id);
		};

		/**
		 * Methods for Lab Tests
		 */
		this.create_lab_test = function(labTest) {
			return $http.post(srvr_address+'/lab-test', labTest);
		};

		this.list_lab_tests = function() {
			return $http.get(srvr_address+'/lab-test');
		};

		this.get_lab_test = function(test) {
			return $http.get(srvr_address+'/lab-test/' + test._id);
		};

		this.get_lab_test_by_description_category = function(description, category) {
			return $http.get(srvr_address+'/lab-test/' + description + '/' + category);
		};

		this.update_lab_test = function(test) {
			return $http.put(srvr_address+'/lab-test/' + test._id, test);
		};

		this.delete_lab_test = function(test) {
			return $http.delete(srvr_address+'/lab-test/' + test._id);
		};

		/**
		 * Methods for Lab Order
		 */
		this.list_lab_orders = function() {
			return $http.get(srvr_address+'/lab-order');
		};

		this.list_lab_orders_by_date = function(fromDate,toDate) {
			return $http.get(srvr_address+'/lab-orders-date/' + fromDate+'/'+toDate);
		};

		this.get_lab_order = function(labOrderId) {
			return $http.get(srvr_address+'/lab-order/' + labOrderId);
		};

		this.update_lab_order = function(labOrder) {
			return $http.put(srvr_address+'/lab-order/' + labOrder._id, labOrder);
		};

		this.update_lab_order_status = function(labOrderId, description, status) {
			return $http.put(srvr_address+'/lab-order/' + labOrderId + '/' + description + '/' + status);
		};

		this.get_result = function(patientMRN, description, labOrderId) {
			return $http.get(srvr_address+'/lab-result/' + patientMRN + '/' + description + '/' + labOrderId);
		};

		this.get_lab_test_result = function(patientMRN, description) {
			return $http.get(srvr_address+'/lab-result/' + patientMRN + '/' + description);
		};

		this.get_doctor_signature = function(doctorUsername) {
			return $http.get(srvr_address+'/users/' + doctorUsername);
		};

		this.create_lab_test_result = function(labResult) {
			return $http.put(srvr_address+'/lab-result/' + labResult._id, labResult);
		};

		this.list_lab_result = function() {
			return $http.get(srvr_address+'/lab-result');
		};

		this.list_patient_test_results = function(patientMRN,description) {
			return $http.get(srvr_address+'/patient-test-results/'+patientMRN+'/'+description);
		};

		this.save_lab_result = function(resultObj) {
			return $http.post(srvr_address+'/lab-result',resultObj);
		};

		this.get_patient_info = function(patientMRN) {
			return $http.get(srvr_address+'/patient/'+patientMRN);
		};

		this.save_lab_setting = function(labSetting) {
			return $http.post(srvr_address+'/lab-setting',labSetting);
		};

		this.get_lab_setting = function() {
			return $http.get(srvr_address+'/lab-setting');
		};

        this.uplaodImage = function(item, file){
            return Upload.upload({
                url: '/labimage',
                fields: item,
                file: file
            }).success(function (data, status, headers, config) {
             }).error(function (data, status, headers, config) {
                            });
        };

		this.update_user = function(user) {
			return $http.put(srvr_address+'/users', user);
		};

		return this;
	}
]);
