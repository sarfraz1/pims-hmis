'use strict';

// Inventories Pricing controller
angular.module('inventories').controller('InventoriesPricingController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll', 'inventory_service', function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll, inventory_service) {

		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if (!$scope.authentication.user) $location.path('/signin');

		$scope.pricingType = 'sale';
		$scope.sourceType = 'customer';
		$scope.discountType = 'percent';
		$scope.showSupplierList = false;

		var inventory;
		var pricing = {
			'dt' : new Date(),
			'ed' : new Date(),
			'sourceType' : 'customer',
			'pricingType' : 'sale',
			'discountAmount' : 0,
			'discountType' : 'percent',
			'sellingPrice' : 0,
			'purchasingPrice' : 0,
			'retailPrice' : 0
		};

		var retrieved;
		pricing.updateFlag = false;
		$scope.supplierColumns = ['description','emailAddress'];


		function get_inventory_data() {
			inventory = inventory_service.return_inventory_data();
			$scope.purchase_unit = inventory.purchase_unit;
			$scope.selling_unit = inventory.selling_unit;
			if (inventory.updateFlag === true) {
				inventory_service.view_inventory_pricing(inventory).then(function (response) {
					retrieved = response;
					$scope.pricing = angular.copy(retrieved);
					$scope.pricingType = retrieved.pricingType;
					$scope.sourceType = retrieved.sourceType;
					$scope.discountType = retrieved.discountType;
					$scope.dt = new Date(retrieved.dt);
					$scope.ed = new Date(retrieved.ed);

				}, function (error) {
					console.log(error);
				});
			}
		}

		get_inventory_data();

		var getSuppliers = function(){
            inventory_service.get_inventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
            }).catch(function(error) {
                    console.log(error);
            });
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
			$scope.pricing = angular.copy(pricing);
			getSuppliers();
		};

		$scope.reset_pricing = function(form) {
			$scope.reset(form);
		};

		$scope.selectSupplier = function(supplier){
            $scope.selectedSupplier = supplier;
            $scope.showSupplierList = false;
        };

        $scope.submit_pricing = function(pricing, form) {
        	pricing.inventoryID = inventory.code;
        	pricing.ed = $scope.ed.toISOString();
        	pricing.dt = $scope.dt.toISOString();
        	pricing.discountType = $scope.discountType;
        	pricing.sourceType = $scope.sourceType;
        	pricing.pricingType = $scope.pricingType;
        	pricing.supplierId = $scope.selectedSupplier._id;
        	if (inventory.updateFlag === true) {
        		inventory_service.update_inventory_pricing(pricing).then(function(response) {
	                ngToast.create({
	                    className: 'success',
	                    content: 'Inventory Pricing Updated Successfully',
	                    dismissButton: true
	                });
        			$location.path('/inventories/create');
        		});
        	} else {
	        	inventory_service.create_inventory_pricing(pricing).then(function(response) {
	                ngToast.create({
	                    className: 'success',
	                    content: 'Inventory Pricing Created Successfully',
	                    dismissButton: true
	                });
	    			$location.path('/inventories/create');
	        	});
        	}
			$scope.reset(form);
        };

        getSuppliers();
    }
]);
