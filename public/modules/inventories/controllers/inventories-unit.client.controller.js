'use strict';

angular.module('inventories').controller('InventoriesUnitController', ['$scope', '$stateParams', '$location', 'Authentication', 'inventory_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, inventory_service, ngToast, $anchorScroll) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if (!$scope.authentication.user) $location.path('/signin');
				
        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 49});
            $scope.headings.push({'alias': 'Default Option', 'name': 'defaultOption', 'width': 49});
            $scope.samplejson = [];
            $scope.samplejson = response.data;
            $scope.unitsToShow = [];
            for (var i = 0; i < $scope.samplejson.length; i++) {
                var defaultOption = 'No';
                if ($scope.samplejson[i].defaultOption === true) {
                    defaultOption = 'Yes';
                }
                $scope.unitsToShow.push({'description': $scope.samplejson[i].description, 'defaultOption': defaultOption});
            }
        };
        
        var getItems = function() {
            inventory_service.list_inventory_unit()
                .then(function (response) {
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Units.',
                    dismissButton: true
                });
            });
        };

        getItems();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var unitIndex = getIndex($scope.samplejson, 'description', selected_item[0].description);
            $scope.unit = $scope.samplejson[unitIndex];
            $scope.UpdateItem = true;
        };

		$scope.UpdateItem = false;
		$scope.defaultOption = false;
        $scope.showTable = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.prefix = 'unitdiv';
        $scope.headings = [];
        $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.headings.push({'alias': 'Default Option', 'name': 'defaultOption', 'width': 49});
        $scope.samplejson = [];
        $scope.unitsToShow = [];

		var unit = {
			'description' : '',
			'defaultOption' : false
		};

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
            $scope.defaultOption = false;
			$scope.unit = angular.copy(unit);
            $scope.confirmationPopup = 'hide-popup';
		};

        $scope.submit_unit = function(unit, form) {
        	unit.defaultOption = $scope.defaultOption;
    		inventory_service.create_inventory_unit(unit).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Unit Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
                getItems();
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Inventory Unit exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add Inventory Unit.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.delete_unit = function() {
            $scope.confirmationPopup = 'confirmation-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            inventory_service.delete_inventory_unit($scope.unit).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Unit Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                getItems();
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Inventory Unit.',
                    dismissButton: true
                });
            });
        };

        $scope.update_unit = function(unit, form) {
        	unit.defaultOption = $scope.defaultOption;
        	inventory_service.update_inventory_unit(unit).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Unit Updated Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
                getItems();
            }).catch(function (error) {
                if (error.data.message.indexOf('already exists') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Inventory Unit exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Inventory Unit.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_unit = function(form) {
        	$scope.reset(form);
            getItems();
        };
	}
]);