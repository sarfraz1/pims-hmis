'use strict';

angular.module('inventories').controller('StockReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'inventory_service', 'print_service', '$anchorScroll','$timeout',
    function($scope, $http, $stateParams, $location, Authentication, ngToast, inventory_service, print_service, $anchorScroll,$timeout) {

        $scope.authentication = Authentication;
        $scope.selectedSupplier = 'all';

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        else {
            // if($scope.authentication.user.roles === 'admin') {
            //     //$location.path('/admin-main-page');
            // }
            // else{
            //     $location.path('/signin')
            // }
        }

		$scope.storeData = [];
        $scope.storesToShow = [];
        $scope.prefixStore = 'storediv';
        $scope.headingsStores = [];
        $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var getAllStock = function(){
			$scope.totalstockprice = 0;
			$scope.totalpurchaseprice = 0;
			$scope.totalprofit = 0;
			$scope.processing = true;
            inventory_service.getStockReport().then(function (response) {
                for(var i=0;i<response.data.length;i++){
                   var totalQuantity = 0;
				   if(response.data[i].batch) {
					   for(var j=0;j<response.data[i].batch.length;j++){
						   if(response.data[i].batch[j].storeDescription == $scope.storeDescription) {
								totalQuantity = totalQuantity + response.data[i].batch[j].quantity;
						   }
					   }
				   }

				   if(response.data[i].inventory_pricing.length>0) {
						response.data[i].stockprice = Math.round(totalQuantity*response.data[i].inventory_pricing[response.data[i].inventory_pricing.length-1].sellingPrice);
						response.data[i].saleperunit = response.data[i].inventory_pricing[response.data[i].inventory_pricing.length-1].sellingPrice;
					} else {
						response.data[i].stockprice = 0;
					}
				   response.data[i].totalInStock = totalQuantity;
				   //if(response.data[i].stockprice !== 'Not defined')
					$scope.totalstockprice+=Number(response.data[i].stockprice);
                }
					$scope.totalstockprice =  $scope.totalstockprice.toFixed(2);
					$scope.inventoryStock = response.data;
					$scope.processing = false;
					inventory_service.getAverageStockPrice('All').then(function (response) {
                    //console.log(response.data);
					$scope.purchaseprices = response.data;
					for(var i=0;i<$scope.inventoryStock.length;i++) {
						for(var j=0;j<$scope.purchaseprices.length;j++) {
							if($scope.inventoryStock[i].id == $scope.purchaseprices[j].id) {
								$scope.inventoryStock[i].averagePrice = $scope.purchaseprices[j].averagePrice;
								$scope.inventoryStock[i].totalprice = $scope.inventoryStock[i].averagePrice*$scope.inventoryStock[i].totalInStock;
								$scope.inventoryStock[i].profit = Math.round($scope.inventoryStock[i].stockprice - $scope.inventoryStock[i].totalprice);
								$scope.totalpurchaseprice+=Number($scope.inventoryStock[i].totalprice);
								$scope.totalprofit+=Number($scope.inventoryStock[i].profit);
								break;
							}
						}
					}

				});
            }).catch(function(err){
                console.log(err);
            });

        };

        var getSuppliers = function(){
            inventory_service.get_inventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
                    $timeout(function() {
                        $scope.selectedSupplier = 'all';
                    }, 100);

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier list.',
                    dismissButton: true
                });
            });
        };

		var getStores = function(){
            inventory_service.get_inventory_stores()
                .then(function(response) {
                if (response.data.length > 0) {
                    $scope.storeData = [];
                    $scope.storeData = response.data;
                    $scope.storesToShow = [];
                    $scope.headingsStores = [];
                    $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.storesToShow.push({'description': response.data[i].description});
                    }

					$scope.storeDescription = response.data[0].description;

                    $scope.showStoresList = false;
                } else {
                    ngToast.create({
                        className: 'warning',
                        content: 'Warning: No Inventory Stores added.',
                        dismissButton: true
                    });
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
        };

		$scope.printreport = function() {
			var dt = moment(new Date());
			var ts = [];

			var obj = {
				'inventoryStock': $scope.inventoryStock,
				'totalAmounts' : $scope.totalstockprice,
				'totalpuchaseprice': $scope.totalpurchaseprice,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
      obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/inventories/views/stock-report-print.client.view.html',obj,
			function(){
			});
		};

        $scope.updateReport = function(){
			$scope.totalstockprice = 0;
            if($scope.selectedSupplier === 'all'){
                getAllStock();
            }
            else {
                $scope.processing = true;
                inventory_service.list_supplier_inventory_stock($scope.selectedSupplier, $scope.storeDescription)
                    .then(function(response) {

						for(var i=0;i<response.data.length;i++){

						   response.data[i].stockprice = Math.round(response.data[i].totalInStock*response.data[i].sellingPrice);

						   $scope.totalstockprice+=Number(response.data[i].stockprice);
						   response.data[i].saleperunit = response.data[i].sellingPrice;
						}
						$scope.totalstockprice =   $scope.totalstockprice.toFixed(2);
						$scope.inventoryStock = response.data;
						$scope.processing = false;
						inventory_service.getAverageStockPrice('All').then(function (response) {
							$scope.purchaseprices = response.data;
							for(var i=0;i<$scope.inventoryStock.length;i++) {
								for(var j=0;j<$scope.purchaseprices.length;j++) {
									if($scope.inventoryStock[i].id == $scope.purchaseprices[j].id) {
										$scope.inventoryStock[i].averagePrice = $scope.purchaseprices[j].averagePrice;
										$scope.inventoryStock[i].totalprice = $scope.inventoryStock[i].averagePrice*$scope.inventoryStock[i].totalInStock;
										$scope.inventoryStock[i].profit = $scope.inventoryStock[i].stockprice - $scope.inventoryStock[i].totalprice;
										break;
									}
								}
							}
						});
					}).catch(function (error) {
						ngToast.create({
							className: 'danger',
							content: 'Error: Unable to retrieve Customer/Supplier list.',
							dismissButton: true
						});
					});
            }
        };

        $scope.quantityFilter = function(item) {
            if ($scope.totalInStock === '')
                return item.totalInStock;
            else
                return item.totalInStock <= $scope.totalInStock;
        };

        $scope.selectStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.storeId = $scope.storeData[storeIndex]._id;
            $scope.storeDescription = $scope.storeData[storeIndex].description;
            $scope.showStoresList = false;
        };

		$scope.openStoreList = function() {
            $scope.showStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixStore + $scope.prefixStore));
            //console.log(setFocus);
            $timeout(function() {setFocus[0].focus()});
        };

		$scope.getReport = function() {
			if($scope.selectedSupplier == 'all' || !$scope.selectedSupplier) {
				getAllStock();
			} else {
				$scope.updateReport();
			}
		};

        $scope.init = function(){
            //getAllStock();
            getSuppliers();
			getStores();
            $scope.processing = false;
        };

        $scope.init();

    }
]);
