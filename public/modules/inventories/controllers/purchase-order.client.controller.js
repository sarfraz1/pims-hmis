'use strict';

// Inventories controller
angular.module('inventories').controller('purchaseOrderController', ['$scope', '$http','$stateParams', '$location', '$timeout', 'Authentication', 'inventory_service','ngToast','$anchorScroll','print_service',
    function($scope,$http ,$stateParams, $location, $timeout, Authentication, inventory_service,ngToast,$anchorScroll,print_service) {

        $scope.authentication = Authentication;

        if (!$scope.authentication.user) $location.path('/signin');

        $scope.showSearchMessage = false;
        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showGrid = false;
        $scope.columnSize = 300; //variable to set width of columns
        $scope.samplejson = [];
        $scope.searchOrderKeyword = '';
        // variable to store inventory pricing for supplier
        $scope.inventorySuppliers = [];
        // medicines simple picklist
        $scope.allMedicines = []; // for data retrieval
        $scope.allItems = []; // to be passed to directive
        $scope.prefix = 'meddiv';
        // purchase requisitions expandable picklist
        $scope.purchaseRequisitionsData = [];
        $scope.PR = [];
        $scope.expandableHeadingPR = 'itemsList';
        $scope.prefixPR = 'prdiv';
        // purchase order search simple picklist
        $scope.purchaseOrderData = [];
        $scope.prefixPO = 'podiv';
        $scope.headingsPO = [];
        $scope.PO = [];
        $scope.headingsPO.push({'alias': 'Purchase Order Number', 'name': 'purchaseOrderNumber', 'width': 30});
        $scope.headingsPO.push({'alias': 'Date', 'name': 'date', 'width': 30});
        $scope.headingsPO.push({'alias': 'Remarks', 'name': 'remarks', 'width': 38});
        // suppliers simple picklist
        $scope.suppliers = [];
        $scope.suppliersToShow = [];
        $scope.prefixSuppliers = 'suppliersdiv';
        $scope.headingsSuppliers = [];
        $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var dateConverter = function(dateinput){
          try{
            var todayTime = Date();
            var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate(), todayTime.getHours(), todayTime.getMinutes(), todayTime.getSeconds()));
            utcDate = utcDate.toUTCString();
          }
          catch(error){
            return dateinput;
          }
        };

        var getSuppliers = function(){
            inventory_service.get_inventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
                    $scope.headingsSuppliers = [];
                    $scope.headingsSuppliers.push({'alias': 'Description', 'name': 'description', 'width': 49});
                    $scope.headingsSuppliers.push({'alias': 'NTN', 'name': 'NTN', 'width': 49});
                    $scope.suppliersToShow = [];
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.suppliersToShow.push({'description': response.data[i].description, 'NTN': response.data[i].NTN});
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier List.',
                    dismissButton: true
                });
            });
        };

        var getTransactionCategories = function(){
          inventory_service.getTransactionCategories()
            .then(function(response) {
              $scope.transcationCategories = response.data;
              $scope.transcationCategories.unshift({'purchaseType': 'Default'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Document Category List.',
                    dismissButton: true
                });
            });
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var todayTime = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy + ' ' + todayTime.getHours() + ':' + todayTime.getMinutes() + ':' + todayTime.getSeconds();
            return today;
        };

        var getPurchaseRequisitions = function(){
          inventory_service.getPurchaseRequisitions()
            .then(function(response) {
              $scope.PR = [];
              $scope.headingsPR = [];
              $scope.expandableHeadingsPR = [];
              $scope.headingsPR.push({'alias': 'Purchase Requisition Code', 'name': 'name', 'width': 50});
              $scope.headingsPR.push({'alias': 'Creation Date', 'name': 'date', 'width': 50});
              $scope.expandableHeadingsPR.push({'alias': 'Item Code', 'name': 'itemcode', 'width': 20});
              $scope.expandableHeadingsPR.push({'alias': 'Description', 'name': 'description', 'width': 25});
              $scope.expandableHeadingsPR.push({'alias': 'Quantity', 'name': 'quantity', 'width': 10});
              $scope.expandableHeadingsPR.push({'alias': 'Remarks', 'name': 'remarks', 'width': 20});
              $scope.expandableHeadingsPR.push({'alias': 'Requested By', 'name': 'requestedBy', 'width': 25});
              $scope.purchaseRequisitionsData = response.data;
              for (var i = 0; i < response.data.length; i++) {
                var creationDate = getDateFormat(response.data[i].date);
                $scope.PR.push({'code': response.data[i].code, 'date': creationDate, 'itemsList': []});
                for (var j = 0; j < response.data[i].itemsList.length; j++) {
                  var remarks = '';
                  var requestedBy = '';
                  if (response.data[i].remarks === '' || response.data[i].remarks === undefined || response.data[i].remarks === null) {
                    remarks = 'No Remarks';
                  } else {
                    remarks = response.data[i].remarks;
                  }
                  if (response.data[i].requestedBy === '' || response.data[i].requestedBy === undefined || response.data[i].requestedBy === null) {
                    requestedBy = 'Default';
                  } else {
                    requestedBy = response.data[i].requestedBy;
                  }
                  $scope.PR[i].itemsList.push({'itemcode': response.data[i].itemsList[j].code, 'description': response.data[i].itemsList[j].description, 'quantity': response.data[i].itemsList[j].quantity, 'remarks': remarks, 'requestedBy': requestedBy});
                }
              }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Purchase Requisition List.',
                    dismissButton: true
                });
            });
        };

        var pushItems = function(index) {
          var formula = '';
          if ($scope.allMedicines[index].formula === '' || $scope.allMedicines[index].formula == null)
              formula = 'No Formula';
          else
              formula = $scope.allMedicines[index].formula;
          $scope.allItems.push({'code': $scope.allMedicines[index].code, 'description': $scope.allMedicines[index].description, 'formula': formula});
        };

        $scope.loadMoreItems = function() {
          var length = $scope.allItems.length + 100;
          if (length < 1000) {
            for (var i = length - 100; i < length; i++) {
              if ($scope.allMedicines[i] != undefined) {
                pushItems(i);
              }
            }
          }
        };

        $scope.searchMedicines = function(item) {
          $scope.allItems = [];
          for (var i = 0; i < $scope.allMedicines.length; i++) {
            if ($scope.allItems.length > 100) {
              break;
            }
            if (item.length === 0) {
              pushItems(i);
            } else {
              for (var key in $scope.allMedicines[i]) {
                for (var j = 0; j < $scope.headingsMedicines.length; j++) {
                  if (key === $scope.headingsMedicines[j].name) {
                    if ($scope.allMedicines[i][key].toLowerCase().includes(item.toLowerCase())) {
                      pushItems(i);
                    }
                  }
                }
              }
            }
          }
        };

        var getAllItems = function(){
            $scope.processing = true;
            inventory_service.listAllItems()
                .then(function(response) {
                    $scope.allMedicines = response;
                    $scope.allItems = [];
                    $scope.headingsMedicines = [];
                    var headingsMedicines = [];
                    headingsMedicines.push({'alias': 'Item Code', 'name': 'code', 'width': 32});
                    headingsMedicines.push({'alias': 'Description', 'name': 'description', 'width': 33});
                    headingsMedicines.push({'alias': 'Formula', 'name': 'formula', 'width': 33});
                    $scope.headingsMedicines = angular.copy(headingsMedicines);

                    var max;
                    if($scope.allMedicines.length>100){
                        max=100;
                    }else
                        max = $scope.allMedicines.length;

                    for (var i = 0; i < max; i++) {
                      pushItems(i);
                    }
                    $scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

        var getItemPricing = function(index){

            if($scope.itemsList[index]!==undefined){
                inventory_service.view_inventory_pricing($scope.itemsList[index]).then(function(response) {
                  $scope.itemsList[index].rate = response.purchasingPrice;
                  //$scope.itemsList[index].requestedBy = $scope.authentication.user.displayName;
                  index++;
                  getItemPricing(index);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
        };

        var calculate_total = function(){
          var totalAmount = 0;

          for(var i=0;i<$scope.itemsList.length;i++){
            var totalTax = 0;
            var totalDiscount = 0;
            if($scope.itemsList[i].discountType === 'PerItem'){
              totalDiscount = $scope.itemsList[i].quantity*$scope.itemsList[i].discount;
            }
            else if($scope.itemsList[i].discountType === 'perc'){
              totalDiscount = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].discount/100);
            }
            else if($scope.itemsList[i].discountType === 'lumsum'){
              totalDiscount = $scope.itemsList[i].discount;
            }

            if($scope.itemsList[i].taxType === 'perc') {
              totalTax = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].saleTax/100);
            }
            else if($scope.itemsList[i].taxType === 'lumsum'){
              totalTax = $scope.itemsList[i].saleTax;
            }
            $scope.itemsList[i].netValue = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)+totalTax-totalDiscount;
            $scope.itemsList[i].netValue = Number($scope.itemsList[i].netValue.toFixed(2));
            if(isNaN($scope.itemsList[i].netValue)){
              $scope.itemsList[i].netValue = 0;
            }
            totalAmount = totalAmount+$scope.itemsList[i].netValue;
            totalAmount = Number(totalAmount.toFixed(2));
          }
          return totalAmount;
        };

        var handleResponse = function(response) {
            if (response.data.length > 0) {
                $scope.showGrid = true;
                $scope.purchaseOrderData = [];
                $scope.purchaseOrderData = response.data;
                $scope.headingsPO = [];
                $scope.PO = [];

                $scope.headingsPO.push({'alias': 'Purchase Order Number', 'name': 'purchaseOrderNumber', 'width': 30});
                $scope.headingsPO.push({'alias': 'Date', 'name': 'date', 'width': 30});
                $scope.headingsPO.push({'alias': 'Remarks', 'name': 'remarks', 'width': 38});
                for (var i = 0; i < response.data.length; i++) {
                  var creationDate = getDateFormat(response.data[i].date);
                  var remarks = '';
                  if (response.data[i].remarks === '' || response.data[i].remarks === undefined || response.data[i].remarks === null) {
                    remarks = 'No Remarks';
                  } else {
                    remarks = response.data[i].remarks;
                  }
                  $scope.PO.push({'purchaseOrderNumber': response.data[i].purchaseOrderNumber, 'date': creationDate, 'remarks': remarks});
                }
                var setFocus = angular.element(document.querySelector('#' + $scope.prefixPO + $scope.prefixPO));
                $timeout(function() {setFocus[0].focus()});
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'No results found.',
                    dismissButton: true
                });
            }
        };

        $scope.callbackfn = function(selecteditem) { //callback function to get object from directive
            var itemIndex = getIndex($scope.purchaseOrderData, 'purchaseOrderNumber', selecteditem[0].purchaseOrderNumber);
            var selected_item = $scope.purchaseOrderData[itemIndex];
            var handled = false;
            for (var i = 0; i < selected_item.itemsList.length; i++) {
              if (selected_item.itemsList[i].handledQty > 0) {
                handled = true;
                break;
              }
            }
            if (handled) {
                ngToast.create({
                    className: 'warning',
                    content: 'Cannot update Purchase Order already processed.',
                    dismissButton: true
                });
                $scope.UpdateItem = false;
                $scope.showGrid = false;
                $scope.reset();
            } else {
              $scope.purchaseOrder = selected_item;
              $scope.purchaseOrder.supplierQuotationNo = parseFloat(selected_item.supplierQuotationNo);
              $scope.purchaseOrder.date = new Date(selected_item.date);
              $scope.purchaseOrder.supplierQuotationDate = new Date(selected_item.supplierQuotationDate);
              $scope.itemsList = selected_item.itemsList;
              for (i = 0; i < selected_item.itemsList.length; i++) {
                if (selected_item.itemsList[i].purchaseRequisitionCode === '') {
                  $scope.purchaseOrder.request = 'Direct';
                }
                if (selected_item.itemsList[i].purchaseRequisitionCode !== '') {
                  $scope.purchaseOrder.request = 'Purchase Requisition';
                }
                $scope.itemsList[i].date = new Date(selected_item.itemsList[i].date);
                $scope.itemsList[i].estDeliveryDate = new Date(selected_item.itemsList[i].estDeliveryDate);
              }
              $scope.calculate_total_net_amount();
              $scope.UpdateItem = true;
            }
            $scope.showGrid = false;
            $scope.searchPurchaseKeyword = '';
        };

		var addItemtoList = function(code, price, stock) {
				inventory_service.get_inventory_by_code(code).then(function (response) {
                 // var medIndex = getIndex($scope.allMedicines, 'code', $scope.inventorySuppliers[i].inventoryCode);
					var quantity = 0;
					for(var i =0;i<stock[0].batch.length;i++) {
						quantity+=stock[0].batch[i].quantity;
					}
					  var item = angular.copy(response.data);

						quantity = quantity/item.purchase_to_storage;
						quantity = quantity/item.storage_to_selling;
						quantity = Math.floor(quantity);
					  item.rate = price;
					  item.discountType = 'perc';
					  item.discount = 0;
					  item.taxType = 'perc';
					  item.saleTax = 0;
					  item.netValue = undefined;
					  if(item.par_level > 0 && item.reorder_level>0) {
						  if(quantity < item.reorder_level ) {
							  item.quantity = item.par_level - quantity;
							  $scope.itemsList.push(item);
							  $scope.calculate_total_net_amount();
						  }
					  } else {
						if($scope.addallmed == true) {
							$scope.itemsList.push(item);
						}
					  }

				  });
		}

        $scope.selectSupplier = function(supplier){
            var supplierIndex = getIndex($scope.suppliers, 'description', supplier[0].description);
            inventory_service.get_inventory_pricing_supplier_stock($scope.suppliers[supplierIndex]._id).then(function (response) {
              $scope.inventorySuppliers = response.data;
              var itemsArray = [];
              for (var i = 0; i < $scope.inventorySuppliers.length; i++) {
					addItemtoList($scope.inventorySuppliers[i].inventoryCode, $scope.inventorySuppliers[i].purchasingPrice, $scope.inventorySuppliers[i].stock);
              }
            })
            $scope.selectedSupplier = $scope.suppliers[supplierIndex];
            $scope.emailAddressHtml = $scope.suppliers[supplierIndex].emailAddress;
            $scope.purchaseOrder.supplierId = $scope.suppliers[supplierIndex]._id;
            $scope.purchaseOrder.supplierDescription = $scope.suppliers[supplierIndex].description;
			$scope.purchaseOrder.paymentMode = $scope.suppliers[supplierIndex].paymentMode;
            $scope.showSupplierList = false;
        };

        $scope.getPendingPurchaseOrders = function() {
          if ($scope.searchOrderKeyword.length === 0) {
            inventory_service.getPurchaseOrders().then(function (response) {
                handleResponse(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Purchase Orders.',
                    dismissButton: true
                });
            });
          } else if ($scope.searchOrderKeyword.length > 0) {
            inventory_service.getSearchedPurchaseOrders($scope.searchOrderKeyword).then(function (response) {
                handleResponse(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Purchase Orders.',
                    dismissButton: true
                });
            });
          }
        };

        $scope.calculate_total_net_amount = function(){
          $scope.totalAmount = calculate_total();
        };

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };

        $scope.selectedItems = function(obj){
          for (var j = 0; j < obj.length; j++) {
            var objIndex = getIndex($scope.purchaseRequisitionsData, 'code', obj[j].code);
            var objtemp = $scope.purchaseRequisitionsData[objIndex];
            var subobjIndex = getIndex(objtemp.itemsList, 'code', obj[j].itemcode);
            var subobjtemp = objtemp.itemsList[subobjIndex];
            var temp = {
              'code' : subobjtemp.code,
              'purchaseRequisitionCode' : objtemp.code,
              'purchaseRequisitionRemarks' : objtemp.remarks,
              'date' : objtemp.date,
              'description' : subobjtemp.description,
              'estDeliveryDate' : subobjtemp.estDeliveryDate,
              'rate' : subobjtemp.estRate,
              'quantity' : subobjtemp.quantity,
              'remarks' : subobjtemp.remarks,
              'requestedBy' : subobjtemp.requestedBy,
              'discountType' : 'perc',
              'discount' : 0,
              'taxType' : 'perc',
              'saleTax' : 0
            };
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===temp.code && $scope.itemsList[i].purchaseRequisitionCode === temp.purchaseRequisitionCode){
                    medExists = true;
                }
            }
            if(medExists === false){
              $scope.itemsList.push(temp);
              $scope.calculate_total_net_amount();
            }
          }
          $scope.showPurchaseRequisitions = false;
        };

        $scope.populateList = function(selectedItems){
            for (var i = 0; i < selectedItems.length; i++) {
              var medExists = false;
                for(var j=0;j<$scope.itemsList.length;j++){
                    if($scope.itemsList[j].code===selectedItems[i].code){
                        medExists = true;
                    }
                }
                if(medExists === false){
                  var medIndex = getIndex($scope.allMedicines, 'code', selectedItems[i].code);
                  $scope.itemsList.push($scope.allMedicines[medIndex]);
                }
            }
            for (var i = $scope.allMedicines.length - 1; i >= 0; i--) {
              $scope.allMedicines[i].discountType = 'perc';
              $scope.allMedicines[i].discount = 0;
              $scope.allMedicines[i].taxType = 'perc';
              $scope.allMedicines[i].saleTax = 0;
              $scope.allMedicines[i].netValue = undefined;
            }
            $scope.showAllItems = false;
            getItemPricing(0);
        };

        $scope.add_medicine = function(med_obj) {
          var alreadyExists = false;
          for (var i = 0; i < $scope.itemsList.length; i++) {
            if ($scope.itemsList[i].code === med_obj.code) {
              $scope.itemsList[i].quantity++;
              alreadyExists = true;
            }
          }
          if (!alreadyExists)
            $scope.itemsList.push(med_obj);

          $scope.totalAmount = calculate_total();
          $scope.show_dropdown = false;
          $scope.search_keyword = '';
        };

        $scope.itemsSearchTypeahead = function(val){
            return inventory_service.search_by_keyword(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        $scope.selectedSearchItem = function(obj){
            obj.discountType = 'perc';
            obj.discount = 0;
            obj.taxType = 'perc';
            obj.saleTax = 0;
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===obj.code){
                    medExists = true;
                }
            }
            if(medExists === false){
              inventory_service.view_inventory_pricing(obj).then(function(response) {
                  obj.rate = response.purchasingPrice;
                  $scope.itemsList.push(obj);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Item Purchase Price.',
                        dismissButton: true
                    });
                });
            }
            $scope.searchKeyword = '';
        };

        $scope.remove_medicine = function(index) {
          $scope.itemsList.splice(index,1);
        };

        $scope.reset = function(form){
          if (form) {
            form.$setPristine();
            form.$setUntouched();
            form.submitted = false;
          }
          $location.hash('headerid');
          $anchorScroll.yOffset = 100;
          $anchorScroll();

          $scope.purchaseOrder = {
            'purchaseOrderNumber' : '',
            'request' : 'Direct',
            'date': new Date(),
            'transcationCategory' : 'Default',
            'supplierId': '',
            'supplierDescription':''
          };

          $scope.processing = false;
          $scope.totalAmount = 0;
          $scope.itemsList = [];
          $scope.UpdateItem = false;
          $scope.showPurchaseRequisitions = false;
          $scope.showAllItems = false;
          $scope.itemsQty = false;
          $scope.selectedSupplier = undefined;
          $scope.mode = 'Basic';

          getTransactionCategories();
          getPurchaseRequisitions();
          getSuppliers();
          //getAllItems();
        };

        $scope.delete_purchase_order = function() {
            $scope.confirmationPopup = 'confirmation-popup-right';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            inventory_service.delete_purchase_order($scope.purchaseOrder).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Purchase Order Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Purchase Order.',
                    dismissButton: true
                });
            });
        };

        $scope.update_purchase_order = function(purchaseOrder, form) {
          purchaseOrder.itemsList = $scope.itemsList;
          for (var i = 0; i < $scope.itemsList.length; i++) {
            if ($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity === undefined || $scope.itemsList[i].quantity < 1) {
              $scope.itemsQty = true;
              return false;
            }
          }

          if (purchaseOrder.date === null) {
            purchaseOrder.date = new Date();
          }

          if (purchaseOrder.supplierQuotationDate === null) {
            purchaseOrder.supplierQuotationDate = new Date();
          }

          purchaseOrder.date = dateConverter(purchaseOrder.date);
          purchaseOrder.supplierQuotationDate = dateConverter(purchaseOrder.supplierQuotationDate);

          if (purchaseOrder.request !== undefined && purchaseOrder.date !== undefined && $scope.itemsList.length !== 0) {
            $scope.processing = true;
            inventory_service.update_purchase_order(purchaseOrder).then(function (response) {
              ngToast.create({
                className: 'success',
                content: 'Purchase Order Updated Successfully',
                dismissButton: true
              });
              $scope.printData = response.data;
              $scope.printData.total = 0;
              for(var k=0;k<$scope.printData.itemsList.length;k++){
                $scope.printData.total = $scope.printData.total + $scope.printData.itemsList[k].netValue;
              }
              $scope.hospitallogo = $scope.hosDetail[0].image_url;
              $scope.hospitalname = $scope.hosDetail[0].title;
              $scope.hospitalAddress = $scope.hosDetail[0].address;
              $scope.hospitalPHNumber = $scope.hosDetail[0].number;
              $scope.hospitalEmail = $scope.hosDetail[0].email;
              print_service.printFromScope('/modules/inventories/views/purchase-order-print.client.view.html',$scope,function(){
                $scope.reset(form);
                $scope.processing = false;
              });
            }).catch(function (error) {
              $scope.processing = false;
              ngToast.create({
                className: 'danger',
                content: 'Error: Unable to update Purchase Order.',
                dismissButton: true
              });
            });
          }
        };

        var hospitalDetails = {};
        var orderObject = {};

        inventory_service.get_hospital_details().then(function (response) {
            if (response.data.length > 0) {
                var getHospitalDetails = response.data[0];
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Hospital details do not exist.',
                    dismissOnTimeout: false
                });
                var getHospitalDetails = {
                    name: 'Hospital',
                    addressLineOne: 'Address Line One',
                    addressLineTwo: 'Address Line Two',
                    phone: [{
                        number: '0000000'
                    }]
                };
            }
            orderObject.hospitalName = getHospitalDetails.name;
            orderObject.hospitalAddressOne = getHospitalDetails.addressLineOne;
            orderObject.hospitalAddressTwo = getHospitalDetails.addressLineTwo;
            orderObject.hospitalPhone = getHospitalDetails.phone[0].number;
        }).catch(function (error) {
            ngToast.create({
                className: 'danger',
                content: 'Error: Unable to retrieve Hospital details.',
                dismissOnTimeout: false
            });
        });

        var get_time = function() {
            var d = new Date();
            var hr = d.getHours();
            var min = d.getMinutes();
            if (min < 10) {
                min = '0' + min;
            }
            var ampm = hr < 12 ? 'AM' : 'PM';
            if (hr > 12) {
                hr = hr - 12;
            }
            return (hr + ':' + min + ' ' + ampm);
        };

        var get_date = function() {
            var d = new Date();
            return (d.toLocaleDateString());
        };

        $scope.submit_purchase_order = function(purchaseOrder,form,sendEmail){
          purchaseOrder.itemsList = $scope.itemsList;
          // chrome print
          var total = 0;
          orderObject.itemsList = [];
          for(var i=0;i<$scope.itemsList.length;i++){
                // chrome print
                total += $scope.itemsList[i].netValue;
                // chrome print
                orderObject.itemsList.push({'price': $scope.itemsList[i].netValue, 'description': $scope.itemsList[i].description, 'quantity': $scope.itemsList[i].quantity});
                if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined){
                    $scope.itemsQty = true;
                    return false;
                }
            }

          if (purchaseOrder.date === null || purchaseOrder.date === undefined) {
            purchaseOrder.date = new Date();
          }

          if (purchaseOrder.supplierQuotationDate === null || purchaseOrder.supplierQuotationDate === undefined) {
            purchaseOrder.supplierQuotationDate = new Date();
          }

          if(purchaseOrder.request!==undefined && purchaseOrder.date!==undefined && $scope.itemsList.length!==0){

            // chrome print
            orderObject.username = $scope.authentication.user.displayName;
            orderObject.purchaseOrderDate = get_date();
            orderObject.purchaseOrderTime = get_time();
            orderObject.purchaseOrderNumber = '0';
            orderObject.from = 'purchase order';
            orderObject.total = total;
            orderObject.supplier = purchaseOrder.supplierDescription;
            // chrome.runtime.sendMessage('chklklmchflbhdhlheeddigflianadik', orderObject);
            $scope.processing = true;
            purchaseOrder.date = dateConverter(purchaseOrder.date);
            purchaseOrder.supplierQuotationDate = dateConverter(purchaseOrder.supplierQuotationDate);

            inventory_service.create_purchase_order(purchaseOrder).then(function(response){
              ngToast.create({
                className: 'success',
                content: 'Purchase Order Created Successfully',
                dismissButton: true
              });
              $scope.printData = response.data;
              $scope.printData.total = 0;
			  $scope.createdDate = moment($scope.printData.created).format('DD-MM-YYYY hh:mma');
			  $scope.printingDate = moment().format('DD-MM-YYYY hh:mma');

              for(var k=0;k<$scope.printData.itemsList.length;k++){
                $scope.printData.total = $scope.printData.total + $scope.printData.itemsList[k].netValue;
              }
              $scope.hospitallogo = $scope.hosDetail[0].image_url;
              $scope.hospitalname = $scope.hosDetail[0].title;
              $scope.hospitalAddress = $scope.hosDetail[0].address;
              $scope.hospitalPHNumber = $scope.hosDetail[0].number;
              $scope.hospitalEmail = $scope.hosDetail[0].email;

              print_service.printFromScope('/modules/inventories/views/purchase-order-print-new.client.view.html',$scope,function(){
                if(sendEmail===true){
                  print_service.printAndEmail('/modules/inventories/views/purchase-order-print-new.client.view.html',$scope,"Purchase Order",function(){
                    $scope.reset(form);
                    $scope.processing = false;
                  });
                }else{
                  $scope.reset(form);
                  $scope.processing = false;
                }

              });
            }).catch(function (error) {
              $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error : '+error.data.message,
                    dismissButton: true
                });
            });
          }
        };

        $scope.getItems = function(){
          inventory_service.search_by_keyword($scope.searchKeyword)
            .then(function(response) {
              $scope.showGrid = true;
              $scope.samplejson = [];
              $scope.samplejson = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        $scope.reset();

        $scope.openItemsList = function() {
            $scope.showAllItems = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openPurchaseRequisitions = function() {
            $scope.showPurchaseRequisitions = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixPR + $scope.prefixPR));
            $timeout(function() {setFocus[0].focus()});
        };


        $scope.openSupplierList = function() {
            $scope.showSupplierList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixSuppliers + $scope.prefixSuppliers));
            $timeout(function() {setFocus[0].focus()});
        };
      }
]);
