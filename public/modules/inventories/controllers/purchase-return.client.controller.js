'use strict';

// Inventories controller
angular.module('inventories').controller('purchaseReturnController', ['$scope', '$http','$stateParams', '$location', '$timeout', 'Authentication', 'inventory_service','ngToast','$anchorScroll','print_service',
    function($scope,$http ,$stateParams, $location, $timeout, Authentication, inventory_service,ngToast,$anchorScroll,print_service) {

        $scope.authentication = Authentication;

        if (!$scope.authentication.user) $location.path('/signin');

        // GRNs expandable picklist
        $scope.GRNData = [];
        $scope.GRNs = [];
        $scope.prefixGRN = 'grndiv';
        $scope.headingsGRN = [];
        $scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 49});
        $scope.headingsGRN.push({'alias': 'Creation Date', 'name': 'date', 'width': 49});
        $scope.expandableHeadingGRN = 'itemsList';
        $scope.expandableHeadingsGRN = [];
        $scope.expandableHeadingsGRN.push({'alias': 'Item Code', 'name': 'code', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Description', 'name': 'description', width: 20});
        $scope.expandableHeadingsGRN.push({'alias': 'Batch', 'name': 'batchID', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Date', 'name': 'date', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Expiry Date', 'name': 'expiryDate', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Expiry Enabled', 'name': 'expiry_date_enabled', width: 20});
        $scope.expandableHeadingsGRN.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Rate', 'name': 'rate', width: 10});
        $scope.expandableHeadingsGRN.push({'alias': 'Net Value', 'name': 'netValue', width: 20});

		$scope.confirmationPopup = 'hide-popup';
		// store simple picklist
        $scope.storeData = [];
        $scope.storesToShow = [];
		$scope.prefixfromStore = 'fromstorediv';
        $scope.headingsStores = [];
        $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});

        // medicines simple picklist
        $scope.allMedicines = []; // for data retrieval
        $scope.allItems = []; // to be passed to directive
        $scope.prefix = 'meddiv';

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };

        var getTransactionCategories = function(){
          inventory_service.getTransactionCategories()
            .then(function(response) {
              $scope.transcationCategories = response.data;
              $scope.transcationCategories.unshift({'purchaseType': 'Default'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Document Category List.',
                    dismissButton: true
                });
            });
        };

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        };

        var getGRN = function(){
          inventory_service.getAllGRN()
            .then(function(response) {
              $scope.GRNData = [];
              $scope.GRNData = response.data;
              $scope.GRNs = [];
              $scope.headingsGRN = [];
              $scope.headingsGRN.push({'alias': 'GRN Number', 'name': 'GRNNumber', 'width': 49});
              $scope.headingsGRN.push({'alias': 'Creation Date', 'name': 'date', 'width': 49});
              $scope.expandableHeadingsGRN = [];
              $scope.expandableHeadingsGRN.push({'alias': 'Item Code', 'name': 'code', width: 15});
              $scope.expandableHeadingsGRN.push({'alias': 'Description', 'name': 'description', width: 25});
              $scope.expandableHeadingsGRN.push({'alias': 'Batch', 'name': 'batchID', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Expiry Date', 'name': 'expiryDate', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Expiry Enabled', 'name': 'expiry_date_enabled', width: 20});
              $scope.expandableHeadingsGRN.push({'alias': 'Quantity', 'name': 'quantity', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Rate', 'name': 'rate', width: 10});
              $scope.expandableHeadingsGRN.push({'alias': 'Net Value', 'name': 'netValue', width: 20});
              for (var i = 0; i < response.data.length; i++) {
                var creationDate = getDateFormat(response.data[i].date);
                $scope.GRNs.push({'GRNNumber': response.data[i].GRNNumber, 'date': creationDate, 'itemsList': []});
                for (var j = 0; j < response.data[i].itemsList.length; j++) {
                  // no check for expiry date enabled/disabled
                  var expiryDate = '';
                  if (response.data[i].itemsList[j].expiryDate === null || response.data[i].itemsList[j].expiryDate === undefined || response.data[i].itemsList[j].expiryDate === '')
                    expiryDate = 'Disabled';
                  else {
                   expiryDate = getDateFormat(response.data[i].itemsList[j].expiryDate);
                  }
                  $scope.GRNs[i].itemsList.push({'code': response.data[i].itemsList[j].code, 'description': response.data[i].itemsList[j].description, 'batchID': response.data[i].itemsList[j].batchID, 'expiryDate': expiryDate,'expiry_date_enabled': response.data[i].itemsList[j].expiry_date_enabled, 'quantity': response.data[i].itemsList[j].quantity, 'rate': response.data[i].itemsList[j].rate, 'netValue': response.data[i].itemsList[j].netValue});
                }
              }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve GRN List.',
                    dismissButton: true
                });
            });
        };


        var calculate_total = function(){
          var totalAmount = 0;

          for(var i=0;i<$scope.itemsList.length;i++){
            var totalTax = 0;
            var totalDiscount = 0;
            if($scope.itemsList[i].discountType === 'PerItem'){
              totalDiscount = $scope.itemsList[i].quantity*$scope.itemsList[i].discount;
            }
            else if($scope.itemsList[i].discountType === 'perc'){
              totalDiscount = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].discount/100);
            }
            else if($scope.itemsList[i].discountType === 'lumsum'){
              totalDiscount = $scope.itemsList[i].discount;
            }

            if($scope.itemsList[i].taxType === 'perc') {
              totalTax = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)*($scope.itemsList[i].saleTax/100);
            }
            else if($scope.itemsList[i].taxType === 'lumsum'){
              totalTax = $scope.itemsList[i].saleTax;
            }
            $scope.itemsList[i].netValue = ($scope.itemsList[i].quantity*$scope.itemsList[i].rate)+totalTax-totalDiscount;
            if(isNaN($scope.itemsList[i].netValue)){
              $scope.itemsList[i].netValue = 0;
            }
            totalAmount = totalAmount+$scope.itemsList[i].netValue;
          }
          return totalAmount;
        };

        $scope.calculate_total_net_amount = function(){
          $scope.totalAmount = calculate_total();
        };

        $scope.selectedItems = function(obj){

            for (var j = 0; j < obj.length; j++) {
                var objIndex = -1;
                for (var x = 0; x < $scope.GRNData.length; x++) {
                    if ($scope.GRNData[x].GRNNumber === obj[j].GRNNumber) {
                        objIndex = x;
                    }
                }
                var objtemp = $scope.GRNData[objIndex];
                var subobjIndex = -1;
                for (var y = 0; y < objtemp.itemsList.length; y++) {
                    if (objtemp.itemsList[y].code === obj[j].code) {
                        subobjIndex = y;
                    }
                }
                var subobjtemp = objtemp.itemsList[subobjIndex];
                var dateObj;
                if(subobjtemp.expiryDate!==undefined)
                  dateObj = new Date(subobjtemp.expiryDate);
                else
                  dateObj = undefined;
                var temp = {
                  'code' : subobjtemp.code,
                  'GRNNumber' : objtemp.GRNNumber,
                  'date' : objtemp.date,
                  'expiryDate' : dateObj,
                  'description' : subobjtemp.description,
                  'rate' : subobjtemp.rate,
                  'quantity' : subobjtemp.quantity,
                  'expiry_date_enabled' : subobjtemp.expiry_date_enabled
                };
                var medExists = false;
                for (var i = 0; i < $scope.itemsList.length; i++) {
                    if ($scope.itemsList[i].code === temp.code && $scope.itemsList[i].GRNNumber === temp.GRNNumber){
                        $scope.itemsList[i] = temp;
                        medExists = true;
                    }
                }
                if (medExists === false){
                    $scope.itemsList.push(temp);
                    $scope.totalAmount = calculate_total();
                }
            }
            $scope.showGRNs = false;
        };

        $scope.populateList = function(selectedItems){
            for (var i = 0; i < selectedItems.length; i++) {
              var medExists = false;
                for (var j = 0;j < $scope.itemsList.length; j++){
                    if($scope.itemsList[j].code === selectedItems[i].code) {
                        medExists = true;
                    }
                }
                if(medExists === false){
                  var medIndex = getIndex($scope.allMedicines, 'code', selectedItems[i].code);
                  $scope.itemsList.push($scope.allMedicines[medIndex]);
                }
            }
            for (var i = $scope.allMedicines.length - 1; i >= 0; i--) {
              $scope.allMedicines[i].discountType = 'perc';
              $scope.allMedicines[i].discount = 0;
              $scope.allMedicines[i].taxType = 'perc';
              $scope.allMedicines[i].saleTax = 0;
              $scope.allMedicines[i].netValue = undefined;
            }
            $scope.showAllItems = false;
        };

		$scope.search_medicines_stock = function() {
			if ($scope.name_search_keyword.length > 0) {
				inventory_service.search_inventory_by_name_store($scope.name_search_keyword, $scope.purchaseOrder.storeId).then(function (response) {
					$scope.searched_medicines = response;
					$scope.show_dropdown = true;
				});
			}

			if ($scope.name_search_keyword.length === 0) {
				$scope.show_dropdown = false;
			}
		};

        $scope.remove_medicine = function(index) {
          $scope.itemsList.splice(index,1);
        };

		$scope.batch_selected = function(index, batchId) {
			for(var i=0;i<$scope.itemsList[index].batches.length;i++) {
				if($scope.itemsList[index].batches[i].batch == batchId) {
					$scope.itemsList[index].availableQuantity = $scope.itemsList[index].batches[i].qty;
					$scope.itemsList[index].expiryDate = $scope.itemsList[index].batches[i].expiry;
				}
			}
		}

		$scope.add_search_medicine = function(med_obj) {
			$scope.name_search_keyword = '';
			$scope.show_dropdown = false;

			if(getIndex($scope.itemsList,"code", med_obj.code) == -1) {
				med_obj.quantity = 1;
				$scope.itemsList.push(med_obj);
				var ind = $scope.itemsList.length-1;
				inventory_service.get_inventory_stock(med_obj.code).then(function (response) {
					var med = response.data;
					var batches = [];

					if(med.batch) {
						for(var i=0;i<med.batch.length;i++) {

							if(med.batch[i].quantity > 0) {
								batches.push({
										batch: med.batch[i].batchID,
										grn: med.batch[i].grnNumber,
										expiry: med.batch[i].expiryDate,
										qty: med.batch[i].quantity});
							}
						}
					}
					$scope.itemsList[ind].batches = batches;
				});

			} else {
				ngToast.create({
                    className: 'warning',
                    content: 'Already added in list',
                    dismissButton: true
                });
			}
		};

        $scope.add_medicine = function(med_obj) {
          var alreadyExists = false;
          for (var i = 0; i < $scope.itemsList.length; i++) {
            if ($scope.itemsList[i].code === med_obj.code) {
              $scope.itemsList[i].quantity++;
              alreadyExists = true;
            }
          }
          if (!alreadyExists)
            $scope.itemsList.push(med_obj);

          $scope.totalAmount = calculate_total();
          $scope.show_dropdown = false;
          $scope.search_keyword = '';
        };



		var getStores = function(){
            inventory_service.get_inventory_stores()
                .then(function(response) {
                if (response.data.length > 0) {
                    $scope.storeData = [];
                    $scope.storeData = response.data;
                    $scope.storesToShow = [];
                    $scope.headingsStores = [];
                    $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.storesToShow.push({'description': response.data[i].description});
                    }

				} else {
                    ngToast.create({
                        className: 'warning',
                        content: 'Warning: No Inventory Stores added.',
                        dismissButton: true
                    });
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

        $scope.selectedSearchItem = function(obj){
            obj.discountType = 'perc';
            obj.discount = 0;
            obj.taxType = 'perc';
            obj.saleTax = 0;
            var medExists = false;
            for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].code===obj.code){
                    medExists = true;
                }
            }
            if(medExists === false)
              $scope.itemsList.push(obj);
            $scope.searchKeyword = '';
        };


        $scope.reset = function(form){

			$scope.btnType = "Save";
			$scope.btnDelshow = false;

          if (form) {
            form.$setPristine();
            form.$setUntouched();
            form.submitted = false;
          }
          $location.hash('headerid');
          $anchorScroll.yOffset = 100;
          $anchorScroll();

          $scope.purchaseOrder = {
            'purchaseReturnNumber' : '',
            'request' : 'Direct',
            'date' : new Date(),
            'transcationCategory' : 'Default'
          };

          $scope.processing = false;
          $scope.totalAmount = 0;
          $scope.itemsList = [];
          $scope.showGRNs = false;
          $scope.showAllItems = false;
          $scope.itemsQty = false;

          getTransactionCategories();
		  getStores();
          //getGRN();
          //getAllItems();
        };

		$scope.delete = function() {
            $scope.confirmationPopup = 'confirmation-popup-right';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            inventory_service.DeletePurchaseReturn($scope.purchaseOrder.purchaseReturnNumber).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Purchase Return Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete purchase return.',
                    dismissButton: true
                });
            });
        };


        $scope.submit_purchase_order = function(purchaseOrder,form){

          purchaseOrder.itemsList = $scope.itemsList;
          purchaseOrder.date = dateConverter(purchaseOrder.date);

          for(var i=0;i<$scope.itemsList.length;i++){
                if($scope.itemsList[i].quantity < 1 || $scope.itemsList[i].quantity===undefined || $scope.itemsList[i].rate < 1 || $scope.itemsList[i].rate===undefined){
                    $scope.itemsQty = true;
                    return false;
                }
                $scope.itemsList[i].expiryDate = dateConverter($scope.itemsList[i].expiryDate);

            }

          if(purchaseOrder.request!==undefined && purchaseOrder.date!==undefined && $scope.itemsList.length!==0){
            $scope.processing = true;
            inventory_service.create_purchase_return(purchaseOrder).then(function(response){
              ngToast.create({
                className: 'success',
                content: 'Purchase Return Created Successfully',
                dismissButton: true
              });

              $scope.printData = response.data;
              $scope.print(form);

            }).catch(function (error) {
              $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: '+error.data.message,
                    dismissButton: true
                });
            });
          }
        };

        $scope.print = function(form) {

            $scope.printData.total = 0;
            $scope.createdDate = moment($scope.printData.created).format('DD-MM-YYYY hh:mma');
            $scope.printingDate = moment().format('DD-MM-YYYY hh:mma');
            $scope.totalforPrint = $scope.totalAmount;
            $scope.hospitallogo = $scope.hosDetail[0].image_url;
            $scope.hospitalname = $scope.hosDetail[0].title;
            $scope.hospitalAddress = $scope.hosDetail[0].address;
            $scope.hospitalPHNumber = $scope.hosDetail[0].number;
            $scope.hospitalEmail = $scope.hosDetail[0].email;
            /*for(var k=0;k<$scope.printData.itemsList.length;k++){
              $scope.printData.total = $scope.printData.total + $scope.printData.itemsList[k].netValue;
            }*/
            print_service.printFromScope('/modules/inventories/views/purchase-return-print.client.view.html',$scope,function(){

            });
              $scope.processing = false;
              $scope.reset(form);
      };

        $scope.getItems = function(){
          inventory_service.search_by_keyword($scope.searchKeyword)
            .then(function(response) {
              $scope.showGrid = true;
              $scope.samplejson = [];
              $scope.samplejson = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        ///////////handle Submit Request///////////
         $scope.handle_Submit_Request = function(purchaseOrder,form){

             if( $scope.btnType== "Save"){
                $scope.submit_purchase_order(purchaseOrder,form)

             }
             else{
                 $scope.UpdatePurchaseReturnObj = {};
                 $scope.UpdatePurchaseReturnObj.purchaseReturnNumber= $scope.purchaseReturnNumber;
                 $scope.UpdatePurchaseReturnObj.itemsList = $scope.itemsList;

                 $scope.Update_purchase_return($scope.UpdatePurchaseReturnObj);
            }


         };

        /////////Update Purchase Return///////////
         $scope.Update_purchase_return =function(Obj){


              inventory_service.UpdatePurchaseReturn(Obj)
                .then(function(response) {
                       console.log(response.data);
                       $scope.reset();

                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to Update Purchase Return.',
                        dismissButton: true
                    });
                });


         };
        ///////////////////////////////////////////

        ////////Search by Purchase Return//////////
        $scope.btnType= "Save";
        $scope.searchPurchaseReturnKeyword ="";
         $scope.btnDelshow = false;

        $scope.get_purchase_return =function(){

            if($scope.searchPurchaseReturnKeyword == "")
            {
                 $scope.get_Purchase_Return_PickList();
            }
            else
            {
                $scope.searched_purchase_return($scope.searchPurchaseReturnKeyword);
                $scope.purchaseReturnNumber = $scope.searchPurchaseReturnKeyword;
            }


             $scope.searchPurchaseReturnKeyword ="";
        };

		$scope.searched_purchase_return =function(id){
			inventory_service.getSearchedPurchaseReturn(id)
			.then(function(response) {

			$scope.itemsList = response.data[0].itemsList;
			$scope.purchaseOrder.storeId = response.data[0].storeId;
			$scope.purchaseOrder.storeDescription = response.data[0].storeDescription;
			$scope.purchaseOrder.remarks = response.data[0].remarks;
			$scope.purchaseOrder.transcationCategory = response.data[0].transcationCategory;
			$scope.purchaseOrder.date = response.data[0].date;
			$scope.purchaseOrder.purchaseReturnNumber = response.data[0].purchaseReturnNumber;

			//$scope.btnType= "Update";
			$scope.btnDelshow = true;

			}).catch(function (error) {
				ngToast.create({
					className: 'danger',
					content: 'Error: Unable to retrieve Purchase Return.',
					dismissButton: true
				});
			});
		};

        //////////////////////////////////////////
        /////////Purchase Return PickList/////////
        $scope.showAllPurchaseReturns = false;
        $scope.headingsPurchaseReturns ="";
        $scope.allPurchaseReturns ="";


        $scope.get_Purchase_Return_PickList = function(){
                $scope.processing = true;
                $scope.showAllPurchaseReturns = true;

                inventory_service.getPurchaseReturn()
                    .then(function(response) {

						$scope.allPurchaseReturns =[];
						$scope.headingsPurchaseReturns =[];

						$scope.headingsPurchaseReturns.push({'alias': 'Purchase Return No', 'name': 'purchaseReturnNumber', 'width': 33});
						$scope.headingsPurchaseReturns.push({'alias': 'Date', 'name': 'date', 'width': 33});
						$scope.headingsPurchaseReturns.push({'alias': 'Category', 'name': 'transcationCategory', 'width': 33});


						for(var i =0 ; i< response.data.length ; i++)
						{
							$scope.allPurchaseReturns.push({
								'purchaseReturnNumber' : response.data[i].purchaseReturnNumber,
												'date' : moment(response.data[i].date).format('DD/MM/YYYY'),
								 'transcationCategory' : response.data[i].transcationCategory
							 });
						}

						$scope.processing = false;

                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Purchase Return.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
            };

             $scope.Purchase_Return_CallBack = function(selItem){

				$scope.purchaseReturnNumber = selItem[0].purchaseReturnNumber;
				$scope.searched_purchase_return(selItem[0].purchaseReturnNumber);
				$scope.btnType= "Update";
				$scope.btnDelshow = true;
				$scope.showAllPurchaseReturns = false;
            };


        $scope.reset();

        $scope.openItemsList = function() {
            $scope.showAllItems = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.openGRNs = function() {
            $scope.showGRNs = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixGRN + $scope.prefixGRN));
            $timeout(function() {setFocus[0].focus()});
        };

		$scope.selectfromStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.purchaseOrder.storeId = $scope.storeData[storeIndex]._id;
            $scope.purchaseOrder.storeDescription = $scope.storeData[storeIndex].description;
            $scope.showfromStoresList = false;
        };

		$scope.openfromStoreList = function() {
            $scope.showfromStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixfromStore + $scope.prefixfromStore));
            $timeout(function() {setFocus[0].focus()});
        };
      }
]);
