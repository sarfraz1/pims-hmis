'use strict';

angular.module('inventories').controller('InventoriesCustomerSupplierController', ['$scope', '$stateParams', '$location', 'Authentication', 'inventory_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, inventory_service, ngToast, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        var spliceArray = function() {
            for (var i = 0; i < $scope.contactNumber.length; i++) {
                if (!$scope.contactNumber[i].number) {
                    $scope.contactNumber.splice(i, 1);
                }
            }
            for (i = 0; i < $scope.postalAddress.length; i++) {
                if (!$scope.postalAddress[i].address) {
                    $scope.postalAddress.splice(i ,1);
                }
            }
        };

        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 70});
            $scope.headings.push({'alias': 'NTN', 'name': 'NTN', 'width': 30});
            $scope.samplejson = [];
            $scope.samplejson = response.data;
            $scope.listToShow = [];
            for (var i = 0; i < $scope.samplejson.length; i++) {
                $scope.listToShow.push({'description': $scope.samplejson[i].description, 'NTN': $scope.samplejson[i].NTN});
            }
        };
        
        var getItems = function() {
            inventory_service.list_inventory_customer_supplier()
                .then(function (response) {
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier List.',
                    dismissButton: true
                });
            });
        };

        getItems();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var listIndex = getIndex($scope.samplejson, 'NTN', selected_item[0].NTN);
	    	$scope.customerSupplier = $scope.samplejson[listIndex];
        	$scope.postalAddress = $scope.samplejson[listIndex].postalAddress;
        	$scope.contactNumber = $scope.samplejson[listIndex].contactNumber;
            for (var i = 0; i < $scope.samplejson[listIndex].contactNumber.length; i++) {
                $scope.contactNumber[i].number = parseFloat($scope.samplejson[listIndex].contactNumber[i].number);
            }
            $scope.customerSupplier.customerSupplierType = $scope.samplejson[listIndex].customerSupplierType;
            $scope.UpdateItem = true;
	 	};

		$scope.UpdateItem = false;
		$scope.samplejson = [];
        $scope.showTable = false;
        $scope.customerSupplier = {};
        $scope.customerSupplier.customerSupplierType = 'supplier';
        $scope.confirmationPopup = 'hide-popup';
        $scope.headings = [];
        $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 70});
        $scope.headings.push({'alias': 'NTN', 'name': 'NTN', 'width': 30});
        $scope.samplejson = [];
        $scope.listToShow = [];

		var customerSupplier = {
			'name': '',
			'description': '',
			'postalAddress': [{
				'address': ''
			}],
            'customerSupplierType': 'supplier',
			'contactNumber': [{
				'number': ''
			}],
			'emailAddress': '',
			'salesTaxRegistrationNumber': '',
			'NTN': '',
			'CNIC': ''
		};

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.customerSupplier = angular.copy(customerSupplier);
            $scope.postalAddress = [{}];
            $scope.contactNumber = [{}];
            $scope.confirmationPopup = 'hide-popup';
		};

        $scope.submit_customer_supplier = function(customerSupplier, form) {
            spliceArray();
        	customerSupplier.postalAddress = $scope.postalAddress;
        	customerSupplier.contactNumber = $scope.contactNumber;
            customerSupplier.customerSupplierType = $scope.customerSupplier.customerSupplierType;
    		inventory_service.create_inventory_customer_supplier(customerSupplier).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Customer/Supplier Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
                getItems();
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Customer/Supplier exists with the same NTN.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add Customer/Supplier.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.delete_customer_supplier = function() {
            $scope.confirmationPopup = 'confirmation-popup customer-supplier-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            inventory_service.delete_inventory_customer_supplier($scope.customerSupplier).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Customer/Supplier Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                getItems();
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Customer/Supplier.',
                    dismissButton: true
                });
            });
        };

        $scope.update_customer_supplier = function(customerSupplier, form) {
            spliceArray();
        	customerSupplier.postalAddress = $scope.postalAddress;
        	customerSupplier.contactNumber = $scope.contactNumber;
            customerSupplier.customerSupplierType = $scope.customerSupplier.customerSupplierType;
        	inventory_service.update_inventory_customer_supplier(customerSupplier).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Customer/Supplier Updated Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
                getItems();
            }).catch(function (error) {
                if (error.data.message.indexOf('already exists') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another Customer/Supplier exists with the same NTN.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update Customer/Supplier.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_customer_supplier = function(form) {
        	$scope.reset(form);
            getItems();
        };

        $scope.postalAddress = [{}];
        $scope.contactNumber = [{}];

        $scope.addPostalAddress = function() {
        	$scope.postalAddress.unshift({});
        };

        $scope.removePostalAddress = function(index) {
        	$scope.postalAddress.splice(index, 1);
        };

        $scope.addContactNumber = function() {
        	$scope.contactNumber.unshift({});
        };

        $scope.removeContactNumber = function(index) {
        	$scope.contactNumber.splice(index, 1);
        };
	}
]);