'use strict';

// Stock Transfer controller
angular.module('inventories').controller('stockTransferController', ['$scope', '$http','$stateParams', '$location', '$timeout', 'Authentication', 'inventory_service','ngToast','$anchorScroll','print_service',
    function($scope,$http ,$stateParams, $location, $timeout, Authentication, inventory_service,ngToast,$anchorScroll,print_service) {

        $scope.authentication = Authentication;

        if (!$scope.authentication.user) $location.path('/signin');

        $scope.showSearchMessage = false;
        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showGrid = false;
        $scope.columnSize = 300; //variable to set width of columns

	    $scope.stocktransfer = {};
		$scope.itemsList = [];

		// store simple picklist
        $scope.storeData = [];
        $scope.storesToShow = [];
        $scope.prefixtoStore = 'storediv';
		$scope.prefixfromStore = 'fromstorediv';
        $scope.headingsStores = [];
        $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});

		var KeyCodes = {
			BACKSPACE : 8,
			TABKEY : 9,
			RETURNKEY : 13,
			ESCAPE : 27,
			SPACEBAR : 32,
			LEFTARROW : 37,
			UPARROW : 38,
			RIGHTARROW : 39,
			DOWNARROW : 40,
		};

    var start = function (){
      $http.get('/hospital/logoname.json').then(function(response){
      $scope.hosDetail= response.data.user;
    });
    };

    start();

		$scope.onKeydown = function(item, $event) {
			var e = $event;
			var $target = $(e.target);
			var nextTab;

			switch (e.keyCode) {
				case KeyCodes.ESCAPE:
					$target.blur();
					$scope.name_search_keyword = '';
					$scope.show_dropdown = false;
					nextTab = 0;
					break;
				case KeyCodes.UPARROW:
					e.preventDefault();
					if(parseInt($target.attr('tabindex')) == 4)
						nextTab = -3;
					else
						nextTab = -1;
						break;
				case KeyCodes.RETURNKEY:
					e.preventDefault();
					nextTab = 1;
					break;
				case KeyCodes.DOWNARROW:
					e.preventDefault();
					if(parseInt($target.attr('tabindex')) == 1 || parseInt($target.attr('tabindex')) == 25 || parseInt($target.attr('tabindex')) == 26)
						nextTab = 3;
					else
						nextTab = 1;
					break;
			}
			if (nextTab != undefined) {
				// do this outside the current $digest cycle
				// focus the next element by tabindex
				if (parseInt($target.attr('tabindex')) + nextTab !== 0)
					$timeout(function() { $('[tabindex=' + (parseInt($target.attr('tabindex')) + nextTab) + ']').focus()});
			}
		};

		var getIndex = function(array, prop, value) {
			var index = -1;
			for (var x = 0; x < array.length; x++) {
				if (array[x][prop] === value) {
					index = x;
				}
			}
			return index;
        };

        var dateConverter = function(dateinput){
          try{
            var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
            utcDate = utcDate.toUTCString();
            return utcDate;
          }
          catch(error){
            return dateinput;
          }
        };

		$scope.changeNetTotal = function(med) {
			med.netValue = Number((med.rate*med.quantity).toFixed(2));
		};

		$scope.add_search_medicine = function(med_obj) {
			$scope.name_search_keyword = '';
			$scope.show_dropdown = false;

			if($scope.stocktransfer.fromStoreDescription == 'Stock Adjustment') {
				med_obj.totalstock = 100;
			}

			if(getIndex($scope.itemsList,"code", med_obj.code) == -1) {
				if(med_obj.totalstock == 0) {
					med_obj.quantity = 0;
				} else {
					med_obj.quantity = 1;
				}

				inventory_service.view_inventory_pricing(med_obj).then(function (pricing) {
					if($scope.stocktransfer.fromStoreDescription == 'Stock Adjustment') {
						inventory_service.get_inventory_by_code(med_obj.code).then(function (inv) {
							med_obj.rate = Number((pricing.purchasingPrice/inv.data.storage_to_selling).toFixed(2));
							med_obj.netValue = med_obj.rate;
							$scope.itemsList.push(med_obj);
						})
					} else {
						med_obj.rate = pricing.sellingPrice;
						med_obj.netValue = med_obj.rate;
						$scope.itemsList.push(med_obj);
					}
				});


			} else {
				ngToast.create({
                    className: 'warning',
                    content: 'Already added in transfer list',
                    dismissButton: true
                });
			}
		};


        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy;
            return today;
        };

		$scope.search_medicines_stock = function() {
			if ($scope.name_search_keyword.length > 0) {
				if($scope.stocktransfer.fromStoreDescription !== 'Stock Adjustment') {
				inventory_service.search_inventory_by_name_store($scope.name_search_keyword, $scope.stocktransfer.fromStoreId).then(function (response) {
					$scope.searched_medicines = response;
						$scope.show_dropdown = true;
					});
				} else {
					inventory_service.search_inventory_by_name_store($scope.name_search_keyword, $scope.stocktransfer.toStoreId).then(function (response) {
					//inventory_service.search_by_keyword($scope.name_search_keyword).then(function (response) {
						$scope.searched_medicines = response;
						$scope.show_dropdown = true;
					});
				}
			}

			if ($scope.name_search_keyword.length === 0) {
				$scope.show_dropdown = false;
			}
		};

        $scope.remove_medicine = function(index) {
          $scope.itemsList.splice(index,1);
        };

        $scope.reset = function(form){
          if (form) {
            form.$setPristine();
            form.$setUntouched();
            form.submitted = false;
          }
          $location.hash('headerid');
          $anchorScroll.yOffset = 100;
          $anchorScroll();

          $scope.processing = false;
          $scope.itemsList = [];
		  $scope.stocktransfer = {};

		  getStores();
          //getAllItems();
        };

        $scope.delete_purchase_order = function() {
            $scope.confirmationPopup = 'confirmation-popup-right';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
          /*  inventory_service.delete_purchase_order($scope.purchaseOrder).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Purchase Order Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Purchase Order.',
                    dismissButton: true
                });
            });*/
        };

        var hospitalDetails = {};
        var orderObject = {};


        var get_time = function() {
            var d = new Date();
            var hr = d.getHours();
            var min = d.getMinutes();
            if (min < 10) {
                min = '0' + min;
            }
            var ampm = hr < 12 ? 'AM' : 'PM';
            if (hr > 12) {
                hr = hr - 12;
            }
            return (hr + ':' + min + ' ' + ampm);
        };

        var get_date = function() {
            var d = new Date();
            return (d.toLocaleDateString());
        };

        var getStores = function(){
            inventory_service.get_inventory_stores()
                .then(function(response) {
                if (response.data.length > 0) {
                    $scope.storeData = [];
                    $scope.storeData = response.data;
                    $scope.storesToShow = [];
                    $scope.headingsStores = [];
                    $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.storesToShow.push({'description': response.data[i].description});
                    }
                   // $scope.stocktransfer.fromstore = response.data[0].description;
					//$scope.stocktransfer.tostore = response.data[0].description;
					//$scope.fromstoreid = response.data[0]._id;

                    $scope.showStoresList = false;
                } else {
                    ngToast.create({
                        className: 'warning',
                        content: 'Warning: No Inventory Stores added.',
                        dismissButton: true
                    });
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

        $scope.submit_transfer = function(stocktransfer,form){

			if(stocktransfer.fromStoreId == stocktransfer.toStoreId) {
					ngToast.create({
						className: 'danger',
						content: 'Error: From and To store are same.',
						dismissButton: true
					});

					return;
			}
			stocktransfer.transferedBy = $scope.authentication.user.displayName;
			stocktransfer.itemsList = $scope.itemsList;
            stocktransfer.date = new Date();

			if($scope.itemsList.length!==0){


				$scope.processing = true;

				inventory_service.create_stocktransfer(stocktransfer).then(function(response){
				  ngToast.create({
					className: 'success',
					content: 'Stock Transfer Successful',
					dismissButton: true
				  });

                  $scope.printData = response.data;
                  $scope.print(form);

				}).catch(function (error) {
				  $scope.processing = false;
					ngToast.create({
						className: 'danger',
						content: 'Error : '+error.data.message,
						dismissButton: true
					});
				});
			}
        };

        $scope.print = function(form) {

            $scope.printData.total = 0;
            $scope.createdDate = moment($scope.printData.created).format('DD-MM-YYYY hh:mma');
            $scope.printingDate = moment().format('DD-MM-YYYY hh:mma');
            $scope.totalforPrint = $scope.totalAmount;
            $scope.hospitallogo = $scope.hosDetail[0].image_url;
            $scope.hospitalname = $scope.hosDetail[0].title;
            $scope.hospitalAddress = $scope.hosDetail[0].address;
            $scope.hospitalPHNumber = $scope.hosDetail[0].number;
            $scope.hospitalEmail = $scope.hosDetail[0].email;
            /*for(var k=0;k<$scope.printData.itemsList.length;k++){
              $scope.printData.total = $scope.printData.total + $scope.printData.itemsList[k].netValue;
            }*/
            print_service.printFromScope('/modules/inventories/views/stock-transfer-print.client.view.html',$scope,function(){

            });
              $scope.processing = false;
              $scope.reset(form);
      };

        $scope.reset();

        $scope.openItemsList = function() {
            $scope.showAllItems = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        $scope.selecttoStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.stocktransfer.toStoreId = $scope.storeData[storeIndex]._id;
            $scope.stocktransfer.toStoreDescription = $scope.storeData[storeIndex].description;
            $scope.showtoStoresList = false;
        };

        $scope.selectfromStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.stocktransfer.fromStoreId = $scope.storeData[storeIndex]._id;
            $scope.stocktransfer.fromStoreDescription = $scope.storeData[storeIndex].description;
            $scope.showfromStoresList = false;
			$scope.itemsList = [];
        };

        $scope.opentoStoreList = function() {
            $scope.showtoStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixtoStore + $scope.prefixtoStore));
            $timeout(function() {setFocus[0].focus()});
        };

		$scope.openfromStoreList = function() {
            $scope.showfromStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixfromStore + $scope.prefixfromStore));
            $timeout(function() {setFocus[0].focus()});
        };
    }
]);
