'use strict';

angular.module('inventories').controller('InventoryDetailsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'inventory_service', 'print_service', 'ngToast', '$anchorScroll', '$timeout',
	function($scope, $http, $stateParams, $location, Authentication, inventory_service, print_service, ngToast, $anchorScroll, $timeout) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user)
        	$location.path('/signin');
		
		$scope.inventoryToShow = [];
        $scope.showTable = false;
        $scope.showGrid = false;
		// store simple picklist
        $scope.storeData = [];
        $scope.storesToShow = [];
		$scope.prefixfromStore = 'fromstorediv';
        $scope.headingsStores = [];
        $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.itemsSearchTypeahead = function(val){
        	return inventory_service.search_inventory_by_name_store($scope.name_search_keyword, $scope.fromStoreId)
	            .then(function (response) {
	            	return response.map(function(item){
				        return item;
				    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };
		
		

		var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var storeDesc ='';
		var getStores = function(){
            inventory_service.get_inventory_stores()
                .then(function(response) {
                if (response.data.length > 0) {
                    $scope.storeData = [];
                    $scope.storeData = response.data;
                    storeDesc = response.data.description;
                    $scope.storesToShow = [];
                    $scope.headingsStores = [];
                    $scope.headingsStores.push({'alias': 'Description', 'name': 'description', 'width': 99});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.storesToShow.push({'description': response.data[i].description});
                    }
                   // $scope.stocktransfer.fromstore = response.data[0].description;
					//$scope.stocktransfer.tostore = response.data[0].description;
					//$scope.fromstoreid = response.data[0]._id;

                    $scope.showStoresList = false;
                } else {
                    ngToast.create({
                        className: 'warning',
                        content: 'Warning: No Inventory Stores added.',
                        dismissButton: true
                    });
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Stores.',
                    dismissButton: true
                });
            });
        };

		getStores();

        $scope.selectfromStore = function(store){
            var storeIndex = getIndex($scope.storeData, 'description', store[0].description);
            $scope.fromStoreId = $scope.storeData[storeIndex]._id;
            $scope.fromStoreDescription = $scope.storeData[storeIndex].description;
            storeDesc = $scope.storeData[storeIndex].description;
            $scope.showfromStoresList = false;
        };

		$scope.openfromStoreList = function() {
            $scope.showfromStoresList = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefixfromStore + $scope.prefixfromStore));
            $timeout(function() {setFocus[0].focus()});
        };

		var calculateRunningBalance = function(itemlist) {
			var balance = $scope.openingBalance;
			for(var i=0;i<itemlist.documents.length;i++) {
				if(itemlist.documents[i].transactionType == 'IN') {
					balance = balance + itemlist.documents[i].quantity;
					itemlist.documents[i].inquantity = itemlist.documents[i].quantity;
					itemlist.documents[i].outquantity = 0;
					itemlist.documents[i].balance = balance;
				} else {
					balance = balance - itemlist.documents[i].quantity;
					itemlist.documents[i].inquantity = 0;
					itemlist.documents[i].outquantity = itemlist.documents[i].quantity;
					itemlist.documents[i].balance = balance;
				}
			}

			$scope.item = itemlist;

		};

		$scope.getreport = function() {
			if($scope.code != null){
				var toDate = dateConverter($scope.toDate);
				var fromDate = dateConverter($scope.fromDate);
				$scope.processing = true;
                //console.log($scope.code);
                console.log($scope.storeData.description)
				inventory_service.get_inventory_details($scope.code, storeDesc, $scope.fromStoreId, fromDate, toDate).then(function (response) {
					$scope.processing = false;
					$scope.openingBalance = response.data.startingbalance;
					$scope.showGrid = true;
					$scope.item1 = response.data.itemDetails;
                    //console.log(response.data);

					function custom_sort(a, b) {
						return new Date(a.created).getTime() - new Date(b.created).getTime();
					}

					$scope.item1.documents.sort(custom_sort);
					calculateRunningBalance($scope.item1);
				});
			} else {
				ngToast.create({
                    className: 'danger',
                    content: 'Error: Please Select an Item.',
                    dismissButton: true
                });
			}
		};

		$scope.printreport = function() {
			var dt = moment(new Date());

			var data = angular.copy($scope.item);

			for(var i=0;i<data.documents.length;i++) {
				data.documents[i].created = moment(data.documents[i].created).format('DD-MM-YYYY');
			}

			var obj = {
				'itemledger': data,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a'),
				'fromdate': moment($scope.fromDate).format('DD-MM-YYYY'),
				'todate': moment($scope.toDate).format('DD-MM-YYYY'),
				'openingBalance': $scope.openingBalance
			};
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/inventories/views/itemledger-report-print.client.view.html',obj,
			function(){
			});
		};


        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };


        $scope.callbackfn = function(selected_item) {
            $scope.code = selected_item.code;
        };
	}
]);
