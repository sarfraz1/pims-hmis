'use strict';

angular.module('inventories').controller('InventoriesStockController', ['$scope', '$stateParams', '$location', 'Authentication', 'inventory_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, inventory_service, ngToast, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
		
        var handleResponse = function(response) {
            $scope.showGrid = true;
            $scope.samplejson = [];
            $scope.samplejson = response.data;
        };
        
        $scope.searchKeyword = '';
        $scope.getItems = function() {
            if ($scope.searchKeyword.length === 0) {
                inventory_service.list_inventory_stock()
                    .then(function (response) {
                        handleResponse(response);
                }).catch(function(error) {
                    console.log(error);
                });
            } else if ($scope.searchKeyword.length > 0) {
                inventory_service.search_stock_by_keyword($scope.searchKeyword)
                    .then(function (response) {
                        handleResponse(response);
                }).catch(function(error) {
                    console.log(error);
                });
            }
        };

	 	$scope.hide_directive = function() {
	 		$scope.showGrid = false;
	 	};

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
	    	$scope.stock = selected_item;
	    	$scope.searchKeyword = '';
        	$scope.batch = selected_item.batch;
	    	$scope.UpdateItem = true;
	    	$scope.showGrid = false;
	 	};

		$scope.columnsToshow = ['id'];
		$scope.UpdateItem = false;
		$scope.showGrid = false;
		$scope.columnSize = 300; //variable to set width of columns
		$scope.samplejson = [];
		$scope.defaultOption = false;

		var stock = {
			'id': '',
			'batch': {
				'batchID': '',
				'quantity': 0
			}
		};

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.stock = angular.copy(stock);
            $scope.batch = [{}];
		};

        $scope.submit_stock = function(stock, form) {
        	stock.batch = $scope.batch;
    		inventory_service.create_inventory_stock(stock).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Stock Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
        	});
        };

        $scope.delete_stock = function(form) {
        	inventory_service.delete_inventory_stock($scope.stock).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Stock Deleted Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
        	});
        };

        $scope.update_stock = function(stock, form) {
        	stock.batch = $scope.batch;
        	inventory_service.update_inventory_stock(stock).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Inventory Stock Updated Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
        	});
        };

        $scope.reset_stock = function(form) {
        	$scope.reset(form);
        };

        $scope.batch = [{}];

        $scope.addBatch = function() {
        	$scope.batch.push({});
        };

        $scope.removeBatch = function(index) {
        	$scope.batch.splice(index, 1);
        };
	}
]);