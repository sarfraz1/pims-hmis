'use strict';

angular.module('inventories').controller('LowStockReportController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'ngToast', 'inventory_service', 'print_service', '$anchorScroll','$timeout',
    function($scope, $http, $stateParams, $location, Authentication, ngToast, inventory_service, print_service, $anchorScroll,$timeout) {

        $scope.authentication = Authentication;
        $scope.selectedSupplier = 'all';

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        else {
            // if($scope.authentication.user.roles === 'admin') {
            //     //$location.path('/admin-main-page');
            // }
            // else{
            //     $location.path('/signin')
            // }
        }

        $scope.totalInStock = '';

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var getAllStock = function(){
			$scope.totalstockprice = 0;
            inventory_service.getLowStockReport().then(function (response) {
                $scope.inventoryStock = response.data;
            }).catch(function(err){
                console.log(err);
            });
        };

        var getSuppliers = function(){
            inventory_service.get_inventory_customer_suppliers()
                .then(function(response) {
                    $scope.suppliers = response.data;
                    $timeout(function() {
                        $scope.selectedSupplier = 'all';
                    }, 100);

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Customer/Supplier list.',
                    dismissButton: true
                });
            });
        };

        $scope.updateReport = function(){
			$scope.totalstockprice = 0;
            if($scope.selectedSupplier === 'all'){
                getAllStock();
            }
            else {
                $scope.processing = true;
                inventory_service.list_supplier_inventory_stock($scope.selectedSupplier)
                    .then(function(response) {

						for(var i=0;i<response.data.length;i++){

						   response.data[i].stockprice = (response.data[i].totalInStock*response.data[i].sellingPrice).toFixed(2);

						   $scope.totalstockprice+=Number(response.data[i].stockprice);
						}
                     $scope.totalstockprice =   $scope.totalstockprice.toFixed(2);
					$scope.inventoryStock = response.data;
					$scope.processing = false;
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve Customer/Supplier list.',
                        dismissButton: true
                    });
                });
            }
        };

		$scope.printreport = function() {
			var dt = moment(new Date());
			var ts = [];

			var obj = {
				'inventoryStock': $scope.inventoryStock,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
      $scope.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/inventories/views/stockreorder-report-print.client.view.html',obj,
			function(){
			});
		};

        $scope.quantityFilter = function(item) {
            if ($scope.totalInStock === '')
                return item.totalInStock;
            else
                return item.totalInStock <= $scope.totalInStock;
        };


        $scope.init = function(){
            getAllStock();
            getSuppliers();
            $scope.processing = false;
        };

        $scope.init();

    }
]);
