'use strict';

//Inventories service used to communicate Inventories REST endpoints
angular.module('inventories').factory('inventory_service', ['$http','Upload', '$q', 'config_service',
	function($http, Upload, $q, config_service) {
        var allMedicines = [];
		var srvr_address = config_service.serverAddress;

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        /**
         * Methods for Medicines
         */
        this.search_medicine_formula = function(keyword) {
            return $http.get(srvr_address+'/medicines/formula/' + keyword);
        };

        this.search_medicine_name = function(keyword) {
            return $http.get(srvr_address+'/brandnames/name/' + keyword);
        };

        /**
         * Methods for Inventory Stock
         */
         this.create_inventory_stock = function(item) {
            return $http.post(srvr_address+'/inventory-stock', item);
         };
		 
         this.get_inventory_stock = function(code) {
            return $http.get(srvr_address+'/inventory-stockbyCode/'+code);
         };

         this.list_inventory_stock = function() {
            return $http.get(srvr_address+'/inventory-stock');
         };

         this.list_supplier_inventory_stock = function(supplierId, store) {
            return $http.get(srvr_address+'/inventory-supplier-stock-list/'+supplierId+'/'+store);
         };

         this.delete_inventory_stock = function(item) {
            return $http.delete(srvr_address+'/inventory-stock/' + item._id);
         };

         this.update_inventory_stock = function(item) {
            return $http.put(srvr_address+'/inventory-stock/' + item._id, item);
         };
		 
		 this.update_inventorystock_name = function(code, name) {
			return $http.get(srvr_address+'/inventory-stockname/' + code + '/' + name); 
		 }

         this.search_stock_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-stock/inventory/' + keyword);
         };
		 
		 
		var search_inventory_name_store = function(keyword, store) {
			return $http.get(srvr_address+'/getInventorybyStore/' + keyword+'/'+store);
		};
		
		this.search_inventory_by_name_store = function(keyword, store) {
			var deferred = $q.defer();
			var searched_medicines = [];
			search_inventory_name_store(keyword, store).then(function (response) {
				for (var j = 0; j < response.data.length; j++) {
					searched_medicines.push({code: response.data[j].id, description: response.data[j].description, totalstock: response.data[j].totalstock});
				}
				deferred.resolve(searched_medicines);
			}).catch(function (err) {
				return deferred.reject();
			});
			return deferred.promise;
		};

        /**
         * Methods for Inventory Customer Supplier
         */
         this.search_customer_supplier_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-customer-supplier/name/' + keyword);
         };

         this.list_inventory_customer_supplier = function() {
            return $http.get(srvr_address+'/inventory-customer-supplier');
         };

         this.create_inventory_customer_supplier = function(item) {
            return $http.post(srvr_address+'/inventory-customer-supplier', item);
         };
		 
         this.create_stocktransfer = function(item) {
            return $http.post(srvr_address+'/stockTransfer', item);
         };

         this.delete_inventory_customer_supplier = function(item) {
            return $http.delete(srvr_address+'/inventory-customer-supplier/' + item._id);
         };

         this.update_inventory_customer_supplier = function(item) {
            return $http.put(srvr_address+'/inventory-customer-supplier/' + item._id, item);
         };

         this.get_inventory_customer_suppliers = function(item) {
            return $http.get(srvr_address+'/inventory-customer-supplier');
         };

         this.get_inventory_customer_supplier = function(id) {
            return $http.get(srvr_address+'/inventory-customer-supplier/' + id);
         };
		 
         this.get_all_manufacturers = function() {
            return $http.get(srvr_address+'/inventory-getmanufacturers');
         };

        /**
         * Methods for Inventory Transaction Category
         */
        this.search_transaction_category_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-transaction-category/type/' + keyword);
        };

        this.list_inventory_transaction_category = function() {
            return $http.get(srvr_address+'/inventory-transaction-category');
        };

        this.create_inventory_transaction_category = function(item) {
            return $http.post(srvr_address+'/inventory-transaction-category', item);
        };

        this.delete_inventory_transaction_category = function(item) {
            return $http.delete(srvr_address+'/inventory-transaction-category/' + item._id);
        };

        this.update_inventory_transaction_category = function(item) {
            return $http.put(srvr_address+'/inventory-transaction-category/' + item._id, item);
        };

        /**
         * Methods for Inventory Store
         */
        this.search_store_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-store/description/' + keyword);
        };

        this.list_inventory_store = function() {
            return $http.get(srvr_address+'/inventory-store');
        };

        this.create_inventory_store = function(item) {
            return $http.post(srvr_address+'/inventory-store', item);
        };

        this.delete_inventory_store = function(item) {
            return $http.delete(srvr_address+'/inventory-store/' + item._id);
        };

        this.update_inventory_store = function(item) {
            return $http.put(srvr_address+'/inventory-store/' + item._id, item);
        };

        this.get_inventory_stores = function(item) {
            return $http.get(srvr_address+'/inventory-store/');
        };

        this.get_inventory_store = function(id) {
            return $http.get(srvr_address+'/inventory-store/' + id);
        };

        /**
         * Methods for Inventory Label
         */
        this.search_label_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-label/name/' + keyword);
        };

        this.list_inventory_label = function() {
            return $http.get(srvr_address+'/inventory-label');
        };

        this.create_inventory_label = function(item) {
            return $http.post(srvr_address+'/inventory-label', item);
        };

        this.delete_inventory_label = function(item) {
            return $http.delete(srvr_address+'/inventory-label/' + item._id);
        };

        this.update_inventory_label = function(item) {
            return $http.put(srvr_address+'/inventory-label/' + item._id, item);
        };

        /**
         * Methods for Inventory Unit
         */
        this.search_unit_by_keyword = function(keyword) {
            return $http.get(srvr_address+'/inventory-unit/name/' + keyword);
        };

        this.list_inventory_unit = function() {
            return $http.get(srvr_address+'/inventory-unit');
        };

        this.create_inventory_unit = function(item) {
            return $http.post(srvr_address+'/inventory-unit', item);
        };

        this.delete_inventory_unit = function(item) {
            return $http.delete(srvr_address+'/inventory-unit/' + item._id);
        };

        this.update_inventory_unit = function(item) {
            return $http.put(srvr_address+'/inventory-unit/' + item._id, item);
        };

        /**
         * Methods for Inventory Pricing
         */
    	this.create_inventory_pricing = function(item) {
    		return $http.post(srvr_address+'/inventory-pricing', item);
    	};

    	this.delete_inventory_pricing = function(item) {
    		return $http.delete(srvr_address+'/inventory-pricing/'+item.code);
    	};

    	this.update_inventory_pricing = function(item) {
    		return $http.put(srvr_address+'/inventory-pricing/'+item.inventoryID, item);
    	};

    	this.view_inventory_pricing = function(item) {
            return $http.get(srvr_address+'/inventory-pricing/'+item.code)
                .then(function(response) {
                    if (typeof response.data === 'object') {
                        return response.data;
                    } else {
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    return $q.reject(response.data);
                });
    	};

    	this.list_inventory_pricing = function() {
    		return $http.get(srvr_address+'/inventory-pricing');
    	};

        /**
         * Methods for Inventory
         */
    	this.create_inventory = function(item){
    		inventoryItem = item;
            allMedicines.push(item);
	    	return $http.post(srvr_address+'/inventories', item);
    	};

    	this.delete_inventory = function(item){
    		inventoryItem = item;
            var itemIndex = getIndex(allMedicines, 'code', item.code);
            allMedicines.splice(itemIndex, 1);
	    	return $http.delete(srvr_address+'/inventories/'+item.code);
    	};

    	this.get_inventory_by_code = function(code){
	    	return $http.get(srvr_address+'/inventories/'+code);
    	};
		
    	this.update_inventory = function(item){
    		inventoryItem = item;
    		inventoryItem.updateFlag = true;
            var itemIndex = getIndex(allMedicines, 'code', item.code);
            allMedicines[itemIndex] = item;
	    	return $http.put(srvr_address+'/inventories/'+item.code , item);
    	};

    	this.search_by_keyword = function(keyword){
    		return $http.get(srvr_address+'/getInventory/description/'+keyword);
    	};

        this.getItemCode = function(){
            return $http.get(srvr_address+'/getInventoryItemCode');
        };

        this.getUnits = function(){
            return $http.get(srvr_address+'/inventory-unit');
        };

        this.getLabels = function(){
            return $http.get(srvr_address+'/inventory-label');
        };

        this.getTransactionCategories = function(){
            return $http.get(srvr_address+'/inventory-transaction-category');
        };

        this.getGRN = function() {
            return $http.get(srvr_address+'/GRN');
        };
		
        this.getGRNByNumber = function(keyword) {
            return $http.get(srvr_address+'/GRNbyNumber/' + keyword);
        };

        this.getSearchedGRN = function(keyword) {
            return $http.get(srvr_address+'/GRN/GRNNumber/' + keyword);
        };

        this.getAllGRN = function() {
            return $http.get(srvr_address+'/GRN/all');
        };

        //////////////Purchase Return///////////////////
        this.DeletePurchaseReturn= function(code) {
            return $http.get(srvr_address+'/DeletePurchaseReturn/' + code);
        };
        
        this.getSearchedPurchaseReturn= function(keyword) {
            return $http.get(srvr_address+'/PurchaseReturnById/' + keyword);
        };
		
        this.getStockReport= function() {
            return $http.get(srvr_address+'/inventory-stockreport');
        };
		
		this.getAverageStockPrice= function(supplier) {
			return $http.get(srvr_address+'/inventory-stockaverageprice/'+supplier);
		};

        this.getLowStockReport= function() {
            return $http.get(srvr_address+'/inventory-lowstockreport');
        };		

        this.UpdatePurchaseReturn= function(Obj) {
            return $http.post(srvr_address+'/UpdatePurchaseReturn' , Obj );
        };


        this.getPurchaseReturn = function() {
            return $http.get(srvr_address+'/purchaseReturn');
        };
        //////////////////////////////////////////////////

       

        this.getPendingPurchaseRequisitions = function() {
            return $http.get(srvr_address+'/purchaseRequisition/status/Pending');
        };

        this.getSearchedPendingPurchaseRequisitions = function(keyword) {
            return $http.get(srvr_address+'/purchaseRequisition/code/' + keyword);
        };

        this.getPurchaseRequisitions = function(){
            return $http.get(srvr_address+'/purchaseRequisition');
        };

        this.getPurchaseOrders = function(){
            return $http.get(srvr_address+'/purchaseOrder');
        };

        this.getSearchedPurchaseOrders = function (keyword) {
            return $http.get(srvr_address+'/purchaseOrder/number/' + keyword);
        };

        this.listAllItems = function() {
            var deferred = $q.defer();
            if (allMedicines.length === 0) {
                $http.get(srvr_address+'/inventories').then(function (response) {
                    allMedicines = response.data;
                    deferred.resolve(allMedicines);
                });
            } else {
                deferred.resolve(allMedicines);
            }
            return deferred.promise;
        };

        this.getItemExpirayStatus = function(itemCode){
            return $http.get(srvr_address+'/getInventoryItemExpiryStatus/'+itemCode);
            
        };

        this.uplaodImage = function(item,file){
            return Upload.upload({
                url: '/inventoryImage',
                fields: item,
                file: file
            }).success(function (data, status, headers, config) {
                console.log('Inventory Item is Saved!');
             }).error(function (data, status, headers, config) {
                            });            
        };

        this.create_GRN = function(grn_obj){
            return $http.post('/GRN' , grn_obj);
        };

        this.update_GRN = function(grn_obj) {
            return $http.put(srvr_address+'/GRN', grn_obj);
        };

        this.delete_GRN = function(grn_obj) {
            return $http.delete(srvr_address+'/GRN/' + grn_obj._id);
        };

        this.create_purchase_order = function(purchaseOrder){
            return $http.post(srvr_address+'/purchaseOrder' , purchaseOrder);
        };

        this.update_purchase_order = function(purchaseOrder) {
            return $http.put(srvr_address+'/purchaseOrder/' + purchaseOrder._id, purchaseOrder);
        };

        this.delete_purchase_order = function(purchaseOrder) {
            return $http.post(srvr_address+'/deletePurchaseOrder',purchaseOrder);
        };

        this.create_purchase_requisition = function(purchaseRequisition){
            return $http.post(srvr_address+'/purchaseRequisition' , purchaseRequisition);
        };

        this.update_purchase_requisition = function(purchaseRequisition) {
            return $http.post(srvr_address+'/purchaseRequisitionUpdate', purchaseRequisition);
        };

        this.delete_purchase_requisition = function(purchaseRequisition) {
            return $http.delete(srvr_address+'/purchaseRequisition/' + purchaseRequisition._id);
        };
        
        this.create_purchase_return = function(purchaseReturn){
            return $http.post(srvr_address+'/purchaseReturn' , purchaseReturn);
        };

        this.get_hospital_details = function() {
            return $http.get(srvr_address+'/hospital');
        };

        this.get_inventory_pricing_supplier = function(supplierId) {
            return $http.get(srvr_address+'/inventory-pricing-supplier/' + supplierId);
        };
		
        this.get_inventory_pricing_supplier_stock = function(supplierId) {
            return $http.get(srvr_address+'/inventory-pricing-supplier-stock/' + supplierId);
        };

        this.update_inventory_grn_status = function(inventory_code){
            for(var i=0;i<allMedicines.length;i++){
                if(allMedicines[i].code===inventory_code){
                    allMedicines[i].grn_created = true;
                    return true;
                }
            }
        };

        this.get_inventory_details = function(code,storedesc,store,fromdate,todate) {
            return $http.get(srvr_address+'/getInventoryDetailsByCode/' + code+'/'+storedesc+'/'+store+'/'+fromdate+'/'+todate);
        };

        /**
         * Method for passing data between controllers
         */
        var inventoryItem;

        this.return_inventory_data = function() {
            return inventoryItem;
        };

		return this;
	}
]);