'use strict';

// Configuring the Articles module
angular.module('inventories').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('item', 'Inventories', 'inventories', 'dropdown', '/inventories(/create)?');
		Menus.addSubMenuItem('item', 'inventories', 'List Inventories', 'inventories');
		Menus.addSubMenuItem('item', 'inventories', 'New Inventory', 'inventories/create');
	}
]);