'use strict';

//Setting up route
angular.module('inventories').config(['$stateProvider',
	function($stateProvider) {
		// Inventories state routing
		$stateProvider.
		state('inventory-details', {
			url: '/inventory-details',
			templateUrl: 'modules/inventories/views/inventory-details.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-stock', {
			url: '/inventory-stock',
			templateUrl: 'modules/inventories/views/inventory-stock.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		}).
		state('inventory-customer-supplier', {
			url: '/inventory-customer-supplier',
			templateUrl: 'modules/inventories/views/inventory-customer-supplier.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-transaction-category', {
			url: '/inventory-transaction-category',
			templateUrl: 'modules/inventories/views/inventory-transaction-category.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-store', {
			url: '/inventory-store',
			templateUrl: 'modules/inventories/views/inventory-store.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-label', {
			url: '/inventory-label',
			templateUrl: 'modules/inventories/views/inventory-label.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-unit', {
			url: '/inventory-unit',
			templateUrl: 'modules/inventories/views/inventory-unit.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('inventory-pricing', {
			url: '/inventory-pricing',
			templateUrl: 'modules/inventories/views/inventory-pricing.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('listInventories', {
			url: '/inventories',
			templateUrl: 'modules/inventories/views/list-inventories.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('createInventory', {
			url: '/inventories/create',
			templateUrl: 'modules/inventories/views/create-inventory.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('viewInventory', {
			url: '/inventories/:inventoryId',
			templateUrl: 'modules/inventories/views/view-inventory.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('editInventory', {
			url: '/inventories/:inventoryId/edit',
			templateUrl: 'modules/inventories/views/edit-inventory.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('purchaseRequisition', {
			url: '/purchaseRequisition',
			templateUrl: 'modules/inventories/views/purchase-requisition.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('purchaseOrder', {
			url: '/purchaseOrder',
			templateUrl: 'modules/inventories/views/purchase-order.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('stockTransfer', {
			url: '/stockTransfer',
			templateUrl: 'modules/inventories/views/stock-transfer.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('purchaseReturn', {
			url: '/purchaseReturn',
			templateUrl: 'modules/inventories/views/purchase-return.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('GRN', {
			url: '/GRN',
			templateUrl: 'modules/inventories/views/GRN.client.view.html',
			access: ['pharmacy admin', 'super admin']
		}).
		state('stockReport', {
			url: '/stockReport',
			templateUrl: 'modules/inventories/views/stock-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		}).
		state('lowstockReport', {
			url: '/lowstockReport',
			templateUrl: 'modules/inventories/views/stockreorder-report.client.view.html',
			access: ['pharmacy admin', 'super admin', 'reports']
		});
	}
]);
