'use strict';

angular.module('ipd').factory('ipd_service', ['$http',
	function($http) {

		this.create_wards = function(facility) {
			return $http.post('/ward', facility);
		};

		this.delete_beds = function(catagory_name) {
			return $http.delete('/bed-pricing/'+ catagory_name);
		};

		this.get_wards = function(facilityName) {
			return $http.get('/ward/'+ facilityName);
		};

		this.get_all_wards = function() {
			return $http.get('/ward');
		};

		this.remove_wards = function(obj) {
			return $http.post('/removeWard',obj);
		};

		this.update_wards = function(obj) {
			return $http.post('/updateWard', obj);
		};

		this.get_last_nodes_wards = function() {
			return $http.get('/wardlastNodes');
		};

		this.get_bed_and_price = function(category) {
			return $http.get('/bed-pricing/'+category);
		};

		this.create_bed_price = function(service) {
			return $http.post('/bed-pricing', service);
		};

		this.get_beds_by_ward = function(ward) {
			return $http.get('/bed-pricingbycategory/'+ward);
		};
	/*	this.get_beds_by_ward_name = function(ward) {
			return $http.get('/bed-pricingbycategory/'+ward);
		};*/

		this.create_ipd_patient = function(patient) {
			return $http.post('/ipdpatient', patient);
		};

		this.add_services_bill = function(obj) {
			return $http.post('/addservicestobill', obj);
		};

		this.get_ipd_patients = function() {
			return $http.get('/ipdpatient');
		};
		this.get_admitted_ipd_patients = function() {
			return $http.get('/admittedipdpatient');
		};


		this.update_ipd_patient = function(patient) {
			return $http.put('/ipdpatient/' + patient._id, patient);
		};

		this.update_bed_details = function(bed) {
			return $http.put('/bed-pricing/' + bed._id, bed);
		};

		this.search_packages = function(keyword) {
			return $http.get('/getPackage/' + keyword);
		};

		this.create_package = function(pkg) {
			return $http.post('/package', pkg);
		};

		this.get_package = function(pkg) {
			return $http.get('/package/' + pkg._id);
		};

		this.update_package = function(pkg) {
			return $http.put('/package/' + pkg._id, pkg);
		};

		this.update_ipd_medicine = function(data) {
			return $http.post('/ipdpatient/updatemedicine',data);
		};

		this.get_facilities_by_category = function(category) {
			return $http.get('/facility-pricing/'+category);
		};

		this.get_all_facilities = function() {
			return $http.get('/facility-pricing');
		};

		this.add_service_to_bill = function(data) {
			return $http.post('/addservicebynametobill', data);
		};

		this.add_services_to_bill = function(data) {
			return $http.post('/addservicesbynametobill', data);
		};

		this.get_facilities = function(facilityName) {
			return $http.get('/facility/'+ facilityName);
		};
		this.get_doctor_names = function() {
			return $http.get('/doctornames');
		};
		this.get_package_names = function() {
			return $http.get('/getPackageNames');
		};
		this.get_package_detail = function(name) {
			return $http.get('/packagedetail/'+name);
		};
		this.get_OTAs = function(name) {
			return $http.get('/getOTAs');
		};


		return this;
	}
]);
