'use strict';

//Setting up route
angular.module('ipd').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('ipdreception', {
			url: '/ipdreception',
			controller: 'IPDReceptionController',
			templateUrl: 'modules/ipd/views/ipdreception.client.view.html',
			access: ['ipd receptionist', 'ipd admin', 'super admin']
		})
		.state('ipdreception.registerPatient', {
			url: '/ipdregisterPatient',
			controller: 'registerPatientController',
			templateUrl: 'modules/opd/views/register-patient.client.view.html',
			access: ['ipd receptionist', 'ipd admin', 'super admin']
		})
		.state('ipdreception.viewIPDPatients', {
			url: '/viewIPDPatients',
			controller: 'ViewIPDPatientsController',
			templateUrl: 'modules/ipd/views/view-ipdpatients.client.view.html',
			access: ['ipd receptionist', 'ipd admin', 'super admin']
		})
		.state('ipdreception.patientAdmission', {
			url: '/patientAdmission',
			controller: 'PatientAdmissionController',
			templateUrl: 'modules/ipd/views/patient-admission.client.view.html',
			access: ['ipd receptionist', 'ipd admin', 'super admin']
		})
		.state('ipdreception.generateBill', {
			url: '/ipdgenerateBill/:MRN',
			controller: 'IPDGenerateBillController',
			templateUrl: 'modules/ipd/views/ipdgenerate-bill.client.view.html',
			access: ['ipd receptionist', 'ipd admin', 'super admin']
		})
		.state('create-wards', {
			url: '/create-wards',
			controller: 'CreateWardsController',
			templateUrl: 'modules/ipd/views/admin-createwards.client.view.html',
			access: ['ipd admin', 'super admin']
		})
		.state('create-beds', {
			url: '/create-beds',
			controller: 'CreateBedsController',
			templateUrl: 'modules/ipd/views/admin-createbeds.client.view.html',
			access: ['ipd admin', 'super admin']
		})
		.state('create-packages', {
			url: '/create-packages',
			controller: 'createPackageController',
			templateUrl: 'modules/ipd/views/create-package.client.view.html',
			access: ['ipd admin', 'super admin']
		});
	}
]);