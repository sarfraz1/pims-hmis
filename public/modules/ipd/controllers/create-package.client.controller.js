'use strict';

angular.module('ipd').controller('createPackageController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'ipd_service','hospitalAdmin_service','print_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, ipd_service, hospitalAdmin_service, print_service, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showPackages = false;
        $scope.updateItem = false;
        $scope.searchPackageKeyword = '';
        $scope.processing = false;
				$scope.departments=[];
			//	$scope.disableEditing=false;

		// Package simple picklist
        $scope.packageData = [];
				$scope.doctors=[];
				$scope.OTAs=[];
				$scope.anesthetist=[];
        $scope.packages = [];
        $scope.headingsPackage = [];
        $scope.prefixPackage = 'patientdiv';
        $scope.headingsPackage.push({'alias': 'Name', 'name': 'name', 'width': 100});
       // $scope.headingsPackage.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

			 $scope.servicesData = [{
					 'description' : '',
					 'fee' : 0,
					 'total' : 0,
					 'quantity' : 0
			 }];

	var  pkg = {
            'name': '',
						'bed_info':{
						'category': '',
						'days': 0},
						'equipment_limit': 0,
						'medicine_limit': 0,
						'OT_charges': 0,
						'other_charges': 0,
						'total_charges': 0,
						'servicesData':[{
		 					 'description' : '',
		 					 'fee' : 0,
		 					 'total' : 0,
		 					 'quantity' : 0
		 			 }],
					 'surgeon_charges':[{
								'name' : '',
							'_id': '',
							'fee' : 0,
					}],
					'anesthetist_charges':[{
							 'name' : '',
						 '_id': '',
						 'fee' : 0,
				 }],
				 'OTA_charges':[{
							'name' : '',
						'_id': '',
						'fee' : 0,
				}]
        };
				$scope.pkg=angular.copy(pkg);
				$scope.init=function(){
					getFacilities();
					getDepartments();
					getDoctors();
					getOTAs();
				}

				var KeyCodes = {
            BACKSPACE : 8,
            TABKEY : 9,
            RETURNKEY : 13,
            ESCAPE : 27,
            SPACEBAR : 32,
            LEFTARROW : 37,
            UPARROW : 38,
            RIGHTARROW : 39,
            DOWNARROW : 40,
        };


        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };
				var getDoctors=function(){
					ipd_service.get_doctor_names().then(function (response){
						$scope.doctors=angular.copy(response.data);

					})
					.catch(function (error) {
							ngToast.create({
									className: 'danger',
									content: 'Error: Unable to retrieve Doctors.',
									dismissButton: true
							});
					});

				};

				var getOTAs=function(){
					ipd_service.get_OTAs().then(function (response){
						$scope.OTAs=angular.copy(response.data);

					})
					.catch(function (error) {
							ngToast.create({
									className: 'danger',
									content: 'Error: Unable to retrieve OTAs.',
									dismissButton: true
							});
					});

				};

				//Get List of facilities
				var getFacilities = function() {
            //ipd_service.get_facilities('Hospital').then(function (response) {
						hospitalAdmin_service.get_facilities_and_price().then(function (response) {
                $scope.facilities = angular.copy(response.data);

              //  $scope.facilites = angular.copy(response.data);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve facilites.',
                    dismissButton: true
                });
            });
        };

				//Calculating Total
					$scope.calculateTotal=function(){
					$scope.pkg.total_charges=$scope.pkg.OT_charges+$scope.pkg.equipment_limit+$scope.pkg.medicine_limit;

					for(var i=0; i<$scope.pkg.servicesData.length; i++){
						$scope.pkg.total_charges=$scope.pkg.total_charges+$scope.pkg.servicesData[i].total;
					}
					if($scope.pkg.bed_info.days>0){
						for(var i=0; i<$scope.wardss.length; i++){
							if($scope.wardss[i].children.length===0){
								if($scope.wardss[i].name===$scope.pkg.bed_info.category)
								{
									var bedcharges= $scope.wardss[i].bedPrice*$scope.pkg.bed_info.days;
									$scope.pkg.total_charges=$scope.pkg.total_charges+bedcharges;

								}
							}

						}

					}
				}

				$scope.selectFacility = function(facility,service,index){

						var found = false;
						for(var i=0;i<$scope.pkg.servicesData.length;i++){
								if(facility.description===$scope.pkg.servicesData[i].description){
										found = true;
										ngToast.create({
												className: 'danger',
												content: 'Already in list.',
												dismissButton: true
										});
										$timeout(function() {
												var setFocus = angular.element(document.querySelector('#servicesInput'+index.toString()));
												setFocus[0].focus();
										},300);
										break;
								}
						}

						if(found === false){
								service.service_id = facility._id;
								service.description = facility.description;
								service.fee = facility.price;
								service.quantity = 1;
								service.category = facility.category;
								service.total=service.fee*service.quantity;

								$scope.addService(index+1);
								$scope.calculateTotal();
								$scope.dropdownFlag = undefined;
						}

				};

				$scope.discountChange = function(index){
							$scope.pkg.servicesData[index].total=$scope.pkg.servicesData[index].fee * $scope.pkg.servicesData[index].quantity;

        };

				$scope.addService = function(index){
            $scope.pkg.servicesData.push({
                'description' : '',
                'fee' : 0,
                'quantity' : 0,
                'total' : 0
            });
						if(index){
            $timeout(function() {
                var setFocus = angular.element(document.querySelector('#servicesInput'+index.toString()));
                setFocus[0].focus();
            },300);
					}
					};

					$scope.OTA_changed=function(index){
						for(var i=0; i<$scope.OTAs.length; i++)
						{
							if($scope.OTAs[i].displayName===$scope.pkg.OTA_charges[index].name){
								$scope.pkg.OTA_charges[index]._id= $scope.OTAs[i]._id;
							}
						}
					}

					$scope.anesthetist_changed=function(index){
						for(var i=0; i<$scope.doctors.length; i++)
						{
							if($scope.doctors[i].name===$scope.pkg.anesthetist_charges[index].name){
								$scope.pkg.anesthetist_charges[index]._id= $scope.doctors[i]._id;
							}
						}
					}

					$scope.surgeon_changed=function(index){
						for(var i=0; i<$scope.doctors.length; i++)
						{
							if($scope.doctors[i].name===$scope.pkg.surgeon_charges[index].name){
								$scope.pkg.surgeon_charges[index]._id= $scope.doctors[i]._id;
							}
						}
					}

					$scope.addSurgeon = function(index){
	            $scope.pkg.surgeon_charges.push({
	                'name' : '',
									'_id':'',
	                'fee' : 0,
	            });
							if(index){
	            $timeout(function() {
	                var setFocus = angular.element(document.querySelector('#SurgeonInput'+index.toString()));
	                setFocus[0].focus();
	            },300);
						}
						};

						$scope.addAnesthetist = function(index){
		            $scope.pkg.anesthetist_charges.push({
		                'name' : '',
										'_id': '',
		                'fee' : 0,
		            });
								if(index){
		            $timeout(function() {
		                var setFocus = angular.element(document.querySelector('#AnesthetistInput'+index.toString()));
		                setFocus[0].focus();
		            },300);
							}
							};
							$scope.addOTA = function(index){
			            $scope.pkg.OTA_charges.push({
										'name' : '',
										'_id': '',
			                'fee' : 0,
			            });
									if(index){
			            $timeout(function() {
			                var setFocus = angular.element(document.querySelector('#OTAInput'+index.toString()));
			                setFocus[0].focus();
			            },300);
								}
								};

				$scope.addService();

				$scope.removeService = function(index){
            $scope.pkg.servicesData.splice(index,1);
						$scope.calculateTotal();
        };
				$scope.removeAnesthetist = function(index){
            $scope.pkg.anesthetist_charges.splice(index,1);
        };
				$scope.removeOTA = function(index){
            $scope.pkg.OTA_charges.splice(index,1);
        };

				$scope.removeSurgeon = function(index){
            $scope.pkg.surgeon_charges.splice(index,1);
        };


				$scope.openDropdown = function(index){
                $scope.dropdownFlag = index;
                if (!e) var e = window.event;
                    e.cancelBubble = true;
                    if (e.stopPropagation) e.stopPropagation();
        };

        $scope.closeDropdown = function(event){
                $scope.dropdownFlag = undefined;
        };

				$scope.onKeydown = function(item, $event) {
						var e = $event;
						var $target = $(e.target);
						var nextTab;
						switch (e.keyCode) {
								case KeyCodes.ESCAPE:
										$target.blur();
										break;
								case KeyCodes.UPARROW:
										nextTab = - 1;
										break;
								case KeyCodes.RETURNKEY: e.preventDefault();
								case KeyCodes.DOWNARROW:
										nextTab = 1;
										break;
						}
						if (nextTab != undefined) {
								// do this outside the current $digest cycle
								// focus the next element by tabindex
								if (parseInt($target.attr('tabindex')) + nextTab !== 0)
										$timeout(function() { $('[tabindex=' + (parseInt($target.attr('tabindex')) + nextTab) + ']').focus()});
						}
				};

				$scope.onFocus = function(item, $event) {
						// clear all other items
						angular.forEach($scope.facilities, function(item) {
								item.selected = undefined;
						});
						// select this one
						item.selected = 'selected';
				};

				//Geting wards here
       	var getDepartments = function() {
            ipd_service.get_all_wards().then(function (response) {
                $scope.wardss = angular.copy(response.data);
								var j=0;
								for(var i=0; i<$scope.wardss.length;i++){
									if($scope.wardss[i].children.length===0){
										$scope.departments[j]=$scope.wardss[i].name;
										j=j+1;
									}
									}
							//	$scope.departments=angular.copy($scope.wardss.name);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Wards.',
                    dismissButton: true
                });
            });
        };

        // callback functions
        $scope.callbackpatient = function(selected_package) {
            var packageIndex = getIndex($scope.packageData, 'name', selected_package[0].name);
          //  $scope.packageData[patientIndex].dob = new Date($scope.patientData[patientIndex].dob);

            $scope.pkg = $scope.packageData[packageIndex];
            $scope.updateItem = true;
            $scope.showPackages = false;
            $scope.searchPackageKeyword = '';
        };


        // retrieve data for patient searching
        $scope.getPackages = function(searchType) {
        	if ($scope.searchPackageKeyword.length > 0) {
                $scope.processing = true;
                ipd_service.search_packages($scope.searchPackageKeyword).then(function (response) {
                    $scope.packageData = [];
                    $scope.packageData = response.data;
                    $scope.packages = [];
                    $scope.headingsPackage = [];
					$scope.prefixPackage = 'patientdiv';
					$scope.headingsPackage.push({'alias': 'Name', 'name': 'name', 'width': 100});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.packages.push({'name': response.data[i].name});
                    }
                    if (searchType === 'packages')
                        $scope.showPackages = true;

                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPackage + $scope.prefixPackage));
                  //  $timeout(function() {setFocus[0].focus()});
                    $scope.searchPackageKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve packages.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter package name to search.',
                    dismissButton: true
                });
            }
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.updateItem = false;
            $scope.pkg = angular.copy(pkg);
        };

        // manage patients functionaltiy
        $scope.submit_package = function(patient, form) {
            $scope.processing = true;
            ipd_service.create_package($scope.pkg).then(function (response) {

                $scope.processing = false;
                $scope.reset(form);
                $scope.pkg = angular.copy(response.data);
                $scope.updateItem = true;

                ngToast.create({
                    className: 'success',
                    content: 'Package Added Successfully',
                    dismissButton: true
                });

            }).catch(function (error) {
                $scope.processing = false;
                if(error.data.code===11000){
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Package already registered.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add package.',
                        dismissButton: true
                    });
                }
            });
        };


        $scope.update_package = function(pkg, form) {
            $scope.processing = true;
            ipd_service.update_package($scope.pkg).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Package Updated Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                $scope.processing = false;
                $scope.pkg = angular.copy(response.data);
                $scope.updateItem = true;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update package.',
                    dismissButton: true
                });
            });
        };

        $scope.reset_package = function(form) {
            $scope.reset(form);
        };

        $scope.reset();
				$scope.init();

    }
]);
