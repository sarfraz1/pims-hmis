'use strict';

angular.module('ipd').controller('PatientAdmissionController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'opd_service', 'ipd_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, opd_service, ipd_service,  $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');


		 var dateConverter = function(dateinput){
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch (error) {
                return dateinput;
            }
        };


				$scope.referingDoctor={
					'name': "",
					'_id':"",
					'speciality':""
				};
        $scope.showDoctorsDropDown = false;

        $scope.searchPatientKeyword = '';
        $scope.searchDoctor = '';
        $scope.searchDoctorID = '';
        $scope.todayDate = new Date();
				$scope.todayDate = dateConverter($scope.todayDate);

        $scope.isDoctorSelected = false;
        $scope.processing = false;
        $scope.showWalkIn = false;
				$scope.IsSurgeon=false;
				$scope.IsOTA=false;
				$scope.IsAnesthetist=false;
				$scope.selectedpatient_edit=false;
        var doctor_id = '';
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
				$scope.departments=[]
				$scope.packages=[];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

        var patient = {
            'name': '',
            'phone' : null,
            'gender': 'male',
            'dob': undefined,
            'address' : '',
            'cnic': '',
            'mr_number': '',
            'panel_id': 'None'
        };


        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.panels = [];

        var getPanels = function() {
            opd_service.get_panels().then(function (response) {
                $scope.panels = response.data;
                $scope.panels.unshift({description: 'None'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panels.',
                    dismissButton: true
                });
            });
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        var initialize = function() {
            $scope.searchDoctor = '';
            $scope.showMRNumber = false;
			 ipd_service.get_admitted_ipd_patients().then(function (response) {
							$scope.ipdpatients = response.data;
						}).catch(function (error) {
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to retrieve admitted Patients.',
								dismissButton: true
							});
						});
            var today = getDateFormat(new Date());
        };

        initialize();

        getPanels();

		$scope.selectedpatient = function(patient){
			$scope.selectedpatient_edit=true;
			$scope.patient = patient;
			$scope.currstatus = patient.status;
			$scope.currward = patient.bed_details.ward;
		//	$scope.selecteddepartment=
			$scope.currbed = patient.bed_details.bed_description;
			$scope.alreadyadmitted = true;
			//$scope.referingDoctor_search_keyword = patient.refering_doctor.name;
		//	$scope.transfertoward = patient.transfertoward;
			if(patient.package_details){
				$scope.package=true;
				if(patient.package_details){
					$scope.selectedPackageName = patient.package_details.name;
					$scope.getPackageDetails();
				}
			//	 bill.data.billNumber=patient.bill_number;
			}
			else{
				$scope.package=false;
				$scope.selecteddepartment=patient.bed_details.ward;
				$scope.selectedbed={}
				$scope.selectedbed.description=patient.bed_details.description;
				$scope.selectedbed._id=patient.bed_details.bed_id;
				if(patient.refering_doctor){
				$scope.referingDoctor.name=patient.refering_doctor.name;
				$scope.referingDoctor._id=patient.refering_doctor.doc_id;
				$scope.referingDoctor_search_keyword=patient.refering_doctor.name;
			}
			if(patient.assigned_doctor){
				$scope.assignedDoctor={}
				$scope.assignedDoctor.name=patient.assigned_doctor.name;
				$scope.assignedDoctor._id=patient.assigned_doctor.doc_id;
				$scope.assignedDoctor_search_keyword=patient.assigned_doctor.name;
			}
			$scope.getBeds();
			}
		};

        $scope.callbackpatient = function(selected_patient) {
					$scope.reset();
					$scope.selectedpatient_edit=false;
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
						var InPatientIndex=getIndex($scope.ipdpatients,'mr_number',selected_patient[0].mr_number);
            var panelId;
					//	if()
						$scope.patient = $scope.patientData[patientIndex];
					//	$scope.selectedpatient($scope.patientData[patientIndex]);
            $scope.showPatients = false;
            $scope.searchPatientKeyword = '';
            panelId = $scope.patientData[patientIndex].panel_id;
						if(InPatientIndex >= 0){
							$scope.selectedpatient($scope.ipdpatients[InPatientIndex]);
						}

        };

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
        	if ($scope.searchPatientKeyword.length > 0) {
                $scope.processing = true;
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    } else if (searchType === 'walkin') {
                        $scope.showWalkIn = true;
                    }
                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPatient + $scope.prefixPatient));
                    $timeout(function() {setFocus[0].focus()});
                    $scope.searchPatientKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };
        // manage appointments functionality
        $scope.searched_doctors = [];
        var tempDoctor = {};

		var getDepartments = function() {
            ipd_service.get_all_wards().then(function (response) {
                $scope.wardss = angular.copy(response.data);
								var j=0;
								for(var i=0; i<$scope.wardss.length;i++){
									if($scope.wardss[i].children.length===0){
										$scope.departments[j]=$scope.wardss[i].name;
										j=j+1;
									}
									}

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Wards.',
                    dismissButton: true
                });
            });
        };
		getDepartments();

		var getPackages=function(){
			ipd_service.get_package_names().then(function (response) {

			$scope.packages	= angular.copy(response.data);
			if($scope.patient){
			}

			}).catch(function (error) {
					ngToast.create({
							className: 'danger',
							content: 'Error: Unable to retrieve Packages.',
							dismissButton: true
					});
			});
		}
		getPackages();

		$scope.getPackageDetails=function(){
			ipd_service.get_package_detail($scope.selectedPackageName).then(function(response){
				$scope.selectedPackage = angular.copy(response.data);
				$scope.beds=[];
				$scope.selectedbed=undefined;
				$scope.selectedOTA=undefined;
				$scope.selectedSurgeon=undefined;
				$scope.selectedAnesthetist=undefined;

				if($scope.selectedPackage.bed_info.days>0)
				{
					ipd_service.get_beds_by_ward($scope.selectedPackage.bed_info.category).then(function (response) {

		                $scope.beds = angular.copy(response.data);
										$scope.packageWard=$scope.selectedPackage.bed_info.category;
		            }).catch(function (error) {
		                ngToast.create({
		                    className: 'danger',
		                    content: 'Error: Unable to retrieve beds.',
		                    dismissButton: true
		                });
		            });
				}
				if($scope.selectedPackage.surgeon_charges[0].name!==""){
					$scope.IsSurgeon=true;
				}
				else{$scope.IsSurgeon=false;}
				if($scope.selectedPackage.anesthetist_charges[0].name!==""){
					$scope.IsAnesthetist=true;
				}
				else{$scope.IsAnesthetist=false;}
				if($scope.selectedPackage.OTA_charges[0].name!==""){
					$scope.IsOTA=true;
				}
				else{
					$scope.IsOTA=false;
				}
				if($scope.patient){
					var patient=$scope.patient;
				if(patient.bed_details){
					$scope.selectedbed={};
				 $scope.selectedbed._id= patient.bed_details.bed_id;
				 $scope.selectedbed.description=patient.bed_details.bed_description;
				 $scope.selecteddepartment=patient.bed_details.ward;
			 }
			 if(patient.surgeon){
				 $scope.IsSurgeon=true;
					$scope.selectedSurgeon = patient.surgeon.name;
					$scope.selectedSurgeonID = patient.surgeon.doc_id;
				}

				if(patient.anesthetist){
					$scope.IsAnesthetist=true;
					 $scope.selectedAnesthetist=patient.anesthetist.name;
					$scope.selectedAnesthetistID=patient.anesthetist.id;
				}
				if(patient.OTA){
					$scope.IsOTA=true;
					$scope.selectedOTA=patient.OTA.name;
					$scope.selectedOTAID=patient.OTA.id;
				}
				if(patient.refering_doctor){
					$scope.referingDoctor.name = patient.refering_doctor.name;
					$scope.referingDoctor._id = patient.refering_doctor._id;
					$scope.referingDoctor_search_keyword=patient.refering_doctor.name;
				}
			}


			}).catch(function (error) {
					ngToast.create({
							className: 'danger',
							content: 'Error: Unable to retrieve Package',
							dismissButton: true
					});
			});
		};

	 $scope.calculateTotalCost=function(){
		 $scope.totalcost=$scope.selectedPackage.total_charges;
		 if($scope.selectedPackage.surgeon_charges.length>0){
			 if($scope.selectedSurgeon){
				 for(var i=0;i < $scope.selectedPackage.surgeon_charges.length; i++)
				 {
					 if($scope.selectedPackage.surgeon_charges[i].name===$scope.selectedSurgeon){
						 $scope.totalcost=$scope.totalcost+$scope.selectedPackage.surgeon_charges[i].fee;
					 }
				 }
			 }
		 }

		 if($scope.selectedPackage.anesthetist_charges.length>0){
			 if($scope.selectedAnesthetist){
				 for(var i=0;i<$scope.selectedPackage.anesthetist_charges.length; i++)
				 {
					 if($scope.selectedPackage.anesthetist_charges[i].name===$scope.selectedAnesthetist){
						 $scope.totalcost=$scope.totalcost+$scope.selectedPackage.anesthetist_charges[i].fee;
					 }
				 }
			 }

		 }

		 if($scope.selectedPackage.OTA_charges.length>0){
			 if($scope.selectedOTA){
 				for(var i=0;i<$scope.selectedPackage.OTA_charges.length; i++)
 				{
 					if($scope.selectedPackage.OTA_charges[i].name===$scope.selectedOTA){
 						$scope.totalcost=$scope.totalcost+$scope.selectedPackage.OTA_charges[i].fee;
 					}
 				}
 			}
		 }
	 }

		$scope.NewValue=function(value){
			$scope.package=!$scope.package;
			$scope.beds=[];
			$scope.selectedbed=undefined;
			$scope.selectedPackage=undefined;
			$scope.selectedOTA=undefined;
			$scope.selectedSurgeon=undefined;
			$scope.selectedAnesthetist=undefined;
		}

		$scope.search_refering_doctor = function() {
				if ($scope.referingDoctor_search_keyword.length > 0) {
						opd_service.search_doctor_by_name($scope.referingDoctor_search_keyword).then(function (response) {
								$scope.searched_doctor = response.data;
								$scope.show_refering_dropdown = true;
						});
				}

				if ($scope.referingDoctor_search_keyword.length === 0) {
						$scope.show_refering_dropdown = false;
				}

		};
		$scope.search_assigned_doctor = function() {
				if ($scope.assignedDoctor_search_keyword.length > 0) {
						opd_service.search_doctor_by_name($scope.assignedDoctor_search_keyword).then(function (response) {
								$scope.searched_doctor = response.data;
								$scope.show_assigned_dropdown = true;
						});
				}

				if ($scope.assignedDoctor_search_keyword.length === 0) {
						$scope.show_assigned_dropdown = false;
				}

		};

		$scope.add_search_assignedDoctor = function(doc){
				$scope.show_assigned_dropdown = false;
				$scope.assignedDoctor_search_keyword = doc.name;
				$scope.assignedDoctor={
					'name': "",
					'_id':"",
					'speciality':""
				};
				$scope.assignedDoctor.name=doc.name;
				$scope.assignedDoctor._id = doc._id;
				$scope.assignedDoctor.speciality = doc.speciality;
		};
		$scope.add_search_referingDoctor = function(doc){
				$scope.show_refering_dropdown = false;
				$scope.referingDoctor_search_keyword = doc.name;
				$scope.referingDoctor={
					'name': "",
					'_id':"",
					'speciality':""
				};
				$scope.referingDoctor.name = doc.name;
				$scope.referingDoctor._id = doc._id;
				$scope.referingDoctor.speciality = doc.speciality;
		};

        $scope.searchDoctors = function(change, check) {
            doctor_id = null;
            if (check)
                changeSearch(check);
            if (change === 'change')
                $scope.showAppointments = false;
            $scope.searched_doctors = [];
            $scope.processing = true;
            if ($scope.searchDoctor.length > 0) {
                opd_service.search_doctor_by_name($scope.searchDoctor).then(function (response) {
                    $scope.searched_doctors = response.data;
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            } else {
                opd_service.list_all_doctors().then(function (response) {
                    $scope.searched_doctors = response.data;
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            }
            if ($scope.searchDoctor === '') {
                $scope.showDoctorsDropDown = false;
            }
        };
		$scope.searchDoctors();

		$scope.showAdvance=function(){
			return (!$scope.package & !$scope.alreadyadmitted)
		}

// Admit Patient
        $scope.submit = function() {


					if($scope.package){
							$scope.calculateTotalCost()
							if($scope.selectedPackage===undefined)
							{
								ngToast.create({
									className: 'danger',
									content: 'Please Select a Package.',
									dismissButton: true
								});

							}
							else{
								var correctpackage=true;
								if($scope.IsSurgeon && $scope.selectedSurgeon===undefined)
								{
									correctpackage = false;
									ngToast.create({
										className: 'danger',
										content: 'Please Select a Surgeon.',
										dismissButton: true
									});
								}
								if($scope.IsAnesthetist && $scope.selectedAnesthetist===undefined)
								{
									correctpackage = false;
									ngToast.create({
										className: 'danger',
										content: 'Please Select an Anesthetist.',
										dismissButton: true
									});

								}
								if($scope.IsOTA && $scope.selectedOTA===undefined)
								{
									correctpackage = false;
									ngToast.create({
										className: 'danger',
										content: 'Please Select an OTA.',
										dismissButton: true
									});
								}
								if($scope.selectedPackage.bed_info.days>0 && $scope.selectedbed===undefined)
								{
									correctpackage = false;
									ngToast.create({
										className: 'danger',
										content: 'Please Select a Bed.',
										dismissButton: true
									});
								}
								if($scope.selectedbed)
								{
									if($scope.selectedbed.current_status==="Occupied"){
										correctpackage = false;
										ngToast.create({
											className: 'danger',
											content: 'Please Select an Unoccupied Bed.',
											dismissButton: true
										});
									}
								}
								if(correctpackage)
								{
								for(var i=0; i<$scope.selectedPackage.surgeon_charges.length; i++){
									if($scope.selectedPackage.surgeon_charges[i].name===$scope.selectedSurgeon && $scope.selectedPackage.surgeon_charges[i].name!== "")
									{
										$scope.selectedSurgeonID=$scope.selectedPackage.surgeon_charges[i]._id;
										$scope.selectedSurgeonFee=$scope.selectedPackage.surgeon_charges[i].fee
									}
								}
						for(var i=0; i<$scope.selectedPackage.anesthetist_charges.length; i++){
							if($scope.selectedPackage.anesthetist_charges[i].name===$scope.selectedAnesthetist && $scope.selectedPackage.anesthetist_charges[i].name!=="")
							{
								$scope.selectedAnesthetistID=$scope.selectedPackage.anesthetist_charges[i]._id;
								$scope.selectedAnesthetistFee=$scope.selectedPackage.anesthetist_charges[i].fee;
							}
						}
						for(var i=0; i<$scope.selectedPackage.OTA_charges.length; i++){
							if($scope.selectedPackage.OTA_charges[i].name===$scope.selectedOTA && $scope.selectedPackage.OTA_charges[i].name!=="")
							{
								$scope.selectedOTAID=$scope.selectedPackage.OTA_charges[i]._id;
								$scope.selectedOTAFee=$scope.selectedPackage.OTA_charges[i].fee;
							}
						}
						var billingDetails = {

	                    'patientInfo' : {
	                        'mr_number' : $scope.patient.mr_number,
	                        'name' : $scope.patient.name,
	                        'phone' : $scope.patient.phone
	                    },
	                    'servicesInfo' : [{
	                        'service_id' : $scope.selectedPackageName,
	                        'description' : $scope.selectedPackageName,
													'category' : 'Hospital-IPD-Packages',
	                        'fee' : $scope.totalcost,
	                        'discount' : 0,
	                        'quantity' : 1,
													'Surgeon':{
														'name':$scope.selectedSurgeon,
														'fee':$scope.selectedSurgeonFee,

														'_id':$scope.selectedSurgeonID
													},
													'Anesthetist':{
														'name':$scope.selectedAnesthetist,
														'fee':$scope.selectedAnesthetistFee,

														'_id':$scope.selectedAnesthetistID
													},
													'OTA':{
														'name':$scope.selectedOTA,
														'fee':$scope.selectedOTAFee,

														'_id':$scope.selectedOTAID
													},
	                    }],

											'referingDoctor':{
												'name':$scope.referingDoctor.name,
												'_id':$scope.referingDoctor._id
											},
	                    'grandTotal' : $scope.totalcost,
	                    'recievedCash' : 0,
	                    'discount' : 0,
	                    'paymentMethod' : 'cash',
	                };
									opd_service.save_bill(billingDetails).then(function (bill) {
						$scope.selectedbed.current_status = "Occupied";
						ipd_service.update_bed_details($scope.selectedbed).then(function(response) {

							var ipd_patient = {
							name: $scope.patient.name,
							mr_number: $scope.patient.mr_number,
							phone: $scope.patient.phone,
							bill_number: bill.data.billNumber,
							bed_details: {
								bed_id: $scope.selectedbed._id,
								bed_description: $scope.selectedbed.description,
								ward: $scope.packageWard
							},
							surgeon: {
								name: $scope.selectedSurgeon,
								doc_id: $scope.selectedSurgeonID
							},
							anesthetist: {
								name: $scope.selectedAnesthetist,
								id: $scope.selectedAnesthetistID
							},
							OTA: {
								name: $scope.selectedOTA,
								id: $scope.selectedOTAID
							},
							refering_doctor: {
								name: $scope.referingDoctor.name,
								doc_id: $scope.referingDoctor._id
							},
							package_details:{
								name:$scope.selectedPackage.name,
								package_id: $scope.selectedPackage._id
							}
						};

						ipd_service.create_ipd_patient(ipd_patient).then(function(response) {
							$scope.patient.admissionStatus='Admitted';
							$scope.generate_bill();
							ngToast.create({
								className: 'success',
								content: 'Admission Successful.',
								dismissButton: true
							});
						}).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Wards.',
                    dismissButton: true
                });
            });

				});
			});
		}
	}
		}else{
			var correctpackage=true;
			if($scope.selecteddepartment===undefined)
			{
					correctpackage=false;
				ngToast.create({
					className: 'danger',
					content: 'Please Select a Ward.',
					dismissButton: true
				});
			}
			if($scope.selectedbed===undefined)
			{
				correctpackage=false;
				ngToast.create({
					className: 'danger',
					content: 'Please Select a Bed.',
					dismissButton: true
				});
			}
				if($scope.selectedbed)
				{
					if($scope.selectedbed.current_status==="Occupied"){
						correctpackage=false;
						ngToast.create({
							className: 'danger',
							content: 'Please Select an Unoccupied Bed.',
							dismissButton: true
						});
					}
				}
			/*	if(!$scope.advance){
					correctpackage=false;
					ngToast.create({
						className: 'danger',
						content: 'Please add Advance Payment.',
						dismissButton: true
					});
				}*/
				if(!$scope.assignedDoctor){
					correctpackage=false;
					ngToast.create({
						className: 'danger',
						content: 'Please select an assigned doctor.',
						dismissButton: true
					});
				}
				if(correctpackage){

			var billingDetails = {

								'patientInfo' : {
										'mr_number' : $scope.patient.mr_number,
										'name' : $scope.patient.name,
										'phone' : $scope.patient.phone
								},
								'servicesInfo' : [{
									'service_id' : 'Admission Fee',
								'description' : 'Admission Fee',
								'category' : 'Hospital-IPD-Admission Fee',
								'fee' : 2000,
								'discount' : 0,
								'quantity' : 1
						}],
						'referingDoctor':{
							'name':$scope.referingDoctor.name,
							'_id':$scope.referingDoctor._id
						},
						'referedDoctor':{
							'name':$scope.assignedDoctor.name,
							'_id':$scope.assignedDoctor._id
						},
								'grandTotal' : 2000,
								'recievedCash' : 0,
								'discount' : 0,
								'paymentMethod' : 'cash',
						};

			opd_service.save_bill(billingDetails).then(function (bill) {
			$scope.selectedbed.current_status = "Occupied";
			ipd_service.update_bed_details($scope.selectedbed).then(function(response) {

				var ipd_patient = {
				name: $scope.patient.name,
				mr_number: $scope.patient.mr_number,
				phone: $scope.patient.phone,
				bill_number: bill.data.billNumber,
				bed_details: {
					bed_id: $scope.selectedbed._id,
					bed_description: $scope.selectedbed.description,
					ward: $scope.selecteddepartment
				},
				refering_doctor: {
					name: $scope.referingDoctor.name,
					doc_id: $scope.referingDoctor._id
				},
				assigned_doctor: {
					name: $scope.assignedDoctor.name,
					doc_id: $scope.assignedDoctor._id
				},
			};

			ipd_service.create_ipd_patient(ipd_patient).then(function(response) {
				$scope.generate_bill();
				$scope.patient.admissionStatus='Admitted';
				ngToast.create({
					className: 'success',
					content: 'Admission Successful.',
					dismissButton: true
				});
			}).catch(function (error) {
					ngToast.create({
							className: 'danger',
							content: 'Error: Unable to Admit.',
							dismissButton: true
					});
			});
		});
});
	}
}
		};

		$scope.sortBy = function(propertyName) {
				$scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
						? !$scope.reverse : true;
				$scope.propertyName = propertyName;
		};

		$scope.sortBy('created');

		$scope.getBeds = function() {
			ipd_service.get_beds_by_ward($scope.selecteddepartment).then(function (response) {

                $scope.beds = angular.copy(response.data);
								$scope.beds = $scope.beds.sort((a, b) => {
                      return a.bed_no - b.bed_no;
										})
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve beds.',
                    dismissButton: true
                });
            });
		};

		$scope.selectbed = function(index){
			$scope.selectedbed = $scope.beds[index];
		};

        $scope.calculateDOB = function(){
            var todayDate = new Date();
            $scope.patient.dob = new Date((todayDate.getFullYear()-$scope.patient.age)+'-'+(todayDate.getMonth()+1).toString()+'-'+todayDate.getDate());
        };

        $scope.checkDOB = function(dob){
            var todayDate = new Date();
            if(dob>todayDate){
                $scope.patient.dob = undefined;
                ngToast.create({
                    className: 'danger',
                    content: 'Enter Correct Date of birth.',
                    dismissButton: true
                });
            }
        };

		$scope.generate_bill = function() {
			$location.path('/ipdreception/ipdgenerateBill/'+$scope.patient.mr_number);
            $scope.switchTabs('generateBill');
		}

		$scope.discharge=function(){
			if($scope.patient){
				if($scope.selectedbed){
				$scope.selectedbed.current_status = 'Free';
				$scope.selectedbed.occupant_details = {};
				ipd_service.update_bed_details($scope.selectedbed).then(function(response) {
					ngToast.create({
						className: 'success',
						content: 'Bed Free',
						dismissButton: true
					});

				$scope.patient.status='Discharged';
				ipd_service.update_ipd_patient($scope.patient).then(function(response){
					ngToast.create({
						className: 'success',
						content: 'Discharge Successful.',
						dismissButton: true
					});
					$scope.generate_bill();
				}).catch(function (error) {
						ngToast.create({
								className: 'danger',
								content: 'Error: Unable to Discharge.',
								dismissButton: true
						});
				});
			});
		}
		else{
			ipd_service.update_ipd_patient($scope.patient).then(function(response){
				ngToast.create({
					className: 'success',
					content: 'Discharge Successful.',
					dismissButton: true
				});
			}).catch(function (error) {
					ngToast.create({
							className: 'danger',
							content: 'Error: Unable to Discharge.',
							dismissButton: true
					});
		});
			}
		}
	};

		$scope.update = function() {

			var objtobill = {
				'servicesInfo' : [{
                        'service_id' : 'Bed charges',
                        'description' : 'Bed Charges '+$scope.patient.bed_details.ward+'-'+$scope.patient.bed_details.bed_description,
                        'fee' : 3000,
                        'discount' : 0,
                        'quantity' : 1
				}],
				'mr_number' : $scope.patient.mr_number
			};
			ipd_service.add_services_bill(objtobill)
                .then(function (response) {
					ngToast.create({
                    className: 'success',
                    content: 'Patient Bill updated.',
                    dismissButton: true
                });
			});
			$scope.patient.status = 'Admitted';
			$scope.patient.bed_details.ward = $scope.selecteddepartment;
			$scope.patient.bed_details.bed_description = $scope.selectedbed.description;
			$scope.patient.bed_details.bed_id =  $scope.selectedbed._id;

			$scope.processing = true;
			ipd_service.update_ipd_patient($scope.patient)
                .then(function (response) {
					ngToast.create({
                    className: 'success',
                    content: 'Patient status updated.',
                    dismissButton: true
                });
				$scope.reset();
				$scope.processing = false;
				$scope.alreadyadmitted = false;

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update status.',
                    dismissButton: true
                });
				$scope.processing = false;
            });
		};

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.showMRNumber = false;
            $scope.patient = {};
						$scope.beds=[];
						if($scope.selectedPackageName)
						$scope.selectedPackageName=undefined;
						if($scope.selectedbed)
						$scope.selectedbed=undefined;
						if($scope.selectedOTA)
						$scope.selectedOTA=undefined;
						if($scope.selectedSurgeon)
						$scope.selectedSurgeon=undefined;
						if($scope.selectedAnesthetist)
						$scope.selectedAnesthetist=undefined;
						if($scope.referingDoctor_search_keyword)
						$scope.referingDoctor_search_keyword=undefined;
						if($scope.assignedDoctor_search_keyword)
						$scope.assignedDoctor_search_keyword=undefined;
						if($scope.selectedPackage)
						$scope.selectedPackage={};
						if($scope.selecteddepartment)
						$scope.selecteddepartment=undefined;
						$scope.package=false;
						$scope.alreadyadmitted=false;
            $scope.isDoctorSelected = false;
        };
		$scope.reset();
    }
]);
