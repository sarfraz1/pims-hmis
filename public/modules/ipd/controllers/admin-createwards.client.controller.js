'use strict';

angular.module('ipd').controller('CreateWardsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','ipd_service',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll, ipd_service) {

        $scope.authentication = Authentication;
        $scope.saving = false;
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');


        var getNode = function(tree){
        
            ipd_service.get_wards(tree.name).then(function (response) {
                if(response.data.children){
                    tree.saved = true;
                    for(var i=0;i<response.data.children.length;i++){
                        tree.nodes.push({name: response.data.children[i],saved:true ,nodes: []});
                        getNode(tree.nodes[i]);
                    }
                }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve facilities.',
                    dismissButton: true
                });
            });
        };

        $scope.delete = function(data) {
            
            ipd_service.remove_wards(data).then(function (response) {
                data.nodes = [];
            }).catch(function (error) {
                console.log(error);
                if(error.data.message){
                    ngToast.create({
                        className: 'danger',
                        content: error.data.message,
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to remove children.',
                        dismissButton: true
                    });
                }
            });
            
        };

        $scope.saveNode = function(data){
            data.saved = true;
            $scope.saving = false;
            data.parentNode = $scope.parentNode.name;
            ipd_service.create_wards(data).then(function (response) {
                console.log(response.data);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        $scope.add = function(data) {
            $scope.saving = true;
            $scope.parentNode = data;
            data.nodes.push({name: '',saved:false,nodes: []});
        };
        
        $scope.tree = [{name: "Hospital",saved:false ,nodes: []}];
        $scope.parentNode = {};
        getNode($scope.tree[0],'Hospital');
        // $scope.submit = function(){
        //     hospitalAdmin_service.create_facilities($scope.tree[0]).then(function (response) {
        //         console.log(response.data);
        //     }).catch(function (error) {
        //         ngToast.create({
        //             className: 'danger',
        //             content: 'Error: Unable to retrieve doctors.',
        //             dismissButton: true
        //         });
        //     });
        // };
        
    }
]);