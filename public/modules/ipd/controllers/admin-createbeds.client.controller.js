
'use strict';

angular.module('ipd').controller('CreateBedsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','ipd_service','hospitalAdmin_service',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll,ipd_service, hospitalAdmin_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.saving = false;
        $scope.nodes = [];
        $scope.showGrid = false;
        $scope.selectedService = '';
			//	$scope.services=[];

        $scope.services = [{
            'description' : '',
            'price' : undefined,
            'category' : ''
        }];

        var get_facilities = function(name){
            ipd_service.get_wards(name).then(function (response) {
                if(response.data.children.length>0){
                    response.data.selected = '';
                    $scope.showGrid = false;
                    $scope.nodes.push(response.data);
                }
                else {
                    var category = 'Hospital';
											//console.log("UpdateWards Called");
											$scope.selected_ward=response.data;
												$scope.showGen = true;
											if($scope.selected_ward.NoOfBeds)
											{
											$scope.cost=$scope.selected_ward.bedPrice;
											$scope.numofbeds=$scope.selected_ward.NoOfBeds;
										for(var j=0;j<$scope.selected_ward.NoOfBeds; j++)
										{
											var k=j+1;
											var service={
													'description' : $scope.selected_ward.name + "-" + k,
													'price' : $scope.selected_ward.bedPrice,
													'category' : ''
											};
											$scope.services.push(service);
										}

										if($scope.selected_ward.NoOfBeds<1)
										{
											$scope.update=false;
											$scope.services = [{
													'description' : '',
													'price' : undefined,
													'category' : ''
											}];

										}
										else{
											$scope.update=true;
											//$scope.cost=response.data.bedPrice;
										//	$scope.numofbeds=response.data.NoOfBeds;
										}
									}
									else{
									$scope.update=false;
									$scope.cost=0;
									$scope.numofbeds=0;
								}
										$scope.showGrid = true;
										$scope.prefix=$scope.nodes[$scope.nodes.length-1].selected+'-';

            }}).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve beds.',
                    dismissButton: true
                });
            });
        };


        $scope.selectFacility = function(index){
            //if()
            var i = index+1;
            var length = $scope.nodes.length-index;
            if(i>0)
                $scope.nodes.splice(i,length);
            $scope.services = [];
            get_facilities($scope.nodes[index].selected);
        };


//Add Bed Function
        $scope.addService = function(){
					var category = 'Hospital';
					for(var j=0;j<$scope.nodes.length;j++){
						category = category+'-'+$scope.nodes[j].selected;
					}
					var num=$scope.selected_ward.NoOfBeds +1;
					var newBed={
							'description' : $scope.prefix + "-" +num,
							'price' : $scope.selected_ward.bedPrice,
							'category' : category
					};

						var ward_update={'name':$scope.nodes[$scope.nodes.length-1].selected,
							'bedPrice': $scope.cost,
							'NoOfBeds': num
						};

						ipd_service.update_wards(ward_update).then(function (response) {
							console.log("UpdateWards Called");
							$scope.selected_ward=response.data;
							$scope.services.push(newBed);
							$scope.numofbeds=$scope.numofbeds+1;
							ipd_service.create_bed_price(newBed).then(function (response) {
															//$scope.services=response.data;
				                            ngToast.create({
				                                className: 'success',
				                                content: 'Success: Beds Generated.',
				                                dismissButton: true
				                            });
																		$scope.update=true;
				                      //  }
				                    }).catch(function (error) {
				                        ngToast.create({
				                            className: 'danger',
				                            content: 'Error: Unable to add beds.',
				                            dismissButton: true
				                        });
				                    });


						}).catch(function (error) {
							console.log(error.message);
								ngToast.create({
										className: 'danger',
										content: 'Error: Unable to add bed.',
										dismissButton: true
								});
						});

        };

        $scope.removeItem = function(objList,index){
            objList.splice(index,1);
        };


//Generate Beds Function.
		$scope.generatebeds = function() {
			var category = 'Hospital';
			var ward_update={'name':$scope.nodes[$scope.nodes.length-1].selected,
				'bedPrice': $scope.cost,
				'NoOfBeds': $scope.numofbeds
			};
			console.log(ward_update);

			ipd_service.update_wards(ward_update).then(function (response) {
				console.log("UpdateWards Called");
				$scope.selected_ward=response.data;
			}).catch(function (error) {
				console.log(error.message);
					ngToast.create({
							className: 'danger',
							content: 'Error: Unable to generate beds.',
							dismissButton: true
					});
			});

			var count = 0;
			$scope.services=[];
			for(var j=0;j<$scope.nodes.length;j++){
				category = category+'-'+$scope.nodes[j].selected;
			}
			for(var i=1; i<=$scope.numofbeds; i++) {
				var obj = { 'bed_no': i,
							'description': $scope.prefix+i,
							'price' : $scope.cost,
							'category': category
				};

				$scope.services.push(obj);

			ipd_service.create_bed_price(obj).then(function (response) {
											//$scope.services=response.data;
                        count++;
                        if(count===$scope.numofbeds){
													var facilityPrice={
														'description' : $scope.prefix + "charges",
						                'price' : $scope.cost,
						                'category' : 'Hospital-IPD-Bed'
													}

													hospitalAdmin_service.create_facility_price(facilityPrice).then(function (response) {
																	ngToast.create({
																			className: 'success',
																			content: 'Success: Service price saved.',
																			dismissButton: true
																	});
																});

                            ngToast.create({
                                className: 'success',
                                content: 'Success: Beds Generated.',
                                dismissButton: true
                            });
														$scope.update=true;
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to generate beds.',
                            dismissButton: true
                        });
                    });
				}
		}

        get_facilities('Hospital');


// Update Beds Function. 593d50b0d0cb3769aea29e0d

				$scope.updatebeds=function(){
					if( $scope.cost!=$scope.selected_ward.bedPrice){

					var category = 'Hospital';
					for(var j=0;j<$scope.nodes.length;j++){
						category = category+'-'+$scope.nodes[j].selected;
					}
		      $scope.generatebeds();



					}

			}


//Submit
        $scope.submit = function(){
            var category = 'Hospital';
            var count = 0;
            for(var j=0;j<$scope.nodes.length;j++){
                category = category+'-'+$scope.nodes[j].selected;
            }
            for(var i=0;i<$scope.services.length;i++){
                $scope.services[i].category = category;
                if($scope.services[i].description && $scope.services[i].price){
                    ipd_service.create_bed_price($scope.services[i]).then(function (response) {

                        count++;
                        if(count===$scope.services.length){
                            ngToast.create({
                                className: 'success',
                                content: 'Success: Service price saved.',
                                dismissButton: true
                            });
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve doctors.',
                            dismissButton: true
                        });
                    });
                }
            }
        };


    }
]);
