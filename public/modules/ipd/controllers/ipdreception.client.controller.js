'use strict';

angular.module('ipd').controller('IPDReceptionController', ['$scope', '$stateParams', '$interval', 'Authentication', 'ngToast', 'ipd_service',
    function($scope, $stateParams, $interval, Authentication,ngToast, ipd_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.switchTabs = function(value) {
            if (value === 'registerPatient') {
                $scope.managepatients = true;
                $scope.viewpatients = false;
                $scope.patientadmission = false;
                $scope.generatebill = false;
			} else if (value === 'viewPatients') {
                $scope.managepatients = false;
                $scope.viewpatients = true;
                $scope.patientadmission = false;
                $scope.generatebill = false;
            } else if (value === 'patientAdmission') {
                $scope.managepatients = false;
                $scope.viewpatients = false;
                $scope.patientadmission = true;
                $scope.generatebill = false;
            } else if (value === 'generateBill') {
                $scope.managepatients = false;
                $scope.viewpatients = false;
                $scope.patientadmission = false;
                $scope.generatebill = true;
            }
        };
		
		
			ipd_service.get_ipd_patients().then(function (response) {
              //  $scope.processing = false;
                response.data;
				var ispend = false;
				for(var i=0;i<response.data.length;i++) {
					if(response.data[i].status=='Discharge Order' || response.data[i].status=='Transfer Order') {
						ispend = true;
					}
				}
				
				if(ispend == true) {
					ngToast.create({
                    className: 'danger',
                    content: 'Alert: Pending Discharge/Transfer Orders',
                    dismissButton: true
					});
				}
            }).catch(function (error) {
               // $scope.processing = false;
               /* ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });*/
            });
			
		$interval( function() {
			ipd_service.get_ipd_patients().then(function (response) {
              //  $scope.processing = false;
                response.data;
				var ispend = false;
				for(var i=0;i<response.data.length;i++) {
					if(response.data[i].status=='Discharge Order' || response.data[i].status=='Transfer Order') {
						ispend = true;
					}
				}
				
				if(ispend == true) {
					ngToast.create({
                    className: 'danger',
                    content: 'Alert: Pending Discharge/Transfer Orders',
                    dismissButton: true
					});
				}
            }).catch(function (error) {
               // $scope.processing = false;
               /* ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });*/
            });
		},60000);
    }
]);