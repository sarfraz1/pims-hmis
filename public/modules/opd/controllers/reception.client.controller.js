'use strict';

angular.module('opd').controller('ReceptionController', ['$scope', '$stateParams', 'Authentication',
    function($scope, $stateParams, Authentication) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.switchTabs = function(value) {
            if (value === 'registerPatient') {
                $scope.managepatients = true;
                $scope.manageappointments = false;
                $scope.viewappointments = false;
                $scope.patientarrival = false;
                $scope.generatebill = false;
            } else if (value === 'createAppointment') {
                $scope.managepatients = false;
                $scope.manageappointments = true;
                $scope.viewappointments = false;
                $scope.patientarrival = false;
                $scope.generatebill = false;
            } else if (value === 'viewAppointments') {
                $scope.managepatients = false;
                $scope.manageappointments = false;
                $scope.viewappointments = true;
                $scope.patientarrival = false;
                $scope.generatebill = false;
            } else if (value === 'patientArrival') {
                $scope.managepatients = false;
                $scope.manageappointments = false;
                $scope.viewappointments = false;
                $scope.patientarrival = true;
                $scope.generatebill = false;
            } else if (value === 'generateBill') {
                $scope.managepatients = false;
                $scope.manageappointments = false;
                $scope.viewappointments = false;
                $scope.patientarrival = false;
                $scope.generatebill = true;
            }
        };
    }
]);