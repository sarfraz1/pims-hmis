'use strict';

angular.module('opd').controller('PatientArrivalController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'opd_service', 'hospitalAdmin_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, opd_service, hospitalAdmin_service,  $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showAppointments = false;
        $scope.showPatientAppointments = false;
        $scope.showMRNumber = false;
        $scope.showDoctorsDropDown = false;
        $scope.updateItem = false;
        $scope.showAppointmentSave = false;
        $scope.showDoctorListDropdown = false;
        $scope.showAppointmentDelete = false;
        $scope.searchPatientKeyword = '';
        $scope.searchDoctor = '';
        $scope.searchDoctorID = '';
        $scope.appointmentDate = new Date();
        $scope.searchDate = new Date();
        $scope.viewDoctor = '';
        $scope.isDoctorSelected = false;
        $scope.processing = false;
        $scope.showWalkIn = false;
        $scope.register = false;
        $scope.walkinpatient = {};
        $scope.walkinappointment = false;
        var doctor_id = '';
        var walkindoctor = {};
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

        var patient = {
            'name': '',
            'phone' : null,
            'gender': 'male',
            'dob': undefined,
            'address' : '',
            'cnic': '',
            'mr_number': '',
			'mr_prefix': '',
            'panel_id': 'None'
        };

        var appointment = {
            'patientName': '',
            'patientPhoneNumber': null,
            'doctorName': '',
            'date': new Date()
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.panels = [];

        var getPanels = function() {
            opd_service.get_panels().then(function (response) {
                $scope.panels = response.data;
                $scope.panels.unshift({description: 'None'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panels.',
                    dismissButton: true
                });
            });
        };

        $scope.areas = [];

        var getAreas = function() {
            opd_service.get_areas().then(function (response) {
                $scope.areas = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Areas.',
                    dismissButton: true
                });
            });
        };

		getAreas();

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        var initialize = function() {
            $scope.searchDoctor = '';
            $scope.register = false;
            $scope.showWalkIn = false;
            $scope.showMRNumber = false;
            $scope.speciality = 'All';
            var today = getDateFormat(new Date());
            $scope.processing = true;
            opd_service.get_doctor_date_appointments('all', today).then(function (response) {
                $scope.processing = false;
                $scope.doctorAppointments = response.data;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        initialize();

        var getSpecialities = function() {
            opd_service.get_specialities().then(function (response) {
                $scope.specialities = response.data;
                $scope.specialities.unshift({description: 'All'});
                for (var i = 0; i < $scope.specialities.length; i++) {
                    if ($scope.specialities[i].description === 'Medical Officer')
                        $scope.specialities.splice(i, 1);
                }
                $scope.speciality = 'All';
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Specialities.',
                    dismissButton: true
                });
            });
        };

        getSpecialities();
        getPanels();

        $scope.filterSpeciality = function(speciality) {
            return function(doctor) {
                if (speciality === 'All')
                    return doctor;
                else
                    return doctor.speciality === speciality;
            };
        };

        $scope.callbackwalkin = function(selected_patient) {
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
            $scope.walkinpatient = $scope.patientData[patientIndex];
            $scope.showWalkIn = false;
            $scope.showMRNumber = true;
            $scope.searchPatientKeyword = '';
        };

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
        	if ($scope.searchPatientKeyword.length > 0) {
                $scope.processing = true;
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    } else if (searchType === 'walkin') {
                        $scope.showWalkIn = true;
                    }
                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPatient + $scope.prefixPatient));
                    $timeout(function() {setFocus[0].focus()});
                    $scope.searchPatientKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };

        var removeMedicalOfficer = function() {
            for (var i = 0; i < $scope.searched_doctors.length; i++) {
                if ($scope.searched_doctors[i].speciality === 'Medical Officer') {
                    $scope.searched_doctors.splice(i, 1);
                    i--;
                }
            }
        };

        // manage changes on search doctor
        var changeSearch = function(check) {
            if (check === 'appointment') {
                $scope.timeSlot = '';
            } else if (check === 'view') {
                $scope.processing = true;
                opd_service.get_doctor_date_appointments('all', getDateFormat(new Date())).then(function (response) {
                    $scope.doctorAppointments = response.data;
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve appointments.',
                        dismissButton: true
                    });
                });
            } else if (check === 'walkin') {
                $scope.isDoctorSelected = false;
            }
        };

        // manage appointments functionality
        $scope.searched_doctors = [];
        $scope.noAppointments = false;

        var dateConverter = function(dateinput){
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch (error) {
                return dateinput;
            }
        };

        var tempDoctor = {};
        $scope.showDoctorAppointments = function(selectedDoctorSlots) {
            tempDoctor = selectedDoctorSlots;
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(selectedDoctorSlots._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = selectedDoctorSlots.name;
                $scope.selectedDoctor = selectedDoctorSlots;
                $scope.showDoctorsDropDown = false;
            });
        };

        $scope.changeSpeciality = function() {
            $scope.searchDoctor = '';
            $scope.showAppointments = false;
            $scope.timeSlot = '';
            $scope.isDoctorSelected = false;
            $scope.showDoctorsDropDown = false;
        };


        $scope.showDoctorAppointmentsByDate = function() {
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(tempDoctor._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = tempDoctor.name;
                $scope.selectedDoctor = tempDoctor;
                $scope.showDoctorsDropDown = false;
            });
        };

        $scope.searchDoctors = function(change, check) {
            doctor_id = null;
            if (check)
                changeSearch(check);
            if (change === 'change')
                $scope.showAppointments = false;
            $scope.searched_doctors = [];
            $scope.processing = true;
            if ($scope.searchDoctor.length > 0) {
                opd_service.search_doctor_by_name($scope.searchDoctor).then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            } else {
                opd_service.list_all_doctors().then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            }
            if ($scope.searchDoctor === '') {
                $scope.showDoctorsDropDown = false;
            }
        };

        $scope.save_appointment = function(form) {
            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            if (!$scope.noAppointments) {
                var appointment = {
                    'patient_name': $scope.appointment.patientName,
                    'doctor_name': $scope.selectedDoctor.name,
                    'doctor_id': $scope.selectedDoctor._id,
                    'phone': $scope.appointment.patientPhoneNumber,
                    'appointment_date': getDateFormat($scope.appointmentDate),
                    'time': $scope.timeSlot
                }
                if ($scope.showMRNumber) {
                    appointment.mr_number = $scope.appointment.patientMRNumber;
					appointment.mr_prefix = $scope.appointment.mr_prefix;
                }
                if ($scope.appointmentDate < yesterday) {
                    ngToast.create({
                        className: 'warning',
                        content: 'Please select correct date for appointment.',
                        dismissButton: true
                    });
                } else {
                    $scope.processing = true;
                    opd_service.create_appointment(appointment).then(function (response) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'success',
                            content: 'Appointment Saved Successfully',
                            dismissButton: true
                        });
                    }).catch(function (error) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to save appointment.',
                            dismissButton: true
                        });
                    });
                    $scope.reset(form);
                }
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please select correct date for appointment.',
                    dismissButton: true
                });
            }
        };

        $scope.get_appointment = function(timeSlot) {
            if (timeSlot.status !== 'Booked') {
                $scope.timeSlot = timeSlot.slot;
                $scope.showAppointmentDelete = false;
                $scope.showAppointmentSave = true;
            }
        };

		$scope.reset_patient = function(form) {
			$scope.reset(form);
		};

     	$scope.submit_patient_walkin = function(patient, form) {
            $scope.processing = true;
            opd_service.create_patient(patient).then(function (response) {
                var billingDetails = {
                    'created' : response.data.created,
                    'patientInfo' : {
                        'mr_number' : response.data.mr_number,
						'mr_prefix' : response.data.mr_prefix,
                        'name' : response.data.name,
                        'phone' : response.data.phone,
						'panel_id' : response.data.panel_id
                    },
                    'patientMRN' : response.data.mr_number,
                    'servicesInfo' : [{
                        'service_id' : 'registration',
                        'description' : 'Registration',
                        'category' : 'Hospital-Registration',
                        'fee' : 0,
                        'discount' : 0,
                        'quantity' : 1
                    }],
					//'servicesInfo' : [],
                    'grandTotal' : 0,
                    'recievedCash' : 0,
                    'discount' : 0,
                    'paymentMethod' : 'cash',
                };
                opd_service.save_bill(billingDetails).then(function (response) {
                });

                $scope.processing = false;
                if ($scope.walkinappointment) {
                    $scope.walkinpatient.name = response.data.name;
                    $scope.walkinpatient.phone = response.data.phone;
                    $scope.walkinpatient.mr_number = response.data.mr_number;
					$scope.walkinpatient.mr_prefix = response.data.mr_prefix;
                    $scope.showMRNumber = true;
                    $scope.register = false;
                } else {
                    for (var i = 0; i < $scope.doctorAppointments.length; i++){
                        if ($scope.doctorAppointments[i]._id === $scope.appointment_id) {
                            $scope.doctorAppointments[i].mr_number = response.data.mr_number;
							$scope.doctorAppointments[i].mr_prefix = response.data.mr_prefix;
                            $scope.doctorAppointments[i].patient_name = response.data.name;
                            $scope.doctorAppointments[i].phone = response.data.phone;
                            break;
                        }
                    }
                    var Update_patient_info = {
                        'appointment_id': $scope.appointment_id,
                        'patient_name': response.data.name,
                        'mr_number': response.data.mr_number,
						'mr_prefix': response.data.mr_prefix,
                        'phone': response.data.phone
                    };

                    opd_service.update_appointment_onregister(Update_patient_info).then(function (response) {
                        $scope.register = false;
                        $scope.processing = false;
                    }).catch(function (error) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to Update Appointment.',
                            dismissButton: true
                        });
                    });
                }

                ngToast.create({
                    className: 'success',
                    content: 'Patient Added Successfully',
                    dismissButton: true
                });

            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add patient.',
                    dismissButton: true
                });
            });
        };

        $scope.print_receipt = function(form) {
            if ($scope.walkinpatient.name === undefined || $scope.walkinpatient.mr_number === undefined || $scope.walkinpatient.phone === undefined) {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter patient details.',
                    dismissButton: true
                });
            } else if ($scope.searchDoctor === '') {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter doctor name.',
                    dismissButton: true
                });
            } else {
                var appointment = {
                    'patient_name': $scope.walkinpatient.name,
                    'doctor_name': $scope.searchDoctor,
                    'doctor_id': $scope.searchDoctorID,
                    'phone': $scope.walkinpatient.phone,
                    'mr_number': $scope.walkinpatient.mr_number,
					'mr_prefix': $scope.walkinpatient.mr_prefix,
                    'appointment_date': getDateFormat(new Date()),
                    'appointment_status': 'Arrived'
                }
                appointment.time = '';
                $scope.processing = true;
                opd_service.create_appointment_walkin(appointment).then(function (response) {
                    appointment.number_in_queue = response.data.number_in_queue;
                    opd_service.search_doctor_by_id(appointment.doctor_id).then(function(response) {

						hospitalAdmin_service.get_facilities_byname_category('Hospital-OPD-Consultation',response.data.name).then(function(service) {

                        $scope.processing = false;
                        $scope.print_appointment = angular.copy(appointment);
                        $scope.print_receipt_data = [{
                            'description' : 'Consultancy',
                            'fee' : response.data.consultation_fee
                        }];

                        var billingDetails = {
                            'patientInfo' : {
                                'mr_number' : $scope.print_appointment.mr_number,
                                'name' : $scope.print_appointment.patient_name,
                                'phone' : $scope.print_appointment.phone
                            },
                            'referedDoctor' : response.data,
                            'doctorCut' : response.data.consultation_fee*response.data.percentage_fee/100,
                            'servicesInfo' : [{
                                'description' : response.data.name,
                                'fee' : response.data.consultation_fee,
								'category' : "Hospital-OPD-Consultation",
								'service_id' : service.data._id,
                                'discount' : 0,
                                'quantity' : 1
                            }],
                            'grandTotal' : response.data.consultation_fee,
                            'recievedCash' : 0,
                            'discount' : 0,
                            'paymentMethod' : 'cash',
                        };

                       opd_service.get_patient($scope.print_appointment).then(function(patient){
						if(patient.data[0].panel_id && patient.data[0].panel_id !=='None'){
							opd_service.get_service_discount(patient.data[0].panel_id,billingDetails.servicesInfo[0].category,billingDetails.servicesInfo[0].description).then(function (response) {
								if(response.data.discountType == 'amount') {
									billingDetails.servicesInfo[0].discount = response.data.discount;
								} else if(response.data.discount) {
									billingDetails.servicesInfo[0].discount = Math.round(response.data.discount/100*response.data.price);
								}
								billingDetails.servicesInfo[0].fee = response.data.price;
								billingDetails.grandTotal = billingDetails.servicesInfo[0].fee-billingDetails.servicesInfo[0].discount;
								opd_service.save_bill(billingDetails).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Fee added to bill.',
                            dismissButton: true
                        });
                        $location.path('/reception/generateBill/'+response.data.patientInfo.mr_number);
                        $scope.switchTabs('generateBill');

                    }).catch(function(error){
                        ngToast.create({
                            className: 'danger',
                            content: 'Fee not added in bill.',
                            dismissButton: true
                        });
                    });
							}).catch(function(response){
								//errrr panel discount not found
							});
						} else {
							opd_service.save_bill(billingDetails).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Fee added to bill.',
                            dismissButton: true
                        });
                        $location.path('/reception/generateBill/'+response.data.patientInfo.mr_number);
                        $scope.switchTabs('generateBill');

                    }).catch(function(error){
                        ngToast.create({
                            className: 'danger',
                            content: 'Fee not added in bill.',
                            dismissButton: true
                        });
                    });
						}
					});

                        $scope.print_total = response.data.consultation_fee;
                        $scope.walkinpatient = {};
                        $scope.searchDoctor = '';
                        $scope.reset(form);
						}).catch(function (error) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to print receipt.',
                            dismissButton: true
                        });
                    });
                    }).catch(function (error) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to print receipt.',
                            dismissButton: true
                        });
                    });
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to print receipt.',
                        dismissButton: true
                    });
                });
            }
        };

        $scope.selectDoctorAppointment = function(doctor) {
            $scope.searchDoctor = doctor.name;
            $scope.isDoctorSelected = true;
            $scope.searchDoctorID = doctor._id;
            $scope.showDoctorsDropDown = false;
            var today = getDateFormat(new Date());
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(doctor._id, today).then(function (response) {
                if (response.data === 'Not available on this day') {
                    ngToast.create({
                        className: 'warning',
                        content: 'Doctor not available today.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                    $scope.searchDoctor = '';
                } else {
                    opd_service.get_doctor_date_appointments(doctor._id, today).then(function (response) {
                        $scope.processing = false;
                        $scope.doctorAppointments = response.data;
                    }).catch(function (error) {
                        $scope.processing = false;
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve appointments.',
                            dismissButton: true
                        });
                    });
                }
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.sortBy('time');
        $scope.sortBy('time');

        $scope.register_new_patient = function(name, phone_number, appointment_id) {
			$scope.patient = {};
            $scope.patient = angular.copy(patient);
            $scope.patient.gender = 'male';
			$scope.patient.name = name;
			$scope.patient.phone = phone_number;
            $scope.appointment_id = appointment_id;
            $scope.register = true;
            $scope.showMRNumber = false;
        };

        $scope.calculateDOB = function(){
            var todayDate = new Date();
            $scope.patient.dob = new Date((todayDate.getFullYear()-$scope.patient.age)+'-'+(todayDate.getMonth()+1).toString()+'-'+todayDate.getDate());
        };

        $scope.checkDOB = function(dob){
            var todayDate = new Date();
            if(dob>todayDate){
                $scope.patient.dob = undefined;
                ngToast.create({
                    className: 'danger',
                    content: 'Enter Correct Date of birth.',
                    dismissButton: true
                });
            }
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.UpdateItem = false;
            $scope.showMRNumber = false;
            $scope.patient = angular.copy(patient);
            $scope.showAppointmentDelete = false;
            $scope.showAppointmentSave = false;
            $scope.isDoctorSelected = false;
        };

        $scope.print_receipt_appointment = function(appointment) {
            $scope.processing = true;
            opd_service.update_appointment_status({'appointment_id':appointment._id,'status':'Arrived'}).then(function (response) {
							console.log(response);
                appointment.appointment_status = 'Arrived';
                $scope.print_appointment = angular.copy(appointment);
                opd_service.search_doctor_by_id(appointment.doctor_id).then(function(response){
					hospitalAdmin_service.get_facilities_byname_category('Hospital-OPD-Consultation',response.data.name).then(function(service) {


                    $scope.processing = false;
                    $scope.print_receipt_data = [{
                        'description' : 'Consultancy',
                        'fee' : response.data.consultation_fee
                    }];

                    var billingDetails = {
                        'patientInfo' : {
                            'mr_number' : $scope.print_appointment.mr_number,
                            'name' : $scope.print_appointment.patient_name,
                            'phone' : $scope.print_appointment.phone
                        },
                        'referedDoctor' : response.data,
                        'doctorCut' : response.data.consultation_fee*response.data.percentage_fee/100,
                        'servicesInfo' : [{
                                'description' : response.data.name,
                                'fee' : response.data.consultation_fee,
								'category' : "Hospital-OPD-Consultation",
								'service_id' : service.data._id,
                                'discount' : 0,
                                'quantity' : 1
                        }],
                        'grandTotal' : response.data.consultation_fee,
                        'recievedCash' : 0,
                        'discount' : 0,
                        'paymentMethod' : 'cash',
                    };

					opd_service.get_patient($scope.print_appointment).then(function(patient){
						if(patient.data[0].panel_id && patient.data[0].panel_id !=='None'){
							opd_service.get_service_discount(patient.data[0].panel_id,billingDetails.servicesInfo[0].category,billingDetails.servicesInfo[0].description).then(function (response) {
								if(response.data.discountType == 'amount') {
									billingDetails.servicesInfo[0].discount = response.data.discount;
								} else if(response.data.discount) {
									billingDetails.servicesInfo[0].discount = Math.round(response.data.discount/100*response.data.price);
								}
								billingDetails.servicesInfo[0].fee = response.data.price;
								billingDetails.grandTotal = billingDetails.servicesInfo[0].fee-billingDetails.servicesInfo[0].discount;
								opd_service.save_bill(billingDetails).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Fee added to bill.',
                            dismissButton: true
                        });
                        $location.path('/reception/generateBill/'+response.data.patientInfo.mr_number);
                        $scope.switchTabs('generateBill');

                    }).catch(function(error){
                        ngToast.create({
                            className: 'danger',
                            content: 'Fee not added in bill.',
                            dismissButton: true
                        });
                    });
							}).catch(function(response){
								//errrr panel discount not found
							});
						} else {
							opd_service.save_bill(billingDetails).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Fee added to bill.',
                            dismissButton: true
                        });
                        $location.path('/reception/generateBill/'+response.data.patientInfo.mr_number);
                        $scope.switchTabs('generateBill');

                    }).catch(function(error){
                        ngToast.create({
                            className: 'danger',
                            content: 'Fee not added in bill.',
                            dismissButton: true
                        });
                    });
						}
					});
				/*	*/


                    ngToast.create({
                        className: 'success',
                        content: 'Patient Arrival Status Updated',
                        dismissButton: true
                    });
				})
                .catch(function(error){
                    ngToast.create({
                        className: 'danger',
                        content: 'Unable to set status',
                        dismissButton: true
                    });
                });
                })
                .catch(function(error){
                    ngToast.create({
                        className: 'danger',
                        content: 'Unable to set status',
                        dismissButton: true
                    });
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to set status.',
                    dismissButton: true
                });
            });
        };

		$scope.reset();
    }
]);
