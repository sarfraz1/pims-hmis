'use strict';

angular.module('opd').controller('CreateAppointmentController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'opd_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, opd_service, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showAppointments = false;
        $scope.showPatientAppointments = false;
        $scope.showMRNumber = false;
        $scope.showDoctorsDropDown = false;
        $scope.updateItem = false;
        $scope.showAppointmentSave = false;
        $scope.showDoctorListDropdown = false;
        $scope.showAppointmentDelete = false;
        $scope.searchPatientKeyword = '';
        $scope.searchDoctor = '';
        $scope.searchDoctorID = '';
        $scope.appointmentDate = new Date();
        $scope.searchDate = new Date();
        $scope.viewDoctor = '';
        $scope.isDoctorSelected = false;
        $scope.processing = false;
        var doctor_id = '';
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

        var patient = {
            'name': '',
            'phone' : null,
            'gender': undefined,
            'dob': new Date(),
            'address' : '',
            'cnic': '',
            'mr_number': '',
            'panel_id': 'None'
        };

        var appointment = {
            'patientName': '',
            'patientPhoneNumber': null,
            'doctorName': '',
            'date': new Date()
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.panels = [];

        var getPanels = function() {
            opd_service.get_panels().then(function (response) {
                $scope.panels = response.data;
                $scope.panels.unshift({description: 'None'});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panels.',
                    dismissButton: true
                });
            });
        };

        var getSpecialities = function() {
            opd_service.get_specialities().then(function (response) {
                $scope.specialities = response.data;
                $scope.specialities.unshift({description: 'All'});
                for (var i = 0; i < $scope.specialities.length; i++) {
                    if ($scope.specialities[i].description === 'Medical Officer')
                        $scope.specialities.splice(i, 1);
                }
                $scope.speciality = 'All';
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Specialities.',
                    dismissButton: true
                });
            });
        };

        getSpecialities();
        getPanels();

        $scope.filterSpeciality = function(speciality) {
            return function(doctor) {
                if (speciality === 'All')
                    return doctor;
                else
                    return doctor.speciality === speciality;
            };
        };

        // callback function
        $scope.callbackappointment = function(selected_patient) {
            if ($scope.showAppointmentDelete) {
                ngToast.create({
                    className: 'warning',
                    content: 'Cannot select patient when deleting appointment.',
                    dismissButton: true
                });
                $scope.showPatientAppointments = false;
            } else {
                var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
                $scope.appointment.patientName = $scope.patientData[patientIndex].name;
                $scope.appointment.patientPhoneNumber = $scope.patientData[patientIndex].phone;
                $scope.appointment.patientMRNumber = $scope.patientData[patientIndex].mr_number;
				 $scope.appointment.patientMRPrefix = $scope.patientData[patientIndex].mr_prefix;
                $scope.showPatientAppointments = false;
                $scope.showMRNumber = true;
            }
        };

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
        	if ($scope.searchPatientKeyword.length > 0) {
                $scope.processing = true;
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    } else if (searchType === 'walkin') {
                        $scope.showWalkIn = true;
                    }
                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPatient + $scope.prefixPatient));
                    $timeout(function() {setFocus[0].focus()});
                    $scope.searchPatientKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.UpdateItem = false;
            $scope.showMRNumber = false;
            $scope.appointment = angular.copy(appointment);
            $scope.searchDoctor = '';
            $scope.timeSlot = '';
            $scope.showAppointments = false;
            $scope.appointmentDate = new Date();
            $scope.showAppointmentDelete = false;
            $scope.showAppointmentSave = false;
            $scope.isDoctorSelected = false;
			$scope.walkinappointment = false;
        };

        // manage changes on search doctor
        var changeSearch = function(check) {
            if (check === 'appointment') {
                $scope.timeSlot = '';
            } else if (check === 'view') {
                $scope.processing = true;
                opd_service.get_doctor_date_appointments('all', getDateFormat(new Date())).then(function (response) {
                    $scope.doctorAppointments = response.data;
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve appointments.',
                        dismissButton: true
                    });
                });
            } else if (check === 'walkin') {
                $scope.isDoctorSelected = false;
            }
        };

        // manage appointments functionality
        $scope.searched_doctors = [];
        $scope.noAppointments = false;

        var dateConverter = function(dateinput){
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch (error) {
                return dateinput;
            }
        };

        var tempDoctor = {};

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        $scope.showDoctorAppointments = function(selectedDoctorSlots) {
            //console.log(selectedDoctorSlots);
            tempDoctor = selectedDoctorSlots;
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(selectedDoctorSlots._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = selectedDoctorSlots.name;
                $scope.selectedDoctor = selectedDoctorSlots;
                $scope.showDoctorsDropDown = false;
            });
        };

        $scope.changeSpeciality = function() {
            $scope.searchDoctor = '';
            $scope.showAppointments = false;
            $scope.timeSlot = '';
            $scope.isDoctorSelected = false;
            $scope.showDoctorsDropDown = false;
        };


        $scope.showDoctorAppointmentsByDate = function() {
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(tempDoctor._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = tempDoctor.name;
                $scope.selectedDoctor = tempDoctor;
                $scope.showDoctorsDropDown = false;
            });
        };

        var removeMedicalOfficer = function() {
            for (var i = 0; i < $scope.searched_doctors.length; i++) {
                if ($scope.searched_doctors[i].speciality === 'Medical Officer') {
                    $scope.searched_doctors.splice(i, 1);
                    i--;
                }
            }
        };

        $scope.searchDoctors = function(change, check) {
            doctor_id = null;
            if (check)
                changeSearch(check);
            if (change === 'change')
                $scope.showAppointments = false;
            $scope.searched_doctors = [];
            $scope.processing = true;
            if ($scope.searchDoctor.length > 0) {
                opd_service.search_doctor_by_name($scope.searchDoctor).then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            } else {
                opd_service.list_all_doctors().then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            }
            if ($scope.searchDoctor === '') {
                $scope.showDoctorsDropDown = false;
            }
        };

        $scope.save_appointment = function(form) {
            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            if (!$scope.noAppointments) {
                var appointment = {
                    'patient_name': $scope.appointment.patientName,
                    'doctor_name': $scope.selectedDoctor.name,
                    'doctor_id': $scope.selectedDoctor._id,
                    'phone': $scope.appointment.patientPhoneNumber,
                    'appointment_date': getDateFormat($scope.appointmentDate)
                }
                if ($scope.showMRNumber) {
                    appointment.mr_number = $scope.appointment.patientMRNumber;
                    appointment.mr_prefix = $scope.appointment.patientMRPrefix;
                }
				if(!$scope.walkinappointment) {
					appointment.time = $scope.timeSlot;
				}
                if ($scope.appointmentDate < yesterday) {
                    ngToast.create({
                        className: 'warning',
                        content: 'Please select correct date for appointment.',
                        dismissButton: true
                    });
                } else {
                    $scope.processing = true;
					if(!$scope.walkinappointment) {
						opd_service.create_appointment(appointment).then(function (response) {
							$scope.processing = false;
							ngToast.create({
								className: 'success',
								content: 'Appointment Saved Successfully',
								dismissButton: true
							});
						}).catch(function (error) {
							$scope.processing = false;
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to save appointment.',
								dismissButton: true
							});
						});
					} else {
						opd_service.create_appointment_walkin(appointment).then(function (response) {
							$scope.processing = false;
							ngToast.create({
								className: 'success',
								content: 'Appointment Saved Successfully',
								dismissButton: true
							});
						}).catch(function (error) {
							$scope.processing = false;
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to save appointment.',
								dismissButton: true
							});
						});						
					}
                    $scope.reset(form);
                }
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please select correct date for appointment.',
                    dismissButton: true
                }); 
            }
        };

        $scope.delete_appointment = function(form) {
            var searchDoctor = $scope.searchDoctor;
            // dummy logic start
            delete $scope.doctorSlots[$scope.doctorIndex].slots[$scope.timeIndex].patientName;
            delete $scope.doctorSlots[$scope.doctorIndex].slots[$scope.timeIndex].patientNumber;
            delete $scope.doctorSlots[$scope.doctorIndex].slots[$scope.timeIndex].patientMRNumber;
            $scope.doctorSlots[$scope.doctorIndex].slots[$scope.timeIndex].status = 'available';
            // dummy logic end
            ngToast.create({
                className: 'success',
                content: 'Appointment Deleted Successfully',
                dismissButton: true
            });
            $scope.reset(form);
            $scope.searchDoctor = searchDoctor;
            $scope.showAppointments = true;
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };
		
        $scope.get_appointment_walkin = function() {
			$scope.showAppointmentDelete = false;
			$scope.showAppointmentSave = true;
			$scope.walkinappointment = true;
			$scope.waitingslot = 'Waiting';
        };

        $scope.get_appointment = function(timeSlot) {
			$scope.walkinappointment = false;
            var newDate = new Date();
            var curTime = {
                hour : '',
                minutes : '',
                timeStr : ''
            };
            curTime.hour = newDate.getHours();
            curTime.minutes = newDate.getMinutes().toString();
            
            if(curTime.minutes.length===1){
                curTime.minutes = '0'+ curTime.minutes;
            }
            curTime.timeStr = curTime.hour+curTime.minutes;
            
            var slotTimeArr = timeSlot.slot.split(" ");
            var temp = slotTimeArr[0].split(':');
            
            temp[0] = Number(temp[0])
            
            if(slotTimeArr[1]==='PM'){
                if(temp[0]!==12)
                    temp[0]+=12;
            }

            var slotTime = {
                hour : temp[0],
                minutes : temp[1],
                timeStr : temp[0]+temp[1]
            };
            
            if(Number(slotTime.timeStr)<Number(curTime.timeStr) && $scope.appointmentDate<=newDate){
                ngToast.create({
                    className: 'danger',
                    content: 'Selected slot has passed.',
                    dismissButton: true
                });
            }
            else if (timeSlot.status !== 'Booked') {
                $scope.timeSlot = timeSlot.slot;
                $scope.showAppointmentDelete = false;
                $scope.showAppointmentSave = true;
            }
        };

        $scope.reset_appointment = function(form) {
            $scope.showAppointments = false;
            $scope.reset(form);
        };

		$scope.reset();
    }
]);