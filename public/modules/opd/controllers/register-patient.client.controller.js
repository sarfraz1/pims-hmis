'use strict';

angular.module('opd').controller('registerPatientController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'opd_service','print_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, opd_service, print_service, $anchorScroll) {
        $scope.authentication = Authentication;
		var prefix_panels = true;
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showPatients = false;
        $scope.showAppointments = false;
        $scope.showPatientAppointments = false;
        $scope.showMRNumber = false;
        $scope.updateItem = false;
        $scope.searchPatientKeyword = '';
        $scope.processing = false;
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

        var patient = {
            'name': '',
            'phone' : null,
            'gender': 'male',
            'dob': undefined,
            'address' : '',
            'cnic': '',
            'mr_number': '',
            'panel_id': 'None'
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.panels = [];

        var getPanels = function() {
            opd_service.get_panels().then(function (response) {
                $scope.panels = response.data;
                $scope.panels.unshift({description: 'None'});
                $scope.panel = 'None';
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Panels.',
                    dismissButton: true
                });
            });
        };

        $scope.areas = [];

        var getAreas = function() {
            opd_service.get_areas().then(function (response) {
                $scope.areas = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Areas.',
                    dismissButton: true
                });
            });
        };

        var calculateAge = function(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        getPanels();
		getAreas();

        // callback functions
        $scope.callbackpatient = function(selected_patient) {
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
            $scope.patientData[patientIndex].dob = new Date($scope.patientData[patientIndex].dob);

            $scope.patient = $scope.patientData[patientIndex];
            $scope.patient.age = calculateAge($scope.patient.dob);
            $scope.UpdateItem = true;
            $scope.showPatients = false;
            $scope.showMRNumber = true;
            $scope.searchPatientKeyword = '';

			if($scope.patient.panel_id !== 'None') {
				console.log('show name');
				for(var i=0;i<$scope.panels.length;i++) {
					if($scope.panels[i].description == $scope.patient.panel_id) {
						$scope.paneldata = $scope.panels[i];
					}
				}
			}
        };

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
        	if ($scope.searchPatientKeyword.length > 0) {
                $scope.processing = true;
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
										console.log($scope.patients);
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    } else if (searchType === 'walkin') {
                        $scope.showWalkIn = true;
                    }
                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPatient + $scope.prefixPatient));
                    $timeout(function() {setFocus[0].focus()});
                    $scope.searchPatientKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };

		$scope.panelChanged = function() {
			var panel = JSON.parse($scope.paneldata);
			$scope.patient.panel_id = panel.description;

			if(panel.mrNumberPrefix) {
				$scope.patient.mr_prefix = panel.mrNumberPrefix;
			}
		};

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.UpdateItem = false;
            $scope.showMRNumber = false;
            $scope.patient = angular.copy(patient);
        };

        // manage patients functionaltiy
        $scope.submit_patient = function(patient, form) {
            $scope.processing = true;
			if(prefix_panels && !$scope.paneldata) {
				console.log('Panel Data');
				$scope.patient.mr_prefix = 'PRT';
			}
            opd_service.create_patient(patient).then(function (response) {

                var billingDetails = {
                    'created' : response.data.created,
                    'patientInfo' : {
                        'mr_number' : response.data.mr_number,
                        'name' : response.data.name,
                        'phone' : response.data.phone,
						'panel_id': response.data.panel_id=='None'?undefined:response.data.panel_id,
						'panelCardNumber': response.data.panelCardNumber
                    },
                    'servicesInfo' : [{
                        'service_id' : 'registration',
                        'description' : 'Registration',
                        'category' : 'Hospital-Registration',
                        'fee' : 200,
                        'discount' : 0,
                        'quantity' : 1
                    }],
					//'servicesInfo' : [],
                    'grandTotal' : 200,
                    'recievedCash' : 0,
                    'discount' : 0,
                    'paymentMethod' : 'cash',
                };

                opd_service.save_bill(billingDetails).then(function (response) {

                });
                $scope.processing = false;
                $scope.reset(form);

                response.data.dob = new Date(response.data.dob);
                $scope.patient = response.data;
				$scope.patient.age = calculateAge($scope.patient.dob);
                $scope.UpdateItem = true;
                $scope.showMRNumber = true;

                ngToast.create({
                    className: 'success',
                    content: 'Patient Added Successfully',
                    dismissButton: true
                });

            }).catch(function (error) {
                $scope.processing = false;
                if(error.data.code===11000){
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Patient already registered.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add patient.',
                        dismissButton: true
                    });
                }
            });
        };

		$scope.generate_bill = function(patient) {
            if($scope.authentication.user.roles === "ipd receptionist" || $scope.authentication.user.roles === "ipd admin"){
                $location.path('/ipdreception/ipdgenerateBill/'+patient.mr_number);
                $scope.switchTabs('generateBill');
            } else {
    			$location.path('/reception/generateBill/'+patient.mr_number);
                $scope.switchTabs('generateBill');
            }
		};

		$scope.print_mrno = function(patient) {
			var dt = moment(new Date());
			var obj = {
				'name': patient.name,
				'mr_number': patient.mr_prefix+'-'+patient.mr_number,
				'address': patient.address,
				'age' : patient.age,
				'phone': patient.phone,
				'date': dt.format('DD-MM-YYYY'),
				'time': dt.format('h:mm a')
			};
			print_service.print('/modules/opd/views/patient-print.client.view.html',obj,
            function(){
            });
		};

		$scope.print_card = function(patient) {
			var dt = moment(new Date());
			var obj = {
				'name': patient.name,
                'mr_number': patient.mr_number,
                'mr_number': patient.mr_prefix+'-'+patient.mr_number,
				'panel' : patient.panel_id,
				'phone': patient.phone,
				'date': dt.format('DD-MM-YYYY'),
				'options': {
					width: 2,
					height: 60,
					displayValue: false,
					font: 'monospace',
					textAlign: 'left',
					fontSize: 14,
				}
			};
			print_service.print('/modules/opd/views/card-print.client.view.html',obj,
            function(){
            });
		};

        $scope.update_patient = function(patient, form) {
            $scope.processing = true;
            opd_service.update_patient(patient).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Patient Updated Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                $scope.processing = false;
                response.data.dob = new Date(response.data.dob);
                $scope.patient = response.data;
                $scope.UpdateItem = true;
                $scope.showMRNumber = true;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update patient.',
                    dismissButton: true
                });
            });
        };

        $scope.reset_patient = function(form) {
            $scope.reset(form);
        };

        $scope.calculateDOB = function(){
            var todayDate = new Date();
			if($scope.age_type == 'Years') {
				$scope.patient.dob = new Date((todayDate.getFullYear()-$scope.patient.age)+'-'+(todayDate.getMonth()+1).toString()+'-'+todayDate.getDate());
			} else if ($scope.age_type == 'Months') {
				$scope.patient.dob = (moment().subtract($scope.patient.age, 'M')).toDate();
			} else if ($scope.age_type == 'Days') {
				$scope.patient.dob = (moment().subtract($scope.patient.age, 'd')).toDate();
			}
        };

        $scope.checkDOB = function(dob){
            var todayDate = new Date();
            if(dob>todayDate){
                $scope.patient.dob = undefined;
                ngToast.create({
                    className: 'danger',
                    content: 'Enter Correct Date of birth.',
                    dismissButton: true
                });
            }
        };

        $scope.reset();
    }
]);
