'use strict';

angular.module('opd').controller('ArrivalController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','opd_service','orderByFilter',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll,opd_service,orderBy) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.searchPatientKeyword ='';
        $scope.showPatients = false;
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        var getAppointments = function(){
            opd_service.get_all_appointments().then(function (response) {
                $scope.doctorAppointments = response.data;
                console.log($scope.doctorAppointments);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        $scope.callbackpatient = function(selected_patient) {
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
            $scope.printAppointment.phoneNumber = $scope.patientData[patientIndex].phone;
            $scope.printAppointment.patientName = $scope.patientData[patientIndex].name;
            $scope.patient = $scope.patientData[patientIndex];
            $scope.showPatients = false;
            $scope.showMRNumber = true;
            $scope.searchPatientKeyword = '';
        };



        $scope.panels = [{'panel_id': '12345678', 'panel_name': 'Company1'}, {'panel_id': '12345679', 'panel_name': 'Company2'}];

        // retrieve data for patient searching
        $scope.getPatients = function(searchType) {
            if ($scope.searchPatientKeyword.length > 0) {
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    }
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Warning: Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };


        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.appointments = orderBy($scope.appointments, $scope.propertyName, $scope.reverse);
        };

        $scope.doctorSearchTypeahead = function(val){
            return opd_service.search_doctor(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Inventory Items.',
                    dismissButton: true
                });
            });
        };

        $scope.registerClick = function(appoin){
            if(appoin!==undefined) {
                $scope.patient = {
                    'name' : appoin.patientName,
                    'phoneNumber' : appoin.phoneNumber
                };
            }
            $scope.register = true;
        };

        $scope.submit_patient = function(patient, form) {
            opd_service.create_patient(patient).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Patient Added Successfully',
                    dismissButton: true
                });
                $scope.showPatients = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add patient.',
                    dismissButton: true
                });
            });
        };

        var patient = {
            'name': '',
            'phone' : null,
            'address' : '',
            'cnic': '',
            'mr_number': '',
            'panel_id': ''
        };

        var printAppointment = {
            'patientName': '',
            'phoneNumber': null,
            'doctorName': ''
        };

        $scope.submit_patient = function(patient, form) {
            opd_service.create_patient(patient).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Patient Added Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add patient.',
                    dismissButton: true
                });
            });
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            getAppointments();
            $scope.UpdateItem = false;
            $scope.showMRNumber = false;
            $scope.patient = angular.copy(patient);
            $scope.printAppointment = angular.copy(printAppointment);
        };

        $scope.init = function(){
            $scope.panels = ['company1', 'company2', 'company3'];
            $scope.register = false;
            $scope.appointment = true;
            $scope.walkin = false;
            $scope.appointments = [{
                'patientName' : 'Affan',
                'phoneNumber' : '0341-5656378',
                'MRNo' : '5635373',
                'arrived' : false,
                'registered' : false
            },{
                'patientName' : 'Jamal',
                'phoneNumber' : '0341-5236378',
                'MRNo' : '765763',
                'arrived' : true,
                'registered' : false
            },{
                'patientName' : 'Abdullah',
                'phoneNumber' : '0341-9827827',
                'MRNo' : '674573',
                'arrived' : true,
                'registered' : true
            }];
        };
        $scope.init();
        $scope.reset();
    }
]);