'use strict';

//Setting up route
angular.module('opd').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('reception', {
			url: '/reception',
			controller: 'ReceptionController',
			templateUrl: 'modules/opd/views/reception.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		})
		.state('reception.registerPatient', {
			url: '/registerPatient',
			controller: 'registerPatientController',
			templateUrl: 'modules/opd/views/register-patient.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		})
		.state('reception.createAppointment', {
			url: '/createAppointment',
			controller: 'CreateAppointmentController',
			templateUrl: 'modules/opd/views/create-appointment.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		})
		.state('reception.viewAppointments', {
			url: '/viewAppointments',
			controller: 'ViewAppointmentsController',
			templateUrl: 'modules/opd/views/view-appointments.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		})
		.state('reception.patientArrival', {
			url: '/patientArrival',
			controller: 'PatientArrivalController',
			templateUrl: 'modules/opd/views/patient-arrival.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		})
		.state('reception.generateBill', {
			url: '/generateBill/:MRN',
			templateUrl: 'modules/opd/views/generate-bill.client.view.html',
			access: ['opd receptionist', 'opd admin', 'super admin']
		});
	}
]);