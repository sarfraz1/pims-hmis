'use strict';

angular.module('opd').factory('opd_service', ['$http', 'config_service',
	function($http, config_service) {
		
		var srvr_address = config_service.serverAddress;

		this.search_doctor = function(keyword) {
			return $http.get(srvr_address+'/getDoctors/'+keyword);
		};

		this.get_specialities = function() {
			return $http.get(srvr_address+'/specialities');
		};
		
		/* 
		 * Methods for OPD Reception - Create Patient
		 */
		this.create_patient = function(patient) {
			return $http.post(srvr_address+'/patient', patient);
		};

		this.get_patient = function(patient) {
			return $http.get(srvr_address+'/patient/' + patient.mr_number);
		};

		this.update_patient = function(patient) {
			return $http.put(srvr_address+'/patient/' + patient.mr_number, patient);
		};

		this.search_patients = function(keyword) {
			return $http.get(srvr_address+'/getPatient/' + keyword);
		};

		this.get_panels = function(keyword) {
			return $http.get(srvr_address+'/panel');
		};

		/*
		 * Methods for OPD Reception - Manage Appointments
		 */
		this.search_doctor_by_name = function(keyword) {
			return $http.get(srvr_address+'/getDoctor/' + keyword);
		};

		this.search_doctor_by_id = function(keyword) {
	 		return $http.get(srvr_address+'/doctor/' + keyword);
		};

		this.list_all_doctors = function () {
			return $http.get(srvr_address+'/doctor');
		};

		this.get_appointment_by_doctor = function(doctorid, date) {
			return $http.get(srvr_address+'/getDoctorslots/' + doctorid + '/' + date);
		};

	 	this.create_appointment = function(appointment) {
			return $http.post(srvr_address+'/appointment', appointment);
		};
		
		this.update_appointment_onregister = function(appointment) {
			return $http.post(srvr_address+'/updateappointmentonregister', appointment);
		};
		
		this.update_vitals_status = function(appt_id) {
			return $http.post(srvr_address+'/updateappointmentvitals', {appointment_id: appt_id});
		};
		
		this.update_appointment_status = function(appointment) {
			return $http.post(srvr_address+'/updateappoinmentstatus', appointment);
		};

		this.create_appointment_walkin = function(appointment) {
			return $http.post(srvr_address+'/createwalkinappointment', appointment);
		};

		this.delete_appointment = function(appointment) {
			return $http.delete(srvr_address+'/appointment/' + appointment);
		};

		/**
		 * Methods for OPD Reception - View Appointments
		 */
		this.get_all_appointments = function() {
			return $http.get(srvr_address+'/appointment');
		};

		this.get_doctor_date_appointments = function(doctorid, date) {
			return $http.get(srvr_address+'/viewAppointments/' + doctorid + '/' + date);
		};

		this.get_service_discount = function(panelId,categoryId,serviceDesc){
			return $http.get(srvr_address+'/getDiscount/'+panelId+'/'+categoryId+'/'+serviceDesc);
		};
		
		this.save_lab_order = function(labOrder) {
			return $http.post(srvr_address+'/lab-order',labOrder);
		};
		
		this.save_radiology_order = function(radiologyOrder) {
			return $http.post(srvr_address+'/radiology-order',radiologyOrder);
		};

		/* 
		 * Methods for Billing
		 */
		this.save_bill = function(bill) {
			return $http.post(srvr_address+'/bill',bill);
		};

		this.get_bill = function(patientMrn) {
			return $http.get(srvr_address+'/patientBill/'+patientMrn);
		};

		this.get_bill_RP = function(patientMrn) {
			return $http.get(srvr_address+'/patientBillRP/'+patientMrn);
		};

		this.get_history_bills = function(patientMrn) {
			return $http.get(srvr_address+'/patientHistory/'+patientMrn);
		};

		this.save_refund = function(billNum,RefundInfo) {
			return $http.post(srvr_address+'/refundBill/'+billNum,RefundInfo);
		};
		
		this.get_areas = function() {
			return $http.get(srvr_address+'/areas');
		};

		return this;
	}
]);