'use strict';

angular.module('hospitals').factory('hospital_service', ['$http', 'config_service',
	function($http, config_service) {
		/**
		 * Methods for hospitals
		 */
		 
		var srvr_address = config_service.serverAddress;
		this.create_hospital = function(item) {
			return $http.post(srvr_address+'/hospital', item);
		};

		this.list_hospitals = function() {
			return $http.get(srvr_address+'/hospital');
		};

		this.delete_hospital = function(item) {
			return $http.delete(srvr_address+'/hospital/' + item._id);
		};

		this.update_hospital = function(item) {
			return $http.put(srvr_address+'/hospital/' + item._id, item);
		};

		return this;
	}
]);