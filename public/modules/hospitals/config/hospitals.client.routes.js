'use strict';

//Setting up route
angular.module('hospitals').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('hospitals', {
			url: '/hospitals',
			templateUrl: 'modules/hospitals/views/hospitals.client.view.html',
			access: ['super admin']
		});
	}
]);