'use strict';

angular.module('hospitals').controller('HospitalsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'hospital_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, ngToast, hospital_service, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

		var hospital = {
			'name': 'Hospital Name',
			'phone': [{
				'number': 512222222
			}],
			'fax': [{
				'number': 512222223
			}],
			'addressLineOne': 'Street 1, H-10',
			'addressLineTwo': 'Islamabad'
		};

		hospital_service.list_hospitals().then(function (response) {
			if (response.data.length === 0) {
				$scope.UpdateItem = false;
				$scope.hospital = hospital;
				$scope.fax = hospital.fax;
				$scope.phone = hospital.phone;
			} else {
				$scope.UpdateItem = true;
				$scope.hospital = response.data[0];
				$scope.fax = response.data[0].fax;
				for (var i = 0; i < response.data[0].fax.length; i++) {
					$scope.fax[i].number = parseFloat(response.data[0].fax[i].number);
				}
				$scope.phone = response.data[0].phone;
				for (i = 0; i < response.data[0].phone.length; i++) {
					$scope.phone[i].number = parseFloat(response.data[0].phone[i].number);
				}
			}
		});

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
			hospital_service.list_hospitals().then(function (response) {
				$scope.UpdateItem = true;
				$scope.hospital = response.data[0];
				$scope.fax = response.data[0].fax;
				for (var i = 0; i < response.data[0].fax.length; i++) {
					$scope.fax[i].number = parseFloat(response.data[0].fax[i].number);
				}
				$scope.phone = response.data[0].phone;
				for (i = 0; i < response.data[0].phone.length; i++) {
					$scope.phone[i].number = parseFloat(response.data[0].phone[i].number);
				}
			});
		};

        $scope.submit_hospital = function(hospital, form) {
        	hospital.phone = $scope.phone;
        	hospital.fax = $scope.fax;
        	if ($scope.UpdateItem === false) {
	    		hospital_service.create_hospital(hospital).then(function(response) {
	                ngToast.create({
	                    className: 'success',
	                    content: 'Hospital Added Successfully',
	                    dismissButton: true
	                });
	                $scope.reset(form);
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to add Hospital.',
	                    dismissButton: true
	                });
	            });
    		} else {
	        	hospital_service.update_hospital(hospital).then(function(response) {
	                ngToast.create({
	                    className: 'success',
	                    content: 'Hospital Updated Successfully',
	                    dismissButton: true
	                });
	                $scope.reset(form);
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to update Hospital.',
	                    dismissButton: true
	                });
	            });
    		}
        };


        $scope.reset_hospital = function(form) {
        	$scope.reset(form);
        };

        $scope.phone = [{}];

        $scope.addPhone = function() {
        	$scope.phone.unshift({});
        };

        $scope.removePhone = function(index) {
        	$scope.phone.splice(index, 1);
        };

        $scope.fax = [{}];

        $scope.addFax = function() {
        	$scope.fax.unshift({});
        };

        $scope.removeFax = function(index) {
        	$scope.fax.splice(index, 1);
        };
	}
]);