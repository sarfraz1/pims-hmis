'use strict';

//Setting up route
angular.module('doctor').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('doctor', {
			url: '/doctor',
			templateUrl: 'modules/doctor/views/appointments.client.view.html',
			access: ['doctor']
		})
		.state('doctor.appointments', {
			url: '/appointments',
			templateUrl: 'modules/doctor/views/appointments.client.view.html',
			access: ['doctor']
		})
		.state('doctor.ipd', {
			url: '/ipdpatients',
			controller: 'DoctorIPDPatientsController',
			templateUrl: 'modules/doctor/views/doctor-ipdpatients.client.view.html',
			access: ['doctor']
		})
		.state('prescription', {
			url: '/prescription',
			templateUrl: 'modules/doctor/views/prescription.client.view.html',
			access: ['doctor']
		})
		.state('prescriptionipd', {
			url: '/prescription-ipd',
			controller: 'PrescriptionIPDController',
			templateUrl: 'modules/doctor/views/prescription-ipd.client.view.html',
			access: ['doctor']
		})
		.state('prescriptionPref', {
			url: '/prescriptionPref',
			templateUrl: 'modules/doctor/views/prescriptionPrefrences.client.view.html',
			access: ['doctor']
		});
	}
]);