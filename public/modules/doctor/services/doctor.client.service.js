'use strict';

angular.module('doctor').factory('doctor_service', ['$http', 'config_service',
	function($http, config_service) {
		/**
		 * Methods for hospitals
		 */
		 
		var srvr_address = config_service.serverAddress;		 
		var selectedAppointment = {};
		var selectedPatient = {};
		var prescriptionPrinted = false;
		
		this.set_patient = function(patient_info) {
			selectedPatient = patient_info;
		};

		this.get_patient = function() {
			return selectedPatient;
		};
		
		this.set_appointment = function(appoin_info) {
			selectedAppointment = appoin_info;
		};

		this.get_appointment = function() {
			return selectedAppointment;
		};

		this.set_prescription_status = function(status) {
			prescriptionPrinted = status;
		};

		this.get_prescription_status = function() {
			return prescriptionPrinted;
		};

		this.create_hospital = function(item) {
			return $http.post(srvr_address+'/hospital', item);
		};

		this.list_hospitals = function() {
			return $http.get(srvr_address+'/hospital');
		};

		this.delete_hospital = function(item) {
			return $http.delete(srvr_address+'/hospital/' + item._id);
		};

		this.update_hospital = function(item) {
			return $http.put(srvr_address+'/hospital/' + item._id, item);
		};

		this.create_prescription = function(prescription) {
			return $http.post(srvr_address+'/prescription',prescription);
		};

		this.create_prescriptionPref = function(prescriptionPref) {
			return $http.post(srvr_address+'/prescriptionPref',prescriptionPref);
		};

		this.get_prescriptionPref = function(doctorId) {
			return $http.get(srvr_address+'/prescriptionPref/'+doctorId);
		};

		this.get_patient_prescriptions = function(mrNo) {
			return $http.get(srvr_address+'/patientPrescriptions/'+mrNo);
		};

		this.search_medicine = function(keyword) {
			return $http.get(srvr_address+'/brandnames/name/'+keyword);
		};
		
		this.getlabsbyMRN = function(keyword) {
			return $http.get(srvr_address+'/getlabsbyMRN/'+keyword);
		};
		
		this.getimglabsbyMRN = function(keyword) {
			return $http.get(srvr_address+'/labordersbyMRN/'+keyword);
		};
		
		this.getradiologybyMRN = function(keyword) {
			return $http.get(srvr_address+'/getradiologybyMRN/'+keyword);
		};
		
		this.search_services = function(cat, keyword) {
			return $http.get(srvr_address+'/facility-pricingsearch/'+cat+'/'+keyword);
		};

		this.get_old_complaints = function(doctoruser,value) {
			if(value)
				return $http.get(srvr_address+'/prescription/complaint/'+doctoruser+'/'+value);
			else
				return $http.get(srvr_address+'/prescription/complaint/'+doctoruser);
		};
		
		this.get_old_procedures = function(doctoruser,value) {
			if(value)
				return $http.get(srvr_address+'/prescription/procedure/'+doctoruser+'/'+value);
			else
				return $http.get(srvr_address+'/prescription/procedure/'+doctoruser);
		};

		this.get_old_advice = function(doctoruser,value) {
			if(value)
				return $http.get(srvr_address+'/prescription/advice/'+doctoruser+'/'+value);
			else
				return $http.get(srvr_address+'/prescription/advice/'+doctoruser);
		};

		this.get_old_diagnosis = function(doctoruser,value) {
			if(value)
				return $http.get(srvr_address+'/prescription/diagnosis/'+doctoruser+'/'+value);
			else
				return $http.get(srvr_address+'/prescription/diagnosis/'+doctoruser);
		};

		this.get_doctor_byname = function (username) {
			return $http.get(srvr_address+'/doctorbyname/'+username);
		};

		this.get_prescription_byappointmentid = function (apptid) {
			return $http.get(srvr_address+'/patientPrescriptionbyappointmentId/'+apptid);
		};		
		
		this.get_todays_appointments = function(doctor_username,date){
			return $http.get(srvr_address+'/viewappointmentsbyUsername/'+doctor_username+'/'+date);
		};
		

		return this;
	}
]);