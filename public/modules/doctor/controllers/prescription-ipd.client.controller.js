'use strict';

angular.module('doctor').controller('PrescriptionIPDController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'doctor_service', '$anchorScroll', '$timeout', 'inventory_service','staff_service','opd_service', 'ipd_service', 'print_service',
	function($scope, $stateParams, $location, Authentication, ngToast, doctor_service, $anchorScroll, $timeout, inventory_service,staff_service,opd_service, ipd_service, print_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'doctor') {
        	
        } else {
        	$location.path('/signin');
        }
		$scope.allemr = [];
        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };
		var hours = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
		$scope.hours = ['07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','00:00','01:00','02:00','03:00','04:00','05:00','06:00'];
		$scope.rowsgrid = [1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,1,2];
	$scope.options1 = {
            chart: {
                type: 'lineChart',
                height: 600,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 65
                },
                x: function(d){ return d[0]; },
                y: function(d){ return d[1]; },

                color: d3.scale.category10().range(),
                duration: 300,
                useInteractiveGuideline: true,
                clipVoronoi: false,

                xAxis: {
                    axisLabel: 'Time',
					ticks: 24,
                    tickFormat: function(d) {
                        return hours[d];
                    },
                    showMaxMin: false
                },

                yAxis: {
                    axisLabel: 'CARDIOVASCULAR SYSTEM',
					ticks: 30,
					tickFormat: function(d) {
						return d3.format(',.0f')(d);
                    },
					showMaxMin: true
                },
				forceY: [0, 300],
				forceX: [0, 23]
				
				
            }
        };

        $scope.data = [
            {
                key: "PULSE",
                values: [ [ 0 , 76] , 
				[ 1 , 70] , 
				[ 2 , 80] , 
				[ 3 , 82] , 
				[ 4 , 78] , 
				[ 5 , 88] , 
				[ 6 , 72] , 
				[ 7 , 72],
				[ 8 , 72],
				[ 9 , 72],
				[ 10 , 72],
				[ 11 , 72],
				[ 12 , 72],
				[ 13 , 72],
				[ 14 , 72],
				[ 15 , 72],
				[ 16 , 72],
				[ 17 , 72],
				[ 18 , 72],
				[ 19 , 72],
				[ 20 , 72],[ 21 , 72],[ 22 , 72],[ 23 , 72]]
            },
            {
                key: "TEMPERATURE",
                values: [ [0, 98.7], [ 1 , 99] , 
				[ 2 , 98,7] , 
				[ 3 , 99] , 
				[ 4 , 100] , 
				[ 5 , 101] , 
				[ 6 , 102] , 
				[ 7 , 103] , 
				[ 8 , 100],
				[ 9 , 99],
				[ 10 , 98],
				[ 11 , 98],
				[ 12 , 98],
				[ 13 , 98],
				[ 14 , 98],
				[ 15 , 98],
				[ 16 , 98],
				[ 17 , 98],
				[ 18 , 98],
				[ 19 , 98],
				[ 20 , 98],
				[ 21 , 98],[ 22 , 98],[ 23 , 98]]
                ,
                mean: 65
            },
            {
                key: "SYSTOLIC",
                values: [ [0, 120], [ 1 , 120] , 
				[ 2 , 140] , 
				[ 3 , 130] , 
				[ 4 , 120] , 
				[ 5 , 120] , 
				[ 6 , 160] , 
				[ 7 , 120] , 
				[ 8 , 110],
				[ 9 , 120],
				[ 10 , 120],
				[ 11 , 120],
				[ 12 , 120],
				[ 13 , 120],
				[ 14 , 120],
				[ 15 , 120],
				[ 16 , 120],
				[ 17 , 120],
				[ 18 , 120],
				[ 19 , 120],
				[ 20 , 120],
				[ 21 , 120],[ 22 , 120],[ 23 , 120]]
                ,
                mean: 65
            },
            {
                key: "DIASTOLIC",
                values: [ [0, 80], [ 1 , 70] , 
				[ 2 , 60] , 
				[ 3 , 80] , 
				[ 4 , 70] , 
				[ 5 , 90] , 
				[ 6 , 90] , 
				[ 7 , 70] , 
				[ 8 , 80],
				[ 9 , 80],
				[ 10 , 80],
				[ 11 , 80],
				[ 12 , 80],
				[ 13 , 80],
				[ 14 , 80],
				[ 15 , 80],
				[ 16 , 80],
				[ 17 , 80],
				[ 18 , 80],
				[ 19 , 80],
				[ 20 , 80],
				[ 21 , 80],[ 22 , 80],[ 23 , 80]]
                ,
                mean: 65
            }

        ];

		  /*  $timeout(function() {
               staff_service.send_vitals_alert_nurse({})
                .then(function(response) {
                    //$scope.suppliers = response.data;
            }); 
            }, 3000);*/
			
		
        var getPrescriptionPref = function(){
            doctor_service.get_prescriptionPref($scope.authentication.user.username)
                .then(function(response) {
                    $scope.pref = response.data;
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        var getNurseNotes = function(){
            staff_service.get_nurse_notes_mrnumber($scope.patient_info.mr_number)
                .then(function(response) {
                    $scope.notes = response.data[response.data.length-1];
					
					for(var i=0;i<response.data.length;i++) {
						response.data[i].type = 'notes';
						response.data[i].date = response.data[i].created;
						$scope.allemr.push(response.data[i]);
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        var getLab = function(){
            doctor_service.getlabsbyMRN($scope.patient_info.mr_number)
                .then(function(response) {
                    $scope.labs = response.data;
					
					for(var i=0; i<$scope.labs.length;i++) {
						$scope.labs[i].type = 'lab';
						$scope.labs[i].date = $scope.labs[i].tests[0].Created;
						$scope.allemr.push($scope.labs[i]);
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Fetch Lab Reports.',
                    dismissButton: true
                });
            });
        };
		
		var getRadiologyTests = function(){
            doctor_service.getradiologybyMRN($scope.patient_info.mr_number)
                .then(function(response) {
					
					for(var i=0; i<response.data.length;i++) {
						response.data[i].type = 'radiology';
						response.data[i].date = response.data[i].Created;
						$scope.allemr.push(response.data[i]);
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Fetch Radiology Reports.',
                    dismissButton: true
                });
            });
        };
		
		
		
		var getDepartments = function() {
            ipd_service.get_all_wards().then(function (response) {
       
                $scope.departments = angular.copy(response.data);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Wards.',
                    dismissButton: true
                });
            });
        };
		getDepartments();
		
        var getPatientPrescription = function(){
            doctor_service.get_patient_prescriptions($scope.patient_info.mr_number)
                .then(function(response) {
                    $scope.prescriptions = response.data;
					for(var i=0; i<$scope.prescriptions.length;i++) {
						$scope.prescriptions[i].type = 'prescription';
						$scope.allemr.push($scope.prescriptions[i]);
					}
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.prescriptions = orderBy(prescriptions, $scope.propertyName, $scope.reverse);
        };

        $scope.getMedicines = function(val){
            return inventory_service.search_medicine_name(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };

        $scope.selectPescription = function(prescription){
			if(prescription.type === 'prescription') {
				$scope.prescription = angular.copy(prescription);
				$scope.prescription.followupDate = new Date($scope.prescription.followupDate);
				$scope.oldPrescriptionSelected = true;
				$scope.prescriptionPreview = true;
			} else if(prescription.type == 'lab') {
				$scope.labResult = angular.copy(prescription);
				$scope.oldLabSelected = true;
				$scope.labPreview = true;
			}
        };

        $scope.addMedicine = function(){
        	var tempMed = {
        		'name' : '',
        		'dosage' : '',
        		'timing' : '',
        		'meal' : '',
        		'days' : undefined
        	};
        	$scope.prescription.medicines.push(tempMed);
        };

        $scope.addLabTest = function(){
        	var tempLabTest = {
        		'description' : '',
        		'remarks' : ''
        	};

        	$scope.prescription.labTest.push(tempLabTest);
        };

        $scope.addRadiologyTest = function(){
        	var tempRadioTest = {
        		'description' : '',
        		'remarks' : ''
        	};

        	$scope.prescription.radiology.push(tempRadioTest);
        };

        $scope.removeCol = function(obj,index) {
            obj.splice(index,1);
        };

        $scope.print = function(){
            console.log('print');
        };

        $scope.submit = function(prescription,form){
            prescription.patientName = $scope.patient_info.patient_name;
            prescription.doctorDisplayName = $scope.authentication.user.displayName;
            prescription.doctorUsername = $scope.authentication.user.username;
            prescription.MRNo = $scope.patient_info.mr_number;
            if($scope.notes)
                prescription.nurseNotesId = $scope.notes._id;
           // prescription.date = dateConverter(prescription.date);
            prescription.followupDate = dateConverter(prescription.followupDate);
            
            doctor_service.create_prescription(prescription)
                .then(function(response) {
                    
					if($scope.prescription.transferto) {
						$scope.patient_info.transfertoward = $scope.prescription.transferto.ward;
						$scope.patient_info.status = 'Transfer Order';
					    ipd_service.update_ipd_patient($scope.patient_info).then(function (response) {
							ngToast.create({
								className: 'success',
								content: 'Transfer order generated',
								dismissButton: true
							});					
						}).catch(function (error) {
							ngToast.create({
								className: 'danger',
								content: 'Error: Unable to generate transfer order.',
								dismissButton: true
							});
							
							/*if()
							ipd_service.update_ipdpatient()*/
						});
					}
					
					if($scope.prescription.medicines.length>0) {
						var obj = {
							'mr_number': $scope.patient_info.mr_number,
							'medicine' : $scope.prescription.medicines
						};
						ipd_service.update_ipd_medicine(obj).then(function (response) {
							console.log('medicine updated');			
						}).catch(function (error) {
							console.log('medicine update failed');
						});
					}
					
					var objtosend = [];
					var objtopass = {};
					if(prescription.labTest.length>0) {
						
						for(var j=0;j<prescription.labTest.length;j++) {
							if(prescription.labTest[j].description) {
								
								var ind = getIndex($scope.facilities,'description', prescription.labTest[j].description);
								var cat = '';
								if(ind !== -1) { 
									cat = $scope.facilities[ind].category;
									objtosend .push({
										'service_name': prescription.labTest[j].description,
										'category': cat
									});
								}
								
								/*ipd_service.add_service_to_bill(objtosend).then(function (response) {
									ngToast.create({
										className: 'success',
										content: 'Billing department notified.',
										dismissButton: true
									});
								});*/
							}
						}
					}
					
					if(prescription.radiology.length>0) {
						for(var j=0;j<prescription.radiology.length;j++) {
							if(prescription.radiology[j].description) {
								
								var ind = getIndex($scope.facilities,'description', prescription.radiology[j].description);
								var cat = '';
								if(ind !== -1) { 
									cat = $scope.facilities[ind].category;
								
									objtosend.push({
										'service_name': prescription.radiology[j].description,
										'category': cat
									});
								}

							}
						}
					}
					
					if(objtosend.length > 0) {
						objtopass.mr_number = $scope.patient_info.mr_number;
						objtopass.name = $scope.patient_info.patient_name;
						objtopass.phone = $scope.patient_info.phone;
						objtopass.doctor_name = $scope.doctor_details.name;
						objtopass.doctor_id = $scope.doctor_details._id;
						objtopass.speciality = $scope.doctor_details.speciality;
						objtopass.services = objtosend;
					}
					
					ipd_service.add_services_to_bill(objtopass).then(function (response) {
						ngToast.create({
							className: 'success',
							content: 'Patient Bill updated.',
							dismissButton: true
						});
					});
				/*	print_service.print('/modules/doctor/views/prescription-print.client.view.html',prescription,function(){
                        $location.path('/doctor');
                        $scope.reset(form);
                    });*/
            
                    
                    doctor_service.set_prescription_status(true);

                    ngToast.create({
                        className: 'success',
                        content: 'Prescription Saved Successfully.',
                        dismissButton: true
                    });
                    
            }).catch(function(error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Prescription.',
                    dismissButton: true
                });
            });      	
        };
		
		ipd_service.get_facilities_by_category('Hospital-Lab').then(function (response) {
			$scope.labs = response.data;
		});
		
		ipd_service.get_all_facilities().then(function (response) {
			$scope.facilities = response.data;
		})

        $scope.reset = function(form){
            if (form) {
              form.$setPristine();
              form.$setUntouched();
            }
            $scope.oldPrescriptionSelected = false;
        	$scope.prescriptions = [];
            $scope.prescriptionPreview = false;
			
			$scope.oldLabSelected = false;
			$scope.labPreview = false;
        	$scope.prescription = {
        		'date' : new Date(),
        		'medicineReminder' : true,
        		'medicines' : [{
		        		'name' : '',
		        		'dosage' : '',
		        		'timing' : '',
		        		'meal' : '',
		        		'days' : undefined
		        	}],
		       	'labTest' : [{
		        		'description' : '',
		        		'remarks' : ''
		        	}],
				'radiology' : [{
		        		'description' : '',
		        		'remarks' : ''
		        	}]
		    };
			$scope.allemr = [];
            getPrescriptionPref();
            $scope.patient_info = doctor_service.get_patient();
            console.log($scope.patient_info._id);
            getPatientPrescription();
            getNurseNotes();
			getLab();
			getRadiologyTests();
			
            $scope.processing = false;
        };

        $scope.getoldComplains = function(val){
            return doctor_service.get_old_complaints($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };

        $scope.getoldAdvice = function(val){
            return doctor_service.get_old_advice($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };
		
        $scope.processdischarge = function(){
			$scope.patient_info.status = 'Discharge Order';
			$scope.processing = true;
			ipd_service.update_ipd_patient($scope.patient_info)
                .then(function (response) {
					ngToast.create({
                    className: 'success',
                    content: 'Discharge order posted.',
                    dismissButton: true
                });
				$scope.processing = false;
				$scope.discharge = false;
                    
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to process discharge.',
                    dismissButton: true
                });
				$scope.processing = false;
            });
        };

        $scope.getoldDiagnosis = function(val){
            return doctor_service.get_old_diagnosis($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };

        $scope.init = function(){
            $scope.pref = {};
        	$scope.options = [{value: true, label: 'Yes'},{value: false, label: 'No'}];
        	$scope.reset();
            $scope.oldPrescription = false;
        };

        $scope.init();

       
	}
]).directive('typeaheadFocus', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModel) {

      //trigger the popup on 'click' because 'focus'
      //is also triggered after the item selection
      element.bind('click', function () {

        var viewValue = ngModel.$viewValue;

        //restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue(null);
        }

        //force trigger the popup
        ngModel.$setViewValue(' ');

        //set the actual value in case there was already a value in the input
        ngModel.$setViewValue(viewValue || ' ');
      });

      //compare function that treats the empty space as a match
      scope.emptyOrMatch = function (actual, expected) {
        if (expected == ' ') {
          return true;
        }
        return actual.indexOf(expected) > -1;
      };
    }
  };
});;