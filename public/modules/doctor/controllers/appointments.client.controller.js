'use strict';

angular.module('doctor').controller('DoctorAppointmentController', ['$scope', '$stateParams', '$location', '$timeout', 'Authentication', 'ngToast', 'doctor_service', '$anchorScroll','opd_service',
	function($scope, $stateParams, $location, $timeout, Authentication, ngToast, doctor_service, $anchorScroll,opd_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'doctor') {
        	
        } else {
        	$location.path('/signin');
        }

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        $scope.sortBy = function(propertyName) {
		    $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
		        ? !$scope.reverse : false;
		    $scope.propertyName = propertyName;
		    $scope.appointmentData = orderBy(appointmentData, $scope.propertyName, $scope.reverse);
		};


        $scope.selectAppointmentDoctors = function(doctor) {

            var appointmentDate = new Date();
            appointmentDate = getDateFormat(appointmentDate);
            doctor_service.get_todays_appointments($scope.authentication.user.username, appointmentDate).then(function (response) {
                $scope.appointmentData = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });

        };
		
	    var interval = setInterval($scope.selectAppointmentDoctors, 10000);

		$scope.$on("$destroy", function(event) {clearInterval(interval)});

		$scope.selectAppointment = function(appointment){
			doctor_service.set_appointment(appointment);
			$location.path('/prescription');
		};

		$scope.init = function(){
			$scope.selectAppointmentDoctors();
			$scope.appointmentData = [];
		};

		$scope.init();
	}
]);