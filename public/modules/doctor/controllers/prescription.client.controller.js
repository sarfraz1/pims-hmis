'use strict';

angular.module('doctor').controller('PrescriptionController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'doctor_service', '$anchorScroll','inventory_service','staff_service','opd_service', 'ipd_service', 'print_service','$http',
	function($scope, $stateParams, $location, Authentication, ngToast, doctor_service, $anchorScroll,inventory_service,staff_service,opd_service, ipd_service, print_service,$http) {

        $scope.authentication = Authentication;
        $scope.isDentist =  false;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'doctor') {
             doctor_service.get_doctor_byname($scope.authentication.user.username)
                .then(function(response) {
                    if(response.data.speciality === "Dentist"){
                        $scope.isDentist = true;
                    } else{
                        $scope.isDentist = false;
                    }
                });
        } else {
        	$location.path('/signin');
        }
		$scope.allemr = [];
		$scope.addedMedicines = [];

		var start = function (){
			$http.get('/hospital/logoname.json').then(function(response){
			     $scope.hosDetail= response.data.user;
		  });
		};


		start();

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getPrescriptionPref = function(){
            doctor_service.get_prescriptionPref($scope.authentication.user._id)
                .then(function(response) {
                    $scope.pref = response.data;

					if(!response.data) {
						$scope.pref = {};
						$scope.pref.diagnosis = true;
						$scope.pref.proceduresPerformed = true;
						$scope.pref.timingEnglish = true;
						$scope.pref.advice = true;
						$scope.pref.personalNotes = true;
					}
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        var getNurseNotes = function(){
            doctor_service.get_doctor_byname($scope.authentication.user.username)
                .then(function(response) {
                    if(response.data.speciality === "Dentist"){
                        $scope.isDentist = true;
                        staff_service.get_dental_nurse_notes_mrnumber($scope.patient_info.mr_number)
                            .then(function(response) {
                                response.data[response.data.length-1].extraorals = checkBoxestoString(response.data[response.data.length-1].extraoral);
                                response.data[response.data.length-1].intraorals = checkBoxestoString(response.data[response.data.length-1].intraoral);
                                $scope.notes = response.data[response.data.length-1];

                                for(var i=0;i<response.data.length;i++) {
                                    response.data[i].extraorals = checkBoxestoString(response.data[i].extraoral);
                                    response.data[i].intraorals = checkBoxestoString(response.data[i].intraoral);
                                    response.data[i].type = 'notes';
                                    response.data[i].date = response.data[i].created;
                                    $scope.allemr.push(response.data[i]);
                                }
                        }).catch(function(error) {

                            ngToast.create({
                                className: 'danger',
                                content: 'Error: Unable to Get Vitals.',
                                dismissButton: true
                            });
                        });
                    } else{
                        $scope.isDentist = false;
                        staff_service.get_nurse_notes_mrnumber($scope.patient_info.mr_number)
                            .then(function(response) {
                                $scope.notes = response.data[response.data.length-1];

                                for(var i=0;i<response.data.length;i++) {
                                    response.data[i].type = 'notes';
                                    response.data[i].date = response.data[i].created;
                                    $scope.allemr.push(response.data[i]);
                                }
                        }).catch(function(error) {

                            ngToast.create({
                                className: 'danger',
                                content: 'Error: Unable to Get Vitals.',
                                dismissButton: true
                            });
                        });
                    }
                });
        };

        var checkBoxestoString = function(array){
            var anyTrue =  false;
            var toString = '';
            for(var key in array){
                if(key !== "otherreason" && key !== "other") {
                    if(array[key]){
                        anyTrue =  true;
                        toString+= key;
                        toString+=', ';
                    }
                }
            }
            if(!anyTrue){
                toString = undefined;
            } else {
                toString = toString.slice(0, toString.length-2);
            }
            return toString;
        };

        var getExistingPrescription = function(){
            doctor_service.get_prescription_byappointmentid($scope.patient_info._id)
                .then(function(response) {

				if(response.data) {
					$scope.prescription.diagnosis = response.data.diagnosis;
					$scope.prescription.procedures = response.data.procedures;
					$scope.prescription.advice = response.data.advice;
					$scope.prescription.complaint = response.data.complaint;
					$scope.prescription.notes = response.data.notes;
				}

                   // $scope.prescription = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        var getPatientPrescription = function(){
            doctor_service.get_patient_prescriptions($scope.patient_info.mr_number)
                .then(function(response) {
                    $scope.prescriptions = response.data;
					for(var i=0; i<$scope.prescriptions.length;i++) {
						$scope.prescriptions[i].type = 'prescription';
						$scope.allemr.push($scope.prescriptions[i]);
					}
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescriptions.',
                    dismissButton: true
                });
            });
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.prescriptions = orderBy(prescriptions, $scope.propertyName, $scope.reverse);
        };

        $scope.getMedicines = function(val){
            return inventory_service.search_by_keyword(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };

		$scope.setTradeName = function(item) {

			for(var i=0;i<$scope.addedMedicines.length;i++) {
				for(var j=0;j<item.interactions.length;j++) {
					if(item.interactions[j].name && $scope.addedMedicines[i].formula.indexOf(item.interactions[j].name) >=0) {
						ngToast.create({
							className: 'danger',
							content: item.description+' interacts with '+$scope.addedMedicines[i].description,
							dismissButton: true
						});
					}
				}
			}

			$scope.addedMedicines.push({formula: item.formula,
								description: item.description
								});

		};


        $scope.getServices = function(cat, val){
            return doctor_service.search_services(cat, val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Labs.',
                    dismissButton: true
                });
            });
        };

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

		/*ipd_service.get_facilities_by_category('Hospital-Lab').then(function (response) {
			$scope.labs = response.data;
		});

		ipd_service.get_facilities_by_category('Hospital-OPD-Consultation').then(function (response) {
			$scope.doctorfacilities = response.data;
		});*/

		$scope.labs = [];
		$scope.doctorfacilities = [];
		$scope.others = [];
		ipd_service.get_all_facilities().then(function (response) {
			$scope.facilities = response.data;

			for(var i=0;i<$scope.facilities.length;i++) {
				if($scope.facilities[i].category.indexOf('Hospital-Lab') !== -1){
					$scope.labs.push($scope.facilities[i]);

				} else if($scope.facilities[i].category == 'Hospital-OPD-Consultation') {
					$scope.doctorfacilities.push($scope.facilities[i]);

				} else {
					$scope.others.push($scope.facilities[i]);
				}
			}
		})

 /*       $scope.selectPescription = function(prescription){
            $scope.prescription = angular.copy(prescription);
            $scope.prescription.followupDate = new Date($scope.prescription.followupDate);
            $scope.oldPrescriptionSelected = true;
            $scope.prescriptionPreview = true;
        };*/

        $scope.addMedicine = function(){
        	var tempMed = {
        		'name' : '',
        		'dosage' : '',
        		'timing' : '',
        		'meal' : '',
        		'days' : undefined
        	};
        	$scope.prescription.medicines.push(tempMed);
        };

        $scope.addLabTest = function(){
        	var tempLabTest = {
        		'description' : '',
        		'remarks' : ''
        	};

        	$scope.prescription.labTest.push(tempLabTest);
        };

        $scope.addRadiologyTest = function(){
        	var tempRadioTest = {
        		'description' : '',
        		'remarks' : ''
        	};

        	$scope.prescription.radiology.push(tempRadioTest);
        };


        var getLabTests = function(){
            doctor_service.getlabsbyMRN($scope.patient_info.mr_number)
                .then(function(response) {
					for(var i=0; i<response.data.length;i++) {
						response.data[i].type = 'lab';
						response.data[i].date = response.data[i].tests[0].Created;
						$scope.allemr.push(response.data[i]);
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Fetch Lab Reports.',
                    dismissButton: true
                });
            });
			//for image only labs use only this code
			/* doctor_service.getimglabsbyMRN($scope.patient_info.mr_number)
                .then(function(response) {
					for(var i=0; i<response.data.length;i++) {
						for(var j=0; j<response.data[i].labTests.length;j++) {
							response.data[i].labTests[j].type = 'lab';
							response.data[i].labTests[j].date = response.data[i].created_date;
							response.data[i].labTests[j].time = response.data[i].time;
							$scope.allemr.push(response.data[i].labTests[j]);
						}
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Fetch Lab Reports.',
                    dismissButton: true
                });
            }); */
        };

        var getRadiologyTests = function(){
            doctor_service.getradiologybyMRN($scope.patient_info.mr_number)
                .then(function(response) {

					for(var i=0; i<response.data.length;i++) {
						response.data[i].type = 'radiology';
						response.data[i].date = response.data[i].Created;
						$scope.allemr.push(response.data[i]);
					}
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Fetch Radiology Reports.',
                    dismissButton: true
                });
            });
        };

        $scope.removeCol = function(obj,index) {
            obj.splice(index,1);
        };

        $scope.print = function(pres, form){
			pres.pref = $scope.pref;
			pres.hospitallogo = $scope.hosDetail[0].image_url;
			pres.hospitalname = $scope.hosDetail[0].title;
			pres.hospitalAddress = $scope.hosDetail[0].address;
			pres.hospitalPHNumber = $scope.hosDetail[0].number;
			print_service.print('/modules/doctor/views/prescription-print.client.view.html',pres,function(){

			});
        };

        $scope.submit = function(prescription,form){
            prescription.patientName = $scope.patient_info.patient_name;
			prescription.appointment_id = $scope.patient_info._id;
            prescription.doctorDisplayName = $scope.authentication.user.displayName;
            prescription.doctorUsername = $scope.authentication.user.username;
            prescription.MRNo = $scope.patient_info.mr_number;

			for(var i=0; i<prescription.medicines.length;i++) {
				if(!prescription.medicines[i].name) {
					prescription.medicines.splice(i,1);
				}
			}

            if($scope.notes)
                prescription.nurseNotesId = $scope.notes._id;
          //  prescription.date = dateConverter(prescription.date);
            prescription.followupDate = dateConverter(prescription.followupDate);

            doctor_service.create_prescription(prescription)
                .then(function(response) {

                    opd_service.update_appointment_status({'appointment_id':$scope.patient_info._id,'status':'Confirmed'}).then(function (response) {

                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to set status.',
                            dismissButton: true
                        });
                    });
					var objtosend = prescription;
					objtosend.pref = $scope.pref;
					objtosend.speciality = $scope.doctor_details.speciality;
					objtosend.qualification = $scope.doctor_details.qualification;
					objtosend.notes = $scope.notes;
					objtosend.hospitallogo = $scope.hosDetail[0].image_url;
					objtosend.hospitalname = $scope.hosDetail[0].title;
					objtosend.hospitalAddress = $scope.hosDetail[0].address;
					objtosend.hospitalPHNumber = $scope.hosDetail[0].number;
                    print_service.print('/modules/doctor/views/prescription-print.client.view.html',objtosend,function(){
                        $location.path('/doctor');
                        $scope.reset(form);
                    });


                    doctor_service.set_prescription_status(true);
					var objtosend = [];
					var objtopass = {};
					if(prescription.labTest.length>0) {

						for(var j=0;j<prescription.labTest.length;j++) {
							if(prescription.labTest[j].description) {

								var ind = getIndex($scope.facilities,'description', prescription.labTest[j].description);
								var cat = '';
								if(ind !== -1) {
									cat = $scope.facilities[ind].category;
									objtosend .push({
										'service_name': prescription.labTest[j].description,
										'category': cat
									});
								}

								/*ipd_service.add_service_to_bill(objtosend).then(function (response) {
									ngToast.create({
										className: 'success',
										content: 'Billing department notified.',
										dismissButton: true
									});
								});*/
							}
						}
					}


					if(prescription.radiology.length>0) {
						for(var j=0;j<prescription.radiology.length;j++) {
							if(prescription.radiology[j].description) {

								var ind = getIndex($scope.facilities,'description', prescription.radiology[j].description);
								var cat = '';
								if(ind !== -1) {
									cat = $scope.facilities[ind].category;

									objtosend.push({
										'service_name': prescription.radiology[j].description,
										'category': cat
									});
								}

							}
						}
					}


					if(prescription.referedto) {

						objtosend.push({
								'service_name': prescription.referedto,
								'category': 'Hospital-OPD-Consultation'
							});

							//var ind = getIndex($scope.facilities,'description',objtosend.service_name);
							//objtosend.category = $scope.facilities[ind].category;

					}

					if(objtosend.length > 0) {
						objtopass.mr_number = $scope.patient_info.mr_number;
						objtopass.name = $scope.patient_info.patient_name;
						objtopass.phone = $scope.patient_info.phone;
						objtopass.doctor_name = $scope.doctor_details.name;
						objtopass.doctor_id = $scope.doctor_details._id;
						objtopass.speciality = $scope.doctor_details.speciality;
						objtopass.services = objtosend;
					}

					ipd_service.add_services_to_bill(objtopass).then(function (response) {
						ngToast.create({
							className: 'success',
							content: 'Patient Bill updated.',
							dismissButton: true
						});
					});

                    ngToast.create({
                        className: 'success',
                        content: 'Prescription Saved Successfully.',
                        dismissButton: true
                    });

            }).catch(function(error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Prescription.',
                    dismissButton: true
                });
            });
        };

        $scope.selectEMR = function(prescription){
			if(prescription.type === 'prescription') {
				$scope.prescription = angular.copy(prescription);
				$scope.prescription.followupDate = new Date($scope.prescription.followupDate);
				$scope.oldPrescriptionSelected = true;
				$scope.prescriptionPreview = true;
			} else if(prescription.type == 'radiology') {
				$scope.radiologytest = angular.copy(prescription);
				$scope.radiologySelected = true;
				$scope.radiologyPreview = true;
			} else if(prescription.type == 'lab') {
				$scope.selectedlabtest = angular.copy(prescription);
				$scope.labSelected = true;
				$scope.labtestPreview = true;
			} else if(prescription.type == 'notes') {
				$scope.selectednotes = angular.copy(prescription);
				$scope.notesSelected = true;
				$scope.notesPreview = true;
			}
        };

        $scope.reset = function(form){
            if (form) {
              form.$setPristine();
              form.$setUntouched();
            }
            $scope.oldPrescriptionSelected = false;
			$scope.radiologySelected = false;
			$scope.radiologyPreview = false;
        	$scope.prescriptions = [];
            $scope.prescriptionPreview = false;
			$scope.labSelected = false;
			$scope.labtestPreview = false;

			$scope.notesSelected = false;
			$scope.notesPreview = false;

        	$scope.prescription = {
        		'date' : new Date(),
        		'medicineReminder' : true,
        		'medicines' : [{
		        		'name' : '',
		        		'dosage' : '',
		        		'timing' : '',
		        		'meal' : '',
		        		'days' : undefined
		        	}],
		       	'labTest' : [{
		        		'description' : '',
		        		'remarks' : ''
		        	}],
				'radiology' : [{
		        		'description' : '',
		        		'remarks' : ''
		        	}]
		    };

            getPrescriptionPref();
            $scope.patient_info = doctor_service.get_appointment();
            //console.log($scope.patient_info._id);

			$scope.allemr = [];
            getPatientPrescription();
			getLabTests();
			getRadiologyTests();

			getNurseNotes();
			getExistingPrescription();
            $scope.processing = false;
        };

		var icd10 = [
		'A212	Pulmonary tularemia',
		'A213	Gastrointestinal tularemia',
		'A217	Generalized tularemia',
		'A218	Other forms of tularemia',
		'A219	Tularemia, unspecified',
		'A220	Cutaneous anthrax',
		'A221	Pulmonary anthrax',
		'A222	Gastrointestinal anthrax',
		'A227	Anthrax sepsis',
		'A228	Other forms of anthrax',
		'A229	Anthrax, unspecified',
		'A230	Brucellosis due to Brucella melitensis',
		'A231	Brucellosis due to Brucella abortus',
		'A232	Brucellosis due to Brucella suis',
		'A233	Brucellosis due to Brucella canis',
		'A238	Other brucellosis',
		'A239	Brucellosis, unspecified',
		'A240	Glanders',
		'A241	Acute and fulminating melioidosis',
		'A242	Subacute and chronic melioidosis',
		'A243	Other melioidosis',
		'A249	Melioidosis, unspecified',
		'A250	Spirillosis',
		'A251	Streptobacillosis',
		'A259	Rat-bite fever, unspecified',
		'A260	Cutaneous erysipeloid',
		'A267	Erysipelothrix sepsis',
		'A268	Other forms of erysipeloid',
		'A269	Erysipeloid, unspecified',
		'A270	Leptospirosis icterohemorrhagica',
		'A2781	Aseptic meningitis in leptospirosis',
		'A2789	Other forms of leptospirosis',
		'A279	Leptospirosis, unspecified',
		'A280	Pasteurellosis',
		'A281	Cat-scratch disease',
		'A282	Extraintestinal yersiniosis',
		'A288	Other specified zoonotic bacterial diseases, not elsewhere classified',
		'A289	Zoonotic bacterial disease, unspecified',
		'A300	Indeterminate leprosy',
		'A301	Tuberculoid leprosy',
		'A302	Borderline tuberculoid leprosy',
		'A303	Borderline leprosy',
		'A304	Borderline lepromatous leprosy',
		'A305	Lepromatous leprosy',
		'A308	Other forms of leprosy',
		'A309	Leprosy, unspecified',
		'A310	Pulmonary mycobacterial infection',
		'A311	Cutaneous mycobacterial infection',
		'A312	Disseminated mycobacterium avium-intracellulare complex (DMAC)',
		'A318	Other mycobacterial infections',
		'A319	Mycobacterial infection, unspecified',
		'A320	Cutaneous listeriosis',
		'A3211	Listerial meningitis',
		'A3212	Listerial meningoencephalitis',
		'A327	Listerial sepsis',
		'A3281	Oculoglandular listeriosis',
		'A3282	Listerial endocarditis',
		'A3289	Other forms of listeriosis',
		'A329	Listeriosis, unspecified',
		'A33		Tetanus neonatorum',
		];

		$scope.getoldProcedures = function(val){
            return doctor_service.get_old_procedures($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve procedures.',
                    dismissButton: true
                });
            });
        };

        $scope.getoldComplains = function(val){
            return doctor_service.get_old_complaints($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve complaints.',
                    dismissButton: true
                });
            });
        };

        $scope.getoldAdvice = function(val){
            return doctor_service.get_old_advice($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve advices.',
                    dismissButton: true
                });
            });
        };

		var getdoctordetails = function() {
			 doctor_service.get_doctor_byname($scope.authentication.user.username)
                .then(function(response) {
					$scope.doctor_details = response.data;
				});
		};

        $scope.getoldDiagnosis = function(val){
            return doctor_service.get_old_diagnosis($scope.authentication.user.username,val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };

        $scope.init = function(){
            $scope.pref = {};
        	$scope.options = [{value: true, label: 'Yes'},{value: false, label: 'No'}];
        	$scope.reset();
            $scope.oldPrescription = false;
			getdoctordetails();
        };

        $scope.init();


	}
]).directive('typeaheadFocus', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModel) {

      //trigger the popup on 'click' because 'focus'
      //is also triggered after the item selection
      element.bind('click', function () {

        var viewValue = ngModel.$viewValue;

        //restore to null value so that the typeahead can detect a change
        if (ngModel.$viewValue == ' ') {
          ngModel.$setViewValue(null);
        }

        //force trigger the popup
        ngModel.$setViewValue(' ');

        //set the actual value in case there was already a value in the input
        ngModel.$setViewValue(viewValue || ' ');
      });

      //compare function that treats the empty space as a match
      scope.emptyOrMatch = function (actual, expected) {
        if (expected == ' ') {
          return true;
        }
        return actual.indexOf(expected) > -1;
      };
    }
  };
});;
