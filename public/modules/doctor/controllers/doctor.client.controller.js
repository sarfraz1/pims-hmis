'use strict';

angular.module('doctor').controller('DoctorMainController', ['$scope', '$stateParams', '$state', 'Authentication',
    function($scope, $stateParams, $state, Authentication) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.switchTabs = function(value) {
            if (value === 'appointments') {
                $scope.appointments = true;
                $scope.ipd = false;
            } else if (value === 'ipd') {
                $scope.appointments = false;
				$scope.ipd = true;
            } 
        };
		
    }
]);