'use strict';

angular.module('doctor').controller('PrescriptionPrefrencesController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'doctor_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, ngToast, doctor_service, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'doctor') {
        	
        } else {
        	$location.path('/signin');
        }

        $scope.submit = function(pref){
            pref.doctorName = $scope.authentication.user.username;
			pref.doctorId = $scope.authentication.user._id;
            doctor_service.create_prescriptionPref(pref)
                .then(function(response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Preferences Saved Successfully.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                    //$scope.suppliers = response.data;
            }).catch(function(error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save preferences.',
                    dismissButton: true
                });
            });
        };

        var getPrescriptionPref = function(){
            doctor_service.get_prescriptionPref($scope.authentication.user._id)
                .then(function(response) {
                    $scope.pref = response.data;
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };

        $scope.init = function(){
            $scope.processing = false;
            $scope.pref = {
                'logo' : false,
                'advice' : false,
                'medInstructions' : false,
                'personalNotes' : false,
                'mealOptions' : false,
                'timingEnglish' : false,
                'precrPadTop' : 0
            };
            getPrescriptionPref();
        };

        $scope.init();

       
	}
]);