'use strict';

//Inventories service used to communicate Inventories REST endpoints
angular.module('core').factory('print_service', ['$rootScope', '$compile', '$http', '$timeout','$q',
	function($rootScope, $compile, $http, $timeout,$q) {
        var printHtml = function (html) {
            var deferred = $q.defer();
            var hiddenFrame = $('<iframe style="width: 0;height: 0;border: none;"></iframe>').appendTo('body')[0];
            hiddenFrame.contentWindow.printAndRemove = function() {
                hiddenFrame.contentWindow.print();
                $(hiddenFrame).remove();
                deferred.resolve();
            };
            var htmlContent = "<!doctype html>"+
                        "<html>"+
                            '<head><link rel="stylesheet" type="text/css" href="/style/css/purchase-order-print.css"/></head>'+
                            '<body onload="printAndRemove();">' +
                                html +
                            '</body>'+
                        "</html>";
            var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");
            doc.write(htmlContent);
            doc.close();
            return deferred.promise;
        };

        var openNewWindow = function (html) {
            var newWindow = window.open("printTest.html");
            newWindow.addEventListener('load', function(){
                $(newWindow.document.body).html(html);
            }, false);
        };

        var print = function (templateUrl, data,afterPrint) {
            $rootScope.isBeingPrinted = true;
            $http.get(templateUrl).success(function(template){
                var printScope = $rootScope.$new()
                angular.extend(printScope, data);
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if(printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint, 1000);
                    } else {
                        // Replace printHtml with openNewWindow for debugging
                        printHtml(element.html()).then(function() {
                            if (afterPrint) {
                                afterPrint();
                            }
                        });
                        printScope.$destroy();
                    }
                };
                waitForRenderAndPrint();
            });
        };

        var printAndEmailData = function (templateUrl, data,subject, afterPrint) {
            $rootScope.isBeingPrinted = true;
            $http.get(templateUrl).success(function(template){
                var printScope = $rootScope.$new()
                angular.extend(printScope, data);
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if(printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint, 1000);
                    } else {
                        var html = element.html();
                        var htmlContent = "<!doctype html>"+
                        "<html>"+
                            '<head><link rel="stylesheet" type="text/css" href="/style/css/purchase-order-print.css"/>'+
                            '</head>'+
                            '<body onload="printAndRemove();">' +
                            '<style>h1 {text-align: center;}.purchase-order-print {margin-top: 100px;}.heading {font-size: 14px;font-weight: bold;}.margin-left {padding-left: 20px;: }.grid-table {margin-top: 20px;text-align: center;tr:nth-child(odd) {background-color: #F9F9F9};width: 100%}.ng-hide {display: none}.td-padding {padding-left: 5px;padding-right: 5px}.tr-border {border: 1px solid #ccc}.total-container {border-top: 1px solid #ccc;font-size: 16px;padding: 15px;text-align: right}.signature {border-top: 1px solid #000;width: 138px;padding: 5px 10px;float: right}.signature-container {margin-top: 40px;height: 40px}.pr-no-width {width: 100px}.remarks-container {margin-top: 5px;margin-bottom: 10px;}.remarks-text {margin-left: 80px;}.remarks {float: left;}.footer {bottom: 0;position: absolute;text-align: center;margin-left: -102px;left: 50%;}</style>'+
                                html +
                            '</body>'+
                        "</html>";
                        var emailContent = {
                            'html' : htmlContent,
                            'emailAddress' : data.emailAddressHtml,
														'subject':subject,
                        };
                        $rootScope.isBeingPrinted = false;
                        $http.post('/emailHtml',emailContent).then(function(){
                            if (afterPrint) {
                                afterPrint();
                            }
                        });
                        // Replace printHtml with openNewWindow for debugging
                        printHtml(element.html()).then(function() {
                            // if (afterPrint) {
                            //     afterPrint();
                            // }
                        });
                        // printScope.$destroy();
                    }
                };
                waitForRenderAndPrint();
            });
        };


        var printFromScope = function (templateUrl, scope, afterPrint) {
            $rootScope.isBeingPrinted = true;
            $http.get(templateUrl).then(function(response){
                var template = response.data;
                var printScope = scope;
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if (printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint);
                    } else {
                        printHtml(element.html()).then(function() {
                            $rootScope.isBeingPrinted = false;
                            if (afterPrint) {
                                afterPrint();
                            }
                        });
                    }
                };
                waitForRenderAndPrint();
            });
        };

        var printAndEmail = function (templateUrl, scope, subject, afterPrint) {
					console.log(scope);
						$rootScope.isBeingPrinted = true;
            $http.get(templateUrl).then(function(response){
                var template = response.data;
                var printScope = scope;
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if (printScope.$$phase || $http.pendingRequests.length) {
                        $timeout(waitForRenderAndPrint);
                    } else {
                        var html = element.html();
                        var htmlContent = "<!doctype html>"+
                        "<html>"+
                            '<head><link rel="stylesheet" type="text/css" href="/style/css/purchase-order-print.css"/>'+
                            '</head>'+
                            '<body onload="printAndRemove();">' +
                                html +
                            '</body>'+
                        "</html>";
                        var emailContent = {
                            'html' : htmlContent,
                            'emailAddress' : scope.emailAddressHtml,
														'subject': subject
                        };
                        $rootScope.isBeingPrinted = false;
                        $http.post('/emailHtml',emailContent).then(function(){
                            if (afterPrint) {
                                afterPrint();
                            }
                        });


                    }
                };
                waitForRenderAndPrint();
            });
        };
        return {
            print: print,
            printFromScope:printFromScope,
            printAndEmail:printAndEmail,
            printAndEmailData:printAndEmailData
        }
    }
]);
