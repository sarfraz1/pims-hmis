/**
 *
 * parameters:
 * 1. rowsToAdd:     array of data to show only
 * 2. headingsToAdd: array of headings = [{alias, name, width}]
 *                   width should be passed as %. total should = 100
 *                   will display alias as headings only
 * 3. prefix:        prefix for div ids
 * 4. multiselect:   multiselect/singleselect ['multiple' or 'single']
 * 5. callbackfn:    callback function to return array
 * 6. height:        height of div showing rows
 *
 * requirements:
 * 1. both arrays should be preformatted
 * 2. both arrays should have same order
 *    (array of data's object values should be in same order as array of headings' alias)
 *
 */

'use strict';

angular.module('core').directive('picklistSimple', function($timeout) {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/picklist-simple.client.view.html',      
        scope: {
            headingsToAdd : '=',
            rowsToAdd: '=',
            callbackfn: '&callbackfn',
            prefix: '=',
            multiselect: '=',
            height: '=',
            title: '='
        },
        link: function(scope, element, timeout) {
        	scope.sample = [];
        	scope.headings = [];
			scope.sampleHeadings = 0;

			var assignRowsTimeout = function() {
	        	scope.sample = scope.rowsToAdd;
				for (var i = 0; i < scope.sample.length; i++) {
					scope.sample[i].selected = 'not selected';
					scope.sample[i].searched = 'not searched';
					scope.sample[i].filtered = 'shown';
					scope.sample[i].searchid = i;
				}
			};

			var assignHeadingsTimeout = function() {
				scope.sampleHeadings = 0;
				scope.headings = scope.headingsToAdd;
				for (var key in scope.sample[0]) {
					scope.sampleHeadings++;
				}
				scope.propertyName = scope.headings[0].name;
			};

			var assignHeightTimeout = function() {
				scope.hgt = {'height': scope.height + 'px'};
			};

        	var assignRows = function() {
        		$timeout(assignRowsTimeout);
			};

			var assignHeadings = function() {
				$timeout(assignHeadingsTimeout);
			};

			var assignHeight = function() {
				$timeout(assignHeightTimeout);
			};

			scope.$watch('rowsToAdd', assignRows);
			scope.$watch('headingsToAdd', assignHeadings);
			scope.$watch('height', assignHeight);

			scope.singleClickOutput = [];
			scope.selected = 'no-highlight';
			scope.reverse = true;  
			scope.enterPressed = 0;
			scope.results = [];

			scope.sortBy = function(propertyName) {
				scope.reverse = (scope.propertyName === propertyName.name) ? !scope.reverse : false;
				scope.propertyName = propertyName.name;
			};

			scope.searchHeading = function(heading, searchFilter) {
				if (searchFilter !== '') {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'hidden';
						if (scope.sample[i][heading].toLowerCase().includes(searchFilter.toLowerCase())) {
							scope.sample[i].searched = 'searched';
							scope.sample[i].filtered = 'shown';
						}
					}
				} else {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'shown';
					}
				}
				scope.searchHeadingOutput = heading;
			};

			scope.singleClick = function(item) {
				var alreadyExists = false;
				if (scope.multiselect === 'single') {
					scope.singleClickOutput.splice(0, 1);
					for (var j = 0; j < scope.sample.length; j++) {
						scope.sample[j].selected = 'not selected';
					}
				}
				for (var i = 0; i < scope.singleClickOutput.length; i++) {
					if (scope.singleClickOutput[i] === item) {
						scope.singleClickOutput.splice(i, 1);
						alreadyExists = true;
						item.selected = 'not selected';
					}
				}
				if (!alreadyExists) {
					item.selected = 'selected';
					scope.singleClickOutput.push(item);
				}
			};

			scope.jumpSearch = function() {
				if (scope.enterPressed + 1 < scope.results.length) {
					var main_columns = document.getElementById('main-columns');
					main_columns.scrollTop = 0;
					var prefixno = document.getElementById('' + scope.prefix + scope.results[scope.enterPressed]);
					main_columns.scrollTop = main_columns.scrollTop + prefixno.offsetTop - main_columns.offsetTop;
					scope.enterPressed++;
				} else {
					scope.enterPressed = 0;
				}
			};

			scope.searchItems = function() {
				scope.enterPressed = 0;
				scope.results = [];
				var main_columns = document.getElementById('main-columns');
				main_columns.scrollTop = 0;
				if (scope.searchItem !== '') {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'hidden';
						for (var keys in scope.sample[i]) {
							if (keys !== 'selected' && keys !== 'searched' && keys !== '$$hashKey' && keys !== 'searchid' && keys !== 'filtered') {
								if (scope.sample[i][keys].toLowerCase().includes(scope.searchItem.toLowerCase())) {
									scope.sample[i].searched = 'searched';
									scope.sample[i].filtered = 'shown';
									scope.results.push(i);
								}
							}
						}
					}
				} else {
					main_columns.scrollTop = 0;
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'shown';
					}
				}
			};

			scope.doubleClick = function() {
				if (scope.multiselect === 'single') {
					scope.submit();
				}
			};

			scope.submit = function() {
				for (var i = 0; i < scope.singleClickOutput.length; i++) {
					delete scope.singleClickOutput[i].filtered;
					delete scope.singleClickOutput[i].searchid;
					delete scope.singleClickOutput[i].searched;
					delete scope.singleClickOutput[i].selected;
					delete scope.singleClickOutput[i].$$hashKey;
				}
				scope.callbackfn({arg1: scope.singleClickOutput});
				scope.singleClickOutput = [];
			};
        }
    };
});