'use strict';

angular.module('core').directive('expandablePicklist', function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/expandable-picklist.client.view.html',
        scope: {
            columnsToAdd : '=',
            rowsToAdd: '=datafortable',
            callbackfn: '&callbackfn',
            columnSize: '=columnsizefortable',
            columnToExpand: '=columntoexpand', // column to expand in row
            expandedColumns: '=expandedcolumns' // columns to display for each parent row column expanded
        },
        link: function(scope, element) {
			scope.gridOptions = {
				expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions" style="height:140px;"></div>',
				expandableRowHeight: 150,
                enableFiltering: true,
                enableColumnMenus: false,
				expandableRowScope: {
					subGridVariable: 'subGridScopeVariable'
				},
                enableRowHeaderSelection: false,
                multiSelect: false
			};

            scope.closePopup = function() {
                scope.closepopup();
            };

			var initialize = function() {
				for(var i = 0; i < data.length; i++) {
					data[i].subGridOptions = {
		                enableFiltering: true,
	    				enableRowSelection: false,
                        enableColumnMenus: false,
						appScopeProvider: scope,
						columnDefs: [],
						data: data[i][scope.columnToExpand]
					};
					for (var key in scope.expandedColumns) {
						data[i].subGridOptions.columnDefs.push({name: scope.expandedColumns[key], width: scope.columnSize});
						data[i].subGridOptions.columnDefs.unshift({name: 'Add', width: 105, cellTemplate: '<div class="ui-grid-button-align"><button class="btn btn-xs btn-secondary-outline margin-inline" ng-click="grid.appScope.addSubRow(row)">Add</button></div>', enableFiltering: false});
					}
				}
				scope.gridOptions.data = data;
			};

			var data = [];
            scope.info = {};
            scope.gridWidth = scope.columnSize*scope.columnsToAdd.length+170;
            scope.$watch('rowsToAdd', function() {
                if (scope.rowsToAdd === undefined || scope.columnSize === undefined);
                else {
                    scope.gridOptions.data = scope.rowsToAdd;
                    scope.gridOptions.columnDefs = [];
                    data = scope.rowsToAdd;
                    for (var key in scope.gridOptions.data[0]) {
                        for(var i=0; i<scope.columnsToAdd.length; i++){
                            if(scope.columnsToAdd[i]===key){
                                scope.gridOptions.columnDefs.push({
                                    name: key,
                                    width: scope.columnSize
                                });
                            }
                        }
                    }
    				scope.gridOptions.columnDefs.unshift({name: 'add', width: 120, cellTemplate: '<div class="ui-grid-button-align"><button class="btn btn-xs btn-secondary-outline margin-inline" ng-click="grid.appScope.addEntireRow(row.entity)">Add</button><div>', enableFiltering: false});
					initialize();
                }
            });

			scope.addSubRow = function (row) {
				var obj3 = {};
				for (var attrname in row.entity) { 
					obj3[attrname+scope.columnToExpand] = row.entity[attrname]; 
				}
				for (attrname in row.grid.parentRow.entity) {
					if (attrname !== 'subGridOptions')
						if (attrname === scope.columnToExpand);
						else
							obj3[attrname] = row.grid.parentRow.entity[attrname]; 
				}
				scope.callbackfn({arg1: obj3});
			};

			var scrollArray = function (indexList, enterPressed) {
                scope.gridApi.core.scrollTo(scope.gridOptions.data[indexList[enterPressed]], scope.gridOptions.columnDefs[keyIndex]);
            };

            scope.scrollToFocus = function(rowIndex, colIndex) {
                scope.gridApi.selection.selectRow(scope.gridOptions.data[rowIndex]);
                scope.gridApi.core.scrollTo(scope.gridOptions.data[rowIndex], scope.gridOptions.columnDefs[colIndex]);
            };

            scope.resetEnterPressed = function () {
                scope.enterPressed = 0;
                var results = [];
                scope.searchResults = false;
            };

            scope.searchResults = false;
            scope.enterPressed = 0;
            var results = [];
            var keyIndex = 0;
            scope.searchMode = 'select';

            scope.searchExpandableGrid = function() {
                if (scope.searchMode === 'jump') {
                    if (results.length > 0) {
                        if (scope.enterPressed < results.length - 1) {
                            scope.enterPressed++;
                            scrollArray(results, scope.enterPressed);
                        } else if (scope.enterPressed >= results.length - 1) {
                            scope.enterPressed = 0;
                            results = [];
                        }
                    }
                    if (!scope.searchResults) {
                        results = [];
                        for (var i = 0; i < scope.gridOptions.data.length; i++) {
                            keyIndex = 0;
                            for (var key in scope.gridOptions.data[i]) {
                                keyIndex++;
                                if (scope.gridOptions.data[i][key] === scope.searchObject) {
                                    results.push(i);
                                }
                            }
                        }
                        scrollArray(results, scope.enterPressed);
                    }
                } else if (scope.searchMode === 'select') {
                    for (var j = 0; j < scope.gridOptions.data.length; j++) {
                        keyIndex = 0;
                        for (var key2 in scope.gridOptions.data[j]) {
                            keyIndex++;
                            if (scope.gridOptions.data[j][key2] === scope.searchObject) {
                                scope.scrollToFocus(j, keyIndex - 1);
                                return;
                            }
                        }
                    }
                }
            };

			scope.gridOptions.onRegisterApi = function(gridApi) {
				scope.gridApi = gridApi;
			};

			scope.addEntireRow = function (row) {
				var obj3 = {};
				var newobj = row[scope.columnToExpand];
				for (var i = 0; i < newobj.length; i++) {
					obj3 = {};
					for (var attr in row) {
						if (attr === scope.columnToExpand || attr === 'subGridOptions');
						else
							obj3[attr] = row[attr];
					}

					for (var key in newobj[i]) {
						obj3[key+scope.columnToExpand] = newobj[i][key];
					}
					scope.callbackfn({arg1: obj3});
				}
			};
        }
    };
});