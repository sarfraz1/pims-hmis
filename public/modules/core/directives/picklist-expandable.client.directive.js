/**
 *
 * parameters:
 * 1. rowsToAdd:               array of data to show only
 * 2. headingsToAdd:           array of headings = [{alias, name, width}]
 *                             width should be passed as %. total should = 100
 *                             will display alias as headings only
 * 3. prefix:                  prefix for div ids
 * 4. expandableHeadingsToAdd: headings in subrow to show
 * 5. expandableHeading:       heading in "rowsToAdd" which needs to be expanded
 * 5. callbackfn:              callback function to return array
 * 6. height:        		   height of div showing rows
 *
 * requirements:
 * 1. all arrays should be preformatted
 * 2. both arrays (rowsToAdd & headingsToAdd) should have same order
 *    (array of data's object values should be in same order as array of headings' alias)
 *
 */

'use strict';

angular.module('core').directive('picklistExpandable', function($timeout) {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/picklist-expandable.client.view.html',      
        scope: {
            headingsToAdd : '=',
            expandableHeadingsToAdd : '=',
            expandableHeading : '=',
            rowsToAdd: '=',
            callbackfn: '&callbackfn',
            prefix: '=',
            height: '=',
            title: '='
        },
        link: function(scope, element, timeout) {
        	scope.sample = [];
        	scope.headings = [];
			scope.sampleHeadings = 0;
			scope.expandableHeadings =[];
			scope.expandList = false;
			scope.expandedLists = [];
			scope.expandableCount = 0;
			scope.expandedClickOutput = [];

			var assignHeadingsTimeout = function() {
				scope.sampleHeadings = 0;
				scope.headings = scope.headingsToAdd;
				scope.expandableHeadings = scope.expandableHeadingsToAdd;
				scope.expandableHeading = scope.expandableHeading;
				for (var key in scope.sample[0]) {
					scope.sampleHeadings++;
				}
				scope.propertyName = scope.headings[0].name;
			};

        	var assignRowsTimeout = function() {
	        	scope.sample = scope.rowsToAdd;
				for (var i = 0; i < scope.sample.length; i++) {
					scope.sample[i].selected = 'not selected';
					scope.sample[i].searched = 'not searched';
					scope.sample[i].show = false;
					scope.sample[i].filtered = 'shown';
					scope.sample[i].searchid = i;
					for (var j = 0; j < scope.sample[i][scope.expandableHeading].length; j++) {
						scope.sample[i][scope.expandableHeading][j].selected = 'not selected';
						scope.sample[i][scope.expandableHeading][j].parentid = i;
						scope.sample[i][scope.expandableHeading][j].itemid = j;
					}
				}
			};

			var countExpandableHeadingsTimeout = function() {
				scope.expandableCount = scope.expandableHeadings.length;
			};

			var assignHeightTimeout = function() {
				scope.hgt = {'height': scope.height + 'px'};
			};

			var assignHeadings = function() {
				$timeout(assignHeadingsTimeout);
			};

        	var assignRows = function() {
        		$timeout(assignRowsTimeout);
			};

			var countExpandableHeadings = function() {
				$timeout(countExpandableHeadingsTimeout);
			};

			var assignHeight = function() {
				$timeout(assignHeightTimeout);
			};

			scope.$watch('rowsToAdd', assignRows);
			scope.$watch('headingsToAdd', assignHeadings);
			scope.$watch('headingsToAdd',countExpandableHeadings);
			scope.$watch('height', assignHeight);

			scope.selected = 'no-highlight';
			scope.reverse = true;  
			scope.enterPressed = 0;
			scope.results = [];

			scope.sortBy = function(propertyName) {
				scope.reverse = (scope.propertyName === propertyName.name) ? !scope.reverse : false;
				scope.propertyName = propertyName.name;
			};

			scope.doubleClick = function(index) {
				scope.expandList = !scope.expandList;
			};

			scope.searchHeading = function(heading, searchFilter) {
				if (searchFilter !== '') {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'hidden';
						if (scope.sample[i][heading].toLowerCase().includes(searchFilter.toLowerCase())) {
							scope.sample[i].searched = 'searched';
							scope.sample[i].filtered = 'shown';
						}
					}
				} else {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'shown';
					}
				}
				scope.searchHeadingOutput = heading;
			};

			scope.singleClick = function(item) {
				var alreadyExists = false;
				var expandedItem = {};
				var expandedItems = [];
				for (var i = 0; i < item[scope.expandableHeading].length; i++) {
					expandedItem = {};
					for (var key in item) {
						if (key !== scope.expandableHeading) {
							expandedItem[key] = item[key];
						}
					}

					for (var property in item[scope.expandableHeading][i]) {
						expandedItem[property] = item[scope.expandableHeading][i][property];
					}
					expandedItems.push(expandedItem);
				}

				if (item.selected === 'selected') {
					for (var j = 0; j < expandedItems.length; j++) {
						for (var k = 0; k < scope.expandedClickOutput.length; k++) {
							if (scope.expandedClickOutput[k].parentid === expandedItems[j].parentid && scope.expandedClickOutput[k].itemid === expandedItems[j].itemid) {
								scope.expandedClickOutput.splice(k, 1);
							}
						}
						scope.sample[expandedItems[j].parentid][scope.expandableHeading][expandedItems[j].itemid].selected = 'not selected';
						scope.sample[expandedItems[j].parentid].selected = 'not selected';
					}
				} else {
					for (var j = 0; j < expandedItems.length; j++) {
						alreadyExists = false;
						for (var k = 0; k < scope.expandedClickOutput.length; k++) {
							if (scope.expandedClickOutput[k].parentid === expandedItems[j].parentid && scope.expandedClickOutput[k].itemid === expandedItems[j].itemid) {
								alreadyExists = true;
							}
						}
						if (!alreadyExists) {
							scope.expandedClickOutput.push(expandedItems[j]);
						}
						scope.sample[expandedItems[j].parentid][scope.expandableHeading][expandedItems[j].itemid].selected = 'selected';
						scope.sample[expandedItems[j].parentid].selected = 'selected';
					}
				}
			};

			scope.singleClickExpanded = function(subItem, item) {
				var alreadyExists = false;
				var expandedItem = {};
				for (var key in item) {
					if (key !== scope.expandableHeading) {
						expandedItem[key] = item[key];
					}
				}
				for (key in subItem) {
					expandedItem[key] = subItem[key];
				}
				for (var i = 0; i < scope.expandedClickOutput.length; i++) {
					if (scope.expandedClickOutput[i].itemid === expandedItem.itemid && scope.expandedClickOutput[i].parentid === expandedItem.parentid) {
						alreadyExists = true;
						subItem.selected = 'not selected';
						item.selected = 'not selected';
						scope.expandedClickOutput.splice(i, 1);
					}
				}
				if (!alreadyExists) {
					subItem.selected = 'selected';
					var count = 0;
					for (var j = 0; j < scope.sample[expandedItem.parentid][scope.expandableHeading].length; j++) {
						if (scope.sample[expandedItem.parentid][scope.expandableHeading][j].selected === 'selected') {
							count++;
						}
					}
					if (count === scope.sample[expandedItem.parentid][scope.expandableHeading].length) {
						scope.sample[expandedItem.parentid].selected = 'selected';
					}
					scope.expandedClickOutput.push(expandedItem);
				}
			};

			scope.jumpSearch = function() {
				if (scope.enterPressed + 1 < scope.results.length) {
					var main_columns = document.getElementById('main-columns');
					main_columns.scrollTop = 0;
					var prefixno = document.getElementById('' + scope.prefix + scope.results[scope.enterPressed]);
					main_columns.scrollTop = main_columns.scrollTop + prefixno.offsetTop;
				scope.enterPressed++;
				} else {
					scope.enterPressed = 0;
				}
			};

			scope.searchItems = function() {
				scope.enterPressed = 0;
				scope.results = [];
				var main_columns = document.getElementById('main-columns');
				main_columns.scrollTop = 0;
				if (scope.searchItem !== '') {
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'hidden';
						for (var keys in scope.sample[i]) {
							if (keys !== 'selected' && keys !== 'searched' && keys !== '$$hashKey' && keys !== 'searchid' && keys !== 'filtered' && keys !== scope.expandableHeading && keys !== 'show') {
								if (scope.sample[i][keys].toLowerCase().includes(scope.searchItem.toLowerCase())) {
									scope.sample[i].searched = 'searched';
									scope.sample[i].filtered = 'shown';
									scope.results.push(i);
								}
							}
						}
					}
				} else {
					main_columns.scrollTop = 0;
					for (var i = 0; i < scope.sample.length; i++) {
						scope.sample[i].searched = 'not searched';
						scope.sample[i].filtered = 'shown';
					}
				}
			};

			scope.submit = function() {
				for (var i = 0; i < scope.expandedClickOutput.length; i++) {
					delete scope.expandedClickOutput[i].filtered;
					delete scope.expandedClickOutput[i].searchid;
					delete scope.expandedClickOutput[i].searched;
					delete scope.expandedClickOutput[i].selected;
					delete scope.expandedClickOutput[i].$$hashKey;
					delete scope.expandedClickOutput[i].parentid;
					delete scope.expandedClickOutput[i].itemid;
					delete scope.expandedClickOutput[i].show;
				}
				scope.callbackfn({arg1: scope.expandedClickOutput});
				scope.expandedClickOutput = [];
			};
        }
    };
});