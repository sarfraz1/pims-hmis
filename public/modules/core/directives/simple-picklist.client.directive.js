'use strict';

angular.module('core').directive('simplePicklist', function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/simple-picklist.client.view.html',      
        scope: {
            columnsToAdd : '=',
            rowsToAdd: '=datafortable',
            callbackfn: '&callbackfn',
            columnSize: '=columnsizefortable'
        },
        link: function(scope, element) {
            scope.gridOptions = {
				headerRowHeight: 30,
				rowHeight: 35,
				enableColumnMenus: false,
				enableFiltering: true,
				enableRowHeaderSelection: false,
				multiSelect: true,
				enableRowSelection: true,
				enableSelectAll: true
            };
            scope.info = {};
            scope.gridWidth = scope.columnSize*scope.columnsToAdd.length;
            scope.$watch('rowsToAdd', function() {
                if (scope.rowsToAdd === undefined || scope.columnSize === undefined);
                else {
                    scope.gridOptions.data = scope.rowsToAdd;
                    scope.gridOptions.columnDefs = [];
                    for (var key in scope.gridOptions.data[0]) {
                        for(var i=0;i<scope.columnsToAdd.length;i++){
                            if(scope.columnsToAdd[i]===key){
                                scope.gridOptions.columnDefs.push({
                                    name: key,
                                    width: scope.columnSize
                                });
                            }
                        }       
                    }
                }
            });

            scope.searchMode = 'select';
            scope.searchResults = false;
            scope.enterPressed = 0;
            var results = [];
            var keyIndex = 0;

            scope.getCurrentSelection = function() {
				var values = [];
				var currentSelection = scope.gridApi.selection.getSelectedRows();
				for (var i = 0; i < currentSelection.length; i++) {
					values.push(currentSelection[i]);
				}
                scope.callbackfn({arg1: values});
            };

            var scrollArray = function (indexList, enterPressed) {
      			scope.gridApi.core.scrollTo(scope.gridOptions.data[indexList[enterPressed]], scope.gridOptions.columnDefs[keyIndex]);
            };

            scope.resetEnterPressed = function () {
                scope.enterPressed = 0;
                var results = [];
                scope.searchResults = false;
            };

            scope.searchSomething2 = function() {
                if (scope.searchMode === 'jump') {
                    if (results.length > 0) {
                        if (scope.enterPressed < results.length-1) {
                            scope.enterPressed++;
                            scrollArray(results, scope.enterPressed);
                        } else if (scope.enterPressed >= results.length-1) {
                            scope.enterPressed = 0;
                            results = [];
                            scope.searchSomething(scope.searchObject);
                        }
                    }
                    if (!scope.searchResults) {
                        results = [];
                        for (var i = 0; i < scope.gridOptions.data.length; i++) {
                            keyIndex = 0;
                            for (var key in scope.gridOptions.data[i]) {
                                keyIndex++;
                                if (scope.gridOptions.data[i][key] === scope.searchObject) {
                                    results.push(i);
                                }
                            }
                        }
                        scrollArray(results, scope.enterPressed);
                    }
                } else if (scope.searchMode === 'select') {
                    for (var j = 0; j < scope.gridOptions.data.length; j++) {
                        keyIndex = 0;
                        for (var key2 in scope.gridOptions.data[j]) {
                            keyIndex++;
                            if (scope.gridOptions.data[j][key2] === scope.searchObject) {
								scope.gridApi.selection.selectRow(scope.gridOptions.data[j]);
								scope.gridApi.core.scrollTo(scope.gridOptions.data[j]);
							}
                        }
                    }
                }
            };

            scope.scrollTo = function (rowIndex, colIndex) {
                scope.gridApi.core.scrollTo(scope.gridOptions.data[rowIndex], scope.gridOptions.columnDefs[colIndex]);
            };

            scope.filterOptions = {
                filterText: ''
            };

            scope.scrollToFocus = function( rowIndex, colIndex) {
				scope.gridApi.selection.rowSelection(rowIndex);
            };

			scope.gridOptions.onRegisterApi = function(gridApi) {
				scope.gridApi = gridApi;
				gridApi.selection.on.rowSelectionChanged(scope,function(row){
					var msg = 'row selected ' + row.isSelected;
					// console.log(msg);
				});

				gridApi.selection.on.rowSelectionChangedBatch(scope,function(rows){
					var msg = 'rows changed ' + rows.length;
					// console.log(msg);
				});
			};
        }
    };
});