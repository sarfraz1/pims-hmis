/**
 *
 * parameters:
 * 1. rowsToAdd:     array of data to show only
 * 2. headingsToAdd: array of headings = [{alias, name, width}]
 *                   width should be passed as %. total should = 100
 *                   will display alias as headings only
 * 3. prefix:        prefix for div ids
 * 4. multiselect:   multiselect/singleselect ['multiple' or 'single']
 * 5. callbackfn:    callback function to return array
 * 6. height:        height of div showing rows
 * 7. loadmore:      function used for paginating data on scroll
 * 8. searchdata:    function called to search through original array
 *				     in controller (see purchase order controller)
 *
 * requirements:
 * 1. both arrays should be preformatted
 * 2. both arrays should have same order
 *    (array of data's object values should be in same order as array of headings' alias)
 *
 * limitations:
 * 1. 'description' is used to save selected values
 */

'use strict';

angular.module('core').directive('picklistSimplePagination', function($timeout) {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/picklist-simple-pagination.client.view.html',      
        scope: {
            headingsToAdd : '=',
            rowsToAdd: '=',
            callbackfn: '&',
            loadmore: '&',
            searchdata: '&',
            prefix: '=',
            multiselect: '=',
            height: '=',
            title: '='
        },
        link: function(scope, element, timeout) {
        	scope.sample = [];
        	scope.headings = [];
			scope.sampleHeadings = 0;
			scope.searchKeyword = '';
			scope.singleClickOutput = [];
			scope.selected = 'no-highlight';
			scope.reverse = true;  
			scope.results = [];

			var checkSelected = function() {
				for (var i = 0; i < scope.singleClickOutput.length; i++) {
					for (var j = 0; j < scope.sample.length; j++) {
						if (scope.sample[j].description === scope.singleClickOutput[i].description) {
							scope.sample[j].selected = 'selected';
						}
					}
				}
			};

			var assignRowsTimeout = function() {
				for (var i = scope.sample.length; i < scope.rowsToAdd.length; i++) {
					scope.sample.push(scope.rowsToAdd[i]);
					scope.sample[i].selected = 'not selected';
					scope.sample[i].searched = 'not searched';
					scope.sample[i].filtered = 'shown';
					scope.sample[i].searchid = i;
				}
				checkSelected();
			};

			var assignHeadingsTimeout = function() {
				scope.sampleHeadings = 0;
				scope.headings = scope.headingsToAdd;
				for (var key in scope.sample[0]) {
					scope.sampleHeadings++;
				}
				scope.propertyName = scope.headings[0].name;
			};

			var assignHeightTimeout = function() {
				scope.hgt = {'height': scope.height + 'px'};
			};

        	var assignRows = function() {
        		if (scope.sample.length < 110)
        			$timeout(assignRowsTimeout);
			};

			var assignHeadings = function() {
				if (scope.sample.length < 110)
					$timeout(assignHeadingsTimeout);
			};

			var assignHeight = function() {
				if (scope.sample.length < 110)
					$timeout(assignHeightTimeout);
			};

			scope.$watch('rowsToAdd', assignRows);
			scope.$watch('headingsToAdd', assignHeadings);
			scope.$watch('height', assignHeight);

			scope.sortBy = function(propertyName) {
				scope.reverse = (scope.propertyName === propertyName.name) ? !scope.reverse : false;
				scope.propertyName = propertyName.name;
			};

			scope.singleClick = function(item) {
				var alreadyExists = false;
				if (scope.multiselect === 'single') {
					scope.singleClickOutput.splice(0, 1);
					for (var j = 0; j < scope.sample.length; j++) {
						scope.sample[j].selected = 'not selected';
					}
				}
				for (var i = 0; i < scope.singleClickOutput.length; i++) {
					if (scope.singleClickOutput[i] === item) {
						scope.singleClickOutput.splice(i, 1);
						alreadyExists = true;
						item.selected = 'not selected';
					}
				}
				if (!alreadyExists) {
					item.selected = 'selected';
					scope.singleClickOutput.push(item);
				}
			};

			scope.checkKeywordLength = function() {
				if (scope.searchKeyword.length === 0) {
					scope.sample = [];
					scope.searchdata({keyword: scope.searchKeyword});
				}
			};

			scope.searchControllerItems = function() {
				if (scope.searchKeyword.length > 0) {
					scope.sample = [];
					scope.searchdata({keyword: scope.searchKeyword});
				}
			};

			scope.doubleClick = function() {
				if (scope.multiselect === 'single') {
					scope.submit();
				}
			};

			scope.pagination = function() {
				if (scope.searchKeyword.length === 0) {
					scope.loadmore();
					assignRowsTimeout();
				}
			};

			scope.submit = function() {
				for (var i = 0; i < scope.singleClickOutput.length; i++) {
					delete scope.singleClickOutput[i].filtered;
					delete scope.singleClickOutput[i].searchid;
					delete scope.singleClickOutput[i].searched;
					delete scope.singleClickOutput[i].selected;
					delete scope.singleClickOutput[i].$$hashKey;
				}
				for (i = 0; i < scope.sample.length; i++) {
					if (scope.sample[i].selected === 'selected') {
						scope.sample[i].selected = 'not selected';
					}
				}
				scope.callbackfn({arg1: scope.singleClickOutput});
				scope.singleClickOutput = [];
			};
        }
    };
});