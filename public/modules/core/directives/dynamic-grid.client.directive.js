/* How to use:
 * In controller, the following is required:
 *  1. $scope.callbackfn = function(test) { //callback function to get object from directive
 *      console.log(test);
 *  };
 *
 *  2. $scope.columnSize = 300; //variable to set width of columns
 *  3. $scope.samplejson = {}; //JSON to populate ui-grid with
 *
 * In controller HTML, the following is required:
 *  <dynamic-grid datafortable="samplejson" callbackfn="callbackfn(arg1)" columnsizefortable="columnSize"></dynamic-grid>
 *
 */

'use strict';

angular.module('core').directive('dynamicGrid', function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/core/views/dynamic-grid.client.view.html',      
        scope: {
            columnsToAdd : '=',
            rowsToAdd: '=datafortable',
            callbackfn: '&callbackfn',
            columnSize: '=columnsizefortable'
        },
        link: function(scope, element) {
            scope.gridOptions = {
                headerRowHeight: 30,
                rowHeight: 35,
                enableColumnMenus: false,
                enableFiltering: true,
                enableRowHeaderSelection: false,
                multiSelect: false
            };
            scope.info = {};
            scope.currentFocused = '';
            scope.gridWidth = scope.columnSize*scope.columnsToAdd.length;
            scope.$watch('rowsToAdd', function() {
                if (scope.rowsToAdd === undefined || scope.columnSize === undefined);
                else {
                    scope.gridOptions.data = scope.rowsToAdd;
                    scope.gridOptions.columnDefs = [];
                    for (var key in scope.gridOptions.data[0]) {
                        for(var i=0;i<scope.columnsToAdd.length;i++){
                            if(scope.columnsToAdd[i]===key){
                                scope.gridOptions.columnDefs.push({
                                    name: key,
                                    width: scope.columnSize
                                });
                            }
                        }    
                    }
                    scope.gridOptions.columnDefs.unshift(scope.gridOptions.columnDefs.pop());
                }
            });

            scope.searchResults = false;
            scope.enterPressed = 0;
            var results = [];
            var keyIndex = 0;

            scope.getCurrentSelection = function() {
                var values = [];
                var currentSelection = scope.gridApi.selection.getSelectedRows();
                scope.callbackfn({arg1: currentSelection[0]});
            };

            var scrollArray = function (indexList, enterPressed) {
                scope.gridApi.core.scrollTo(scope.gridOptions.data[indexList[enterPressed]], scope.gridOptions.columnDefs[keyIndex]);
            };

            scope.resetEnterPressed = function () {
                scope.enterPressed = 0;
                var results = [];
                scope.searchResults = false;
            };

            scope.searchSomething = function() {
                if (scope.searchMode === 'jump') {
                    if (results.length > 0) {
                        if (scope.enterPressed < results.length - 1) {
                            scope.enterPressed++;
                            scrollArray(results, scope.enterPressed);
                        } else if (scope.enterPressed >= results.length - 1) {
                            scope.enterPressed = 0;
                            results = [];
                        }
                    }
                    if (!scope.searchResults) {
                        results = [];
                        for (var i = 0; i < scope.gridOptions.data.length; i++) {
                            keyIndex = 0;
                            for (var key in scope.gridOptions.data[i]) {
                                keyIndex++;
                                if (scope.gridOptions.data[i][key] === scope.searchObject) {
                                    results.push(i);
                                }
                            }
                        }
                        scrollArray(results, scope.enterPressed);
                    }
                } else if (scope.searchMode === 'select') {
                    keyIndex = 0;
                    for (var j = 0; j < scope.gridOptions.data.length; j++) {
                        for (var key2 in scope.gridOptions.data[j]) {
                            if (scope.gridOptions.data[j][key2] === scope.searchObject) {
                                scope.scrollToFocus(j, keyIndex - 1);
                                return;
                            }
                            keyIndex++;
                        }
                    }
                }
            };

            scope.searchMode = 'select';

            scope.scrollTo = function (rowIndex, colIndex) {
                scope.gridApi.core.scrollTo(scope.gridOptions.data[rowIndex], scope.gridOptions.columnDefs[colIndex]);
            };

            scope.filterOptions = {
                filterText: ''
            };

            scope.scrollToFocus = function(rowIndex, colIndex) {
                scope.gridApi.cellNav.scrollToFocus(scope.gridOptions.data[rowIndex], scope.gridOptions.columnDefs[colIndex]);
                scope.gridApi.selection.selectRow(scope.gridOptions.data[rowIndex]);
            };

            scope.gridOptions.onRegisterApi = function (gridApi) {
                scope.gridApi = gridApi;
                gridApi.cellNav.on.navigate(scope,function(newRowCol, oldRowCol) {
                    gridApi.selection.selectRow(newRowCol.row.entity);
                });
            };
        }
    };
});