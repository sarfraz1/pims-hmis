'use strict';

// Configuring the Articles module
angular.module('core').run(['$rootScope', '$state', 'Menus', '$http', 'Authentication',
	function($rootScope, $state, Menus, $http, Authentication) {
		$rootScope.$on('$stateChangeStart', function(e, to) {

			// access not required for signin and signup
			if (to.url !== '/signin' && to.url !== '/signup') {
				var auth = Authentication;
				var index = -1;

				// check if module is not for data entry/backdoor access
				if (to.access == undefined) {
					e.preventDefault();
				} else {
					for (var i = 0; i < to.access.length; i++) {
						if (to.access[i] === auth.user.roles) {
							index = i;
						}
					}

					if (index === -1) {
						e.preventDefault();
					}
				}
			}
		});

		// Set top bar menu items
		Menus.addMenu('topbar',false,['opd admin', 'opd receptionist', 'ipd admin', 'ipd receptionist', 'doctor', 'nurse', 'pharmacy admin', 'super admin', 'lab receptionist', 'lab technician', 'lab pathologist']);

		// Inventories for Super Admin and Pharmacy Admin
		Menus.addMenuItem('topbar', 'Inventory Setup','inventories','item','',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Inventory','inventories/create','inventories/create',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Units','inventory-unit','inventory-unit',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Labels','inventory-label','inventory-label',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Stores','inventory-store','inventory-store',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Customer/Supplier','inventory-customer-supplier','inventory-customer-supplier',undefined,['pharmacy admin','super admin'],0);
		Menus.addSubMenuItem('topbar', 'inventories','Document Category','inventory-transaction-category','inventory-transaction-category',undefined,['pharmacy admin','super admin'],0);

		// Documents for Super Admin and Pharmacy Admin
		Menus.addMenuItem('topbar', 'Documents','purchasing','item','',undefined,['pharmacy admin', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'purchasing','Purchase Requisition','purchaseRequisition','purchaseRequisition',undefined,['pharmacy admin', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'purchasing','Purchase Order','purchaseOrder','purchaseOrder',undefined,['pharmacy admin', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'purchasing','GRN','GRN','GRN',undefined,['admin', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'purchasing','Purchase Return','purchaseReturn','purchaseReturn',undefined,['pharmacy admin', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'purchasing','Stock Transfer','stockTransfer','stockTransfer',undefined,['pharmacy admin', 'super admin'],0);

		// All Reports for Super Admin
		Menus.addMenuItem('topbar', 'Reports','superAdminReport','item','',undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Stock Report','stockReport','stockReport', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Sales Report','salesReport','salesReport', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Summarized Sales Report','summarySalesReport','summarySalesReport', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Purchase Report','purchaseReport','purchaseReport', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','PatientWise Revenue','revenue-report','revenue-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Panel Report','panel-report','panel-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Patient Demographics','patients-report','patients-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Doctor Payable','doctor-bills-report','doctor-bills-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','OTA Payable','ota-bills-report','ota-bills-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Invoices Report','invoices-report','invoices-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Itemwise Ledger','inventory-details','inventory-details', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','DepartmentWise Revenue','category-report','category-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','Billings Report','billings-report','billings-report', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'superAdminReport','DepartmentWise Summary Report','departmentwise-all-report','departmentwise-all-report', undefined,['super admin'],0);

		// OPD Reports for OPD Admin
		Menus.addMenuItem('topbar', 'Reports','opdAdminReport','item','',undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','PatientWise Revenue','revenue-report','revenue-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','Doctor Payable','doctor-bills-report','doctor-bills-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','OTA Payable','ota-bills-report','ota-bills-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','Daily Billing','billings-report','billings-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','Panel Report','panel-report','panel-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','Patient Demographics','patients-report','patients-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','PatientWise Discount','discount-report','discount-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','PatientWise Refund','refund-report','refund-report', undefined,['opd admin'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminReport','DepartmentWise Revenue','category-report','category-report', undefined,['opd admin'],0);


		// Pharmacy Reports for Pharmacy Admin
		Menus.addMenuItem('topbar', 'Reports','adminReport','item','',undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Stock Report','stockReport','stockReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Low Stock Report','lowstockReport','lowstockReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Sales Report','salesReport','salesReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Summarized Sales Report','summarySalesReport','summarySalesReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Purchase Report','purchaseReport','purchaseReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Invoices Report','invoices-report','invoices-report', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Stock Adjustment Report','incdecReport','incdecReport', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Itemwise Ledger','inventory-details','inventory-details', undefined,['pharmacy admin'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Narcotics Report','narcoticssalesReport','narcoticssalesReport', undefined,['pharmacy admin'],0);

		// Pharmacy Reports for Report viewing role only
		Menus.addMenuItem('topbar', 'Pharmacy Reports','adminviewingReport','item','',undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminviewingReport','Stock Report','stockReport','stockReport', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminviewingReport','Sales Report','salesReport','salesReport', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Summarized Sales Report','summarySalesReport','summarySalesReport', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminReport','Purchase Report','purchaseReport','purchaseReport', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminviewingReport','Invoices Report','invoices-report','invoices-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminviewingReport','Itemwise Ledger','inventory-details','inventory-details', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'adminviewingReport','Narcotics Report','narcoticssalesReport','narcoticssalesReport', undefined,['reports'],0);

		Menus.addMenuItem('topbar', 'All Reports','opdAdminViewingReport','item','',undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','PatientWise Revenue','revenue-report','revenue-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','Doctor Payable','doctor-bills-report','doctor-bills-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','Daily Billing','billings-report','billings-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','Panel Report','panel-report','panel-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','Patient Demographics','patients-report','patients-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','PatientWise Discount','discount-report','discount-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','PatientWise Refund','refund-report','refund-report', undefined,['reports'],0);
		Menus.addSubMenuItem('topbar', 'opdAdminViewingReport','DepartmentWise Revenue','category-report','category-report', undefined,['reports'],0);

		// Setups for Super Admin and OPD Admin
		Menus.addMenuItem('topbar', 'Setups for Admin', 'hospital-admin', 'item', '', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Doctors', 'doctors', 'doctors', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Departments', 'create-facility', 'create-facility', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Service Pricing', 'facility-pricing', 'facility-pricing', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Specialities', 'specialities', 'specialities', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Areas', 'areas', 'areas', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Panels', 'panels', 'panels', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Reception', 'reception/registerPatient', 'reception/registerPatient', undefined, ['opd admin','super admin'], 0);
		//Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Expenses', 'manage-expense', 'manage-expense', undefined, ['opd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'hospital-admin', 'Manage Users', 'user-management', 'user-management', undefined, ['opd admin','super admin'], 0);

		//Setups for Emergency Admin
		Menus.addMenuItem('topbar', 'Emergency Setups', 'emergency-admin', 'item', '', undefined, ['emergency admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'emergency-admin', 'Manage Beds', 'create-beds', 'create-beds', undefined, ['emergency admin','super admin'], 0);

		//Setups for IPD Admin
		Menus.addMenuItem('topbar', 'IPD Setups', 'ipd-admin', 'item', '', undefined, ['ipd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'ipd-admin', 'Manage Wards', 'create-wards', 'create-wards', undefined, ['ipd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'ipd-admin', 'Manage Beds', 'create-beds', 'create-beds', undefined, ['ipd admin','super admin'], 0);
		Menus.addSubMenuItem('topbar', 'ipd-admin', 'Manage Packages', 'create-packages', 'create-packages', undefined, ['ipd admin','super admin'], 0);

		// Setups for OPD Receptionist
	//	Menus.addMenuItem('topbar', 'OPD', 'opd-receptionist', 'item', '', undefined, ['opd receptionist'], 0);
	//	Menus.addSubMenuItem('topbar', 'opd-receptionist', 'Reception', 'reception/registerPatient', 'reception/registerPatient', undefined, ['opd receptionist'], 0);
	//	Menus.addSubMenuItem('topbar', 'opd-receptionist', 'Generate Bill', 'generate_bill/', 'generate_bill/', undefined, ['opd receptionist'], 0);

		// Reports for OPD Receptionist
		Menus.addMenuItem('topbar', 'Reports','opd-report','item','',undefined,['opd receptionist'],0);
		Menus.addSubMenuItem('topbar', 'opd-report','PatientWise Report','revenue-report','revenue-report', undefined,['opd receptionist'],0);
		Menus.addSubMenuItem('topbar', 'opd-report','Billing Report','billings-report','billings-report', undefined,['opd receptionist'],0);


		// Hospital Details for Super Admin
		Menus.addMenuItem('topbar', 'Admin Setups','hospitals','item','',undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'hospitals','Hospital Details','hospitals','hospitals', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'hospitals','Manage Panel Labs','panel-labs','panel-labs', undefined,['super admin'],0);

		// Lab Orders List and Lab Test Creation for Lab Technician and Lab Pathologist
		Menus.addMenuItem('topbar', 'Lab Setups','lab','item','',undefined,['lab technician', 'lab pathologist','super admin'],0);
		Menus.addSubMenuItem('topbar', 'lab','Lab Test','lab-tests-form','lab-tests-form', undefined,['lab technician', 'lab pathologist','super admin'],0);
		Menus.addSubMenuItem('topbar', 'lab','Lab Orders','lab-orders-list','lab-orders-list', undefined,['lab technician', 'lab pathologist','super admin'],0);
		Menus.addSubMenuItem('topbar', 'lab','Lab Print Setting','lab-setting','lab-setting', undefined,['super admin'],0);
		Menus.addSubMenuItem('topbar', 'lab','Lab Pathologist Signartue','pathologist-signature','pathologist-signature', undefined,['lab pathologist','super admin'],0);

		Menus.addMenuItem('topbar', 'Prescription','prescription','item','',undefined,['doctor'],0);
		Menus.addSubMenuItem('topbar', 'prescription','Appointments','doctor','doctor', undefined,['doctor'],0);
		Menus.addSubMenuItem('topbar', 'prescription','Prescription Preferences','prescriptionPref','prescriptionPref', undefined,['doctor'],0);

		// Radiology Orders List and Radiology Test Creation for Radiologist
		Menus.addMenuItem('topbar', 'Radiology Setups','radiology','item','',undefined,['radiologist', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'radiology','Radiology Test','radiology-tests-form','radiology-tests-form', undefined,['radiologist', 'super admin'],0);
		Menus.addSubMenuItem('topbar', 'radiology','My Signature','radiologist-signautre','radiologist-signature', undefined,['radiologist'],0);

		// Inventories for Store Admin
		Menus.addMenuItem('topbar', 'Inventory Setup','storeinventories','item','',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Inventory','storeinventories/create','storeinventories/create',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Units','storeinventory-unit','storeinventory-unit',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Labels','storeinventory-label','storeinventory-label',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Stores','storeinventory-store','storeinventory-store',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Customer/Supplier','storeinventory-customer-supplier','storeinventory-customer-supplier',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeinventories','Document Category','storeinventory-transaction-category','storeinventory-transaction-category',undefined,['store admin'],0);

		// Documents for Store Admin
		Menus.addMenuItem('topbar', 'Documents','storepurchasing','item','',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','Purchase Requisition','storepurchaseRequisition','storepurchaseRequisition',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','Purchase Order','storepurchaseOrder','storepurchaseOrder',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','GRN','storeGRN','storeGRN',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','Purchase Return','storepurchaseReturn','storepurchaseReturn',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','Issue Stock','storestockIssuance','storestockIssuance',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storepurchasing','Stock Transfer','storestockTransfer','storestockTransfer',undefined,['store admin'],0);

		// Pharmacy Reports for Store Admin
		Menus.addMenuItem('topbar', 'Reports','storeadminReport','item','',undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeadminReport','Stock Report','storestockReport','storestockReport', undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeadminReport','Low Stock Report','storelowstockReport','storelowstockReport', undefined,['store admin'],0);
		Menus.addSubMenuItem('topbar', 'storeadminReport','Itemwise Ledger','storeinventory-details','storeinventory-details', undefined,['store admin'],0);

	}
]);
