'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider','$animateProvider',
	function($stateProvider, $urlRouterProvider,$animateProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/signin');

		// Home state routing
		//$stateProvider.
		// state('home', {
		// 	url: '/',
		// 	templateUrl: 'modules/core/views/home.client.view.html'
		// });
		// state('signin', {
		// 	url: '/',
		// 	templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		// });
		$animateProvider.classNameFilter(/animate/);
	}
]);