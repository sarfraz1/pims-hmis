'use strict';

angular.module('core').controller('SidebarController', ['$http','$scope', '$location','Authentication', 'Menus','$timeout',
		function($http,$scope,$location, Authentication, Menus,$timeout) {
			$scope.authentication = Authentication;

			if (!$scope.authentication.user) {
					$location.path('/signin');
			}else{
				if ($scope.authentication.user.roles === 'receptionist') {
	                    $location.path('/pos');
                } else if($scope.authentication.user.roles === 'admin') {
				    $location.path('/main-page');
                } else if($scope.authentication.user.roles === 'super admin') {
                    $location.path('/admin-main-page');
                }
			}
			$scope.logoClick = function(){
				if ($scope.authentication.user.roles === 'receptionist') {
	                    $location.path('/pos');
                } else if($scope.authentication.user.roles === 'admin') {
				    $location.path('/main-page');
                } else if($scope.authentication.user.roles === 'super admin') {
                    $location.path('/admin-main-page');
                }
			};

			
	}
]);