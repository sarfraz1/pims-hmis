'use strict';

angular.module('core').controller('HeaderController', ['$http','$scope', '$location','Authentication', 'Menus','$timeout','ngToast',
		function($http,$scope,$location, Authentication, Menus,$timeout,ngToast) {

		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');
		$scope.changePass = false;
		var currentLoc = $location.$$absUrl;
		currentLoc = currentLoc.split('/');
		currentLoc = currentLoc[currentLoc.length-1];
		

		$timeout(function () {
		    if(currentLoc === 'signin' || currentLoc === 'signup' || currentLoc === 'pos'){
				angular.element(document.getElementById('headertemplate')).addClass('ng-hide'); 
				angular.element(document.getElementById('leftbartemplate')).addClass('ng-hide');     
			}
		});

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		$scope.logoutUser = function() {

			$http.get('/auth/signout',$scope.authentication.user).success(function(response) {
				// If successful we assign the response to the global user model
				angular.element(document.getElementById('leftbartemplate')).addClass('ng-hide');
                angular.element(document.getElementById('headertemplate')).addClass('ng-hide');
                angular.element(document.getElementById('changepagecontent')).addClass('reduce-page-content');

                $scope.authentication.user = undefined;
				$location.path('/signin');
			
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.changePassword = function() {
			$http.post('/users/password',$scope.credentials).success(function(response) {
				console.log(response);
				$scope.credentials = {};
				$scope.changePass = false;
				ngToast.create({
                    className: 'success',
                    content: response.message,
                    dismissButton: true
                });
			}).error(function(response) {
				ngToast.create({
                    className: 'danger',
                    content: response.message,
                    dismissButton: true
                });
				$scope.error = response.message;
			});
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
	}
]);