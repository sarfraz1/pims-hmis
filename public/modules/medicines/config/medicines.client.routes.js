'use strict';

//Setting up route
angular.module('medicines').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('medicines', {
			url: '/medicines',
			templateUrl: 'modules/medicines/views/medicines.client.view.html'
		})
		.state('medicinesdata', {
			url: '/medicinesdata',
			templateUrl: 'modules/medicines/views/medicinesdata.client.view.html'
		})
		;
	}
]);