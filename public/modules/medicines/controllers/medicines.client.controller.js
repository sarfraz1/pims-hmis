'use strict';

angular.module('medicines').controller('MedicinesController', ['$scope', '$stateParams', '$location', 'Authentication', 'medicines_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, medicines_service, ngToast, $anchorScroll) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if (!$scope.authentication.user) $location.path('/signin');

        var handleResponse = function(response) {
            $scope.showGrid = true;
            $scope.samplejson = [];
            $scope.samplejson = response.data;
        };
        $scope.tradeNames = [];
		
        
        for(var i=0; i<200; i++) {
            $scope.tradeNames.push({description:'',
           inStock: false});
        
        }
        $scope.searchKeyword = '';
        $scope.getItems = function() {
            if ($scope.searchKeyword.length === 0) {
                medicines_service.list_medicines()
                    .then(function (response) {
                        handleResponse(response);
                }).catch(function(error) {
                    console.log(error);
                });
            } else if ($scope.searchKeyword.length > 0) {
                medicines_service.search_medicines_by_name($scope.searchKeyword)
                    .then(function (response) {
                        handleResponse(response);
                }).catch(function(error) {
                    console.log(error);
                });
            }
        };

	 	$scope.hide_directive = function(){
	 		$scope.showGrid = false;
	 	};

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
	    	$scope.medicine = selected_item;
	    	$scope.searchKeyword = '';
	    	$scope.UpdateItem = true;
	    	$scope.showGrid = false;
	 	};

		$scope.columnsToshow = ['tradeName','formula'];
		$scope.UpdateItem = false;
		$scope.showGrid = false;
		$scope.columnSize = 300; //variable to set width of columns
		$scope.samplejson = [];

		var medicine = {
			'tradeName' : '',
			'formula' : ''
		};

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
                  $scope.tradeNames = [];
		
        
        for(var i=0; i<200; i++) {
            $scope.tradeNames.push({description:'',
           inStock: false});
        
        }
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.medicine = angular.copy(medicine);
		};

        $scope.submit_medicine = function(medicine, form) {
            medicine.tradeName = [];
             for(var i=0; i<200; i++) {
                 if($scope.tradeNames[i].description !== '') {
            medicine.tradeName.push($scope.tradeNames[i]);
                     }
        
             }
    		medicines_service.create_medicine(medicine).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Medicine Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
        	});
        };

        $scope.delete_medicine = function(form) {
        	medicines_service.delete_medicine($scope.medicine).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Medicine Deleted Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
        	});
        };

        $scope.update_medicine = function(medicine, form) {
        	medicines_service.update_medicine(medicine).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Medicine Updated Successfully',
                    dismissButton: true
                });
        		$scope.reset(form);
        	});
        };

        $scope.reset_medicine = function(form) {
        	$scope.reset(form);
        };
	}
]);