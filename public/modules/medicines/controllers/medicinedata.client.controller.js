'use strict';

// Inventories controller
angular.module('medicines').controller('MedicineDataController', ['$scope', '$stateParams', '$location', '$timeout', 'Authentication', 'medicines_service','ngToast','$anchorScroll',

	function($scope, $stateParams, $location, $timeout, Authentication, medicines_service,ngToast,$anchorScroll) {
		
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if (!$scope.authentication.user) $location.path('/signin');

        var callSearchTimeout = function() {
            $scope.showSearchMessage = false;
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}

			$scope.processing = false;
			$scope.searchKeyword = '';
			$scope.UpdateItem = false;        
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
			$scope.item = angular.copy(item);
		};
		
		$scope.fetchbrands = function() {
			 medicines_service.fetch_brandnames($scope.item.brandname)
	            .then(function(response) {
					if(response.data != 0)
					{
						for(var i=0;i<response.data.length;i++) {
							$scope.item.alternatebrands[i].name = response.data[i];
						}
					}	
	                //$location.path('/inventory-pricing');
            }).catch(function (error) {
            	
            });
		}

        $scope.submit_item = function(item,form){

    		var item_obj =  {
			'brandname' : item.brandname,
			'pharmaceutical' : item.pharmaceutical,
			'formulaNames' : [],
			'forms' : [],
			'alternatebrands' : []
			};
		
			for(var i=0; i<10; i++) {
				if(item.formulaNames[i].name) {
					item_obj.formulaNames.push(item.formulaNames[i].name);
				}
				
				if(item.forms[i].packaging) {
					item_obj.forms.push(item.forms[i]);
				}
				
			}
			
			for(var i=0; i<200; i++) {
				if(item.alternatebrands[i].name) {
					item_obj.alternatebrands.push(item.alternatebrands[i].name);
				}
			}
			
			if(!item_obj.brandname) {
				ngToast.create({
                    className: 'danger',
                    content: 'Error: Add a brandname.',
                    dismissButton: true
                });
			} else if (item_obj.formulaNames.length == 0) {
					ngToast.create({
                    className: 'danger',
                    content: 'Error: Add included drugs.',
                    dismissButton: true
                });
			}  else if (item_obj.forms.length == 0) {
					ngToast.create({
                    className: 'danger',
                    content: 'Error: Add available Forms and price.',
                    dismissButton: true
                });
			} else {

	        $scope.processing = true;
	        medicines_service.create_medicinedata(item_obj)
	            .then(function(response) {

					ngToast.create({
					  className: 'success',
					  content: 'Medicine Added Successfully',
					  dismissButton: true
					});
					$scope.reset(form);
	                //$location.path('/inventory-pricing');
            }).catch(function (error) {
            	$scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add Medicine.',
                    dismissButton: true
                });
            });
		}
  			
        };

		$scope.getItems = function() {
        	if ($scope.searchKeyword.length === 0) {
        		medicines_service.listAllItems()
        			.then(function (response) {
                        if (response.data.length > 0) {
                            $scope.showGrid = true;
                            $scope.samplejson = [];
                            $scope.samplejson = response.data;
                        } else {
                            $scope.showSearchMessage = true;
                            $timeout(callSearchTimeout, 2000);
                        }
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to retrieve Inventory Items.',
	                    dismissButton: true
	                });
	            });
        	} else if ($scope.searchKeyword.length > 0) {
	        	medicines_service.search_by_keyword($scope.searchKeyword)
		            .then(function (response) {
                        if (response.data.length > 0) {
                            $scope.showGrid = true;
                            $scope.samplejson = [];
                            $scope.samplejson = response.data;
                        } else {
                            $scope.showSearchMessage = true;
                            $timeout(callSearchTimeout, 2000);
                        }
	            }).catch(function (error) {
	                ngToast.create({
	                    className: 'danger',
	                    content: 'Error: Unable to retrieve medicine.',
	                    dismissButton: true
	                });
	            });
	    	}
        };
		
       $scope.callbackfn = function(selected_item) { //callback function to get object from directive
	    	$scope.item = selected_item;

            $scope.item.image_url = $scope.item.image_url + '?' + new Date().getTime();

	    	$scope.searchKeyword = selected_item.code;
	    	$scope.UpdateItem = true;
	 	};

        $scope.update_item = function(item,form){
        	$scope.processing = true;
        	
    		var item_obj = item;

    		medicines_service.update_medicinedata(item_obj)
	            .then(function(response) {
  			
		            ngToast.create({
					  className: 'success',
					  content: 'Medicine Updated Successfully',
					  dismissButton: true
					});
					$scope.reset(form);
					//$location.path('/inventory-pricing');
            }).catch(function (error) {
            	$scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update Medicine.',
                    dismissButton: true
                });
            });   
        };

 /*       $scope.delete_item = function(){
        	$scope.confirmationPopup = 'confirmation-popup';
        };

        $scope.hide_delete_popup = function(){
        	$scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(item,form){
        	$scope.confirmationPopup = 'hide-popup';
        	var item_obj = item;

	        inventory_service.delete_inventory(item_obj)
	            .then(function(response) {
	            	ngToast.create({
					  className: 'success',
					  content: 'Inventory Deleted Successfully',
					  dismissButton: true
					});
					inventory_service.delete_inventory_pricing(item_obj)
						.then(function (response) {
							//console.log(response);
		            }).catch(function (error) {
		                ngToast.create({
		                    className: 'danger',
		                    content: 'Error: Unable to delete Inventory Pricing.',
		                    dismissButton: true
		                });
		            });					
            		$scope.reset(form);
	                //console.log(response);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete Inventory.',
                    dismissButton: true
                });
            });
        };*/

        $scope.itemsSearchTypeahead = function(val){
        	return medicines_service.search_by_keyword(val)
	            .then(function (response) {
	            	return response.data.map(function(item){
				        return item;
				    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve medicine.',
                    dismissButton: true
                });
            });
        };

	 	$scope.hide_directive = function(){
	 		$scope.showGrid = false;
	 	};

        $scope.authentication = Authentication;

        var item = {
			'brandname' : '',
			'pharmaceutical' : '',
			'formulaNames' : [],
			'forms' : [],
			'alternatebrands' : []
		};
		
		for(var i=0;i<10;i++) {
			item.formulaNames[i] = {};
			item.forms[i] = {};
		}
		
		for(var i=0;i<200;i++) {
			item.alternatebrands[i]={};
		}

		$scope.item = item;
		
		$scope.columnsToshow = ['code','description','barcode'];

		$scope.reset();

		$scope.mode = 'Basic';
		$scope.UpdateItem = false;
		$scope.columnSize = 300; //variable to set width of columns

		var fileFormat = '';
	}
]);