'use strict';

angular.module('medicines').factory('medicines_service', ['$http','Upload', '$q', 'config_service',
	function($http, Upload, $q, config_service) {
		/**
		 * Methods for medicines
		 */
		var srvr_address = config_service.serverAddress;

		this.create_medicine = function(item) {
			return $http.post(srvr_address+'/medicines', item);
		};

		this.list_medicines = function() {
			return $http.get(srvr_address+'/medicines');
		};

		this.delete_medicine = function(item) {
			return $http.delete(srvr_address+'/medicines/' + item._id);
		};

		this.update_medicine = function(item) {
			return $http.put(srvr_address+'/medicines/' + item._id, item);
		};

		this.search_medicines_by_name = function(name) {
			return $http.get(srvr_address+'/medicines/name/' + name);
		};

		this.search_medicines_by_formula = function(formula) {
			return $http.get(srvr_address+'/medicines/formula' + formula);
		};
		
		 /**
         * Methods for MedicineData
         */
    	this.create_medicinedata = function(item){
	    	return $http.post(srvr_address+'/medicinedata', item);
    	};

    	this.delete_medicinedata = function(item){
	    	return $http.delete(srvr_address+'/medicinedata/'+item.code);
    	};

    	this.update_medicinedata = function(item){
	    	return $http.put(srvr_address+'/medicinedata/'+item.code , item);
    	};

    	this.search_by_keyword = function(keyword){
    		return $http.get(srvr_address+'/getMedicinedata/description/'+keyword);
    	};

		this.fetch_brandnames = function(keyword){
    		return $http.get(srvr_address+'/getalternatebrands/'+keyword);
    	};
		
		return this;
	}
]);