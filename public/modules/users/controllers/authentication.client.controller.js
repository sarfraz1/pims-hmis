'use strict';

angular.module('users').controller('AuthenticationController', ['$scope','$compile','$cookies','$window', '$remember', '$http','$timeout' ,'$location', 'Authentication','Menus',
	function($scope,$compile, $cookies, $window, $remember, $http,$timeout ,$location, Authentication ,Menus) {
		$scope.authentication = Authentication;

        if ($scope.authentication.user) {
            if ($scope.authentication.user.roles === 'pharmacy salesman') {
                $location.path('/pos');
            } else if ($scope.authentication.user.roles === 'opd admin') {
                $location.path('/doctors');
            } else if ($scope.authentication.user.roles === 'pharmacy admin') {
                $location.path('/main-page');
            } else if ($scope.authentication.user.roles === 'store admin') {
                $location.path('/storemain-page');
            } else if ($scope.authentication.user.roles === 'reports') {
                $location.path('/main-page');
            } else if ($scope.authentication.user.roles === 'super admin') {
                $location.path('/admin-main-page');
            } else if ($scope.authentication.user.roles === 'doctor') {
                $location.path('/doctor');
            } else if ($scope.authentication.user.roles === 'opd receptionist') {
                $location.path('/reception/registerPatient');
            } else if ($scope.authentication.user.roles === 'nurse') {
                $location.path('/nurse-home');
            } else if ($scope.authentication.user.roles === 'dental nurse') {
                $location.path('/nurse-home');
            } else if ($scope.authentication.user.roles === 'ipd nurse') {
                $location.path('/ipdnurse-home');
            } else if ($scope.authentication.user.roles === 'emergency nurse') {
                $location.path('/emergencynurse-home');
            } else if ($scope.authentication.user.roles === 'lab receptionist') {
                $location.path('/lab-reception');
            } else if ($scope.authentication.user.roles === 'lab technician') {
                $location.path('/lab-orders-list');
            } else if ($scope.authentication.user.roles === 'lab pathologist') {
                $location.path('/lab-orders-list');
            } else if ($scope.authentication.user.roles === 'radiologist') {
                $location.path('/radiology-reception');
            } else if ($scope.authentication.user.roles === 'radiology receptionist') {
                $location.path('/radiology-reception');
            } else if ($scope.authentication.user.roles === 'ipd receptionist') {
                $location.path('/ipdreception/ipdregisterPatient');
            } else if ($scope.authentication.user.roles === 'emergency receptionist') {
                $location.path('/emergencyreception/emergencyregisterPatient');
            } else if ($scope.authentication.user.roles === 'ipd admin') {
                $location.path('/create-wards');
            } else if ($scope.authentication.user.roles === 'emergency admin') {
                $location.path('/emergencycreate-beds');
            } else if ($scope.authentication.user.roles === 'medical officer') {
                $location.path('/gynaecology-history');
            }
			
        } else {
            $timeout(function(){
                angular.element(document.getElementById('leftbartemplate')).addClass('ng-hide');
                angular.element(document.getElementById('headertemplate')).addClass('ng-hide');
                angular.element(document.getElementById('changepagecontent')).addClass('reduce-page-content');
            });
        }

		// If user is signed in then redirect back home
		//if ($scope.authentication.user) $location.path('/');

        // directive temp

        $scope.columnsToshow = ['name','description'];
		
        $scope.error = '';

        $scope.credentials = {
            'username' : '',
            'displayName': '',
            'roles': '',
            'password':''
        };

        $scope.dup_password='';

        // Set Background Image for Form Block
        function setImage() {
            var imgUrl = $('.page-content-inner').css('background-image');

            $('.blur-placeholder').css('background-image', imgUrl);
        }

        function changeImgPositon() {
            var width = $(window).width(),
                    height = $(window).height(),
                    left = - (width - $('.single-page-block-inner').outerWidth()) / 2,
                    top = - (height - $('.single-page-block-inner').outerHeight()) / 2;


            $('.blur-placeholder').css({
                width: width,
                height: height,
                left: left,
                top: top
            });
        }

        setImage();
        changeImgPositon();

        $(window).on('resize', function(){
            changeImgPositon();
        });

        // Mouse Move 3d Effect
        var rotation = function(e){
            var TweenMax = new TimelineMax();
            var perX = (e.clientX/$(window).width())-0.5;
            var perY = (e.clientY/$(window).height())-0.5;
            TweenMax.to('.effect-3d-element', 0.4, { rotationY:15*perX, rotationX:15*perY,  ease:Linear.easeNone, transformPerspective:1000, transformOrigin:'center' });
        };

        if (!cleanUI.hasTouch) {
            $('body').mousemove(rotation);
        }

        $scope.remember = false;

        if ($remember('username')) {
            $scope.remember = true;
            $scope.credentials.username  = $remember('username');
        }
        $scope.rememberMe = function() {
            if ($scope.remember) {
                $remember('username', $scope.credentials.username);
            } else {
                $remember('username', '');
            }
        };

        $scope.signup = function() {
            //console.log($scope.credentials);
            if($scope.credentials.displayName!== '' &&
                $scope.credentials.username !== '' &&
                $scope.credentials.roles !== '' &&
                $scope.credentials.password === $scope.dup_password && 
                $scope.credentials.password.length>6){
                $http.post('/auth/signup', $scope.credentials).success(function(response) {
                    // If successful we assign the response to the global user model
                    $scope.authentication.user = response;

                    // And redirect to the index page
                    
                    angular.element(document.getElementById('leftbartemplate')).removeClass('ng-hide');
                    angular.element(document.getElementById('headertemplate')).removeClass('ng-hide');
                    angular.element(document.getElementById('changepagecontent')).removeClass('reduce-page-content');
                    if ($scope.credentials.roles === 'pharmacy salesman') {
                        $location.path('/pos');
                    } else if (response.roles === 'pharmacy admin') {
                        $location.path('/main-page');
                    } else if (response.roles === 'store admin') {
                        $location.path('/storemain-page');
                    } else if (response.roles === 'reports') {
                        $location.path('/main-page');
                    } else if (response.roles === 'opd admin') {
                        $location.path('/doctors');
                    } else if (response.roles === 'super admin') {
                        $location.path('/admin-main-page');
                    } else if (response.roles === 'doctor') {
                        $location.path('/doctor');
                    } else if (response.roles === 'opd receptionist') {
                        $location.path('/reception/registerPatient');
                    } else if (response.roles === 'nurse') {
                        $location.path('/nurse-home');
                    } else if (response.roles === 'dental nurse') {
                        $location.path('/nurse-home');
                    } else if (response.roles === 'ipd nurse') {
                        $location.path('/ipdnurse-home');
                    } else if (response.roles === 'emergency nurse') {
                        $location.path('/emergencynurse-home');
                    } else if (response.roles === 'lab receptionist') {
                        $location.path('/lab-reception');
                    } else if (response.roles === 'lab technician') {
                        $location.path('/lab-orders-list');
                    } else if (response.roles === 'lab pathologist') {
                        $location.path('/lab-orders-list');
                    } else if (response.roles === 'radiologist') {
                        $location.path('/radiology-reception');
                    } else if (response.roles === 'radiology receptionist') {
                        $location.path('/radiology-reception');
                    } else if (response.roles === 'ipd receptionist') {
                        $location.path('/ipdreception/ipdregisterPatient');
                    } else if (response.roles === 'ipd admin') {
						$location.path('/create-wards');
					} else if (response.roles === 'emergency receptionist') {
                        $location.path('/emergencyreception/emergencyregisterPatient');
                    } else if (response.roles === 'emergency admin') {
						$location.path('/emergencycreate-beds');
					} else if (response.roles === 'medical officer') {
						$location.path('/gynaecology-history');
					}
                    // var content = angular.element(document.querySelector( '#navHeader' ));
                    // var scope = content.scope();
                    // $compile(content.contents()(scope));
                    //$window.location.reload();
                    Menus.updateUser();

                }).error(function(response) {
                    if(response.code===11000){
                        $scope.error = 'User already exists';
                    }else {
                     $scope.error = response.message;
                    }
                });
            }
            else if($scope.credentials.displayName=== ''){
                $scope.error = 'Display name field is Empty';
            }
            else if($scope.credentials.username=== ''){
                $scope.error = 'Email or username field is Empty';
            }
            else if($scope.credentials.roles=== ''){
                $scope.error = 'Select role of user';
            }
            else if($scope.credentials.password.length<7){
                $scope.error = 'password Length should be greator than 6';
            }
            else if($scope.credentials.password === $scope.dup_password){
                $scope.error = 'Password do not match';
            }
        };

		$scope.signin = function() {
            $scope.credentials.password = '' + $scope.credentials.password;

			$http.post('/auth/signin', $scope.credentials, {"Content-Type":"text/plain"}).success(function(response) {
                if ($scope.remember) {
                    $remember('username', $scope.credentials.username);
                } else {
                    $remember('username', '');
                }
				// If successful we assign the response to the global user model

				$scope.authentication.user = response;
                // console.log(response);
                angular.element(document.getElementById('leftbartemplate')).removeClass('ng-hide');
                angular.element(document.getElementById('headertemplate')).removeClass('ng-hide');
                angular.element(document.getElementById('changepagecontent')).removeClass('reduce-page-content');
                if (response.roles === 'pharmacy salesman') {
                    $location.path('/pos');
                } else if (response.roles === 'opd admin') {
				    $location.path('/doctors');
                } else if (response.roles === 'pharmacy admin') {
                    $location.path('/main-page');
                } else if (response.roles === 'store admin') {
                    $location.path('/storemain-page');
                } else if (response.roles === 'reports') {
                    $location.path('/main-page');
                } else if (response.roles === 'super admin') {
                    $location.path('/admin-main-page');
                } else if (response.roles === 'doctor') {
                    $location.path('/doctor');
                } else if (response.roles === 'opd receptionist') {
                    $location.path('/reception/registerPatient');
                } else if (response.roles === 'nurse') {
                    $location.path('/nurse-home');
                } else if (response.roles === 'dental nurse') {
                    $location.path('/nurse-home');
                } else if (response.roles === 'ipd nurse') {
                    $location.path('/ipdnurse-home');
                } else if (response.roles === 'emergency nurse') {
                    $location.path('/emergencynurse-home');
                } else if (response.roles === 'lab receptionist') {
                    $location.path('/lab-reception');
                } else if (response.roles === 'lab technician') {
                    $location.path('/lab-orders-list');
                } else if (response.roles === 'lab pathologist') {
                    $location.path('/lab-orders-list');
                } else if (response.roles === 'radiologist') {
                    $location.path('/radiology-reception');
                } else if (response.roles === 'radiology receptionist') {
                    $location.path('/radiology-reception');
                } else if (response.roles === 'ipd receptionist') {
                    $location.path('/ipdreception/ipdregisterPatient');
                }  else if (response.roles === 'ipd admin') {
                    $location.path('/create-wards');
                } else if (response.roles === 'emergency receptionist') {
                    $location.path('/emergencyreception/emergencyregisterPatient');
                }  else if (response.roles === 'emergency admin') {
                    $location.path('/emergencycreate-beds');
                }  else if (response.roles === 'medical officer') {
                    $location.path('/gynaecology-history');
                }

                // var content = angular.element();
                // var scope = content.scope();
                // $compile(content)(scope);
                //angular.element(document.querySelector('#navHeader')).$render();
                //$compile(content);

                //$window.location.reload();
                Menus.updateUser();
                
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);