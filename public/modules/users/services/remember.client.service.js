'use strict';

angular.module('users').factory('$remember', function() {
    function fetchValue(name) {
        var gCookieVal = document.cookie.split('; ');
        for (var i = 0; i < gCookieVal.length; i++)
        {
            // a name/value pair (a crumb) is separated by an equal sign
            var gCrumb = gCookieVal[i].split('=');
            if (name === gCrumb[0])
            {
                var value = '';
                try {
                    value = angular.fromJson(gCrumb[1]);
                } catch(e) {
                    value = unescape(gCrumb[1]);
                }
                return value;
            }
        }
        // a cookie with the requested name does not exist
        return null;
    }
    return function(name, values) {
        if(arguments.length === 1) return fetchValue(name);
        var cookie = name + '=';
        cookie += values + ';';
        var date = new Date();
        date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
        cookie += 'expires=' + date.toGMTString() + '; ';
        document.cookie = cookie;
    }
});