'use strict';

angular.module('hospital-admin').controller('AdminDoctorsController', ['$scope', '$stateParams', '$timeout', '$location', 'Authentication', 'hospitalAdmin_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $timeout, $location, Authentication, hospitalAdmin_service, ngToast, $anchorScroll) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showDoctors = false;
        $scope.searchDoctorKeyword = '';
        $scope.processing = false;
        
        // doctor simple picklist
        $scope.prefix = 'doctordiv';
        $scope.doctorsToShow = [];
        $scope.headings = [];
        $scope.doctorsData = [];
        $scope.headings.push({'alias': 'Name', 'name': 'name', 'width': 49});
        $scope.headings.push({'alias': 'Speciality', 'name': 'speciality', 'width': 49});

        // time picker settings
		$scope.hstep = 1;
		$scope.mstep = 10;
		$scope.ismeridian = true;
		$scope.schedules = [{}];

		// time schedule array
		var reset_schedules = function() {
			$scope.schedules = [{}];
	        $scope.schedules[0].endhrs = 5;
	        $scope.schedules[0].endmins = 30;
	        $scope.schedules[0].starthrs = 8;
	        $scope.schedules[0].startmins = 30;
	        $scope.schedules[0].end_meridian = 'PM';
	        $scope.schedules[0].start_meridian = 'AM';
	        $scope.schedules[0].days = [
	        {
	        	'name': 'Monday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Tuesday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Wednesday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Thursday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Friday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Saturday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Sunday',
	        	'selected': 'no'
	        }
	        ];
		};

		var doctor = {
			'pmdc_number': '',
			'name': '',
			'consultation_fee': undefined,
			'percentage_fee': 100,
			'speciality': '',
			'slot_duration': 1,
        	'schedule': [
        	{
	        	'days': [],
	        	'endhrs': 0,
	        	'endmins': 0,
	        	'end_meridian': 'AM',
	        	'starthrs': 0,
	        	'startmins': 0,
	        	'start_meridian': 'AM'
        	}
        	]
		};

		var getSpecialities = function() {
			hospitalAdmin_service.get_specialities().then(function (response) {
				$scope.specialities = response.data;
			}).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Specialities.',
                    dismissButton: true
                });
			});
		};

		getSpecialities();

		$scope.updateUserName = function() {
			$scope.doctor.username = $scope.doctor.name.replace(/\s+/g, '').toLowerCase();
		};

		$scope.getDoctors = function() {
			if ($scope.searchDoctorKeyword.length === 0) {
				$scope.processing = true;
				hospitalAdmin_service.get_doctors().then(function (response) {
			        $scope.doctorsToShow = [];
			        $scope.headings = [];
			        $scope.doctorsData = [];
			        $scope.doctorsData = response.data;
			        $scope.headings.push({'alias': 'Name', 'name': 'name', 'width': 49});
			        $scope.headings.push({'alias': 'Speciality', 'name': 'speciality', 'width': 49});
			        for (var i = 0; i < response.data.length; i++) {
			        	$scope.doctorsToShow.push({'name': response.data[i].name, 'speciality': response.data[i].speciality});
			        }
			        $scope.showDoctors = true;
			        $scope.processing = false;
			        var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
			        $timeout(function() {setFocus[0].focus()});
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve doctors.',
                        dismissButton: true
                    });
                });
			} else {
				$scope.processing = true;
				hospitalAdmin_service.get_doctors().then(function (response) {
			        $scope.doctorsData = [];
			        $scope.doctorsData = response.data;
			        $scope.processing = false;
                }).catch(function (error) {
                	$scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve doctors.',
                        dismissButton: true
                    });
                });
			}
		};

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

		$scope.changemeridianfrom = function(index) {
			if($scope.schedules[index].start_meridian == 'AM') {
				$scope.schedules[index].start_meridian = 'PM';
			} else {
				$scope.schedules[index].start_meridian = 'AM';
			}
		};
		
		$scope.changemeridianTo = function(index) {
			if($scope.schedules[index].end_meridian == 'AM') {
				$scope.schedules[index].end_meridian = 'PM';
			} else {
				$scope.schedules[index].end_meridian = 'AM';
			}
		};
		
		$scope.callbackdoctor = function(doctor) {
            if (Array.isArray(doctor)) {
                var doctorIndex = getIndex($scope.doctorsData, 'name', doctor[0].name);
            } else {
                var doctorIndex = getIndex($scope.doctorsData, 'name', doctor.name);
            }
			reset_schedules();
			var id = $scope.doctorsData[doctorIndex]._id;
			hospitalAdmin_service.get_doctor(id).then(function (response) {
				$scope.reset();
				$scope.doctor = response.data;
				
				var schedule = response.data.schedule;
				var mod_schedule = [];
				for(var i=0;i<schedule.length;i++) {
					var timing = {};

					for(var j = 0; j < schedule[i].timings.length; j++) {
						var index = undefined;
						for(var k=0;k<mod_schedule.length;k++) {
							if(mod_schedule[k].starthrs == schedule[i].timings[j].starthrs && mod_schedule[k].startmins == schedule[i].timings[j].startmins &&
								 mod_schedule[k].start_meridian == schedule[i].timings[j].start_meridian && mod_schedule[k].endhrs == schedule[i].timings[j].endhrs && mod_schedule[k].endmins == schedule[i].timings[j].endmins &&
								 mod_schedule[k].end_meridian == schedule[i].timings[j].end_meridian) {
								index = k;
								break;
							}
						}
					/*	var index = findIndex(mod_schedule, function(o) { return (o.starthrs == schedule[i].timings[j].starthrs && o.startmins == schedule[i].timings[j].startmins &&
						 o.start_meridian == schedule[i].timings[j].start_meridian && o.endhrs == schedule[i].timings[j].endhrs && o.endmins == schedule[i].timings[j].endmins &&
						 o.end_meridian == schedule[i].timings[j].end_meridian); });*/
						var obj = {};
						if(index == undefined) {
							obj.days = [{name: schedule[i].day}];
							obj.starthrs = schedule[i].timings[j].starthrs;
							obj.endhrs = schedule[i].timings[j].endhrs;
							obj.start_meridian = schedule[i].timings[j].start_meridian;
							obj.end_meridian = schedule[i].timings[j].end_meridian;
							obj.startmins = schedule[i].timings[j].startmins;
							obj.endmins = schedule[i].timings[j].endmins;
							mod_schedule.push(obj);
						} else {
							mod_schedule[index].days.push({name:schedule[i].day});
						}
					}
				}
				$scope.doctor.schedule = mod_schedule;

				for (i = 0; i < $scope.doctor.schedule.length; i++) {
					$scope.add_timing();
				}
				$scope.schedules.splice(0, 1);

				for (i = 0; i < $scope.doctor.schedule.length; i++) {
					for (var j = 0; j < $scope.doctor.schedule[i].days.length; j++) {
						var dayIndex = getIndex($scope.schedules[i].days, 'name', $scope.doctor.schedule[i].days[j].name)
						if (dayIndex !== -1) {
							$scope.schedules[i].days[dayIndex].selected = 'yes';
							$scope.schedules[i].endhrs = parseFloat($scope.doctor.schedule[i].endhrs);
							$scope.schedules[i].endmins = parseFloat($scope.doctor.schedule[i].endmins);
							$scope.schedules[i].end_meridian = $scope.doctor.schedule[i].end_meridian;
							$scope.schedules[i].starthrs = parseFloat($scope.doctor.schedule[i].starthrs);
							$scope.schedules[i].startmins = parseFloat($scope.doctor.schedule[i].startmins);
							$scope.schedules[i].start_meridian = $scope.doctor.schedule[i].start_meridian;
						}
					}
				}
				$scope.doctor.schedule = $scope.schedules;
				if($scope.doctor.pmdc_expiry) {
					var pmdc = moment($scope.doctor.pmdc_expiry);
					var now = moment();
					if(pmdc.isBefore(now)) {
						ngToast.create({
							className: 'danger',
							content: 'PMDC number has expired',
							dismissButton: true
						});
					}
				}
				$scope.doctor.pmdc_expiry = new Date($scope.doctor.pmdc_expiry);
				
				
				$scope.UpdateItem = true;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
			$scope.showDoctors = false;
		};

		$scope.select_day = function(day) {
			if (day.selected === 'yes') {
				day.selected = 'no';
			} else {
				day.selected = 'yes';
			}
		};

		$scope.check_timing = function() {
			var check = validate_schedules();
			if (check)
				$scope.add_timing();
		};

		$scope.add_timing = function() {
			$scope.schedules.unshift({});
	        $scope.schedules[0].endhrs = 5;
	        $scope.schedules[0].endmins = 30;
	        $scope.schedules[0].end_meridian = 'PM';
	        $scope.schedules[0].starthrs = 8;
	        $scope.schedules[0].startmins = 30;
	        $scope.schedules[0].start_meridian = 'AM';
			$scope.schedules[0].days = [
	        {
	        	'name': 'Monday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Tuesday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Wednesday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Thursday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Friday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Saturday',
	        	'selected': 'no'
	        },
	        {
	        	'name': 'Sunday',
	        	'selected': 'no'
	        }
	        ];
		};

        $scope.reset = function(form) {
        	$scope.doctor = {};
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.UpdateItem = false;
            $scope.doctor = angular.copy(doctor);
            $scope.searchDoctorKeyword = '';
			reset_schedules();
				hospitalAdmin_service.get_doctors().then(function (response) {
		        $scope.doctorsData = [];
		        $scope.doctorsData = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        $scope.reset();

        var validate_schedules = function() {
        	var scheduleArray = [];
        	for (var i = 0; i < $scope.schedules.length; i++) {
        		var count = 0;
        		for (var j = 0; j < $scope.schedules[i].days.length; j++) {
        			if ($scope.schedules[i].days[j].selected === 'yes') {
        				scheduleArray.push({'day': $scope.schedules[i].days[j].name, 'starthrs': $scope.schedules[i].starthrs, 'startmins': $scope.schedules[i].startmins,
						'start_meridian': $scope.schedules[i].start_meridian, 'endhrs': $scope.schedules[i].endhrs, 'endmins': $scope.schedules[i].endmins,
						'end_meridian': $scope.schedules[i].end_meridian});
        			} else {
        				count++;
        			}
        		}
        		// no days selected in some schedule
        		if (count === 7) {
		            ngToast.create({
		                className: 'warning',
		                content: 'No day(s) selected in Schedule ' + ($scope.schedules.length - i) + '.',
		                dismissButton: true
		            });
        			return false;
        		}
        	}
        	for (i = 0; i < scheduleArray.length; i++) {
        		var originalTimingTo = 0;
        		var originalTimingFrom = 0;
				var tohrs = (scheduleArray[i].end_meridian == 'PM' && parseInt(scheduleArray[i].endhrs) != 12) ? (parseInt(scheduleArray[i].endhrs)+12) : (parseInt(scheduleArray[i].endhrs));
				var fromhrs = (scheduleArray[i].start_meridian == 'PM' && parseInt(scheduleArray[i].starthrs)!=12) ? (parseInt(scheduleArray[i].starthrs) + 12) : (parseInt(scheduleArray[i].starthrs));
        		originalTimingTo = (tohrs * 60) + parseInt(scheduleArray[i].endmins);
        		originalTimingFrom = (fromhrs * 60) + parseInt(scheduleArray[i].startmins);
        		var totalTime = originalTimingTo - originalTimingFrom;
        		if (totalTime % $scope.doctor.slot_duration !== 0) {
		            ngToast.create({
		                className: 'warning',
		                content: 'Timing and slot duration have conflicting values in schedules.',
		                dismissButton: true
		            });
        			return false;
        		}
        		for (var j = 0; j < scheduleArray.length; j++) {
        			var checkTimingTo = 0;
        			var checkTimingFrom = 0;
        		
					var tohrs = (scheduleArray[j].end_meridian == 'PM' && parseInt(scheduleArray[j].endhrs) != 12) ? (parseInt(scheduleArray[j].endhrs) + 12) : (parseInt(scheduleArray[j].endhrs));
					var fromhrs =  (scheduleArray[j].start_meridian == 'PM' && parseInt(scheduleArray[j].starthrs) != 12) ? (parseInt(scheduleArray[j].starthrs) + 12) : (parseInt(scheduleArray[j].starthrs));
					checkTimingTo = (tohrs * 60) + parseInt(scheduleArray[j].endmins);
					checkTimingFrom = (fromhrs * 60) + parseInt(scheduleArray[j].startmins);
	        		// end time is smaller than start time
	        		if (checkTimingTo - checkTimingFrom <= 0) {
			            ngToast.create({
			                className: 'warning',
			                content: 'Start time should be less than ending time in all schedules.',
			                dismissButton: true
			            });
	        			return false;
	        		}
	        		// overlapping timings for same day
	        		if (i !== j && scheduleArray[i].day === scheduleArray[j].day) {
	        			if (checkTimingFrom <= originalTimingTo && originalTimingFrom <= checkTimingTo) {
				            ngToast.create({
				                className: 'warning',
				                content: 'Schedules contain overlapping timings.',
				                dismissButton: true
				            });
	        				return false;
	        			}
	        		}
        		}
        	}
        	return true;
        };

        $scope.submit_medical_officer = function(doctor, form) {
        	$scope.processing = true;
        	$scope.doctor.schedule = [];
        	$scope.doctor.slot_duration = 1;
			hospitalAdmin_service.create_doctor($scope.doctor).then(function (response) {
	            ngToast.create({
	                className: 'success',
	                content: 'Doctor Added Successfully.',
	                dismissButton: true
	            });
				$scope.reset(form);
				$scope.processing = false;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to add doctor.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
        };

		$scope.submit_doctor = function(doctor, form) {
			var valid = validate_schedules();
			if (!valid) {
	            ngToast.create({
	                className: 'warning',
	                content: 'Invalid timings entered.',
	                dismissButton: true
	            });
			} else {
				$scope.processing = true;
				$scope.doctor.schedule = angular.copy($scope.schedules);
				for (var i = 0; i < $scope.doctor.schedule.length; i++) {
					for (var j = 0; j < $scope.doctor.schedule[i].days.length; j++) {
						if ($scope.doctor.schedule[i].days[j].selected === 'no') {
							$scope.doctor.schedule[i].days.splice(j, 1);
							j--;
						} else {
							delete $scope.doctor.schedule[i].days[j].selected;
						}
					}
				}
				if ($scope.doctor.percentage_fee == null) {
					$scope.doctor.percentage_fee = 100;
				}
				hospitalAdmin_service.create_doctor($scope.doctor).then(function (doctor) {
		            ngToast.create({
		                className: 'success',
		                content: 'Doctor Added Successfully.',
		                dismissButton: true
		            });
					
					//create service pricing for doctor
					/*var service = {
						'description' : doctor.data.name,
						'price' : doctor.data.consultation_fee,
						'category' : 'Hospital-OPD-Consultation'
					};
					
					 hospitalAdmin_service.create_facility_price(service).then(function (response) {
						var serviceDetails = {
								description : doctor.data.name, 
								category : 'Hospital-OPD-Consultation',
								expenses : [],
								shares : [{
											doctorId : doctor.data._id,
											doctorName : doctor.data.name,
											commissionPerc : doctor.data.percentage_fee
										}]
						};
						hospitalAdmin_service.update_service_details(serviceDetails).then(function (response) {
							$scope.reset(form);
							$scope.processing = false;
						});
					 });*/
					$scope.reset(form);
					$scope.processing = false;
                }).catch(function (error) {
                	$scope.doctor.schedule = angular.copy($scope.schedules);
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add doctor.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
			}
		};

		$scope.update_medical_officer = function(doctor, form) {
			$scope.processing = true;
			$scope.doctor.pmdc_number = '' + $scope.doctor.pmdc_number;
        	$scope.doctor.schedule = [];
        	$scope.doctor.slot_duration = 1;
			hospitalAdmin_service.update_doctor($scope.doctor).then(function (response) {
	            ngToast.create({
	                className: 'success',
	                content: 'Doctor Updated Successfully.',
	                dismissButton: true
	            });
				$scope.reset(form);
				$scope.processing = false;
            }).catch(function (error) {
            	$scope.doctor.schedule = angular.copy($scope.schedules);
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update doctor.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
		};

		$scope.update_doctor = function(doctor, form) {
			var valid = validate_schedules();
			if (!valid) {
	            ngToast.create({
	                className: 'warning',
	                content: 'Invalid timings entered.',
	                dismissButton: true
	            });
			} else {
				$scope.processing = true;
				$scope.doctor.schedule = angular.copy($scope.schedules);
				for (var i = 0; i < $scope.doctor.schedule.length; i++) {
					for (var j = 0; j < $scope.doctor.schedule[i].days.length; j++) {
						if ($scope.doctor.schedule[i].days[j].selected === 'no') {
							$scope.doctor.schedule[i].days.splice(j, 1);
							j--;
						} else {
							delete $scope.doctor.schedule[i].days[j].selected;
						}
					}
				}
				$scope.doctor.pmdc_number = '' + $scope.doctor.pmdc_number;
				hospitalAdmin_service.update_doctor($scope.doctor).then(function (doctor) {
		            ngToast.create({
		                className: 'success',
		                content: 'Doctor Updated Successfully.',
		                dismissButton: true
		            });
					
					//create service pricing for doctor
			/*		var service = {
						'description' : doctor.data.name,
						'price' : doctor.data.consultation_fee,
						'category' : 'Hospital-OPD-Consultation'
					};
					
					 hospitalAdmin_service.create_facility_price(service).then(function (response) {
						var serviceDetails = {
								description : doctor.data.name, 
								category : 'Hospital-OPD-Consultation',
								expenses : [],
								shares : [{
											doctorId : doctor.data._id,
											doctorName : doctor.data.name,
											commissionPerc : doctor.data.percentage_fee
										}]
						};
						hospitalAdmin_service.update_service_details(serviceDetails).then(function (response) {
							$scope.reset(form);
							$scope.processing = false;
						});
					 });*/
					$scope.reset(form);
					$scope.processing = false;
                }).catch(function (error) {
                	$scope.doctor.schedule = angular.copy($scope.schedules);
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update doctor.',
                        dismissButton: true
                    });
                    $scope.processing = false;
                });
			}
		};

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.delete_doctor = function(form, panel) {
        	$scope.confirmationPopup = 'confirmation-popup-doctor';
        };

		$scope.confirm_delete_popup = function(form) {
			$scope.processing = true;
			hospitalAdmin_service.delete_doctor($scope.doctor._id).then(function (response) {
	            ngToast.create({
	                className: 'success',
	                content: 'Doctor Deleted Successfully.',
	                dismissButton: true
	            });
				$scope.reset(form);
				$scope.confirmationPopup = 'hide-popup';
				$scope.processing = false;
            }).catch(function (error) {
            	$scope.doctor.schedule = angular.copy($scope.schedules);
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete doctor.',
                    dismissButton: true
                });
                $scope.processing = false;
            });
		};

		$scope.reset_doctor = function(form) {
			$scope.reset(form);
		};

		$scope.remove_timing = function(index) {
			$scope.schedules.splice(index, 1);
		};

		$scope.increment = function(index, start, check) {
			if (check === 'mins') {
				if (start === true) {
					if ($scope.schedules[index].startmins + 1 < 60) {
						$scope.schedules[index].startmins++;
					} else {
						$scope.schedules[index].startmins = 0;
					}
				} else if (start === false) {
					if ($scope.schedules[index].endmins + 1 < 60) {
						$scope.schedules[index].endmins++;
					} else {
						$scope.schedules[index].endmins = 0;
					}
				}
			} else if (check === 'hrs') {
				if (start === true) {
					if ($scope.schedules[index].starthrs + 1 < 13) {
						$scope.schedules[index].starthrs++;
					} else {
						$scope.schedules[index].starthrs = 1;
					}
				} else if (start === false) {
					if ($scope.schedules[index].endhrs + 1 < 13) {
						$scope.schedules[index].endhrs++;
					} else {
						$scope.schedules[index].endhrs = 1;
					}
				}
			}
		};

		$scope.decrement = function(index, start, check) {
			if (check === 'mins') {
				if (start === true) {
					if ($scope.schedules[index].startmins - 1 > -1) {
						$scope.schedules[index].startmins--;
					} else {
						$scope.schedules[index].startmins = 59;
					}
				} else if (start === false) {
					if ($scope.schedules[index].endmins - 1 > -1) {
						$scope.schedules[index].endmins--;
					} else {
						$scope.schedules[index].endmins = 59;
					}
				}
			} else if (check === 'hrs') {
				if (start === true) {
					if ($scope.schedules[index].starthrs - 1 > 0) {
						$scope.schedules[index].starthrs--;
					} else {
						$scope.schedules[index].starthrs = 12;
					}
				} else if (start === false) {
					if ($scope.schedules[index].endhrs - 1 > 0) {
						$scope.schedules[index].endhrs--;
					} else {
						$scope.schedules[index].endhrs = 12;
					}
				}
			}
		};

		$scope.doctorsSearchTypeahead = function(val) {
        	return hospitalAdmin_service.search_doctors(val)
	            .then(function (response) {
	            	return response.data.map(function(item){
				        return item;
				    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve list of Doctors.',
                    dismissButton: true
                });
            });
		}
	}
]);