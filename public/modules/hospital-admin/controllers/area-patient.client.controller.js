'use strict';

angular.module('hospital-admin').controller('AdminAreaController', ['$scope', '$stateParams', '$location', 'Authentication', 'hospitalAdmin_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, hospitalAdmin_service, ngToast, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showAreas = false;
        $scope.showTable = false;
        // specalities simple picklist
        $scope.prefix = 'areadiv';
        $scope.areasToShow = [];
        $scope.headings = [];
        $scope.areasData = [];
        $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 100});

        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 100});
            $scope.areasData = [];
            $scope.areasData = response.data;
            $scope.areasToShow = [];
            for (var i = 0; i < $scope.areasData.length; i++) {
                $scope.areasToShow.push({'description': $scope.areasData[i].description});
            }
        };

        var getAreas = function() {
            hospitalAdmin_service.list_areas()
                .then(function (response) {
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve areas.',
                    dismissButton: true
                });
            });
        };

        var area = {
        	'description': ''
        };

        getAreas();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var areaIndex = getIndex($scope.areasData, 'description', selected_item[0].description);
            $scope.area = $scope.areasData[areaIndex];
            $scope.UpdateItem = true;
        };

		$scope.reset = function(form) {
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.area = angular.copy(area);
            $scope.confirmationPopup = 'hide-popup';
            getAreas();
		};

        $scope.submit_area = function(area, form) {
    		hospitalAdmin_service.create_area(area).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Area Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another area exists with the same name.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add area.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.delete_area = function() {
            $scope.confirmationPopup = 'confirmation-popup area-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            hospitalAdmin_service.delete_area($scope.area).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Area Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete area.',
                    dismissButton: true
                });
            });
        };

        $scope.update_area = function(area, form) {
    		hospitalAdmin_service.update_area(area).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Area Updated Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another area exists with the same name.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update area.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_area = function(form) {
            $scope.reset(form);
        };
	}
]);