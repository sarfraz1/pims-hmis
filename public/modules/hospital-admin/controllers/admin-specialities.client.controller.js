'use strict';

angular.module('hospital-admin').controller('AdminSpecialitiesController', ['$scope', '$stateParams', '$location', 'Authentication', 'hospitalAdmin_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, hospitalAdmin_service, ngToast, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showSpecialities = false;
        $scope.showTable = false;
        // specalities simple picklist
        $scope.prefix = 'specialitydiv';
        $scope.specialitiesToShow = [];
        $scope.headings = [];
        $scope.specialitiesData = [];
        $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 100});

        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 100});
            $scope.specialitiesData = [];
            $scope.specialitiesData = response.data;
            $scope.specialitiesToShow = [];
            for (var i = 0; i < $scope.specialitiesData.length; i++) {
                $scope.specialitiesToShow.push({'description': $scope.specialitiesData[i].description});
            }
        };

        var getSpecialities = function() {
            hospitalAdmin_service.list_specialities()
                .then(function (response) {
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve specialities.',
                    dismissButton: true
                });
            });
        };

        var speciality = {
        	'description': ''
        };

        getSpecialities();

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var specialityIndex = getIndex($scope.specialitiesData, 'description', selected_item[0].description);
            $scope.speciality = $scope.specialitiesData[specialityIndex];
            $scope.UpdateItem = true;
        };

		$scope.reset = function(form) {
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.speciality = angular.copy(speciality);
            $scope.confirmationPopup = 'hide-popup';
            getSpecialities();
		};

        $scope.submit_speciality = function(speciality, form) {
    		hospitalAdmin_service.create_speciality(speciality).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Speciality Added Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another speciality exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add speciality.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.delete_speciality = function() {
            $scope.confirmationPopup = 'confirmation-popup speciality-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            hospitalAdmin_service.delete_speciality($scope.speciality).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Speciality Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete speciality.',
                    dismissButton: true
                });
            });
        };

        $scope.update_speciality = function(speciality, form) {
    		hospitalAdmin_service.update_speciality(speciality).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'Speciality Updated Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another speciality exists with the same description.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update speciality.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_speciality = function(form) {
            $scope.reset(form);
        };
	}
]);