
'use strict';

angular.module('hospital-admin').controller('expenseManagerController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','hospitalAdmin_service',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll,hospitalAdmin_service) {

        $scope.authentication = Authentication;
        
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        
        var fileFormat = '',
        keyword='all';

        var dateConverter = function(dateinput){
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var get_expenses = function(name){
            hospitalAdmin_service.list_hospital_expenses($scope.pageNo,keyword).then(function (response) {
                $scope.expenses = response.data;
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };
		
		var get_departments = function(name) {
			hospitalAdmin_service.get_facilities(name).then(function (response) {
                $scope.departments = response.data.children;
			});
		};

        $scope.$watch('keyword', function() {
            if($scope.keyword!==''){
                keyword = angular.copy($scope.keyword);
                $scope.pageNo = 0;
            }else {
                keyword = 'all';
            }
            get_expenses();
            
        });

        var init = function(){
            $scope.pageNo = 0;
            $scope.keyword = '';
            $scope.editMode = false;
            $scope.options = [
                {value: true, label: 'Yes'},
                {value: false, label: 'No'}
            ];
            $scope.expenses = [];
            $scope.expense = {
                'expenseNumber' : '',
                'expenseType' : '',
                'description' : '',
                'amount' : undefined,
                'tags' : [],
                'fileUrl' : '',
                'repeat' : false,
                'repeatNum' : '',
                'repeatFrequency' : 'month',
                'fileFormat' : '',
                'created' : new Date()
            };
            get_expenses();
			get_departments('Hospital');
        };

        init();

        $scope.changePage = function(direction){
            if(direction==='next'){
                $scope.pageNo++;
                get_expenses();
            }
            else if(direction==='prev'){
                if($scope.pageNo>0){
                    $scope.pageNo--;
                    get_expenses();
                }
            }
        };

        $scope.clearForm = function(){
            $scope.expense = {
                'expenseNumber' : '',
                'expenseType' : '',
                'description' : '',
                'amount' : undefined,
                'tags' : [],
                'fileUrl' : '',
                'repeat' : false,
                'repeatNum' : '',
                'repeatFrequency' : 'month',
                'fileFormat' : '',
                'created' : new Date()
            };
        };


        $scope.selectExpense = function(expenseObj){
            $scope.expense = angular.copy(expenseObj);
            try{
                $scope.expense.created = new Date(Date.parse($scope.expense.created));
            } catch (err){
                console.log(err);
            }
            $scope.editMode = true;
        };

        $scope.addTag = function(tagtoadd){
			var t = undefined;
			if(tagtoadd !== undefined) {
				t = $scope.searched_tags[tagtoadd];
			} else {
				t = $scope.tag;
			}
            $scope.expense.tags.push(t);
			$scope.showDoctorsDropDown = false;
            $scope.tag = '';
        };

        $scope.removeTag = function(index){
            $scope.expense.tags.splice(index,1);
        };

        $scope.uploadFile = function(file) {
            if (file) {
                var imageReader = new FileReader();
                imageReader.onload = function(image) {
                    $scope.$apply(function($scope) {
                        $scope.itemImage = image.target.result;
                        if(file.type==='image/png'){
                        	$scope.expense.fileFormat = '.png';
                        }
                    	else if(file.type==='image/jpeg') {
                    		$scope.expense.fileFormat = '.jpeg';
                    	}
                    });
                };
                imageReader.readAsDataURL(file);
            }
        };
		
        $scope.searchTags = function() {
            $scope.searched_tags = [];
            
            if ($scope.searched_tags.length == 0) {
                hospitalAdmin_service.get_expense_tags().then(function (response) {
                    if(response.data){
                        $scope.searched_tags = response.data;   
                        $scope.showDoctorsDropDown = true;    
                    }				
                });
            } else {
                $scope.showDoctorsDropDown = true;
            }
            if ($scope.tag === '') {
                $scope.showDoctorsDropDown = false;
            }
        };
		
        $scope.submit = function(){
			if($scope.expense.tags.length == 0) {
				if($scope.tag) {
					$scope.expense.tags.push($scope.tag)
				}
			}
			$scope.showDoctorsDropDown = false;
            $scope.expense.created = dateConverter($scope.expense.created);
            $scope.expense.tag2 = JSON.stringify($scope.expense.tags);  
            hospitalAdmin_service.saveExpense($scope.expense,$scope.itemImage)
	            .then(function(response) {
                    $scope.editMode = false;
                    get_expenses();
                    $scope.clearForm();
                    ngToast.create({
                        className: 'success',
                        content: 'Saved Successfully.',
                        dismissButton: true
                    });
	                //console.log(response);
            }).catch(function (error) {
                //console.log(error);
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to upload image.',
                    dismissButton: true
                });
            });
        };
        
    }
]);