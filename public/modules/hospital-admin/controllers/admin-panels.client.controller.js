'use strict';

angular.module('hospital-admin').controller('AdminPanelsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll', 'hospitalAdmin_service', '$timeout',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll, hospitalAdmin_service, $timeout) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        $scope.nodes = [];
        $scope.searchPanelKeyword = '';
        $scope.updatePanel = false;
        $scope.hideServices = false;
        var facilities = [];
        // panel simple picklist
        $scope.prefix = 'paneldiv';
        $scope.panelsToShow = [];
        $scope.headings = [];
        $scope.panelsData = [];
        $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 49});
        $scope.headings.push({'alias': 'Name', 'name': 'name', 'width': 49});

        var panel = {
        	'description': '',
        	'name': '',
        	'pocName': '',
        	'pocNumber': undefined,
        	'NTN': '',
        	'address': '',
        	'remarks': '',
        	'coverages': [{
        		'category': '',
                'facilities': [{
                    'description': '',
                    'price': '',
					'discountType': 'percent',
                    'discount': undefined
                }],
                'overallDiscount': undefined,
				'overallDiscountType': 'percent'
        	}]
        };

        var get_facilities = function(index, name) {
            hospitalAdmin_service.get_facilities(name).then(function (response) {
                if (response.data.children.length > 0) {
                    response.data.selected = '';
                    $scope.coverages[index].nodes.push(response.data);
                    $scope.services[index] = [];
                } else {
                    var category = 'Hospital';
                    for (var j = 0; j < $scope.coverages[index].nodes.length; j++)
                        category = category + '-' + $scope.coverages[index].nodes[j].selected;
                    hospitalAdmin_service.create_facilities_and_price(category).then(function (response) {
                        $scope.hideServices = false;
                        if (response.data.length > 0) {
                            $scope.services[index] = response.data;
                            $scope.services[index].discount = 0;
							$scope.services[index].discountType = 'percent';
                        } else {
                            $scope.services[index] = [{
                                'description': '',
                                'price': undefined,
                                'category': '',
                                'discount': 0,
								'discountType': 'percent'
                            }];
                        }
                    });
                }
            }).catch(function (error) {
                $scope.hideServices = true;
                if (error.message.includes('Cannot read property')) {
                    ngToast.create({
                        className: 'warning',
                        content: 'No facilities found.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve facilities.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.add_all_coverage = function() {
            hospitalAdmin_service.get_facilities_aggregate_category().then(function (response) {
                facilities = [];
                $scope.panel = {
                    'description': '',
                    'name': '',
                    'pocName': '',
                    'pocNumber': undefined,
                    'NTN': '',
                    'address': '',
                    'remarks': '',
                    'coverages': []
                };
                $scope.coverages = [{}];
                $scope.coverages[0].nodes = [];
                $scope.coverages[0].facilities = [];
                
                for(var k=0; k<response.data.length; k++) {
                    if(response.data[k].facilities.length) {
                        response.data[k].category =response.data[k]._id;
                        delete response.data[k]._id;
                        response.data[k].overallDiscount = undefined;
                        response.data[k].overallDiscountType = 'percent';
                        $scope.panel.coverages.push(response.data[k]);
                    }
                }

                for (var i = 0; i < $scope.panel.coverages.length; i++) {
                    var splitFacilities = $scope.panel.coverages[i].category.split('-');
                    if (splitFacilities[splitFacilities.length - 1] === '')
                        splitFacilities.splice(splitFacilities.length - 1, 1);
                    facilities.push(splitFacilities);
                }
                get_split_facilities(facilities[0], 0, 0);
                
            });
        };        

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        var initialize = function() {
            get_facilities(0, 'Hospital');
        };

        // checks for any blank nodes between selected nodes
        // last node can remain unselected
        var check_coverage = function() {
            var valid = true;
            if ($scope.coverages[0].overallDiscount === undefined || $scope.coverages[0].overallDiscount === null) {
                valid = false;
                ngToast.create({
                    className: 'warning',
                    content: 'Discount is required.',
                    dismissButton: true
                });
                return valid;
            }
            if ($scope.coverages[0].nodes.length === 1 && $scope.coverages[0].nodes[0].selected === '') {
                valid = false;
                ngToast.create({
                    className: 'warning',
                    content: 'Facility contains empty value.',
                    dismissButton: true
                });
                return valid;
            }
            for (var j = 0; j < $scope.coverages[0].nodes.length; j++) {
                if ($scope.coverages[0].nodes[j].selected === '') {
                    if ($scope.coverages[0].nodes[j + 1] === undefined) {
                        valid = true;
                    } else {
                        ngToast.create({
                            className: 'warning',
                            content: 'Facility contains empty value.',
                            dismissButton: true
                        });
                        valid = false;
                        break;
                    }
                }
            }
            return valid;
        };

        var create_coverages = function() {
            var coverages = [];
            for (var i = 0; i < $scope.coverages.length; i++) {
                var category = 'Hospital';
                for (var j = 0; j < $scope.coverages[i].nodes.length; j++) {
                    category = category + '-' + $scope.coverages[i].nodes[j].selected;
                }
                coverages.push({'category': category, 'overallDiscount': $scope.coverages[i].overallDiscount});
            }

            var valid = true;
            for (var i = 0; i < coverages.length; i++) {
                for (var j = 0; j < coverages.length; j++) {
                    if (i != j && coverages[i].category === coverages[j].category) {
                        valid = false;
                        break;
                    }
                }
            }
            if (valid) {
                $scope.panel.coverages = angular.copy(coverages);
                for (var i = 0; i < $scope.panel.coverages.length; i++) {
                    $scope.panel.coverages[i].facilities = [];
                    for (var j = 0; j < $scope.services[i].length; j++) {
                        $scope.panel.coverages[i].facilities.push({
                            'description': $scope.services[i][j].description,
                            'price': $scope.services[i][j].price,
                            'discount': $scope.services[i][j].discount,
							'discountType': $scope.services[i][j].discountType
                        });
                    }
                }
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Coverages contain duplicate values.',
                    dismissButton: true
                });
            }
            return valid;
        };

        var handleResponse = function(response) {
            $scope.panelsToShow = [];
            $scope.headings = [];
            $scope.panelsData = [];
            $scope.panelsData = response.data;
            $scope.headings.push({'alias': 'Description', 'name': 'description', 'width': 49});
            $scope.headings.push({'alias': 'Company Name', 'name': 'name', 'width': 49});
            for (var i = 0; i < response.data.length; i++) {
                $scope.panelsToShow.push({'description': response.data[i].description, 'name': response.data[i].name});
            }
            $scope.showPanels = true;
            var setFocus = angular.element(document.querySelector('#' + $scope.prefix + $scope.prefix));
            $timeout(function() {setFocus[0].focus()});
        };

        var get_split_facilities = function(splitFacilities, parentIndex, index) {
            if (parentIndex === facilities.length) {
                $scope.coverages.splice(0, 1);
                $scope.services.splice(0, 1);
                return;
            } else if (index === splitFacilities.length) {
                parentIndex++;
                $scope.coverages.unshift({});
                $scope.services.unshift({});
                $scope.coverages[0].nodes = [];
                $scope.coverages[0].facilities = [];
                get_split_facilities(facilities[parentIndex], parentIndex, 0);
            } else {
                if(splitFacilities[index] == 'X') {
                    splitFacilities[index] = splitFacilities[index] + "-Ray";
                    splitFacilities.splice(index+1, 1);
                }
                hospitalAdmin_service.get_facilities(splitFacilities[index]).then(function (response) {
                    if (response.data.children.length > 0) {
                        $scope.coverages[0].nodes.push(response.data);
                        $scope.coverages[0].nodes[index].selected = facilities[parentIndex][index + 1];
                        $scope.services[0] = [];
                        index++;
                        get_split_facilities(splitFacilities, parentIndex, index);
                    } else {
                        var category = 'Hospital';
                        for (var j = 1; j < facilities[parentIndex].length; j++)
                            category = category + '-' + facilities[parentIndex][j];
                        hospitalAdmin_service.create_facilities_and_price(category).then(function (response) {
                            index++;
                            if (response.data.length > 0) {
                                $scope.services[0] = response.data;
                                $scope.coverages[0].overallDiscount = $scope.panel.coverages[parentIndex].overallDiscount;
                            } else {
                                $scope.services[parentIndex] = [{
                                    'description': '',
                                    'price': undefined,
                                    'category': '',
                                    'discount': 0,
									'discountType': 'percent'
                                }];
                            }
                            for (var i = 0; i < $scope.panel.coverages[parentIndex].facilities.length; i++) {
                                for (var j = 0; j < $scope.services[0].length; j++) {
                                    if ($scope.services[0][j].description === $scope.panel.coverages[parentIndex].facilities[i].description) {
                                        $scope.services[0][j].discount = $scope.panel.coverages[parentIndex].facilities[i].discount;
										$scope.services[0][j].price = $scope.panel.coverages[parentIndex].facilities[i].price;
										$scope.services[0][j].discountType = $scope.panel.coverages[parentIndex].facilities[i].discountType;
                                    }
                                }
                            }
                            get_split_facilities(splitFacilities, parentIndex, index);
                        });
                    }
                });
            }
        };

/*
        var set_coverage_selected = function() {
            for (var i = 0; i < $scope.coverages.length; i++) {
                for (var j = 0; j < $scope.coverages[i].nodes.length; j++) {
                    if (facilities[$scope.coverages.length - i - 1][$scope.coverages.length - j - 1] !== 'Hospital')
                        $scope.coverages[i].nodes[j].selected = facilities[$scope.coverages.length - i - 1][$scope.coverages.length - j - 1];
                }
            }
        };
*/
        $scope.callbackPanel = function(panel) {
            $scope.updatePanel = true;
            $scope.showPanels = false;
            facilities = [];
            $scope.panel = {};
            $scope.coverages = [{}];
            $scope.coverages[0].nodes = [];
            $scope.coverages[0].facilities = [];
            var panelIndex = getIndex($scope.panelsData, 'description', panel[0].description);
            $scope.panel = $scope.panelsData[panelIndex];
            for (var i = 0; i < $scope.panel.coverages.length; i++) {
                var splitFacilities = $scope.panel.coverages[i].category.split('-');
                if (splitFacilities[splitFacilities.length - 1] === '')
                    splitFacilities.splice(splitFacilities.length - 1, 1);
                facilities.push(splitFacilities);
            }
            get_split_facilities(facilities[0], 0, 0);
        };

        $scope.getPanels = function() {
            if ($scope.searchPanelKeyword.length === 0) {
                hospitalAdmin_service.list_panels().then(function (response) {
                    handleResponse(response);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve panels.',
                        dismissButton: true
                    });
                });
            } else {
                hospitalAdmin_service.search_panels_by_description($scope.searchPanelKeyword).then(function (response) {
                    handleResponse(response);
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve panels.',
                        dismissButton: true
                    });
                });
            }
        };
		
	    $scope.changeDiscountType = function(index) {
				
			if ($scope.panel.coverages[index].overallDiscountType == 'percent')
				$scope.panel.coverages[index].overallDiscountType = 'amount';
			else
				$scope.panel.coverages[index].overallDiscountType = 'percent';
				
		};

        $scope.overall_discount_change = function(overallDiscount, index, overallDiscountType) {
            for (var i = 0; i < $scope.services[index].length; i++) {
                $scope.services[index][i].discount = overallDiscount;
				$scope.services[index][i].discountType = overallDiscountType?overallDiscountType:'percent';
            }
        };

        $scope.individual_discount_change = function(index) {
            $scope.coverages[index].overallDiscount = 0;
        };
		
        $scope.individual_discounttype_change = function(parentindex, index) {
			if ($scope.services[parentindex][index].discountType == 'percent' || !$scope.services[parentindex][index].discountType)
				$scope.services[parentindex][index].discountType = 'amount';
			else
				$scope.services[parentindex][index].discountType = 'percent';
			
			$scope.coverages[parentindex].overallDiscount = 0;
        };

        $scope.remove_coverage = function(index) {
            $scope.coverages.splice(index, 1);
            $scope.services.splice(index, 1);
        };

		$scope.add_coverage = function() {
            var valid = true;
            if ($scope.coverages.length != 0) {
                valid = check_coverage();
            }
            if (valid) {
                $scope.coverages.unshift({});
                $scope.services.unshift({});
                $scope.coverages[0].nodes = [];
                $scope.coverages[0].facilities = [];
                get_facilities(0, 'Hospital');
            }
		};

        $scope.reset = function(form) {
        	$scope.panel = {};
        	$scope.coverages = [{}];
            $scope.coverages[0].nodes = [];
            $scope.coverages[0].facilities = [];
            $scope.services = [{}];
            $scope.updatePanel = false;
            initialize();
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.panel = angular.copy(panel);
            facilities = [];
        };

        $scope.reset();

        $scope.submit_panel = function(form, panel) {
            var valid = check_coverage();
            if (valid) {
                valid = create_coverages();
                if (valid) {
                    hospitalAdmin_service.create_panel($scope.panel).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Panel Added Successfully.',
                            dismissButton: true
                        });
                        $scope.reset(form);
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to add panel.',
                            dismissButton: true
                        });
                    });
                }
            }
        };

        $scope.update_panel = function(form, panel) {
            var valid = check_coverage();
            if (valid) {
                valid = create_coverages();
                if (valid) {
                    hospitalAdmin_service.update_panel($scope.panel).then(function (response) {
                        ngToast.create({
                            className: 'success',
                            content: 'Panel Updated Successfully.',
                            dismissButton: true
                        });
                        $scope.reset(form);
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to update panel.',
                            dismissButton: true
                        });
                    });
                }
            }
        };

        $scope.reset_panel = function(form) {
        	$scope.reset(form);
        };

        $scope.select_facility = function (parentIndex, index, value) {
            var i = index + 1;
            var length = $scope.coverages[parentIndex].nodes.length - index;
            if (i > 0) {
                $scope.coverages[parentIndex].nodes.splice(i, length);
                $scope.services[parentIndex] = [];
            }
            get_facilities(parentIndex, value);
        };
    }
]);