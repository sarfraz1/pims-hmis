'use strict';

angular.module('hospital-admin').controller('UserManagementController', ['$scope', '$stateParams', '$location', 'Authentication', 'hospitalAdmin_service', 'ngToast', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, hospitalAdmin_service, ngToast, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
				var allusers=[];
				var user_avaliable=false;
        $scope.require = false;
        $scope.UpdateItem = false;
        $scope.confirmationPopup = 'hide-popup';
        $scope.showUsers = false;
        $scope.showTable = false;
        // specalities simple picklist
        $scope.prefix = 'userdiv';
        $scope.usersToShow = [];
        $scope.headings = [];
        $scope.usersData = [];
        $scope.stores = [];
        $scope.Salesman = [];
        $scope.headings.push({'alias': 'Username', 'name': 'username', 'width': 33});
		$scope.headings.push({'alias': 'Display Name', 'name': 'displayName', 'width': 33});
		$scope.headings.push({'alias': 'Role', 'name': 'roles', 'width': 33});

        var handleResponse = function(response) {
            $scope.headings = [];
            $scope.headings.push({'alias': 'Username', 'name': 'username', 'width': 33});
			$scope.headings.push({'alias': 'Display Name', 'name': 'displayName', 'width': 33});
			$scope.headings.push({'alias': 'Role', 'name': 'roles', 'width': 33});
            $scope.usersData = [];
            $scope.usersData = response.data;
            $scope.usersToShow = [];
            //console
            for (var i = 0; i < $scope.usersData.length; i++) {
                $scope.usersToShow.push({'username': $scope.usersData[i].username, 'displayName': $scope.usersData[i].displayName, 'roles':$scope.usersData[i].roles});
            }
        };

        var getUsers = function() {
            hospitalAdmin_service.getusers()
                .then(function (response) {
									allusers=response.data;
                    if (response.data.length === 0) {
                        $scope.showTable = false;
                    } else {
                        handleResponse(response);
                        $scope.showTable = true;
                    }
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve users.',
                    dismissButton: true
                });
            });
        };

        (function getStores(){
            hospitalAdmin_service.list_stores().then((response) => {
                $scope.stores = response.data;
            });
        })();

        var user = {
        	'username': '',
			'displayName': '',
			'roles': '',
            'store': ''
        };

        getUsers();


        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        $scope.callbackfn = function(selected_item) { //callback function to get object from directive
            var areaIndex = getIndex($scope.usersData, 'username', selected_item[0].username);
            $scope.user = $scope.usersData[areaIndex];
            $scope.UpdateItem = true;
            if($scope.user.roles === 'pharmacy salesman'){
                getSalesmanStore();
                $scope.require = true;
            } else {
                $scope.require = false;
            }
        };

		$scope.reset = function(form) {
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
			$location.hash('headerid');
			$anchorScroll.yOffset = 100;
      		$anchorScroll();
      		$scope.UpdateItem = false;
			$scope.user = angular.copy(user);
            $scope.confirmationPopup = 'hide-popup';
            getUsers();
		};

        $scope.submit_user = function(user, form) {
					//console.log(allusers);
					for(var i=0; i<allusers.length;i++) {
						if(user.username===allusers[i].username){
							user_avaliable=true;
							break;
						}
					}
					if(!user_avaliable) {
						hospitalAdmin_service.createuser(user).then(function(response) {
		                ngToast.create({
		                    className: 'success',
		                    content: 'User Added Successfully',
		                    dismissButton: true
		               });
								 });
							 }

                else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another user exists with the same name.',
                        dismissButton: true
                    });
                }

        };

        $scope.delete_user = function() {
            $scope.confirmationPopup = 'confirmation-popup user-popup';
        };

        $scope.hide_delete_popup = function() {
            $scope.confirmationPopup = 'hide-popup';
        };

        $scope.confirm_delete_popup = function(form) {
            $scope.confirmationPopup = 'hide-popup';
            hospitalAdmin_service.deleteuser($scope.user).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'User Deleted Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to delete user.',
                    dismissButton: true
                });
            });
        };

        $scope.update_user = function(user, form) {
    		hospitalAdmin_service.updateuser(user, user._id).then(function(response) {
                ngToast.create({
                    className: 'success',
                    content: 'User Updated Successfully',
                    dismissButton: true
                });
				$scope.reset(form);
            }).catch(function (error) {
                if (error.data.message.indexOf('duplicate') !== -1) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Another user exists with the same name.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to update user.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.reset_user = function(form) {
            $scope.reset(form);
        };

        var getSalesmanStore = function() {
						$scope.SalesmanStore = "None";
            hospitalAdmin_service.getauser($scope.user._id).then((response) => {
                $scope.user.store = response.data.store;
            });
        };




/*		var options = {
		// changes the value of 'this' in the success, error, timeout and complete
		// handlers. The default value of 'this' is the instance of the PrintNodeApi
		// object used to make the api call
		context: null,
		// called if the api call was a 2xx success
		success: function (response, headers, xhrObject) {
			console.log(this);
			console.log("success", response, headers);
		},
		// called if the api call failed in any way
		error: function (response, headers, xhrObject) {
			console.log("error", response, headers);
		},
		// called afer the api call has completed after success or error callback
		complete: function (xhrObject) {
		  console.log(
			  "%d %s %s returned %db in %dms",
			  response.xhr.status,
			  response.reqMethod,
			  response.reqUrl,
			  response.xhr.responseText.length,
			  response.getDuration()
		  );
		},
		// called if the api call timed out
		timeout: function (url, duration) {
			console.log("timeout", url, duration)
		},
		// the timeout duration in ms
		timeoutDuration: 3000
		};

		var api = new PrintNode.HTTP(
			new PrintNode.HTTP.ApiKey('2c39d05157f363b6291c982f3041831e609edd37'),
			options
		);

		api.whoami(options).then(
		  function success (response, info) {
			console.log(response, info);
		  },
		  function error (err) {
			console.error(err);
		  }
		);*/
	}
]);
