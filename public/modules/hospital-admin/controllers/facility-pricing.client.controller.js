
'use strict';

angular.module('hospital-admin').controller('FacilityPricingController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','hospitalAdmin_service','print_service','$http',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll,hospitalAdmin_service,print_service,$http) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.saving = false;
        $scope.nodes = [];
        $scope.showGrid = false;
        $scope.selectedService = '';

        $scope.services = [{
            'description' : '',
            'price' : undefined,
            'category' : ''
        }];

				var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();

        var get_facilities = function(name){
            hospitalAdmin_service.get_facilities(name).then(function (response) {
                if(response.data.children.length>0){
                    response.data.selected = '';
                    $scope.showGrid = false;
                    $scope.nodes.push(response.data);
                }
                else {
                    var category = 'Hospital';
                    for(var j=0;j<$scope.nodes.length;j++){
                        category = category+'-'+$scope.nodes[j].selected;
                    }
                    hospitalAdmin_service.create_facilities_and_price(category).then(function (response) {
                        if(response.data.length>0){
                            $scope.services = response.data;
                            //$scope.showGrid = false;
                        }
                        else{

                            $scope.services = [{
                                'description' : '',
                                'price' : undefined,
                                'category' : ''
                            }];
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve doctors.',
                            dismissButton: true
                        });
                    });
                    $scope.showGrid = true;
                }

            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        var initServiceDetails = function(){

            $scope.expenses = [{
                description: '',
                cost : undefined
            }];

            $scope.shares = [{
                doctorId : '',
                doctorName : '',
                commissionPerc : undefined
            }];
        };

        initServiceDetails();

        $scope.selectFacility = function(index){
            //if()
            var i = index+1;
            var length = $scope.nodes.length-index;
            if(i>0)
                $scope.nodes.splice(i,length);
            $scope.services = [];
            get_facilities($scope.nodes[index].selected);


        };

        $scope.selectService = function(serviceName){
            var category = 'Hospital';
            for(var j=0;j<$scope.nodes.length;j++){
                category = category+'-'+$scope.nodes[j].selected;
            }
            $scope.selectedService = serviceName;
            hospitalAdmin_service.get_service_details(serviceName,category).then(function (response) {

                if(response.data){
                    if(response.data.expenses){
                         $scope.expenses = response.data.expenses;
                    }else{
                        $scope.expenses = [{
                            description: '',
                            cost : undefined
                        }];
                    }

                    if(response.data.shares){
                        $scope.shares = response.data.shares;
                    } else {
                        $scope.shares = [{
                            doctorId : '',
                            doctorName : '',
                            commissionPerc : undefined
                        }];
                    }


                }else {

                    initServiceDetails();
                }
            }).catch(function (error) {
                initServiceDetails();
                ngToast.create({
                    className: 'danger',
                    content: 'Error: No service details found.',
                    dismissButton: true
                });
            });

        };

        $scope.callbackdoctor = function(selectedDoctor,index){
            $scope.shares[index].doctorId = selectedDoctor._id;
            $scope.shares[index].doctorName = selectedDoctor.name;
        };

        $scope.doctorsSearchTypeahead = function(val) {
        	return hospitalAdmin_service.search_doctors(val)
	            .then(function (response) {
	            	return response.data.map(function(item){
				        return item;
				    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve list of Doctors.',
                    dismissButton: true
                });
            });
		}

        $scope.addService = function(){
            $scope.services.unshift({
                'description' : '',
                'price' : undefined,
                'category' : ''
            });
        };

        $scope.removeItem = function(objList,index){
            objList.splice(index,1);
        };



        $scope.addExpense = function(){
            $scope.expenses.push({
                description: '',
                cost : undefined
            });
        };

        $scope.addShare = function(){
            $scope.shares.push({
                doctorId : '',
                doctorName : '',
                commissionPerc : undefined
            });
        };

        get_facilities('Hospital');

        $scope.submit = function(){
            var category = 'Hospital';
            var count = 0;
            for(var j=0;j<$scope.nodes.length;j++){
                category = category+'-'+$scope.nodes[j].selected;
            }
            for(var i=0;i<$scope.services.length;i++){
                $scope.services[i].category = category;
                if($scope.services[i].description && $scope.services[i].price){
                    hospitalAdmin_service.create_facility_price($scope.services[i]).then(function (response) {

                        count++;
                        if(count===$scope.services.length){
                            ngToast.create({
                                className: 'success',
                                content: 'Success: Service price saved.',
                                dismissButton: true
                            });
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve doctors.',
                            dismissButton: true
                        });
                    });
                }
            }
        };

				$scope.exportfile = function(){
					var filestring = '';

					//create_header
					var header = "ID,Description,price\n";
					filestring +=header;
					for(var i=0;i<$scope.services.length;i++) {
							var line = $scope.services[i]._id;
							line+= ','+ $scope.services[i].description;
							line+= ','+ $scope.services[i].price;
							filestring+=line+'\n';
			}
			var data = new Blob([filestring], { type: 'text/plain;charset=utf-8' });
			return saveAs(data, ['Service-Pricing' + '.csv']);
		};

		$scope.printreport = function() {
			var services = [];
			var obj = {
			'services': $scope.services
		}
			obj.hospitallogo = $scope.hosDetail[0].image_url;
			print_service.print('/modules/hospital-admin/views/service-pricing-print.client.view.html',obj,
			function(){
			});
		};

        $scope.submitDetails = function(){
            var category = 'Hospital';
            for(var j=0;j<$scope.nodes.length;j++){
                category = category+'-'+$scope.nodes[j].selected;
            }
            var serviceDetails = {
                description : $scope.selectedService,
                category : category,
                expenses : $scope.expenses,
                shares : $scope.shares
            };
            hospitalAdmin_service.update_service_details(serviceDetails).then(function (response) {
                initServiceDetails();
                ngToast.create({
                    className: 'success',
                    content: 'Success: Service details saved.',
                    dismissButton: true
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Service details not saved.',
                    dismissButton: true
                });
            });

        };

    }
]);
