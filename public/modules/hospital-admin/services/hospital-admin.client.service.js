'use strict';

angular.module('hospital-admin').factory('hospitalAdmin_service', ['$http','Upload', 'config_service',
	function($http, Upload, config_service) {
		/**
		 * Methods for Doctors
		 */
		var srvr_address = config_service.serverAddress;
		 
		this.create_doctor = function(doctor) {
			return $http.post(srvr_address+'/doctor', doctor);
		};

		this.get_doctors = function() {
			return $http.get(srvr_address+'/doctor');
		};

		this.get_doctor = function(id) {
			return $http.get(srvr_address+'/doctor/' + id);
		};

		this.delete_doctor = function(id) {
			return $http.delete(srvr_address+'/doctor/' + id);
		};

		this.update_doctor = function(doctor) {
			return $http.put(srvr_address+'/doctor/' + doctor._id, doctor);
		};

		this.search_doctors = function(keyword) {
			return $http.get(srvr_address+'/getDoctor/' + keyword);
		};

		
		this.get_specialities = function() {
			return $http.get(srvr_address+'/specialities');
		};

		/**
		 * Methods for Specialities
		 */
		this.list_specialities = function() {
			return $http.get(srvr_address+'/specialities');
		};

		this.create_speciality = function(speciality) {
			return $http.post(srvr_address+'/specialities', speciality);
		};

		this.delete_speciality = function(speciality) {
			return $http.delete(srvr_address+'/specialities/' + speciality._id);
		};

		this.update_speciality = function(speciality) {
			return $http.put(srvr_address+'/specialities/' + speciality._id, speciality);
		};

		this.create_facilities = function(facility) {
			return $http.post(srvr_address+'/facility', facility);
		};

		this.create_facility_price = function(service) {
			return $http.post(srvr_address+'/facility-pricing', service);
		};

		this.create_facilities_and_price = function(category) {
			return $http.get(srvr_address+'/facility-pricing/'+category);
		};

		this.get_facilities_and_price = function() {
			return $http.get(srvr_address+'/facility-pricing');
		};

		this.get_facilities_aggregate_category = function() {
			return $http.get(srvr_address+'/facility-pricing/aggregatebycategory');
		};

		this.get_facilities = function(facilityName) {
			return $http.get(srvr_address+'/facility/'+ facilityName);
		};
		
		this.get_facilities_byname_category = function(category, facilityName) {
			return $http.get(srvr_address+'/facility-pricing/'+ category+'/'+facilityName);
		};

		this.remove_facility = function(obj) {
			return $http.post(srvr_address+'/removeFacility',obj);
		};

		this.create_panel = function(panel) {
			return $http.post(srvr_address+'/panel', panel);
		};

		this.update_panel = function(panel) {
			return $http.put(srvr_address+'/panel/' + panel._id, panel);
		};

		this.get_service_details = function(serviceDesc,category) {
			return $http.get(srvr_address+'/facility-details/'+serviceDesc+'/'+category);
		};

		this.get_expense_tags = function() {
			return $http.get(srvr_address+'/gettags');
		};
		
		this.update_service_details = function(serviceDetails) {
			return $http.post(srvr_address+'/facility-details', serviceDetails);
		};

		this.search_panels_by_description = function(keyword) {
			return $http.get(srvr_address+'/panel/description/' + keyword);
		};

		this.get_last_nodes_facilities = function() {
			return $http.get(srvr_address+'/facilitylastNodes');
		};

		/**
		 * Methods for Patient Area
		 */		

		this.get_areas = function() {
			return $http.get(srvr_address+'/areas');
		};

		this.list_areas = function() {
			return $http.get(srvr_address+'/areas');
		};

		this.create_area = function(area) {
			return $http.post(srvr_address+'/areas', area);
		};
		
		this.createuser = function(user) {
			return $http.post(srvr_address+'/usermanagement', user);
		};
		
		this.updateuser = function(user, userid) {
			return $http.put(srvr_address+'/usermanagement/'+userid, user);
		};
		
		this.getauser = function(userid) {
			return $http.get(srvr_address+'/usermanagement/' + userid);
		};

		this.getusers = function() {
			return $http.get(srvr_address+'/usermanagement');
		};		

		this.delete_area = function(area) {
			return $http.delete(srvr_address+'/areas/' + area._id);
		};

		this.update_area = function(area) {
			return $http.put(srvr_address+'/areas/' + area._id, area);
		};

		this.list_panels = function() {
			return $http.get(srvr_address+'/panel');
		};
		
		this.getPanels = function() {
			return $http.get(srvr_address+'/getPanelList');
		};

		this.list_hospital_expenses = function(pageNo,keyword) {
			return $http.get(srvr_address+'/hospitalExpense/'+pageNo+'/'+keyword);
		};

		this.list_stores = function() {
			return $http.get(srvr_address+'/inventory-store');
		}

		this.saveExpense = function(item,file){
            return Upload.upload({
                url: '/hospitalExpense',
                fields: item,
                file: file
            }).success(function (data, status, headers, config) {
                //console.log('Inventory Item is Saved!');
             }).error(function (data, status, headers, config) {
            });            
        };

		return this;
	}
]);