'use strict';

//Setting up route
angular.module('hospital-admin').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('admin-specialities', {
			url: '/specialities',
			templateUrl: 'modules/hospital-admin/views/admin-specialities.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('admin-areas', {
			url: '/areas',
			templateUrl: 'modules/hospital-admin/views/area-patient.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('admin-panels', {
			url: '/panels',
			templateUrl: 'modules/hospital-admin/views/admin-panels.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('admin-doctors', {
			url: '/doctors',
			templateUrl: 'modules/hospital-admin/views/admin-doctors.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('create-facility', {
			url: '/create-facility',
			templateUrl: 'modules/hospital-admin/views/create-facility.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('facility-pricing', {
			url: '/facility-pricing',
			templateUrl: 'modules/hospital-admin/views/facility-pricing.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('manage-expense', {
			url: '/manage-expense',
			templateUrl: 'modules/hospital-admin/views/expense-manage.client.view.html',
			access: ['opd admin', 'super admin']
		}).
		state('user-management', {
			url: '/user-management',
			templateUrl: 'modules/hospital-admin/views/user-management.client.view.html',
			access: ['opd admin', 'super admin']
		});
	}
]);