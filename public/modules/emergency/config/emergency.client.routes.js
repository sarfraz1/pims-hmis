'use strict';

//Setting up route
angular.module('emergency').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('emergencyreception', {
			url: '/emergencyreception',
			controller: 'EmergencyReceptionController',
			templateUrl: 'modules/emergency/views/emergencyreception.client.view.html',
			access: ['emergency receptionist', 'emergency admin', 'super admin']
		})
		.state('emergencyreception.registerPatient', {
			url: '/emergencyregisterPatient',
			controller: 'emregisterPatientController',
			templateUrl: 'modules/emergency/views/emergencyregister-patient.client.view.html',
			access: ['emergency receptionist', 'emergency admin', 'super admin']
		})
		.state('emergencyreception.viewEmergencyPatients', {
			url: '/viewEmergencyPatients',
			controller: 'ViewEmergencyPatientsController',
			templateUrl: 'modules/emergency/views/view-emergencypatients.client.view.html',
			access: ['emergency receptionist', 'emergency admin', 'super admin']
		})
		.state('emergencyreception.generateBill', {
			url: '/emergencygenerateBill/:MRN',
			controller: 'EmergencyGenerateBillController',
			templateUrl: 'modules/emergency/views/emergencygenerate-bill.client.view.html',
			access: ['emergency receptionist', 'emergency admin', 'super admin']
		})
		.state('emergencycreate-beds', {
			url: '/emergencycreate-beds',
			controller: 'emCreateBedsController',
			templateUrl: 'modules/emergency/views/emergencyadmin-createbeds.client.view.html',
			access: ['emergency admin', 'super admin']
		});
	}
]);