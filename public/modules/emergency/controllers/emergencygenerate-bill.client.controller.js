'use strict';

angular.module('emergency').controller('EmergencyGenerateBillController', ['$scope', '$http', '$stateParams', '$location','$timeout', 'Authentication', 'ngToast', '$anchorScroll','opd_service','hospitalAdmin_service','orderByFilter','print_service',
    function($scope, $http, $stateParams, $location, $timeout,Authentication, ngToast, $anchorScroll,opd_service,hospitalAdmin_service,orderBy,print_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        $scope.searchPatientKeyword ='';
        $scope.showPatients = false;
        // patient simple picklist
        $scope.patientData = [];
        $scope.patients = [];
        $scope.headingsPatient = [];
        $scope.prefixPatient = 'patientdiv';
        $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 50});
        $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});
        $scope.doctorName = '';
        var KeyCodes = {
            BACKSPACE : 8,
            TABKEY : 9,
            RETURNKEY : 13,
            ESCAPE : 27,
            SPACEBAR : 32,
            LEFTARROW : 37,
            UPARROW : 38,
            RIGHTARROW : 39,
            DOWNARROW : 40,
        };

        var start = function (){
          $http.get('/hospital/logoname.json').then(function(response){
          $scope.hosDetail= response.data.user;
        });
        };

        start();


        var dateConverter = function(dateinput) {
            try{
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch(error){
                return dateinput;
            }
        };

        var getDoctors = function(){
            hospitalAdmin_service.get_doctors().then(function (response) {
                $scope.doctors = response.data;
				/*for(var i=0;i<$scope.doctors.length;i++){
                    $scope.facilities.push({
                        category : '',
                        description : 'Consult '+$scope.doctors[i].name,
                        price : $scope.doctors[i].consultation_fee
                    });
                }*/
            });
        };

        var getFaciltyPricing = function(){
            hospitalAdmin_service.get_facilities_and_price().then(function (response) {
                $scope.facilities = angular.copy(response.data);
                getDoctors();
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };

        var calculateGrandTotal = function(){
            var grandTotal = 0;
            for(var i=0;i<$scope.servicesData.length;i++){
                var fee = $scope.servicesData[i].fee;
                var qty = $scope.servicesData[i].quantity;
                var dis = $scope.servicesData[i].discount;
                grandTotal = grandTotal + (fee * qty - dis);
            }

            return grandTotal;
        };

        var getIndex = function(array, prop, value) {
          var index = -1;
          for (var x = 0; x < array.length; x++) {
            if (array[x][prop] === value) {
              index = x;
            }
          }
          return index;
        };



        var getBill = function(MRN){
            opd_service.get_bill(MRN).then(function (response) {
                if(response.data){
                    $scope.patient = response.data.patientInfo;

                    if(response.data.referedDoctor){
                        $scope.doctorName = response.data.referedDoctor.name;
                        $scope.referedDoctor = JSON.stringify(response.data.referedDoctor);
                    }
                    if(response.data.referingDoctor)
                        $scope.referingDoctor = JSON.stringify(response.data.referingDoctor);
                        // console.log($scope.referingDoctor);

                    $scope.servicesData = response.data.servicesInfo;
                    //if(response.data.paymentMethod==='cheque'){
                    //}
                    //if()
                    var invoiceLen = response.data.invoice.length-1;
                    $scope.chequeInfo = response.data.invoice[invoiceLen].chequeInfo;
                    if($scope.chequeInfo)
                        $scope.chequeInfo.date = new Date($scope.chequeInfo.date);
                    $scope.cardInfo = response.data.invoice[invoiceLen].cardInfo;

                    $scope.selectPaymentMethod(response.data.invoice[invoiceLen].paymentMethod);

                    $scope.grandTotal = calculateGrandTotal();
                    $scope.recievedTemp = response.data.recievedCash;
                    $scope.balance = $scope.grandTotal-response.data.recievedCash;

                    if($scope.recieved>0){
                        $scope.disableEditing = true;
                    }else {
                        $scope.disableEditing = false;
                    }
                    //$scope.discount = response.data.discount;

                }
            });
        };

        var getHistoryBills = function(MRN){
            opd_service.get_history_bills(MRN).then(function (response) {
                $scope.billHistory = response.data;
            });
        };

        $scope.confirmRefund = function(){
            $scope.processing = true;

            opd_service.save_refund($scope.refundDetails.billNumber,{'refundAmount':$scope.refundAmount,'refundRemarks':$scope.refundRemarks}).then(function (response) {
                //$scope.billHistory = response.data;
                var newDate = new Date();
                response.data.time = newDate.getHours() + ' : ' + newDate.getMinutes();
                response.data.hospitalname = $scope.hosDetail[0].title;
                response.data.hospitallogo = $scope.hosDetail[0].image_url;
                response.data.hospitalAddress = $scope.hosDetail[0].address;
                response.data.hospitalPHNumber = $scope.hosDetail[0].number;
                print_service.print('/modules/billing/views/billing-refund-print.client.view.html',response.data,
                function(){
                    $scope.processing = false;
                    $scope.refundPopUp = false;
                    ngToast.create({
                        className: 'success',
                        content: 'Refund saved!',
                        dismissButton: true
                    });
                });

            }).catch(function(response){
                ngToast.create({
                    className: 'danger',
                    content: response.data.message,
                    dismissButton: true
                });
            });
        };

        $scope.refundClick = function(index){
            $scope.refundDetails = angular.copy($scope.billHistory[index]);
            $scope.refundPopUp = true;
        };

        $scope.printoldBill = function(index){
            var newDate = new Date($scope.billHistory[index].modified);
            $scope.billHistory[index].modified = newDate.getDate() +'-'+(Number(newDate.getMonth())+1).toString()+'-'+ newDate.getFullYear();
            $scope.billHistory[index].time = newDate.getHours() + ' : ' + newDate.getMinutes();
            $scope.billHistory[index].hospitallogo = $scope.hosDetail[0].image_url;
						$scope.billHistory[index].hospitalname = $scope.hosDetail[0].title;
						$scope.billHistory[index].hospitalAddress = $scope.hosDetail[0].address;
						$scope.billHistory[index].hospitalPHNumber = $scope.hosDetail[0].number;
            print_service.print('/modules/billing/views/billing-print.client.view.html',$scope.billHistory[index],
            function(){
            });
        };

        $scope.updateBalance = function(){
            if($scope.balance>0)
                $scope.balance = $scope.grandTotal - $scope.recieved - $scope.recievedTemp;
            else
                $scope.balance = $scope.grandTotal - $scope.recieved;
        };

        $scope.selectFacility = function(facility,service,index){

            var found = false;
            for(var i=0;i<$scope.servicesData.length;i++){
                if(facility.description===$scope.servicesData[i].description){
                    found = true;
                    ngToast.create({
                        className: 'danger',
                        content: 'Already in list.',
                        dismissButton: true
                    });
                    $timeout(function() {
                        var setFocus = angular.element(document.querySelector('#servicesInput'+index.toString()));
                        setFocus[0].focus();
                    },300);
                    break;
                }
            }

            if(found === false){
                service.service_id = facility._id;
                service.description = facility.description;
                service.fee = facility.price;
                service.quantity = 1;
                service.category = facility.category;
                if(panelId!=='None'){
                    opd_service.get_service_discount(panelId,facility.category,service.description).then(function (response) {
                        service.discount = response.data;
                        $scope.grandTotal = calculateGrandTotal();
                        $scope.updateBalance();
                    }).catch(function(response){
                        ngToast.create({
                            className: 'danger',
                            content: 'Please select a patient.',
                            dismissButton: true
                        });
                    });
                }

                $scope.addService(index+1);
                $scope.grandTotal = calculateGrandTotal();
                $scope.updateBalance();
                $scope.dropdownFlag = undefined;
            }

        };

        $scope.discountChange = function(){
            $scope.grandTotal = calculateGrandTotal();
            $scope.updateBalance();
        };

        $scope.callbackpatient = function(selected_patient) {
            var patientIndex = getIndex($scope.patientData, 'mr_number', selected_patient[0].mr_number);
            $scope.printAppointment.phoneNumber = $scope.patientData[patientIndex].phone;
            $scope.printAppointment.patientName = $scope.patientData[patientIndex].name;
            $scope.patient = $scope.patientData[patientIndex];
            $scope.showPatients = false;
            $scope.searchPatientKeyword = '';


            panelId = $scope.patientData[patientIndex].panel_id;
            getBill(selected_patient[0].mr_number);
            getHistoryBills(selected_patient[0].mr_number);
            $scope.updateBalance();
        };

        $scope.getPatients = function(searchType) {
            if ($scope.searchPatientKeyword.length > 0) {
                opd_service.search_patients($scope.searchPatientKeyword).then(function (response) {
                    $scope.patientData = [];
                    $scope.patientData = response.data;
                    $scope.patients = [];
                    $scope.headingsPatient = [];
                    $scope.headingsPatient.push({'alias': 'MR Number', 'name': 'mr_number', 'width': 30});
                    $scope.headingsPatient.push({'alias': 'Name', 'name': 'name', 'width': 40});
                    $scope.headingsPatient.push({'alias': 'Phone Number', 'name': 'phone', 'width': 28});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.patients.push({'mr_number': response.data[i].mr_number, 'name': response.data[i].name, 'phone': response.data[i].phone});
                    }
                    if (searchType === 'patients')
                        $scope.showPatients = true;
                    else if (searchType === 'appointments') {
                        $scope.showPatientAppointments = true;
                    }
                }).catch(function (error) {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve patients.',
                        dismissButton: true
                    });
                });
            } else {
                ngToast.create({
                    className: 'warning',
                    content: 'Warning: Please enter patient phone number or MR number to search.',
                    dismissButton: true
                });
            }
        };


        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
            $scope.appointments = orderBy($scope.appointments, $scope.propertyName, $scope.reverse);
        };

        $scope.selectPaymentMethod = function(selected){
            if(selected==='cash'){
                $scope.cash = true;
                $scope.cheque = false;
                $scope.card = false;
            }
            else if(selected==='cheque'){
                $scope.cash = false;
                $scope.cheque = true;
                $scope.card = false;
            }else if(selected==='card'){
                $scope.cash = false;
                $scope.cheque = false;
                $scope.card = true;
            }

            selected = true;
        };
        /////////////auto complete//////////
        $scope.referedDoctor_search_keyword = "";
        $scope.show_refered_dropdown = false;

        $scope.referingDoctor_search_keyword = "";
        $scope.show_refering_dropdown = false;

        $scope.search_refering_doctor = function() {
            if ($scope.referingDoctor_search_keyword.length > 0) {
                opd_service.search_doctor_by_name($scope.referingDoctor_search_keyword).then(function (response) {
                    $scope.searched_doctor = response.data;
                    $scope.show_refering_dropdown = true;
                });
            }

            if ($scope.referingDoctor_search_keyword.length === 0) {
                $scope.show_refering_dropdown = false;
            }

        };

        $scope.search_refered_doctor = function() {

                if ($scope.referedDoctor_search_keyword.length > 0) {
                    opd_service.search_doctor_by_name($scope.referedDoctor_search_keyword).then(function (response) {
                        $scope.searched_doctor = response.data;
                        $scope.show_refered_dropdown = true;
                    });
                }

                if ($scope.referedDoctor_search_keyword.length === 0) {
                    $scope.show_refered_dropdown = false;
                }
        };

        $scope.add_search_referingDoctor = function(doc){
            $scope.show_refering_dropdown = false;
            $scope.referingDoctor_search_keyword = doc;
            $scope.referingDoctor.name = doc;
        };

        $scope.add_search_referedDoctor = function(doc){
            $scope.show_refered_dropdown = false;
            $scope.referedDoctor_search_keyword = doc;
            $scope.referedDoctor.name = doc;

        };


        ////////////////////////////////////
        $scope.addService = function(index){
            $scope.servicesData.push({
                'description' : '',
                'fee' : 0,
                'quantity' : 0,
                'discount' : 0
            });
            $timeout(function() {
                var setFocus = angular.element(document.querySelector('#servicesInput'+index.toString()));
                setFocus[0].focus();
            },300);
        };

        $scope.removeService = function(index){
            $scope.servicesData.splice(index,1);
            $scope.grandTotal = calculateGrandTotal();
            $scope.updateBalance();

        };

        $scope.openDropdown = function(index){
                $scope.dropdownFlag = index;
                if (!e) var e = window.event;
                    e.cancelBubble = true;
                    if (e.stopPropagation) e.stopPropagation();
        };

        $scope.closeDropdown = function(event){
                $scope.dropdownFlag = undefined;
        };

        $scope.onKeydown = function(item, $event) {
            var e = $event;
            var $target = $(e.target);
            var nextTab;
            switch (e.keyCode) {
                case KeyCodes.ESCAPE:
                    $target.blur();
                    break;
                case KeyCodes.UPARROW:
                    nextTab = - 1;
                    break;
                case KeyCodes.RETURNKEY: e.preventDefault();
                case KeyCodes.DOWNARROW:
                    nextTab = 1;
                    break;
            }
            if (nextTab != undefined) {
                // do this outside the current $digest cycle
                // focus the next element by tabindex
                if (parseInt($target.attr('tabindex')) + nextTab !== 0)
                    $timeout(function() { $('[tabindex=' + (parseInt($target.attr('tabindex')) + nextTab) + ']').focus()});
            }
        };

        $scope.onFocus = function(item, $event) {
            // clear all other items
            angular.forEach($scope.facilities, function(item) {
                item.selected = undefined;
            });

            // select this one
            item.selected = 'selected';
        };

        $scope.submit = function(){
            $scope.processing = true;
            var referedDoctor,referingDoctor;

            if($scope.referedDoctor){
                try {
                    referedDoctor = $scope.referedDoctor;
                } catch(err){
                    console.log(err);
                }
            }
            if($scope.referingDoctor){
                try {
                    referingDoctor = $scope.referingDoctor;
                } catch(err){
                    console.log(err);
                }
            }

            var billingDetails = {
                'patientInfo' : $scope.patient,
                'referedDoctor' : referedDoctor,
                'referingDoctor' : referingDoctor,
                'servicesInfo' : $scope.servicesData,
                'grandTotal' : $scope.grandTotal,
                'recievedCash' : $scope.recieved,
                'discount' : $scope.discount,
            };

            billingDetails.status = 'billed';

            if($scope.cheque){
                billingDetails.chequeInfo = angular.copy($scope.chequeInfo);
                billingDetails.chequeInfo.date = dateConverter(billingDetails.chequeInfo.date);
                billingDetails.paymentMethod = 'cheque';
            } else if($scope.card) {
                billingDetails.cardInfo = $scope.cardInfo;
                billingDetails.paymentMethod = 'card';
            }
            else {
                billingDetails.paymentMethod = 'cash';
            }

            billingDetails.handlerName = $scope.authentication.user.username;


            for(var i=0;i<billingDetails.servicesInfo.length;i++){
				if(billingDetails.servicesInfo[i].description === 'Emergency M/O Charges' && !referedDoctor.name) {
					ngToast.create({
						className: 'danger',
						content: 'Error: Please select refered doctor.',
						dismissButton: true
					});
					$scope.processing = false;
					return;
				}
                if(billingDetails.servicesInfo[i].description==='' || billingDetails.servicesInfo[i].description===undefined){
                    billingDetails.servicesInfo.splice(i,1);
                    i--;
                }
            }

            opd_service.save_bill(billingDetails).then(function (response) {
                var newTime = new Date();
                if(response.data.grandTotal===response.data.recievedCash) {
                    var labOrder = {
                        patientMRN: response.data.patientInfo.mr_number,
                        patientName: response.data.patientInfo.name,
                        time: newTime.getHours()+':'+newTime.getMinutes(),
                        created_date: dateConverter(newTime),
                        billNumber: response.data.billNumber,
                        lastModifiedBy: $scope.authentication.user.username,
                        labTests : []
                    };
                    var labTestAdded = false;
                    for(var i=0;i<billingDetails.servicesInfo.length;i++){
                        if(billingDetails.servicesInfo[i].category){
                            var labService = billingDetails.servicesInfo[i].category.indexOf('Lab');
                            if(labService>-1){
                                labTestAdded = true;
                                labOrder.labTests.push({
                                    testDescriptionId : billingDetails.servicesInfo[i].service_id,
                                    testDescription : billingDetails.servicesInfo[i].description,
                                    testCategory : billingDetails.servicesInfo[i].category
                                });
                            }
                        }
                        if(billingDetails.servicesInfo[i].description==='' || billingDetails.servicesInfo[i].description===undefined){
                            billingDetails.servicesInfo.splice(i,1);
                            i--;
                        }
                    }
                    if(labTestAdded){
                        opd_service.save_lab_order(labOrder).then(function (response) {
                            ngToast.create({
                                className: 'success',
                                content: 'Lab tests sent to take blood sample.',
                                dismissButton: true
                            });
                        }).catch(function (error) {
                            ngToast.create({
                                className: 'danger',
                                content: 'Error: Unable to save lab tests.',
                                dismissButton: true
                            });
                        });
                    }
                }

                var newDate = new Date();
                response.data.modified = newDate.getDate() +'-'+(Number(newDate.getMonth())+1).toString()+'-'+ newDate.getFullYear();
                response.data.time = newDate.getHours() + ' : ' + newDate.getMinutes();
                response.data.hospitallogo = $scope.hosDetail[0].image_url;
  							response.data.hospitalname = $scope.hosDetail[0].title;
  							response.data.hospitalAddress = $scope.hosDetail[0].address;
  							response.data.hospitalPHNumber = $scope.hosDetail[0].number;
                print_service.print('/modules/billing/views/billing-print.client.view.html',response.data,
                    function(){

                    $scope.reset();
                    //$location.path('/reception/registerPatient');
                });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save bill.',
                    dismissButton: true
                });
            });

            //console.log(billingDetails);
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }

            $scope.cash = true;
            $scope.cheque = false;
            $scope.card = false;
            $scope.chequeInfo = {
                instrumentNumber : '',
                date : undefined,
                bankName : ''
            };
            $scope.servicesData = [{
                'description' : '',
                'fee' : 0,
                'discount' : 0,
                'quantity' : 1
            }];

            $scope.referedDoctor = {};
            $scope.referingDoctor = {};
            $scope.patient = {};
            $scope.grandTotal = 0;
            $scope.recieved = 0;
            $scope.discount = 0;
            $scope.balance = 0;
            $scope.recievedTemp = 0;
            $scope.processing = false;
            $scope.dropdownFlag = undefined;
            $scope.disableEditing = false;
            $scope.refundPopUp = false;
            $scope.todayDate = new Date();
            $scope.todayDate = dateConverter($scope.todayDate);
            $scope.billHistory = [];
            panelId = undefined;
            $scope.refundDetails = {};

            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();

        };

        $scope.init = function(){
            getFaciltyPricing();

            if($stateParams.MRN)
                getBill($stateParams.MRN);
        };

        var patient = {};
        var panelId;
        $scope.printAppointment = {};
        $scope.init();
        $scope.reset();
    }
]);
