
'use strict';

angular.module('emergency').controller('emCreateBedsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', '$anchorScroll','ipd_service',
	function($scope, $stateParams, $location, Authentication, ngToast, $anchorScroll,ipd_service) {

        $scope.authentication = Authentication;
        
        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');
        
        $scope.saving = false;
        $scope.nodes = [];
        $scope.showGrid = false;
        $scope.selectedService = '';
        
        $scope.services = [{
            'description' : '',
            'price' : undefined,
            'category' : ''
        }];

        

        var get_facilities = function(name){
            ipd_service.get_wards(name).then(function (response) {
                if(response.data.children.length>0){
                    response.data.selected = '';
                    $scope.showGrid = false;
                    $scope.nodes.push(response.data);
                }
                else {
                    var category = 'Hospital';
                    for(var j=0;j<$scope.nodes.length;j++){
                        category = category+'-'+$scope.nodes[j].selected;
                    }
                    ipd_service.get_bed_and_price(category).then(function (response) {
                        if(response.data.length>0){
                            $scope.services = response.data;
							$scope.showGen = true;
                            //$scope.showGrid = false;
                        }
                        else{

                            $scope.services = [{
                                'description' : '',
                                'price' : undefined,
                                'category' : ''
                            }];
							$scope.showGen = true;
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve doctors.',
                            dismissButton: true
                        });
                    });
                    $scope.showGrid = true;
					$scope.prefix=$scope.nodes[$scope.nodes.length-1].selected+'-';
                }
                
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve doctors.',
                    dismissButton: true
                });
            });
        };


        $scope.selectFacility = function(index){
            //if()
            var i = index+1;
            var length = $scope.nodes.length-index;
            if(i>0)
                $scope.nodes.splice(i,length);
            $scope.services = [];
            get_facilities($scope.nodes[index].selected);

            
        };

        $scope.addService = function(){
            $scope.services.push({
                'description' : '',
                'price' : undefined,
                'category' : ''
            });
        };

        $scope.removeItem = function(objList,index){
            objList.splice(index,1);
        };

		$scope.generatebeds = function() {
			var category = 'Hospital';
			var count = 0;
			for(var j=0;j<$scope.nodes.length;j++){
				category = category+'-'+$scope.nodes[j].selected;
			}
			for(var i=1; i<=$scope.numofbeds; i++) {
				var obj = { 'description': $scope.prefix+i,
							'price' : $scope.cost,
							'category': category
					
				};
			
			ipd_service.create_bed_price(obj).then(function (response) {
                       
                        count++;
                        if(count===$scope.services.length){
                            ngToast.create({
                                className: 'success',
                                content: 'Success: Beds Generated.',
                                dismissButton: true
                            });
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to generate beds.',
                            dismissButton: true
                        });
                    });
				}
		}

        get_facilities('Hospital');

        $scope.submit = function(){
            var category = 'Hospital';
            var count = 0;
            for(var j=0;j<$scope.nodes.length;j++){
                category = category+'-'+$scope.nodes[j].selected;
            }
            for(var i=0;i<$scope.services.length;i++){
                $scope.services[i].category = category;
                if($scope.services[i].description && $scope.services[i].price){
                    ipd_service.create_bed_price($scope.services[i]).then(function (response) {
                       
                        count++;
                        if(count===$scope.services.length){
                            ngToast.create({
                                className: 'success',
                                content: 'Success: Service price saved.',
                                dismissButton: true
                            });
                        }
                    }).catch(function (error) {
                        ngToast.create({
                            className: 'danger',
                            content: 'Error: Unable to retrieve doctors.',
                            dismissButton: true
                        });
                    });
                }
            }
        };


    }
]);