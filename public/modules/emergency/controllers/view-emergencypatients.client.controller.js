'use strict';

angular.module('emergency').controller('ViewEmergencyPatientsController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'opd_service', 'print_service', 'ipd_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, opd_service, print_service, ipd_service, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showDoctorsDropDown = false;
        $scope.searchDoctor = '';
        $scope.searchDoctorID = '';
        $scope.searchDate = new Date();
        $scope.viewDoctor = '';
        $scope.isDoctorSelected = false;
        $scope.processing = false;
        var doctor_id = '';


        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        var removeMedicalOfficer = function() {
            for (var i = 0; i < $scope.searched_doctors.length; i++) {
                if ($scope.searched_doctors[i].speciality === 'Medical Officer') {
                    $scope.searched_doctors.splice(i, 1);
                    i--;
                }
            }
        };

        var initialize = function() {
            $scope.processing = true;
            ipd_service.get_ipd_patients().then(function (response) {
                $scope.processing = false;
                $scope.ipdpatients = response.data;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        initialize();

        // manage changes on search doctor
        var changeSearch = function(check) {
            if (check === 'appointment') {
                $scope.timeSlot = '';
            } else if (check === 'view') {
                $scope.processing = true;
                opd_service.get_doctor_date_appointments('all', getDateFormat(new Date())).then(function (response) {
                    $scope.doctorAppointments = response.data;
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve appointments.',
                        dismissButton: true
                    });
                });
            } else if (check === 'walkin') {
                $scope.isDoctorSelected = false;
            }
        };

        // manage appointments functionality
        $scope.searched_doctors = [];
        $scope.noAppointments = false;

        var dateConverter = function(dateinput){
            try {
                var utcDate = new Date(Date.UTC(dateinput.getFullYear(), dateinput.getMonth(), dateinput.getDate()));
                utcDate = utcDate.toUTCString();
                return utcDate;
            }
            catch (error) {
                return dateinput;
            }
        };

        var tempDoctor = {};

        $scope.showDoctorAppointments = function(selectedDoctorSlots) {
            tempDoctor = selectedDoctorSlots;
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(selectedDoctorSlots._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = selectedDoctorSlots.name;
                $scope.selectedDoctor = selectedDoctorSlots;
                $scope.showDoctorsDropDown = false;
            });
        };

        $scope.showDoctorAppointmentsByDate = function() {
            var appointmentDate = getDateFormat($scope.appointmentDate);
            $scope.processing = true;
            opd_service.get_appointment_by_doctor(tempDoctor._id, appointmentDate).then(function (response) {
                if (response.data === 'Not available on this day') {
                    $scope.noAppointments = true;
                } else {
                    $scope.noAppointments = false;
                    $scope.selectedDoctorSlots = response.data;
                }
                $scope.processing = false;
                $scope.showAppointments = true;
                $scope.searchDoctor = tempDoctor.name;
                $scope.selectedDoctor = tempDoctor;
                $scope.showDoctorsDropDown = false;
            });
        };

        $scope.searchDoctors = function(change, check) {
            doctor_id = null;
            if (check)
                changeSearch(check);
            if (change === 'change')
                $scope.showAppointments = false;
            $scope.searched_doctors = [];
            $scope.processing = true;
            if ($scope.searchDoctor.length > 0) {
                opd_service.search_doctor_by_name($scope.searchDoctor).then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            } else {
                opd_service.list_all_doctors().then(function (response) {
                    $scope.searched_doctors = response.data;
                    removeMedicalOfficer();
                    $scope.showDoctorsDropDown = true;
                    $scope.processing = false;
                });
            }
            if ($scope.searchDoctor === '') {
                $scope.showDoctorsDropDown = false;
            }
        };

        $scope.selectAppointmentDoctors = function(doctor) {
            doctor_id = doctor._id;
            $scope.searchDoctor = doctor.name;
            $scope.showDoctorsDropDown = false;
            $scope.processing = true;
            opd_service.get_doctor_date_appointments(doctor_id, getDateFormat($scope.searchDate)).then(function (response) {
                $scope.processing = false;
                $scope.doctorAppointments = response.data;
                $scope.showDoctorsDropDown = false;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        $scope.cancel_appointment = function(appointment) {
            $scope.processing = true;
            opd_service.delete_appointment(appointment).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Appointment Canceled Successfully',
                    dismissButton: true
                });
                var search = 'all';
                if (doctor_id != null && doctor_id != '')
                    search = doctor_id;
                opd_service.get_doctor_date_appointments(search, getDateFormat(new Date())).then(function (response) {
                    $scope.processing = false;
                    $scope.doctorAppointments = response.data;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve appointments.',
                        dismissButton: true
                    });
                });
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to cancel appointment.',
                    dismissButton: true
                });
            });
        };

        $scope.viewDoctorAppointments = function() {
            if ($scope.searchDoctor.length === 0) {
                doctor_id = 'all';
            }
            if (doctor_id === '') {
                doctor_id = 'all';
            }
            $scope.processing = true;
            opd_service.get_doctor_date_appointments(doctor_id, getDateFormat($scope.searchDate)).then(function (response) {
                $scope.processing = false;
                $scope.doctorAppointments = response.data;
                $scope.showDoctorsDropDown = false;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };

        $scope.get_appointment = function(timeSlot) {
            if (timeSlot.status !== 'Booked') {
                $scope.timeSlot = timeSlot.slot;
                $scope.showAppointmentDelete = false;
                $scope.showAppointmentSave = true;
            }
        };

        $scope.sortBy = function(propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        $scope.sortBy('time');

	function predicatBy(prop){
	    return function(a,b){
		  if( a[prop] > b[prop]){
			  return 1;
		  }else if( a[prop] < b[prop] ){
			  return -1;
		  }
		  return 0;
	   }
	};
        $scope.submit = function(){
            $scope.processing = true;
			$scope.apptdata = [];
			var appts = [];
			for(var i=0;i<$scope.doctorAppointments.length;i++) {
				appts.push($scope.doctorAppointments[i]);
				if(!appts[i].time) {
					appts[i].time = "Waiting-"+appts[i].number_in_queue;
				}
			}
			
			appts.sort(predicatBy("time"));
			$scope.apptdata.doctorAppointments = appts;
			$scope.apptdata.searchDate = moment($scope.searchDate).format('DD-MM-YYYY');
			$scope.apptdata.doctor = $scope.searchDoctor;
			print_service.print('/modules/billing/views/appointment-print.client.view.html',$scope.apptdata,
				function(){

				$scope.processing = false;
				
			});


            //console.log(billingDetails);
        };
		
    }
]);