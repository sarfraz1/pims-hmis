'use strict';

angular.module('ipd').controller('createPackageController', ['$scope', '$stateParams', '$location', 'Authentication', '$timeout', 'ngToast', 'ipd_service','print_service', '$anchorScroll',
	function($scope, $stateParams, $location, Authentication, $timeout, ngToast, ipd_service, print_service, $anchorScroll) {
        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if (!$scope.authentication.user) $location.path('/signin');

        // variables used throughout
        $scope.showPackages = false;
        $scope.updateItem = false;
        $scope.searchPackageKeyword = '';
        $scope.processing = false;
        
		// patient simple picklist
        $scope.packageData = [];
        $scope.packages = [];
        $scope.headingsPackage = [];
        $scope.prefixPackage = 'patientdiv';
        $scope.headingsPackage.push({'alias': 'Name', 'name': 'name', 'width': 100});
       // $scope.headingsPackage.push({'alias': 'Phone Number', 'name': 'phone', 'width': 50});

   /*     var patient = {
            'name': '',
            'phone' : null,
            'gender': 'male',
            'dob': undefined,
            'address' : '',
            'cnic': '',
            'mr_number': '',
            'panel_id': 'None'
        };*/

        var getIndex = function(array, prop, value) {
            var index = -1;
            for (var x = 0; x < array.length; x++) {
                if (array[x][prop] === value) {
                    index = x;
                }
            }
            return index;
        };

       	var getDepartments = function() {
            ipd_service.get_all_wards().then(function (response) {
       
                $scope.departments = angular.copy(response.data);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Wards.',
                    dismissButton: true
                });
            });
        };
		
		getDepartments();

        // callback functions
        $scope.callbackpatient = function(selected_package) {
            var patientIndex = getIndex($scope.packageData, 'name', selected_patient[0].mr_number);
            $scope.packageData[patientIndex].dob = new Date($scope.patientData[patientIndex].dob);
            
            $scope.pkg = $scope.packageData[patientIndex];
            $scope.UpdateItem = true;
            $scope.showPackages = false;
            $scope.searchPackageKeyword = '';
        };

        // retrieve data for patient searching
        $scope.getPackages = function(searchType) {
        	if ($scope.searchPackageKeyword.length > 0) {
                $scope.processing = true;
                ipd_service.search_packages($scope.searchPatientKeyword).then(function (response) {
                    $scope.packageData = [];
                    $scope.packageData = response.data;
                    $scope.packages = [];
                    $scope.headingsPackage = [];
					$scope.prefixPackage = 'patientdiv';
					$scope.headingsPackage.push({'alias': 'Name', 'name': 'name', 'width': 100});
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.packages.push({'name': response.data[i].name});
                    }
                    if (searchType === 'packages')
                        $scope.showPackages = true;
                 
                    var setFocus = angular.element(document.querySelector('#' + $scope.prefixPackage + $scope.prefixPackage));
                    $timeout(function() {setFocus[0].focus()});
                    $scope.searchPackageKeyword = '';
                    $scope.processing = false;
                }).catch(function (error) {
                    $scope.processing = false;
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to retrieve packages.',
                        dismissButton: true
                    });
                });
        	} else {
                ngToast.create({
                    className: 'warning',
                    content: 'Please enter package name to search.',
                    dismissButton: true
                });
            }
        };

        $scope.reset = function(form) {
            if (form) {
                form.$setUntouched();
                form.$setPristine();
            }
            $location.hash('headerid');
            $anchorScroll.yOffset = 100;
            $anchorScroll();
            $scope.UpdateItem = false;
            $scope.pkg = undefined;
        };

        // manage patients functionaltiy
        $scope.submit_package = function(patient, form) {
            $scope.processing = true;
            opd_service.create_package($scope.pkg).then(function (response) {


                $scope.processing = false;
                $scope.reset(form);
            
                
                $scope.pkg = response.data;
                $scope.UpdateItem = true;

                ngToast.create({
                    className: 'success',
                    content: 'Package Added Successfully',
                    dismissButton: true
                });
                
            }).catch(function (error) {
                $scope.processing = false;
                if(error.data.code===11000){
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Package already registered.',
                        dismissButton: true
                    });
                } else {
                    ngToast.create({
                        className: 'danger',
                        content: 'Error: Unable to add package.',
                        dismissButton: true
                    });
                }
            });
        };

        $scope.update_package = function(pkg, form) {
            $scope.processing = true;
            ipd_service.update_package(pkg).then(function (response) {
                ngToast.create({
                    className: 'success',
                    content: 'Package Updated Successfully',
                    dismissButton: true
                });
                $scope.reset(form);
                $scope.processing = false;
                $scope.pkg = response.data;
                $scope.UpdateItem = true;
            }).catch(function (error) {
                $scope.processing = false;
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to update package.',
                    dismissButton: true
                });
            });
        };

        $scope.reset_package = function(form) {
            $scope.reset(form);
        };


        $scope.reset();
    }
]);