'use strict';

angular.module('ipd').factory('ipd_service', ['$http', 'config_service',
	function($http, config_service) {

		var srvr_address = config_service.serverAddress;
		this.create_wards = function(facility) {
			return $http.post(srvr_address+'/ward', facility);
		};

		this.get_wards = function(facilityName) {
			return $http.get(srvr_address+'/ward/'+ facilityName);
		};
		
		this.get_all_wards = function() {
			return $http.get(srvr_address+'/wardlastNodes');
		};

		this.remove_wards = function(obj) {
			return $http.post(srvr_address+'/removeWard',obj);
		};
		
		this.get_last_nodes_wards = function() {
			return $http.get(srvr_address+'/wardlastNodes');
		};
		
		this.get_bed_and_price = function(category) {
			return $http.get(srvr_address+'/bed-pricing/'+category);
		};
		
		this.create_bed_price = function(service) {
			return $http.post(srvr_address+'/bed-pricing', service);
		};

		this.get_beds_by_ward = function(ward) {
			return $http.get(srvr_address+'/bed-pricingbycategory/'+ward);
		};
		
		this.create_ipd_patient = function(patient) {
			return $http.post(srvr_address+'/ipdpatient', patient);
		};
		
		this.add_services_bill = function(obj) {
			return $http.post(srvr_address+'/addservicestobill', obj);
		};
		
		this.get_ipd_patients = function() {
			return $http.get(srvr_address+'/ipdpatient');
		};
		
		this.update_ipd_patient = function(patient) {
			return $http.put(srvr_address+'/ipdpatient/' + patient._id, patient);
		};
		
		this.update_bed_details = function(bed) {
			return $http.put(srvr_address+'/bed-pricing/' + bed._id, bed);
		};
		
		this.search_packages = function(keyword) {
			return $http.get(srvr_address+'/getPackage/' + keyword);
		};
		
		this.create_package = function(pkg) {
			return $http.post(srvr_address+'/package', pkg);
		};

		this.get_package = function(pkg) {
			return $http.get(srvr_address+'/package/' + pkg._id);
		};

		this.update_package = function(pkg) {
			return $http.put(srvr_address+'/package/' + pkg._id, pkg);
		};
		
		this.update_ipd_medicine = function(data) {
			return $http.post(srvr_address+'/ipdpatient/updatemedicine',data);
		};
		
		this.get_facilities_by_category = function(category) {
			return $http.get(srvr_address+'/facility-pricing/'+category);
		};
		
		this.get_all_facilities = function() {
			return $http.get(srvr_address+'/facility-pricing');
		};
		
		this.add_service_to_bill = function(data) {
			return $http.post(srvr_address+'/addservicebynametobill', data);
		};	
		
		return this;
	}
]);