'use strict';

angular.module('staff').factory('staff_service', ['$http', 'config_service',
	function($http, config_service) {
		/**
		 * Methods for hospitals
		 */
		var selected_appointment_patient_info = {};
		var isDentist;
		var srvr_address = config_service.serverAddress;

		this.set_patient_info = function(patient_info){
			selected_appointment_patient_info = patient_info;
		};

		this.set_isDentist = function(value){
			//console.log(value);
			isDentist = value;
		};

		this.get_isDentist = function(){
			return isDentist;
		};

		this.search_reason_for_coming = function(keyword) {
            if(keyword!=' '){
                return $http.get(srvr_address+'/notes/reasonforcoming/'+keyword);
            }
            else {
                keyword = "All"
                return $http.get(srvr_address+'/notes/reasonforcoming/' +keyword);
            }
        };

		this.get_doctor_by_keyword = function(keyword){
			return $http.get(srvr_address+'/getDoctor/'+keyword);
		};

		this.get_patient_info = function(patirnt_info){
			return selected_appointment_patient_info;
		}; 

		this.create_hospital = function(item) {
			return $http.post(srvr_address+'/hospital', item);
		};

		this.list_hospitals = function() {
			return $http.get(srvr_address+'/hospital');
		};

		this.delete_hospital = function(item) {
			return $http.delete(srvr_address+'/hospital/' + item._id);
		};

		this.update_hospital = function(item) {
			return $http.put(srvr_address+'/hospital/' + item._id, item);
		};

		this.create_nurse_note = function(notes) {
			return $http.post(srvr_address+'/nursenotes',notes);
		};

		this.create_dental_nurse_note = function(notes) {
			return $http.post(srvr_address+'/dentalnursenotes',notes);
		};
		
		this.get_nurse_notes_mrnumber = function(mrno) {
			return $http.get(srvr_address+'/nursenotes/'+mrno);
		};

		this.get_dental_nurse_notes_mrnumber = function(mrno) {
			return $http.get(srvr_address+'/dentalnursenotes/'+mrno);
		};

		this.get_nurse_notes = function(appointmentId) {
			return $http.get(srvr_address+'/getAppointmentNurseNotes/'+appointmentId);
		};

		this.get_dental_nurse_notes = function(appointmentId) {
			return $http.get(srvr_address+'/getAppointmentDentalNurseNotes/'+appointmentId);
		};
		
		this.update_nurse_notes = function(appointmentId, data) {
			return $http.put(srvr_address+'/getAppointmentNurseNotes/'+appointmentId, data);
		};

		this.update_dental_nurse_notes = function(appointmentId, data) {
			return $http.put(srvr_address+'/getAppointmentDentalNurseNotes/'+appointmentId, data);
		};

		return this;
	}
]);