'use strict';

angular.module('staff').controller('DentalNurseNotesController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'hospital_service', 'doctor_service', 'opd_service', '$anchorScroll','staff_service',
	function($scope, $stateParams, $location, Authentication, ngToast, hospital_service, doctor_service, opd_service,$anchorScroll,staff_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'super admin' || $scope.authentication.user.roles === 'dental nurse'  ) {
            
        } else {
            $location.path('/signin');
        }
		$scope.submit = function(note,form){
			if(!note._id) {
				staff_service.create_dental_nurse_note(note)
					.then(function(response) {
						ngToast.create({
							className: 'success',
							content: 'Vitals Saved Successfully.',
							dismissButton: true
						});
						
						opd_service.update_vitals_status($scope.apponitmentInfo._id).then(function (response) {
							
						}).catch(function (error) {
						   
						});
						$location.path('/nurse-home');
						$scope.reset(form);
						//$scope.suppliers = response.data;
				}).catch(function(error) {
					ngToast.create({
						className: 'danger',
						content: 'Error: Unable to save Vitals.',
						dismissButton: true
					});
				});
			} else {
				staff_service.update_dental_nurse_notes(note.appintmentId,note)
					.then(function(response) {
						ngToast.create({
							className: 'success',
							content: 'Vitals Saved Successfully.',
							dismissButton: true
						});
						
						$location.path('/nurse-home');
						$scope.reset(form);
						//$scope.suppliers = response.data;
				}).catch(function(error) {
					ngToast.create({
						className: 'danger',
						content: 'Error: Unable to save Vitals.',
						dismissButton: true
					});
				});				
			}
        	
        };

        $scope.reasonSearchTypeahead = function(val) {
	 		var obj_items = [];
 			return staff_service.search_reason_for_coming(val).then(function (response) {
 				return response.data.map(function (item) {
 					return item;
 				});
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve reason for coming.',
                    dismissButton: true
                });
            });
	 	};
		
		var getNursingNotes = function(){
            staff_service.get_dental_nurse_notes($scope.note.appintmentId)
                .then(function(response) {
					if(response.data) {
						$scope.note = response.data;
					}
					//$scope.prescription = $scope.prescriptions[0];
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

            });
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}

			$scope.apponitmentInfo = staff_service.get_patient_info();
			if(!$scope.apponitmentInfo.bed_details) {
				$scope.note.patientName = $scope.apponitmentInfo.patient_name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.note.appintmentId = $scope.apponitmentInfo._id;
			} else {
				$scope.note.patientName = $scope.apponitmentInfo.patient_name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.ward = $scope.apponitmentInfo.bed_details.ward;
				$scope.bed = $scope.apponitmentInfo.bed_details.bed_description;
			}
			
			getNursingNotes();
			$scope.UpdateItem = false;
			$scope.confirmationPopup = 'ng-hide';
		};
		
		
		
        $scope.init = function(){
        	$scope.note = {};
        	$scope.reset();
        };

        $scope.init();
	}
]);