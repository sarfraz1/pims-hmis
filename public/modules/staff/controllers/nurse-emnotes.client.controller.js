'use strict';

angular.module('staff').controller('NurseEmergencyNotesController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'hospital_service', 'doctor_service', '$anchorScroll','staff_service', 'ipd_service', 'inventory_service',
	function($scope, $stateParams, $location, Authentication, ngToast, hospital_service, doctor_service, $anchorScroll,staff_service, ipd_service, inventory_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'nurse' || $scope.authentication.user.roles === 'emergency nurse'  ) {
            
        } else {
            $location.path('/signin');
        }
		$scope.submit = function(note,form){
			staff_service.create_nurse_note(note)
                .then(function(response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Vitals Saved Successfully.',
                        dismissButton: true
                    });
                    $location.path('/nurse-home');
                    $scope.reset(form);
                    //$scope.suppliers = response.data;
            }).catch(function(error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Vitals.',
                    dismissButton: true
                });
            });
        	
        };
		
		var getPatientPrescription = function(){
            doctor_service.get_patient_prescriptions($scope.note.MRNumber)
                .then(function(response) {
                    $scope.prescriptions = response.data;
					$scope.prescription = $scope.prescriptions[response.data.length-1];
					$scope.prescription.medicines = $scope.apponitmentInfo.medicine;
					
					for(var i=0;i<$scope.prescription.medicines.length;i++) {
						if($scope.prescription.medicines[i].last_adminsitered) {
							$scope.prescription.medicines[i].next_administer = new Date(moment($scope.prescription.medicines[i].last_adminsitered).add(4, 'h').toISOString());
						} else {
							$scope.prescription.medicines[i].next_administer = new Date(moment($scope.prescription.medicines[i].start_datetime).add(4, 'h').toISOString());
						}
					}
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };
		
        $scope.addMedicine = function(){
        	var tempMed = {
        		'name' : '',
        		'quantity' : undefined,
        		'remarks' : '',

        	};
        	$scope.medicines.push(tempMed);
        };
		
		$scope.getBeds = function() {
			ipd_service.get_beds_by_ward('Hospital-Ground Floor-Emergency').then(function (response) {
       
                $scope.beds = angular.copy(response.data);
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve beds.',
                    dismissButton: true
                });
            });
		};
		
		$scope.getBeds();
		
        $scope.getMedicines = function(val){
            return inventory_service.search_medicine_name(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };
		
        $scope.removeCol = function(obj,index) {
            obj.splice(index,1);
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
        	$scope.medicines =  [{
		        		'name' : '',
		        		'quantity' : undefined,
		        		'remarks' : ''
		        	}],
			$scope.apponitmentInfo = staff_service.get_patient_info();
			if(!$scope.apponitmentInfo.bed_details) {
				$scope.note.patientName = $scope.apponitmentInfo.patient_name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.note.appintmentId = $scope.apponitmentInfo._id;
			} else {
				$scope.note.patientName = $scope.apponitmentInfo.name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.ward = $scope.apponitmentInfo.bed_details.ward;
				$scope.bed = $scope.apponitmentInfo.bed_details.bed_description;
			}
			
			getPatientPrescription();
			$scope.UpdateItem = false;
			$scope.confirmationPopup = 'ng-hide';
		};
		
		
		
        $scope.init = function(){
        	$scope.note = {};
        	$scope.reset();
        };

        $scope.init();
	}
]);