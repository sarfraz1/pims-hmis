'use strict';

angular.module('staff').controller('NurseIPDNotesController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'hospital_service', 'doctor_service', '$anchorScroll','staff_service', 'inventory_service',
	function($scope, $stateParams, $location, Authentication, ngToast, hospital_service, doctor_service, $anchorScroll,staff_service, inventory_service) {

        $scope.authentication = Authentication;

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'nurse' || $scope.authentication.user.roles === 'ipd nurse'  ) {
            
        } else {
            $location.path('/signin');
        }
		
		
		var hours = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
		$scope.hours = ['07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','00:00','01:00','02:00','03:00','04:00','05:00','06:00'];
		$scope.rowsgrid = [1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6];
		/*	$timeout(function() {
               staff_service.send_medication_alert_nurse({})
                .then(function(response) {
                    //$scope.suppliers = response.data;
            }); 
            }, 3000);
			
			$timeout(function() {
               staff_service.send_medication_alert_doctor({})
                .then(function(response) {
                    //$scope.suppliers = response.data;
            }); 
            }, 7000);*/
			
		$scope.submit = function(note,form){
			staff_service.create_nurse_note(note)
                .then(function(response) {
                    ngToast.create({
                        className: 'success',
                        content: 'Vitals Saved Successfully.',
                        dismissButton: true
                    });
                    $location.path('/nurse-home');
                    $scope.reset(form);
                    //$scope.suppliers = response.data;
            }).catch(function(error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to save Vitals.',
                    dismissButton: true
                });
            });
        	
        };
		
		var getPatientPrescription = function(){
            doctor_service.get_patient_prescriptions($scope.note.MRNumber)
                .then(function(response) {
                    $scope.prescriptions = response.data;
					$scope.prescription = $scope.prescriptions[response.data.length-1];
					$scope.prescription.medicines = $scope.apponitmentInfo.medicine;
					
					for(var i=0;i<$scope.prescription.medicines.length;i++) {
						if($scope.prescription.medicines[i].last_adminsitered) {
							$scope.prescription.medicines[i].next_administer = new Date(moment($scope.prescription.medicines[i].last_adminsitered).add(4, 'h').toISOString());
						} else {
							$scope.prescription.medicines[i].next_administer = new Date(moment($scope.prescription.medicines[i].start_datetime).add(4, 'h').toISOString());
						}
					}
                    //$scope.suppliers = response.data;
            }).catch(function(error) {

                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to Get Prescription Prefrences.',
                    dismissButton: true
                });
            });
        };
		
        $scope.addMedicine = function(){
        	var tempMed = {
        		'name' : '',
        		'quantity' : undefined,
        		'remarks' : '',

        	};
        	$scope.medicines.push(tempMed);
        };
		
        $scope.getMedicines = function(val){
            return inventory_service.search_medicine_name(val)
                .then(function (response) {
                    return response.data.map(function(item){
                        return item;
                    });
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve Brand Names.',
                    dismissButton: true
                });
            });
        };
		
        $scope.removeCol = function(obj,index) {
            obj.splice(index,1);
        };

		$scope.reset = function(form){
			if (form) {
			  form.$setPristine();
			  form.$setUntouched();
			}
        	$scope.medicines =  [{
		        		'name' : '',
		        		'quantity' : undefined,
		        		'remarks' : ''
		        	}],
			$scope.apponitmentInfo = staff_service.get_patient_info();
			if(!$scope.apponitmentInfo.bed_details) {
				$scope.note.patientName = $scope.apponitmentInfo.patient_name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.note.appintmentId = $scope.apponitmentInfo._id;
			} else {
				$scope.note.patientName = $scope.apponitmentInfo.name;
				$scope.note.MRNumber = $scope.apponitmentInfo.mr_number;
				$scope.ward = $scope.apponitmentInfo.bed_details.ward;
				$scope.bed = $scope.apponitmentInfo.bed_details.bed_description;
			}
			
			getPatientPrescription();
			$scope.UpdateItem = false;
			$scope.confirmationPopup = 'ng-hide';
		};
		
		
		
        $scope.init = function(){
        	$scope.note = {};
        	$scope.reset();
        };

        $scope.init();
	}
]);