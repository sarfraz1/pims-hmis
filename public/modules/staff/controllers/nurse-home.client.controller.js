'use strict';

angular.module('doctor').controller('NurseHomeController', ['$scope', '$stateParams', '$location', 'Authentication', 'ngToast', 'doctor_service', '$anchorScroll','opd_service','staff_service',
	function($scope, $stateParams, $location, Authentication, ngToast, doctor_service, $anchorScroll,opd_service,staff_service) {

        $scope.authentication = Authentication;
        var tempAppointmentData = [];

        // If user is signed in then redirect back home
        if ($scope.authentication.user && $scope.authentication.user.roles === 'nurse') {
        	$scope.dentalnurse = false;
        } else if ($scope.authentication.user && $scope.authentication.user.roles === 'dental nurse') {
            $scope.dentalnurse = true;
        } else {
        	$location.path('/signin');
        }

        var getDateFormat = function(date) {
            var today = new Date(date);
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            } 
            if (mm < 10) {
                mm = '0' + mm;
            } 
            today = dd + '-' + mm + '-' + yyyy;
            return today;
        };

        $scope.sortBy = function(propertyName) {
		    $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
		        ? !$scope.reverse : false;
		    $scope.propertyName = propertyName;
		    $scope.appointmentData = orderBy(appointmentData, $scope.propertyName, $scope.reverse);
		};


        $scope.selectAppointmentDoctors = function(doctor) {

            var appointmentDate = new Date();
            appointmentDate = getDateFormat(appointmentDate);
            opd_service.get_doctor_date_appointments('all', appointmentDate).then(function (response) {
                tempAppointmentData = response.data;
                staff_service.get_doctor_by_keyword("Dentist").then(function (response) {
                    var tempData = [];
                    var dentists = response.data;
                    if($scope.dentalnurse){
                        for(var i=0;i<tempAppointmentData.length;i++){
                            for(var j=0;j<dentists.length;j++){
                                if(tempAppointmentData[i].doctor_id === dentists[j]._id){
                                    tempData.push(tempAppointmentData[i]);
                                    break;
                                }
                            }
                        }
                    } else {
                        for(var i=0;i<tempAppointmentData.length;i++){
                            for(var j=0;j<dentists.length;j++){
                                if(tempAppointmentData[i].doctor_id !== dentists[j]._id){
                                    tempData.push(tempAppointmentData[i]);
                                }
                            }
                        }
                    }
                    $scope.appointmentData = tempData;
                });
                
            }).catch(function (error) {
                ngToast.create({
                    className: 'danger',
                    content: 'Error: Unable to retrieve appointments.',
                    dismissButton: true
                });
            });
        };
		
	    var interval = setInterval($scope.selectAppointmentDoctors, 10000);

		$scope.$on("$destroy", function(event) {clearInterval(interval)});

		$scope.selectAppointment = function(appointment){
			staff_service.set_patient_info(appointment);
            if($scope.dentalnurse) {
                $location.path('/dental-nurse-notes');
            } else {
                $location.path('/nurse-notes');
            }
			
		};

		$scope.init = function(){
			$scope.appointmentData = [];
			$scope.selectAppointmentDoctors();
			$scope.appointmentData = [];
		};

		$scope.init();
	}
]);