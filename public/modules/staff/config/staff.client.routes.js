'use strict';

//Setting up route
angular.module('staff').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
		state('staff', {
			url: '/staff',
			templateUrl: 'modules/staff/views/staff.client.view.html'
		}).
		state('nurse-notes', {
			url: '/nurse-notes',
			templateUrl: 'modules/staff/views/nurse-notes.client.view.html',
			access: ['nurse', 'ipd nurse']
		}).state('dental-nurse-notes', {
			url: '/dental-nurse-notes',
			templateUrl: 'modules/staff/views/dental-nurse-notes.client.view.html',
			access: ['dental nurse', 'super admin']
		}).
		state('nurse-ipdnotes', {
			url: '/nurse-ipdnotes',
			templateUrl: 'modules/staff/views/nurse-ipdnotes.client.view.html',
			access: ['nurse', 'ipd nurse']
		}).
		state('nurse-emergencynotes', {
			url: '/nurse-emergencynotes',
			templateUrl: 'modules/staff/views/nurse-emnotes.client.view.html',
			access: ['nurse', 'emergency nurse']
		}).
		state('nurse-home', {
			url: '/nurse-home',
			templateUrl: 'modules/staff/views/nurse-home.client.view.html',
			access: ['dental nurse', 'nurse', 'ipd nurse']
		})
		.state('ipdnurse-home', {
			url: '/ipdnurse-home',
			controller: 'NurseIPDPatientsController',
			templateUrl: 'modules/staff/views/ipdnurse-home.client.view.html',
			access: ['ipd nurse']
		})
		.state('emergencynurse-home', {
			url: '/emergencynurse-home',
			controller: 'NurseEmergencyPatientsController',
			templateUrl: 'modules/staff/views/emnurse-home.client.view.html',
			access: ['emergency nurse']
		});
	}
]);